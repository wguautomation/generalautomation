
package ExtSitePages;

import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 1/25/2015.
 */
public class extDegreesAndProgramsPage extends extHomePage {
    public extDegreesAndProgramsPage() {
        this(null);
    }

    public extDegreesAndProgramsPage(WebPage existingPage) {
        super(existingPage);
    }

    public extDegreesAndProgramsPage load() {
        super.load();
        clickDegreesAndPrograms();
        return this;
    }

    public extDegreesAndProgramsPage clickTeachersCollege() {
        get(loginType.getStartingUrl() + "/education/online_teaching_degree");
        return this;
    }

    public extDegreesAndProgramsPage clickLicensurePrograms() {
        get(loginType.getStartingUrl() + "/education/teacher_certification");
        return this;
    }

    public extDegreesAndProgramsPage clickBaInterdisciplinaryStudies() {
        get(loginType.getStartingUrl() + "/education/teacher_certification_elementary_bachelor_degree");
        return this;
    }

    public extDegreesAndProgramsPage clickBaSpecialEducation() {
        get(loginType.getStartingUrl() + "/education/teacher_certification_special_education_bachelor_degree");
        return this;
    }

    public extDegreesAndProgramsPage clickBaMathematics() {
        clickLicensurePrograms();
        getByXpath("//*[@id=\"dhtml_menu-557\"]");
        getByXpath("//*[@id=\"dhtml_menu-557\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBaScience() {
        clickLicensurePrograms();
        getByXpath("//*[@id=\"dhtml_menu-558\"]");
        getByXpath("//*[@id=\"dhtml_menu-558\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBaChemistry() {
        clickLicensurePrograms();
        getByXpath("//*[@id=\"dhtml_menu-537\"]");
        getByXpath("//*[@id=\"dhtml_menu-537\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBaPhysics() {
        clickLicensurePrograms();
        getByXpath("//*[@id=\"dhtml_menu-559\"]");
        getByXpath("//*[@id=\"dhtml_menu-559\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBaBiologicalScience() {
        clickLicensurePrograms();
        getByXpath("//*[@id=\"dhtml_menu-536\"]");
        getByXpath("//*[@id=\"dhtml_menu-536\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBaGeosciences() {
        clickLicensurePrograms();
        getByXpath("//*[@id=\"dhtml_menu-538\"]");
        getByXpath("//*[@id=\"dhtml_menu-538\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickPbtElementaryEducation() {
        clickLicensurePrograms();
        getByXpath("//*[@id=\"dhtml_menu-561\"]");
        getByXpath("//*[@id=\"dhtml_menu-561\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickPbtMathematics() {
        clickLicensurePrograms();
        getByXpath("//*[@id=\"dhtml_menu-562\"]");
        getByXpath("//*[@id=\"dhtml_menu-562\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickPbtScience() {
        clickLicensurePrograms();
        getByXpath("//*[@id=\"dhtml_menu-563\"]");
        getByXpath("//*[@id=\"dhtml_menu-563\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickPbtSocialScience() {
        clickLicensurePrograms();
        getByXpath("//*[@id=\"dhtml_menu-564\"]");
        getByXpath("//*[@id=\"dhtml_menu-564\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMatElementaryEducation() {
        clickLicensurePrograms();
        getByXpath("//*[@id=\"dhtml_menu-565\"]");
        getByXpath("//*[@id=\"dhtml_menu-565\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMatEnglish() {
        clickLicensurePrograms();
        getByXpath("//*[@id=\"dhtml_menu-12366\"]");
        getByXpath("//*[@id=\"dhtml_menu-12366\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMatMathematics() {
        clickLicensurePrograms();
        getByXpath("//*[@id=\"dhtml_menu-566\"]");
        getByXpath("//*[@id=\"dhtml_menu-566\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMatScience() {
        clickLicensurePrograms();
        getByXpath("//*[@id=\"dhtml_menu-567\"]");
        getByXpath("//*[@id=\"dhtml_menu-567\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMatSocialScience() {
        clickLicensurePrograms();
        getByXpath("//*[@id=\"dhtml_menu-568\"]");
        getByXpath("//*[@id=\"dhtml_menu-568\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickGraduatePrograms() {
        clickTeachersCollege();
        getByXpath("//*[@id=\"dhtml_menu-5287\"]");
        getByXpath("//*[@id=\"dhtml_menu-5287\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMsCurriculum() {
        clickGraduatePrograms();
        getByXpath("//*[@id=\"dhtml_menu-1637\"]");
        getByXpath("//*[@id=\"dhtml_menu-1637\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMsSpecialEd() {
        clickGraduatePrograms();
        getByXpath("//*[@id=\"dhtml_menu-580\"]");
        getByXpath("//*[@id=\"dhtml_menu-580\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMsEduLeadership() {
        clickGraduatePrograms();
        getByXpath("//*[@id=\"dhtml_menu-579\"]");
        getByXpath("//*[@id=\"dhtml_menu-579\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMaEnglish() {
        clickGraduatePrograms();
        getByXpath("//*[@id=\"dhtml_menu-578\"]");
        getByXpath("//*[@id=\"dhtml_menu-578\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMedInstructionalDesign() {
        clickGraduatePrograms();
        getByXpath("//*[@id=\"dhtml_menu-572\"]");
        getByXpath("//*[@id=\"dhtml_menu-572\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMedLearningAndTechnology() {
        clickGraduatePrograms();
        getByXpath("//*[@id=\"dhtml_menu-571\"]");
        getByXpath("//*[@id=\"dhtml_menu-571\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMaMathEducation() {
        clickGraduatePrograms();
        getByXpath("//*[@id=\"dhtml_menu-577\"]");
        getByXpath("//*[@id=\"dhtml_menu-577\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMaScience() {
        clickGraduatePrograms();
        getByXpath("//*[@id=\"dhtml_menu-576\"]");
        getByXpath("//*[@id=\"dhtml_menu-576\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMaChemistry() {
        clickGraduatePrograms();
        getByXpath("//*[@id=\"dhtml_menu-575\"]");
        getByXpath("//*[@id=\"dhtml_menu-575\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMaPhysics() {
        clickGraduatePrograms();
        getByXpath("//*[@id=\"dhtml_menu-574\"]");
        getByXpath("//*[@id=\"dhtml_menu-574\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMaBiologicalScience() {
        clickGraduatePrograms();
        getByXpath("//*[@id=\"dhtml_menu-869\"]");
        getByXpath("//*[@id=\"dhtml_menu-869\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMaGeosciences() {
        clickGraduatePrograms();
        getByXpath("//*[@id=\"dhtml_menu-573\"]");
        getByXpath("//*[@id=\"dhtml_menu-573\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickEllEndorsementPrep() {
        clickGraduatePrograms();
        getByXpath("//*[@id=\"dhtml_menu-569\"]");
        getByXpath("//*[@id=\"dhtml_menu-569\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickEdLeadershipPrep() {
        clickGraduatePrograms();
        getByXpath("//*[@id=\"dhtml_menu-1177\"]");
        getByXpath("//*[@id=\"dhtml_menu-1177\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickCollegeOfBusiness() {
        clickTeachersCollege();
        getByXpath("//*[@id=\"dhtml_menu-5181\"]");
        getByXpath("//*[@id=\"dhtml_menu-5181\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBusinessBachelors() {
        clickCollegeOfBusiness();
        getByXpath("//*[@id=\"dhtml_menu-5176\"]");
        getByXpath("//*[@id=\"dhtml_menu-5176\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBsBusinessManagement() {
        clickBusinessBachelors();
        getByXpath("//*[@id=\"dhtml_menu-298\"]");
        getByXpath("//*[@id=\"dhtml_menu-298\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBsHrManagement() {
        clickBusinessBachelors();
        getByXpath("//*[@id=\"dhtml_menu-526\"]");
        getByXpath("//*[@id=\"dhtml_menu-526\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBsItManagement() {
        clickBusinessBachelors();
        getByXpath("//*[@id=\"dhtml_menu-527\"]");
        getByXpath("//*[@id=\"dhtml_menu-527\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBsSalesAndSales() {
        clickBusinessBachelors();
        getByXpath("//*[@id=\"dhtml_menu-1415\"]");
        getByXpath("//*[@id=\"dhtml_menu-1415\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBsMarketingManagement() {
        clickBusinessBachelors();
        getByXpath("//*[@id=\"dhtml_menu-528\"]");
        getByXpath("//*[@id=\"dhtml_menu-528\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBsAccounting() {
        clickBusinessBachelors();
        getByXpath("//*[@id=\"dhtml_menu-524\"]");
        getByXpath("//*[@id=\"dhtml_menu-524\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBusinessMasters() {
        clickCollegeOfBusiness();
        getByXpath("//*[@id=\"dhtml_menu-5191\"]");
        getByXpath("//*[@id=\"dhtml_menu-5191\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMbaHealthcareManagement() {
        clickBusinessMasters();
        getByXpath("//*[@id=\"dhtml_menu-529\"]");
        getByXpath("//*[@id=\"dhtml_menu-529\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMbaItManagement() {
        clickBusinessMasters();
        getByXpath("//*[@id=\"dhtml_menu-433\"]");
        getByXpath("//*[@id=\"dhtml_menu-433\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMbaMasterOfBusiness() {
        clickBusinessMasters();
        getByXpath("//*[@id=\"dhtml_menu-530\"]");
        getByXpath("//*[@id=\"dhtml_menu-530\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMsManagementAndLeadership() {
        clickBusinessMasters();
        getByXpath("//*[@id=\"dhtml_menu-11336\"]");
        getByXpath("//*[@id=\"dhtml_menu-11336\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMsAccounting() {
        clickBusinessMasters();
        getByXpath("//*[@id=\"dhtml_menu-11826\"]");
        getByXpath("//*[@id=\"dhtml_menu-11826\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickCollegeOfIt() {
        getByXpath("//*[@id=\"dhtml_menu-5127\"]");
        getByXpath("//*[@id=\"dhtml_menu-5127\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickItBachelors() {
        clickCollegeOfIt();
        getByXpath("//*[@id=\"dhtml_menu-5137\"]");
        getByXpath("//*[@id=\"dhtml_menu-5137\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBsIt() {
        clickItBachelors();
        getByXpath("//*[@id=\"dhtml_menu-432\"]");
        getByXpath("//*[@id=\"dhtml_menu-432\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBsItNetworkAdmin() {
        clickItBachelors();
        getByXpath("//*[@id=\"dhtml_menu-430\"]");
        getByXpath("//*[@id=\"dhtml_menu-430\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBsItSecurity() {
        clickItBachelors();
        getByXpath("//*[@id=\"dhtml_menu-428\"]");
        getByXpath("//*[@id=\"dhtml_menu-428\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBsItSoftware() {
        clickItBachelors();
        getByXpath("//*[@id=\"dhtml_menu-427\"]");
        getByXpath("//*[@id=\"dhtml_menu-427\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBsSoftwareDevelopment() {
        clickItBachelors();
        getByXpath("//*[@id=\"dhtml_menu-11721\"]");
        getByXpath("//*[@id=\"dhtml_menu-11721\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickItMasters() {
        clickCollegeOfIt();
        getByXpath("//*[@id=\"dhtml_menu-5132\"]");
        getByXpath("//*[@id=\"dhtml_menu-5132\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMsInfoSecurity() {
        clickItMasters();
        getByXpath("//*[@id=\"dhtml_menu-466\"]");
        getByXpath("//*[@id=\"dhtml_menu-466\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMsItManagement() {
        clickItMasters();
        getByXpath("//*[@id=\"dhtml_menu-3981\"]");
        getByXpath("//*[@id=\"dhtml_menu-3981\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickCollegeOfHealth() {
        clickDegreesAndPrograms();
        getByXpath("//*[@id=\"dhtml_menu-5087\"]");
        getByXpath("//*[@id=\"dhtml_menu-5087\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickHealthBachelors() {
        clickCollegeOfHealth();
        getByXpath("//*[@id=\"dhtml_menu-5097\"]");
        getByXpath("//*[@id=\"dhtml_menu-5097\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBsNursingRn() {
        clickHealthBachelors();
        getByXpath("//*[@id=\"dhtml_menu-5082\"]");
        getByXpath("//*[@id=\"dhtml_menu-5082\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBsNursingPrelicensure() {
        clickHealthBachelors();
        getByXpath("//*[@id=\"dhtml_menu-997\"]");
        getByXpath("//*[@id=\"dhtml_menu-997\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickBsHealthInformatics() {
        clickHealthBachelors();
        getByXpath("//*[@id=\"dhtml_menu-532\"]");
        getByXpath("//*[@id=\"dhtml_menu-532\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickHealthMasters() {
        clickCollegeOfHealth();
        getByXpath("//*[@id=\"dhtml_menu-5092\"]");
        getByXpath("//*[@id=\"dhtml_menu-5092\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMsNursingEducation() {
        clickHealthMasters();
        getByXpath("//*[@id=\"dhtml_menu-534\"]");
        getByXpath("//*[@id=\"dhtml_menu-534\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMsNursingLeadership() {
        clickHealthMasters();
        getByXpath("//*[@id=\"dhtml_menu-535\"]");
        getByXpath("//*[@id=\"dhtml_menu-535\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMsNursingEducationRn() {
        clickHealthMasters();
        getByXpath("//*[@id=\"dhtml_menu-1158\"]");
        getByXpath("//*[@id=\"dhtml_menu-1158\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage clickMsNursingLeadershipRn() {
        clickHealthMasters();
        getByXpath("//*[@id=\"dhtml_menu-1157\"]");
        getByXpath("//*[@id=\"dhtml_menu-1157\"]").click();
        return this;
    }

    public extDegreesAndProgramsPage ExercisePage(boolean cascade) {
        General.Debug("\nextDegreesAndProgramsPage::ExercisePage(" + cascade + ")");
        load();

        if (cascade) {
//            return this;
        }

        clickTeachersCollege();
        clickLicensurePrograms();
        clickBaInterdisciplinaryStudies();
        clickBaSpecialEducation();
        clickBaMathematics();
        clickBaScience();
        clickBaChemistry();
        clickBaPhysics();
        clickBaBiologicalScience();
        clickBaGeosciences();
        clickPbtElementaryEducation();
        clickPbtMathematics();
        clickPbtScience();
        clickPbtSocialScience();
        clickMatElementaryEducation();
        clickMatEnglish();
        clickMatMathematics();
        clickMatScience();
        clickMatSocialScience();

        clickGraduatePrograms();
        clickMsCurriculum();
        clickMsSpecialEd();
        clickMsEduLeadership();
        clickMaEnglish();
        clickMedInstructionalDesign();
        clickMedLearningAndTechnology();
        clickMaMathEducation();
        clickMaScience();
        clickMaChemistry();
        clickMaPhysics();
        clickMaBiologicalScience();
        clickMaGeosciences();
        clickEllEndorsementPrep();
        clickEdLeadershipPrep();

        clickCollegeOfBusiness();
        clickBusinessBachelors();
        clickBsBusinessManagement();
        clickBsHrManagement();
        clickBsItManagement();
        clickBsSalesAndSales();
        clickBsMarketingManagement();
        clickBsAccounting();

        clickBusinessMasters();
        clickMbaHealthcareManagement();
        clickMbaItManagement();
        clickMbaMasterOfBusiness();
        clickMsManagementAndLeadership();
        clickMsAccounting();

        clickCollegeOfIt();
        clickItBachelors();
        clickBsIt();
        clickBsItNetworkAdmin();
        clickBsItSecurity();
        clickBsItSoftware();
        clickBsSoftwareDevelopment();
        clickItMasters();
        clickMsInfoSecurity();
        clickMsItManagement();

        clickCollegeOfHealth();
        clickHealthBachelors();
        clickBsNursingRn();
        clickBsNursingPrelicensure();
        clickBsHealthInformatics();
        clickHealthMasters();
        clickMsNursingEducation();
        clickMsNursingLeadership();
        clickMsNursingEducationRn();
        clickMsNursingLeadershipRn();
        return this;
    }
}

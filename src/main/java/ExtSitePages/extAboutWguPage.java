
package ExtSitePages;

import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 1/22/2015.
 */
public class extAboutWguPage extends extHomePage {
    public extAboutWguPage() {
        this(null);
    }

    public extAboutWguPage(WebPage existingPage) {
        super(existingPage);
    }

    public extAboutWguPage load() {
        super.load();
        clickAboutWgu();
        return this;
    }

    public extAboutWguPage clickTheWguStory() {
        getByLinkText("The WGU Story");
        getByLinkText("The WGU Story").click();
        return this;
    }

    public extAboutWguPage clickAboutOurStudents() {
        getByLinkText("About Our Students");
        getByLinkText("About Our Students").click();
        return this;
    }

    public extAboutWguPage clickAboutWguFaculty() {
        getByLinkText("About WGU Faculty");
        getByLinkText("About WGU Faculty").click();
        return this;
    }

    // missing
    public extAboutWguPage clickStudentAndGradSuccess() {
        getByLinkText("Part");
        getByLinkText("Part").click();
        return this;
    }

    public extAboutWguPage clickHowWereDifferent() {
        getByLinkText("How We're Different");
        getByLinkText("How We're Different").click();
        return this;
    }

    public extAboutWguPage clickWhatOthersSay() {
        getByLinkText("What Others Say");
        getByLinkText("What Others Say").click();
        return this;
    }

    public extAboutWguPage clickAccreditation() {
        getByLinkText("Accreditation and Recognition");
        getByLinkText("Accreditation and Recognition").click();
        return this;
    }

    public extAboutWguPage clickUniversityGovernance() {
        getByLinkText("University Governance");
        getByLinkText("University Governance").click();
        return this;
    }

    public extAboutWguPage clickOnlineLibrary() {
        getByLinkText("Online Library");
        getByLinkText("Online Library").click();
        return this;
    }

    public extAboutWguPage clickEmploymentAtWgu() {
        getByLinkText("Employment at WGU");
        getByLinkText("Employment at WGU").click();
        return this;
    }

    public extAboutWguPage clickTheNightOwlBlog() {
        getByLinkText("WGU Connection Blog");
        getByLinkText("WGU Connection Blog").click();
        return this;
    }

    public extAboutWguPage clickInTheNews() {
        getByLinkText("In the News");
        getByLinkText("In the News").click();
        return this;
    }

    public extAboutWguPage clickFeaturedCoverage() {
        getByLinkText("Featured Coverage");
        getByLinkText("Featured Coverage").click();
        return this;
    }

    public extAboutWguPage clickNewsReleases() {
        getByLinkText("News Releases");
        getByLinkText("News Releases").click();
        return this;
    }

    public extAboutWguPage clickWguRssFeed() { // ERROR - does not exist in sidebar TODO
//        getByXpath("").click();
        return this;
    }

    public extAboutWguPage clickProfiles() {
        getByLinkText("Profiles");
        getByLinkText("Profiles").click();
        return this;
    }

    public extAboutWguPage clickNewsletters() {
        getByLinkText("Newsletters");
        getByLinkText("Newsletters").click();
        return this;
    }

    public extAboutWguPage clickResources() {
        getByLinkText("Resources");
        getByLinkText("Resources").click();
        return this;
    }

    public extAboutWguPage ExercisePage(boolean cascade) {
        General.Debug("\nextAboutWguPage::ExercisePage(" + cascade + ")");
        load();

        if (cascade) {
//            return this;
        }

        clickTheWguStory();
        clickAboutOurStudents();
        clickAboutWguFaculty();
//        clickStudentAndGradSuccess();
        clickHowWereDifferent();
        clickWhatOthersSay();
        clickAccreditation();
        clickUniversityGovernance();
        clickOnlineLibrary();
        clickEmploymentAtWgu();
        clickTheNightOwlBlog();

        load();
        clickInTheNews();
        clickFeaturedCoverage();
        clickNewsReleases();
        clickProfiles();
        clickNewsletters();
        clickResources();
        return this;
    }
}

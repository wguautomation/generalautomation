
package ExtSitePages;

/*
 * Created by timothy.hallbeck on 8/19/2015.
 */

public class extXpath {

    public static String AboutWGUPage_Loaded      = "//*[@id='main-content']/div/section/div/div/div/div[1]/h3";
    public static String ExtUrl_AboutWGU          = "/about_WGU/overview";
    public static String HomePage_FooterLoaded    = "//*[@class='footer-info__text']";

}


package ExtSitePages;

/*
 * Created by timothy.hallbeck on 1/22/2015.
 */

import BaseClasses.WebPage;
import Utils.General;

public class extTuitionFinancialAidPage extends extHomePage {
    public extTuitionFinancialAidPage() {
        this(null);
    }

    public extTuitionFinancialAidPage(WebPage existingPage) {
        super(existingPage);
    }

    public extTuitionFinancialAidPage load() {
        super.load();
        clickTuitionAndFinancialAid();
        return this;
    }

    // This no longer exists
    public extTuitionFinancialAidPage clickTuitionSidebar() {
//        getByXpath("//*[@id=\"dhtml_menu-313\"]");
//        getByXpath("//*[@id=\"dhtml_menu-313\"]").click();
        return this;
    }

    public extTuitionFinancialAidPage clickTuitionAndFees() {
        getByLinkText("Tuition and Fees");
        getByLinkText("Tuition and Fees").click();
        return this;
    }

    public extTuitionFinancialAidPage clickFinancialPolicy() { // ERROR - missing from sidebar
        return this;
    }

    public extTuitionFinancialAidPage clickFinancialAid() {
        getByLinkText("Financial Aid");
        getByLinkText("Financial Aid").click();
        return this;
    }

    public extTuitionFinancialAidPage clickVAandMilitary() {
        getByLinkText("VA and Military");
        getByLinkText("VA and Military").click();
        return this;
    }

    public extTuitionFinancialAidPage clickApplyingForFinancialAid() {
        getByLinkText("Applying for Financial Aid");
        getByLinkText("Applying for Financial Aid").click();
        return this;
    }

    public extTuitionFinancialAidPage clickMoreAboutFinancialAid() {
        getByLinkText("More about Financial Aid");
        getByLinkText("More about Financial Aid").click();
        return this;
    }

    public extTuitionFinancialAidPage clickYourFinancialObligations() {
        getByLinkText("Your Financial Obligations");
        getByLinkText("Your Financial Obligations").click();
        return this;
    }

    public extTuitionFinancialAidPage clickConsumerInformationGuide() { // ERROR this does not exist in sidebar
        clickFinancialAid();
//        getByXpath("").click();
        return this;
    }

    public extTuitionFinancialAidPage clickScholarships() {
        getByLinkText("Scholarships");
        getByLinkText("Scholarships").click();
        return this;
    }

    public extTuitionFinancialAidPage clickTuitionAssistance() {
        getByLinkText("Tuition Assistance");
        getByLinkText("Tuition Assistance").click();
        return this;
    }

    public extTuitionFinancialAidPage clickPartTimePrograms() {
        getByLinkText("Part-Time Programs for Military");
        getByLinkText("Part-Time Programs for Military").click();
        return this;
    }

    public extTuitionFinancialAidPage clickPartTimeTuition() {
        getByLinkText("Part-Time Tuition");
        getByLinkText("Part-Time Tuition").click();
        return this;
    }

    public extTuitionFinancialAidPage clickCorpReimbursement() {
        get(loginType.getStartingUrl() + "/tuition_financial_aid/corporate");
        return this;
    }

    public extTuitionFinancialAidPage ExercisePage(boolean cascade) {
        General.Debug("\nextTuitionFinancialAidPage::ExercisePage(" + cascade + ")");
        load();

        clickApplyingForFinancialAid();
        clickConsumerInformationGuide();
        clickCorpReimbursement();
        clickFinancialPolicy();
        clickMoreAboutFinancialAid();
        clickPartTimePrograms();
        clickPartTimeTuition();
        clickScholarships();
        clickVAandMilitary();
        clickTuitionAndFees();
        clickTuitionAssistance();
        clickYourFinancialObligations();

        return this;
    }
}

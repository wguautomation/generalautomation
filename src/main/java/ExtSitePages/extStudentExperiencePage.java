
package ExtSitePages;

import BaseClasses.WebPage;
import Utils.General;

/**
 * Created by timothy.hallbeck on 1/24/2015.
 */
public class extStudentExperiencePage extends extWguExperiencePage {
    public extStudentExperiencePage() {
        this(null);
    }

    public extStudentExperiencePage(WebPage existingPage) {
        super(existingPage);
    }

    public extStudentExperiencePage load() {
        super.load();
        clickStudentExperience();
        return this;
    }

    public extStudentExperiencePage clickHowYoullComplete() {
        clickStudentExperience();
        getByXpath("//*[@id=\"are-you-left\"]/div[1]/div[3]/div/a/img").click();
        getByXpath("//*[@id=\"closeBut\"]").click();
        return this;
    }

    public extStudentExperiencePage clickHowOurCompetency() {
        clickStudentExperience();
        getByXpath("//*[@id=\"are-you-left\"]/div[2]/div[3]/div/a/img[1]").click();
        getByXpath("//*[@id=\"closeBut\"]").click();
        return this;
    }

    public extStudentExperiencePage clickYourStudentMentor() {
        clickStudentExperience();
        getByXpath("//*[@id=\"are-you-left\"]/div[3]/div[3]/div/a/img[1]").click();
        getByXpath("//*[@id=\"closeBut\"]").click();
        return this;
    }

    public extStudentExperiencePage clickYourPersonalDegree() {
        clickStudentExperience();
        getByXpath("//*[@id=\"are-you-left\"]/div[4]/div[3]/div/a/img").click();
        getByXpath("//*[@id=\"closeBut\"]").click();
        return this;
    }

    public extStudentExperiencePage clickWhatYoullUse() {
        clickStudentExperience();
        getByXpath("//*[@id=\"are-you-left\"]/div[5]/div[3]/div/a/img[1]").click();
        getByXpath("//*[@id=\"closeBut\"]").click();
        return this;
    }

    public extStudentExperiencePage clickConnectingWithOther() {
        clickStudentExperience();
        getByXpath("//*[@id=\"are-you-left\"]/div[6]/div[3]/div/a/img[1]").click();
        getByXpath("//*[@id=\"closeBut\"]").click();
        return this;
    }

    public extStudentExperiencePage clickTheOpportunityToFinish() {
        clickStudentExperience();
        getByXpath("//*[@id=\"are-you-left\"]/div[7]/div[3]/div/a/img[1]").click();
        getByXpath("//*[@id=\"closeBut\"]").click();
        return this;
    }

    public extStudentExperiencePage clickSupportFromTheUniv() {
        clickStudentExperience();
        getByXpath("//*[@id=\"are-you-left\"]/div[8]/div[3]/div/a/img[1]").click();
        getByXpath("//*[@id=\"closeBut\"]").click();
        return this;
    }

    public extStudentExperiencePage ExercisePage(boolean cascade) {
        General.Debug("\nextStudentExperiencePage::ExercisePage(" + cascade + ")");
        load();

        // this is all removed
/*
        clickConnectingWithOther();
        clickHowYoullComplete();
        clickHowOurCompetency();
        clickSupportFromTheUniv();
        clickTheOpportunityToFinish();
        clickWhatYoullUse();
        clickYourStudentMentor();
        clickYourPersonalDegree();
*/
        return this;
    }
}


package ExtSitePages;

import BaseClasses.WebPage;
import Utils.General;

/**
 * Created by timothy.hallbeck on 1/23/2015.
 */
public class extWhyWguPage extends extHomePage {
    public extWhyWguPage() {
        this(null);
    }

    public extWhyWguPage(WebPage existingPage) {
        super(existingPage);
    }

    public extWhyWguPage load() {
        super.load();
        clickAboutWgu(); // Why WGU does not have a top level link, just a sidebar
        clickWhyWguSidebar();
        return this;
    }

    public extWhyWguPage clickWhyWguSidebar() {
        getByXpath("//*[@id=\"dhtml_menu-308\"]");
        getByXpath("//*[@id=\"dhtml_menu-308\"]").click();
        return this;
    }

    public extWhyWguPage clickOurCommitments() {
        clickWhyWguSidebar();
        getByXpath("//*[@id=\"dhtml_menu-581\"]");
        getByXpath("//*[@id=\"dhtml_menu-581\"]").click();
        return this;
    }

    public extWhyWguPage clickCompetencyBased() {
        clickWhyWguSidebar();
        getByXpath("//*[@id=\"dhtml_menu-586\"]");
        getByXpath("//*[@id=\"dhtml_menu-586\"]").click();
        return this;
    }

    public extWhyWguPage clickWhoMakesAGood() {
        clickWhyWguSidebar();
        getByXpath("//*[@id=\"dhtml_menu-582\"]");
        getByXpath("//*[@id=\"dhtml_menu-582\"]").click();
        return this;
    }

    public extWhyWguPage clickOurAccreditation() {
        clickWhyWguSidebar();
        getByXpath("//*[@id=\"dhtml_menu-587\"]");
        getByXpath("//*[@id=\"dhtml_menu-587\"]").click();
        return this;
    }

    public extWhyWguPage clickHowWeCompare() {
        clickWhyWguSidebar();
        getByXpath("//*[@id=\"dhtml_menu-583\"]");
        getByXpath("//*[@id=\"dhtml_menu-583\"]").click();
        return this;
    }

    public extWhyWguPage clickWhatEmployersSay() {
        clickWhyWguSidebar();
        getByXpath("//*[@id=\"dhtml_menu-584\"]");
        getByXpath("//*[@id=\"dhtml_menu-584\"]").click();
        return this;
    }

    public extWhyWguPage clickSupportedBy() {
        clickWhyWguSidebar();
        getByXpath("//*[@id=\"dhtml_menu-585\"]");
        getByXpath("//*[@id=\"dhtml_menu-585\"]").click();
        return this;
    }

    public extWhyWguPage clickWguIsANonprofit() {
        clickWhyWguSidebar();
        getByXpath("//*[@id=\"dhtml_menu-1538\"]");
        getByXpath("//*[@id=\"dhtml_menu-1538\"]").click();
        return this;
    }

    public extWhyWguPage clickTestimonials() {
        clickWhyWguSidebar();
        getByXpath("//*[@id=\"dhtml_menu-3076\"]");
        getByXpath("//*[@id=\"dhtml_menu-3076\"]").click();
        return this;
    }

    public extWhyWguPage clickSmarterWayToLearn() { // ERROR - item not present in sidebar
        clickWhyWguSidebar();
//        getByXpath("").click();
        return this;
    }

    public extWhyWguPage ExercisePage(boolean cascade) {
        General.Debug("\nextWhyWguPage::ExercisePage(" + cascade + ")");
        load();

        clickCompetencyBased();
        clickHowWeCompare();
        clickOurCommitments();
        clickOurAccreditation();
        clickSmarterWayToLearn();
        clickSupportedBy();
        clickTestimonials();
        clickWguIsANonprofit();
        clickWhoMakesAGood();
        clickWhatEmployersSay();
        return this;
    }
}

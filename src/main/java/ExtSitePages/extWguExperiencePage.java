
package ExtSitePages;

import BaseClasses.WebPage;
import Utils.General;

/**
 * Created by timothy.hallbeck on 1/22/2015.
 */
public class extWguExperiencePage extends extHomePage {
    public extWguExperiencePage() {
        this(null);
    }

    public extWguExperiencePage(WebPage existingPage) {
        super(existingPage);
    }

    public extWguExperiencePage load() {
        super.load();
        clickWguExperience();
        return this;
    }

    public extWguExperiencePage clickAcademicExperience() {
        getByLinkText("Academic Experience");
        getByLinkText("Academic Experience").click();
        return this;
    }

    public extWguExperiencePage clickYourMentor() {
        getByLinkText("Your Mentor");
        getByLinkText("Your Mentor").click();
        return this;
    }

    public extWguExperiencePage clickDegreePlan() {
        getByLinkText("Degree Plan");
        getByLinkText("Degree Plan").click();
        return this;
    }

    public extWguExperiencePage clickCompletingAssessments() {
        getByLinkText("Completing Assessments");
        getByLinkText("Completing Assessments").click();
        return this;
    }

    public extWguExperiencePage clickUsingLearningResources() {
        getByLinkText("Using Learning Resources");
        getByLinkText("Using Learning Resources").click();
        return this;
    }

    public extWguExperiencePage clickStudentCommunities() {
        getByLinkText("Student Communities");
        getByLinkText("Student Communities").click();
        return this;
    }

    public extWguExperiencePage clickGettingStarted() {
        getByLinkText("Getting Started");
        getByLinkText("Getting Started").click();
        return this;
    }

    public extWguExperiencePage clickStudentExperience() {
        getByLinkText("Student Experience");
        getByLinkText("Student Experience").click();
        return this;
    }

    public extWguExperiencePage clickGraduationAndBeyond() {
        getByLinkText("Graduation and Beyond");
        getByLinkText("Graduation and Beyond").click();
        return this;
    }

    public extWguExperiencePage ExercisePage(boolean cascade) {
        General.Debug("\nextWguExperiencePage::ExercisePage(" + cascade + ")");
        load();

        if (cascade) {
            new extGettingStartedPage(this).ExercisePage(true);
            new extGraduationAndBeyondPage(this).ExercisePage(true);
            new extStudentExperiencePage(this).ExercisePage(true);
//            return this;
        }

        clickAcademicExperience();
        clickYourMentor();
        clickDegreePlan();
        clickCompletingAssessments();
        clickUsingLearningResources();
        clickStudentCommunities();
        clickGettingStarted();
        clickStudentExperience();
        clickGraduationAndBeyond();

        return this;
    }
}

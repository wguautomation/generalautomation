
package ExtSitePages;

import BaseClasses.WebPage;
import Utils.General;

/**
 * Created by timothy.hallbeck on 1/24/2015.
 */
public class extGettingStartedPage extends extWguExperiencePage {
    public extGettingStartedPage() {
        this(null);
    }

    public extGettingStartedPage(WebPage existingPage) {
        super(existingPage);
    }

    public extGettingStartedPage load() {
        super.load();
        clickGettingStarted();
        return this;
    }

    public extGettingStartedPage clickSpeakingWithYourEnrollment() {
        clickGettingStarted();
        getByXpath("//*[@id=\"are-you-left\"]/div[1]/div[2]/div[2]/a/img[1]").click();
        getByXpath("//*[@id=\"closeBut\"]").click();
        return this;
    }

    public extGettingStartedPage clickStartingYourDegree() {
        clickGettingStarted();
        getByXpath("//*[@id=\"are-you-left\"]/div[2]/div[2]/div[2]/a/img").click();
        getByXpath("//*[@id=\"closeBut\"]").click();
        return this;
    }

    public extGettingStartedPage clickPayingForYourEducation() {
        clickGettingStarted();
        getByXpath("//*[@id=\"are-you-left\"]/div[3]/div[2]/div/a/img[1]").click();
        getByXpath("//*[@id=\"closeBut\"]").click();
        return this;
    }

    public extGettingStartedPage clickTransferringCredits() {
        clickGettingStarted();
        getByXpath("//*[@id=\"are-you-left\"]/div[4]/div[2]/div/a/img[1]").click();
        getByXpath("//*[@id=\"closeBut\"]").click();
        return this;
    }

    public extGettingStartedPage ExercisePage(boolean cascade) {
        General.Debug("\nextGettingStartedPage::ExercisePage(" + cascade + ")");
        load();

        if (cascade) {
//            new extSpeakingWithYourEnrollmentPage(this).ExercisePage(true);
//            new extStartingYourDegreePage(this).ExercisePage(true);
//            new extPayingForYourEducationPage(this).ExercisePage(true);
//            new extTransferringCreditsPage(this).ExercisePage(true);
            return this;
        }

        clickPayingForYourEducation();
        clickSpeakingWithYourEnrollment();
        clickStartingYourDegree();
        clickTransferringCredits();
        return this;
    }
}

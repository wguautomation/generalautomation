
package ExtSitePages;

import BaseClasses.WebPage;
import Utils.General;

/**
 * Created by timothy.hallbeck on 1/22/2015.
 */
public class extAdmissionsPage extends extHomePage {
    public extAdmissionsPage() {
        this(null);
    }

    public extAdmissionsPage(WebPage existingPage) {
        super(existingPage);
    }

    public extAdmissionsPage load() {
        super.load();
        clickAdmissions();
        return this;
    }

    public extAdmissionsPage clickAdmissionsSidebar() {
        getByXpath("//*[@id=\"dhtml_menu-312\"]");
        getByXpath("//*[@id=\"dhtml_menu-312\"]").click();
        return this;
    }

    public extAdmissionsPage clickAcademicExperience() {
        clickAdmissionsSidebar();
        getByXpath("//*[@id=\"dhtml_menu-589\"]");
        getByXpath("//*[@id=\"dhtml_menu-589\"]").click();
        return this;
    }

    public extAdmissionsPage clickYourMentor() {
        clickAcademicExperience();
        getByXpath("//*[@id=\"dhtml_menu-592\"]");
        getByXpath("//*[@id=\"dhtml_menu-592\"]").click();
        return this;
    }

    public extAdmissionsPage clickDegreePlan() {
        clickAcademicExperience();
        getByXpath("//*[@id=\"dhtml_menu-590\"]");
        getByXpath("//*[@id=\"dhtml_menu-590\"]").click();
        return this;
    }

    public extAdmissionsPage clickCompletingAssessments() {
        clickAcademicExperience();
        getByXpath("//*[@id=\"dhtml_menu-588\"]");
        getByXpath("//*[@id=\"dhtml_menu-588\"]").click();
        return this;
    }

    public extAdmissionsPage clickCoursesOfStudy() {
        clickCompletingAssessments();
        getByXpath("//*[@id=\"dhtml_menu-1602\"]");
        getByXpath("//*[@id=\"dhtml_menu-1602\"]").click();
        return this;
    }

    public extAdmissionsPage clickUsingLearningResources() {
        clickAcademicExperience();
        getByXpath("//*[@id=\"dhtml_menu-591\"]");
        getByXpath("//*[@id=\"dhtml_menu-591\"]").click();
        return this;
    }

    public extAdmissionsPage clickStudentCommunities() {
        clickAcademicExperience();
        getByXpath("//*[@id=\"dhtml_menu-593\"]");
        getByXpath("//*[@id=\"dhtml_menu-593\"]").click();
        return this;
    }

    public extAdmissionsPage clickAdmissionsRequirements() {
        clickAdmissionsSidebar();
        getByXpath("//*[@id=\"dhtml_menu-338\"]");
        getByXpath("//*[@id=\"dhtml_menu-338\"]").click();
        return this;
    }

    public extAdmissionsPage clickTeachersCollegeAdmissionsReq() {
        clickAdmissionsRequirements();
        getByXpath("//*[@id=\"dhtml_menu-594\"]");
        getByXpath("//*[@id=\"dhtml_menu-594\"]").click();
        return this;
    }

    public extAdmissionsPage clickCollegeOfBusinessReq() {
        clickAdmissionsRequirements();
        getByXpath("//*[@id=\"dhtml_menu-597\"]");
        getByXpath("//*[@id=\"dhtml_menu-597\"]").click();
        return this;
    }

    public extAdmissionsPage clickCollegeOfITAdmissionsReq() {
        clickAdmissionsRequirements();
        getByXpath("//*[@id=\"dhtml_menu-595\"]");
        getByXpath("//*[@id=\"dhtml_menu-595\"]").click();
        return this;
    }

    public extAdmissionsPage clickCollegeOfHealthAdmissionsReq() {
        clickAdmissionsRequirements();
        getByXpath("//*[@id=\"dhtml_menu-596\"]");
        getByXpath("//*[@id=\"dhtml_menu-596\"]").click();
        return this;
    }

    public extAdmissionsPage clickApplyForAdmission() {
        clickAdmissionsSidebar();
        getByXpath("//*[@id=\"dhtml_menu-5281\"]");
        getByXpath("//*[@id=\"dhtml_menu-5281\"]").click();
        return this;
    }

    public extAdmissionsPage clickEnrollmentChecklist() {
        clickAdmissionsSidebar();
        getByXpath("//*[@id=\"dhtml_menu-337\"]");
        getByXpath("//*[@id=\"dhtml_menu-337\"]").click();
        return this;
    }

    public extAdmissionsPage clickTransferring() {
        clickAdmissionsSidebar();
        getByXpath("//*[@id=\"dhtml_menu-333\"]");
        getByXpath("//*[@id=\"dhtml_menu-333\"]").click();
        return this;
    }

    public extAdmissionsPage clickTransferringFAQ() {
        clickTransferring();
        getByXpath("//*[@id=\"dhtml_menu-334\"]");
        getByXpath("//*[@id=\"dhtml_menu-334\"]").click();
        return this;
    }

    public extAdmissionsPage clickTransferringCommunityCollege() {
        clickTransferring();
        getByXpath("//*[@id=\"dhtml_menu-336\"]");
        getByXpath("//*[@id=\"dhtml_menu-336\"]").click();
        return this;
    }

    public extAdmissionsPage clickTransferringTranscripts() {
        clickTransferring();
        getByXpath("//*[@id=\"dhtml_menu-335\"]");
        getByXpath("//*[@id=\"dhtml_menu-335\"]").click();
        return this;
    }

    public extAdmissionsPage ExercisePage(boolean cascade) {
        General.Debug("\nextAdmissionsPage::ExercisePage(" + cascade + ")");
        load();

        if (cascade) {
//            return this;
        }

        clickAcademicExperience();
        clickYourMentor();
        clickDegreePlan();
        clickCompletingAssessments();
        clickCoursesOfStudy();
        clickUsingLearningResources();
        clickStudentCommunities();

        clickTeachersCollegeAdmissionsReq();
        clickCollegeOfBusinessReq();
        clickCollegeOfITAdmissionsReq();
        clickCollegeOfHealthAdmissionsReq();
        clickApplyForAdmission();

        load();
        clickEnrollmentChecklist();
        clickTransferringFAQ();
        clickTransferringCommunityCollege();
        clickTransferringTranscripts();
        return this;
    }
}

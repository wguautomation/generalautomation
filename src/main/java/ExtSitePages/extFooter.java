
package ExtSitePages;

/*
 * Created by timothy.hallbeck on 1/24/2015.
 */

import BaseClasses.WebPage;
import Utils.General;

public class extFooter extends extHomePage {
    public extFooter() {
        this(null);
    }

    public extFooter(WebPage existingPage) {
        super(existingPage);
    }

    public extFooter load() {
        super.load();
        return this;
    }

    public extFooter clickAdmissions() {
        clickIfExists("//*[@id='LPMcloseButton-1441396418220-2']");
        scrollIntoView("//*[@id='admissions']/a");
        getByXpath("//*[@id='admissions']/a").click();
        getByXpath("//*[@id='node-100']/div[1]/h2");

        return this;
    }

    public extFooter clickTuitionAndFinancialAid() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[1]/ul[1]/li[2]/a").click();
        getByXpath("//*[@id=\"node-49\"]/div[1]/h2[1]");
        return this;
    }

    public extFooter clickAboutWgu() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[1]/ul[1]/li[3]/a").click();
        getByXpath("//*[@id=\"node-39\"]/div[1]/h2");
        return this;
    }

    public extFooter clickAcademicExperience() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[1]/ul[1]/li[4]/a").click();
        getByXpath("//*[@id=\"container-information\"]/div[2]/h1");
        return this;
    }

    public extFooter clickEmploymentAtWgu() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[1]/ul[2]/li[1]/a").click();
        getByXpath("//*[@id=\"node-135\"]/div[1]/h2[1]");
        return this;
    }

    public extFooter clickNewsroom() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[1]/ul[2]/li[2]/a").click();
        getByXpath("//*[@id=\"node-3\"]/div[1]/h2");
        return this;
    }

    public extFooter clickStudentPortal() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[1]/ul[2]/li[3]/a").click();
        getByXpath("/html/body/div/div[1]/div/div[1]/p/span");
        return this;
    }

    public extFooter clickTeachersCollege() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[2]/div[1]/h3[1]/a").click();
        getByXpath("//*[@id=\"node-10586\"]/div[1]/h3[1]");
        return this;
    }

    public extFooter clickTeacherLicensure() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[2]/div[1]/ul[1]/li[1]/a").click();
        getByXpath("//*[@id=\"node-11337\"]/div[1]/div[1]/h3");
        return this;
    }

    public extFooter clickGraduatePrograms() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[2]/div[1]/ul[1]/li[2]/a").click();
        getByXpath("//*[@id=\"node-10251\"]/div[1]/h3[1]");
        return this;
    }

    public extFooter clickCollegeOfBusiness() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[2]/div[1]/h3[2]/a").click();
        getByXpath("//*[@id=\"node-10222\"]/div[1]/h3[1]");
        return this;
    }

    public extFooter clickBusinessBachelors() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[2]/div[1]/ul[2]/li[1]/a").click();
        getByXpath("//*[@id=\"node-10242\"]/div[1]/h3[1]");
        return this;
    }

    public extFooter clickBusinessMasters() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[2]/div[1]/ul[2]/li[2]/a").click();
        getByXpath("//*[@id=\"node-10247\"]/div[1]/h3[1]");
        return this;
    }

    public extFooter clickWguIndiana() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[2]/div[4]/ul/li[1]/a").click();
        return this;
    }

    public extFooter clickWguMissouri() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[2]/div[4]/ul/li[2]/a").click();
        return this;
    }

    public extFooter clickWguTennessee() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[2]/div[4]/ul/li[3]/a").click();
        return this;
    }

    public extFooter clickWguTexas() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[2]/div[5]/ul/li[1]/a").click();
        return this;
    }

    public extFooter clickWguWashington() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[2]/div[5]/ul/li[2]/a").click();
        return this;
    }

    public extFooter clickCollegeOfIt() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[2]/div[2]/h3[1]/a").click();
        getByXpath("//*[@id=\"node-7152\"]/div[1]/p[1]");
        return this;
    }

    public extFooter clickITBachelors() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[2]/div[2]/ul[1]/li[1]/a").click();
        getByXpath("//*[@id=\"node-5301\"]/div[1]/h3[1]");
        return this;
    }

    public extFooter clickITMasters() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[2]/div[2]/ul[1]/li[2]/a").click();
        getByXpath("//*[@id=\"node-5297\"]/div[1]/h3[1]");
        return this;
    }

    public extFooter clickCollegeOfHealth() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[2]/div[2]/h3[2]/a").click();
        getByXpath("//*[@id=\"node-5767\"]/div[1]/h3[1]");
        return this;
    }

    public extFooter clickHealthcareBachelors() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[2]/div[2]/ul[2]/li[1]/a").click();
        getByXpath("//*[@id=\"node-5276\"]/div[1]/h3");
        return this;
    }

    public extFooter clickHealthcareMasters() {
        getByXpath("//*[@id=\"footer_site_links\"]/div[2]/div[2]/ul[2]/li[2]/a").click();
        getByXpath("//*[@id=\"node-5287\"]/div[1]/h3[1]");
        return this;
    }

    public extFooter clickPrivacyPolicy() {
        getByXpath("//*[@id=\"block-block-157\"]/div/div[2]/div/p[1]/a[1]").click();
        getByXpath("//*[@id=\"container-information\"]/div[2]/h1");
        return this;
    }

    public extFooter clickSiteMap() {
        getByXpath("//*[@id=\"block-block-157\"]/div/div[2]/div/p[1]/a[2]").click();
        return this;
    }

    public extFooter clickContactUs() {
        getByXpath("//*[@id=\"block-block-157\"]/div/div[2]/div/p[1]/a[3]").click();
        return this;
    }

    public extFooter ExercisePage(boolean cascade) {
        General.Debug("\nextFooter::ExercisePage(" + cascade + ")");
        load();

        if (cascade) {
//            new myAdmissionsPage(this).ExercisePage(true);
//            new myAcademicExperiencePage(this).ExercisePage(true);
//            new myBusinessBachelorsPage(this).ExercisePage(true);
//            new myBusinessMastersPage(this).ExercisePage(true);
//            new myCollegeOfBusinessPage(this).ExercisePage(true);
//            new myCollegeOfItPage(this).ExercisePage(true);
//            new myEmploymentPage(this).ExercisePage(true);
//            new myGraduateProgramsPage(this).ExercisePage(true);
//            new myHealthcareBachelorsPage(this).ExercisePage(true);
//            new myHealthcareMastersPage(this).ExercisePage(true);
//            new myITBachelorsPage(this).ExercisePage(true);
//            new myITMastersPage(this).ExercisePage(true);
//            new myNewsroomPage(this).ExercisePage(true);
//            new myTuitionAndFinancialAidPage(this).ExercisePage(true);
//            new myWguIndianaPage(this).ExercisePage(true);
//            new myWguMissouriPage(this).ExercisePage(true);
//            new myWguTennesseePage(this).ExercisePage(true);
//            new myWguTexasPage(this).ExercisePage(true);
//            new myWguWashingtonPage(this).ExercisePage(true);
//            return this;
        }

        clickAdmissions();
        clickTuitionAndFinancialAid();
        clickAboutWgu();
        clickAcademicExperience();
        clickEmploymentAtWgu();
        clickNewsroom();
        clickStudentPortal();
        load();

        clickTeachersCollege();
        clickTeacherLicensure();
        clickGraduatePrograms();
        clickCollegeOfBusiness();
        clickBusinessBachelors();
        clickBusinessMasters();

        clickCollegeOfIt();
        clickITBachelors();
        clickITMasters();
        clickCollegeOfHealth();
        clickHealthcareBachelors();
        clickHealthcareMasters();

        clickWguIndiana();
        load();
        clickWguMissouri();
        load();
        clickWguTennessee();
        load();
        clickWguTexas();
        load();
        clickWguWashington();
        load();

        clickPrivacyPolicy();
        clickSiteMap();
        clickContactUs();
        return this;
    }

}

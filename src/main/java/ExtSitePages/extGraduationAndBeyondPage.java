
package ExtSitePages;

import BaseClasses.WebPage;
import Utils.General;

/**
 * Created by timothy.hallbeck on 1/24/2015.
 */
public class extGraduationAndBeyondPage extends extWguExperiencePage {
    public extGraduationAndBeyondPage() {
        this(null);
    }

    public extGraduationAndBeyondPage(WebPage existingPage) {
        super(existingPage);
    }

    public extGraduationAndBeyondPage load() {
        super.load();
        clickWguExperience();
        clickGraduationAndBeyond();
        return this;
    }

    public extGraduationAndBeyondPage clickGraduationCeremony() {
        clickGraduationAndBeyond();
        getByXpath("//*[@id=\"are-you-left\"]/div[1]/div[3]/div/a/img").click();
        Sleep(2000);
        getByXpath("//*[@id=\"closeBut\"]").click();
        return this;
    }

    public extGraduationAndBeyondPage clickWguAlumniAssoc() {
        clickGraduationAndBeyond();
        getByXpath("//*[@id=\"are-you-left\"]/div[2]/div[3]/div/a/img").click();
        Sleep(2000);
        getByXpath("//*[@id=\"closeBut\"]").click();
        return this;
    }

    public extGraduationAndBeyondPage clickCareerAndProf() {
        clickGraduationAndBeyond();
        getByXpath("//*[@id=\"are-you-left\"]/div[3]/div[3]/div/a/img").click();
        Sleep(2000);
        getByXpath("//*[@id=\"closeBut\"]").click();
        return this;
    }

    public extGraduationAndBeyondPage clickWhatEmployersSay() {
        clickGraduationAndBeyond();
        getByXpath("//*[@id=\"are-you-left\"]/div[4]/div[3]/div/a/img").click();
        Sleep(2000);
        getByXpath("//*[@id=\"closeBut\"]").click();
        return this;
    }

    public extGraduationAndBeyondPage ExercisePage(boolean cascade) {
        General.Debug("\nextGraduationAndBeyondPage::ExercisePage(" + cascade + ")");
        load();

        if (cascade) {
            return this;
        }

        clickCareerAndProf();
        clickGraduationCeremony();
        clickWguAlumniAssoc();
        clickWhatEmployersSay();
        return this;
    }

}


package ExtSitePages;

/*
 * Created by timothy.hallbeck on 1/25/2015.
 */

import Utils.General;

public class extRequestInfoPage extends extHomePage {
    public extRequestInfoPage() {
        super();
    }

    public extRequestInfoPage load() {
        super.load();
        clickRequestInfo();
        return this;
    }

    public extRequestInfoPage typeFirstName(String text) {
        typeByXpath("//*[@id='edit-first-name']", text);
        return this;
    }

    public extRequestInfoPage typeLastName(String text) {
        typeByXpath("//*[@id='edit-last-name']", text);
        return this;
    }

    public extRequestInfoPage typeEmailAddress(String text) {
        typeByXpath("//*[@id='edit-email-1']", text);
        return this;
    }

    public extRequestInfoPage typeRepeatEmail(String text) {
        typeByXpath("//*[@id='edit-email-2']", text);
        return this;
    }

    public extRequestInfoPage typeMailingAddress1(String text) {
        typeByXpath("//*[@id='edit-address-1']", text);
        return this;
    }

    public extRequestInfoPage typeMailingAddress2(String text) {
        typeByXpath("//*[@id='edit-address-2']", text);
        return this;
    }

    public extRequestInfoPage typeCity(String text) {
        typeByXpath("//*[@id='edit-city']", text);
        return this;
    }

    public extRequestInfoPage selectState(String value) {
        selectByXpathByValue("//*[@id='edit-state']", value);
        return this;
    }

    public extRequestInfoPage typeZip(String text) {
        typeByXpath("//*[@id='edit-zip']", text);
        return this;
    }

    public extRequestInfoPage selectPrimaryPhone(String value) {
        selectByXpathByValue("//*[@id='edit-phone-type']", value);
        return this;
    }

    public extRequestInfoPage typePhone(String text1, String text2, String text3) {
        typeByXpath("//*[@id='edit-phone-a']", text1);
        typeByXpath("//*[@id='edit-phone-b']", text2);
        typeByXpath("//*[@id='edit-phone-c']", text3);
        return this;
    }

    public extRequestInfoPage selectCountry(String value) {
        selectByXpathByValue("//*[@id='edit-country']", value);
        return this;
    }

    public extRequestInfoPage selectProgram(String value) {
        selectByXpathByValue("//*[@id='edit-program-interest']", value);
        return this;
    }

    public extRequestInfoPage selectHSYear(String text) {
        selectByXpathByText("//*[@id='edit-ws-req-a0ma0000001xZfFAAU']", text);
        return this;
    }

    public extRequestInfoPage selectEnroll(String text) {
        selectByXpathByText("//*[@id='edit-ws-req-a0ma0000001xZfOAAU']", text);
        return this;
    }

    public extRequestInfoPage selectTimePerWeek(String text) {
        selectByXpathByText("//*[@id='edit-ws-req-a0ma0000001xZfKAAU']", text);
        return this;
    }

    public extRequestInfoPage selectRateYourself(String text) {
        selectByXpathByText("//*[@id='edit-ws-req-a0ma0000001xZfOAAU']", text);
        return this;
    }

    public extRequestInfoPage ExercisePage(boolean cascade) {
        General.Debug("\nextRequestInfoPage::ExercisePage(" + cascade + ")");
        load();

        typeFirstName("John");
        typeLastName("Smith");
        typeEmailAddress("johnsmith@aol.com");
        typeRepeatEmail("johnsmith@aol.com");
        typeMailingAddress1("123 Anystreet");
        typeMailingAddress2("");
        typeCity("Chicago");
        selectState("IL");
        typeZip("60609");
        selectCountry("US");
        selectPrimaryPhone("Mobile");
        typePhone("800", "555", "4444");
        selectProgram("MBA");
        selectHSYear("2000");
        selectEnroll("Immediately");
        selectTimePerWeek("");
        selectRateYourself("");
        return this;
    }

}















package ExtSitePages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;
import org.openqa.selenium.WebElement;

/*
 * Created by timothy.hallbeck on 1/19/2015.
 */

public class extHomePage extends WebPage {
    public extHomePage() {
        this(null);
    }
    public extHomePage(WebPage existingPage) {
        this(existingPage, LoginType.EXTERNAL_SITE);
    }
    public extHomePage(WebPage existingPage, LoginType loginType) {
        this(existingPage, LoginType.EXTERNAL_SITE, Browser.Firefox);
    }
    public extHomePage(WebPage existingPage, LoginType loginType, Browser browser) {
        super(existingPage, LoginType.EXTERNAL_SITE, browser);
    }

    public extHomePage load() {
        super.load();
        getByXpath(extXpath.HomePage_FooterLoaded);
        wguPowerPointFileUtils.saveScreenshot(this, "extHomePage");

        return this;
    }

    public extHomePage clickAboutWgu() {
        get(loginType.getStartingUrl() + extXpath.ExtUrl_AboutWGU);
        Sleep(1000);
        getByXpath(extXpath.AboutWGUPage_Loaded);
        wguPowerPointFileUtils.saveScreenshot(this, "clickAboutWgu");

        return this;
    }

    public extHomePage clickAdmissions() {
        get(loginType.getStartingUrl() + "/admissions/requirements");
        Sleep(1000);
        wguPowerPointFileUtils.saveScreenshot(this, "clickAdmissions");

        return this;
    }

    public extHomePage clickDegreesAndPrograms() {
        get(loginType.getStartingUrl() + "/degrees_and_programs");
        Sleep(1000);

        return this;
    }

    public extHomePage clickTuitionAndFinancialAid() {
        get(loginType.getStartingUrl() + "/tuition_financial_aid/overview");
        Sleep(1000);

        return this;
    }

    public extHomePage clickWguExperience() {
        getByXpath("//*[@id='main-menu']/div/div/ul/li[5]/a").click();
        Sleep(1000);

        return this;
    }

    public extHomePage clickRequestInfo() {
        get("https://wwwforms.wgu.edu/wgu/inquiry_form");
        Sleep(1000);

        return this;
    }

    public extHomePage clickApplyNow() {
        get("https://wwwforms.wgu.edu/wgu/app/app_step0");
        Sleep(1000);

        return this;
    }

    public extHomePage clickTeachersCollege() {
        get(loginType.getStartingUrl() + "/education/online_teaching_degree");
        Sleep(1000);

        return this;
    }

    public extHomePage clickCollegeOfBusiness() {
        get(loginType.getStartingUrl() + "/business/online_business_degree");
        Sleep(1000);

        return this;
    }

    public extHomePage clickCollegeOfIt() {
        get(loginType.getStartingUrl() + "/online_it_degrees/programs");
        Sleep(1000);

        return this;
    }

    public extHomePage clickCollegeOfHealth() {
        get(loginType.getStartingUrl() + "/online_health_professions_degrees/online_healthcare_degree");
        Sleep(1000);

        return this;
    }

    public extHomePage verifyNewsAndEvents() {
        WebElement newsElement;
        Integer    i = 1;
        String     newsXpath;

        do {
            newsXpath = "//*[@id='news']/p[" + i.toString() + "]/a";
            newsElement = getByXpath(newsXpath, true);
            if (newsElement != null) {
                newsElement.click();
                getByXpath("//*[@id='container-information']/div[2]/h1", true);
                clickBrowserBackButton();
            }
        } while (newsElement != null && i++ < 20);
        return this;
    }

    public extHomePage ExercisePage(boolean cascade) {
        General.Debug("\nextHomePage::ExercisePage(" + cascade + ")");
        load();

        if (cascade) {
            new extFooter(this).ExercisePage(true);
            new extAboutWguPage(this).ExercisePage(true);
            new extAdmissionsPage(this).ExercisePage(true);
            new extWguExperiencePage(this).ExercisePage(true);
            new extDegreesAndProgramsPage(this).ExercisePage(true);
            new extTuitionFinancialAidPage(this).ExercisePage(true);
//            new extWhyWguPage(this).ExercisePage(true);
        }

        verifyNewsAndEvents();

        clickDegreesAndPrograms();
        clickAdmissions();
        clickTuitionAndFinancialAid();
        clickAboutWgu();
        clickWguExperience();

        clickRequestInfo();
        clickBrowserBackButton();
        clickApplyNow();
        load();

        clickTeachersCollege();
        clickBrowserBackButton();

        clickCollegeOfBusiness();
        clickBrowserBackButton();

        clickCollegeOfIt();
        clickBrowserBackButton();

        clickCollegeOfHealth();
        clickBrowserBackButton();

        return this;
    }
}

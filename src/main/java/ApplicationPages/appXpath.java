
package ApplicationPages;

/*
 * Created by timothy.hallbeck on 8/14/2015.
 */

public class appXpath {
    public static String BasicInfoPage_Address1            = "//*[@id='edit-address-1']";
    public static String BasicInfoPage_Address1Label       = "//*[@id='edit-address-1-wrapper']/label";
    public static String BasicInfoPage_Address2            = "//*[@id='edit-address-2']";
    public static String BasicInfoPage_Address2Label       = "//*[@id='edit-address-2-wrapper']/label";
    public static String BasicInfoPage_City                = "//*[@id='edit-city']";
    public static String BasicInfoPage_CityLabel           = "//*[@id='edit-city-wrapper']/label";
    public static String BasicInfoPage_CollegeInterest     = "//*[@id='edit-college-interest']";
    public static String BasicInfoPage_CollegeInterestLabel= "//*[@id='edit-college-interest-wrapper']/label";
    public static String BasicInfoPage_Country             = "//*[@id='edit-country']";
    public static String BasicInfoPage_CountryLabel        = "//*[@id='edit-country-wrapper']/label";
    public static String BasicInfoPage_Email               = "//*[@id='edit-email-1']";
    public static String BasicInfoPage_EmailLabel          = "//*[@id='edit-email-1-wrapper']/label";
    public static String BasicInfoPage_FirstName           = "//*[@id='edit-first-name']";
    public static String BasicInfoPage_FirstNameLabel      = "//*[@id='edit-first-name-wrapper']/label";
    public static String BasicInfoPage_LastName            = "//*[@id='edit-last-name']";
    public static String BasicInfoPage_LastNameLabel       = "//*[@id='edit-last-name-wrapper']/label";
    public static String BasicInfoPage_MaidenName          = "//*[@id='edit-maiden-name']";
    public static String BasicInfoPage_MaidenLabel         = "//*[@id='edit-maiden-name-wrapper']/label";
    public static String BasicInfoPage_MajorInterest       = "//*[@id='edit-major-interest']";
    public static String BasicInfoPage_MajorInterestLabel  = "//*[@id='edit-major-interest-wrapper']/label";
    public static String BasicInfoPage_MiddleInitial       = "//*[@id='edit-middle-initial']";
    public static String BasicInfoPage_MiddleInitialLabel  = "//*[@id='edit-middle-initial-wrapper']/label";
    public static String BasicInfoPage_Nickname            = "//*[@id='edit-nickname']";
    public static String BasicInfoPage_NicknameLabel       = "//*[@id='edit-nickname-wrapper']/label";
    public static String BasicInfoPage_Password1           = "//*[@id='edit-password-1']";
    public static String BasicInfoPage_Password1Label      = "//*[@id='edit-password-1-wrapper']/label";
    public static String BasicInfoPage_Password2           = "//*[@id='edit-password-2']";
    public static String BasicInfoPage_Password2Label      = "//*[@id='edit-password-2-wrapper']/label";
    public static String BasicInfoPage_PhoneType           = "//*[@id='edit-phone-type']";
    public static String BasicInfoPage_PhoneTypeLabel      = "//*[@id='edit-phone-type-wrapper']/label";
    public static String BasicInfoPage_PhoneLabel          = "//*[@id='main']/div/div/div[13]/fieldset/legend";
    public static String BasicInfoPage_Phone1              = "//*[@id='edit-phone-a']";
    public static String BasicInfoPage_Phone2              = "//*[@id='edit-phone-b']";
    public static String BasicInfoPage_Phone3              = "//*[@id='edit-phone-c']";
    public static String BasicInfoPage_Prefix              = "//*[@id='edit-prefix']";
    public static String BasicInfoPage_PrefixLabel         = "//*[@id='edit-prefix-wrapper']/label";
    public static String BasicInfoPage_StartingUrl         = "https://stg.wwwforms.wgu.edu/wgu/app/app_step0";
    public static String BasicInfoPage_State               = "//*[@id='edit-state']";
    public static String BasicInfoPage_StateLabel          = "//*[@id='edit-state-wrapper']/label";
    public static String BasicInfoPage_Suffix              = "//*[@id='edit-suffix']";
    public static String BasicInfoPage_SuffixLabel         = "//*[@id='edit-suffix-wrapper']/label";
    public static String BasicInfoPage_Zip                 = "//*[@id='edit-zip']";
    public static String BasicInfoPage_ZipLabel            = "//*[@id='edit-zip-wrapper']/label";

    public static String Button_BasicInfo                  = "//*[@id='edit-tab-1']";
    public static String Button_MoreInfo                   = "//*[@id='edit-tab-2']";
    public static String Button_Education                  = "//*[@id='edit-tab-3']";
    public static String Button_Demographics               = "//*[@id='edit-tab-4']";
    public static String Button_Review                     = "//*[@id='edit-tab-5']";
    public static String Button_PayAndFinish               = "//*[@id='edit-tab-6']";
    public static String Button_Next                       = "//*[@id='edit-submit']";

    public static String DemographicsPage_Sources          = "//*[@id='edit-reach-market-source-wrapper']/input";
    public static String DemographicsPage_SourcesLabel     = "//*[@id='edit-reach-market-source-wrapper']/label/span";

    public static String DemographicsPage_Employment       = "//*[@id='employment_id']";
    public static String DemographicsPage_EmploymentLabel  = "//*[@id='edit-employment-wrapper']/label";

    public static String DemographicsPage_KnowSomeoneNo    = "//*[@id='edit-reach-market-student-No']";
    public static String DemographicsPage_KnowSomeoneYes   = "//*[@id='edit-reach-market-student-Yes']";
    public static String DemographicsPage_KnowSomeoneLabel = "//*[@id='main']/div/div/div[2]/div/fieldset/legend/span";

    public static String EducationPage_AddInstitution      = "//*[@id='edit-submit-institution']";
    public static String EducationPage_AttendanceFromMonth = "//*[@id='edit-dates-from-m']";
    public static String EducationPage_AttendanceFromYear  = "//*[@id='edit-dates-from-y']";
    public static String EducationPage_AttendanceToMonth   = "//*[@id='edit-dates-to-m']";
    public static String EducationPage_AttendanceToYear    = "//*[@id='edit-dates-to-y']";
    public static String EducationPage_Degree              = "//*[@id='edit-degree']";
    public static String EducationPage_DegreeLabel         = "//*[@id='edit-degree-wrapper']/label";
    public static String EducationPage_Institution         = "//*[@id='edit-iped-code']";
    public static String EducationPage_InstitutionLabel    = "//*[@id='edit-iped-code-wrapper']/label";
    public static String EducationPage_SSN                 = "//*[@id='edit-ssn']";
    public static String EducationPage_SSNLabel            = "//*[@id='edit-ssn-wrapper']/label";
    public static String EducationPage_TaxID               = "//*[@id='edit-itn']";
    public static String EducationPage_TaxIDLabel          = "//*[@id='edit-itn-wrapper']/label";

    public static String MoreInfoPage_DateOfBirth          = "//*[@id='edit-dob-date']";
    public static String MoreInfoPage_Male                 = "//*[@id='edit-gender-M']";
    public static String MoreInfoPage_Female               = "//*[@id='edit-gender-F']";
    public static String MoreInfoPage_HighSchoolYes        = "//*[@id='edit-hs-or-ged-Y']";
    public static String MoreInfoPage_HighSchoolNo         = "//*[@id='edit-hs-or-ged-N']";
    public static String MoreInfoPage_EnglishYes           = "//*[@id='edit-en-native-lang-Y']";
    public static String MoreInfoPage_EnglishNo            = "//*[@id='edit-en-native-lang-N']";

    public static String ReviewPage_DemographicsText       = "//*[@id='main']/div/div/h3[4]";

    public static String PayAndFinishPage_SpecialRadio     = "//*[@id='edit-payment-method-discount-wrapper']/label";
    public static String PayAndFinishPage_AffCodeEdit      = "//*[@id='edit-coupon-code']";
    public static String PayAndFinishPage_AffCodeApply     = "//*[@id='edit-check-coupon-code']";
    public static String PayAndFinishPage_AffCodeAccepted  = "//*[@id='promo_message_check']";
    public static String PayAndFinishPage_Finish           = "//*[@id='edit-submit']";

}

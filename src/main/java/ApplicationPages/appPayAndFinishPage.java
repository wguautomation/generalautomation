
package ApplicationPages;

import BaseClasses.Page;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;

/*
 * Created by timothy.hallbeck on 8/17/2015.
 */

public class appPayAndFinishPage extends appBasicInfoPage {
    public appPayAndFinishPage() {
    }
    public appPayAndFinishPage(WebPage existingPage) {
        super(existingPage);
    }

    public appPayAndFinishPage load() {
        if (driver.getCurrentUrl().equalsIgnoreCase("https://stg.wwwforms.wgu.edu/wgu/application/step6"))
            return this;

        clickPayAndFinish();
        getByXpath(appXpath.PayAndFinishPage_Finish);
        wguPowerPointFileUtils.saveScreenshot(this, "appPayAndFinishPage");

        return this;
    }

    public appPayAndFinishPage verifyBasicInfo() {
        wguPowerPointFileUtils.saveScreenshot(this, "appPayAndFinishPage verifyBasicInfo()");

        return this;
    }

    public appPayAndFinishPage ExercisePage(boolean cascade) {
        General.Debug("\nappPayAndFinishPage::ExercisePage(" + cascade + ")");
        load();

        getByXpath(appXpath.PayAndFinishPage_SpecialRadio).click();
        Page.Sleep(500);
        typeByXpath(appXpath.PayAndFinishPage_AffCodeEdit, "WGUTXT");
        getByXpath(appXpath.PayAndFinishPage_AffCodeApply).click();
        Page.Sleep(1000);
        getByXpath(appXpath.PayAndFinishPage_AffCodeAccepted);
        getByXpath(appXpath.PayAndFinishPage_Finish).click();
        Page.Sleep(4000);

        wguPowerPointFileUtils.saveScreenshot(this, "appPayAndFinishPage after ExercisePage()");

        return this;
    }

}




package ApplicationPages;

import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;

/*
 * Created by timothy.hallbeck on 8/17/2015.
 */

public class appReviewPage extends appBasicInfoPage {
    public appReviewPage() {
    }
    public appReviewPage(WebPage existingPage) {
        super(existingPage);
    }

    public appReviewPage load() {
        if (driver.getCurrentUrl().equalsIgnoreCase("https://stg.wwwforms.wgu.edu/wgu/application/step5"))
            return this;

        clickReview();
        getByXpath(appXpath.ReviewPage_DemographicsText);
        wguPowerPointFileUtils.saveScreenshot(this, "appReviewPage");

        return this;
    }

    public appReviewPage verifyBasicInfo() {
        wguPowerPointFileUtils.saveScreenshot(this, "appReviewPage verifyBasicInfo()");

        return this;
    }

    public appReviewPage ExercisePage(boolean cascade) {
        General.Debug("\nappReviewPage::ExercisePage(" + cascade + ")");
        load();

        wguPowerPointFileUtils.saveScreenshot(this, "appReviewPage after ExercisePage()");
        if (cascade) {
            clickNext();
            appPayAndFinishPage page = new appPayAndFinishPage(this);
            page.ExercisePage(true);
        }

        return this;
    }

}



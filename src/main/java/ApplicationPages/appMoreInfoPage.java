
package ApplicationPages;

import BaseClasses.Page;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;

/*
 * Created by timothy.hallbeck on 8/14/2015.
 */

public class appMoreInfoPage extends appBasicInfoPage {
    public appMoreInfoPage() {
    }
    public appMoreInfoPage(WebPage existingPage) {
        super(existingPage);
    }

    public appMoreInfoPage load() {
        if (driver.getCurrentUrl().equalsIgnoreCase("https://stg.wwwforms.wgu.edu/wgu/application/step2"))
            return this;

        clickMoreInfo();
        getByXpath(appXpath.MoreInfoPage_HighSchoolYes);
        wguPowerPointFileUtils.saveScreenshot(this, "appMoreInfoPage");

        return this;
    }

    public appMoreInfoPage enterDateOfBirth(String mmddyyyy) {
        typeByXpath(appXpath.MoreInfoPage_DateOfBirth, mmddyyyy);
        wguPowerPointFileUtils.addBulletPoint("enterDateOfBirth " + mmddyyyy);

        return this;
    }

    public appMoreInfoPage clickGenderMale() {
        getByXpath(appXpath.MoreInfoPage_Male).click();
        wguPowerPointFileUtils.addBulletPoint("clickGenderMale");

        return this;
    }
    public appMoreInfoPage clickGenderFemale() {
        getByXpath(appXpath.MoreInfoPage_Female).click();
        wguPowerPointFileUtils.addBulletPoint("clickGenderFemale");

        return this;
    }

    public appMoreInfoPage clickHighSchoolYes() {
        getByXpath(appXpath.MoreInfoPage_HighSchoolYes).click();
        wguPowerPointFileUtils.addBulletPoint("clickHighSchoolYes");

        return this;
    }
    public appMoreInfoPage clickHighSchoolNo() {
        getByXpath(appXpath.MoreInfoPage_HighSchoolNo).click();
        wguPowerPointFileUtils.addBulletPoint("clickHighSchoolNo");

        return this;
    }

    public appMoreInfoPage clickEnglishYes() {
        getByXpath(appXpath.MoreInfoPage_EnglishYes).click();
        wguPowerPointFileUtils.addBulletPoint("clickHighEnglishYes");

        return this;
    }
    public appMoreInfoPage clickEnglishNo() {
        getByXpath(appXpath.MoreInfoPage_EnglishNo).click();
        wguPowerPointFileUtils.addBulletPoint("clickEnglishNo");

        return this;
    }

    public appMoreInfoPage verifyBasicInfo() {
        wguPowerPointFileUtils.saveScreenshot(this, "appMoreInfoPage verifyBasicInfo()");

        General.assertDebug(getByXpath(appXpath.MoreInfoPage_DateOfBirth) != null, "MoreInfoPage_DateOfBirth");
        General.assertDebug(getByXpath(appXpath.MoreInfoPage_Male) != null, "MoreInfoPage_Male");
        General.assertDebug(getByXpath(appXpath.MoreInfoPage_Female) != null, "MoreInfoPage_Female");

        return this;
    }

    public appMoreInfoPage ExercisePage(boolean cascade) {
        General.Debug("\nappMoreInfoPage::ExercisePage(" + cascade + ")");
        load();

        enterDateOfBirth("01/08/1935");
        clickGenderMale();
        clickHighSchoolYes();
        clickEnglishYes();

        wguPowerPointFileUtils.saveScreenshot(this, "appMoreInfoPage after ExercisePage()");
        if (cascade) {
            clickNext();
            Page.Sleep(5000);
            appEducationPage page = new appEducationPage(this);
            page.ExercisePage(true);
        }

        return this;
    }

}

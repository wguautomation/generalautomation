
package ApplicationPages;

import BaseClasses.Page;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;

/*
 * Created by timothy.hallbeck on 8/17/2015.
 */

public class appEducationPage extends appBasicInfoPage {
    public appEducationPage() {
    }
    public appEducationPage(WebPage existingPage) {
        super(existingPage);
    }

    public appEducationPage load() {
        if (driver.getCurrentUrl().equalsIgnoreCase("https://stg.wwwforms.wgu.edu/wgu/application/step3"))
            return this;

        clickEducation();
        getByXpath(appXpath.EducationPage_Institution);
        wguPowerPointFileUtils.saveScreenshot(this, "appEducationPage");

        return this;
    }

    public appEducationPage enterSSN(String text) {
        typeByXpath(appXpath.EducationPage_SSN, text);
        wguPowerPointFileUtils.addBulletPoint("enterSSN " + text);

        return this;
    }

    public appEducationPage selectInstitution(String text) {
        selectByXpathByValue(appXpath.EducationPage_Institution, text);
        wguPowerPointFileUtils.addBulletPoint("selectInstitution " + text);

        return this;
    }

    public appEducationPage selectFromMonth(String text) {
        selectByXpathByValue(appXpath.EducationPage_AttendanceFromMonth, text);
        wguPowerPointFileUtils.addBulletPoint("selectFromMonth " + text);

        return this;
    }

    public appEducationPage selectFromYear(String text) {
        selectByXpathByValue(appXpath.EducationPage_AttendanceFromYear, text);
        wguPowerPointFileUtils.addBulletPoint("selectFromYear " + text);

        return this;
    }

    public appEducationPage selectToMonth(String text) {
        selectByXpathByValue(appXpath.EducationPage_AttendanceToMonth, text);
        wguPowerPointFileUtils.addBulletPoint("selectToMonth " + text);

        return this;
    }

    public appEducationPage selectToYear(String text) {
        selectByXpathByValue(appXpath.EducationPage_AttendanceToYear, text);
        wguPowerPointFileUtils.addBulletPoint("selectToYear " + text);

        return this;
    }

    public appEducationPage selectDegree(String text) {
        selectByXpathByValue(appXpath.EducationPage_Degree, text);
        wguPowerPointFileUtils.addBulletPoint("selectDegree " + text);

        return this;
    }

    public appEducationPage clickAddInstitution() {
        getByXpath(appXpath.EducationPage_AddInstitution).click();
        Page.Sleep(2000);
        wguPowerPointFileUtils.addBulletPoint("clickAddInstitution");

        return this;
    }

    public appEducationPage verifyBasicInfo() {
        wguPowerPointFileUtils.saveScreenshot(this, "appEducationPage verifyBasicInfo()");

        General.assertDebug(getByXpath(appXpath.EducationPage_AddInstitution)      != null, "EducationPage_AddInstitution");
        General.assertDebug(getByXpath(appXpath.EducationPage_AttendanceFromMonth) != null, "EducationPage_AttendanceFromMonth");
        General.assertDebug(getByXpath(appXpath.EducationPage_AttendanceFromYear)  != null, "EducationPage_AttendanceFromYear");
        General.assertDebug(getByXpath(appXpath.EducationPage_AttendanceToMonth)   != null, "EducationPage_AttendanceToMonth");
        General.assertDebug(getByXpath(appXpath.EducationPage_AttendanceToYear)    != null, "EducationPage_AttendanceToYear");
        General.assertDebug(getByXpath(appXpath.EducationPage_Degree)              != null, "EducationPage_Degree");
        General.assertDebug(getByXpath(appXpath.EducationPage_DegreeLabel)         != null, "EducationPage_DegreeLabel");
        General.assertDebug(getByXpath(appXpath.EducationPage_Institution)         != null, "EducationPage_Institution");
        General.assertDebug(getByXpath(appXpath.EducationPage_InstitutionLabel)    != null, "EducationPage_InstitutionLabel");
        General.assertDebug(getByXpath(appXpath.EducationPage_SSN)                 != null, "EducationPage_SSN");
        General.assertDebug(getByXpath(appXpath.EducationPage_SSNLabel)            != null, "EducationPage_SSNLabel");
        General.assertDebug(getByXpath(appXpath.EducationPage_TaxID)               != null, "EducationPage_TaxID");
        General.assertDebug(getByXpath(appXpath.EducationPage_TaxIDLabel)          != null, "EducationPage_TaxIDLabel");

        return this;
    }

    public appEducationPage ExercisePage(boolean cascade) {
        General.Debug("\nappEducationPage::ExercisePage(" + cascade + ")");
        load();

        enterSSN("111223333");
        selectInstitution("455363-001a000001ABYoPAAX");

        scrollDown(1);
        selectFromMonth("1");
        selectFromYear("2014");
        selectToMonth("12");
        selectToYear("2015");
        selectDegree("No Degree Earned");
        clickAddInstitution();
        Page.Sleep(5000);

        wguPowerPointFileUtils.saveScreenshot(this, "appEducationPage after ExercisePage()");
        if (cascade) {
            clickNext();
            appDemographicsPage page = new appDemographicsPage(this);
            page.ExercisePage(true);
        }

        return this;
    }

}



package ApplicationPages;

import BaseClasses.Page;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;

/*
 * Created by timothy.hallbeck on 8/17/2015.
 */

public class appDemographicsPage extends appBasicInfoPage {
    public appDemographicsPage() {
    }
    public appDemographicsPage(WebPage existingPage) {
        super(existingPage);
    }

    public appDemographicsPage load() {
        if (driver.getCurrentUrl().equalsIgnoreCase("https://stg.wwwforms.wgu.edu/wgu/application/step4"))
            return this;

        clickDemographics();
        getByXpath(appXpath.DemographicsPage_Employment);
        wguPowerPointFileUtils.saveScreenshot(this, "appDemographicsPage");

        return this;
    }

    public appDemographicsPage selectSource() {
        getByXpath(appXpath.DemographicsPage_Sources).click();
        Page.Sleep(1000);
        getByXpath("//*[@id='edit-reach-market-source-wrapper']/div[1]/label[2]/input").click();
        getByXpath(appXpath.DemographicsPage_Sources).click();
        wguPowerPointFileUtils.addBulletPoint("selectSource()");

        return this;
    }

    public appDemographicsPage clickKnowSomeoneNo() {
        getByXpath(appXpath.DemographicsPage_KnowSomeoneNo).click();
        wguPowerPointFileUtils.addBulletPoint("clickKnowSomeoneNo");

        return this;
    }
    public appDemographicsPage clickKnowSomeoneYes() {
        getByXpath(appXpath.DemographicsPage_KnowSomeoneYes).click();
        wguPowerPointFileUtils.addBulletPoint("clickKnowSomeoneYes");

        return this;
    }

    public appDemographicsPage selectEmployment(String text) {
        selectByXpathByValue(appXpath.DemographicsPage_Employment, text);
        wguPowerPointFileUtils.addBulletPoint("selectEmployment()" + text);

        return this;
    }

    public appDemographicsPage verifyBasicInfo() {
        wguPowerPointFileUtils.saveScreenshot(this, "appDemographicsPage verifyBasicInfo()");

//        General.assertDebug(getByXpath(appXpath.MoreInfoPage_DateOfBirth) != null, "MoreInfoPage_DateOfBirth");

        return this;
    }

    public appDemographicsPage ExercisePage(boolean cascade) {
        General.Debug("\nappDemographicsPage::ExercisePage(" + cascade + ")");
        load();

        selectSource();
        clickKnowSomeoneNo();
        selectEmployment("UE");

        wguPowerPointFileUtils.saveScreenshot(this, "appDemographicsPage after ExercisePage()");
        if (cascade) {
            clickNext();
            appReviewPage page = new appReviewPage(this);
            page.ExercisePage(true);
        }

        return this;
    }

}



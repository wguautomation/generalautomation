
package ApplicationPages;

/*
 * Created by timothy.hallbeck on 8/14/2015.
 */

import BaseClasses.Page;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;

public class appBasicInfoPage extends WebPage {
    public appBasicInfoPage() {
        super();
    }
    public appBasicInfoPage(WebPage existingPage) {
        super(existingPage);
    }

    public appBasicInfoPage load() {
        get(appXpath.BasicInfoPage_StartingUrl);
        wguPowerPointFileUtils.saveScreenshot(this, "appBasicInfoPage");
        wguPowerPointFileUtils.addBulletPoint("load()");

        return this;
    }

    public appBasicInfoPage clickMoreInfo() {
        getByXpath(appXpath.Button_MoreInfo).click();
        wguPowerPointFileUtils.addBulletPoint("clickMoreInfo");

        return this;
    }

    public appBasicInfoPage clickEducation() {
        getByXpath(appXpath.Button_Education).click();
        wguPowerPointFileUtils.addBulletPoint("clickEducation");

        return this;
    }

    public appBasicInfoPage clickDemographics() {
        getByXpath(appXpath.Button_Demographics).click();
        wguPowerPointFileUtils.addBulletPoint("clickDemographics");

        return this;
    }

    public appBasicInfoPage clickReview() {
        getByXpath(appXpath.Button_Review).click();
        wguPowerPointFileUtils.addBulletPoint("clickReview");

        return this;
    }

    public appBasicInfoPage clickPayAndFinish() {
        getByXpath(appXpath.Button_PayAndFinish).click();
        wguPowerPointFileUtils.addBulletPoint("clickPayAndFinish");

        return this;
    }

    public appBasicInfoPage clickNext() {
        getByXpath(appXpath.Button_Next).click();
        wguPowerPointFileUtils.addBulletPoint("clickNext");

        return this;
    }

    public appBasicInfoPage selectPrefix(String text) {
        selectByXpathByValue(appXpath.BasicInfoPage_Prefix, text);
        wguPowerPointFileUtils.addBulletPoint("selectPrefix " + text);

        return this;
    }

    public appBasicInfoPage enterFirstName(String text) {
        typeByXpath(appXpath.BasicInfoPage_FirstName, text);
        wguPowerPointFileUtils.addBulletPoint("enterFirstName " + text);

        return this;
    }

    public appBasicInfoPage enterMiddleInitial(String text) {
        typeByXpath(appXpath.BasicInfoPage_MiddleInitial, text);
        wguPowerPointFileUtils.addBulletPoint("enterMiddleInitial " + text);

        return this;
    }

    public appBasicInfoPage enterLastName(String text) {
        typeByXpath(appXpath.BasicInfoPage_LastName, text);
        wguPowerPointFileUtils.addBulletPoint("enterLastName " + text);

        return this;
    }

    public appBasicInfoPage selectSuffix(String text) {
        selectByXpathByValue(appXpath.BasicInfoPage_Suffix, text);
        wguPowerPointFileUtils.addBulletPoint("selectSuffix " + text);

        return this;
    }

    public appBasicInfoPage enterMaidenName(String name) {
        typeByXpath(appXpath.BasicInfoPage_MaidenName, name);
        wguPowerPointFileUtils.addBulletPoint("enterMaidenName " + name);

        return this;
    }

    public appBasicInfoPage enterNickname(String text) {
        typeByXpath(appXpath.BasicInfoPage_Nickname, text);
        wguPowerPointFileUtils.addBulletPoint("enterNickname " + text);

        return this;
    }

    public appBasicInfoPage enterAddress1(String text) {
        typeByXpath(appXpath.BasicInfoPage_Address1, text);
        wguPowerPointFileUtils.addBulletPoint("enterAddress1 " + text);

        return this;
    }

    public appBasicInfoPage enterAddress2(String text) {
        typeByXpath(appXpath.BasicInfoPage_Address2, text);
        wguPowerPointFileUtils.addBulletPoint("enterAddress2 " + text);

        return this;
    }

    public appBasicInfoPage enterCity(String text) {
        typeByXpath(appXpath.BasicInfoPage_City, text);
        wguPowerPointFileUtils.addBulletPoint("enterCity " + text);

        return this;
    }

    public appBasicInfoPage selectState(String text) {
        selectByXpathByValue(appXpath.BasicInfoPage_State, text);
        wguPowerPointFileUtils.addBulletPoint("selectState " + text);

        return this;
    }

    public appBasicInfoPage enterZip(String text) {
        typeByXpath(appXpath.BasicInfoPage_Zip, text);
        wguPowerPointFileUtils.addBulletPoint("enterZip " + text);

        return this;
    }

    public appBasicInfoPage selectCountry(String text) {
        selectByXpathByValue(appXpath.BasicInfoPage_Country, text);
        wguPowerPointFileUtils.addBulletPoint("selectCountry " + text);

        return this;
    }

    public appBasicInfoPage selectPhoneType(String text) {
        selectByXpathByValue(appXpath.BasicInfoPage_PhoneType, text);
        wguPowerPointFileUtils.addBulletPoint("selectType " + text);

        return this;
    }

    public appBasicInfoPage enterPhone1(String text) {
        typeByXpath(appXpath.BasicInfoPage_Phone1, text);
        wguPowerPointFileUtils.addBulletPoint("enterPhone1 " + text);

        return this;
    }

    public appBasicInfoPage enterPhone2(String text) {
        typeByXpath(appXpath.BasicInfoPage_Phone2, text);
        wguPowerPointFileUtils.addBulletPoint("enterPhone2 " + text);

        return this;
    }

    public appBasicInfoPage enterPhone3(String text) {
        typeByXpath(appXpath.BasicInfoPage_Phone3, text);
        wguPowerPointFileUtils.addBulletPoint("enterPhone3 " + text);

        return this;
    }

    public appBasicInfoPage enterEmail(String text) {
        typeByXpath(appXpath.BasicInfoPage_Email, text);
        wguPowerPointFileUtils.addBulletPoint("enterEmail " + text);

        return this;
    }

    public appBasicInfoPage selectCollegeInterest(String text) {
        selectByXpathByValue(appXpath.BasicInfoPage_CollegeInterest, text);
        wguPowerPointFileUtils.addBulletPoint("selectCollegeInterest " + text);

        return this;
    }

    public appBasicInfoPage selectMajorInterest(String text) {
        selectByXpathByValue(appXpath.BasicInfoPage_MajorInterest, text);
        wguPowerPointFileUtils.addBulletPoint("selectMajorInterest " + text);

        return this;
    }

    public appBasicInfoPage enterPassword(String text) {
        typeByXpath(appXpath.BasicInfoPage_Password1, text);
        wguPowerPointFileUtils.addBulletPoint("enterPassword " + text);

        return this;
    }

    public appBasicInfoPage enterRepeatPassword(String text) {
        typeByXpath(appXpath.BasicInfoPage_Password2, text);
        wguPowerPointFileUtils.addBulletPoint("enterRepeatPassword " + text);

        return this;
    }

    public appBasicInfoPage verifyBasicInfo() {
        wguPowerPointFileUtils.saveScreenshot(this, "appBasicInfoPage verifyBasicInfo()");

        General.assertDebug(getByXpath(appXpath.BasicInfoPage_PrefixLabel) != null, "BasicInfoPage_PrefixLabel");
        General.assertDebug(getByXpath(appXpath.BasicInfoPage_Prefix) != null, "BasicInfoPage_Prefix");
        General.assertDebug(getByXpath(appXpath.BasicInfoPage_FirstNameLabel) != null, "BasicInfoPage_FirstNameLabel");
        General.assertDebug(getByXpath(appXpath.BasicInfoPage_FirstName) != null, "BasicInfoPage_FirstName");
        General.assertDebug(getByXpath(appXpath.BasicInfoPage_MiddleInitialLabel) != null, "BasicInfoPage_MiddleInitialLabel");
        General.assertDebug(getByXpath(appXpath.BasicInfoPage_MiddleInitial) != null, "BasicInfoPage_MiddleInitial");
        General.assertDebug(getByXpath(appXpath.BasicInfoPage_LastNameLabel) != null, "BasicInfoPage_LastNameLabel");
        General.assertDebug(getByXpath(appXpath.BasicInfoPage_LastName) != null, "BasicInfoPage_LastName");
        General.assertDebug(getByXpath(appXpath.BasicInfoPage_SuffixLabel) != null, "BasicInfoPage_SuffixLabel");
        General.assertDebug(getByXpath(appXpath.BasicInfoPage_Suffix) != null, "BasicInfoPage_Suffix");

        return this;
    }

    public appBasicInfoPage ExercisePage(boolean cascade) {
        General.Debug("\nappBasicInfoPage::ExercisePage(" + cascade + ")");
        load();

        selectPrefix("Mr");
        enterFirstName("Johnny");
        enterMiddleInitial("B");
        enterLastName("Utah");
        selectSuffix("Sr");
        enterMaidenName("McBrain");
        enterNickname("TheDude");

        scrollDown(1);
        enterAddress1("123 East 321 St.");
        enterAddress2("South by West Easterly");
        enterCity("Salt Lake City");
        selectState("UT");
        enterZip("84121");
        selectCountry("US");
        selectPhoneType("Home");
        enterPhone1("801");
        enterPhone2("555");
        enterPhone3("1212");
        enterEmail("johnnyutah20150814_1232@wgu.edu");
        selectCollegeInterest("IT");
        Page.Sleep(1000);

        scrollDown(1);
        selectMajorInterest("BSIT-a0ka000000HizKyAAJ");
        enterPassword("123456");
        enterRepeatPassword("123456");
        wguPowerPointFileUtils.saveScreenshot(this, "appBasicInfoPage after ExercisePage()");

        if (cascade) {
            clickNext();
            Page.Sleep(5000);
            appMoreInfoPage page = new appMoreInfoPage(this);
            page.ExercisePage(true);
        }

        return this;
    }

}

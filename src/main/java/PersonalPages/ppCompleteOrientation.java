
package PersonalPages;

/*
 * Created by timothy.hallbeck on 7/28/2015.
 */

import BaseClasses.WebPage;
import Utils.General;

public class ppCompleteOrientation extends ppLandingPage {

    public ppCompleteOrientation() {
        this(null);
        clickCompleteOrientation();
    }
    public ppCompleteOrientation(WebPage existingPage) {
        super(existingPage);
    }

    public ppCompleteOrientation load() {
        super.load();
        clickCompleteOrientation();

        return this;
    }

    public ppCompleteOrientation ExercisePage(boolean cascade) {
        General.Debug("mySuccessCentersPage::ExercisePage(" + cascade + ")");
        load();
        clickIfExists(ppXpath.Popup_OrientationCloseButton);

        return this;
    }

}


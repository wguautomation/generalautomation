
package PersonalPages;

/*
 * Created by timothy.hallbeck on 7/28/2015.
 */

import BaseClasses.WebPage;
import Utils.General;

public class ppCompleteInterview extends ppLandingPage {

    public ppCompleteInterview() {
        this(null);
        clickCompleteInterview();
    }
    public ppCompleteInterview(WebPage existingPage) {
        super(existingPage);
    }

    public ppCompleteInterview load() {
        super.load();
        clickCompleteInterview();

        return this;
    }

    public ppCompleteInterview ExercisePage(boolean cascade) {
        General.Debug("mySuccessCentersPage::ExercisePage(" + cascade + ")");
        load();
        clickIfExists(ppXpath.Popup_IntakeCloseButton);

        return this;
    }

}


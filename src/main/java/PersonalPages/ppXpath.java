
package PersonalPages;

/*
 * Created by timothy.hallbeck on 7/27/2015.
 */

public class ppXpath {

   public static String Eligibility_TeachersCollege              = "/html/body/div/div/ng-view/div[2]/div/div/div/main/div/ul/li[1]/a/div";
   public static String Eligibility_CollegeOfBusiness           = "/html/body/div/div/ng-view/div[2]/div/div/div/main/div/ul/li[2]/a/div";
   public static String Eligibility_CollegeOfIT                    = "/html/body/div/div/ng-view/div[2]/div/div/div/main/div/ul/li[3]/a/div";
   public static String Eligibility_CollegeOfHealthProf       = "/html/body/div/div/ng-view/div[2]/div/div/div/main/div/ul/li[4]/a/div";

   public static String FinancialAid_AdditionalReqs         = "/html/body/div/div/ng-view/div[2]/div/div/div/main/div/ol/li[2]/div/span";
   public static String FinancialAid_AdditionalReqsClose  ="//*[@id='financialAidSteps']/div[2]/button";
   public static String FinancialAid_CalculateLoan           = "/html/body/div/div/ng-view/div[3]/div/div/div/div[2]/div/div[1]/ul/li[6]/a";
   public static String FinancialAid_CounselorEmail         = "/html/body/div/div/ng-view/div[2]/div/div/div/aside/div[1]/div/div[3]/div[2]/div/div/a";
   public static String FinancialAid_FAFSA                     = "/html/body/div/div/ng-view/div[2]/div/div/div/main/div/ol/li[1]/p[1]/a";
   public static String FinancialAid_FinancialAidPolicies  = "/html/body/div/div/ng-view/div[3]/div/div/div/div[2]/div/div[1]/ul/li[1]/a";
   public static String FinancialAid_NSLDS                     = "/html/body/div/div/ng-view/div[3]/div/div/div/div[2]/div/div[1]/ul/li[5]/a";
   public static String FinancialAid_SpecialAidForms       = "/html/body/div/div/ng-view/div[3]/div/div/div/div[2]/div/div[1]/ul/li[4]/a";
   public static String FinancialAid_ScholarshipSearch      = "/html/body/div/div/ng-view/div[3]/div/div/div/div[2]/div/div[1]/ul/li[7]/a";
   public static String FinancialAid_TuitionAndFees         = "/html/body/div/div/ng-view/div[3]/div/div/div/div[2]/div/div[1]/ul/li[2]/a";
   public static String FinancialAid_TuitionPayment        = "/html/body/div/div/ng-view/div[3]/div/div/div/div[2]/div/div[1]/ul/li[3]/a";
   public static String FinancialAid_WhatYear                 = "/html/body/div/div/ng-view/div[2]/div/div/div/main/div/ol/li[1]/p[2]/span";
   public static String FinancialAid_WhatYearClose          ="//*[@id='financialAidYear']/div[2]/button";

   public static String LandingPage_ApplyForAid              = "/html/body/div/div/ng-view/div[4]/div/div/main/div/div[7]/div/div[2]/a";
   public static String LandingPage_ContactCounselor       = "/html/body/div/div/ng-view/div[4]/div/div/main/div/div[3]/div/div[2]/a";
   public static String LandingPage_CompleteInterview     = "/html/body/div/div/ng-view/div[4]/div/div/main/div/div[6]/div/div[2]/a";
   public static String LandingPage_CompleteOrientation  = "/html/body/div/div/ng-view/div[4]/div/div/main/div/div[9]/div/div[2]/a";
   public static String LandingPage_MeetEligibility           = "/html/body/div/div/ng-view/div[4]/div/div/main/div/div[4]/div/div[2]/a";
   public static String LandingPage_PayApplicationFee    = "/html/body/div/div/ng-view/div[4]/div/div/main/div/div[2]/div/div[2]/a";
   public static String LandingPage_SchedulePayment       = "/html/body/div/div/ng-view/div[4]/div/div/main/div/div[8]/div/div[2]/a";
   public static String LandingPage_SubmitTranscripts     = "/html/body/div/div/ng-view/div[4]/div/div/main/div/div[5]/div/div[2]/a";
   public static String LandingPage_WGULogo                = "/html/body/div/div/header/div/div/div[1]/a/img[2]";

   public static String Popup_IntakeCloseButton                    = "//*[@id='intakeLocked']/div[2]/div/div[2]/button";
   public static String Popup_OrientationCloseButton             = "//*[@id='orientationLocked']/div[2]/div/div[2]/button";
   public static String Popup_SchedulePaymentCloseButton   = "//*[@id='paymentLocked']/div[2]/div/div[2]/button";

   public static String SchedulePayment_Scholarships             = "/html/body/div/div/ng-view/div[2]/div/div/div/main/div/p[2]/a[1]";
   public static String SchedulePayment_FinancialAid            = "/html/body/div/div/ng-view/div[2]/div/div/div/main/div/p[2]/a[2]";
   public static String SchedulePayment_VABenefits               = "/html/body/div/div/ng-view/div[2]/div/div/div/main/div/p[2]/a[3]";
   public static String SchedulePayment_CorpReimbursement = "/html/body/div/div/ng-view/div[2]/div/div/div/main/div/p[2]/a[4]";
   public static String SchedulePayment_PaymentPlans          = "/html/body/div/div/ng-view/div[2]/div/div/div/main/div/p[2]/a[5]";
   public static String SchedulePayment_SchedulePayment     = "/html/body/div/div/ng-view/div[2]/div/div/div/main/div/div[2]/a/span";
   public static String SchedulePayment_ViewDetails              = "/html/body/div/div/ng-view/div[2]/div/div/div/main/div/div[3]/div/div/div/a[2]";
   public static String SchedulePayment_TuitionAndFees        = "/html/body/div/div/ng-view/div[3]/div/div/div/div[2]/div/div[1]/ul/li[1]/a";

}

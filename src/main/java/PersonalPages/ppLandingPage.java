
package PersonalPages;

/*
 * Created by timothy.hallbeck on 7/20/2015.
 */

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

public class ppLandingPage extends WebPage {

    public ppLandingPage() {
        super(null, LoginType.LANE1_PP_DANDE20);
    }
    public ppLandingPage(WebPage existingPage) {
        super(existingPage, LoginType.LANE1_PP_DANDE20);
    }
    public ppLandingPage(LoginType loginType) {
        super(null, loginType);
    }

    public ppLandingPage clickLogo() {
        getByXpath(ppXpath.LandingPage_WGULogo).click();
        Sleep(1000);

        return this;
    }

    public ppLandingPage clickApplyForAid() {
        getByXpath(ppXpath.LandingPage_ApplyForAid).click();
        Sleep(1000);

        return this;
    }

    public ppLandingPage clickCompleteInterview() {
        getByXpath(ppXpath.LandingPage_CompleteInterview).click();
        Sleep(1000);

        return this;
    }

    public ppLandingPage clickCompleteOrientation() {
        getByXpath(ppXpath.LandingPage_CompleteOrientation).click();
        Sleep(1000);

        return this;
    }

    public ppLandingPage clickContactCounselor() {
        getByXpath(ppXpath.LandingPage_ContactCounselor).click();
        Sleep(1000);

        return this;
    }

    public ppLandingPage clickMeetEligibility() {
        getByXpath(ppXpath.LandingPage_MeetEligibility).click();
        Sleep(1000);

        return this;
    }

    public ppLandingPage clickPayApplicationFee() {
        getByXpath(ppXpath.LandingPage_PayApplicationFee).click();
        Sleep(1000);

        return this;
    }

    public ppLandingPage clickSchedulePayment() {
        getByXpath(ppXpath.LandingPage_SchedulePayment).click();
        Sleep(1000);

        return this;
    }

    public ppLandingPage clickSubmitTranscripts() {
        getByXpath(ppXpath.LandingPage_SubmitTranscripts).click();
        Sleep(1000);

        return this;
    }

    public ppLandingPage load() {
        clickLogo();
        Sleep(1000);

        return this;
    }

    static final String [] arrayOfPagesToCheck = {
            ppXpath.LandingPage_ApplyForAid,
            ppXpath.LandingPage_CompleteInterview,
            ppXpath.LandingPage_CompleteOrientation,
            ppXpath.LandingPage_ContactCounselor,
            ppXpath.LandingPage_MeetEligibility,
            ppXpath.LandingPage_PayApplicationFee,
            ppXpath.LandingPage_SchedulePayment,
            ppXpath.LandingPage_SubmitTranscripts,
    };

    public ppLandingPage ExercisePage(boolean cascade) {
        General.Debug("ppLandingPage::ExercisePage(" + cascade + ")");

        if (!cascade) {
            load();
            for (String pageXpath : arrayOfPagesToCheck) {
                if (getByXpath(pageXpath, true) != null) {
                    getByXpath(pageXpath).click();
                    Sleep(3000);

                    if (!clickIfExists(ppXpath.Popup_IntakeCloseButton))
                        clickIfExists(ppXpath.Popup_OrientationCloseButton);
                    clickLogo();
                }
            }
            return this;
        }

        load();
        if (getByXpath(ppXpath.LandingPage_ApplyForAid, true) != null)
            new ppApplyForAid(this).ExercisePage(true);

        load();
        if (getByXpath(ppXpath.LandingPage_CompleteInterview, true) != null)
            new ppCompleteInterview(this).ExercisePage(true);

        load();
        if (getByXpath(ppXpath.LandingPage_CompleteOrientation, true) != null)
            new ppCompleteOrientation(this).ExercisePage(true);

        load();
        if (getByXpath(ppXpath.LandingPage_ContactCounselor, true) != null)
            new ppContactCounselor(this).ExercisePage(true);

        load();
        if (getByXpath(ppXpath.LandingPage_MeetEligibility, true) != null)
            new ppMeetEligibility(this).ExercisePage(true);

        load();
        if (getByXpath(ppXpath.LandingPage_PayApplicationFee, true) != null)
            new ppPayApplicationFee(this).ExercisePage(true);

        load();
        if (getByXpath(ppXpath.LandingPage_SchedulePayment, true) != null)
            new ppSchedulePayment(this).ExercisePage(true);

        load();
        if (getByXpath(ppXpath.LandingPage_SubmitTranscripts, true) != null)
            new ppSubmitTranscripts(this).ExercisePage(true);

        return this;
    }

}
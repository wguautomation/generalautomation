
package PersonalPages;

/*
 * Created by timothy.hallbeck on 7/28/2015.
 */

import BaseClasses.WebPage;
import Utils.General;

public class ppApplyForAid extends ppLandingPage {

    public ppApplyForAid() {
        this(null);
        clickApplyForAid();
    }
    public ppApplyForAid(WebPage existingPage) {
        super(existingPage);
    }

    public ppApplyForAid load() {
        super.load();
        clickApplyForAid();

        return this;
    }

    public ppApplyForAid verifyFSALink() {
        scrollUp(1);
        getByXpath(ppXpath.FinancialAid_FAFSA).click();
        Sleep(1000);
        closeTab();

        return this;
    }

    public ppApplyForAid verifyWhatYear() {
        scrollUp(1);
        getByXpath(ppXpath.FinancialAid_WhatYear).click();
        Sleep(1000);
        getByXpath(ppXpath.FinancialAid_WhatYearClose).click();

        return this;
    }

    public ppApplyForAid verifyAdditionalReqs() {
        scrollUp(1);
        getByXpath(ppXpath.FinancialAid_AdditionalReqs).click();
        Sleep(1000);
        scrollDown(1);
        getByXpath(ppXpath.FinancialAid_AdditionalReqsClose).click();

        return this;
    }

/*    public ppApplyForAid verifyCounselorEmail() {
        getByXpath(ppXpath.FinancialAid_CounselorEmail).click();
        Sleep(1000);
        closeTab();

        return this;
    }
*/
    public ppApplyForAid verifyFinancialAidPolicies() {
        scrollDown(1);
        getByXpath(ppXpath.FinancialAid_FinancialAidPolicies).click();
        Sleep(1000);
        closeTab();

        return this;
    }

    public ppApplyForAid verifyTuitionAndFees() {
        scrollDown(1);
        getByXpath(ppXpath.FinancialAid_TuitionAndFees).click();
        Sleep(1000);
        closeTab();

        return this;
    }

    public ppApplyForAid verifyTuitionPayment() {
        scrollDown(1);
        getByXpath(ppXpath.FinancialAid_TuitionPayment).click();
        Sleep(1000);
        closeTab();

        return this;
    }

    public ppApplyForAid verifySpecialAidForms() {
        scrollDown(1);
        getByXpath(ppXpath.FinancialAid_SpecialAidForms).click();
        Sleep(1000);
        closeTab();

        return this;
    }

    public ppApplyForAid verifyNSLDS() {
        scrollDown(1);
        getByXpath(ppXpath.FinancialAid_NSLDS).click();
        Sleep(1000);
        closeTab();

        return this;
    }

    public ppApplyForAid verifyCalculateLoan() {
        scrollDown(1);
        getByXpath(ppXpath.FinancialAid_CalculateLoan).click();
        Sleep(1000);
        closeTab();

        return this;
    }

    public ppApplyForAid verifyScholarshipSearch() {
        scrollDown(1);
        getByXpath(ppXpath.FinancialAid_ScholarshipSearch).click();
        Sleep(1000);
        closeTab();

        return this;
    }

    public ppApplyForAid ExercisePage(boolean cascade) {
        General.Debug("\nmyWellConnectPagemySuccessCentersPage::ExercisePage(" + cascade + ")");
        load();

        verifyFSALink();
        verifyWhatYear();
        verifyAdditionalReqs();
        verifyFinancialAidPolicies();
        verifyTuitionAndFees();
        verifyTuitionPayment();
        verifySpecialAidForms();
        verifyNSLDS();
        verifyCalculateLoan();
        verifyScholarshipSearch();

        return this;
    }

}


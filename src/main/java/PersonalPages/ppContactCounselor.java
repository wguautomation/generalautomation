
package PersonalPages;

/*
 * Created by timothy.hallbeck on 7/28/2015.
 */

import BaseClasses.WebPage;
import Utils.General;

public class ppContactCounselor extends ppLandingPage {

    public ppContactCounselor() {
        this(null);
        clickContactCounselor();
    }
    public ppContactCounselor(WebPage existingPage) {
        super(existingPage);
    }

    public ppContactCounselor load() {
        super.load();
        clickContactCounselor();

        return this;
    }

    public ppContactCounselor ExercisePage(boolean cascade) {
        General.Debug("ppContactCounselor::ExercisePage(" + cascade + ")");
        load();

        return this;
    }

}



package PersonalPages;

/*
 * Created by timothy.hallbeck on 7/28/2015.
 */

import BaseClasses.WebPage;

public class ppSubmitTranscripts extends ppLandingPage {

    public ppSubmitTranscripts() {
        this(null);
        clickSubmitTranscripts();
    }
    public ppSubmitTranscripts(WebPage existingPage) {
        super(existingPage);
    }

    public ppSubmitTranscripts load() {
        super.load();
        clickSubmitTranscripts();

        return this;
    }

    public ppSubmitTranscripts ExercisePage(boolean cascade) {
        load();

        return this;
    }

}



package PersonalPages;

/*
 * Created by timothy.hallbeck on 7/28/2015.
 */

import BaseClasses.WebPage;

public class ppPayApplicationFee extends ppLandingPage {

    public ppPayApplicationFee() {
        this(null);
        clickPayApplicationFee();
    }
    public ppPayApplicationFee(WebPage existingPage) {
        super(existingPage);
    }

    public ppPayApplicationFee load() {
        super.load();
        clickPayApplicationFee();

        return this;
    }

    public ppPayApplicationFee ExercisePage(boolean cascade) {
        load();

        return this;
    }

}


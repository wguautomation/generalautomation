
package PersonalPages;

/*
 * Created by timothy.hallbeck on 7/28/2015.
 */

import BaseClasses.WebPage;
import Utils.General;

public class ppMeetEligibility extends ppLandingPage {

    public ppMeetEligibility() {
        this(null);
        clickMeetEligibility();
    }
    public ppMeetEligibility(WebPage existingPage) {
        super(existingPage);
    }

    public ppMeetEligibility load() {
        super.load();
        clickMeetEligibility();

        return this;
    }

    public ppMeetEligibility verifyTeachersCollege() {
        getByXpath(ppXpath.Eligibility_TeachersCollege).click();
        Sleep(1000);
        closeTab();

        return this;
    }

    public ppMeetEligibility verifyCollegeOfBusiness() {
        getByXpath(ppXpath.Eligibility_CollegeOfBusiness).click();
        Sleep(1000);
        closeTab();

        return this;
    }

    public ppMeetEligibility verifyCollegeOfIT() {
        getByXpath(ppXpath.Eligibility_CollegeOfIT).click();
        Sleep(1000);
        closeTab();

        return this;
    }

    public ppMeetEligibility verifyCollegeOfHealthProf() {
        getByXpath(ppXpath.Eligibility_CollegeOfHealthProf).click();
        Sleep(1000);
        closeTab();

        return this;
    }

    public ppMeetEligibility ExercisePage(boolean cascade) {
        General.Debug("mySuccessCentersPage::ExercisePage(" + cascade + ")");
        load();

        verifyTeachersCollege();
        verifyCollegeOfBusiness();
        verifyCollegeOfIT();
        verifyCollegeOfHealthProf();

        return this;
    }

}



package PersonalPages;

/*
 * Created by timothy.hallbeck on 7/28/2015.
 */

import BaseClasses.WebPage;

public class ppSchedulePayment extends ppLandingPage {

    public ppSchedulePayment() {
        this(null);
    }
    public ppSchedulePayment(WebPage existingPage) {
        super(existingPage);
    }

    public ppSchedulePayment load() {
        super.load();
        clickSchedulePayment();

        return this;
    }

    public ppSchedulePayment verifyScholarships() {
        getByXpath(ppXpath.SchedulePayment_Scholarships).click();
        Sleep(1000);
        closeTab();

        return this;
    }

    public ppSchedulePayment verifyFinancialAid() {
        getByXpath(ppXpath.SchedulePayment_FinancialAid).click();
        Sleep(1000);
        closeTab();

        return this;
    }

    public ppSchedulePayment verifyVABenefits() {
        getByXpath(ppXpath.SchedulePayment_VABenefits).click();
        Sleep(1000);
        closeTab();

        return this;
    }

    public ppSchedulePayment verifyCorpReimbursement() {
        getByXpath(ppXpath.SchedulePayment_CorpReimbursement).click();
        Sleep(1000);
        closeTab();

        return this;
    }

    public ppSchedulePayment verifyPaymentPlans() {
        getByXpath(ppXpath.SchedulePayment_PaymentPlans).click();
        Sleep(1000);
        closeTab();

        return this;
    }

    public ppSchedulePayment verifySchedulePayment() {
        getByXpath(ppXpath.SchedulePayment_SchedulePayment).click();
        Sleep(1000);
        clickBrowserBackButton();

        return this;
    }

    public ppSchedulePayment verifyViewDetails() {
        getByXpath(ppXpath.SchedulePayment_ViewDetails).click();
        Sleep(1000);

        return this;
    }

    public ppSchedulePayment verifyTuitionAndFees() {
        getByXpath(ppXpath.SchedulePayment_TuitionAndFees).click();
        Sleep(1000);
        closeTab();

        return this;
    }

    public ppSchedulePayment ExercisePage(boolean cascade) {

        load();
        verifyScholarships();

        load();
        verifyFinancialAid();

        load();
        verifyVABenefits();

        load();
        verifyCorpReimbursement();

        load();
        verifyPaymentPlans();

        load();
        verifySchedulePayment();

        load();
        verifyViewDetails();

        load();
        verifyTuitionAndFees();

        return this;
    }

}


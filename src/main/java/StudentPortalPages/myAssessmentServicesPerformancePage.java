
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;

/*
 * Created by timothy.hallbeck on 2/29/2016.
 */

public class myAssessmentServicesPerformancePage extends myStudentSupportPage {

    public myAssessmentServicesPerformancePage() {
        this(null);
    }
    public myAssessmentServicesPerformancePage(WebPage existingPage) {
        super(existingPage);
    }
    public myAssessmentServicesPerformancePage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myAssessmentServicesPerformancePage load() {
        super.load();
        clickAssessmentServicesPerformance();
        wguPowerPointFileUtils.saveScreenshot(this, "myAssessmentServicesObjectivePage");
        wguPowerPointFileUtils.addBulletPoint("load()");

        return this;
    }

    public myAssessmentServicesPerformancePage clickTaskstreamHelp() {
        getByXpath(myXpath.AssessmentServicesPerformancePage_TaskstreamHelp).click();
        getByXpathVerifyTextPresent("", "");

        return this;
    }

    public myAssessmentServicesPerformancePage clickECare() {
        getByXpath(myXpath.AssessmentServicesPerformancePage_ECare).click();
        getByXpathVerifyTextPresent("", "");

        return this;
    }

    public myAssessmentServicesPerformancePage clickTurnItIn() {
        getByXpath(myXpath.AssessmentServicesPerformancePage_TurnItIn).click();
        getByXpathVerifyTextPresent("", "");

        return this;
    }

    public myAssessmentServicesPerformancePage clickCapstoneArchive() {
        getByXpath(myXpath.AssessmentServicesPerformancePage_CapstoneArchive).click();
        getByXpathVerifyTextPresent("", "");

        return this;
    }

    public myAssessmentServicesPerformancePage ExercisePage(boolean cascade) {
        General.Debug("\nmyAssessmentServicesPerformancePage::ExercisePage(" + cascade + ")");
        load();
        clickTaskstreamHelp();
        clickECare();
        clickTurnItIn();
        clickCapstoneArchive();

        return this;
    }

}
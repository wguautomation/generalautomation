
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 2/7/2015.
 */

public class myCommunitiesPage extends myHomePage {
    public myCommunitiesPage() {
        this(null);
    }
    public myCommunitiesPage(WebPage existingPage) {
        super(existingPage);
    }
    public myCommunitiesPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myCommunitiesPage load() {
        super.load();
        clickCommunitiesPage();

        return this;
    }

    public myCommunitiesPage clickCommuAvatar() {
        clickCommunitiesPage();
        getByXpath("//*[@class='jive-avatar']").click();
        return this;
    }

    public myCommunitiesPage clickCommuBrowse() {
        clickCommunitiesPage();
        getByXpath("//*[@id='jiveUserMenu5Link']/span").click();
        return this;
    }

    public myCommunitiesPage clickCommuBrowseDiscussions() {
        clickCommuBrowse();
        getByXpath("//*[@id='jiveUserMenu5']/ul/li[1]/a").click();
        return this;
    }

    public myCommunitiesPage clickCommuBrowseDocuments() {
        clickCommuBrowse();
        getByXpath("//*[@id='jiveUserMenu5']/ul/li[2]/a").click();
        return this;
    }

    public myCommunitiesPage clickCommuBrowsePeople() {
        clickCommuBrowse();
        getByXpath("//*[@id='jiveUserMenu5']/ul/li[3]/a").click();
        return this;
    }

    public myCommunitiesPage clickCommuBrowseTags() {
        clickCommuBrowse();
        getByXpath("//*[@id='jiveUserMenu5']/ul/li[4]/a").click();
        return this;
    }

    public myCommunitiesPage clickCommuGettingStarted() {
        clickCommunitiesPage();
        LoadAndSwitchTabs("//*[@id='jive-bodyhome-sidebarcol']/div[1]/p/a");
        return this;
    }

    public myCommunitiesPage clickCommuHistory() {
        clickCommunitiesPage();
        getByXpath("//*[@id='jiveUserMenu4Link']/span").click();
        return this;
    }

    public myCommunitiesPage doCommuSearch(String text) {
        clickCommunitiesPage();
        getByXpath("//*[@id='jive-query']").sendKeys(text);
        getByXpath("//*[@id='jive-userbar-search']/form/button").click();
        return this;
    }

    public myCommunitiesPage clickCommuYourStuff() {
        clickCommunitiesPage();
        getByXpath("//*[@id='jiveUserMenu3Link']/span").click();
        return this;
    }

    public myCommunitiesPage clickCommuYourStuffProfile() {
        clickCommuYourStuff();
        getByXpath("//*[@id='jiveUserMenu3']/ul/li[1]/a").click();
        return this;
    }

    public myCommunitiesPage clickCommuYourStuffOverview() {
        clickCommuYourStuff();
        getByXpath("//*[@id='jiveUserMenu3']/ul/li[2]/a").click();
        return this;
    }

    public myCommunitiesPage clickCommuYourStuffPreferences() {
        clickCommuYourStuff();
        getByXpath("//*[@id='jiveUserMenu3']/ul/li[3]/a").click();
        return this;
    }

    public myCommunitiesPage clickCommuYourStuffEmail() {
        clickCommuYourStuff();
        getByXpath("//*[@id='jiveUserMenu3']/ul/li[4]/a").click();
        return this;
    }

    public myCommunitiesPage clickCommuStudentSuccess() {
        clickCommunitiesPage();
        getByXpath("//*[@id='jive-community-list-long']/ul/li[1]/p/a").click();
        getByXpath("//*[@id='jive-body-intro-content']/h1");
        return this;
    }

    public myCommunitiesPage clickCommuCenterForWriting() {
        clickCommunitiesPage();
        getByXpath("//*[@id='jive-community-list-long']/ul/li[2]/p/a").click();
        getByXpath("//*[@id='jive-body-intro-content']/h1");
        return this;
    }

    public myCommunitiesPage clickCommuAlumniRelations() {
        clickCommunitiesPage();
        getByXpath("//*[@id='jive-community-list-long']/ul/li[3]/p/a").click();
        getByXpath("//*[@id=\"jive-body-intro-content\"]/h1");
        return this;
    }

    public myCommunitiesPage clickCommuCareerAndProfDev() {
        clickCommunitiesPage();
        getByXpath("//*[@id='jive-community-list-long']/ul/li[4]/p/a").click();
        getByXpath("//*[@id=\"jive-body-intro-content\"]/h1");
        return this;
    }

    public myCommunitiesPage clickCommuInformationTechProg() {
        clickCommunitiesPage();
        getByXpath("//*[@id='jive-community-list-long']/ul/li[5]/p/a").click();
        getByXpath("//*[@id=\"jive-body-intro-content\"]/h1");
        return this;
    }

    public myCommunitiesPage clickCommuEconomicsGlobalBiz() {
        clickCommunitiesPage();
        getByXpath("//*[@id='jive-community-list-long']/ul/li[6]/p/a").click();
        getByXpath("//*[@id=\"jive-body-intro-content\"]/h1");
        return this;
    }

    public myCommunitiesPage clickCommuEnglishCompI() {
        clickCommunitiesPage();
        getByXpath("//*[@id='jive-community-list-long']/ul/li[7]/p/a").click();
        getByXpath("//*[@id=\"jive-body-intro-content\"]/h1");
        return this;
    }

    public myCommunitiesPage clickCommuEnglishCompII() {
        clickCommunitiesPage();
        getByXpath("//*[@id='jive-community-list-long']/ul/li[8]/p/a").click();
        getByXpath("//*[@id=\"jive-body-intro-content\"]/h1");
        return this;
    }

    public myCommunitiesPage clickCommuGeneralEducation() {
        clickCommunitiesPage();
        getByXpath("//*[@id='jive-community-list-long']/ul/li[9]/p/a").click();
        getByXpath("//*[@id=\"jive-body-intro-content\"]/h1");
        return this;
    }

    public myCommunitiesPage clickCommuITFundamentals() {
        clickCommunitiesPage();
        getByXpath("//*[@id='jive-community-list-long']/ul/li[10]/p/a").click();
        getByXpath("//*[@id=\"jive-body-intro-content\"]/h1");
        return this;
    }

    public myCommunitiesPage clickCommuLanguageAndCommEssay() {
        clickCommunitiesPage();
        getByXpath("//*[@id=\"jive-community-list-long\"]/ul/li[11]/p/a").click();
        getByXpath("//*[@id=\"jive-body-intro-content\"]/h1");
        return this;
    }

    public myCommunitiesPage clickCommuLanguageAndCommPresentation() {
        clickCommunitiesPage();
        getByXpath("//*[@id=\"jive-community-list-long\"]/ul/li[12]/p/a").click();
        getByXpath("//*[@id=\"jive-body-intro-content\"]/h1");
        return this;
    }

    public myCommunitiesPage clickCommuProfStudiesDiversity() {
        clickCommunitiesPage();
        getByXpath("//*[@id=\"jive-community-list-long\"]/ul/li[13]/p/a").click();
        getByXpath("//*[@id=\"jive-body-intro-content\"]/h1");
        return this;
    }

    public myCommunitiesPage clickCommuReasoningProblemSolving() {
        clickCommunitiesPage();
        getByXpath("//*[@id=\"jive-community-list-long\"]/ul/li[14]/p/a").click();
        getByXpath("//*[@id=\"jive-body-intro-content\"]/h1");
        return this;
    }

    public myCommunitiesPage clickCommuScienceMathLearning() {
        clickCommunitiesPage();
        getByLinkText("Science-Mathematics Learning Community").click();
        getByXpath("//*[@id=\"jive-body-intro-content\"]/h1");
        return this;
    }

    public myCommunitiesPage clickCommuManagingOrganizations() {
        clickCommunitiesPage();
        getByLinkText("Managing Organizations and Leading People Business Graduate Learning Community").click();
        getByXpath("//*[@id=\"jive-body-intro-content\"]/h1");
        return this;
    }

    public myCommunitiesPage clickCommuManagingHumanCapital() {
        clickCommunitiesPage();
        getByLinkText("Managing Human Capital Business Graduate Learning Community").click();
        getByXpath("//*[@id=\"jive-body-intro-content\"]/h1");
        return this;
    }

    public myCommunitiesPage clickCommuGlobalEconomicsManagers() {
        clickCommunitiesPage();
        getByLinkText("Global Economics for Managers Business Graduate Learning Community").click();
        getByXpath("//*[@id=\"jive-body-intro-content\"]/h1");
        return this;
    }

    public myCommunitiesPage clickCommuManagementCommunication() {
        clickCommunitiesPage();
        getByLinkText("Management Communication Business Graduate Learning Community").click();
        getByXpath("//*[@id=\"jive-body-intro-content\"]/h1");
        return this;
    }

    public myCommunitiesPage clickCommuMsnGraduate() {
        clickCommunitiesPage();
        getByLinkText("MSN Graduate Translational Research Learning Community").click();
        getByXpath("//*[@id=\"jive-body-intro-content\"]/h1");
        return this;
    }

    public myCommunitiesPage clickCommuAllContent() {
        clickCommunitiesPage();
        getByXpath("//*[@id=\"jive-content-hdr-filter-btns\"]/span[1]/a").click();
        getByXpath("//*[@id=\"jive-body-intro-content\"]/h1");
        return this;
    }

    public myCommunitiesPage clickCommuYourView() {
        clickCommunitiesPage();
        getByXpath("//*[@id=\"jive-content-hdr-filter-btns\"]/span[2]/a").click();
        getByXpath("//*[@id=\"jive-body-intro-content\"]/h1");
        return this;
    }

    public myCommunitiesPage clickCommuDiscussions() {
        clickCommunitiesPage();
        getByXpath("//*[@id=\"jive-bodyhome-maincol\"]/div/span[2]/a[1]").click();
        return this;
    }

    public myCommunitiesPage clickCommuDocuments() {
        clickCommunitiesPage();
        getByXpath("//*[@id=\"jive-bodyhome-maincol\"]/div/span[2]/a[2]").click();
        return this;
    }

    public myCommunitiesPage ExercisePage(boolean cascade) {
        General.Debug("\nmyCommunitiesPage::ExercisePage(" + cascade + ")");
        load();
    /*        if (cascade) {
                new myPage(this).ExercisePage(true);
                return;
            }
    */
        // Top line navigation
        clickCommuAvatar();
        clickCommuBrowse();
        clickCommuBrowseDiscussions();
        clickCommuBrowseDocuments();
        clickCommuBrowsePeople();
        clickCommuBrowseTags();
//        clickCommuGettingStarted();
        clickCommuHistory();
        doCommuSearch("question");
        clickCommuYourStuff();
        clickCommuYourStuffProfile();
        clickCommuYourStuffOverview();
        clickCommuYourStuffPreferences();
        clickCommuYourStuffEmail();

        // Communities left sidebar navigation, which as of July 28 2015 is now gone
/*        clickCommuStudentSuccess();
        clickCommuCenterForWriting();
        clickCommuAlumniRelations();
        clickCommuCareerAndProfDev();
        clickCommuInformationTechProg();
*/
//        clickCommuEconomicsGlobalBiz();
/*        clickCommuEnglishCompI();
        clickCommuEnglishCompII();
        clickCommuGeneralEducation();
        clickCommuITFundamentals();
        clickCommuLanguageAndCommEssay();
        clickCommuLanguageAndCommPresentation();
        clickCommuProfStudiesDiversity();
        clickCommuReasoningProblemSolving();
        clickCommuScienceMathLearning();
        clickCommuManagingOrganizations();
        clickCommuManagingHumanCapital();
        clickCommuGlobalEconomicsManagers();
        clickCommuManagementCommunication();
        clickCommuMsnGraduate();
*/
        // whats new right sidebar navigation
        clickCommuAllContent();
        clickCommuYourView();
        clickCommuDiscussions();
        clickCommuDocuments();

        return this;
    }
}






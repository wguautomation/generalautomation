
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import JsonDataClasses.*;
import Utils.General;
import Utils.wguEnvironment;
import Utils.wguPowerPointFileUtils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;

/*
 * Created by timothy.hallbeck on 1/16/2015.
 */

public class myHomePage extends WebPage {

    protected wguCommunities        communities       = null;
    protected wguDegreePlan         degreePlan        = null;
    protected wguScheduleItems      scheduleItems     = null;
    protected wguGmailMessageArray  gmailMessageArray = null;
    protected wguNotificationArray  notificationArray = null;
    protected wguSuccessCenterArray successCenters    = null;

    public myHomePage() {
        this(null, LoginType.PRODUCTION_NORMAL);
    }
    public myHomePage(WebPage existingPage) {
        super(existingPage, existingPage == null ? LoginType.PRODUCTION_NORMAL : existingPage.loginType);
        transferDataItems((myHomePage) existingPage);
    }
    public myHomePage(LoginType loginType) {
        super(null, loginType);
    }
    public myHomePage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
        transferDataItems((myHomePage) existingPage);
    }
    public myHomePage(WebPage existingPage, LoginType loginType, Browser browser) {
        super(existingPage, loginType, browser);
        transferDataItems((myHomePage) existingPage);
    }

    private void transferDataItems(myHomePage existingPage) {
        General.Debug("myHomePage::transferDataItems()");
        if (existingPage == null)
            return;

        communities       = existingPage.communities;
        degreePlan        = existingPage.degreePlan;
        scheduleItems     = existingPage.scheduleItems;
        gmailMessageArray = existingPage.gmailMessageArray;
        notificationArray = existingPage.notificationArray;
        successCenters    = existingPage.successCenters;
    }

    public myHomePage load() {
        General.Debug("myHomePage::load()");

        if (!login()) {
            super.load();
        } else {
            loadDataItems();
        }
        wguPowerPointFileUtils.saveScreenshot(this, "myHomePage::load()");
        showPageLoadTimes("myHomePage::load()");

        return this;
    }

    protected void loadDataItems() {
        General.Debug("myHomePage::loadDataItems()");
        // Curl call gets munged by Java on Mac, does not return data
//        if (General.getOS() != General.OS.Windows) {
//            General.Debug("Curl data checks only available on Windows test system. Bug in java + Mac curl lib.");
//            return;
//        }

        General.getPerson(loginType);
        General.getMentors(loginType);
        General.getDegreePlan(loginType);

//        if (degreePlan == null)
//            degreePlan = new wguDegreePlan(loginType);

        if (successCenters == null)
            successCenters = new wguSuccessCenterArray(loginType);

        if (notificationArray == null)
            notificationArray = new wguNotificationArray(loginType);

        if (gmailMessageArray == null)
            gmailMessageArray = new wguGmailMessageArray(loginType);

        if (communities == null)
            communities = new wguCommunities(loginType);

        if (scheduleItems == null)
            scheduleItems = new wguScheduleItems(loginType);

    }

    protected void checkPersonInfo() {
        General.Debug("myHomePage::checkPersonInfo()");
        if (isJenkins()) {
            General.Debug("checkPersonInfo() does not work in Jenkins due to nature of hover-only.");
            return;
        }

        String displayName = getByXpath("//*[@id='user-menu']").getText();
        General.Debug("Display name = " + displayName);
        String personName =  General.getPerson(loginType).getFirstName() + " " + General.getPerson(loginType).getLastName();
        General.Debug("personName = " + personName);
        assert (displayName.contains(personName));
    }

    protected void checkMentorInfo() {
        General.Debug("myHomePage::checkMentorInfo()");
        switchToFrame(0);
        String mentorName = getByXpath(myXpath.HomePage_MentorName).getText();
        String mentorEmail = getByXpath(myXpath.HomePage_MentorEmail).getText();

        if (General.getMentors(loginType) != null && General.getMentors(loginType).size() != 0) {
            assert (mentorName.contains(General.getMentor(loginType).getFirstName()));
            assert (mentorEmail.contains(General.getMentor(loginType).getEmailAddress()));
        }
        switchToDefaultFrame();
    }

    protected void checkEmailEntries() {
        General.Debug("myHomePage::checkEmailEntries()");
        if (isJenkins()) {
            General.Debug("Email text does not render on headless browser. Skipping email data check");
            return;
        }
        switchToFrame(0);

        List<WebElement> emailList = getListByXpath(myXpath.HomePage_EmailList);
        for (int i=0; i<emailList.size(); i++) {
            String displayText  = emailList.get(i).getText().replaceAll("[❗â♥™¥�—]","");
            String endpointText = (gmailMessageArray.getEmail(i).getFrom() + " " + gmailMessageArray.getEmail(i).getSubject()).replaceAll("[❗â♥™¥�—]","");
//            General.Debug(" displayText: " + displayText);
//            General.Debug("endpointText: " + endpointText);
            assert(displayText.contains(endpointText));
        }
    }

    protected void checkScheduleEntries() {

        General.Debug("myHomePage::checkScheduleEntries()");
        if (isJenkins()) {
            General.Debug("myHomePage::checkScheduleEntries() not available for Jenkins run, live browser only");
            return;
        }
        switchToFrame(0);

        List<WebElement> scheduleList = getListByXpath(myXpath.HomePage_ScheduleList);
        General.Debug("scheduleList size is " + scheduleList.size());
        for (int i=0; i<scheduleList.size(); i++) {
            String displayText  = scheduleList.get(i).getText();
            String endpointText  = scheduleItems.getItem(i).getTitle();
            General.Debug("displayText is " + displayText);
            General.Debug("endpointText is " + endpointText);
            assert(displayText.contains(endpointText));
        }

        switchToDefaultFrame();
    }

    protected void checkCommunityEntries() {
        General.Debug("myHomePage::checkCommunityEntries()");
        if (isJenkins()) {
            General.Debug("  skipping myHomePage::checkCommunityEntries. Community displayed data no longer visible under Jenkins");
            return;
        }
        switchToFrame(0);

        List<WebElement> communityList = getListByXpath(myXpath.HomePage_CommunitiesList);
        for (int i=0; i<communityList.size(); i++) {
            String displayText  = communityList.get(i).getText();
            String endpointText  = communities.getCommunity(i).getName();
            Assert.assertTrue(displayText.contains(endpointText), "Community text mismatch: " + displayText + " != " + endpointText);
        }

        switchToDefaultFrame();
    }

    public myHomePage checkDataItems() {
        General.Debug("myHomePage::checkDataItems()");
        if (!doDataValidation())
            return this;

        checkScheduleEntries();
        checkPersonInfo();
        checkMentorInfo();
        checkEmailEntries();
        checkCommunityEntries();

        return this;
    }

    public myHomePage clickHomePage() {
        General.Debug("myHomePage::clickHomePage()");
        get(loginType.getStartingUrl());
        Sleep(2000);
        wguPowerPointFileUtils.saveScreenshot(this, "clickHomePage");

        return this;
    }

    public myHomePage clickCoursesPage() {
        General.Debug("myHomePage::clickCoursesPage()");
        getByXpath(myXpath.HomePage_CoursesLink).click();
        Sleep(4000);
        showPageLoadTimes("myHomePage::clickCoursesPage");
        wguPowerPointFileUtils.saveScreenshot(this, "clickCoursesPage");

        return this;
    }

    public myHomePage clickDegreePlanPage() {
        General.Debug("myHomePage::clickDegreePlanPage()");
        getByXpath(myXpath.HomePage_DegreePlanLink).click();
        Sleep(4000);
        wguPowerPointFileUtils.saveScreenshot(this, "clickDegreePlanPage");

        return this;
    }

    public myHomePage clickSuccessCentersLink() {
        General.Debug("myHomePage::clickSuccessCentersLink()");
        getByXpath(myXpath.HomePage_SuccessCentersLink).click();
        Sleep(3000);
        getByXpath(myXpath.HomePage_SuccessCentersLink).click();
        Sleep(3000);
        wguPowerPointFileUtils.saveScreenshot(this, "clickSuccessCentersLink");

        return this;
    }

    // the hover menus are a pain in chrome vs firefox, disappearing often
    public myHomePage clickStudentSupportMenu() {
        General.Debug("myHomePage::clickStudentSupportMenu()");
        typeByXpath("//*[@id='student-support']", "" + Keys.ESCAPE);
        getByXpath("//*[@id='student-support']").click();
        Sleep(3500);
        getByXpath("//*[@id='student-support']").click();
        getByXpath("//*[@id='student-support']").click();
        Sleep(1500);

        return this;
    }

    public myHomePage clickStudentSupportLink() {
        General.Debug("myHomePage::clickStudentSupportLink()");
        scrollIntoView(myXpath.HomePage_StudentSupportLink).click();
        wguPowerPointFileUtils.saveScreenshot(this, "clickStudentSupportLink");

        return this;
    }

    public myHomePage clickWguIcon() {
        General.Debug("myHomePage::clickWguIcon()");
        scrollIntoView(myXpath.HomePage_WGUIcon).click();
        Sleep(4000);
        wguPowerPointFileUtils.saveScreenshot(this, "clickWguIcon");

        return this;
    }

    public myHomePage clickNewsPage() {
        General.Debug("myHomePage::clickNewsPage()");
        get(myXpath.HomePage_NewsUrl.replace("PREFIX", loginType.getUrlPrefix()));
        Sleep(4000);
        wguPowerPointFileUtils.saveScreenshot(this, "clickNewsPage");

        return this;
    }

    public myHomePage clickSchedulePage() {
        General.Debug("myHomePage::clickSchedulePage()");
        if (loginType == LoginType.PRODUCTION_NORMAL)
            get(myXpath.HomePage_ScheduleUrlProd);
        else
            get(myXpath.HomePage_ScheduleUrlLane1);
        wguPowerPointFileUtils.saveScreenshot(this, "clickSchedulePage");

        return this;
    }

    public myHomePage clickEmailPage() {
        General.Debug("myHomePage::clickEmailPage()");
        if (loginType.getUrlPrefix().isEmpty())
            get("https://mail.google.com/a/wgu.edu/?tab=cm&account_id=" + loginType.getUsername() + "@wgu.edu");
        else if (loginType.getUrlPrefix().toLowerCase().contains("l1"))
            get("https://mail.google.com/a/wgutest.com/?tab=cm&account_id=" + loginType.getUsername() + "@wgu.edu");
        else
            // No configured for Lane 2 so do nothing
            wguPowerPointFileUtils.addBulletPoint("Attempting to click email on Lane 2, not valid");
        Sleep(4000);
        wguPowerPointFileUtils.saveScreenshot(this, "clickEmailPage");

        return this;
    }

    public myHomePage clickStudentLink() {
        General.Debug("myHomePage::clickStudentLink()");
        if (isJenkins())
            General.Debug("---WARNING--- this widget does not work in Jenkins");

        waitForControl(myXpath.HomePage_WGUIcon);
        getByXpath(myXpath.HomePage_StudentLink).click();

        return this;
    }

    public myHomePage clickStudentInfoLink() {
        General.Debug("myHomePage::clickStudentInfoLink()");
        clickStudentLink();
        getByXpath(myXpath.HomePage_StudentInfoLink).click();
        waitForControl(myXpath.HomePage_WGUIcon);

        return this;
    }

    public myHomePage clickPrivacySettings() {
        General.Debug("myHomePage::clickPrivacySettings()");
        clickStudentLink();
        getByXpath(myXpath.HomePage_PrivacySettings).click();
        waitForControl(myXpath.HomePage_WGUIcon);

        return this;
    }

    public myHomePage clickSocialMedia() {
        General.Debug("myHomePage::clickSocialMedia()");
        clickStudentLink();
        getByXpath(myXpath.HomePage_SocialMedia).click();
        waitForControl(myXpath.HomePage_WGUIcon);

        return this;
    }

    public myHomePage clickSecuritySettings() {
        General.Debug("myHomePage::clickSecuritySettings()");
        if (loginType.getEnv() != wguEnvironment.Prod) {
            General.Debug("myHomePage::clickSecuritySettings() can only be used in Prod. Not connected properly in Dev or Qa");
            return this;
        }

        clickStudentLink();
        getByXpath(myXpath.HomePage_SecuritySettings).click();

        return this;
    }

    public myHomePage clickActionItemsPage() {
        General.Debug("myHomePage::clickActionItemsPage()");
        get(myXpath.HomePage_ActionItemsUrl.replace("PREFIX", loginType.getUrlPrefix()));
        Sleep(4000);
        wguPowerPointFileUtils.saveScreenshot(this, "clickActionItemsPage");

        return this;
    }

    // Open in a new tab
    public myHomePage clickCommunitiesPage() {
        General.Debug("myHomePage::clickCommunitiesPage()");
        get(myXpath.HomePage_CommunitiesUrl.replace("PREFIX", loginType.getUrlPrefix()));
        Sleep(4000);
        wguPowerPointFileUtils.saveScreenshot(this, "clickCommunitiesPage");

        return this;
    }

    public myHomePage ExercisePage(boolean cascade) {
        General.Debug("\nmyHomePage::ExercisePage(" + cascade + ")");
        load();
        checkDataItems();

        if (cascade) {
            new myStudentSupportPage(this).ExercisePage(true);
            new mySuccessCentersPage(this).ExercisePage(true);
            new myStudentInfoPage(this).ExercisePage(true);
            new myCoursesPage(this).ExercisePage(true);
            new myEmailPage(this).ExercisePage(true);
            new mySchedulePage(this).ExercisePage(true);
            new myNewsPage(this).ExercisePage(true);
            new myActionItemsPage(this).ExercisePage(true);
            new myDegreePlanPage(this).ExercisePage(true);

            return this;
        }

        if (!isJenkins()) {
            load();
            clickStudentInfoLink();

            load();
            clickPrivacySettings();

            load();
            clickSocialMedia();

            load();
            clickSecuritySettings();
        }

        load();
        clickSchedulePage();

        load();
        clickActionItemsPage();

        load();
        clickCommunitiesPage();

        load();
        clickCoursesPage();

        load();
        clickDegreePlanPage();

        load();
        clickEmailPage();

        load();
        clickNewsPage();

        load();
        clickSuccessCentersLink();

        load();
        clickStudentSupportLink();

        return this;
    }
}

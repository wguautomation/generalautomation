
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 2/7/2015.
 */

public class myProfessionalStudiesPage extends myLearningResourcesPage {
    public myProfessionalStudiesPage() {
        this(null);
    }
    public myProfessionalStudiesPage(WebPage existingPage) {
        super(existingPage);
    }
    public myProfessionalStudiesPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myProfessionalStudiesPage load() {
        super.load();
        clickProfessionalStudies();
        return this;
    }

    public myProfessionalStudiesPage ExercisePage(boolean cascade) {
        General.Debug("\nmyProfessionalStudiesPage::ExercisePage(" + cascade + ")");
        load();
        return this;
    }
}

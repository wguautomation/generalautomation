
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 2/7/2015.
 */

public class myLearningResourcesPage extends myCoursesPage {
    public myLearningResourcesPage() {
        this(null);
    }
    public myLearningResourcesPage(WebPage existingPage) {
        super(existingPage);
    }
    public myLearningResourcesPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myLearningResourcesPage load() {
        super.load();
        clickLearningResources();

        return this;
    }

    public myLearningResourcesPage clickAlumniRelations() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame(0);
        LoadAndSwitchTabs("//*[@id='form1']/div/div/table/tbody/tr[1]/td/a");
        driver.switchTo().defaultContent();
        return this;
    }

    public myLearningResourcesPage clickCareerAndProfDev() {
        driver.switchTo().frame(0);
        driver.switchTo().frame(1);
        LoadAndSwitchTabs("//*[@id='form1']/div/div/table/tbody/tr[2]/td/a");
        driver.switchTo().defaultContent();
        return this;
    }

    public myLearningResourcesPage clickCenterForWriting() {
        driver.switchTo().frame(0);
        driver.switchTo().frame(1);
        LoadAndSwitchTabs("//*[@id='form1']/div/div/table/tbody/tr[3]/td/a");
        driver.switchTo().defaultContent();
        return this;
    }

    public myLearningResourcesPage clickInformationTechProgram() {
        driver.switchTo().frame(0);
        driver.switchTo().frame(1);
        LoadAndSwitchTabs("//*[@id='form1']/div/div/table/tbody/tr[4]/td/a");
        driver.switchTo().defaultContent();
        return this;
    }

    public myLearningResourcesPage clickStudentSuccessCommunity() {
        driver.switchTo().frame(0);
        driver.switchTo().frame(1);
        LoadAndSwitchTabs("//*[@id='form1']/div/div/table/tbody/tr[5]/td/a");
        driver.switchTo().defaultContent();
        return this;
    }

    public myLearningResourcesPage clickProfessionalStudies() {
        driver.switchTo().frame(0);
        driver.switchTo().frame(1);
        LoadAndSwitchTabs("//*[@id='form1']/div/div/table/tbody/tr[6]/td/a");
        driver.switchTo().defaultContent();
        return this;
    }

    public myLearningResourcesPage ExercisePage(boolean cascade) {
        General.Debug("\nmyLearningResourcesPage::ExercisePage(" + cascade + ")");
        load();

        if (cascade) {
            return this;
        }

        return this;
    }
}

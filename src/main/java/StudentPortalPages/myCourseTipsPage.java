
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 2/7/2015.
 */

public class myCourseTipsPage extends myCoursesPage {
    public myCourseTipsPage() {
        this(null);
    }
    public myCourseTipsPage(WebPage existingPage) {
        super(existingPage);
    }
    public myCourseTipsPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myCourseTipsPage load() {
        super.load();
        clickCourseTips();

        return this;
    }

    public myCourseTipsPage ExercisePage(boolean cascade) {
        General.Debug("\nmyCourseTipsPage::ExercisePage(" + cascade + ")");
        load();
        clickCourseTips();
        clickCourseTips();
        return this;
    }
}



package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 2/7/2015.
 */

public class myInformationTechProgramPage extends myLearningResourcesPage {
    public myInformationTechProgramPage() {
        this(null);
    }
    public myInformationTechProgramPage(WebPage existingPage) {
        super(existingPage);
    }
    public myInformationTechProgramPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myInformationTechProgramPage load() {
        super.load();
        clickInformationTechProgram();
        return this;
    }

    public myInformationTechProgramPage ExercisePage(boolean cascade) {
        General.Debug("\nmyInformationTechProgramPage::ExercisePage(" + cascade + ")");
        load();
        return this;
    }
}


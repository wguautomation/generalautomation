
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;

/*
 * Created by timothy.hallbeck on 2/29/2016.
 */

public class myFinancialServicesPage extends myStudentSupportPage {
    public myFinancialServicesPage() {
        this(null);
    }
    public myFinancialServicesPage(WebPage existingPage) {
        super(existingPage);
    }
    public myFinancialServicesPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myFinancialServicesPage load() {
        super.load();
        Sleep(2000);
        clickFinancialServices();
        wguPowerPointFileUtils.saveScreenshot(this, "myFinancialServicesPage");
        wguPowerPointFileUtils.addBulletPoint("load()");

        return this;
    }

    public myFinancialServicesPage clickApplyForAid() {
        getByXpathAndActivate(myXpath.FinancialServicesPage_ApplyForAid, "clickApplyForAid");
        getByXpath(myXpath.FinancialServicesPage_ApplyForAidCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "clickApplyForAid");

        return this;
    }

    public myFinancialServicesPage clickChangeCircumstances() {
//        getByXpathAndActivate(myXpath.FinancialServicesPage_ChangeCircumstances, "clickChangeCircumstances");
//        getByXpath(myXpath.FinancialServicesPage_ChangeCircumstancesCheck);
//        wguPowerPointFileUtils.saveScreenshot(this, "clickChangeCircumstances");

        return this;
    }

    public myFinancialServicesPage clickAcademicCreditAppeal() {
        getByXpathAndActivate(myXpath.FinancialServicesPage_AcademicCreditAppeal, "clickAcademicCreditAppeal");
        getByXpath(myXpath.FinancialServicesPage_AcademicCreditAppealCheck, true);
        wguPowerPointFileUtils.saveScreenshot(this, "clickAcademicCreditAppeal");

        return this;
    }

    public myFinancialServicesPage clickAcceptScholarship() {
        getByXpathAndActivate(myXpath.FinancialServicesPage_AcceptScholarship, "clickAcceptScholarship");
        getByXpath(myXpath.FinancialServicesPage_AcceptScholarshipCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "clickAcceptScholarship");

        return this;
    }

    public myFinancialServicesPage clickAdditionalInfo() {
        getByXpathAndActivate(myXpath.FinancialServicesPage_AdditionalInfo, "clickAdditionalInfo");
        getByXpath(myXpath.FinancialServicesPage_AdditionalInfoCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "clickAdditionalInfo");

        return this;
    }

    public myFinancialServicesPage clickAlternativeLoan() {
        getByXpathAndActivate(myXpath.FinancialServicesPage_AlternativeLoan, "clickAlternativeLoan");
        getByXpath(myXpath.FinancialServicesPage_AlternativeLoanCheck, true);
        wguPowerPointFileUtils.saveScreenshot(this, "clickAlternativeLoan");

        return this;
    }

    public myFinancialServicesPage clickAmendedTaxForms() {
        getByXpathAndActivate(myXpath.FinancialServicesPage_AmendedTaxForms, "clickAmendedTaxForms");
        getByXpath(myXpath.FinancialServicesPage_AmendedTaxFormsCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "clickAmendedTaxForms");

        return this;
    }

    public myFinancialServicesPage clickContinueProcess() {
        scrollIntoView(myXpath.FinancialServicesPage_ContinueProcess, true);
        getByXpathAndActivate(myXpath.FinancialServicesPage_ContinueProcess, "ContinueProcess");
        getByXpath(myXpath.FinancialServicesPage_ContinueProcessCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "clickContinueProcess");

        return this;
    }

    public myFinancialServicesPage clickFAFSA() {
        scrollIntoView(myXpath.FinancialServicesPage_FAFSA, true);
        getByXpathAndActivate(myXpath.FinancialServicesPage_FAFSA, "clickFAFSA");
        getByXpath(myXpath.FinancialServicesPage_FAFSACheck);
        wguPowerPointFileUtils.saveScreenshot(this, "clickFAFSA");

        return this;
    }

    public myFinancialServicesPage clickLoanEntrance() {
        scrollIntoView(myXpath.FinancialServicesPage_LoanEntrance, true);
        getByXpathAndActivate(myXpath.FinancialServicesPage_LoanEntrance, "clickLoanEntrance");
        getByXpath(myXpath.FinancialServicesPage_LoanEntranceCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "clickLoanEntrance");

        return this;
    }

    public myFinancialServicesPage clickMasterNote() {
        scrollIntoView(myXpath.FinancialServicesPage_MasterNote, true);
        getByXpathAndActivate(myXpath.FinancialServicesPage_MasterNote, "MasterNote");
        getByXpath(myXpath.FinancialServicesPage_MasterNoteCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "MasterNote");

        return this;
    }

    public myFinancialServicesPage clickResponsibleBorrowing() {
        scrollIntoView(myXpath.FinancialServicesPage_ResponsibleBorrowing, true);
        getByXpathAndActivate(myXpath.FinancialServicesPage_ResponsibleBorrowing, "ResponsibleBorrowing");
        getByXpath(myXpath.FinancialServicesPage_ResponsibleBorrowingCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "ResponsibleBorrowing");

        return this;
    }

    public myFinancialServicesPage clickSAPAppealApp() {
        scrollIntoView(myXpath.FinancialServicesPage_SAPAppealApp, true);
        getByXpathAndActivate(myXpath.FinancialServicesPage_SAPAppealApp, "SAPAppealApp");
        getByXpath(myXpath.FinancialServicesPage_SAPAppealAppCheck, true);
        wguPowerPointFileUtils.saveScreenshot(this, "SAPAppealApp");

        return this;
    }

    public myFinancialServicesPage clickWSNeedGrant() {
        scrollIntoView(myXpath.FinancialServicesPage_WSNeedGrant);
        getByXpathAndActivate(myXpath.FinancialServicesPage_WSNeedGrant, "WSNeedGrant");
        getByXpath(myXpath.FinancialServicesPage_WSNeedGrantCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "WSNeedGrant");

        return this;
    }

    public myFinancialServicesPage clickDependencyStatus() {
        scrollIntoView(myXpath.FinancialServicesPage_DependencyStatus);
        getByXpathAndActivate(myXpath.FinancialServicesPage_DependencyStatus, "DependencyStatus");
        getByXpath(myXpath.FinancialServicesPage_DependencyStatusCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "DependencyStatus");

        return this;
    }

    public myFinancialServicesPage clickAccountBalance() {
        getByXpathAndActivate(myXpath.FinancialServicesPage_AccountBalance, "AccountBalance");
        getByXpath(myXpath.FinancialServicesPage_AccountBalanceCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "AccountBalance");

        return this;
    }

    public myFinancialServicesPage clickManagePaymentPlan() {
        getByXpathAndActivate(myXpath.FinancialServicesPage_ManagePaymentPlan, "ManagePaymentPlan");
        getByXpath(myXpath.FinancialServicesPage_ManagePaymentPlanCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "ManagePaymentPlan");

        return this;
    }

    public myFinancialServicesPage clickMakeAPayment() {
        getByXpathAndActivate(myXpath.FinancialServicesPage_MakeAPayment, "MakeAPayment");
        getByXpath(myXpath.FinancialServicesPage_MakeAPaymentCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "MakeAPayment");

        return this;
    }

    public myFinancialServicesPage clickSelectRefundMethod() {
        getByXpathAndActivate(myXpath.FinancialServicesPage_SelectRefundMethod, "SelectRefundMethod");
        getByXpath(myXpath.FinancialServicesPage_SelectRefundMethodCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "SelectRefundMethod");

        return this;
    }

    public myFinancialServicesPage clickOptInTaxForm() {
        scrollIntoView(myXpath.FinancialServicesPage_OptInTaxForm, true);
        getByXpathAndActivate(myXpath.FinancialServicesPage_OptInTaxForm, "OptInTaxForm");
        getByXpath(myXpath.FinancialServicesPage_OptInTaxFormCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "OptInTaxForm");

        return this;
    }

    public myFinancialServicesPage clickObtainTaxForm() {
        scrollIntoView(myXpath.FinancialServicesPage_ObtainTaxForm, true);
        getByXpathAndActivate(myXpath.FinancialServicesPage_ObtainTaxForm, "clickObtainTaxForm");
        getByXpath(myXpath.FinancialServicesPage_ObtainTaxFormCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "clickObtainTaxForm");

        return this;
    }

    public myFinancialServicesPage clickProofOfEnrollment() {
        scrollIntoView(myXpath.FinancialServicesPage_ProofOfEnrollment, true);
        getByXpathAndActivate(myXpath.FinancialServicesPage_ProofOfEnrollment, "ProofOfEnrollment");
        getByXpath(myXpath.FinancialServicesPage_ProofOfEnrollmentCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "ProofOfEnrollment");

        return this;
    }

    public myFinancialServicesPage clickPayFees() {
        scrollIntoView(myXpath.FinancialServicesPage_PayFees, true);
        getByXpathAndActivate(myXpath.FinancialServicesPage_PayFees, "PayFees");
        getByXpath(myXpath.FinancialServicesPage_PayFeesCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "PayFees");

        return this;
    }

    public myFinancialServicesPage clickPaymentHistory() {
        scrollIntoView(myXpath.FinancialServicesPage_PaymentHistory, true);
        getByXpathAndActivate(myXpath.FinancialServicesPage_PaymentHistory, "PaymentHistory");
        getByXpath(myXpath.FinancialServicesPage_PaymentHistoryCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "PaymentHistory");

        return this;
    }

    public myFinancialServicesPage clickNSLDS() {
        scrollIntoView(myXpath.FinancialServicesPage_NSLDS, true);
        getByXpathAndActivate(myXpath.FinancialServicesPage_NSLDS, "NSLDS");
        getByXpath(myXpath.FinancialServicesPage_NSLDSCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "NSLDS");

        return this;
    }

    public myFinancialServicesPage clickRepaymentPlan() {
        scrollIntoView(myXpath.FinancialServicesPage_RepaymentPlan, true);
        getByXpathAndActivate(myXpath.FinancialServicesPage_RepaymentPlan, "RepaymentPlan");
        getByXpath(myXpath.FinancialServicesPage_RepaymentPlanCheck);
        wguPowerPointFileUtils.saveScreenshot(this, "RepaymentPlan");

        return this;
    }

    public myFinancialServicesPage ExercisePage(boolean cascade) {
        General.Debug("\nmyFinancialServicesPage::ExercisePage(" + cascade + ")");
        load();
        clickChangeCircumstances();

/*        load();
        clickAcademicCreditAppeal();

        load();
        clickAcceptScholarship();

        load();
        clickAdditionalInfo();

        load();
        clickAlternativeLoan();

        load();
        clickAmendedTaxForms();

        load();
        clickContinueProcess();

        load();
        clickFAFSA();

        load();
        clickLoanEntrance();

        load();
        clickMasterNote();

        load();
        clickResponsibleBorrowing();

        load();
        clickSAPAppealApp();

        load();
        clickWSNeedGrant();

        load();
        clickDependencyStatus();

        load();
        clickAccountBalance();
*/
        load();
        clickManagePaymentPlan();

        load();
        clickMakeAPayment();

        load();
        clickSelectRefundMethod();

        load();
        clickOptInTaxForm();

        load();
        clickObtainTaxForm();

        load();
        clickProofOfEnrollment();

        load();
        clickPayFees();

        load();
        clickPaymentHistory();

        load();
        clickNSLDS();

        load();
        clickRepaymentPlan();

        return this;
    }
}

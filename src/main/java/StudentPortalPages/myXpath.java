
package StudentPortalPages;

/*
 * Created by timothy.hallbeck on 7/27/2015.
 */

public class myXpath {

    public static String CampusStorePage_StartingUrl   = "http://www.wguonlinestore.com/nat/";

    public static String AboutMePage_PrimaryPhoneLabel   = "//*[@class='form__item__label'][contains(text(), 'Primary Phone:')]";
    public static String AboutMePage_StudentIdLabel      = "//*[@class='form__item__label'][contains(text(), 'Student ID:')]";
    public static String AboutMePage_GenericField1       = "//*[@class='form__item__field__static ng-binding ng-scope'][contains(text(), ";
    public static String AboutMePage_GenericField2       = "//*[@class='form__item__field__static ng-binding'][contains(text(), ";
    public static String AboutMePage_GenericField3       = "//*[@class='form__item__field__static  ng-binding ng-scope'][contains(text(), ";

    public static String AssessmentServicesObjectivePage_Scheduling  = "//*[@id='mainContent']/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[1]/a";
    public static String AssessmentServicesObjectivePage_Proctoring  = "//*[@id='mainContent']/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[2]/a";
    public static String AssessmentServicesObjectivePage_Policies    = "//*[@id='mainContent']/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[3]/a";
    public static String AssessmentServicesObjectivePage_Webcam      = "//*[@id='mainContent']/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[4]/a";
    public static String AssessmentServicesObjectivePage_Replacement = "//*[@id='mainContent']/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[5]/a";
    public static String AssessmentServicesObjectivePage_Whiteboard  = "//*[@id='mainContent']/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[6]/a";
    public static String AssessmentServicesObjectivePage_CoverSheet  = "//*[@id='mainContent']/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[7]/a";

    public static String AssessmentServicesPerformancePage_TaskstreamHelp  = "/html/body/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[1]/a";
    public static String AssessmentServicesPerformancePage_ECare           = "/html/body/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[2]/a";
    public static String AssessmentServicesPerformancePage_TurnItIn        = "/html/body/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[3]/a";
    public static String AssessmentServicesPerformancePage_CapstoneArchive = "/html/body/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[4]/a";

    public static String CoursesPage_Frame               = "//*[@id='_WGU_IFrame_Portlet_INSTANCE_7oZc_iframe']";
    public static String CoursesPage_CourseMaterial      = "//*[contains(text(), 'Course Material')]";
    public static String CoursesPage_LearningRes         = "//*[contains(text(), 'Learning Resources')]";
    public static String CoursesPage_Assessment          = "/html/body/div[1]/div[2]/div/div[3]/div/div/div/div/ng-include/div[1]/a[3]";
    public static String CoursesPage_AllNotes            = "//*[contains(text(), 'All Notes')]";
    public static String CoursesPage_StudyPlanToggle     = "//*[contains(text(), 'Study Plan')]";
//    public static String CoursesPage_NextButton          = "//*[@id='courseMaterial']/div[3]/ng-include/div/div/button[2]";
//    public static String CoursesPage_PreviousButton      = "//*[@id='courseMaterial']/div[3]/ng-include/div/div/button[1]";
    public static String CoursesPage_CourseMentor        = "//*[@id='j_id0:j_id11']/div/div[1]";
    public static String CoursesPage_CourseAnnouncements = "/html/body/div/div[1]/div[1]";
    public static String CoursesPage_CourseTips          = "/html/body/div/div[2]/div[1]";
    public static String CoursesPage_CourseSearch        = "/html/body/div/div[3]/div[1]";
    public static String CoursesPage_CourseChatter       = "/html/body/div/div[4]/div[1]";

    public static String CoursesPage_LeftArrow           = "/html/body/div[1]/div[2]/div/div[2]/div/div/div/aside[1]/button[1]/span";
    public static String CoursesPage_RightArrow          = "/html/body/div[1]/div[2]/div/div[2]/div/div/div/aside[2]/button/span";

    public static String DegreePlanPage_CourseList         = "//*[@class='title']";
    public static String DegreePlanPage_DefaultPlan        = "/html/body/div[1]/div/ng-include/header/h1";
    public static String DegreePlanPage_ScenarioCalculator = "/html/body/div[1]/div/div[4]/ng-include[2]/div/div[1]";

    public static String FinancialAid_2012Tab          = "/html/body/div/ng-view/div/div/div/div[2]/div/section/div/div[2]/div[1]/a[1]";
    public static String FinancialAid_2012TabLoaded    = "//*[@id='1213']/section[1]/table/tbody/tr[1]/td[1]/a";
    public static String FinancialAid_2013Tab          = "/html/body/div/ng-view/div/div/div/div[2]/div/section/div/div[2]/div[1]/a[2]";
    public static String FinancialAid_2013TabLoaded    = "//*[@id='1314']/section[1]/table/tbody/tr[1]/td[1]/a";
    public static String FinancialAid_2014Tab          = "/html/body/div/ng-view/div/div/div/div[2]/div/section/div/div[2]/div[1]/a[3]";
    public static String FinancialAid_2014TabLoaded    = "//*[@id='1215']/section[1]/table/tbody/tr[1]/td[1]/a";
    public static String FinancialAid_2015Tab          = "/html/body/div/ng-view/div/div/div/div[2]/div/section/div/div[2]/div[1]/a[4]";
    public static String FinancialAid_2015TabLoaded    = "//*[@id='1216']/section[1]/table/tbody/tr[1]/td[1]/a";

    public static String FinancialServicesPage_AcademicCreditAppeal      = "//*[contains(text(), 'Academic Credit Appeal')]";
    public static String FinancialServicesPage_AcademicCreditAppealCheck = "//*[@id='OnBaseForm']/table[1]/tbody/tr[1]/td/p";
    public static String FinancialServicesPage_AcceptScholarship         = "//*[contains(text(), 'Accept Scholarship Conditions')]";
    public static String FinancialServicesPage_AcceptScholarshipCheck    = "//*[@id='ferpaForm']/label[1]/strong";
    public static String FinancialServicesPage_AdditionalInfo            = "//*[contains(text(), 'Additional Information Needed')]";
    public static String FinancialServicesPage_AdditionalInfoCheck       = "//*[@id='0']/div[2]/h2";
    public static String FinancialServicesPage_AlternativeLoan           = "//*[contains(text(), 'Alternative Loan Entrance Agreement')]";
    public static String FinancialServicesPage_AlternativeLoanCheck      = "/html/body/section[2]/table[1]/tbody/tr[2]/td/p";
    public static String FinancialServicesPage_AmendedTaxForms           = "//*[contains(text(), 'Amended Tax Forms')]";
    public static String FinancialServicesPage_AmendedTaxFormsCheck      = "/html/body/form/div/div[3]/div/div[1]/h3[1]";
    public static String FinancialServicesPage_ApplyForAid               = "//*[contains(text(), 'Apply for Financial Aid')]";
    public static String FinancialServicesPage_ApplyForAidCheck          = "//*[@id='logo']";
    public static String FinancialServicesPage_ChangeCircumstances       = "//*[contains(text(), '2015-2016 Change in Financial Circumstances')]";
    public static String FinancialServicesPage_ChangeCircumstancesCheck  = "/html/body/table[1]/tbody/tr/td[2]/p[2]/font/strong";

    public static String FinancialServicesPage_ContinueProcess           = "//*[contains(text(), 'Continue Your Financial Aid Process')]";
    public static String FinancialServicesPage_ContinueProcessCheck      = "/html/body/div[3]/form/div/table/tbody/tr/td[2]/span";
    public static String FinancialServicesPage_FAFSA                     = "//*[contains(text(), '2015-2016 Free Application for Federal Student Aid (FAFSA)')]";
    public static String FinancialServicesPage_FAFSACheck                = "//*[@id='logo']";
    public static String FinancialServicesPage_LoanEntrance              = "//*[contains(text(), 'Loan Entrance Counseling')]";
    public static String FinancialServicesPage_LoanEntranceCheck         = "/html/body/div[1]/div[1]/div[1]/a[2]/img";
    public static String FinancialServicesPage_MasterNote                = "//*[contains(text(), 'Master Promissory Note')]";
    public static String FinancialServicesPage_MasterNoteCheck           = "//*[@id='main_content']/div[2]/div[2]/p[1]";
    public static String FinancialServicesPage_ResponsibleBorrowing      = "//*[contains(text(), 'Responsible Borrowing Initiative Acknowledgment')]";
    public static String FinancialServicesPage_ResponsibleBorrowingCheck = "/html/body/table[1]/tbody/tr/td[2]/p[2]/font/strong";
    public static String FinancialServicesPage_SAPAppealApp              = "//*[contains(text(), 'SAP Appeal Application')]";
    public static String FinancialServicesPage_SAPAppealAppCheck         = "/html/body/section[2]/h2";
    public static String FinancialServicesPage_WSNeedGrant               = "//*[contains(text(), 'Washington State Need Grant Directive')]";
    public static String FinancialServicesPage_WSNeedGrantCheck          = "/html/body/section[2]/h3";
    public static String FinancialServicesPage_DependencyStatus          = "//*[contains(text(), 'Dependency Status Override Request Form')]";
    public static String FinancialServicesPage_DependencyStatusCheck     = "/html/body/div/table/tbody/tr/td[2]/span";
    public static String FinancialServicesPage_MakeAPayment              = "//*[contains(text(), 'Make a Payment')]";
    public static String FinancialServicesPage_MakeAPaymentCheck         = "//*[@id='lblGlobalMessage']/p/span/strong";
    public static String FinancialServicesPage_AccountBalance            = "//*[contains(text(), 'Balance and Details')]";
    public static String FinancialServicesPage_AccountBalanceCheck       = "//*[@id='lblGlobalMessage']/p/span/strong";
    public static String FinancialServicesPage_ManagePaymentPlan         = "//*[contains(text(), 'Make a Payment')]";
    public static String FinancialServicesPage_ManagePaymentPlanCheck    = "//*[@id='lblGlobalMessage']/p/span/strong";
    public static String FinancialServicesPage_SelectRefundMethod        = "//*[contains(text(), 'Select Refund Method')]";
    public static String FinancialServicesPage_SelectRefundMethodCheck   = "//*[@id='SubHeaderLeftDiv']/h1";
    public static String FinancialServicesPage_OptInTaxForm              = "//*[contains(text(), 'Opt In for Electronic Tax Forms')]";
    public static String FinancialServicesPage_OptInTaxFormCheck         = "//*[@id='main']/div[1]/p[2]";
    public static String FinancialServicesPage_ObtainTaxForm             = "//*[contains(text(), 'Obtain Tax Forms')]";
    public static String FinancialServicesPage_ObtainTaxFormCheck        = "//*[@id='lblGlobalMessage']/p/span/strong";
    public static String FinancialServicesPage_ProofOfEnrollment         = "//*[contains(text(), 'Enrollment Letter and Loan Deferment')]";
    public static String FinancialServicesPage_ProofOfEnrollmentCheck    = "/html/body/div[2]/table/tbody/tr[2]/td";
    public static String FinancialServicesPage_PayFees                   = "//*[contains(text(), 'Pay Fees or Purchase Items')]";
    public static String FinancialServicesPage_PayFeesCheck              = "//*[@id='lblGlobalMessage']/p/span/strong";
    public static String FinancialServicesPage_PaymentHistory            = "//*[contains(text(), 'Payment History')]";
    public static String FinancialServicesPage_PaymentHistoryCheck       = "//*[@id='lblGlobalMessage']/p/span/strong";
    public static String FinancialServicesPage_NSLDS                     = "//*[contains(text(), 'National Student Loan Data System')]";
    public static String FinancialServicesPage_NSLDSCheck                = "//*[@id='displayLoginText']/p";
    public static String FinancialServicesPage_RepaymentPlan             = "//*[contains(text(), 'Loan Repayment Planning')]";
    public static String FinancialServicesPage_RepaymentPlanCheck        = "//*[@id='block-views-preface-block']/div/div/div/div/div/span/span/h1[2]";

    public static String Generic_Header                = "//*[@class='nav-wrap--main']";

    public static String HomePage_ActionItemsUrl       = "https://PREFIXmy.wgu.edu/group/wgu-student-v3/action-items";
    public static String HomePage_CommunitiesUrl       = "https://PREFIXcommunity.wgu.edu/clearspacex/index.jspa";
    public static String HomePage_CommunitiesList      = "//*[@class='list--action__item ng-scope']";
    public static String HomePage_CoursesLink          = "//a[contains(., 'Courses')]";
    public static String HomePage_DegreePlanLink       = "//a[contains(., 'Degree Plan')]";
    public static String HomePage_EmailList            = "//*[@class='list--action__item home-page__email__item ng-scope']";
    public static String HomePage_Logout               = "//a[contains(., 'Log Out')]";
    public static String HomePage_MentorName           = "//*[@id='homePageMentorContent']/div[2]/div/h4";
    public static String HomePage_MentorEmail          = "//*[@id='homePageMentorContent']/div[2]/div/a";
    public static String HomePage_NewsUrl              = "https://PREFIXwebapp47.wgu.edu/passthrough/forapp/homepage?#/news";
    public static String HomePage_PrivacySettings      = "//a[contains(., 'Privacy Settings')]";
    public static String HomePage_ScheduleUrlProd      = "https://www.google.com/calendar/hosted/wgu.edu/render";
    public static String HomePage_ScheduleUrlLane1     = "https://www.google.com/calendar/hosted/wgutest.com/render";
    public static String HomePage_ScheduleList         = "//*[@class='text--left home-page__schedule__events__event ng-binding ng-scope']";
    public static String HomePage_SecuritySettings     = "//a[contains(., 'Security Settings')]";
    public static String HomePage_SocialMedia          = "//a[contains(., 'Social Media')]";
    public static String HomePage_StudentLink          = "//*[@id='user-menu']";
    public static String HomePage_StudentInfoLink      = "//a[contains(., 'Student Info')]";
    public static String HomePage_StudentSupportLink   = "//a[contains(., 'Student Support')]";
    public static String HomePage_SuccessCentersLink   = "//a[contains(., 'Success Centers')]";
    public static String HomePage_WGUIcon              = "/html/body/div/header/nav/ul[1]/li[1]/a/img";

    public static String LibraryPage_AskUsTab          = "//*[@id='s-lg-guide-tabs']/ul/li[8]/a/span";
    public static String LibraryPage_AskUsLoaded       = "//*[@id='s-lg-box-1108587']/h2";
    public static String LibraryPage_EBooksTab         = "//*[@id='s-lg-guide-tabs']/ul/li[3]/a/span";
    public static String LibraryPage_EBooksLoaded      = "//*[@id='s-lg-box-1108620']/h2";
    public static String LibraryPage_EReservesTab      = "//*[@id='s-lg-guide-tabs']/ul/li[4]/a/span";
    public static String LibraryPage_EReservesLoaded   = "//*[@id='s-lib-public-header-desc']";
    public static String LibraryPage_GuidesTab         = "//*[@id='s-lg-guide-tabs']/ul/li[7]/a/span";
    public static String LibraryPage_GuidesTabLoaded   = "//*[@id='s-lg-box-2134633']/h2";
    public static String LibraryPage_HomeTab           = "//*[@id='s-lg-guide-tabs']/ul/li[1]/a/span";
    public static String LibraryPage_HomeLoaded        = "//*[@id='s-lg-guide-header-url']/span[2]";
    public static String LibraryPage_HowDoITab         = "//*[@id='s-lg-guide-tabs']/ul/li[2]/a/span";
    public static String LibraryPage_HowDoILoaded      = "//*[@id='s-lg-content-2026522']/p[10]/a";
    public static String LibraryPage_LoginInput        = "/html/body/form/input[2]";
    public static String LibraryPage_LoginSubmit       = "/html/body/form/input[3]";
    public static String LibraryPage_QuickLinksTab     = "//*[@id='s-lg-guide-tabs']/ul/li[5]/a/span";
    public static String LibraryPage_QuickLinksLoaded  = "//*[@id='s-lg-box-2109322']/h2";
    public static String LibraryPage_SearchBox         = "//*[@id='ebscohostsearchtext']";
    public static String LibraryPage_SearchButton      = "//*[@id='ebscoIsPubFinder']/input[2]";
    public static String LibraryPage_ServicesTab       = "//*[@id='s-lg-guide-tabs']/ul/li[6]/a/span";
    public static String LibraryPage_ServicesLoaded    = "//*[@id='s-lg-box-1108585']/h2";

    public static String NotesPage_SortBy              = "//*[@id='notes']/div/div/div[1]/div/div/select";

    public static String ReferAFriendPage_StartingUrl  = "https://www.wgu.edu/referfriend";

    public static String ResourcesPage_History            = "/html/body/div/ng-view/div/div[2]/div[3]/div[1]/span/div[1]/ul/li[3]/a";
    public static String ResourcesPage_StartingUrl        = "https://PREFIXmy.wgu.edu/group/wgu-student-v3/success-centers";
    public static String ResourcesPage_CoverSheet         = "/html/body/div/ng-view/div/div[2]/div[3]/div[1]/span/div[2]/ul/li[3]/a";

    public static String StudentLifePage_NightOwlBlog     = "/html/body/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[1]/a";
    public static String StudentLifePage_NightOwlArchive  = "/html/body/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[2]/a";
    public static String StudentLifePage_WGUNews          = "/html/body/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[3]/a";
    public static String StudentLifePage_WGUVideo         = "/html/body/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[4]/a";
    public static String StudentLifePage_CampusStore      = "/html/body/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[5]/a";
    public static String StudentLifePage_SpecialDiscounts = "/html/body/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[6]/a";
    public static String StudentLifePage_ReferAFriend     = "/html/body/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[7]/a";
    public static String StudentLifePage_WGUOrientation   = "/html/body/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[8]/a";
    public static String StudentLifePage_WGUAlumni        = "/html/body/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[9]/a";
    public static String StudentLifePage_Prevention       = "/html/body/ng-include/div/div[2]/div/div[2]/div/div/div[1]/div[10]/a";

    public static String StudentServicesPage_DownloadTranscript = "/html/body/ng-include/div/div[2]/div/div[2]/div/div[1]/div[1]/div[2]/a";
    public static String StudentServicesPage_FERPAForm          = "/html/body/ng-include/div/div[2]/div/div[2]/div/div[1]/div[1]/div[7]/a";
    public static String StudentServicesPage_GradeReport        = "/html/body/ng-include/div/div[2]/div/div[2]/div/div[1]/div[1]/div[5]/a";
    public static String StudentServicesPage_ProofEnrollment    = "/html/body/ng-include/div/div[2]/div/div[2]/div/div[1]/div[1]/div[4]/a";
    public static String StudentServicesPage_RequestAccomm      = "/html/body/ng-include/div/div[2]/div/div[2]/div/div[1]/div[1]/div[8]/a";
    public static String StudentServicesPage_RequestIdCard      = "/html/body/ng-include/div/div[2]/div/div[2]/div/div[1]/div[1]/div[1]/a";
    public static String StudentServicesPage_RequestTranscript  = "/html/body/ng-include/div/div[2]/div/div[2]/div/div[1]/div[1]/div[3]/a";
    public static String StudentServicesPage_StudentSuccessKit  = "/html/body/ng-include/div/div[2]/div/div[2]/div/div[1]/div[1]/div[6]/a";
    public static String StudentServicesPage_TermBreak          = "/html/body/ng-include/div/div[2]/div/div[2]/div/div[1]/div[1]/div[9]/a";

    public static String StudentSupportPage_StudentServices     = "/html/body/div/header/nav/ul[1]/li[8]/ul/li[2]/a";
    public static String StudentSupportPage_ITServiceDesk       = "/html/body/div/header/nav/ul[1]/li[8]/ul/li[3]/a";
    public static String StudentSupportPage_AssessmentServices  = "/html/body/div/header/nav/ul[1]/li[8]/ul/li[4]/a";
    public static String StudentSupportPage_AssessmentServicesObjectives  = "//*[contains(text(), 'Objective Assessments')]";

    public static String StudentSupportPage_FinancialServices   = "/html/body/div/header/nav/ul[1]/li[8]/ul/li[5]/a";
    public static String StudentSupportPage_StudentLife         = "/html/body/div/header/nav/ul[1]/li[8]/ul/li[6]/a";
    public static String StudentSupportPage_ContactWgu          = "/html/body/div/header/nav/ul[1]/li[8]/ul/li[7]/a";
    public static String StudentSupportPage_GenericText         = "//*[contains(text(), 'REPLACETHIS')]";

    public static String StudentSuccessCenterPage_GenericText   = "//*[contains(text(), 'REPLACETHIS')]";

    public static String SuccessCentersPage_BlueSigninBlock       = "/html/body/div/div[2]/div[2]/a";
    public static String SuccessCentersPage_CareerCenter          = "//*[@id='mainContent']/ng-include/div/main/div/section/div/div[4]/div/header/a";
    public static String SuccessCentersPage_Library               = "//*[@id='mainContent']/ng-include/div/main/div/section/div/div[2]/div/header/a";
    public static String SuccessCentersPage_MathCenter            = "//*[@id='mainContent']/ng-include/div/main/div/section/div/div[6]/div/header/a";
    public static String SuccessCentersPage_StudentSuccessCenter  = "//*[@id='mainContent']/ng-include/div/main/div/section/div/div[3]/div/header/a";
    public static String SuccessCentersPage_WellConnect           = "//*[@id='mainContent']/ng-include/div/main/div/section/div/div[5]/div/header/a";
    public static String SuccessCentersPage_WritingCenter         = "//*[@id='mainContent']/ng-include/div/main/div/section/div/div[1]/div/header/a";

    public static String WritingCenterPage_AcademicAuthenticity   = "//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[2]/div/div[5]/div[3]/div[1]/a";
    public static String WritingCenterPage_APACitation            = "//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[2]/div/div[5]/div[4]/div[1]/a";
    public static String WritingCenterPage_AppointmentsCalendar   = "//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[2]/div/div[3]/div[2]/span/font[2]/a";
    public static String WritingCenterPage_AppointmentOverview    = "//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[2]/div/div[3]/div[3]/ul/li[1]/font/a";
    public static String WritingCenterPage_CurrentVideos          = "//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[2]/div/div[4]/div[2]/font/a";
    public static String WritingCenterPage_FAQGuide               = "//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[2]/div/div[3]/div[3]/ul/li[3]/font/a";
    public static String WritingCenterPage_GuideToWriting         = "//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[2]/div/div[1]/div[5]/span/font/a";
    public static String WritingCenterPage_MentorAppointments     = "//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[1]/div/div/div[2]/div/p/span/a";
    public static String WritingCenterPage_MentorMinutes          = "//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[2]/div/div[3]/div[3]/ul/li[2]/font/a";
    public static String WritingCenterPage_OriginalityResources   = "//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[2]/div/div[5]/div[8]/div[1]/a/font";
    public static String WritingCenterPage_PraxisSupport          = "//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[2]/div/div[5]/div[5]/div[1]/a/font";
    public static String WritingCenterPage_ResearchWriting        = "//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[2]/div/div[5]/div[7]/div[1]/a/font";
    public static String WritingCenterPage_WebinarSessions        = "//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[1]/div/div/div[1]/div[1]/span/a";
    public static String WritingCenterPage_WritingInstruction     = "//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[2]/div/div[5]/div[2]/div[1]/a/font";
    public static String WritingCenterPage_WritingProcess         = "//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[2]/div/div[5]/div[6]/div[1]/a/font";

}

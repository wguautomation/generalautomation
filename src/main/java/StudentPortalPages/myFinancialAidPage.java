
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;

/*
 * Created by timothy.hallbeck on 2/2/2015.
 */

public class myFinancialAidPage extends myStudentSupportPage {
    public myFinancialAidPage() {
        this(null);
    }
    public myFinancialAidPage(WebPage existingPage) {
        super(existingPage);
    }
    public myFinancialAidPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myFinancialAidPage load() {
        super.load();
        getByXpath(myXpath.StudentSupportPage_FinancialServices).click();
        Sleep(4000);
        wguPowerPointFileUtils.saveScreenshot(this, "myFinancialAidPage");
        wguPowerPointFileUtils.addBulletPoint("load()");

        return this;
    }

    public myFinancialAidPage verify2012Tab() {
        switchToFrame(0);
        getByXpath(myXpath.FinancialAid_2012Tab).click();
        Sleep(1000);
        getByXpath(myXpath.FinancialAid_2012TabLoaded);
        switchToDefaultFrame();

        return this;
    }

    public myFinancialAidPage ExercisePage(boolean cascade) {
        General.Debug("\nmyFinancialAidPage::ExercisePage(" + cascade + ")");
        load();
        verify2012Tab();

        return this;
    }
}

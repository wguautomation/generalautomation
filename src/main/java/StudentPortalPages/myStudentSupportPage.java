
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;

/*
 * Created by timothy.hallbeck on 2/11/2016.
 */

public class myStudentSupportPage extends myHomePage {

    public myStudentSupportPage() {
        this(null);
    }
    public myStudentSupportPage(WebPage existingPage) {
        super(existingPage);
    }
    public myStudentSupportPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myStudentSupportPage load() {
        super.load();
        clickStudentServicesLink();
        wguPowerPointFileUtils.saveScreenshot(this, "myStudentSupportPage");
        wguPowerPointFileUtils.addBulletPoint("load()");

        return this;
    }

    public myStudentSupportPage clickStudentServicesLink() {
        clickStudentSupportMenu();
        getByXpath(myXpath.StudentSupportPage_StudentServices).click();
        getByXpath(myXpath.Generic_Header).click();
        getByXpathVerifyTextPresent(myXpath.StudentSupportPage_GenericText, "A WGU student identification card provides");

        return this;
    }

    public myStudentSupportPage clickITServiceDesk() {
        clickStudentSupportMenu();
        getByXpath(myXpath.StudentSupportPage_ITServiceDesk).click();
        getByXpath(myXpath.Generic_Header).click();
        getByXpathVerifyTextPresent(myXpath.StudentSupportPage_GenericText, "The IT service desk can provide you");

        return this;
    }

    public myStudentSupportPage clickAssessmentServices() {
        clickStudentSupportMenu();
        getByXpath(myXpath.StudentSupportPage_AssessmentServices).click();
        getByXpath(myXpath.Generic_Header).click();
        getByXpathVerifyTextPresent(myXpath.StudentSupportPage_GenericText, "Assessment Services");

        return this;
    }

    public myStudentSupportPage clickAssessmentServicesObjectives() {
        clickStudentSupportMenu();
        getByXpath(myXpath.StudentSupportPage_AssessmentServices).click();
        getByXpath(myXpath.StudentSupportPage_AssessmentServicesObjectives).click();
        getByXpath(myXpath.Generic_Header).click();
        getByXpathVerifyTextPresent(myXpath.StudentSupportPage_GenericText, "Objective Assessments");

        return this;
    }

    public myStudentSupportPage clickAssessmentServicesPerformance() {
        getByXpath(myXpath.StudentSupportPage_AssessmentServices).click();
        getByXpath(myXpath.Generic_Header).click();
        getByXpathVerifyTextPresent(myXpath.StudentSupportPage_GenericText, "WGU has created a video to help");

        return this;
    }

    public myStudentSupportPage clickFinancialServices() {
        clickStudentSupportMenu();
        getByXpath(myXpath.StudentSupportPage_FinancialServices).click();
        getByXpath(myXpath.Generic_Header).click();
        getByXpathVerifyTextPresent(myXpath.StudentSupportPage_GenericText, "To apply for federal financial aid");

        return this;
    }

    public myStudentSupportPage clickStudentLife() {
        clickStudentSupportMenu();
        getByXpath(myXpath.StudentSupportPage_StudentLife).click();
        getByXpath(myXpath.Generic_Header).click();
        getByXpathVerifyTextPresent(myXpath.StudentSupportPage_GenericText, "Read the latest news, student success stories");

        return this;
    }

    public myStudentSupportPage clickContactWGU() {
        clickStudentSupportMenu();
        getByXpath(myXpath.StudentSupportPage_ContactWgu).click();
        getByXpath(myXpath.Generic_Header).click();
        getByXpathVerifyTextPresent(myXpath.StudentSupportPage_GenericText, "Contact WGU");

        return this;
    }

    public myStudentSupportPage clickCoverSheet() {
        getByXpathAndActivate(myXpath.ResourcesPage_CoverSheet, "Cover Sheet");

        return this;
    }

    public myStudentSupportPage ExercisePage(boolean cascade) {
        General.Debug("\nmyStudentSupportPage::ExercisePage(" + cascade + ")");
        load();

        if (cascade) {
// has been totally revamped            new myFinancialServicesPage(this).ExercisePage(true);
            new myAssessmentServicesObjectivePage(this).ExercisePage(true);
            new myStudentLifePage(this).ExercisePage(true);
            new myITServiceDeskPage(this).ExercisePage(true);
            new myContactWGUPage(this).ExercisePage(true);
            new myStudentServicesPage(this).ExercisePage(true);
        } else {
            clickStudentServicesLink();

            load();
            clickITServiceDesk();

            load();
            clickAssessmentServices();

            load();
            clickAssessmentServicesPerformance();

            load();
            clickFinancialServices();

            load();
            clickStudentLife();

            load();
            clickContactWGU();

        }
        return this;
    }

}



package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 2/7/2015.
 */

public class myAlumniRelationsPage extends myLearningResourcesPage {
    public myAlumniRelationsPage() {
        this(null);
    }
    public myAlumniRelationsPage(WebPage existingPage) {
        super(existingPage);
    }
    public myAlumniRelationsPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myAlumniRelationsPage load() {
        super.load();
        clickAlumniRelations();
        return this;
    }

    public myAlumniRelationsPage ExercisePage(boolean cascade) {
        General.Debug("\nmyAlumniRelationsPage::ExercisePage(" + cascade + ")");
        load();
        return this;
    }
}

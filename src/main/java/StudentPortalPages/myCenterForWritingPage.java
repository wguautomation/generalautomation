
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 2/7/2015.
 */

public class myCenterForWritingPage extends myLearningResourcesPage {
    public myCenterForWritingPage() {
        this(null);
    }
    public myCenterForWritingPage(WebPage existingPage) {
        super(existingPage);
    }
    public myCenterForWritingPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myCenterForWritingPage load() {
        super.load();
        clickCenterForWriting();
        return this;
    }

    public myCenterForWritingPage ExercisePage(boolean cascade) {
        General.Debug("\nmyCenterForWritingPage::ExercisePage(" + cascade + ")");
        load();
        return this;
    }
}

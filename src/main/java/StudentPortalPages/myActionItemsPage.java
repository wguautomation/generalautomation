
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import JsonDataClasses.JsonStudentActionItemArray;
import OracleClasses.OracleTodosTask;
import Utils.*;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by timothy.hallbeck on 2/7/2015.
 */

public class myActionItemsPage extends myHomePage {

    private List<String> titles       = new ArrayList<>();
    private List<String> dueDates     = new ArrayList<>();
    private List<String> descriptions = new ArrayList<>();

    public myActionItemsPage() {
        this(null);
    }
    public myActionItemsPage(WebPage existingPage) {
        super(existingPage);
    }
    public myActionItemsPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myActionItemsPage load() {
        super.load();
        clickActionItemsPage();

        return this;
    }

    public myActionItemsPage AddNewActionItem() {
        switchToFrame(0);
        getByXpath("//*[@id='titleField']").sendKeys("An Action Item");
        getByXpath("//*[@id='date']").sendKeys("03/20/16" + Keys.ESCAPE);
        getByXpath("//*[@id='description']").sendKeys("Planning ahead");
        wguPowerPointFileUtils.saveScreenshot(this, "AddNewActionItem");
        getByXpath("//*[@id='submitBtn']").click();
        RestoreDefaultFrameAndWindow();
        wguPowerPointFileUtils.addBulletPoint("AddNewActionItem()");

        return this;
    }

    public myActionItemsPage toggleActionsFromMentor() {
        wguPowerPointFileUtils.saveScreenshot(this, "toggleActionsFromMentor");
        wguPowerPointFileUtils.addBulletPoint("before toggle");
        switchToFrame(0);
        if (getByXpath("//*[@id='tab2']/div/accordion[1]/div[1]/span").isDisplayed())
            getByXpath("//*[@id='tab2']/div/accordion[1]/div[1]/span").click();
        RestoreDefaultFrameAndWindow();
        wguPowerPointFileUtils.saveScreenshot(this, "toggleActionsFromMentor");
        wguPowerPointFileUtils.addBulletPoint("after toggle");

        return this;
    }

    public myActionItemsPage toggleMyActionList() {
        wguPowerPointFileUtils.saveScreenshot(this, "toggleMyActionList");
        wguPowerPointFileUtils.addBulletPoint("before toggle");

        switchToFrame(0);
        getByXpath("//*[@id='tab3']/div/accordion[1]/div[1]/span").click();
        RestoreDefaultFrameAndWindow();

        wguPowerPointFileUtils.saveScreenshot(this, "toggleMyActionList");
        wguPowerPointFileUtils.addBulletPoint("after toggle");

        return this;
    }

    // Save what we know we need in class members, let the user grab anything else from the element list
    public List<WebElement> GetActiveTasks() {
        List<WebElement> elements;
        WebElement myActionList;

        switchToFrame(0);
        myActionList = getByXpath("//*[@id='tab3']/div");
        elements = getListByXpath(myActionList, ".//*[@class='action-items__ai-container ng-scope']");

        int i=0;
        for (WebElement element : elements) {
            titles.add(getChildByXpath(element, ".//*[@class='action-item__title ng-binding']").getText());
            dueDates.add(getChildByXpath(element, ".//*[@class='action-item__date ng-binding']").getText());
            descriptions.add(getChildByXpath(element, ".//*[@class='action-item__description ng-binding']").getText());

            General.Debug("");
            General.Debug("Title: " + titles.get(i));
            General.Debug("Due:   " + dueDates.get(i));
            General.Debug("Desc:  " + descriptions.get(i));
            i++;
        }

        return elements;
    }

    public myActionItemsPage checkDataItems() {
        if (!doDataValidation())
            return this;
        List <WebElement> taskElements = GetActiveTasks();

        String result;
        if (loginType.isLane1())
            result = new OssoSession().Get("https://student-actionitems.qa.wgu.edu/v2/api/student/106679/tasks", LoginType.LANE1_NORMAL);
        else
            result = new OssoSession().Get("https://student-actionitems.wgu.edu/v2/api/student/106679/tasks", LoginType.PRODUCTION_NORMAL);
        JsonStudentActionItemArray actionItemArray = new JsonStudentActionItemArray(result).getActiveActionItems();

        // compare 2 lists on prod, 3 on qa
        if (loginType.isLane1()) {
            OracleTodosTask taskData = new OracleTodosTask(loginType.getUsername());
            taskData.getActiveTasks();

            General.Debug("Comparing Oracle tasks to displayed tasks", true);
            for (int i = 0; i < taskData.size(); i++) {
                General.assertDebug(wguStringUtils.verifyEqual(taskData.getTitle(i), titles.get(i), actionItemArray.getActionItem(i).getTitle()),
                        "titles didn't match: (" + taskData.getTitle(i) + ", " + titles.get(i) + ", " + actionItemArray.getActionItem(i).getTitle() + ")");
                General.assertDebug(taskData.getTitle(i).contentEquals(titles.get(i)), "  " + taskData.getTitle(i) + " = " + titles.get(i));
                General.assertDebug(taskData.getDueDate(i).contentEquals(dueDates.get(i)), "  " + taskData.getDueDate(i) + " = " + dueDates.get(i));
                General.assertDebug(taskData.getDescription(i).contentEquals(descriptions.get(i)), "  " + taskData.getDescription(i) + " = " + descriptions.get(i));

            }
        } else {
            for (int i = 0; i < actionItemArray.size(); i++) {
                General.assertDebug(wguStringUtils.verifyEqual(titles.get(i), actionItemArray.getActionItem(i).getTitle()),
                        "titles didn't match: (" + titles.get(i) + ", " + actionItemArray.getActionItem(i).getTitle() + ")");
                General.assertDebug(actionItemArray.getActionItem(i).getDateDue().contentEquals(dueDates.get(i)),
                        dueDates.get(i));
                General.assertDebug(actionItemArray.getActionItem(i).getDescription().contentEquals(descriptions.get(i)),
                        descriptions.get(i));

            }
        }

        return this;
    }

    public myActionItemsPage ExercisePage(boolean cascade) {
        General.Debug("\nmyActionItemsPage::ExercisePage(" + cascade + ")");
        load();
        wguPowerPointFileUtils.addBulletPoint("ExercisePage()");

//        checkDataItems();

        toggleActionsFromMentor();
        toggleActionsFromMentor();
        toggleMyActionList();
        toggleMyActionList();

        return this;
    }
}

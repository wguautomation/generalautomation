
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 2/7/2015.
 */

public class myAssessmentPage extends myCoursesPage {
    public myAssessmentPage() {
        this(null);
    }

    public myAssessmentPage(WebPage existingPage) {
        super(existingPage);
    }
    public myAssessmentPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myAssessmentPage load() {
        super.load();
        clickAssessment();

        return this;
    }

    public myAssessmentPage ExercisePage(boolean cascade) {
        General.Debug("\nmyAssessmentPage::ExercisePage(" + cascade + ")");
        load();

        return this;
    }
}


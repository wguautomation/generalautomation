
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import java.util.List;

/*
 * Created by timothy.hallbeck on 1/16/2015.
 */

public class myCoursesPage extends myHomePage {
    public myCoursesPage() {
        this(null);
    }
    public myCoursesPage(WebPage existingPage) {
        super(existingPage);
    }
    public myCoursesPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myCoursesPage load() {
        super.load();
        clickCoursesPage();
        scrollUp(4);
        driver.switchTo().frame(0);
        waitForControl(myXpath.CoursesPage_StudyPlanToggle);
        wguPowerPointFileUtils.addBulletPoint("myCoursesPage::load()");
        switchToDefaultFrame();

        return this;
    }

    public myCoursesPage clickCourseMaterial() {
        driver.switchTo().frame(0);
        getByXpath(myXpath.CoursesPage_CourseMaterial).click();
        switchToDefaultFrame();
        wguPowerPointFileUtils.saveScreenshot(this, "clickCourseMaterial");

        return this;
    }

    public myCoursesPage clickLearningResources() {
        driver.switchTo().frame(0);
        getByXpath(myXpath.CoursesPage_LearningRes).click();
        switchToDefaultFrame();
        wguPowerPointFileUtils.saveScreenshot(this, "clickLearningResources");

        return this;
    }

    public myCoursesPage clickAssessment() {
        driver.switchTo().frame(0);
        getByXpath(myXpath.CoursesPage_Assessment).click();
        switchToDefaultFrame();
        wguPowerPointFileUtils.saveScreenshot(this, "clickAssessment");

        return this;
    }

    public myCoursesPage clickAllNotes() {
        driver.switchTo().frame(0);
        getByXpath(myXpath.CoursesPage_AllNotes).click();
        switchToDefaultFrame();
        wguPowerPointFileUtils.saveScreenshot(this, "clickAllNotes");

        return this;
    }

    public myCoursesPage clickStudyPlanAll() {
        driver.switchTo().frame(0);
        getByXpath(myXpath.CoursesPage_StudyPlanToggle).click();
        switchToDefaultFrame();
        wguPowerPointFileUtils.saveScreenshot(this, "clickStudyPlanAll");

        return this;
    }

    public myCoursesPage clickStudyPlanRemaining() {
        driver.switchTo().frame(0);
        getByXpath(myXpath.CoursesPage_StudyPlanToggle).click();
        switchToDefaultFrame();
        wguPowerPointFileUtils.saveScreenshot(this, "clickStudyPlanRemaining");

        return this;
    }

    public myCoursesPage ClickAllStudyPlanItems() {
        String text;

        clickStudyPlanRemaining();
        clickStudyPlanAll();
        driver.switchTo().frame(0);

        List<WebElement> listsOfLiElements = driver.findElements(By.tagName("li"));

        Actions actionObject = new Actions(driver);
        for (WebElement element : listsOfLiElements) {
            text = element.getAttribute("id");
            if (element.isDisplayed() && (text.contains("course-") || text.contains("competencygroup-") || text.contains("activity-"))) {
                element.click();
                actionObject.sendKeys(Keys.ARROW_DOWN).perform();
                Sleep(250);
            }
        }
        switchToDefaultFrame();

        return this;
    }

    public myCoursesPage clickLeftColumn() {
        driver.switchTo().frame(0);
        getByXpath(myXpath.CoursesPage_LeftArrow).click();
        switchToDefaultFrame();
        wguPowerPointFileUtils.saveScreenshot(this, "clickLeftColumn");

        return this;
    }

    public myCoursesPage clickRightColumn() {
        driver.switchTo().frame(0);
        getByXpath(myXpath.CoursesPage_RightArrow).click();
        switchToDefaultFrame();
        wguPowerPointFileUtils.saveScreenshot(this, "clickRightColumn");

        return this;
    }

    public myCoursesPage clickCourseMentor() {
        driver.switchTo().frame(0);
        driver.switchTo().frame(1);
        getByXpath(myXpath.CoursesPage_CourseMentor).click();
        switchToDefaultFrame();
        wguPowerPointFileUtils.saveScreenshot(this, "clickCourseMentor");

        return this;
    }

    public myCoursesPage clickCourseAnnouncements() {
        driver.switchTo().frame(0);
        driver.switchTo().frame(1);
        getByXpath(myXpath.CoursesPage_CourseAnnouncements).click();
        switchToDefaultFrame();
        wguPowerPointFileUtils.saveScreenshot(this, "clickCourseAnnouncements");

        return this;
    }

    public myCoursesPage clickCourseTips() {
        driver.switchTo().frame(0);
        driver.switchTo().frame(1);
        getByXpath(myXpath.CoursesPage_CourseTips).click();
        switchToDefaultFrame();
        wguPowerPointFileUtils.saveScreenshot(this, "clickCourseTips");

        return this;
    }

    public myCoursesPage clickCourseSearch() {
        driver.switchTo().frame(0);
        driver.switchTo().frame(1);
        getByXpath(myXpath.CoursesPage_CourseSearch).click();
        switchToDefaultFrame();
        wguPowerPointFileUtils.saveScreenshot(this, "clickCourseSearch");

        return this;
    }

    public myCoursesPage clickCourseChatter() {
        driver.switchTo().frame(0);
        driver.switchTo().frame(1);
        getByXpath(myXpath.CoursesPage_CourseChatter).click();
        switchToDefaultFrame();
        wguPowerPointFileUtils.saveScreenshot(this, "clickCourseChatter");

        return this;
    }

    public myCoursesPage ExercisePage(boolean cascade) {
        General.Debug("\nmyCoursesPage::ExercisePage");
        load();

        if (cascade) {
            new myAllNotesPage(this).ExercisePage(true);
            new myAssessmentPage(this).ExercisePage(true);
//            new myCourseAnnouncementsPage(this).ExercisePage(true);
//            new myCourseChatterPage(this).ExercisePage(true);
            new myCourseMaterialPage(this).ExercisePage(true);
//            new myCourseMentorPage(this).ExercisePage(true);
//            new myCourseTipsPage(this).ExercisePage(true);
            new myLearningResourcesPage(this).ExercisePage(true);
//            new myCourseSearchPage(this).ExercisePage(true);

            return this;
        }

        clickLeftColumn();
        clickRightColumn();
        clickLeftColumn();
        clickRightColumn();

        clickCourseMaterial();
        clickLearningResources();
        clickAssessment();
        clickAllNotes();
        clickCourseMentor();
//        clickCourseAnnouncements();
//        clickCourseTips();
//        clickCourseSearch();
//        clickCourseChatter();

        return this;
    }
}


package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;
import org.openqa.selenium.WebElement;

/*
 * Created by timothy.hallbeck on 2/7/2015.
 */

public class mySchedulePage extends myHomePage {
    public mySchedulePage() {
        this(null);
    }
    public mySchedulePage(WebPage existingPage) {
        super(existingPage);
    }
    public mySchedulePage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public mySchedulePage load() {
        super.load();
        clickSchedulePage();

        wguPowerPointFileUtils.saveScreenshot(this, "mySchedulePage");
        wguPowerPointFileUtils.addBulletPoint("load()");

        return this;
    }

    // these 2 >> << have disappeared from google schedule as of 08/18/2015
/*    public mySchedulePage clickBackOneWeek() {
        getByXpath("//*[@id='navBack:2']/div").click();
        wguPowerPointFileUtils.saveScreenshot(this, "after clickBackOneWeek");

        return this;
    }

    public mySchedulePage clickForwardOneWeek() {
        getByXpath("//*[@id='navForward:2']/div").click();
        wguPowerPointFileUtils.saveScreenshot(this, "after clickForwardOneWeek");

        return this;
    }
*/
    public mySchedulePage clickDayButton() {
        getByXpath("//*[@id='topRightNavigation']/div[1]/div[2]/div/div/div/div[2]").click();
        wguPowerPointFileUtils.saveScreenshot(this, "after clickDayButton");

        return this;
    }

    public mySchedulePage clickWeekButton() {
        getByXpath("//*[@id='topRightNavigation']/div[1]/div[3]/div/div/div/div[2]").click();
        wguPowerPointFileUtils.saveScreenshot(this, "after clickWeekButton");

        return this;
    }

    public mySchedulePage clickMonthButton() {
        getByXpath("//*[@id='topRightNavigation']/div[1]/div[4]/div/div/div/div[2]").click();
        wguPowerPointFileUtils.saveScreenshot(this, "after clickMonthButton");

        return this;
    }

    public mySchedulePage click4DaysButton() {
        getByXpath("//*[@id='topRightNavigation']/div[1]/div[5]/div/div/div/div[2]").click();
        wguPowerPointFileUtils.saveScreenshot(this, "after click4DaysButton");

        return this;
    }

    public mySchedulePage clickAgendaButton() {
        getByXpath("//*[@id='topRightNavigation']/div[1]/div[6]/div/div/div/div[2]").click();
        wguPowerPointFileUtils.saveScreenshot(this, "after clickAgendaButton");

        return this;
    }

    public mySchedulePage clickPageDown() {
        WebElement element = getByXpath("//*[@id='lvHeader']/div[1]/img[2]", true);
        if (element != null)
            element.click();
        wguPowerPointFileUtils.saveScreenshot(this, "after clickPageDown");

        return this;
    }

    public mySchedulePage clickPageUp() {
        WebElement element = getByXpath("//*[@id='lvHeader']/div[1]/img[1]", true);
        if (element != null)
            element.click();
        wguPowerPointFileUtils.saveScreenshot(this, "after clickPageUp");

        return this;
    }

    public mySchedulePage clickSettings() {
        getByXpath("//*[@id='mg-settings']/div").click();
        wguPowerPointFileUtils.saveScreenshot(this, "after clickSettings");

        return this;
    }

    public mySchedulePage clickSettingsComfortable() {
        clickSettings();
        getByXpath("//*[@id=':h']/div").click();
        wguPowerPointFileUtils.saveScreenshot(this, "after clickSettingsComfortable");

        return this;
    }

    public mySchedulePage clickSettingsCozy() {
        clickSettings();
        getByXpath("//*[@id=':i']/div").click();
        wguPowerPointFileUtils.saveScreenshot(this, "after clickSettingsCozy");

        return this;
    }

    public mySchedulePage clickSettingsCompact() {
        clickSettings();
        getByXpath("//*[@id=':j']/div").click();
        wguPowerPointFileUtils.saveScreenshot(this, "after clickSettingsCompact");

        return this;
    }

    public mySchedulePage createCalendarEntry(String text) {
        getByXpath("//*[@id='sidebar']/div/div[2]/div/div/div/div[2]").click();
        enterTextHitReturn(text);
        driver.switchTo().defaultContent();
        wguPowerPointFileUtils.saveScreenshot(this, "after createCalendarEntry");

        return this;
    }

    // Google change, taking out for now
    public mySchedulePage createLunchDateToday() {
        clickDayButton();
//        createCalendarEntry("lunch 12pm today with Bob");
        wguPowerPointFileUtils.saveScreenshot(this, "after createLunchDateToday");

        return this;
    }

    public mySchedulePage createDinnerDateToday() {
        clickDayButton();
//        createCalendarEntry("dinner 5pm today with Tom");
        wguPowerPointFileUtils.saveScreenshot(this, "after createDinnerDateToday");

        return this;
    }

    public mySchedulePage deleteDateToday() {
        clickDayButton();
        getByXpath("//*[@id='tgCol0']/div/div[1]/dl/dt").click();
        getByLinkText("Delete").click();
        wguPowerPointFileUtils.saveScreenshot(this, "after deleteDateToday");

        return this;
    }

    public mySchedulePage ExercisePage(boolean cascade) {
        General.Debug("\nmySchedulePage::ExercisePage(" + cascade + ")");
        load();

        createLunchDateToday();
        createDinnerDateToday();

//        clickBackOneWeek();
//        clickForwardOneWeek();
        clickDayButton();
        clickWeekButton();
        clickMonthButton();
        click4DaysButton();
//*[@id="ai-42986"]/button/span
//*[@id="ai-43445"]/button/span
        clickAgendaButton();
        load();
        clickPageDown();
        load();
        clickPageUp();

//        clickSettingsComfortable();
//        clickSettingsCozy();
//        clickSettingsCompact();

        return this;
    }
}









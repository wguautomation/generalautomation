
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;

/*
 * Created by timothy.hallbeck on 2/7/2015.
 */

public class myEmailPage extends myHomePage {
    public myEmailPage() {
        this(null);
    }
    public myEmailPage(WebPage existingPage) {
        super(existingPage);
    }
    public myEmailPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myEmailPage load() {
        super.load();
        clickEmailPage();

        wguPowerPointFileUtils.saveScreenshot(this, "myEmailPage");
        wguPowerPointFileUtils.addBulletPoint("load()");

        return this;
    }

    public myEmailPage selectMail() {
        getByXpath("//*[@class='akh J-J5-Ji J-JN-I']").click();
        getByXpath("//div[contains(.,'Mail')]").click();
//        getByXpath("//*[@class=\"J-N ake\"]").click();
        return this;
    }

    public myEmailPage selectContacts() {
        getByXpath("//*[@class='akh J-J5-Ji J-JN-I']").click();
        getByXpath("//div[contains(.,'Contacts')]").click();
        return this;
    }

    public myEmailPage selectTasks() {
        getByXpath("//*[@class='akh J-J5-Ji J-JN-I']").click();
        getByXpath("//div[contains(.,'Tasks')]").click();
        return this;
    }

    public myEmailPage clickInbox() {
        getByXpath("//*[@href='https://mail.google.com/mail/u/0/?tab=cm&account_id=" + loginType.getUsername() + "@wgu.edu#inbox']").click();
        return this;
    }

    public myEmailPage clickStarred() {
        getByXpath("//*[@href='https://mail.google.com/mail/u/0/?tab=cm&account_id=" + loginType.getUsername() + "@wgu.edu#starred']").click();
        return this;
    }

    public myEmailPage clickImportant() {
        getByXpath("//*[@href='https://mail.google.com/mail/u/0/?tab=cm&account_id=" + loginType.getUsername() + "@wgu.edu#imp']").click();
        return this;
    }

    public myEmailPage clickSentMail() {
        getByXpath("//*[@href='https://mail.google.com/mail/u/0/?tab=cm&account_id=" + loginType.getUsername() + "@wgu.edu#sent']").click();
        return this;
    }

    public myEmailPage clickDrafts() {
        getByXpath("//*[@href='https://mail.google.com/mail/u/0/?tab=cm&account_id=" + loginType.getUsername() + "@wgu.edu#drafts']").click();
        return this;
    }

    public myEmailPage clickFollowup() {
        getByXpath("//*[@href='https://mail.google.com/mail/u/0/?tab=cm&account_id=" + loginType.getUsername() + "@wgu.edu#label/Follow+up']").click();
        return this;
    }

    public myEmailPage clickMigrated() {
        getByXpath("//*[@href='https://mail.google.com/mail/u/0/?tab=cm&account_id=" + loginType.getUsername() + "@wgu.edu#label/Migrated']").click();
        return this;
    }

    public myEmailPage clickCompose() {
        getByXpath("//*[@class='T-I J-J5-Ji T-I-KE L3']").click();
        return this;
    }

    public myEmailPage typeSendTo(String text) {
        getByXpath("//*[@name='to']").click();
        getByXpath("//*[@name='to']").sendKeys(text);
        return this;
    }

    public myEmailPage typeSubject(String text) {
        getByXpath("//*[@name='subjectbox']").click();
        getByXpath("//*[@name='subjectbox']").sendKeys(text);
        return this;
    }

    public myEmailPage typeMessage(String text) {
        getByXpath("//*[@class='Am Al editable LW-avf']").click();
        getByXpath("//*[@class='Am Al editable LW-avf']").sendKeys(text);
        return this;
    }

    public myEmailPage clickSend() {
        getByXpath("//*[@class='T-I J-J5-Ji aoO T-I-atl L3']").click();
        Sleep(2000);
        return this;
    }

    public myEmailPage SendMailToSelf() {
        clickCompose();
        typeSendTo("Cortney Poindexter <cpoindexter@wgu.edu>");
        typeSubject("Hello There");
        typeMessage("I am a Nigerian prince, and I need your help.");
        clickSend();
        clickBrowserRefreshButton();
        return this;
    }

    public myEmailPage DeleteMailFromSelf() {
        clickInbox();
        Sleep(1000);
        getByXpath("//span[contains(.,'Nigerian')]").click();
        Sleep(1000);
        getByXpath("//*[@id=':5']/div[3]/div[1]/div/div[2]/div[3]/div/div").click();
        return this;
    }

    public myEmailPage ExercisePage(boolean cascade) {
        General.Debug("\nmyEmailPage::ExercisePage(" + cascade + ")");
        load();
        if (cascade) {
//            new myEmailInboxPage(this).ExercisePage(true);
//            new myEmailSentPage(this).ExercisePage(true);
//            new myEmailDraftsPage(this).ExercisePage(true);
//            new myEmailContactsPage(this).ExercisePage(true);
//            new myEmailTasksPage(this).ExercisePage(true);
            return this;
        }

        selectMail();
        clickInbox();
        clickStarred();
        clickImportant();
        clickSentMail();
        clickDrafts();
        clickFollowup();
        clickMigrated();

        selectContacts();
        selectTasks();

        selectMail();
        SendMailToSelf();
//        DeleteMailFromSelf();
        return this;
    }
}















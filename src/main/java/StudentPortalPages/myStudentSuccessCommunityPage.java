
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 2/7/2015.
 */

public class myStudentSuccessCommunityPage extends myLearningResourcesPage {
    public myStudentSuccessCommunityPage() {
        this(null);
    }
    public myStudentSuccessCommunityPage(WebPage existingPage) {
        super(existingPage);
    }
    public myStudentSuccessCommunityPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myStudentSuccessCommunityPage load() {
        super.load();
        clickStudentSuccessCommunity();
        return this;
    }

    public myStudentSuccessCommunityPage ExercisePage(boolean cascade) {
        General.Debug("\nmyStudentSuccessCommunityPage::ExercisePage(" + cascade + ")");
        load();
        return this;
    }
}

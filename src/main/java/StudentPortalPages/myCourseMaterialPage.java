
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 2/7/2015.
 */

public class myCourseMaterialPage extends myCoursesPage {
    public myCourseMaterialPage() {
        this(null);
    }
    public myCourseMaterialPage(WebPage existingPage) {
        super(existingPage);
    }
    public myCourseMaterialPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myCourseMaterialPage load() {
        super.load();
        clickCourseMaterial();

        return this;
    }

    public myCourseMaterialPage ExercisePage(boolean cascade) {
        General.Debug("\nmyCourseMaterialPage::ExercisePage(" + cascade + ")");
        load();

        return this;
    }
}

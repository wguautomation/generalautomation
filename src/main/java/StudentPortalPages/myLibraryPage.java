
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 7/31/15.
 */

public class myLibraryPage extends mySuccessCentersPage {

    public myLibraryPage() {
        this(null);
    }
    public myLibraryPage(WebPage existingPage) {
        super(existingPage);
    }
    public myLibraryPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myLibraryPage load() {
        super.load();
        clickLibrary();

        return this;
    }

    public myLibraryPage verifyHome() {
        getByXpath(myXpath.LibraryPage_HomeTab).click();
        Sleep(1000);
        getByXpath(myXpath.LibraryPage_HomeLoaded);

        return this;
    }

    public myLibraryPage verifyHowDoI() {
        getByXpath(myXpath.LibraryPage_HowDoITab).click();
        Sleep(1000);
        getByXpath(myXpath.LibraryPage_HowDoILoaded);

        return this;
    }

    public myLibraryPage verifyEBooks() {
        getByXpath(myXpath.LibraryPage_EBooksTab).click();
        Sleep(1000);
        getByXpath(myXpath.LibraryPage_EBooksLoaded);

        return this;
    }

    public myLibraryPage verifyEReserves() {
        getByXpathAndActivate(myXpath.LibraryPage_EReservesTab, "verifyEReserves");
        Sleep(1000);
        getByXpath(myXpath.LibraryPage_EReservesLoaded);

        return this;
    }

    public myLibraryPage verifyQuickLinks() {
        getByXpath(myXpath.LibraryPage_QuickLinksTab).click();
        Sleep(1000);
        getByXpath(myXpath.LibraryPage_QuickLinksLoaded);

        return this;
    }

    public myLibraryPage verifyServices() {
        getByXpath(myXpath.LibraryPage_ServicesTab).click();
        Sleep(1000);
        getByXpath(myXpath.LibraryPage_ServicesLoaded);

        return this;
    }

    public myLibraryPage verifyGuides() {
        getByXpath(myXpath.LibraryPage_GuidesTab).click();
        Sleep(1000);
        getByXpath(myXpath.LibraryPage_GuidesTabLoaded);

        return this;
    }

    public myLibraryPage verifyAskUs() {
        getByXpath(myXpath.LibraryPage_AskUsTab).click();
        Sleep(2000);
        getByXpath(myXpath.LibraryPage_AskUsLoaded);

        return this;
    }

    public myLibraryPage verifySearch() {
        getByXpath(myXpath.LibraryPage_HomeTab).click();
        typeByXpath(myXpath.LibraryPage_SearchBox, "owl");
        getByXpath(myXpath.LibraryPage_SearchButton).click();
        Sleep(2000);
        closeTab();

        return this;
    }

    public myLibraryPage ExercisePage(boolean cascade) {
// no longer part of my.wgu, separately managed, not mapped
        General.Debug("\nmyLibraryPage::ExercisePage(" + cascade + ")");
        return this;
/*        load();

        verifyHome();
        verifyHowDoI();
        verifyEBooks();
        verifyEReserves();
        load();

        verifyQuickLinks();
        verifyServices();
        verifyGuides();
        verifyAskUs();
        verifySearch();

        return this;
*/    }
}

package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;

/*
 * Created by timothy.hallbeck on 8/29/2016.
 */

public class myMathCenterPage extends mySuccessCentersPage {
    public myMathCenterPage() {
        super();
    }
    public myMathCenterPage(LoginType loginType) {
        super(loginType);
    }
    public myMathCenterPage(WebPage existingPage) {
        super(existingPage);
    }
    public myMathCenterPage(WebPage existingPage, LoginType loginType, Browser browser) {
        super(existingPage, loginType, browser);
    }

    public myMathCenterPage load() {
        General.Debug("myMathCenterPage::load()");
        super.load();
        clickMathCenter();
        Sleep(2000);
        // Ping does not sso, have to click on blue box temporarily
        if (getByXpath("/html/body/div/div[2]/div[2]/a", true) != null)
            getByXpath("/html/body/div/div[2]/div[2]/a").click();
        verifyText("How Math Mentors Help Students");
        wguPowerPointFileUtils.saveScreenshot(this, "myMathCenterPage::load()");

        return this;
    }

    public myMathCenterPage clickWebinarSessions() {
        General.Debug("clickWebinarSessions::load()");
        getByXpath("//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[1]/div/div[2]/div/div[1]/div[1]/span/a").click();

        wguPowerPointFileUtils.saveScreenshot(this, "myMathCenterPage::clickWebinarSessions()");

        return this;
    }

    public myMathCenterPage clickMentorAppointments() {
        General.Debug("clickMentorAppointments::load()");
        getByXpathAndActivate("//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[1]/div/div[2]/div/div[2]/p[1]/span/a");

        verifyText("Select a Resource");
        wguPowerPointFileUtils.saveScreenshot(this, "myMathCenterPage::clickMentorAppointments()");

        return this;
    }

    public myMathCenterPage clickVideos() {
        General.Debug("myMathCenterPage::clickVideos()");
        getByXpath("//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[1]/div/div[2]/div/div[2]/p[2]/span/a").click();

        verifyText("Content Topics");
        wguPowerPointFileUtils.saveScreenshot(this, "myMathCenterPage::clickVideos()");

        return this;
    }

    public myMathCenterPage clickEdReady() {
        General.Debug("myMathCenterPage::clickEdReady()");
        getByXpathAndActivate("//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[2]/div/div[1]/b/a");

        verifyText("What do you want to be");
        wguPowerPointFileUtils.saveScreenshot(this, "myMathCenterPage::clickEdReady()");

        return this;
    }

    public myMathCenterPage clickEdReadyForMath() {
        General.Debug("myMathCenterPage::clickEdReadyForMath()");
        getByXpathAndActivate("//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[3]/div/div[1]/a");

        verifyText("EdReady will help you");
        wguPowerPointFileUtils.saveScreenshot(this, "myMathCenterPage::clickEdReadyForMath()");

        return this;
    }

    public myMathCenterPage clickLiveAppointments() {
        General.Debug("myMathCenterPage::clickLiveAppointments()");
        getByXpath("//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[3]/div/div[3]/a").click();

        verifyText("Live Appointments");
        wguPowerPointFileUtils.saveScreenshot(this, "myMathCenterPage::clickLiveAppointments()");

        return this;
    }

    public myMathCenterPage clickVideoAppointments() {
        General.Debug("myMathCenterPage::clickVideoAppointments()");
        getByXpath("//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[3]/div/div[4]/a").click();

        verifyText("Video Appointments");
        wguPowerPointFileUtils.saveScreenshot(this, "myMathCenterPage::clickVideoAppointments()");

        return this;
    }

    public myMathCenterPage clickLiveEvents() {
        General.Debug("myMathCenterPage::clickLiveEvents()");
        getByXpath("//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[3]/div/div[5]/a").click();

        verifyText("Our Live Event Webinars");
        wguPowerPointFileUtils.saveScreenshot(this, "myMathCenterPage::clickLiveEvents()");

        return this;
    }

    public myMathCenterPage clickBasicSkillsExam() {
        General.Debug("myMathCenterPage::clickBasicSkillsExam()");
        getByXpath("//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[3]/div/div[7]/a").click();

        verifyText("Basic Skills Exam");
        wguPowerPointFileUtils.saveScreenshot(this, "myMathCenterPage::clickBasicSkillsExam()");

        return this;
    }

    public myMathCenterPage clickMeetOurMentors() {
        General.Debug("myMathCenterPage::clickMeetOurMentors()");
        getByXpath("//*[@id='sites-canvas-main-content']/div/table/tbody/tr/td[3]/div/div[9]/a").click();

        verifyText("Meet our Mentors");
        wguPowerPointFileUtils.saveScreenshot(this, "myMathCenterPage::clickMeetOurMentors()");

        return this;
    }

    public myMathCenterPage ExercisePage(boolean cascade) {
        // This page is being changed largely without CAB action, removing from regression
        if (true)
            return this;

        General.Debug("\nmyMathCenterPage::ExercisePage(" + cascade + ")");
        load();
        clickWebinarSessions();

        load();
        clickMentorAppointments();

        load();
        clickVideos();

        load();
        clickEdReady();

        load();
        clickEdReadyForMath();

        load();
        clickLiveAppointments();

        load();
        clickVideoAppointments();

        load();
        clickLiveEvents();

        load();
        clickBasicSkillsExam();

        load();
        clickMeetOurMentors();

        return this;
    }
}





















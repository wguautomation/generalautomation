
package StudentPortalPages;

import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;

/*
 * Created by timothy.hallbeck on 1/16/2015.
 */
public class myLoginPage extends WebPage {

    public myLoginPage() {
        super();
    }

    public myLoginPage load() {
        get(loginType.getStartingUrl());
        wguPowerPointFileUtils.saveScreenshot(this, "myLoginPage");
        wguPowerPointFileUtils.addBulletPoint("load()");
        return this;
    }

    public myLoginPage ExercisePage(boolean cascade) {
        General.Debug("\nmyLoginPage::ExercisePage(" + cascade + ")");
        return this;
    }
}

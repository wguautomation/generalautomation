
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;

/*
 * Created by timothy.hallbeck on 2/7/2015.
 */

public class myNewsPage extends myHomePage {
    public myNewsPage() {
        this(null);
    }
    public myNewsPage(WebPage existingPage) {
        super(existingPage);
    }
    public myNewsPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myNewsPage load() {
        super.load();
        clickNewsPage();

        wguPowerPointFileUtils.saveScreenshot(this, "myNewsPage");
        wguPowerPointFileUtils.addBulletPoint("load()");

        return this;
    }

    public myNewsPage ExercisePage(boolean cascade) {
        General.Debug("\nmyNewsPage::ExercisePage(" + cascade + ")");
        load();
        if (cascade) {
            return this;
        }

        return this;
    }
}







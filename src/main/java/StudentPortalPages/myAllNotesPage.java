
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;

/*
 * Created by timothy.hallbeck on 2/7/2015.
 */

public class myAllNotesPage extends myCoursesPage {
    public myAllNotesPage() {
        this(null);
    }
    public myAllNotesPage(WebPage existingPage) {
        super(existingPage);
    }
    public myAllNotesPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myAllNotesPage load() {
        super.load();
        clickAllNotes();
        showPageLoadTimes("myAllNotesPage::load");

        return this;
    }

    public myAllNotesPage sortByModifiedRecent() {
        driver.switchTo().frame(0);
        selectByXpathByText(myXpath.NotesPage_SortBy, "Modified (Most Recent)");
        driver.switchTo().defaultContent();
        wguPowerPointFileUtils.saveScreenshot(this, "sortByModifiedRecent");

        return this;
    }

    public myAllNotesPage sortByModifiedOld() {
        driver.switchTo().frame(0);
        selectByXpathByText(myXpath.NotesPage_SortBy, "Modified (Oldest)");
        driver.switchTo().defaultContent();
        wguPowerPointFileUtils.saveScreenshot(this, "sortByModifiedOld");

        return this;
    }

    public myAllNotesPage sortByTitleAsc() {
        driver.switchTo().frame(0);
        selectByXpathByText(myXpath.NotesPage_SortBy, "Activity Title (A-Z)");
        driver.switchTo().defaultContent();
        wguPowerPointFileUtils.saveScreenshot(this, "sortByTitleAsc");

        return this;
    }

    public myAllNotesPage sortByTitleDesc() {
        driver.switchTo().frame(0);
        selectByXpathByText(myXpath.NotesPage_SortBy, "Activity Title (Z-A)");
        driver.switchTo().defaultContent();
        wguPowerPointFileUtils.saveScreenshot(this, "sortByTitleDesc");

        return this;
    }

    public myAllNotesPage ExercisePage(boolean cascade) {
        General.Debug("\nmyAllNotesPage::ExercisePage");
        load();

        sortByModifiedOld();
        sortByTitleAsc();
        sortByTitleDesc();
        sortByModifiedRecent();

        return this;
    }
}


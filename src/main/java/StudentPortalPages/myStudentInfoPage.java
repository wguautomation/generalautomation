
package StudentPortalPages;

import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 11/17/2015.
 */

public class myStudentInfoPage extends myHomePage {

    myStudentInfoPage() {
        super();
    }
    myStudentInfoPage(WebPage existingPage) {
        super(existingPage);
    }

    public myStudentInfoPage load() {
        super.load();
        clickStudentInfoLink();
        waitForControl(myXpath.HomePage_WGUIcon);

        return this;
    }

    public myStudentInfoPage checkDataItems() {
        if (!doDataValidation())
            return this;

        Sleep(2000);

        General.Debug("The following verification checks that items are present, but not their position or attributes on the page.", true);
        General.Debug("Verifing Student ID label is present", true);
        getByXpath(myXpath.AboutMePage_StudentIdLabel);

        General.Debug("Verifying Primary phone label is present", true);
        getByXpath(myXpath.AboutMePage_PrimaryPhoneLabel);

        if (General.getPerson(loginType).getPrimaryPhone() != null) {
            General.Debug("Verifying Primary phone data is present", true);
            getByXpath(myXpath.AboutMePage_GenericField1 +
                    "'(" + General.getPerson(loginType).getPrimaryPhone().substring(0, 3) + ")" +
                    " " + General.getPerson(loginType).getPrimaryPhone().substring(3, 6) + "-" +
                    General.getPerson(loginType).getPrimaryPhone().substring(6, 10) + "')]");
        }

        General.Debug("Verifying Student ID data is present", true);
        getByXpath(myXpath.AboutMePage_GenericField2 + "'" + General.getPerson(loginType).getStudentId() + "')]");

        General.Debug("Verifying WGU email data is present", true);
        getByXpath(myXpath.AboutMePage_GenericField1 + "'" + General.getPerson(loginType).getPreferredEmail() + "')]");

        if (General.getPerson(loginType).getStreetLine1() != null) {
            General.Debug("Verifying Address line 1 data is present", true);
            getByXpath(myXpath.AboutMePage_GenericField1 + "'" + General.getPerson(loginType).getStreetLine1() + "')]");
        }

        if (General.getPerson(loginType).getStreetLine2() != null) {
            General.Debug("Verifying Address line 2 data is present", true);
            getByXpath(myXpath.AboutMePage_GenericField1 + "'" + General.getPerson(loginType).getStreetLine2() + "')]");
        }

        if (General.getPerson(loginType).getCity() != null) {
            General.Debug("Verifying City data is present", true);
            getByXpath(myXpath.AboutMePage_GenericField1 + "'" + General.getPerson(loginType).getCity() + "')]");
        }

        if (General.getPerson(loginType).getState() != null) {
            General.Debug("Verifying State data is present", true);
            getByXpath(myXpath.AboutMePage_GenericField1 + "'" + General.getPerson(loginType).getState() + "')]");
        }

        if (General.getPerson(loginType).getZipCode() != null) {
            General.Debug("Verifying Zip code data is present", true);
            getByXpath(myXpath.AboutMePage_GenericField3 + "'" + General.getPerson(loginType).getZipCode() + "')]");
        }

        return this;
    }

    public myStudentInfoPage ExercisePage(boolean cascade) {
        General.Debug("\nmyStudentInfoPage::ExercisePage(" + cascade + ")");
        load();
        checkDataItems();

        load();
        clickSocialMedia();

        load();
        clickPrivacySettings();

        return this;
    }
}

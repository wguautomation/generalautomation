
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 2/7/2015.
 */

public class myCourseAnnouncementsPage extends myCoursesPage {
    public myCourseAnnouncementsPage() {
        this(null);
    }
    public myCourseAnnouncementsPage(WebPage existingPage) {
        super(existingPage);
    }
    public myCourseAnnouncementsPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myCourseAnnouncementsPage load() {
        super.load();
        clickCourseAnnouncements();
        
        return this;
    }

    public myCourseAnnouncementsPage ExercisePage(boolean cascade) {
        General.Debug("\nmyCourseAnnouncementsPage::ExercisePage(" + cascade + ")");
        load();

        return this;
    }
}


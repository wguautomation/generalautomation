
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;
/*
 * Created by timothy.hallbeck on 2/29/2016.
 */

public class myAssessmentServicesObjectivePage extends myStudentSupportPage {

    public myAssessmentServicesObjectivePage() {
        this(null);
    }
    public myAssessmentServicesObjectivePage(WebPage existingPage) {
        super(existingPage);
    }
    public myAssessmentServicesObjectivePage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myAssessmentServicesObjectivePage load() {
        super.load();
        clickAssessmentServicesObjectives();
        wguPowerPointFileUtils.saveScreenshot(this, "myAssessmentServicesObjectivePage");
        wguPowerPointFileUtils.addBulletPoint("load()");

        return this;
    }

    public myAssessmentServicesObjectivePage clickScheduling() {
        getByXpathAndActivate(myXpath.AssessmentServicesObjectivePage_Scheduling, "clickScheduling");
        getByXpathVerifyTextPresent(myXpath.StudentSupportPage_GenericText, "Scheduling and Rescheduling");

        return this;
    }

    public myAssessmentServicesObjectivePage clickProctoring() {
        getByXpathAndActivate(myXpath.AssessmentServicesObjectivePage_Proctoring, "clickProctoring");
        getByXpathVerifyTextPresent(myXpath.StudentSupportPage_GenericText, "Online Proctoring");

        return this;
    }

    public myAssessmentServicesObjectivePage clickPolicies() {
        getByXpathAndActivate(myXpath.AssessmentServicesObjectivePage_Policies, "clickPolicies");
        getByXpathVerifyTextPresent(myXpath.StudentSupportPage_GenericText, "Assessment Policies");

        return this;
    }

    public myAssessmentServicesObjectivePage clickWebcam() {
        getByXpathAndActivate(myXpath.AssessmentServicesObjectivePage_Webcam, "clickWebcam");
        getByXpathVerifyTextPresent(myXpath.StudentSupportPage_GenericText, "Webcam");

        return this;
    }

    public myAssessmentServicesObjectivePage clickReplacement() {
        if (loginType.isProd())
            getByXpathAndActivate(myXpath.AssessmentServicesObjectivePage_Replacement, "clickReplacement");

        return this;
    }

    public myAssessmentServicesObjectivePage clickWhiteboard() {
        if (loginType.isProd()) {
            scrollIntoView(myXpath.AssessmentServicesObjectivePage_Whiteboard);
            getByXpathAndActivate(myXpath.AssessmentServicesObjectivePage_Whiteboard, "clickWhiteboard");
        }

        return this;
    }

    public myAssessmentServicesObjectivePage clickCoverSheet() {
        if (loginType.isProd()) {
            scrollIntoView(myXpath.AssessmentServicesObjectivePage_CoverSheet);
            getByXpath(myXpath.AssessmentServicesObjectivePage_CoverSheet).click();
            Sleep(2000);
        }
        return this;
    }

    public myAssessmentServicesObjectivePage ExercisePage(boolean cascade) {
        General.Debug("\nmyAssessmentServicesObjectivePage::ExercisePage(" + cascade + ")");
        load();
        clickScheduling();

        load();
        clickProctoring();

        load();
        clickPolicies();

        if (loginType.isProd()) {
            load();
            clickWebcam();

            load();
            clickReplacement();

            load();
            clickWhiteboard();

            load();
            clickCoverSheet();
        }

        return this;
    }

}
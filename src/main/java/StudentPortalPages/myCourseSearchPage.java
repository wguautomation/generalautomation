
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
import org.openqa.selenium.Keys;

/*
 * Created by timothy.hallbeck on 2/7/2015.
 */

public class myCourseSearchPage extends myCoursesPage {
    public myCourseSearchPage() {
        this(null);
    }
    public myCourseSearchPage(WebPage existingPage) {
        super(existingPage);
    }
    public myCourseSearchPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myCourseSearchPage load() {
        super.load();
        clickCourseSearch();

        return this;
    }

    public myCourseSearchPage doCourseSearch(String text) {
        driver.switchTo().frame(0);
        driver.switchTo().frame(1);
        getByXpath("/html/body/div/div[3]/div[2]/div/div/input").sendKeys(text + Keys.RETURN);
        Sleep(3000);
        driver.switchTo().window(GetTopWindow());
        getByXpath("/html/body/div/div[2]/div[5]/button").click();
        Sleep(1000);
        driver.switchTo().window(GetTopWindow());
        switchToDefaultFrame();

        return this;
    }

    public myCourseSearchPage ExercisePage(boolean cascade) {
        General.Debug("\nmyCourseSearchPage::ExercisePage(" + cascade + ")");
        load();
        doCourseSearch("hello");

        return this;
    }
}


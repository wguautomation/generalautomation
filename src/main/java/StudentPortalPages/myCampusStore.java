
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
/*
 * Created by timothy.hallbeck on 7/31/15.
 */

public class myCampusStore extends myHomePage {

    public myCampusStore() {
        this(null);
    }
    public myCampusStore(WebPage existingPage) {
        super(existingPage);
    }
    public myCampusStore(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myCampusStore load() {
        login();
        get(myXpath.CampusStorePage_StartingUrl);

        return this;
    }


    public myCampusStore ExercisePage(boolean cascade) {
        General.Debug("\nmyCampusStore::ExercisePage(" + cascade + ")");
        load();

        if (cascade) {

            return this;
        }

        return this;
    }
}

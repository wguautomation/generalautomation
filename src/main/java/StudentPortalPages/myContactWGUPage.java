
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;

/*
 * Created by timothy.hallbeck on 3/1/2016.
 */

public class myContactWGUPage extends myStudentSupportPage {

    public myContactWGUPage() {
        this(null);
    }

    public myContactWGUPage(WebPage existingPage) {
        super(existingPage);
    }

    public myContactWGUPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myContactWGUPage load() {
        super.load();
        clickContactWGU();
        wguPowerPointFileUtils.saveScreenshot(this, "myContactWGUPage");
        wguPowerPointFileUtils.addBulletPoint("load()");

        return this;
    }

    public myContactWGUPage ExercisePage(boolean cascade) {
        General.Debug("\nmyContactWGUPage::ExercisePage(" + cascade + ")");
        load();

        if (cascade) {
        } else {
        }

        return this;
    }
}
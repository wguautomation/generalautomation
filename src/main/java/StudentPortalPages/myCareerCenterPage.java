
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 2/10/2016.
 */

public class myCareerCenterPage extends mySuccessCentersPage {
    public myCareerCenterPage() {
        this(null);
    }
    public myCareerCenterPage(WebPage existingPage) {
        super(existingPage);
    }
    public myCareerCenterPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myCareerCenterPage load() {
        super.load();
        clickCareerCenter();

        return this;
    }

    public myCareerCenterPage ExercisePage(boolean cascade) {
        General.Debug("\nmySuccessCentersPage::ExercisePage(" + cascade + ")");
        load();

        return this;
    }
}


package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;

/*
 * Created by timothy.hallbeck on 2/11/2016.
 */

public class myStudentLifePage extends myStudentSupportPage {

    public myStudentLifePage() {
        this(null);
    }

    public myStudentLifePage(WebPage existingPage) {
        super(existingPage);
    }

    public myStudentLifePage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myStudentLifePage load() {
        super.load();
        clickStudentLife();
        wguPowerPointFileUtils.saveScreenshot(this, "myStudentSupportPage");
        wguPowerPointFileUtils.addBulletPoint("load()");

        return this;
    }

    public myStudentLifePage clickNightOwlBlog() {
        getByXpathAndActivate(myXpath.StudentLifePage_NightOwlBlog, "Night Owl Blog");
        getByXpath("/html/body/div[2]/section[1]/a");

        return this;
    }

    public myStudentLifePage clickNightOwlArchives() {
        getByXpathAndActivate(myXpath.StudentLifePage_NightOwlArchive, "Night Owl Archives");
        getByXpath("/html/body/div[2]/div[2]/div/div/h1");

        return this;
    }

    public myStudentLifePage clickWGUNews() {
        getByXpathAndActivate(myXpath.StudentLifePage_WGUNews, "WGU News");
        getByXpath("//*[contains(text(), 'Newsroom')]");

        return this;
    }

    public myStudentLifePage clickWGUVideo() {
        getByXpathAndActivate(myXpath.StudentLifePage_WGUVideo, "WGU Video");
        getByXpath("//*[@id='contentHeader']");

        return this;
    }

    public myStudentLifePage clickCampusStore() {
        getByXpathAndActivate(myXpath.StudentLifePage_CampusStore, "Campus Store");
        Sleep(3000);
//        getByXpath("/html/body/div[1]/div[1]/div/main/article/div/div[2]/div/div/div/ul/li[1]/a[2]/h3");

        return this;
    }

    public myStudentLifePage clickSpecialDiscounts() {
        getByXpathAndActivate(myXpath.StudentLifePage_SpecialDiscounts, "Tech Deals");
        getByXpathVerifyTextPresent("/html/body/div/ng-view/div/div[2]/h2", "Technology and Software Deals");

        return this;
    }

    public myStudentLifePage clickReferAFriend() {
        getByXpathAndActivate(myXpath.StudentLifePage_ReferAFriend, "Refer A Friend");
        getByXpathVerifyTextPresent("//*[@id='main-content']/div/section/div/div/div/div/div[1]/div/div[1]/p/strong", "Refer your friends, family and");

        return this;
    }

    public myStudentLifePage clickWGUOrientation() {
        getByXpathAndActivate(myXpath.StudentLifePage_WGUOrientation, "WGU Orientation");
//        getByXpathVerifyTextPresent("", ""); // bug - goes to wrong course in CoS

        return this;
    }

    public myStudentLifePage clickWGUAlumni() {
        scrollIntoView(myXpath.StudentLifePage_WGUAlumni, true);
        getByXpathAndActivate(myXpath.StudentLifePage_WGUAlumni, "WGU Alumni");
        getByXpath("//*[@id='ContentLogo']");

        return this;
    }

    public myStudentLifePage clickPrevention() {
        scrollIntoView(myXpath.StudentLifePage_Prevention, true);
        getByXpathAndActivate(myXpath.StudentLifePage_Prevention, "Prevention");
        getByXpathVerifyTextPresent("/html/body/main/div/div/p[1]", "WGU is committed to meeting our legal training");

        return this;
    }

    public myStudentLifePage ExercisePage(boolean cascade) {
        General.Debug("\nmyStudentLifePage::ExercisePage(" + cascade + ")");
        load();

//        if (cascade) {
//            new myWritingCenterPage(this).ExercisePage(true);
//        } else {

        clickWGUAlumni();
        load();

        clickPrevention();
        load();

        clickNightOwlBlog();
        load();

        clickNightOwlArchives();
        load();

        clickWGUNews();
        load();

        clickWGUVideo();
        load();

        clickCampusStore();
        load();

//        clickSpecialDiscounts();
//        load();

        clickReferAFriend();
        load();

        clickWGUOrientation();
        load();
//        }

        return this;
    }
}

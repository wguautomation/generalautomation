
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;

/*
 * Created by timothy.hallbeck on 2/29/2016.
 */

public class myITServiceDeskPage extends myStudentSupportPage {
    public myITServiceDeskPage() {
        this(null);
    }
    public myITServiceDeskPage(WebPage existingPage) {
        super(existingPage);
    }
    public myITServiceDeskPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myITServiceDeskPage load() {
        super.load();
        clickITServiceDesk();

        wguPowerPointFileUtils.saveScreenshot(this, "myITServiceDeskPage");
        wguPowerPointFileUtils.addBulletPoint("load()");

        return this;
    }

    public myITServiceDeskPage ExercisePage(boolean cascade) {
        General.Debug("\nmyITServiceDeskPage::ExercisePage(" + cascade + ")");
        load();

        return this;
    }
}
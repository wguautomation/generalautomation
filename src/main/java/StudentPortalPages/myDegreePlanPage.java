
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/*
 * Created by timothy.hallbeck on 2/2/2015.
 */

public class myDegreePlanPage extends myHomePage {
    public myDegreePlanPage() {
        this(null);
    }
    public myDegreePlanPage(WebPage existingPage) {
        super(existingPage);
    }
    public myDegreePlanPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myDegreePlanPage load() {
        super.load();

        getByXpath(myXpath.HomePage_DegreePlanLink).click();
//        switchToFrame(0);
//        switchToFrame(0);
        driver.switchTo().defaultContent();
        getByXpath(myXpath.Generic_Header);
        driver.switchTo().defaultContent();
        getByXpath(myXpath.HomePage_WGUIcon);

        checkDataItems();
        wguPowerPointFileUtils.saveScreenshot(this, "myDegreePlanPage");
        wguPowerPointFileUtils.addBulletPoint("load()");

        return this;
    }

    public myDegreePlanPage checkDataItems() {
        checkPersonInfo();

        return this;
    }

    public List<String> getListOfCourses() {
        switchToFrame(0);
        List <WebElement> list = getListByXpath(myXpath.DegreePlanPage_CourseList);
        List <String> finalList = new ArrayList<String>();

        String text;
        WebElement element;
        int newline, separator;

        Iterator iterator = list.iterator();
        while(iterator.hasNext()) {
            element = (WebElement) iterator.next();
            text = element.getText();
            separator = text.indexOf(" - ");
            newline = text.indexOf("\n");

            if (separator != -1 && newline != -1 && separator + 3 < newline) {
                text = text.substring(separator + 3, newline);
                finalList.add(text);
                General.Debug(text);
            }
        }
        switchToDefaultFrame();

        return finalList;
    }

    public myDegreePlanPage ExercisePage(boolean cascade) {
        General.Debug("\nmyDegreePlanPage::ExercisePage(" + cascade + ")");
        load();

        switchToFrame(0);
        getByXpath(myXpath.DegreePlanPage_DefaultPlan);
        Sleep(10000);
        getByXpath(myXpath.DegreePlanPage_ScenarioCalculator).click();
        Sleep(2000);
        scrollUp(2);
        getByXpath(myXpath.DegreePlanPage_ScenarioCalculator).click();
        switchToDefaultFrame();
        scrollDown(8);

        switchToFrame(0);
        WebElement element = null;
        for (int i=20; i>0; i--) {
            element = getByXpath("//*[@id='ng-app']/body/div[1]/div[4]/div[4]/div/section[" + i + "]/header/div", true);
            if (element != null && element.isEnabled() && element.isDisplayed()) {
                element.click();
                break;
            }
        }
        switchToDefaultFrame();

        return this;
    }
}

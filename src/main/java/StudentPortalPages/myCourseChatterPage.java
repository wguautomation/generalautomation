
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 2/7/2015.
 */

public class myCourseChatterPage extends myCoursesPage {
    public myCourseChatterPage() {
        this(null);
    }
    public myCourseChatterPage(WebPage existingPage) {
        super(existingPage);
    }
    public myCourseChatterPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myCourseChatterPage load() {
        super.load();
        clickCourseChatter();

        return this;
    }

    public myCourseChatterPage clickExpandChatter() {
        driver.switchTo().frame(0);
        driver.switchTo().frame(1);
        getByXpath("//*[@id='j_id0:j_id76']/button").click();
        driver.switchTo().defaultContent();
        return this;
    }

    public myCourseChatterPage closeChatter() {
        Sleep(14000); // There is no wait function for
        driver.switchTo().window("Chatter - Google Chrome");
        return this;
    }

    public myCourseChatterPage ExercisePage(boolean cascade) {
        General.Debug("\nmyCourseChatterPage::ExercisePage(" + cascade + ")");
        load();
//        clickExpandChatter();
//        closeChatter();
        return this;
    }
}


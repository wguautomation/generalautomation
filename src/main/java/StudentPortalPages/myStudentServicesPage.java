
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguPowerPointFileUtils;

/*
 * Created by timothy.hallbeck on 2/23/2016.
 */

public class myStudentServicesPage extends myStudentSupportPage {
    public myStudentServicesPage() {
        this(null);
    }

    public myStudentServicesPage(WebPage existingPage) {
        super(existingPage);
    }

    public myStudentServicesPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myStudentServicesPage load() {
        super.load();
        clickStudentServicesLink();
        wguPowerPointFileUtils.saveScreenshot(this, "myStudentServicesPage");
        wguPowerPointFileUtils.addBulletPoint("load()");

        return this;
    }

    public myStudentServicesPage clickRequestIdCard() {
        getByXpathAndActivate(myXpath.StudentServicesPage_RequestIdCard, "Request ID Card");
        getByXpathVerifyTextPresent("//*[@id='body_content']/div/p[3]", "You will need a photo for your");
        wguPowerPointFileUtils.saveScreenshot(this, "clickRequestIdCard");

        return this;
    }

    // Has a delayThread then an autoclose, so can't use getByXpathAndActivate
    public myStudentServicesPage clickDownloadTranscript() {
        getByXpathAndActivate(myXpath.StudentServicesPage_DownloadTranscript, "clickDownloadTranscript");
        Sleep(3000);
        wguPowerPointFileUtils.saveScreenshot(this, "clickDownloadTranscript");

        return this;
    }

    public myStudentServicesPage clickRequestTranscript() {
        getByXpathAndActivate(myXpath.StudentServicesPage_RequestTranscript, "Request Transcript");
        getByXpathVerifyTextPresent("//*[@id='loginDefault']/div[4]/div/p[1]/font", "To request a transcript you must");
        wguPowerPointFileUtils.saveScreenshot(this, "clickRequestTranscript");

        return this;
    }

    public myStudentServicesPage clickProofEnrollmentLetter() {
        getByXpathAndActivate(myXpath.StudentServicesPage_ProofEnrollment, "Letter", 8000);
        Sleep(2000);
        wguPowerPointFileUtils.saveScreenshot(this, "clickProofEnrollmentLetter");

        return this;
    }

    public myStudentServicesPage clickGradeReport() {
        getByXpathAndActivate(myXpath.StudentServicesPage_GradeReport, "Grade Report");
        wguPowerPointFileUtils.saveScreenshot(this, "clickGradeReport");

        return this;
    }

    public myStudentServicesPage clickStudentSuccessKit() {
        getByXpathAndActivate(myXpath.StudentServicesPage_StudentSuccessKit, "Degree Resources");
        getByXpathVerifyTextPresent("//*[@id='lblGlobalMessage']/p/span/strong", "To enroll in a payment plan");
        wguPowerPointFileUtils.saveScreenshot(this, "clickStudentSuccessKit");

        return this;
    }

    public myStudentServicesPage clickFERPAForm() {
        getByXpathAndActivate(myXpath.StudentServicesPage_FERPAForm, "FERPA");
        getByXpathVerifyTextPresent("/html/body/section[2]/h1", "AUTHORIZATION TO RELEASE EDUCATION RECORDS");
        wguPowerPointFileUtils.saveScreenshot(this, "clickFERPAForm");

        return this;
    }

    public myStudentServicesPage clickRequestAccommodation() {
        scrollIntoView(myXpath.StudentServicesPage_RequestAccomm);
        getByXpathAndActivate(myXpath.StudentServicesPage_RequestAccomm, "Accommodations");
//        getByXpathVerifyTextPresent("//*[@id='fsSection20217003']/div[1]/div/p[1]", "Western Governors University would like to provide");
        wguPowerPointFileUtils.saveScreenshot(this, "clickRequestAccommodation");

        return this;
    }

    public myStudentServicesPage clickTermBreakInfo() {
        scrollIntoView(myXpath.StudentServicesPage_TermBreak);
        getByXpathAndActivate(myXpath.StudentServicesPage_TermBreak, "Term Break");

        return this;
    }

    public myStudentServicesPage ExercisePage(boolean cascade) {
        General.Debug("\nmyStudentServicesPage::ExercisePage(" + cascade + ")");
        if (!loginType.isProd()) {
            General.Debug("myStudentServicesPage::ExercisePage only testable on Prod as most links dont have sandboxes");
            return this;
        }

        load();
        clickTermBreakInfo();

        load();
        clickRequestAccommodation();

        load();
        clickRequestIdCard();

        load();
        clickDownloadTranscript();

        load();
        clickRequestTranscript();

        load();
        clickProofEnrollmentLetter();

        load();
        clickGradeReport();

        load();
        clickStudentSuccessKit();

        load();
        clickFERPAForm();

        return this;
    }

}


package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 2/10/2016.
 */

public class myStudentSuccessCenterPage extends mySuccessCentersPage {
    public myStudentSuccessCenterPage() {
        this(null);
    }
    public myStudentSuccessCenterPage(WebPage existingPage) {
        super(existingPage);
    }
    public myStudentSuccessCenterPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }
    public myStudentSuccessCenterPage(WebPage existingPage, LoginType loginType, Browser browser) {
        super(existingPage, loginType, browser);
    }

    public myStudentSuccessCenterPage load() {
        super.load();
        clickStudentSuccessCenter();
        Sleep(3000);

        return this;
    }

    String[] allLinks = new String[] {
 //           "Personality Type",
            "\n\t\t\tStress Management and Coping Strategies",
            "Study\t\t\tSkills",
            "Test\n\t\t\tAnxiety",
            "\t\t\tTest Taking Skills",
            "\t\t\tTime Management",
            "PowerPoint - The Basics",
            "Goal Setting - The Foundation",
            "SMART",
            "Test Taking Strategies",
            "Test Anxiety Webinar",
            "Dealing with Failure",
            "Study Strategies Webinar",
            "Learning Styles Webinar",
            "APA Document Formatting",
            "anage Community Email",
            "\t\t\tAcademic Confidence",
            "Computer\n\t\t\tSkills",
            "\t\t\tConfidence Corner",
            "Goal\t\t\tSetting",
            "Learning\n\t\t\tStyles",
            "Life			Balance",
            "Online\n\t\t\tCommunication",
            "\t\t\tAcademic Confidence",
            "Computer\n\t\t\tSkills",
            "\t\t\tConfidence Corner",
            "Goal\t\t\tSetting",
            "Learning\n\t\t\tStyles",
            "Life			Balance",
            "Online\n\t\t\tCommunication",
    };

    public myStudentSuccessCenterPage ExercisePage(boolean cascade) {
        General.Debug("\nmySuccessCentersPage::ExercisePage(" + cascade + ")");
        for (String linkText : allLinks) {
            load();
            String xpath = myXpath.StudentSuccessCenterPage_GenericText.replace("REPLACETHIS", linkText);
            scrollIntoView(xpath);
            getByXpathAndActivate(xpath, linkText);
            Sleep(2000);
        }

        return this;
    }
}


package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguEnvironment;
import Utils.wguPowerPointFileUtils;
import org.openqa.selenium.WebElement;

import java.util.List;

/*
 * Created by timothy.hallbeck on 2/2/2015.
 */

public class mySuccessCentersPage extends myHomePage {

    public mySuccessCentersPage() {
        super();
    }
    public mySuccessCentersPage(LoginType loginType) {
        super(loginType);
    }
    public mySuccessCentersPage(WebPage existingPage) {
        super(existingPage);
    }
    public mySuccessCentersPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }
    public mySuccessCentersPage(WebPage existingPage, LoginType loginType, Browser browser) {
        super(existingPage, loginType, browser);
    }

    public mySuccessCentersPage load() {
        super.load();
        get(myXpath.ResourcesPage_StartingUrl.replace("PREFIX", loginType.getUrlPrefix()));
        showPageLoadTimes("mySuccessCentersPage::load()");
        checkDataItems();

        wguPowerPointFileUtils.saveScreenshot(this, "mySuccessCentersPage");
        wguPowerPointFileUtils.addBulletPoint("load()");

        return this;
    }

    public mySuccessCentersPage checkDataItems() {
        List<WebElement> tileList = getListByXpath("//*[@class='card__content success-centers-card-content']");
        for (int i=0; i<tileList.size(); i++) {
            String displayText         = tileList.get(i).getText();
            String endpointTitle       = successCenters.getSuccessCenter(i).getTitle();
            String endpointDescription = successCenters.getSuccessCenter(i).getShortDesc();
            assert(displayText.contains(endpointTitle));
            assert(displayText.contains(endpointDescription));
        }

        return this;
    }

    public void getByXpathAndActivateWithRedirect(String xpath, String extraThingToClick) {
        getByXpathAndActivate(xpath);
        Sleep(1000);
        WebElement element = getByXpath(extraThingToClick, true);
        if (element != null)
            element.click();
    }

    public mySuccessCentersPage clickWritingCenter() {
        getByXpathAndActivateWithRedirect(myXpath.SuccessCentersPage_WritingCenter, "/html/body/div/div[2]/div[2]/a");

        return this;
    }

    public mySuccessCentersPage clickLibrary() {
        getByXpathAndActivate(myXpath.SuccessCentersPage_Library, "Library");
        if (getByXpath(myXpath.LibraryPage_LoginInput, true) != null) {
            getByXpath(myXpath.LibraryPage_LoginInput).sendKeys("QA0000007");
            getByXpath(myXpath.LibraryPage_LoginSubmit).click();
        }
//        getByXpath(myXpath.LibraryPage_HomeLoaded);

        return this;
    }

    public mySuccessCentersPage clickStudentSuccessCenter() {
        getByXpathAndActivate(myXpath.SuccessCentersPage_StudentSuccessCenter, "Student Success Center");
        if (getByXpath(myXpath.SuccessCentersPage_BlueSigninBlock, true) != null)
            getByXpath(myXpath.SuccessCentersPage_BlueSigninBlock).click();

        return this;
    }

    public mySuccessCentersPage clickCareerCenter() {
        getByXpathAndActivate(myXpath.SuccessCentersPage_CareerCenter, "Career Center");

        return this;
    }

    public mySuccessCentersPage clickWellConnect() {
        getByXpathAndActivate(myXpath.SuccessCentersPage_WellConnect, "Well Connect");

        return this;
    }

    public mySuccessCentersPage clickMathCenter() {
        getByXpathAndActivate(myXpath.SuccessCentersPage_MathCenter, "Math Center");

        return this;
    }

    public mySuccessCentersPage ExercisePage(boolean cascade) {
        General.Debug("\nmySuccessCentersPage::ExercisePage(" + cascade + ")");
        load(); // For timing info

        if (!(loginType.getEnv() == wguEnvironment.Prod)) {
            General.Debug("mySuccessCentersPage only exercisable on prod. All links point to prod regardless of actual environment");
            return this;
        }

        if (cascade) {
            new myMathCenterPage(this).ExercisePage(true);

            load();
            new myWritingCenterPage(this).ExercisePage(true);

            load();
            new myLibraryPage(this).ExercisePage(true);

            load();
            new myStudentSuccessCenterPage(this).ExercisePage(true);

            load();
            new myCareerCenterPage(this).ExercisePage(true);

            load();
            new myWellConnectPage(this).ExercisePage(true);
        } else {
            clickLibrary();
            load();

            clickWritingCenter();
            load();

            clickStudentSuccessCenter();
            load();

            clickCareerCenter();
            load();

            clickWellConnect();
            load();

            clickMathCenter();
        }

        return this;
    }

}















package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 2/7/2015.
 */

public class myCourseMentorPage extends myCoursesPage {
    public myCourseMentorPage() {
        this(null);
    }
    public myCourseMentorPage(WebPage existingPage) {
        super(existingPage);
    }
    public myCourseMentorPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myCourseMentorPage load() {
        super.load();
        clickCourseMentor();

        return this;
    }

    public myCourseMentorPage ExercisePage(boolean cascade) {
        General.Debug("\nmyCourseMentorPage::ExercisePage(" + cascade + ")");
        load();
        clickCourseMentor();
        return this;
    }
}



package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 7/31/15.
 */

public class myReferAFriend extends myHomePage {

    public myReferAFriend() {
        this(null);
    }
    public myReferAFriend(WebPage existingPage) {
        super(existingPage);
    }
    public myReferAFriend(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myReferAFriend load() {
        login();
        get(myXpath.ReferAFriendPage_StartingUrl);

        return this;
    }


    public myReferAFriend ExercisePage(boolean cascade) {
        General.Debug("\nmyReferAFriend::ExercisePage(" + cascade + ")");
        load();

        if (cascade) {

            return this;
        }

        return this;
    }
}

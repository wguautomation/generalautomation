
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 2/10/2016.
 */

public class myWritingCenterPage extends mySuccessCentersPage {
    public myWritingCenterPage() {
        this(null);
    }

    public myWritingCenterPage(WebPage existingPage) {
        super(existingPage);
    }

    public myWritingCenterPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myWritingCenterPage load() {
        super.load();
        clickWritingCenter();

        return this;
    }

    public myWritingCenterPage clickAcademicAuthenticity() {
        getByXpathAndActivate(myXpath.WritingCenterPage_AcademicAuthenticity, "Academic Authenticity");

        return this;
    }

    public myWritingCenterPage clickAPACitation() {
        getByXpathAndActivate(myXpath.WritingCenterPage_APACitation, "APA Citation");

        return this;
    }

    public myWritingCenterPage clickAppointmentsCalendar() {
        getByXpathAndActivate(myXpath.WritingCenterPage_AppointmentsCalendar, "Appointments Calendar");

        return this;
    }

    public myWritingCenterPage clickAppointmentOverview() {
        getByXpathAndActivate(myXpath.WritingCenterPage_AppointmentOverview, "Appointment Overview");

        return this;
    }

    public myWritingCenterPage clickCurrentVideos() {
        getByXpathAndActivate(myXpath.WritingCenterPage_CurrentVideos, "Current Videos");

        return this;
    }

    public myWritingCenterPage clickFAQGuide() {
        getByXpathAndActivate(myXpath.WritingCenterPage_FAQGuide, "FAQ Guide");

        return this;
    }

    public myWritingCenterPage clickGuideToWriting() {
        getByXpathAndActivate(myXpath.WritingCenterPage_GuideToWriting, "Guide To Writing");

        return this;
    }

    public myWritingCenterPage clickMentorAppointments() {
        getByXpathAndActivate(myXpath.WritingCenterPage_MentorAppointments, "Mentor Appointments");

        return this;
    }

    public myWritingCenterPage clickMentorMinutes() {
        getByXpathAndActivate(myXpath.WritingCenterPage_MentorMinutes, "Mentor Minutes");

        return this;
    }

    public myWritingCenterPage clickOriginalityResources() {
        getByXpathAndActivate(myXpath.WritingCenterPage_OriginalityResources, "Originalit Resources");

        return this;
    }

    public myWritingCenterPage clickPraxisSupport() {
        getByXpathAndActivate(myXpath.WritingCenterPage_PraxisSupport, "Praxis Support");

        return this;
    }

    public myWritingCenterPage clickResearchWriting() {
        getByXpathAndActivate(myXpath.WritingCenterPage_ResearchWriting, "Research Writing");

        return this;
    }

    public myWritingCenterPage clickWebinarSessions() {
        getByXpathAndActivate(myXpath.WritingCenterPage_WebinarSessions, "Webinar Sessions");

        return this;
    }

    public myWritingCenterPage clickWritingInstruction() {
        getByXpathAndActivate(myXpath.WritingCenterPage_WritingInstruction, "Writing Instruction");

        return this;
    }

    public myWritingCenterPage clickWritingProcess() {
        getByXpathAndActivate(myXpath.WritingCenterPage_WritingProcess, "Writing Process");

        return this;
    }

    public myWritingCenterPage ExercisePage(boolean cascade) {
        General.Debug("\nmyWritingCenterPage::ExercisePage(" + cascade + ")");
        load();

        clickGuideToWriting();
        load();

        clickMentorAppointments();
        load();

        clickMentorMinutes();
        load();

        clickOriginalityResources();
        load();

        clickPraxisSupport();
        load();

        clickResearchWriting();
        load();

        clickWebinarSessions();
        load();

        clickWritingInstruction();
        load();

        clickWritingProcess();
        load();

        clickAcademicAuthenticity();
        load();

        clickAPACitation();
        load();

        clickAppointmentsCalendar();
        load();

        clickAppointmentOverview();
        load();

        clickCurrentVideos();
//        load();

//        clickFAQGuide();
//        load();

        return this;
    }
}


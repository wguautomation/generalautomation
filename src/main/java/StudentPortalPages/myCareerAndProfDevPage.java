
package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 2/7/2015.
 */

public class myCareerAndProfDevPage extends myLearningResourcesPage {
    public myCareerAndProfDevPage() {
        this(null);
    }
    public myCareerAndProfDevPage(WebPage existingPage) {
        super(existingPage);
    }
    public myCareerAndProfDevPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myCareerAndProfDevPage load() {
        super.load();
        clickCareerAndProfDev();
        return this;
    }

    public myCareerAndProfDevPage cExercisePage(boolean cascade) {
        General.Debug("\nmyCareerAndProfDevPage::ExercisePage(" + cascade + ")");
        load();
        return this;
    }
}


package StudentPortalPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 2/10/2016.
 */

public class myWellConnectPage extends mySuccessCentersPage {
    public myWellConnectPage() {
        this(null);
    }
    public myWellConnectPage(WebPage existingPage) {
        super(existingPage);
    }
    public myWellConnectPage(WebPage existingPage, LoginType loginType) {
        super(existingPage, loginType);
    }

    public myWellConnectPage load() {
        super.load();
        clickWellConnect();

        return this;
    }

    public myWellConnectPage ExercisePage(boolean cascade) {
        General.Debug("\nmyWellConnectPage::ExercisePage(" + cascade + ")");
        load();

        return this;
    }
}


package JiraPages;

/*
 * Created by timothy.hallbeck on 5/23/2015.
 */

import BaseClasses.WebPage;
import Utils.General;

public class jiraIssuePage extends WebPage {
    public jiraIssuePage() {
        this(null);
    }

    public jiraIssuePage(WebPage existingPage) {
        super(existingPage);
    }

    public jiraIssuePage ExercisePage(boolean cascade) {
        General.Debug("\njiraIssuePage::ExercisePage(" + cascade + ")");
        load();

        if (cascade) {
            return this;
        }

        return this;
    }
}

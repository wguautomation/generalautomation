
package JiraPages;

import BaseClasses.WebPage;
import BaseClasses.LoginType;
import Utils.General;

/*
 * Created by timothy.hallbeck on 5/23/2015.
 */

public class jiraHomePage extends WebPage {
    public jiraHomePage() {
        this(null);
    }

    public jiraHomePage(WebPage existingPage) {
        super(existingPage, LoginType.JIRA_HOMEPAGE);
    }

    public boolean login() {
        if (bLoggedIn)
            return false;

        get(loginType.getStartingUrl());
        WaitForPage();
//        switchToFrame("gadget-0");
        getByXpath(loginType.getUsernameXpath()).clear();
        getByXpath(loginType.getUsernameXpath()).sendKeys(loginType.getUsername());
        getByXpath(loginType.getPasswordXpath()).clear();
        getByXpath(loginType.getPasswordXpath()).sendKeys(loginType.getPassword());
        getByXpath(loginType.getLoginButtonXpath()).click();
        RestoreDefaultFrameAndWindow();

        getByXpath("//*[@id='dashboard-content']/div[1]");
        bLoggedIn = true;

        return true;
    }

    public jiraHomePage ExercisePage(boolean cascade) {
        General.Debug("\njiraHomePage::ExercisePage(" + cascade + ")");
        login();

        if (cascade) {
            // ...
            return this;
        }

        return this;
    }

    public jiraHomePage WaitForPage() {
        getByXpath("//*[@id='footer']/section/ul/li[1]");
        getByXpath("//*[@id='publicmodeoffmsg']");
        Sleep(2000);

        return this;
    }
}

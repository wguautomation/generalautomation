
package OracleClasses;

import java.util.List;

/*
 * Created by timothy.hallbeck on 12/17/2015.
 */

public class OracleTodosTask extends OracleBase {

    public OracleTodosTask(String username) {
        super("select * from wgutodos.task where username = '" + username + "' order by due_date ASC");
    }

    public List<List<String>> getAllTasks() {
        doQueries();

        return resultArray;
    }

    public List<List<String>> getActiveTasks() {
        doQueries();
        removeDeleted();
        removeCompleted();
        removeInvalid();

        return resultArray;
    }

    public List<List<String>> getDisplayedTasks() {
        doQueries();
        removeDeleted();
        removeInvalid();

        return resultArray;
    }

    public String getId(int index) {
        return fixNull(resultArray.get(index).get(0));
    }

    public String getVersion(int index) {
        return fixNull(resultArray.get(index).get(1));
    }

    public String getDateCompleted(int index) {
        return formatDate(fixNull(resultArray.get(index).get(2)));
    }

    public String getDescription(int index) {
        return fixNull(resultArray.get(index).get(3));
    }

    public String getDueDate(int index) {
        return formatDate(fixNull(resultArray.get(index).get(4)));
    }

    public String getPriority(int index) {
        return fixNull(resultArray.get(index).get(5));
    }

    public String getTitle(int index) {
        return fixNull(resultArray.get(index).get(6));
    }

    public String getUsername(int index) {
        return fixNull(resultArray.get(index).get(7));
    }

    public String getPidm(int index) {
        return fixNull(resultArray.get(index).get(8));
    }

    public String getDeleted(int index) {
        return fixNull(resultArray.get(index).get(9));
    }

    public String getDateCreated(int index) {
        return formatDate(fixNull(resultArray.get(index).get(10)));
    }

    public int removeDeleted() {
        for (int i = resultArray.size() - 1; i >= 0; i--) {
            if (getDeleted(i).contains("1"))
                resultArray.remove(i);
        }

        return resultArray.size();
    }

    public int removeCompleted() {
        for (int i = resultArray.size() - 1; i >= 0; i--) {
            if (!getDateCompleted(i).isEmpty())
                resultArray.remove(i);
        }

        return resultArray.size();
    }

    public int removeInvalid() {
        for (int i = resultArray.size() - 1; i >= 0; i--) {
            if (getPidm(i).isEmpty() || getDateCreated(i).isEmpty() || getDueDate(i).isEmpty())
                resultArray.remove(i);
        }

        return resultArray.size();
    }

}


package OracleClasses;

import Utils.General;
import org.testng.TestNGException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by timothy.hallbeck on 12/17/2015.
 */

public abstract class OracleBase {

    private   Connection         connection;
    private   Statement          statement = null;
    private   String             dataQuery;
    protected List<List<String>> resultArray = new ArrayList<>();

    OracleBase(String dataQuery)
    {
        try {
            this.dataQuery = dataQuery;
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
            connection = DriverManager.getConnection("jdbc:oracle:thin:@//oaklane1-scan.wgu.edu:1521/lane1.wgu.edu", "thallbeck", "WhatThefuck12!");
        } catch (Exception e) {
            General.Debug("Error in OracleBase() constructor");
            throw new TestNGException(e.toString());
        }
    }

    public int size() {
        return resultArray.size();
    }

    public Connection getConnection() {
        return connection;
    }

    public int doQueries() {
        try {
            statement           = connection.prepareStatement(dataQuery);
            ResultSet resultSet = statement.executeQuery(dataQuery);
            int numcols         = resultSet.getMetaData().getColumnCount();

            while (resultSet.next()) {
                List<String> row = new ArrayList<>(numcols); // new list per row
                int i = 1;
                while (i <= numcols) {  // don't skip the last column, use <=
                    String name = resultSet.getMetaData().getColumnName(i);
                    row.add(resultSet.getString(i++));
                }
                resultArray.add(row); // add it to the result
            }
        } catch (Exception e) {
            General.Debug("Error in OracleBase.doQuery()");
            throw new TestNGException(e.toString());
        } finally {
            close();
        }

        return resultArray.size();
    }

    public void close() {
        try {
            statement.close();
        } catch (Exception e) {
            General.Debug("Error in OracleBase.close()");
            throw new TestNGException(e.toString());
        }
    }

    protected String fixNull(String string) {
        if (string == null)
            return "";
        return string;
    }

    public String formatDate(String oracleDate) {
        if (oracleDate.length() < 10)
            return oracleDate;
        String date = oracleDate.substring(5, 7) + "/" + oracleDate.substring(8, 10) + "/" + oracleDate.substring(2, 4);

        return date;
    }

    @Override
    public String toString() {
        String output = "";
        for (List<String> list : resultArray) {
            output += list.toString() + "\n";
        }
        return output;
    }
}



























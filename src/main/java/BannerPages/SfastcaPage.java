
package BannerPages;

import Utils.wguRobot;
import java.awt.*;

/*
 * Created by timothy.hallbeck on 12/15/2015.
 */

public class SfastcaPage extends MainPage {

    private static String PREFIX = "Sfastca/";

    public SfastcaPage() throws AWTException {
        super();
    }

    public SfastcaPage load() {
        GoToForm("Sfastca");
        Clear(PREFIX + "Term.PNG", 50, -10);

        return this;
    }

    public SfastcaPage setTerm(String term) {
        EnterText(PREFIX + "Term.PNG", term, 100, 0);

        return this;
    }

    public BannerPage setID(String id) {
        EnterText(PREFIX + "ID.PNG", id, 100, -20);

        return this;
    }

    public SfastcaPage setFromDate(String date) {
        EnterText(PREFIX + "RegistrationFromDate.PNG", date, 200, 0);

        return this;
    }

    public SfastcaPage setToDate(String date) {
        EnterText(PREFIX + "RegistrationToDate.PNG", date, 150, 0);

        return this;
    }

    public SfastcaPage ExercisePage() {
        load();
        setID("QA0000007");
        setFromDate("11-NOV-2014");
        setToDate("11-DEC-2015");
        clickNextBlock(2);

        return this;
    }

}

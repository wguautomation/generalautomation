
package BannerPages;

import Utils.wguRobot;
import java.awt.*;

/*
 * Created by timothy.hallbeck on 12/15/2015.
 */

public class TsaarevPage extends MainPage {

    private static String PREFIX = "Tsaarev/";

    public TsaarevPage() throws AWTException {
        super();
    }

    public TsaarevPage load() {
        GoToForm("Tsaarev");
        CheckForPrinterPage();
        Clear(PREFIX + "ID.PNG", 75, -40);

        return this;
    }

    public TsaarevPage ExercisePage() {
        load();
        lookupByID("QA0000007");

        return this;
    }

}

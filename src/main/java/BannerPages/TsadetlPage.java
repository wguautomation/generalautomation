
package BannerPages;

import Utils.wguRobot;
import java.awt.*;

/*
 * Created by timothy.hallbeck on 12/15/2015.
 */

public class TsadetlPage extends MainPage {

    private static String PREFIX = "Tsadetl/";

    public TsadetlPage() throws AWTException {
        super();
    }

    public TsadetlPage load() {
        GoToForm("TSADETL");
        Sleep(1000);
        CheckForPrinterPage();
        Sleep(500);
        Clear(PREFIX + "ID.PNG", 75, -40);

        return this;
    }

    public TsadetlPage clickTab_ChargesPayments() {
        if (Exists(PREFIX + "Tab_ChargesPayments.PNG"))
            SetFocus(PREFIX + "Tab_ChargesPayments.PNG");
        Sleep(500);

        return this;
    }

    public TsadetlPage clickTab_Deposits() {
        if (Exists(PREFIX + "Tab_Deposits.PNG"))
            SetFocus(PREFIX + "Tab_Deposits.PNG");
        Sleep(500);

        return this;
    }

    public TsadetlPage clickTab_Memos() {
        if (Exists(PREFIX + "Tab_Memos.PNG"))
            SetFocus(PREFIX + "Tab_Memos.PNG");
        Sleep(500);

        return this;
    }

    public TsadetlPage clickTab_DatesInvoice() {
        clickTab_ChargesPayments();
        if (Exists(PREFIX + "Tab_DatesInvoice.PNG"))
            SetFocus(PREFIX + "Tab_DatesInvoice.PNG");
        Sleep(500);

        return this;
    }

    public TsadetlPage clickTab_FeedCashier() {
        clickTab_ChargesPayments();
        if (Exists(PREFIX + "Tab_FeedCashier.PNG"))
            SetFocus(PREFIX + "Tab_FeedCashier.PNG");
        Sleep(500);

        return this;
    }

    public TsadetlPage clickTab_CrossRef() {
        clickTab_ChargesPayments();
        if (Exists(PREFIX + "Tab_CrossRef.PNG"))
            SetFocus(PREFIX + "Tab_CrossRef.PNG");
        Sleep(500);

        return this;
    }

    public TsadetlPage clickTab_Tax() {
        clickTab_ChargesPayments();
        if (Exists(PREFIX + "Tab_Tax.PNG"))
            SetFocus(PREFIX + "Tab_Tax.PNG");
        Sleep(500);

        return this;
    }

    public TsadetlPage ExercisePage() {
        load();
        lookupByID("QA0000007");

        clickTab_Memos();
        clickTab_ChargesPayments();
        clickTab_Deposits();
        clickTab_Memos();
        clickTab_Tax();
        clickTab_DatesInvoice();
        clickTab_FeedCashier();
        clickTab_CrossRef();
        clickTab_Tax();

        return this;
    }

}

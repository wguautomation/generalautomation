
package BannerPages;

import Utils.wguRobot;
import java.awt.*;

/*
 * Created by timothy.hallbeck on 1/4/2016.
 */

public class SgaadvrPage extends MainPage {

    public static String PREFIX = "Sgaadvr/";

    public SgaadvrPage() throws AWTException {
        super();
    }

    public SgaadvrPage load() {
        GoToForm("SGAADVR");
        CheckForPrinterPage();

        return this;
    }

    public SgaadvrPage setTerm(String id) {
        EnterText(PREFIX + "Term.PNG", id, 50, 0);

        return this;
    }

    public SgaadvrPage ExercisePage() {
        load();
        setID("QA0000007");
        setTerm("201501");
        clickNextBlock();

        return this;
    }
}


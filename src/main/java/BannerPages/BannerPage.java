
package BannerPages;

import BaseClasses.LoginType;
import Utils.General;
import Utils.wguRobot;
import org.sikuli.script.ImagePath;
import org.sikuli.script.Match;
import org.sikuli.script.Screen;
import org.testng.TestNGException;

import java.awt.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/*
 * Created by timothy.hallbeck on 12/10/2015.
 */

abstract public class BannerPage extends wguRobot {

    final public Screen screen;
    final public static String PREFIX = "";

    public BannerPage() throws AWTException {
        screen = new Screen();
        ImagePath.setBundlePath(ImagePath.getBundlePath() + "\\src\\main\\resources\\Banner\\");
    }

    public BannerPage CheckForPrinterPage() {
        if (Exists(PREFIX + "PrinterPage.PNG"))
            clickClose();
        if (Exists(PREFIX + "PrinterPage2.PNG"))
            clickClose();

        return this;
    }

    public BannerPage clickClose() {
        SetFocus(PREFIX + "Button_Exit.PNG");

        return this;
    }

    public boolean login() {
        String[] fullCommand = {
                "\"C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe \"",
                LoginType.BANNER_LANE1.getStartingUrl(),
        };
        ProcessBuilder builder = new ProcessBuilder(fullCommand);
        String line="";

        try {
            final Process process = builder.start();
            line = new BufferedReader(new InputStreamReader(process.getInputStream())).readLine();
            General.Debug("Response:\n" + line);
        } catch (Exception e) {
            General.Debug("Error: " + e.getMessage());
        }

        Sleep(2000);
        EnterText(LoginType.BANNER_LANE1.getUsernameXpath(), LoginType.BANNER_LANE1.getUsername(), 50, 0);
        EnterText(LoginType.BANNER_LANE1.getPasswordXpath(), LoginType.BANNER_LANE1.getPassword(), 50, 0);
        SetFocus(LoginType.BANNER_LANE1.getLoginButtonXpath());
        Sleep(3000);

        return true;
    }

    public BannerPage GoToForm(String formname) {
        if (!Exists(PREFIX + "GoToForm.PNG"))
            clickClose();
        EnterText(PREFIX + "GoToForm.PNG", formname + "\n");
        Sleep(3000);

        return this;
    }

    public boolean Exists(String imageFile) {
        screen.capture(0, 0, 1400, 900);
        Match match = screen.exists(imageFile);

        if (match == null || match.getScore() < .85) {
            General.Debug("Could not find file [" + imageFile + "]");
            return false;
        }

        return true;
    }

    public Match SetFocus(String imageFile) {
        return SetFocus(imageFile, 0, 0);
    }
    public Match SetFocus(String imageFile, int xOffset, int yOffset) {
        screen.capture(0, 0, 1400, 900);
        Match match = screen.exists(imageFile);
        if (match == null || match.getScore() < .85)
            throw new TestNGException("Could not find [" + imageFile + "]");
        match.setTargetOffset(xOffset, yOffset);
        match.click();
        Sleep(1000);

        return match;
    }

    public Match EnterText(String imageFile, String text) {
        return EnterText(imageFile, text, 0, 0);
    }
    public Match EnterText(String imageFile, String text, int xOffset, int yOffset) {
        Match match = SetFocus(imageFile, xOffset, yOffset);
        Clear(imageFile, xOffset, yOffset);
        match.type(text);
        Sleep(1000);

        return match;
    }

    public void Clear(String imageFile, int xOffset, int yOffset) {
        Match match = SetFocus(imageFile, xOffset, yOffset);
        match.type(wguRobot.KEY_END);
        for (int i=0; i<20; i++)
            match.type(wguRobot.KEY_BACKSP);
    }

    public BannerPage closeBrowserTab() {
        SetFocus(PREFIX + "CloseBannerTab.PNG", 110, 0);

        return this;
    }


    abstract public BannerPage ExercisePage();

}


package BannerPages;

import Utils.wguRobot;
import java.awt.*;

/*
 * Created by timothy.hallbeck on 12/15/2015.
 */

public class SgastdnPage extends MainPage {

    private static String PREFIX = "Sgastdn/";

    public SgastdnPage() throws AWTException {
        super();
    }

    public SgastdnPage load() {
        GoToForm("SGASTDN");
        Clear(PREFIX + "ID.PNG", 75, -40);

        return this;
    }

    public SgastdnPage clickTab_Learner() {
        if (Exists(PREFIX + "Tab_Learner.PNG"))
            SetFocus(PREFIX + "Tab_Learner.PNG");
        Sleep(500);

        return this;
    }

    public SgastdnPage clickTab_Curricula() {
        if (Exists(PREFIX + "Tab_Curricula.PNG"))
            SetFocus(PREFIX + "Tab_Curricula.PNG");
        Sleep(500);

        return this;
    }

    public SgastdnPage clickTab_StudyPath() {
        if (Exists(PREFIX + "Tab_StudyPath.PNG"))
            SetFocus(PREFIX + "Tab_StudyPath.PNG");
        Sleep(500);

        return this;
    }

    public SgastdnPage clickTab_Activities() {
        if (Exists(PREFIX + "Tab_Activities.PNG"))
            SetFocus(PREFIX + "Tab_Activities.PNG");
        Sleep(500);

        return this;
    }

    public SgastdnPage clickTab_Veteran() {
        if (Exists(PREFIX + "Tab_Veteran.PNG"))
            SetFocus(PREFIX + "Tab_Veteran.PNG");
        Sleep(500);

        return this;
    }

    public SgastdnPage clickTab_Comments() {
        if (Exists(PREFIX + "Tab_Comments.PNG"))
            SetFocus(PREFIX + "Tab_Comments.PNG");
        Sleep(500);

        return this;
    }

    public SgastdnPage clickTab_DualDegree() {
        if (Exists(PREFIX + "Tab_DualDegree.PNG"))
            SetFocus(PREFIX + "Tab_DualDegree.PNG");
        Sleep(500);

        return this;
    }

    public SgastdnPage clickTab_Misc() {
        if (Exists(PREFIX + "Tab_Misc.PNG"))
            SetFocus(PREFIX + "Tab_Misc.PNG");
        Sleep(500);

        return this;
    }

    public SgastdnPage ExercisePage() {
        load();
        lookupByID("QA0000008");

        clickTab_Misc();
        clickTab_Learner();
        clickTab_Curricula();
        clickTab_StudyPath();
        clickTab_Activities();
        clickTab_Veteran();
        clickTab_Comments();
        clickTab_DualDegree();
        clickTab_Misc();

        return this;
    }
}


package BannerPages;

import Utils.wguRobot;
import java.awt.*;

/*
 * Created by timothy.hallbeck on 1/4/2016.
 */

public class SoaidenPage extends MainPage {

    private static String PREFIX = "Soaiden/";

    public SoaidenPage() throws AWTException {
        super();
    }

    public SoaidenPage load() {
        GoToForm("SOAIDEN");
        Clear(PREFIX + "ID.PNG", 50, 50);

        return this;
    }

    public SoaidenPage setId(String id) {
        EnterText(PREFIX + "ID.PNG", id, 0, 100);

        return this;
    }

    public SoaidenPage ExercisePage(boolean bRecursive) {
        load();
        setId("QA0000007");
        clickExecuteQuery();

        return this;
    }

}

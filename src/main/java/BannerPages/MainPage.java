
package BannerPages;

import Utils.General;
import Utils.wguRobot;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Match;
import java.awt.*;

/*
 * Created by timothy.hallbeck on 12/9/2015.
 */

public class MainPage extends BannerPage {

    private static String PREFIX = "MainPage/";

    public MainPage() throws AWTException {
        super();
    }

    public void CloseAllFolders() {
        Match match;

        do {
            screen.capture(0, 0, 600, 600);
            match = screen.exists(PREFIX + "OpenFolder.PNG");
        } while (match != null && match.doubleClick() != 0 && Sleep(1500));

        screen.capture(0, 0, 600, 600);
        match = screen.exists(PREFIX + "MyBanner.PNG");
        if (match.getScore() > .9)
            match.click();
    }

    public BannerPage clickExecuteQuery() {
        SetFocus(PREFIX + "Button_ExecuteQuery.PNG").click();

        return this;
    }

    public BannerPage clickNextBlock() {
        return clickNextBlock(1);
    }
    public BannerPage clickNextBlock(int count) {
        Match match = SetFocus(PREFIX + "Button_NextBlock.PNG");
        // SetFocus clicks once already
        while(Sleep(500) && --count > 0)
            match.click();

        return this;
    }

    public BannerPage clickSave() {
        SetFocus(PREFIX + "Button_Save.PNG");
        Sleep(500);

        return this;
    }

    public BannerPage lookupByID(String id) {
        setID(id);
        clickNextBlock(2);

        return this;
    }

    public BannerPage setID(String id) {
        EnterText(PREFIX + "ID.PNG", id, 50, 0);

        return this;
    }

    public void TestTreeFolders() throws FindFailed {
        CloseAllFolders();
    }

    public MainPage ExercisePage(boolean cascade) {
        General.Debug("\nMainPage::ExercisePage(" + cascade + ")");
        return this;
    }

    public MainPage ExercisePage() {
        return this;
    }
}


package BannerPages;

import Utils.wguRobot;
import java.awt.*;

/*
 * Created by timothy.hallbeck on 1/4/2016.
 */

public class TwaarevPage extends MainPage {

    public static String PREFIX = "Twaarev/";

    public TwaarevPage() throws AWTException {
        super();
    }

    public TwaarevPage load() {
        GoToForm("TWAAREV");
        CheckForPrinterPage();

        return this;
    }

    public TwaarevPage setTerm(String id) {
        EnterText(PREFIX + "Term.PNG", id, 50, 0);

        return this;
    }

    public TwaarevPage ExercisePage() {
        load();
        lookupByID("QA0000007");
//        setTerm("201501");
//        clickNextBlock(1);

        return this;
    }
}

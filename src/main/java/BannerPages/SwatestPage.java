
package BannerPages;

import Utils.wguRobot;
import java.awt.*;

/*
 * Created by timothy.hallbeck on 1/4/2016.
 */

public class SwatestPage extends MainPage {

    public static String PREFIX = "Swatest/";

    public SwatestPage() throws AWTException {
        super();
    }

    public SwatestPage load() {
        GoToForm("Swatest");

        return this;
    }

    public SwatestPage setId(String id) {
        EnterText(PREFIX + "ID.PNG", id, 50, 0);

        return this;
    }

    public SwatestPage ExercisePage() {
        load();
        setId("QA0000007");
        clickNextBlock(1);

        return this;
    }
}


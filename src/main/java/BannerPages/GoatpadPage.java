
package BannerPages;

import Utils.wguRobot;
import java.awt.*;

/*
 * Created by timothy.hallbeck on 12/11/2015.
 */

public class GoatpadPage extends MainPage {

    private static String PREFIX = "Goatpad/";

    public GoatpadPage() throws AWTException {
        super();
    }

    public GoatpadPage load() {
        GoToForm("GOATPAD");
        Clear(PREFIX + "ID.PNG", 75, -40);

        return this;
    }

    public GoatpadPage lookupByID(String id) {
        EnterText(PREFIX + "ID.PNG", id, 50, 0);
        clickNextBlock(2);

        return this;
    }

    public GoatpadPage clickTab_PinHistory() {
        if (Exists(PREFIX + "Tab_PinHistory.PNG"))
            SetFocus(PREFIX + "Tab_PinHistory.PNG");
        Sleep(500);

        return this;
    }

    public GoatpadPage clickTab_ThirdPartyHistory() {
        if (Exists(PREFIX + "Tab_ThirdPartyHistory.PNG"))
            SetFocus(PREFIX + "Tab_ThirdPartyHistory.PNG");
        Sleep(500);

        return this;
    }

    public GoatpadPage clickTab_VerificationQuestions() {
        if (Exists(PREFIX + "Tab_VerificationQuestions.PNG"))
            SetFocus(PREFIX + "Tab_VerificationQuestions.PNG");
        Sleep(500);

        return this;
    }

    public GoatpadPage clickTab_VerificationAnswers() {
        if (Exists(PREFIX + "Tab_VerifyAnswers.PNG"))
            SetFocus(PREFIX + "Tab_VerifyAnswers.PNG");
        Sleep(500);

        return this;
    }

    public GoatpadPage ExercisePage() {
        load();
        lookupByID("QA0000008");

        clickTab_PinHistory();
        clickTab_ThirdPartyHistory();
        clickTab_VerificationQuestions();
        clickTab_VerificationAnswers();

        return this;
    }
}

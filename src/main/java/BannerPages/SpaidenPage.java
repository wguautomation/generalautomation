
package BannerPages;

import Utils.wguRobot;
import java.awt.*;

/*
 * Created by timothy.hallbeck on 12/10/2015.
 */

public class SpaidenPage extends MainPage {

    private static String PREFIX = "Spaiden/";

    public SpaidenPage() throws AWTException {
        super();
    }

    public SpaidenPage load() {
        GoToForm("SPAIDEN");
        Clear(PREFIX + "ID.PNG", 50, 0);

        return this;
    }

    public SpaidenPage setFirstName(String name) {
        clickCurrentIdentification();
        EnterText(PREFIX + "FirstName.PNG", name, 50, 0);

        return this;
    }

    public SpaidenPage setMiddleName(String name) {
        clickCurrentIdentification();
        EnterText(PREFIX + "MiddleName.PNG", name, 50, 0);

        return this;
    }

    public SpaidenPage setLastName(String name) {
        clickCurrentIdentification();
        EnterText(PREFIX + "LastName.PNG", name, 50, 0);

        return this;
    }

    public SpaidenPage setStreetLine1(String street) {
        clickAddress();
        EnterText(PREFIX + "StreetLine1.PNG", street, 150, 0);

        return this;
    }

    public SpaidenPage setStreetLine2(String street) {
        clickAddress();
        EnterText(PREFIX + "StreetLine2.PNG", street, 150, 0);

        return this;
    }

    public SpaidenPage setStreetLine3(String street) {
        clickAddress();
        EnterText(PREFIX + "StreetLine3.PNG", street, 150, 0);

        return this;
    }

    public SpaidenPage setCity(String street) {
        clickAddress();
        EnterText(PREFIX + "City.PNG", street, 50, 0);

        return this;
    }

    public SpaidenPage setState(String street) {
        clickAddress();
        EnterText(PREFIX + "State.PNG", street, 50, 0);

        return this;
    }

    public SpaidenPage setZip(String street) {
        clickAddress();
        EnterText(PREFIX + "Zip.PNG", street, 50, 0);

        return this;
    }

    public SpaidenPage setNation(String street) {
        clickAddress();
        EnterText(PREFIX + "Nation.PNG", street, 50, 0);

        return this;
    }

    public SpaidenPage clickCurrentIdentification() {
        if (Exists(PREFIX + "Tab_CurrentIdentification.PNG"))
            SetFocus(PREFIX + "Tab_CurrentIdentification.PNG");
        Sleep(500);

        return this;
    }

    public SpaidenPage clickAlternateIdentification() {
        if (Exists(PREFIX + "Tab_AlternateIdentification.PNG"))
            SetFocus(PREFIX + "Tab_AlternateIdentification.PNG");
        Sleep(500);

        return this;
    }

    public SpaidenPage clickAddress() {
        if (Exists(PREFIX + "Tab_Address.PNG"))
            SetFocus(PREFIX + "Tab_Address.PNG");
        Sleep(500);

        return this;
    }

    public SpaidenPage clickTelephone() {
        if (Exists(PREFIX + "Tab_Telephone.PNG"))
            SetFocus(PREFIX + "Tab_Telephone.PNG");
        Sleep(500);

        return this;
    }

    public SpaidenPage clickBiographical() {
        if (Exists(PREFIX + "Tab_Biographical.PNG"))
            SetFocus(PREFIX + "Tab_Biographical.PNG");
        Sleep(500);

        return this;
    }

    public SpaidenPage clickEmail() {
        if (Exists(PREFIX + "Tab_Email.PNG"))
            SetFocus(PREFIX + "Tab_Email.PNG");
        Sleep(500);

       return this;
    }

    public SpaidenPage clickEmergencyContact() {
        if (Exists(PREFIX + "Tab_EmergencyContact.PNG"))
            SetFocus(PREFIX + "Tab_EmergencyContact.PNG");
        Sleep(500);

        return this;
    }

    public SpaidenPage clickAdditionalInformation() {
        if (Exists(PREFIX + "Tab_AdditionalInformation.PNG"))
            SetFocus(PREFIX + "Tab_AdditionalInformation.PNG");
        Sleep(500);

        return this;
    }

    public SpaidenPage ExercisePage() {
        load();
        lookupByID("QA0000008");
        setStreetLine1("QA8 Line 1");
        setStreetLine2("QA8 Line 2");
        setStreetLine3("QA8 Line 3");
        clickSave();

        clickCurrentIdentification();
        clickAlternateIdentification();
        clickAddress();
        clickTelephone();
        clickBiographical();
        clickEmail();
        clickEmergencyContact();
        clickAdditionalInformation();

        return this;
    }

}

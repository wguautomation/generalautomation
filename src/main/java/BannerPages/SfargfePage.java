
package BannerPages;

import Utils.wguRobot;
import java.awt.*;

/*
 * Created by timothy.hallbeck on 12/15/2015.
 */

public class SfargfePage extends MainPage {

    private static String PREFIX = "Sfargfe/";

    public SfargfePage() throws AWTException {
        super();
    }

    public SfargfePage load() {
        GoToForm("Sfargfe");
        Clear(PREFIX + "Term.PNG", 50, -10);

        return this;
    }

    public SfargfePage setTerm(String term) {
        EnterText(PREFIX + "Term.PNG", term, 50, -20);
        clickNextBlock(2);

        return this;
    }

    public SfargfePage clickTab_StudentCurriculum() {
        if (Exists(PREFIX + "Tab_StudentCurriculum.PNG"))
            SetFocus(PREFIX + "Tab_StudentCurriculum.PNG");
        Sleep(500);

        return this;
    }

    public SfargfePage clickTab_RegistrationCriteria() {
        if (Exists(PREFIX + "Tab_RegistrationCriteria.PNG"))
            SetFocus(PREFIX + "Tab_RegistrationCriteria.PNG");
        Sleep(500);

        return this;
    }

    public SfargfePage clickTab_StudentCourse() {
        if (Exists(PREFIX + "Tab_StudentCourse.PNG"))
            SetFocus(PREFIX + "Tab_StudentCourse.PNG");
        Sleep(500);

        return this;
    }


    public SfargfePage ExercisePage() {
        load();
        setTerm("201510");

        clickTab_StudentCourse();
        clickTab_StudentCurriculum();
        clickTab_RegistrationCriteria();
        clickTab_StudentCourse();

        return this;
    }

}

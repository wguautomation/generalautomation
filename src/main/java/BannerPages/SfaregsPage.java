
package BannerPages;

import Utils.wguRobot;
import java.awt.*;

/*
 * Created by timothy.hallbeck on 1/4/2016.
 */

public class SfaregsPage extends MainPage {

    private static String PREFIX = "Sfaregs/";

    public SfaregsPage() throws AWTException {
        super();
    }

    public SfaregsPage load() {
        GoToForm("Sfaregs");
        CheckForPrinterPage();

        return this;
    }

    public SfaregsPage setTerm(String term) {
        EnterText(PREFIX + "Term.PNG", term, 50, 0);

        return this;
    }

    public SfaregsPage setID(String id) {
        EnterText(PREFIX + "ID.PNG", id, 50, 0);

        return this;
    }

    public SfaregsPage clickRegistration() {
        if (Exists(PREFIX + "Registration.PNG"))
            SetFocus(PREFIX + "Registration.PNG");

        return this;
    }

    public SfaregsPage clickStudentTerm() {
        if (Exists(PREFIX + "StudentTerm.PNG"))
            SetFocus(PREFIX + "StudentTerm.PNG");

        return this;
    }

    public SfaregsPage clickCurricula() {
        if (Exists(PREFIX + "Curricula.PNG"))
            SetFocus(PREFIX + "Curricula.PNG");

        return this;
    }

    public SfaregsPage clickCurriculum() {
        clickCurricula();
        if (Exists(PREFIX + "Curriculum.PNG"))
            SetFocus(PREFIX + "Curriculum.PNG");

        return this;
    }

    public SfaregsPage clickFieldOfStudy() {
        clickCurricula();
        if (Exists(PREFIX + "FieldOfStudy.PNG"))
            SetFocus(PREFIX + "FieldOfStudy.PNG");

        return this;
    }

    public SfaregsPage ExercisePage() {
        load();
        setTerm("201601");
        setID("QA0000007");
        clickNextBlock(2);

        clickCurricula();
        clickRegistration();
        clickStudentTerm();

        clickCurriculum();
        clickFieldOfStudy();

        return this;
    }

}

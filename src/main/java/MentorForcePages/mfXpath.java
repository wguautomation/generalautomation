
package MentorForcePages;

/*
 * Created by timothy.hallbeck on 1/7/2016.
 */

public class mfXpath {
    public static String DegreePlan_HeaderTag = "//*[@class='icon-head my-degree']";

    public static String MainPage_BackToMentorView  = "//*[@id='BackToServiceDesk_Tab']";
    public static String MainPage_ConsoleUrl        = "https://srm--qafull.cs25.my.salesforce.com/console";
    public static String MainPage_DegreePlanButton  = "//*[@id='GoToDegreePlanButton-btnIconEl']";
    public static String MainPage_MentorLoginButton = "//*[@name='login']";
    public static String MainPage_MentorView        = "//*[@class='brandPrimaryFgr']";
    public static String MainPage_url               = "https://srm--qafull.cs25.my.salesforce.com/00530000006rdXx?noredirect=1&isUserEntityOverride=1";

    public static String MainPage_StudentListEntry  = "//*[@class='x-grid-cell-inner']";
}

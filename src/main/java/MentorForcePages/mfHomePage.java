
package MentorForcePages;

import BaseClasses.LoginType;
import BaseClasses.Page;
import BaseClasses.WebPage;
import SalesForcePages.sfHomePage;
import Utils.General;
import org.openqa.selenium.WebElement;

import java.util.List;

/*
 * Created by timothy.hallbeck on 1/5/2016.
 * For Lane1Qa / Lane1 use only.
 */

public class mfHomePage extends sfHomePage {
    public mfHomePage() {
        this(null);
    }

    public mfHomePage(WebPage existingPage) {
        super(existingPage);
    }

    public mfHomePage load() {
        login();
        get(mfXpath.MainPage_url);

        if (getByXpath(mfXpath.MainPage_MentorLoginButton, true) != null)
            getByXpath(mfXpath.MainPage_MentorLoginButton).click();
        Page.Sleep(3000);

        if (getByXpath(mfXpath.MainPage_MentorView, true) != null)
            getByXpath(mfXpath.MainPage_MentorView).click();
        else
            getByXpath(mfXpath.MainPage_BackToMentorView).click();
        Page.Sleep(12000);

        return this;
    }

    public mfHomePage clickHomePage() {
        get(mfXpath.MainPage_ConsoleUrl);
        return this;
    }

    public mfHomePage GoToDegreePlan() {
        switchToFrame(0);
        LoadAndSwitchTabs(mfXpath.MainPage_DegreePlanButton);
        LoginType.Login(this, LoginType.LANE1_APOINDEXTER);
        getByXpath(mfXpath.DegreePlan_HeaderTag);

        return this;
    }

    public mfHomePage ExercisePage(boolean cascade) {
        General.Debug("\nmfHomePage::ExercisePage(" + cascade + ")");
        load();
        switchToFrame(0);
        getByXpath(mfXpath.MainPage_StudentListEntry);

        List<WebElement> students = getListByXpath(null, mfXpath.MainPage_StudentListEntry, false);
        int rowCount = students.size() / 39;
        int currentRow = 0;
        do {
            students.get(currentRow * 39).click();
            Page.Sleep(1000);
            students = getListByXpath(null, mfXpath.MainPage_StudentListEntry, false);
        } while (++currentRow < rowCount);

        GoToDegreePlan();
        load();

        return this;
    }
}

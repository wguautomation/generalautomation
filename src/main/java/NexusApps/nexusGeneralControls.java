
package NexusApps;

import Utils.General;
import Utils.wguRobot;
import org.sikuli.script.FindFailed;
import org.sikuli.script.ImagePath;
import org.sikuli.script.Match;
import org.sikuli.script.Screen;

import java.awt.*;

/*
 * Created by timothy.hallbeck on 11/23/2015.
 */

public class nexusGeneralControls extends wguRobot {

    final public Screen screen;

    public nexusGeneralControls() throws AWTException {
        screen = new Screen();
        String path = ImagePath.getBundlePath();
        path += "\\src\\main\\resources";
        ImagePath.setBundlePath(path);
    }

    public void clickBaseCircle() {
        mouseMove(310, 986);
        LeftClickMouse();
        Sleep(1500);
    }

    public void clickBaseTriangle() {
        mouseMove(179, 986);
        LeftClickMouse();
        Sleep(1500);
    }

    public void clickBaseSquare() {
        mouseMove(441, 986);
        LeftClickMouse();
        Sleep(1500);
    }

    static String[] closeButtons = { "closeXBlack.png", "closeXBlue.png", "closeXGreen.png", "closeXRed.png", "closeXWhite.png" };

    public void closeAllApps() throws FindFailed {
        clickBaseCircle();
        clickBaseSquare();
        Sleep(4500);

        boolean foundOne;
        do {
            foundOne = false;
            screen.capture(400, 400, 600, 600);
            for (String button : closeButtons) {
                General.Debug("looking for " + button);
                Match match = screen.exists(button);
                if (match != null) {
                    General.Debug("  found " + button);
                    foundOne = true;
                    match.click();
                    break;
                }
            }
        } while (foundOne);

        hitEscape();
        Sleep(2500);
    }

}

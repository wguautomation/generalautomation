
package MobilePages;

import Utils.General;
import Utils.wguPowerPointFileUtils;

/*
 * Created by timothy.hallbeck on 4/6/2015.
 */
public class MobileNotificationSettingsPage extends MobileDegreePlanPage {

    public MobileNotificationSettingsPage() throws Exception {
        super();
        getNotificationSettings(this).click();
        Sleep(2000);
        WaitForPageLoad(MobileNotificationSettingsPage.class);
    }
    public MobileNotificationSettingsPage(MobileAppPage page) throws Exception {
        super(page);
    }

    public MobileNotificationSettingsPage setNotificationsCheckbox(boolean trueOrFalse) {
        return setNotificationsCheckbox(this, trueOrFalse);
    }
    public static MobileNotificationSettingsPage setNotificationsCheckbox(MobileNotificationSettingsPage page, boolean trueOrFalse) {
        page.setCheckboxByXpath(MobileXpath.NotifSettingsPage_NotificationsBox, trueOrFalse);
        return page;
    }

    public MobileNotificationSettingsPage setNotificationsSoundCheckbox(boolean trueOrFalse) {
        return setNotificationsSoundCheckbox(this, trueOrFalse);
    }
    public static MobileNotificationSettingsPage setNotificationsSoundCheckbox(MobileNotificationSettingsPage page, boolean trueOrFalse) {
        return (MobileNotificationSettingsPage) page.setCheckboxByXpath(MobileXpath.NotifSettingsPage_SoundBox, trueOrFalse);
    }

    public MobileNotificationSettingsPage setVibrateCheckbox(boolean trueOrFalse) {
        return setVibrateCheckbox(this, trueOrFalse);
    }
    public static MobileNotificationSettingsPage setVibrateCheckbox(MobileNotificationSettingsPage page, boolean trueOrFalse) {
        return (MobileNotificationSettingsPage) page.setCheckboxByXpath(MobileXpath.NotifSettingsPage_VibrateBox, trueOrFalse);
    }

    public MobileNotificationSettingsPage setNotificationLightCheckbox(boolean trueOrFalse) {
        return setNotificationLightCheckbox(this, trueOrFalse);
    }
    public static MobileNotificationSettingsPage setNotificationLightCheckbox(MobileNotificationSettingsPage page, boolean trueOrFalse) {
        return (MobileNotificationSettingsPage) page.setCheckboxByXpath(MobileXpath.NotifSettingsPage_LightBox, trueOrFalse);
    }

    public MobileNotificationSettingsPage verifyBasicInfo() {
        General.assertDebug(getByXpath(MobileXpath.NotifSettingsPage_LightBox) != null, "Light checkbox exists");
        General.assertDebug(getByXpath(MobileXpath.NotifSettingsPage_LightLabel) != null, "Light label exists");
        General.assertDebug(getByXpath(MobileXpath.NotifSettingsPage_LightText) != null, "Light desc exists");
        General.assertDebug(getByXpath(MobileXpath.NotifSettingsPage_NotificationsBox) != null, "Notifications checkbox exists");
        General.assertDebug(getByXpath(MobileXpath.NotifSettingsPage_NotificationsLabel) != null, "Notifications label exists");
        General.assertDebug(getByXpath(MobileXpath.NotifSettingsPage_NotificationsText) != null, "Notifications desc exists");
        General.assertDebug(getByXpath(MobileXpath.NotifSettingsPage_SoundBox) != null, "Sound checkbox exists");
        General.assertDebug(getByXpath(MobileXpath.NotifSettingsPage_SoundLabel) != null, "Sound label exists");
        General.assertDebug(getByXpath(MobileXpath.NotifSettingsPage_SoundText) != null, "Sound desc exists");
        General.assertDebug(getByXpath(MobileXpath.NotifSettingsPage_VibrateBox) != null, "Vibrate checkbox exists");
        General.assertDebug(getByXpath(MobileXpath.NotifSettingsPage_VibrateLabel) != null, "Vibrate label exists");
        General.assertDebug(getByXpath(MobileXpath.NotifSettingsPage_VibrateText) != null, "Vibrate desc exists");

        setNotificationsCheckbox(false);
        wguPowerPointFileUtils.saveScreenshot(this, "setNotificationsCheckbox false");
        setNotificationsCheckbox(true);
        wguPowerPointFileUtils.saveScreenshot(this, "setNotificationsCheckbox true");

        setNotificationsSoundCheckbox(false);
        wguPowerPointFileUtils.saveScreenshot(this, "setNotificationsSoundCheckbox false");
        setNotificationsSoundCheckbox(true);
        wguPowerPointFileUtils.saveScreenshot(this, "setNotificationsSoundCheckbox true");

        setVibrateCheckbox(false);
        wguPowerPointFileUtils.saveScreenshot(this, "setVibrateCheckbox false");
        setVibrateCheckbox(true);
        wguPowerPointFileUtils.saveScreenshot(this, "setVibrateCheckbox true");

        setNotificationLightCheckbox(false);
        wguPowerPointFileUtils.saveScreenshot(this, "setNotificationLightCheckbox false");
        setNotificationLightCheckbox(true);
        wguPowerPointFileUtils.saveScreenshot(this, "setNotificationLightCheckbox true");

        return this;
    }

    public MobileNotificationSettingsPage doSomethingRandom(int count) throws Exception {
        General.Debug("MobileNotificationSettingsPage::doSomethingRandom()");

        while (count-- > 0) {
            NavigateToPage(MobileNotificationSettingsPage.class);
            switch (General.getRandomInt(8)) {
                case 0:
                    setNotificationsCheckbox(false);
                    break;
                case 1:
                    setNotificationsCheckbox(true);
                    break;
                case 2:
                    setNotificationsCheckbox(true);
                    setNotificationsSoundCheckbox(true);
                    break;
                case 3:
                    setNotificationsCheckbox(true);
                    setNotificationsSoundCheckbox(false);
                    break;
                case 4:
                    setNotificationsCheckbox(true);
                    setVibrateCheckbox(true);
                    break;
                case 5:
                    setNotificationsCheckbox(true);
                    setVibrateCheckbox(false);
                    break;
                case 6:
                    setNotificationsCheckbox(true);
                    setNotificationLightCheckbox(true);
                    break;
                case 7:
                    setNotificationsCheckbox(true);
                    setNotificationLightCheckbox(false);
                    break;
                default:
                    break;
            }
        }

        return this;
    }

}

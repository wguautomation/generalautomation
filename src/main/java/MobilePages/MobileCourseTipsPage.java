
package MobilePages;

import Utils.General;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

/*
 * Created by timothy.hallbeck on 4/24/2015.
 */
public class MobileCourseTipsPage extends MobileStudyPlanPage {

    public MobileCourseTipsPage() throws Exception {
        super();
        getCourseTips().click();
        Sleep(2000);
        WaitForPageLoad(MobileCourseTipsPage.class);
    }
    public MobileCourseTipsPage(MobileAppPage page) throws Exception {
        super(page);
    }

    public boolean verifyEmptyTips() {
        WaitForPageLoad(MobileCourseTipsPage.class);
        WebElement element = getByXpath(MobileXpath.CourseTipsPage_Empty, true);

        if (element == null)
            return false;

        if (element.isDisplayed()) {
            String text = element.getText();
            Assert.assertTrue(text.contains("There are no tips"), "Empty announcement text does not match expected text");
        }

        return true;
    }

    // Not broken out into individual items currently 1.17.3
    public boolean verifyTips() {
        WaitForPageLoad(MobileCourseTipsPage.class);
        if (getByXpath(MobileXpath.Generic_WebViewModified, true) != null && getByXpath(MobileXpath.Generic_CardView, true) != null)
            return true;

        return false;
    }

    public MobileCourseTipsPage verifyBasicInfo() {
        WaitForPageLoad(MobileCourseTipsPage.class);

        VerifyCourseHeader();
        getByXpath(MobileXpath.Generic_WebViewModified);
        General.assertDebug(verifyTips() || verifyEmptyTips(), "Neither Tips nor empty Tips controls present");

        return this;
    }

    public MobileCourseTipsPage doSomethingRandom(int count) throws Exception {
        General.Debug("MobileCourseTipsPage::doSomethingRandom()");

        while (count-- > 0) {
            NavigateToPage(MobileCourseTipsPage.class);
            switch (General.getRandomInt(2)) {
                case 0:
                    break;
                case 1:
                    break;
                default:
                    break;
            }
        }

        return this;
    }
}


package MobilePages;

import Utils.General;
import Utils.wguPowerPointFileUtils;
import org.openqa.selenium.WebElement;

/*
 * Created by timothy.hallbeck on 10/5/2015.
 */

public class MobileLearningResourcesPage extends MobileStudyPlanPage {

    public MobileLearningResourcesPage() throws Exception {
        this(0L);
    }
    public MobileLearningResourcesPage(MobileAppPage page) throws Exception {
        super(page);
    }
    public MobileLearningResourcesPage(long presets) throws Exception {
        super(presets);
        getLearningResourceTips().click();
        WaitForPageLoad(MobileLearningResourcesPage.class);
        wguPowerPointFileUtils.saveScreenshot(this, "MobileLearningResourcesPage()");
    }

    public MobileAppPage clickPacingGuide() {
        return clickPacingGuide(this);
    }
    public static MobileAppPage clickPacingGuide(MobileAppPage page) {
        General.Debug("clickPacingGuide()");
        page.getByXpath(MobileXpath.StudyPlanPage_PreparingForSuccess).click();
        page.getByXpath(MobileXpath.LearningResourcesPage_PacingGuide).click();

        return page;
    }

    public WebElement getCohorts() {
        return getCohorts(this);
    }
    public static WebElement getCohorts(MobileAppPage page) {
        General.Debug("getCohorts()");

        return page.getByXpath(MobileXpath.LearningResourcesPage_Cohorts);
    }

    public WebElement getTips() {
        return getTips(this);
    }
    public static WebElement getTips(MobileAppPage page) {
        General.Debug("getTips()");

        return page.getByXpath(MobileXpath.LearningResourcesPage_Tips);
    }

    public MobileLearningResourcesPage verifyBasicInfo() {
        WaitForPageLoad(MobileLearningResourcesPage.class);
 /*       clickPacingGuide();
        Sleep(1000);
        wguPowerPointFileUtils.saveScreenshot(this, "after getPacingGuide");
        if (Data.isPhone())
            clickBackButton();
*/
        getLearningResourceTips().click();
        getTips().click();
        Sleep(1000);
        wguPowerPointFileUtils.saveScreenshot(this, "after getTips");
        if (Data.isPhone())
            clickBackButton();

        return this;
    }

    public MobileLearningResourcesPage doSomethingRandom(int originalCount) throws Exception {
        General.Debug("MobileLearningResourcesPage::doSomethingRandom()");
        NavigateToPage(MobileStudyPlanPage.class);

        int count = originalCount;
        while (count-- > 0) {
            getLearningResourceTips().click();

            switch (General.getRandomInt(3)) {
                case 0:
 //                   clickPacingGuide();
 //                   break;
                case 1:
//                    getCohorts().click();
                    break;
                case 2:
                    getTips().click();
                    break;
                default:
                    break;
            }
            General.Debug("MobileLearningResourcesPage::doSomethingRandom(" + count + " of " + originalCount + ")");
        }

        return this;
    }
}


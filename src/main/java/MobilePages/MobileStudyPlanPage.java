
package MobilePages;

import Utils.Course;
import Utils.General;
import Utils.wguPowerPointFileUtils;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.Iterator;
import java.util.List;

/*
 * Created by timothy.hallbeck on 4/10/2015.
 */

public class MobileStudyPlanPage extends MobileDegreePlanPage {

    public MobileStudyPlanPage() throws Exception {
        this(0L);
    }
    public MobileStudyPlanPage(MobileAppPage page) throws Exception {
        super(page);
    }
    public MobileStudyPlanPage(long presets) throws Exception {
        super(presets);
        Sleep(3000);
        ScrollToCourse(this, "English").click();
        WaitForPageLoad(MobileStudyPlanPage.class);
        wguPowerPointFileUtils.saveScreenshot(this, "MobileStudyPlanPage()");
    }

    public static List<WebElement> getListOfSubjects(MobileAppPage page, boolean bRemoveStandardItems) {
        General.Debug("getListOfSubjects()");
        List<WebElement> list = page.getListByXpath(MobileXpath.StudyPlanPage_Title);

        String text;
        Iterator iterator = list.iterator();

        General.Debug("getListOfSubjects() Begin");
        while(iterator.hasNext()) {
            text = ((WebElement) iterator.next()).getText();
            if (bRemoveStandardItems &&
                    (text.contains("Announcements") || text.contains("Assessments")           || text.contains("Tips") ||
                     text.contains("Introduction")  || text.contains("Preparing for Success") || text.contains("ADA Policy") ||
                     text.contains("Final Steps")   || text.contains("The WGU Library")       || text.contains("The Student Success Center") ||
                     text.contains("Feedback")      || text.contains("Accessibility Policy")  || text.contains("Download Course as PDF") ||
                     text.contains("Activities") )) {
                iterator.remove();
                General.Debug("Removing " + text);
            } else
                General.Debug(text);
        }
        General.Debug("getListOfSubjects() End");

        return list;
    }

    public static MobileAppPage checkEmailCourseMentor(MobileAppPage page) {
        page.WaitForPageLoad(MobileStudyPlanPage.class);
        wguPowerPointFileUtils.saveScreenshot(page, "check for EmailCourseMentor icon");
        getEmailCourseMentor(page);

        return page;
    }

    public static MobileAppPage clickSubject(MobileAppPage page, boolean bRemoveStandardItems, int index, String subject) {
        General.Debug("clickSubject(" + bRemoveStandardItems + ", " + index + ", " + subject + ")");
        page.WaitForPageLoad(MobileStudyPlanPage.class);
        List<WebElement> list = getListOfSubjects(page, bRemoveStandardItems);

        if (index >= 0) {
            WebElement element = list.get(index);
            element.click();
        } else {
            for (WebElement element : list)
                if (element.getText().contains(subject)) {
                    element.click();
                    break;
                }
        }
        Sleep(1000);
        wguPowerPointFileUtils.saveScreenshot(page, "clickSubject()");
        return page;
    }
    public MobileAppPage clickSubject(boolean bRemoveStandardItems, int index) {
        return clickSubject(this, bRemoveStandardItems, index, "");
    }
    public MobileAppPage clickSubject(String subject) {
        return clickSubject(this, true, -1, subject);
    }

    public WebElement getCourseAnnouncements() {
        return getCourseAnnouncements(this);
    }
    public static WebElement getCourseAnnouncements(MobileAppPage page) {
        General.Debug("getCourseAnnouncements()");
        page.swipeDown(3, swipeSide.leftSide);
        page.WaitForPageLoad(MobileStudyPlanPage.class);

        return page.getByXpath(MobileXpath.StudyPlanPage_Announcements);
    }

    public WebElement getCourseTips() {
        return getCourseTips(this);
    }
    public static WebElement getCourseTips(MobileAppPage page) {
        General.Debug("getCourseTips()");
        page.swipeDown(3, swipeSide.leftSide);
        page.WaitForPageLoad(MobileStudyPlanPage.class);

        return page.getByXpath(MobileXpath.StudyPlanPage_Tips);
    }

    public WebElement getCourseAssessments() {
        General.assertDebug(true, "getCourseAssessments()");
        swipeDown(3, swipeSide.leftSide);
        WaitForPageLoad(MobileStudyPlanPage.class);

        return getByXpath(MobileXpath.StudyPlanPage_Assessments);
    }

    public static WebElement getEmailCourseMentor(MobileAppPage page) {
        General.Debug("getEmailCourseMentor()");
        page.swipeDown(3, swipeSide.rightSide);

        return page.getByXpath(MobileXpath.StudyPlanPage_Email);
    }

    public WebElement getCourseIntroduction() {
        General.Debug("getCourseIntroduction()");
        swipeDown(3, swipeSide.leftSide);
        WaitForPageLoad(MobileStudyPlanPage.class);

        return getByXpath(MobileXpath.StudyPlanPage_Introduction);
    }

    public WebElement getLearningResourceTips() {
        General.Debug("getLearningResourceTips()");
        swipeDown(3, swipeSide.leftSide);
        WaitForPageLoad(MobileStudyPlanPage.class);

        return getByXpath(MobileXpath.StudyPlanPage_LearningResourceTips);
    }

    public WebElement getADAPolicy() {
        General.Debug("getADAPolicy()");
        swipeDown(3, swipeSide.leftSide);
        WaitForPageLoad(MobileStudyPlanPage.class);
        swipeUp(3, swipeSide.leftSide);

        return getByXpath(MobileXpath.StudyPlanPage_ADAPolicy);
    }

    public WebElement getSupport() {
        General.Debug("getSupport()");
        swipeDown(3, swipeSide.leftSide);
        WaitForPageLoad(MobileStudyPlanPage.class);
        swipeUp(3, swipeSide.leftSide);

        return getByXpath(MobileXpath.StudyPlanPage_Support);
    }

    public MobileStudyPlanPage verifyDocumentDownload() {
        General.Debug("verifyDocumentDownload()");
        swipeUp(3, swipeSide.leftSide);
        getByXpath(MobileXpath.StudyPlanPage_Download).click();
        wguPowerPointFileUtils.saveScreenshot(this, "downloading pdf");
// check the new Saved Files area possibly
        return this;
    }

    public MobileStudyPlanPage verifyCourseDetails() {
        WaitForPageLoad(MobileStudyPlanPage.class);
        wguPowerPointFileUtils.saveScreenshot(this, "verifyCourseDetails()");

        String courseTitle = getCourseTitle();
        Course course = Course.getCourse(courseTitle.substring(0, 4));
        wguPowerPointFileUtils.addBulletPoint("checking courseTitle");
        String[] subjects = course.getCourseCompetencies();

        for (String singleSubject : subjects) {
            General.Debug("Looking for " + singleSubject, true);
            Assert.assertNotNull(getListItemByXpath(MobileXpath.StudyPlanPage_Title, singleSubject) == null, "Did not find subject " + singleSubject);
        }

        return this;
    }

    public String getCourseTitle() {
        return getByXpath(MobileXpath.Header_TitleText).getText();
    }

    public MobileStudyPlanPage verifyBasicInfo() {
        WaitForPageLoad(MobileStudyPlanPage.class);
        VerifyCourseHeader();
        wguPowerPointFileUtils.addBulletPoint("checking Header_BackButton");
        getByXpath(MobileXpath.Header_BackButton);

        getCourseAnnouncements().click();
        Sleep(2000);
        wguPowerPointFileUtils.saveScreenshot(this, "after getCourseAnnouncements");
        if (Data.isPhone())
            clickBackButton();

        getCourseTips().click();
        Sleep(2000);
        wguPowerPointFileUtils.saveScreenshot(this, "after getCourseTips");
        if (Data.isPhone())
            clickBackButton();

        getCourseAssessments().click();
        Sleep(2000);
        wguPowerPointFileUtils.saveScreenshot(this, "after getCourseAssessments");
        if (Data.isPhone())
            clickBackButton();

        getCourseIntroduction().click();
        Sleep(2000);
        wguPowerPointFileUtils.saveScreenshot(this, "after getCourseIntroduction");
        if (Data.isPhone())
            clickBackButton();

        getLearningResourceTips().click();
        Sleep(2000);
        wguPowerPointFileUtils.saveScreenshot(this, "after getLearningResourceTips");
        if (Data.isPhone())
            clickBackButton();

        getADAPolicy().click();
        Sleep(2000);
        wguPowerPointFileUtils.saveScreenshot(this, "after getADAPolicy");
        if (Data.isPhone())
            clickBackButton();

        wguPowerPointFileUtils.addBulletPoint("looking for Support");
        getSupport().click();
        Sleep(2000);
        wguPowerPointFileUtils.saveScreenshot(this, "after getSupport");
        if (Data.isPhone())
            clickBackButton();

        return this;
    }

    public MobileStudyPlanPage doSomethingRandom(int count) throws Exception {
        General.Debug("MobileStudyPlanPage::doSomethingRandom()");

        while (count-- > 0) {
            NavigateToPage(MobileStudyPlanPage.class);
            switch (General.getRandomInt(2)) {
                case 0:
                    break;
                case 1:
                    break;
                default:
                    break;
            }
        }

        return this;
    }
}


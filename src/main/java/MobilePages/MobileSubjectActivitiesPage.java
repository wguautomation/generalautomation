
package MobilePages;

import Utils.General;
import Utils.wguPowerPointFileUtils;
import org.openqa.selenium.WebElement;

/*
 * Created by timothy.hallbeck on 4/27/2015.
 */

public class MobileSubjectActivitiesPage extends MobileSubjectDetailsPage {

    public MobileSubjectActivitiesPage() throws Exception {
        super();
        Sleep(1000);
        getListOfActivities().get(0).click();
        WaitForPageLoad(MobileSubjectActivitiesPage.class);
    }
    public MobileSubjectActivitiesPage(MobileAppPage page) throws Exception {
        super(page);
    }

    public WebElement getMarkCompleted() {
        return getByXpath(MobileXpath.SubjectActivitiesPage_MarkCompleted);
    }

    public WebElement getSkip() {
        return getByXpath(MobileXpath.SubjectActivitiesPage_Skip);
    }

    public MobileSubjectActivitiesPage verifyBasicInfo() {
        WaitForPageLoad(MobileSubjectActivitiesPage.class);
        VerifyCourseHeader();
        wguPowerPointFileUtils.addBulletPoint("verifying SubjectActivitiesPage_TopicName");
        getByXpath(MobileXpath.SubjectActivitiesPage_TopicName);
        wguPowerPointFileUtils.addBulletPoint("verifying SubjectActivitiesPage_ActivityName");
        getByXpath(MobileXpath.SubjectActivitiesPage_ActivityName);
        wguPowerPointFileUtils.addBulletPoint("verifying SubjectActivitiesPage_MarkedAs");
        getByXpath(MobileXpath.SubjectActivitiesPage_MarkedAs);

        wguPowerPointFileUtils.addBulletPoint("verifying MarkCompleted");
        getMarkCompleted();
        wguPowerPointFileUtils.addBulletPoint("verifying Skip");
        getSkip();

        return this;
    }

    public MobileSubjectActivitiesPage doSomethingRandom(int count) throws Exception {
        while (count-- > 0) {
            NavigateToPage(MobileContactWGUPage.class);
            switch (General.getRandomInt(2)) {
                case 0:
                    getMarkCompleted().click();
                    Sleep(2000);
                    getMarkCompleted().click();
                    Sleep(2000);
                    break;
                case 1:
                    getSkip().click();
                    Sleep(2000);
                    getSkip().click();
                    Sleep(2000);
                    break;
                default:
                    break;
            }
        }

        return this;
    }
}


package MobilePages;

import Utils.General;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

/*
 * Created by timothy.hallbeck on 4/24/2015.
 */
public class MobileCourseAnnouncementsPage extends MobileStudyPlanPage {

    public MobileCourseAnnouncementsPage() throws Exception {
        super();
        getCourseAnnouncements().click();
        WaitForPageLoad(MobileCourseAnnouncementsPage.class);
    }
    public MobileCourseAnnouncementsPage(MobileAppPage page) throws Exception {
        super(page);
    }

    public MobileCourseAnnouncementsPage verifyBasicInfo() {
        WaitForPageLoad(MobileCourseAnnouncementsPage.class);

        VerifyCourseHeader();
        General.assertDebug(getByXpath(MobileXpath.CourseAnnouncementsPage_Title) != null, "Title present");
        General.assertDebug(getByXpath(MobileXpath.Generic_WebViewModified) != null, "Content present");

        return this;
    }

    public WebElement getOverview() {
        WaitForPageLoad(MobileCourseAnnouncementsPage.class);

        return getByXpath(MobileXpath.CourseAnnouncementsPage_Overview);
    }

    // This is not currently well testable. The id_empty_announcement control is always present, and non-empty text we can't get to.
    public MobileCourseAnnouncementsPage verifyEmptyAnnouncement() {
        General.Debug("verifyEmptyAnnouncement()");
        WaitForPageLoad(MobileCourseAnnouncementsPage.class);
        boolean bVisible = getByXpath(MobileXpath.CourseAnnouncementsPage_EmptyMessage).isDisplayed();

        if (bVisible) {
            String text = getByXpath(MobileXpath.CourseAnnouncementsPage_EmptyMessage).getText();
            Assert.assertTrue(text.contains("There are no announcements for this course"), "Empty announcement text does not match expected text");
        }

        return this;
    }

    public MobileCourseAnnouncementsPage doSomethingRandom(int count) throws Exception {
        General.Debug("MobileCourseAnnouncementsPage::doSomethingRandom()");

        while (count-- > 0) {
            NavigateToPage(MobileCourseAnnouncementsPage.class);
            switch (General.getRandomInt(2)) {
                case 0:
                    break;
                case 1:
                    break;
                default:
                    break;
            }
        }

        return this;
    }
}


package MobilePages;

import Utils.General;
import Utils.wguPowerPointFileUtils;
import org.openqa.selenium.WebElement;

import java.util.List;

/*
 * Created by timothy.hallbeck on 5/28/2015.
 */

public class MobileTopicPage extends MobileSubjectDetailsPage {

    public MobileTopicPage() throws Exception {
        super();
        clickTopic(0);
        Sleep(2000);
        WaitForPageLoad(MobileTopicPage.class);
    }
    public MobileTopicPage(MobileAppPage page) throws Exception {
        super(page);
    }

    public List<WebElement> getListOfActivityTopics() {
        return getListByXpath(MobileXpath.TopicPage_Activities);
    }

    public MobileAppPage clickTopActivity() {
        return clickTopActivity(this);
    }
    public static MobileAppPage clickTopActivity(MobileAppPage page) {
        page.WaitForPageLoad(MobileTopicPage.class);
        page.getByXpath(MobileXpath.TopicPage_SubjectTitle).click();
        page.Sleep(3000);
        wguPowerPointFileUtils.saveScreenshot(page, "after clickTopActivity");

        return page;
    }

    public static MobileAppPage clickSkipCourse(MobileAppPage page) {
        page.WaitForPageLoad(MobileTopicPage.class);
        page.clickActiveItemByXpath(MobileXpath.TopicPage_Skip);
        Sleep(1000);
        wguPowerPointFileUtils.saveScreenshot(page, " after clickSkipCourse");

        return page;
    }

    public static MobileAppPage clickCompleteCourse(MobileAppPage page) {
        page.WaitForPageLoad(MobileTopicPage.class);
        page.clickActiveItemByXpath(MobileXpath.TopicPage_MarkCompleted);
        wguPowerPointFileUtils.saveScreenshot(page, "after clickCompleteCourse");

        return page;
    }

    public MobileTopicPage doSomethingRandom(int count) throws Exception {
        General.Debug("MobileTopicPage::doSomethingRandom()");

        while (count-- > 0) {
            NavigateToPage(MobileTopicPage.class);
            switch (General.getRandomInt(2)) {
                case 0:
                    break;
                case 1:
                    break;
                default:
                    break;
            }
        }

        return this;
    }
}


package MobilePages;

/*
 * Created by timothy.hallbeck on 8/27/2015.
 */

import Utils.General;
import org.openqa.selenium.WebElement;

public class MobileResourcesPage extends MobileAppPage {

    public MobileResourcesPage() throws Exception {
        super();
        login();
        getResources(this).click();
        WaitForPageLoad(MobileResourcesPage.class);
    }
    public MobileResourcesPage(MobileAppPage page) throws Exception {
        super(page);
    }

    public WebElement getContactWGU() {
        return getContactWGU(this);
    }
    public static WebElement getContactWGU(MobileAppPage page) {
        return page.getByXpath(MobileXpath.ResourcesPage_ContactWGU);
    }

    public MobileResourcesPage verifyBasicInfo() {
        WaitForPageLoad(MobileResourcesPage.class);

        getByXpath(MobileXpath.ResourcesPage_Academics).click();
        getByXpath(MobileXpath.ResourcesPage_ApaRef).click();
        getByXpath(MobileXpath.ResourcesPage_CompCapstone).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_DegreeResources).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_ModelCapstone).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_StudentVideo).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_TaskStreamHelp).click();
        Sleep(8000);
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_Turnitin).click();
        getByXpath(MobileXpath.ResourcesPage_Academics).click();

        getByXpath(MobileXpath.ResourcesPage_Administrative).click();
        getByXpath(MobileXpath.ResourcesPage_EnrollHistory);
        getByXpath(MobileXpath.ResourcesPage_EnrollHistory).click();
        Sleep(3000);
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_GradeReport).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_RequestId).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_RequestTrans).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_UnofficialTrans).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_Administrative).click();

        getByXpath(MobileXpath.ResourcesPage_ContactWGU).click();
        Sleep(3000);
        clickBackButton();

        getByXpath(MobileXpath.ResourcesPage_Forms).click();
        getByXpath(MobileXpath.ResourcesPage_AccomRequest).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_CoverSheet).click();
        getByXpath(MobileXpath.ResourcesPage_FERPA).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_StudentSuccess).click();
        Sleep(8000);
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_TermBreak).click();
        Sleep(4000);
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_Forms).click();

        getByXpath(MobileXpath.ResourcesPage_Student).click();
        getByXpath(MobileXpath.ResourcesPage_CampusStore).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_ReferAFriend).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_ConsumerGuide).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_Student).click();

        getByXpath(MobileXpath.ResourcesPage_University).click();
        getByXpath(MobileXpath.ResourcesPage_CareerCenter).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_StudentPortal).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_StudentSuccessCenter).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_TechDeals).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_NightOwlBlog).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_NightOwlArchives).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_WGUVideos).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_WGUNews).click();
        clickBackButton();
        getByXpath(MobileXpath.ResourcesPage_University).click();

        getByXpath(MobileXpath.ResourcesPage_Handbook).click();
        Sleep(4000);
        clickBackButton();

        getByXpath(MobileXpath.ResourcesPage_Library).click();
        clickBackButton();

        return this;
    }

    public MobileResourcesPage doSomethingRandom(int count) throws Exception {
        General.Debug("MobileResourcesPage::doSomethingRandom()", true);

        NavigateToPage(MobileResourcesPage.class);
        while (count-- > 0) {
            switch (General.getRandomInt(9)) {
                case 0:
                    getByXpath(MobileXpath.ResourcesPage_Academics).click();
                    getByXpath(MobileXpath.ResourcesPage_Academics).click();
                    break;
                case 1:
                    getByXpath(MobileXpath.ResourcesPage_Administrative).click();
                    getByXpath(MobileXpath.ResourcesPage_Administrative).click();
                    break;
                case 2:
                    getByXpath(MobileXpath.ResourcesPage_ContactWGU).click();
                    clickBackButton();
                    break;
                case 3:
                    getByXpath(MobileXpath.ResourcesPage_Forms).click();
                    getByXpath(MobileXpath.ResourcesPage_Forms).click();
                    break;
                case 4:
                    getByXpath(MobileXpath.ResourcesPage_Handbook).click();
                    Sleep(4000);
                    clickBackButton();
                    break;
                case 5:
                    getByXpath(MobileXpath.ResourcesPage_Library).click();
                    Sleep(4000);
                    clickBackButton();
                    break;
                case 6:
//                    getByXpath(MobileXpath.ResourcesPage_Social).click();
//                    clickBackButton();
                    break;
                case 7:
                    getByXpath(MobileXpath.ResourcesPage_Student).click();
                    getByXpath(MobileXpath.ResourcesPage_Student).click();
                    break;
                case 8:
                    getByXpath(MobileXpath.ResourcesPage_University).click();
                    getByXpath(MobileXpath.ResourcesPage_University).click();
                    break;
                default:
                    break;
            }
        }

        return this;
    }
}

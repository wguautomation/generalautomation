
package MobilePages;

import Utils.General;
import Utils.wguPowerPointFileUtils;
import org.openqa.selenium.WebElement;

import java.util.List;

/*
 * Created by timothy.hallbeck on 6/29/2015.
 */
public class MobileAppCenterPage extends MobileAppPage {

    public MobileAppCenterPage() throws Exception {
        super();
        login();
        getAppCenter(this).click();
        WaitForPageLoad(MobileAppCenterPage.class);
    }
    public MobileAppCenterPage(MobileAppPage page) throws Exception {
        super(page);
    }

    public MobileAppCenterPage verifyBasicInfo() {
        WaitForPageLoad(MobileAppCenterPage.class);
        wguPowerPointFileUtils.saveScreenshot(this, "verifyBasicInfo1");
        getByXpath(MobileXpath.Hamburger_AppCenter);
        assert(getByXpath(MobileXpath.AppCenterPage_Desc).getText().contains("applications allow you to leverage the power of your mobile device"));

        List<WebElement> list = getListByXpath(MobileXpath.AppCenterPage_AppName);
        assert(getListItem(list, "Adobe Connect") != null);
        assert(getListItem(list, "Bluefire Reader") != null);
        assert(getListItem(list, "Bookshelf") != null);
        assert(getListItem(list, "Google Drive") != null);
        assert(getListItem(list, "Lynda.com") != null);

        swipeUp(1);
        list.clear();
        wguPowerPointFileUtils.saveScreenshot(this, "verifyBasicInfo2");
        list = getListByXpath(MobileXpath.AppCenterPage_AppName);
        assert(getListItem(list, "Panopto") != null);
        assert(getListItem(list, "Quizlet") != null);
        assert(getListItem(list, "Schools App") != null);

        return this;
    }

    public MobileAppCenterPage doSomethingRandom(int count) throws Exception {
        General.Debug("MobileAppCenterPage::doSomethingRandom() has no actions, as nothing here is reachable.");

        return this;
    }

}

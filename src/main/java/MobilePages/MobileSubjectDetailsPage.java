
package MobilePages;

import Utils.General;
import Utils.wguPowerPointFileUtils;
import org.openqa.selenium.WebElement;

import java.util.List;

/*
 * Created by timothy.hallbeck on 4/23/2015.
 */

public class MobileSubjectDetailsPage extends MobileStudyPlanPage {

    public MobileSubjectDetailsPage() throws Exception {
        super();
        clickSubject("English");
        WaitForPageLoad(MobileSubjectDetailsPage.class);
    }
    public MobileSubjectDetailsPage(MobileAppPage page) throws Exception {
        super(page);
    }

    public List<WebElement> getListOfActivities() {
        return getListOfActivities(this);
    }
    public static List<WebElement> getListOfActivities(MobileAppPage page) {
        General.Debug("getListOfActivities()");
        List<WebElement> list = page.getListByXpath(MobileXpath.SubjectDetailsPage_ActivityTitle);

        return list;
    }

    public List<WebElement> getListOfTopics() {
        return getListOfTopics(this);
    }
    public static List<WebElement> getListOfTopics(MobileAppPage page) {
        General.Debug("getListOfTopics()");
        List<WebElement> list = page.getListByXpath(MobileXpath.SubjectDetailsPage_TopicTitle);

        return list;
    }

    public MobileAppPage clickCourseChatterIcon() {
        return clickCourseChatterIcon(this);
    }
    public static MobileAppPage clickCourseChatterIcon(MobileAppPage page) {
        page.WaitForPageLoad(MobileSubjectDetailsPage.class);
        page.getByXpath(MobileXpath.Header_CourseChatter).click();
        page.Sleep(3000);
        wguPowerPointFileUtils.saveScreenshot(page, "after clickCourseChatterIcon()");

        return page;
    }

    public MobileAppPage clickTopic(int index) {
        return clickTopic(this, index);
    }
    public static MobileAppPage clickTopic(MobileAppPage page, int index) {
        page.WaitForPageLoad(MobileSubjectDetailsPage.class);
        MobileSubjectDetailsPage.getListOfTopics(page).get(index).click();
        page.Sleep(3000);
        wguPowerPointFileUtils.saveScreenshot(page, "after clickTopic()");

        return page;
    }

    public MobileSubjectDetailsPage verifyBasicInfo() {
        WaitForPageLoad(MobileSubjectDetailsPage.class);
        VerifyCourseHeader();
        wguPowerPointFileUtils.saveScreenshot(this, "MobileSubjectDetailsPage verifyBasicInfo()");

        // Have to open the topics to make the activities clickable
        List<WebElement> topics = getListOfTopics();
        for (WebElement element : topics)
            element.click();
        wguPowerPointFileUtils.saveScreenshot(this, "MobileSubjectDetailsPage open topics()");

        // Toggle the first visible one twice
        List<WebElement> completedButtons = getListByXpath(MobileXpath.TopicPage_MarkCompleted);
        for (WebElement element : completedButtons)
            if (element.isDisplayed() && element.isEnabled()) {
                element.click();
                wguPowerPointFileUtils.saveScreenshot(this, "MobileSubjectDetailsPage toggle completed");
                element.click();
                wguPowerPointFileUtils.saveScreenshot(this, "MobileSubjectDetailsPage toggle completed again");
                break;
            }

        return this;
    }

    public MobileSubjectDetailsPage doSomethingRandom(int count) throws Exception {
        General.Debug("MobileSubjectDetailsPage::doSomethingRandom()");

        while (count-- > 0) {
            NavigateToPage(MobileSubjectDetailsPage.class);
            switch (General.getRandomInt(2)) {
                case 0:
                    break;
                case 1:
                    break;
                default:
                    break;
            }
        }

        return this;
    }

}

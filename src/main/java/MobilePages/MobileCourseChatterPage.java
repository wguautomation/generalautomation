
package MobilePages;

import Utils.General;
import Utils.wguPowerPointFileUtils;
import org.openqa.selenium.WebElement;

import java.util.List;

/*
 * Created by timothy.hallbeck on 6/12/2015.
 */

public class MobileCourseChatterPage extends MobileSubjectDetailsPage {

    public MobileCourseChatterPage() throws Exception {
        super();
        clickCourseChatterIcon();
        WaitForPageLoad(MobileCourseChatterPage.class);
    }
    public MobileCourseChatterPage(MobileAppPage page) throws Exception {
        super(page);
    }

    public List<WebElement> getListOfChats() {
        General.Debug("getListOfChats()");
        List<WebElement> list = getListByXpath(MobileXpath.CourseChatterPage_List);

        String text;
        for (WebElement element : list) {
            text = element.getText();
            General.Debug(text);
        }
        return list;
    }

    public MobileCourseChatterPage clickChat(int index) {
        WaitForPageLoad(MobileCourseChatterPage.class);
        getListOfChats().get(index).click();
        Sleep(3000);

        return this;
    }

    public MobileCourseChatterPage clickAndCheckChatterDetails() {
        General.Debug("MobileCourseChatterPage clickAndCheckChatterDetails()");
        WaitForPageLoad(MobileCourseChatterPage.class);

        getByXpath(MobileXpath.CourseChatterPage_Post).click();
        Sleep(2000);
        getByXpath(MobileXpath.CourseChatterDetailsPage_Username);
        getByXpath(MobileXpath.CourseChatterDetailsPage_Image);
        getByXpath(MobileXpath.CourseChatterDetailsPage_Comments);
        getByXpath(MobileXpath.CourseChatterDetailsPage_Timestamp);
        getByXpath(MobileXpath.CourseChatterDetailsPage_StarImage);
        wguPowerPointFileUtils.saveScreenshot(this, "WaitForMobileCourseChatterPage");

        return this;
    }

    public MobileCourseChatterPage verifyBasicInfo() {
        WaitForPageLoad(MobileCourseChatterPage.class);
        wguPowerPointFileUtils.saveScreenshot(this, "verifyBasicInfo");
        General.Debug("MobileCourseChatterPage verifyBasicInfo()");

        General.assertDebug(getByXpath(MobileXpath.PageTitle_CourseChatter) != null, MobileXpath.PageTitle_CourseChatter + " exists");
        General.assertDebug(getByXpath(MobileXpath.CourseChatterPage_MentorPic) != null, "pic / icon exists");
        General.assertDebug(getByXpath(MobileXpath.CourseChatterPage_MentorNameDegree) != null, "name & degress exist");
        General.assertDebug(getByXpath(MobileXpath.CourseChatterPage_ChatterPost) != null, "chatter post exists");
        General.assertDebug(getByXpath(MobileXpath.CourseChatterPage_Timestamp) != null, "timestamp exists");
        General.assertDebug(getByXpath(MobileXpath.CourseChatterPage_StarPic) != null, "reply indicator exists");

        return this;
    }

    public MobileCourseChatterPage doSomethingRandom(int count) throws Exception {
        General.Debug("MobileCourseChatterPage::doSomethingRandom()");
        NavigateToPage(MobileCourseChatterPage.class);

        while (count-- > 0) {
            List elements = getListOfChats();
            WebElement element = (WebElement) elements.get(General.getRandomInt(elements.size() - 1));
            element.click();
            Sleep(1000);
            clickBackButton();
        }

        return this;
    }
}

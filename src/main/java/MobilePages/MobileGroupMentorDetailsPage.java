
package MobilePages;

import Utils.General;
import org.openqa.selenium.WebElement;
import org.testng.SkipException;

import java.util.List;

/*
 * Created by timothy.hallbeck on 5/4/2015.
 */

public class MobileGroupMentorDetailsPage extends MobileMentorsPage {

    public MobileGroupMentorDetailsPage() throws Exception {
        super();
        Sleep(2000);
        clickTopMentor();
        WaitForPageLoad(MobileGroupMentorDetailsPage.class);
    }
    public MobileGroupMentorDetailsPage(MobileAppPage page) throws Exception {
        super(page);
    }

    public MobileAppPage clickTopMentor() {
        return clickTopMentor(this);
    }
    public static MobileAppPage clickTopMentor(MobileAppPage page) {
        List<WebElement> list = page.getListOfMentorNames();
        if (list.size() <= 1)
            General.SkipTest("No group or course mentors exist for this student, skipping test");

        list.get(1).click();
        return page;
    }

    public MobileGroupMentorDetailsPage verifyBasicInfo() {
        WaitForPageLoad(MobileGroupMentorDetailsPage.class);

        General.assertDebug(getByXpath(MobileXpath.GMDetailsPage_MentorProfilePic) != null, "A profile pic is present");
        General.assertDebug(getByXpath(MobileXpath.GMDetailsPage_MentorName) != null, "Group name is present");

        General.assertDebug(getByXpath(MobileXpath.GMDetailsPage_PhoneTitle) != null, "Phone title is present");
        General.assertDebug(getByXpath(MobileXpath.GMDetailsPage_PhoneImage) != null, "Phone image is present");
        General.assertDebug(getByXpath(MobileXpath.GMDetailsPage_PhoneNumber) != null, "Phone number is present");

        General.assertDebug(getByXpath(MobileXpath.GMDetailsPage_EmailTitle) != null, "Email title is present");
        General.assertDebug(getByXpath(MobileXpath.GMDetailsPage_EmailImage) != null, "Email icon is present");
        General.assertDebug(getByXpath(MobileXpath.GMDetailsPage_EmailAddress) != null, "Email address is present");

        General.assertDebug(getByXpath(MobileXpath.GMDetailsPage_AddButton) != null, "Add contact '+' button is present");
        General.assertDebug(getByXpath(MobileXpath.GMDetailsPage_OfficeHours) != null, "");

        return this;
    }

    public MobileGroupMentorDetailsPage doSomethingRandom(int count) throws Exception {
        General.Debug("MobileGroupMentorDetailsPage::doSomethingRandom()");

        while (count-- > 0) {
            NavigateToPage(MobileGroupMentorDetailsPage.class);
            switch (General.getRandomInt(2)) {
                case 0:
                    break;
                case 1:
                    break;
                default:
                    break;
            }
        }

        return this;
    }
}

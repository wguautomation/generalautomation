
package MobilePages;

/*
 * Created by timothy.hallbeck on 4/7/2015.
 */

import Utils.General;
import Utils.wguPowerPointFileUtils;

public class MobileProfilePage extends MobileAppPage {

    public MobileProfilePage() throws Exception {
        this(null, 0L, "", "");
    }
    public MobileProfilePage(MobileAppPage page) throws Exception {
        super(page, 0L, "", "");
    }
    public MobileProfilePage(MobileAppPage existingPage, long lAttributes, String username, String password) throws Exception {
        super(existingPage, lAttributes, username, password);
        login();
        getProfileLink(this).click();
        WaitForPageLoad(MobileProfilePage.class);
        wguPowerPointFileUtils.saveScreenshot(this, "MobileProfilePage");
    }
    public MobileProfilePage verifyBasicInfo() {
        wguPowerPointFileUtils.saveScreenshot(this, "MobileProfilePage verifyBasicInfo()");

        General.assertDebug(getByXpath(MobileXpath.ProfilePage_StudentFullname) != null, "Name exists");
        General.assertDebug(getByXpath(MobileXpath.ProfilePage_DegreeTitle) != null, "Degree title exists");
        General.assertDebug(getByXpath(MobileXpath.ProfilePage_StudentId) != null, "Student ID exists");
        General.assertDebug(getByXpath(MobileXpath.ProfilePage_Email) != null, "Email exists");
        General.assertDebug(getByXpath(MobileXpath.ProfilePage_Phone) != null, "Primary phone exists");
        General.assertDebug(getByXpath(MobileXpath.ProfilePage_Address1) != null, "Address1 exists");
        General.assertDebug(getByXpath(MobileXpath.ProfilePage_Address2) != null, "Address2 exists");

        General.assertDebug(getByXpath(MobileXpath.ProfilePage_StudentIdLabel).getText().contains("Student ID"), "Student ID label exists");
        General.assertDebug(getByXpath(MobileXpath.ProfilePage_EmailLabel).getText().contains("Email"), "Email label exists");
        General.assertDebug(getByXpath(MobileXpath.ProfilePage_PhoneLabel).getText().contains("Primary Phone"), "Primary phone label exists");
        General.assertDebug(getByXpath(MobileXpath.ProfilePage_AddressLabel).getText().contains("Address"), "Address label exists");

        return this;
    }

    public MobileProfilePage copyStudentIdToClipboard() {
        WaitForPageLoad(MobileProfilePage.class);
        copy(MobileXpath.ProfilePage_StudentId);

        return this;
    }

    public MobileProfilePage doSomethingRandom(int count) throws Exception {
        General.Debug("MobileProfilePage::doSomethingRandom()");

        while (count-- > 0) {
            NavigateToPage(MobileProfilePage.class);
            switch (General.getRandomInt(2)) {
                case 0:
                    break;
                case 1:
                    break;
                default:
                    break;
            }
        }

        return this;
    }
}

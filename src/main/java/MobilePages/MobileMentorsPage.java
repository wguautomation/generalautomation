
package MobilePages;

import Utils.General;
import org.openqa.selenium.WebElement;

import java.util.List;

/*
 * Created by timothy.hallbeck on 4/8/2015.
 */

public class MobileMentorsPage extends MobileAppPage {

    public MobileMentorsPage() throws Exception {
        super();
        login();
        getMentorsIcon().click();
        WaitForPageLoad(MobileMentorsPage.class);
    }
    public MobileMentorsPage(MobileAppPage page) throws Exception {
        super(page);
    }

    public WebElement getTopMentorTitle() {
        return getTopMentorTitle(this);
    }
    public static WebElement getTopMentorTitle(MobileAppPage page) {
        page.WaitForPageLoad(MobileMentorsPage.class);
        return page.getListOfMentorTitles().get(0);
    }

    public WebElement getTopMentorName() {
        return getTopMentorName(this);
    }
    public static WebElement getTopMentorName(MobileMentorsPage page) {
        page.WaitForPageLoad(MobileMentorsPage.class);
        return page.getListOfMentorNames().get(0);
    }

    public MobileAppPage verifyBasics() {
        return verifyBasics(this);
    }
    public static MobileAppPage verifyBasics(MobileAppPage page) {
        page.WaitForPageLoad(MobileMentorsPage.class);

        page.getByXpath(MobileXpath.MentorsPage_MentorName);
        page.getByXpath(MobileXpath.MentorsPage_MentorTitle);

        List <WebElement> nameList = page.getListOfMentorNames();
        List <WebElement> titleList = page.getListOfMentorTitles();

        page.getByXpath(MobileXpath.PageTitle_Mentors);
        page.getByXpath(MobileXpath.MentorsPage_MentorEmailButton);
        page.getByXpath(MobileXpath.MentorsPage_MentorPhoneButton);

        assert(page.getByXpath(MobileXpath.MentorsPage_MentorPhoto) != null);

        return page;
    }

    public MobileMentorsPage doSomethingRandom(int count) throws Exception {
        General.Debug("MobileMentorsPage::doSomethingRandom()");

        while (count-- > 0) {
            NavigateToPage(MobileMentorsPage.class);
            switch (General.getRandomInt(2)) {
                case 0:
                    break;
                case 1:
                    break;
                default:
                    break;
            }
        }

        return this;
    }
}


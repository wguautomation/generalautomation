
package MobilePages;

/*
 * Created by timothy.hallbeck on 4/22/2015.
 */

import Utils.General;
import Utils.wguPowerPointFileUtils;

public class MobileLoadingPage extends MobileAppPage {

    public MobileLoadingPage() throws Exception {
        ; // This is a transient popup, so no inits or login necessary
    }

    public MobileLoadingPage WaitForLoadingPage() {
        return WaitForLoadingPage(this);
    }
    public static MobileLoadingPage WaitForLoadingPage(MobileLoadingPage page) {
        page.getByXpath("//TextView[@id='message']");
        String text = page.getByXpath("//TextView[@id='message']").getText();
        assert(text.contains("Loading") || text.contains("Refreshing") );
        wguPowerPointFileUtils.saveScreenshot(page, "MobileLoadingPage");

        return page;
    }

    // No exercise for transient page
    public MobileLoadingPage ExercisePage(boolean cascade) {
        General.Debug("\nMobileLoadingPage::ExercisePage(" + cascade + ")");
        return this;
    }
}

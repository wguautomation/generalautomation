
package MobilePages;

import BaseClasses.GlobalData;
import BaseClasses.Page;
import Utils.General;
import Utils.wguPowerPointFileUtils;
import io.selendroid.client.SelendroidDriver;
import io.selendroid.common.SelendroidCapabilities;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.testng.Assert;
import org.testng.TestNGException;

import java.net.URL;
import java.util.List;

/*
 * Created by timothy.hallbeck on 4/2/2015.
 */

abstract public class MobileAppPage extends Page {

    public String VERSION_PREFIX = "edu.wgu.students.android:";
//    public String VERSION_PREFIX = "edu.wgu.students.mywgu:";
    public String username;
    public String password;

    public enum swipeSide {
        leftSide,
        rightSide
    }

    // Pick one of these
//    public long PRESETS = GlobalData.LOGIN_LANE1 | GlobalData.SAVE_PRESENTATION | GlobalData.DEVICE_TABLET;
//    public long PRESETS = GlobalData.LOGIN_LANE2 | GlobalData.SAVE_PRESENTATION | GlobalData.DEVICE_TABLET;
//    public long PRESETS = GlobalData.LOGIN_PROD | GlobalData.SAVE_PRESENTATION | GlobalData.ATTACH_PRESENTATION | GlobalData.DEVICE_TABLET;
    public long PRESETS = GlobalData.LOGIN_PROD | GlobalData.DEVICE_TABLET;

    public MobileAppPage() throws Exception {
        this(null, 0L, "", "");
    }
    public MobileAppPage(MobileAppPage existingPage) throws Exception {
        this(existingPage, 0L, "", "");
    }
    public MobileAppPage(MobileAppPage existingPage, long lAttributes) throws Exception {
        this(existingPage, lAttributes, "", "");
    }
    public MobileAppPage(MobileAppPage existingPage, long lAttributes, String username, String password) throws Exception {
        super(existingPage);
        setTimeoutInSecs(25);
        VERSION = "2.0.1";

        wguPowerPointFileUtils.saveScreenshots    = false;
        wguPowerPointFileUtils.savePresentation   = true;
        wguPowerPointFileUtils.attachPresentation = true;

        if (existingPage == null) {
            lAttributes |= PRESETS;
            Data = new GlobalData(lAttributes);
            this.username = username.length() == 0 ? Data.getUsername() : username ;
            this.password = password.length() == 0 ? Data.getPassword() : password ;
        } else {
            // lAttributes value is ignored here, as our data object is already loaded
            Data     = existingPage.Data;
            this.username = existingPage.username;
            this.password = existingPage.password;

            driver    = existingPage.driver;
            bLoggedIn = existingPage.bLoggedIn;
        }

        if (existingPage == null || existingPage.driver == null) {
            SelendroidCapabilities capa  = new SelendroidCapabilities(VERSION_PREFIX + VERSION);
            if (Data.isDeviceVirtual()) {
                capa.setEmulator(true);
            }

            try {
                General.Debug("creating driver");
//                driver = new SelendroidDriver(capa);
//                driver = new SelendroidDriver(new URL("http://localhost:4445/wd/hub"), capa);
                driver = new SelendroidDriver(new URL("http://localhost:4444/wd/hub"), capa);
            } catch (Exception e) {
                General.assertDebug(true, "Error in SelendroidDriver() creation");
                General.assertDebug(true, e.getMessage());
                System.exit(1);
            }
            CheckForUhOh();
        }
    }

    public MobileAppPage login() {
        return login(false);
    }
    public MobileAppPage login(boolean bKeepSignedIn) {
        if (!bLoggedIn)
            login(this, bKeepSignedIn);
        return this;
    }
    public MobileAppPage login(MobileAppPage page, boolean bKeepSignedIn) {
        General.Debug("using account (" + username + ", " + password + ")");
        wguPowerPointFileUtils.saveScreenshot(this, "pre login");
        MobileLoginPage.enterUsername(page, username);
        MobileLoginPage.enterPassword(page, password);
        setSelected(MobileLoginPage.getKeepMeSignedIn(page), bKeepSignedIn);
        wguPowerPointFileUtils.addBulletPoint("set getKeepMeSignedIn to " + bKeepSignedIn);

        wguPowerPointFileUtils.saveScreenshot(this, "before sign in clicked");
        wguPowerPointFileUtils.addBulletPoint("click Sign In button");
        MobileLoginPage.clickSignInButton(this);
        wguPowerPointFileUtils.saveScreenshot(this, "After sign in clicked");
        bLoggedIn = true;

        Sleep(2000);
        WaitforBScreen();

        return page;
    }

    public MobileAppPage clickBackButton() {
        Sleep(1000);
        General.assertDebug(true, "clickBackButton()");
        driver.navigate().back();

        return this;
    }

    public void CheckForUhOh() {
        // will add back in if app starts misbehaving again.
        return;
/*        General.Debug("CheckForUhOh()");
        long saveSecs = getTimeoutInSecs();
        Sleep(1000); // Waiting on initial page load
        setTimeoutInSecs(1);

        if (getByXpath(MobileXpath.UhOhDialog_Title, true) != null) {
            General.Debug("Found UhOh");
            wguPowerPointFileUtils.saveScreenshot(this, "CheckForUhOh");
            getByXpath(MobileXpath.UhOhDialog_OkButton).click();
            Sleep(1000);
        }
        setTimeoutInSecs(saveSecs);
*/
    }
    // I hate marketing people
    public void WaitforBScreen() {
        // will add back in if idiots decide to add it back in again
//        return;
        General.Debug("WaitforBScreen()");
        wguPowerPointFileUtils.addBulletPoint("WaitforBScreen()");

        Sleep(1000);
        getByXpath(MobileXpath.IntroScreen_StatusMessage, true);
        Sleep(1000);
        if (getByXpath(MobileXpath.IntroScreen_SkipButton, true) != null) {
            General.Debug("Found BScreen()");
            wguPowerPointFileUtils.saveScreenshot(this, "BScreen");
            wguPowerPointFileUtils.addBulletPoint("click IntroScreen_SkipButton");
            getByXpath(MobileXpath.IntroScreen_SkipButton).click();
            Sleep(4000);
        } else {
            General.Debug("No BScreen() this time");
        }
    }

    public WebElement getHamburger() {
//        getByXpath(MobileXpath.Hamburger);
        return getByXpath(MobileXpath.Hamburger);
    }
    public MobileAppPage clickHamburger() {
        clickHamburger(this);
        return this;
    }
    public static void clickHamburger(MobileAppPage page) {
        wguPowerPointFileUtils.addBulletPoint("clickHamburger");
        page.getHamburger().click();
        page.WaitForControl(MobileXpath.Hamburger_Signout);
        wguPowerPointFileUtils.saveScreenshot(page, "After clickHamburger");
    }

    public WebElement getNotificationsIcon() {
        return getNotificationsIcon(this);
    }
    public static WebElement getNotificationsIcon(MobileAppPage page) {
        page.clickHamburger();
        return page.getByXpath(MobileXpath.Hamburger_Notifications);
    }

    public WebElement getDegreePlanIcon() {
        return getDegreePlanIcon(this);
    }
    public static WebElement getDegreePlanIcon(MobileAppPage page) {
        page.clickHamburger();
        return page.getByXpath(MobileXpath.Hamburger_DegreePlan);
    }

    public WebElement getMentorsIcon() {
        return getMentorsIcon(this);
    }
    public static WebElement getMentorsIcon(MobileAppPage page) {
        page.clickHamburger();
        return page.getByXpath(MobileXpath.Hamburger_Mentors);
    }

    public WebElement getProfileLink() {
        return getProfileLink(this);
    }
    public static WebElement getProfileLink(MobileAppPage page) {
        page.clickHamburger();
        return page.getByXpath(MobileXpath.Hamburger_Profile);
    }

    public WebElement getAppCenter() {
        return getAppCenter(this);
    }
    public static WebElement getAppCenter(MobileAppPage page) {
        page.clickHamburger();
        return page.getByXpath(MobileXpath.Hamburger_AppCenter);
    }

    public WebElement getResources() {
        return getResources(this);
    }
    public static WebElement getResources(MobileAppPage page) {
        page.clickHamburger();
        return page.getByXpath(MobileXpath.Hamburger_Resources);
    }

    public WebElement getContactWGU() {
        return getContactWGU(this);
    }
    public static WebElement getContactWGU(MobileAppPage page) {
        page.clickHamburger();
        return page.getByXpath(MobileXpath.Hamburger_ContactWGU);
    }

    public WebElement getAboutApp() {
        return getAboutApp(this);
    }
    public static WebElement getAboutApp(MobileAppPage page) {
        page.clickHamburger();
        return page.getByXpath(MobileXpath.Hamburger_AboutApp);
    }

    public WebElement getNotificationSettings() {
        return getNotificationSettings(this);
    }
    public static WebElement getNotificationSettings(MobileAppPage page) {
        page.clickHamburger();
        return page.getByXpath(MobileXpath.Hamburger_NotificationSettings);
    }

    public WebElement getSavedFiles() {
        return getSavedFiles(this);
    }
    public static WebElement getSavedFiles(MobileAppPage page) {
        page.clickHamburger();
        return page.getByXpath(MobileXpath.Hamburger_SavedFiles);
    }

    public WebElement getSignOut() {
        return getSignOut(this);
    }
    public static WebElement getSignOut(MobileAppPage page) {
        page.clickHamburger();
        return page.getByXpath(MobileXpath.Hamburger_Signout);
    }

    public static WebElement getCopyright(MobileAppPage page) {
        page.clickHamburger();
        return page.getByXpath(MobileXpath.Hamburger_Copyright);
    }

    public void quit() {
        General.Debug("quit()");
        super.quit();
        Sleep(1000);
    }

    public MobileAppPage VerifyCourseHeader() {
        wguPowerPointFileUtils.addBulletPoint("VerifyCourseHeader()");
        General.assertDebug(getByXpath(MobileXpath.Header_Toolbar) != null, "Toolbar present");
        General.assertDebug(getByXpath(MobileXpath.Header_CourseChatter) != null, "Course chatter button present");

        return this;
    }

    public MobileAppPage verifyControl(String xPath) {
        getByXpath(xPath);
        wguPowerPointFileUtils.addBulletPoint("Exists: " + xPath);

        return this;
    }

    public MobileAppPage VerifyHamburgerContents() {
        clickHamburger();

        verifyControl(MobileXpath.Hamburger_AppCenter);
        verifyControl(MobileXpath.Hamburger_ContactWGU);
        verifyControl(MobileXpath.Hamburger_Copyright);
        verifyControl(MobileXpath.Hamburger_DegreePlan);
        verifyControl(MobileXpath.Hamburger_Mentors);
        verifyControl(MobileXpath.Hamburger_Notifications);
        verifyControl(MobileXpath.Hamburger_NotificationSettings);
        verifyControl(MobileXpath.Hamburger_Profile);
        verifyControl(MobileXpath.Hamburger_SavedFiles);
        verifyControl(MobileXpath.Hamburger_Signout);

        return this;
    }

    public List<WebElement> getListOfMentorNames() {
        return getListByXpath(MobileXpath.MentorsPage_MentorName);
    }

    public List<WebElement> getListOfMentorTitles() {
        return getListByXpath(MobileXpath.MentorsPage_MentorTitle);
    }

    // not working yet...the displayed and enabled properties are not up to date
    public WebElement scrollToAndClick(WebElement element) {
        assert(element != null);
        final int iMaxScrolls = 3;
        swipeDown(iMaxScrolls);

        while (!(element.isDisplayed() && element.isEnabled()))
            swipeUp(1);
        element.click();

        return element;
    }

    public MobileAppPage swipeUp(int iCount) {
        return swipeUp(iCount, swipeSide.rightSide);
    }
    public MobileAppPage swipeUp(int iCount, swipeSide side) {
        Assert.assertTrue(iCount > 0, "SwipeUp(" + iCount + ") not valid, must be > 0");

        new TouchActions(driver);
        int X;
        if (side == swipeSide.rightSide)
            X = 400;
        else
            X = 100;

        while (iCount-- > 0) {
            if (Data.isPhone())
                new TouchActions(driver).down(X, 250).move(X, 150).up(X, 150).perform();
            else
                new TouchActions(driver).down(X, 600).move(X, 550).up(X, 550).perform();
            Sleep(250);
        }
//        wguPowerPointFileUtils.saveScreenshot(this, "swipeUp");
        return this;
    }

    public MobileAppPage swipeDown(int iCount) {
        return swipeDown(iCount, swipeSide.rightSide);
    }
    public MobileAppPage swipeDown(int iCount, swipeSide side) {
        Assert.assertTrue(iCount > 0, "SwipeDown(" + iCount + ") not valid, must be > 0");

        int X;
        if (side == swipeSide.rightSide)
            X = 400;
        else
            X = 50;

        while (iCount-- > 0) {
            if (Data.isPhone())
                new TouchActions(driver).down(X, 150).move(X, 250).up(X, 250).perform();
            else
                new TouchActions(driver).down(X, 400).move(X, 600).up(X, 600).perform();
            Sleep(250);
        }
//        wguPowerPointFileUtils.saveScreenshot(this, "swipeDown");

        return this;
    }

    public MobileAppPage swipeRight(String xpath, int iCount) {
        Assert.assertTrue(iCount > 0 && xpath.length() > 0, "swipeRight(" + xpath + ", " + iCount + ") not valid");

        WebElement element = getByXpath(xpath);
        Point point = element.getLocation();

        while (iCount-- > 0) {
            new TouchActions(driver).down(point.x, point.y).move(point.x + 250, point.y).up(point.x + 250, point.y).perform();
//            new TouchActions(driver).down(250, 300).move(450, 300).up(450, 300).perform();
            Sleep(250);
        }
//        wguPowerPointFileUtils.saveScreenshot(this, "swipeRight");

        return this;
    }

    public MobileAppPage swipeLeft(String xpath, int iCount) {
        Assert.assertTrue(iCount > 0 && xpath.length() > 0, "SwipeLeft(" + xpath + ", " + iCount + ") not valid");

        WebElement element = getByXpath(xpath);
        Point point = element.getLocation();

        while (iCount-- > 0) {
            new TouchActions(driver).down(point.x + 250, point.y).move(point.x, point.y).up(point.x, point.y).perform();
            Sleep(250);
        }
//        wguPowerPointFileUtils.saveScreenshot(this, "swipeLeft");

        return this;
    }

    public MobileAppPage tap(int X, int Y) {
        return tap(X, Y, 100);
    }
    public MobileAppPage tap(int X, int Y, int delay) {
        new TouchActions(driver).down(X, Y).perform();
        Sleep(100);
        new TouchActions(driver).up  (X, Y).perform();
        Sleep(delay);

        return this;
    }

    public MobileAppPage tap(WebElement element, int offsetX, int offsetY) {
        return tap(element, offsetX, offsetY, 100);
    }
    public MobileAppPage tap(WebElement element, int offsetX, int offsetY, int delay) {
        Point point = element.getLocation();
        new TouchActions(driver).down(point.x + offsetX, point.y + offsetY).perform();
        Sleep(100);
        new TouchActions(driver).up  (point.x + offsetX, point.y + offsetY).perform();
        Sleep(delay);

        return this;
    }

    public MobileAppPage longPress(WebElement element, int offsetX, int offsetY) {
        Point point = element.getLocation();
        new TouchActions(driver).down(point.x+offsetX, point.y+offsetY).perform();
        Sleep(1500);
        wguPowerPointFileUtils.saveScreenshot(this, "longPress");
        new TouchActions(driver).up  (point.x+offsetX, point.y+offsetY).perform();

        return this;
    }

    public MobileAppPage copy(String xpath) {
        General.Debug("copy(" + xpath + ")", true);
        WebElement element = getByXpath(xpath);
        longPress(element, 50, 10);
        getByXpath(MobileXpath.Clipboard_Copy).click();
        wguPowerPointFileUtils.addBulletPoint("after copy to clipboard");

        return this;
    }

    public MobileAppPage paste(String xpath) {
        General.Debug("paste(" + xpath + ")", true);
        WebElement element = getByXpath(xpath);
        element.clear();
        longPress(element, 50, 10);

        if (Data.isPhone())
            tap(element, 0, -20);
        else
            tap(element, 0, -40);

        return this;
    }

    public MobileAppPage WaitForPageLoad(java.lang.Class theClass) {
        General.Debug("WaitForPageLoad(" + theClass.getName() + ")", true);

        if (theClass == MobileAppCenterPage.class) {
            WaitForControl(MobileXpath.Hamburger_WaitForPageLoad);

        } else if (theClass == MobileAboutAppPage.class) {
            WaitForControl(MobileXpath.AboutApp_VersionText);

        } else if (theClass == MobileContactWGUPage.class) {
            WaitForControl(MobileXpath.DialogTitle_ContactWGU);

        } else if (theClass == MobileCourseAnnouncementsPage.class) {
            WaitForControl(MobileXpath.CourseAnnouncementsPage_Title);

        } else if (theClass == MobileCourseChatterPage.class) {
            WaitForControl(MobileXpath.PageTitle_CourseChatter);
            Sleep(2000);

        } else if (theClass == MobileCourseTipsPage.class) {
            WaitForControl(MobileXpath.CourseTipsPage_Tips);

        } else if (theClass == MobileDegreePlanPage.class) {
            Sleep(3000);
            WaitForControl(MobileXpath.DegreePlan_TermDate);
            Sleep(1000);

        } else if (theClass == MobileFeedbackPage.class) {
            WaitForControl(MobileXpath.FeedbackPage_Spinner);

        } else if (theClass == MobileGroupMentorDetailsPage.class) {
            WaitForControl(MobileXpath.GMDetailsPage_MentorName);

        } else if (theClass == MobileLoginPage.class) {
            WaitForControl(MobileXpath.LoginPage_SignInButton);

        } else if (theClass == MobileMentorDetailsPage.class) {
            WaitForControl(MobileXpath.MentorDetailsPage_MentorPic);

        } else if (theClass == MobileMentorsPage.class) {
            WaitForControl(MobileXpath.PageTitle_Mentors);

        } else if (theClass == MobileNotificationSettingsPage.class) {
            WaitForControl(MobileXpath.NotifSettingsPage_NotificationsBox);

        } else if (theClass == MobileNotificationsPage.class) {
            WaitForControl(MobileXpath.PageTitle_Notifications);

        } else if (theClass == MobileProfilePage.class) {
            WaitForControl(MobileXpath.ProfilePage_AddressLabel);

        } else if (theClass == MobileResourcesPage.class) {
            WaitForControl(MobileXpath.PageTitle_Resources);

        } else if (theClass == MobileStudyPlanPage.class) {
            WaitForControl(MobileXpath.StudyPlanPage_Announcements);

        } else if (theClass == MobileSubjectActivitiesPage.class) {
            WaitForControl(MobileXpath.SubjectActivitiesPage_ActivityName);

        } else if (theClass == MobileSubjectDetailsPage.class) {
            WaitForControl(MobileXpath.SubjectDetailsPage_SubjectTitle);

        } else if (theClass == MobileTopicPage.class) {
            WaitForControl(MobileXpath.TopicPage_SubjectTitle);

        } else if (theClass == MobileWGUExternalHomePage.class) {
            getHamburger();
            WaitForControl(MobileXpath.Generic_WebView);
        }

        return this;
    }

    public MobileAppPage NavigateToPage(java.lang.Class theClass) {
        General.Debug("NavigateToPage(" + theClass.getName() + ")", true);

        if (theClass == MobileAppCenterPage.class) {
            getAppCenter(this).click();

        } else if (theClass == MobileAboutAppPage.class) {
            NavigateToPage(MobileDegreePlanPage.class);
            getAboutApp().click();

        } else if (theClass == MobileContactWGUPage.class) {
            NavigateToPage(MobileResourcesPage.class);
            MobileResourcesPage.getContactWGU(this).click();

        } else if (theClass == MobileCourseAnnouncementsPage.class) {
            NavigateToPage(MobileStudyPlanPage.class);
            MobileStudyPlanPage.getCourseAnnouncements(this).click();

        } else if (theClass == MobileCourseChatterPage.class) {
            NavigateToPage(MobileSubjectDetailsPage.class);
            MobileSubjectDetailsPage.clickCourseChatterIcon(this);

        } else if (theClass == MobileCourseTipsPage.class) {
            NavigateToPage(MobileStudyPlanPage.class);
            MobileStudyPlanPage.getCourseTips(this).click();

        } else if (theClass == MobileDegreePlanPage.class) {
            if (getByXpath(MobileXpath.Hamburger, true) == null)
                clickBackButton();
            if (getByXpath(MobileXpath.Hamburger, true) == null)
               clickBackButton();
            clickIfExists(MobileXpath.Generic_Up);
            clickIfExists(MobileXpath.Generic_Up);
            getDegreePlanIcon().click();
            Sleep(2000);

        } else if (theClass == MobileFeedbackPage.class) {
            NavigateToPage(MobileAboutAppPage.class);
            MobileAboutAppPage.getFeedback(this).click();

        } else if (theClass == MobileGroupMentorDetailsPage.class) {
            NavigateToPage(MobileMentorsPage.class);
            MobileGroupMentorDetailsPage.clickTopMentor(this);

        } else if (theClass == MobileLoginPage.class) {
            getSignOut().click();

        } else if (theClass == MobileMentorDetailsPage.class) {
            NavigateToPage(MobileMentorsPage.class);
            MobileMentorDetailsPage.getTopMentorTitle(this).click();

        } else if (theClass == MobileMentorsPage.class) {
            NavigateToPage(MobileDegreePlanPage.class);
            getMentorsIcon().click();

        } else if (theClass == MobileNotificationSettingsPage.class) {
            NavigateToPage(MobileDegreePlanPage.class);
            getNotificationSettings().click();

        } else if (theClass == MobileNotificationsPage.class) {
            NavigateToPage(MobileDegreePlanPage.class);
            getNotificationsIcon().click();

        } else if (theClass == MobileProfilePage.class) {
            NavigateToPage(MobileDegreePlanPage.class);
            getProfileLink().click();

        } else if (theClass == MobileResourcesPage.class) {
            NavigateToPage(MobileDegreePlanPage.class);
            getResources().click();

        } else if (theClass == MobileStudyPlanPage.class) {
            NavigateToPage(MobileDegreePlanPage.class);
            MobileDegreePlanPage.ScrollToCourse(this, "English").click();

        } else if (theClass == MobileSubjectActivitiesPage.class) {
            NavigateToPage(MobileSubjectDetailsPage.class);
            MobileSubjectDetailsPage.getListOfActivities(this).get(0).click();

        } else if (theClass == MobileSubjectDetailsPage.class) {
            NavigateToPage(MobileStudyPlanPage.class);
            MobileDegreePlanPage.ScrollToCourse(this, "English").click();

        } else if (theClass == MobileTopicPage.class) {
            NavigateToPage(MobileSubjectDetailsPage.class);
            MobileTopicPage.clickTopic(this, 0);

        } else if (theClass == MobileWGUExternalHomePage.class) {
            NavigateToPage(MobileLoginPage.class);
            MobileLoginPage.getNotAStudent(this).click();
        } else {
            throw new TestNGException("class not defined in NavigateToPage[" + theClass.toString() + "]");
        }

        WaitForPageLoad(theClass);
        wguPowerPointFileUtils.saveScreenshot(this, "after WaitForPageLoad[" + theClass.getName() + "]");

        return this;
    }

    private MobileAppPage WaitForControl(String xpath) {
        Assert.assertTrue(xpath.length() > 0, "Empty xpath sent to WaitForControl()");
        assert(getByXpath(xpath) != null);

        return this;
    }
    private MobileAppPage WaitForControl(String xpath, String text) {
        Assert.assertTrue(xpath.length() > 0 && text.length() > 0, "Empty xpath or text item sent to WaitForControl()");
        assert(getByXpath(xpath).getText().contains(text));

        return this;
    }

    public MobileAppPage doSomethingRandom(int count) throws Exception {
        General.Debug("");
        General.Debug("MobileAppPage::doSomethingRandom()");

        while (count-- > 0) {
            switch (General.getRandomInt(8)) {
                case 0:
                    General.assertDebug(true, "Random action: MobileNotificationSettingsPage");
                    NavigateToPage(MobileNotificationSettingsPage.class);
                    break;
                case 1:
                    General.assertDebug(true, "Random action: MobileNotificationsPage");
                    NavigateToPage(MobileNotificationsPage.class);
                    break;
                case 2:
                    General.assertDebug(true, "Random action: click Degree Plan");
                    getDegreePlanIcon().click();
                    break;
                case 3:
                    General.assertDebug(true, "Random action: click Mentors");
                    getMentorsIcon().click();
                    break;
                case 4:
                    General.assertDebug(true, "Random action: click Profile");
                    getProfileLink().click();
                    break;
                case 5:
                    General.assertDebug(true, "Random action: click App Center");
                    getAppCenter().click();
                    break;
                case 6:
//                    General.assertDebug(true, "Random action: click Feedback");
//                    getFeedback().click();
//                    break;
                case 7:
                    General.assertDebug(true, "Random action: MobileContactWGUPage");
                    NavigateToPage(MobileContactWGUPage.class);
                    break;
                case 8:
                    General.assertDebug(true, "Random action: MobileNotificationSettingsPage");
                    NavigateToPage(MobileNotificationSettingsPage.class);
                    break;
                default:
                    break;
            }
            General.Debug("MobileAppPage::doSomethingRandom() End");
        }
        return this;
    }

    public MobileAppPage ExercisePage(boolean cascade) {
        General.Debug("\nMobileAppPage::ExercisePage()");
        if (cascade) {
            try {
                new MobileNotificationSettingsPage(this).ExercisePage(cascade);
            } catch (Exception e) {
                quit();
            }
        } else {
            getNotificationSettings(this).click();
            getNotificationsIcon(this).click();
            getDegreePlanIcon(this).click();
            getMentorsIcon(this).click();
            getProfileLink(this).click();
            WaitForPageLoad(MobileProfilePage.class);
            getAppCenter(this).click();
            getResources(this).click();
//            getFeedback(this).click();
//        clickSendLogs();
//        clickVersion();
        }

        getSignOut().click();

        return this;
    }
}

package MobilePages;

import Utils.General;
import org.openqa.selenium.WebElement;

/*
 * Created by timothy.hallbeck on 9/30/2015.
 */

public class MobileAboutAppPage extends MobileDegreePlanPage {

    public MobileAboutAppPage() throws Exception {
        super();
        getAboutApp().click();
        Sleep(1000);
        WaitForPageLoad(MobileAboutAppPage.class);
    }
    public MobileAboutAppPage(MobileAppPage page) throws Exception {
        super(page);
    }

    public static WebElement getAppTour(MobileAppPage page) {
        return page.getByXpath(MobileXpath.AboutApp_AppTour);
    }

    public static WebElement getFAQs(MobileAppPage page) {
        return page.getByXpath(MobileXpath.AboutApp_Faqs);
    }

    public static WebElement getFeedback(MobileAppPage page) {
        return page.getByXpath(MobileXpath.AboutApp_Feedback);
    }

    public static WebElement getSendLogs(MobileAppPage page) {
        return page.getByXpath(MobileXpath.AboutApp_SendLogstoWGU);
    }

    public static String getVersion(MobileAppPage page) {
        return page.getByXpath(MobileXpath.AboutApp_VersionNumber).getText();
    }

    public MobileAboutAppPage verifyBasicInfo() {
        assert(getByXpath(MobileXpath.FeedbackPage_Instructions) != null);
        assert(getByXpath(MobileXpath.FeedbackPage_Spinner) != null);
        assert(getByXpath(MobileXpath.FeedbackPage_NeedHelp) != null);

        return this;
    }

    public MobileAboutAppPage doSomethingRandom(int count) throws Exception {
        General.Debug("MobileFeedbackPage::doSomethingRandom()");

        while (count-- > 0) {
            NavigateToPage(MobileFeedbackPage.class);
            switch (General.getRandomInt(2)) {
                case 0:
                    break;
                case 1:
                    break;
                default:
                    break;
            }
        }

        return this;
    }
}


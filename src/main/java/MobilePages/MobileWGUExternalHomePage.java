
package MobilePages;

/*
 * Created by timothy.hallbeck on 4/15/2015.
 */

import Utils.General;

public class MobileWGUExternalHomePage extends MobileAppPage {

    public MobileWGUExternalHomePage() throws Exception {
        super();
        MobileLoginPage.getNotAStudent(this).click();
        Sleep(4000); // transition to external site can be very slow
        WaitForPageLoad(MobileWGUExternalHomePage.class);
    }
    public MobileWGUExternalHomePage(MobileAppPage page) throws Exception {
        super(page);
    }

    // This is one giant WebView object, nothing significant is testable from automation
    public MobileWGUExternalHomePage verifyBasicInfo() {
        WaitForPageLoad(MobileWGUExternalHomePage.class);

        return this;
    }

    public MobileWGUExternalHomePage doSomethingRandom(int count) throws Exception {
        General.Debug("MobileWGUExternalHomePage::doSomethingRandom()");

        while (count-- > 0) {
            NavigateToPage(MobileWGUExternalHomePage.class);
            switch (General.getRandomInt(2)) {
                case 0:
                    break;
                case 1:
                    break;
                default:
                    break;
            }
        }

        return this;
    }

}



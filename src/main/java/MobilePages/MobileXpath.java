
package MobilePages;

/*
 * Created by timothy.hallbeck on 7/7/2015.
 */

public class MobileXpath {
    public static String AboutApp_AppTour                  = "//AppCompatTextView[@value='App Tour']";
    public static String AboutApp_Faqs                     = "//AppCompatTextView[@value='FAQs']";
    public static String AboutApp_Feedback                 = "//AppCompatTextView[@value='FeedBack']";
    public static String AboutApp_SendLogstoWGU            = "//AppCompatTextView[@value='Send Logs to WGU']";
    public static String AboutApp_VersionText              = "//AppCompatTextView[@value='Version']";
    public static String AboutApp_VersionNumber            = "//AppCompatTextView[@id='id_about_version']";

    public static String AppCenterPage_AppName             = "//AppCompatTextView[@id='id_app_name']";
    public static String AppCenterPage_Desc                = "//AppCompatTextView[@id='id_app_center_desc']";

    public static String Clipboard_Copy                    = "//AppCompatTextView[@id='title']";
    public static String Clipboard_Paste                   = "//AppCompatTextView[@id='title']";

    public static String ContactLink_GenericEmail              = "//ImageView[@id='id_contact_email']";
    public static String ContactLink_GenericPhone              = "//ImageView[@id='id_contact_phone']";
    public static String ContactLink_GenericAddContact         = "//ImageView[@id='id_add_to_contact']";

    public static String ContactLink_CareerServices            = "//TextView[@value='Career Services']";
    public static String ContactLink_Enrollment                = "//TextView[@value='Enrollment']";
    public static String ContactLink_FinancialAid              = "//TextView[@value='Financial Aid']";
    public static String ContactLink_ITServiceDesk             = "//TextView[@value='IT Service Desk']";
    public static String ContactLink_LearningResources         = "//TextView[@value='Learning Resources']";
    public static String ContactLink_StudentAccounts           = "//TextView[@value='Student Accounts']";
    public static String ContactLink_StudentRecords            = "//TextView[@value='Student Records']";
    public static String ContactLink_StudentServices           = "//TextView[@value='Student Services']";
    public static String ContactLink_StudentSuccessCenter      = "//TextView[@value='Student Success Center']";
    public static String ContactLink_TermBreak                 = "//TextView[@value='Term Break']";
    public static String ContactLink_TransferEvaluation        = "//TextView[@value='Transfer Evaluation']";
    public static String ContactLink_WritingCenter             = "//TextView[@value='Writing Center']";
    public static String ContactLink_CareerServicesEmail       = "//ImageView[@name='careers@wgu.edu']";
    public static String ContactLink_EnrollmentEmail           = "//ImageView[@name='info@wgu.edu']";
    public static String ContactLink_FinancialAidEmail         = "//ImageView[@name='finaid@wgu.edu']";
    public static String ContactLink_ITServiceDeskEmail        = "//ImageView[@name='servicedesk@wgu.edu']";
    public static String ContactLink_LearningResourcesEmail    = "//ImageView[@name='learning@wgu.edu']";
    public static String ContactLink_StudentAccountsEmail      = "//ImageView[@name='bursar@wgu.edu']";
    public static String ContactLink_StudentRecordsEmail       = "//ImageView[@name='records@wgu.edu']";
    public static String ContactLink_StudentServicesEmail      = "//ImageView[@name='studentservices@wgu.edu']";
    public static String ContactLink_StudentSuccessCenterEmail = "//ImageView[@name='studentsuccess@wgu.edu']";
    public static String ContactLink_TermBreakEmail            = "//ImageView[@name='termbreak@wgu.edu']";
    public static String ContactLink_TransferEvaluationEmail   = "//ImageView[@name='transcriptinfo@wgu.edu']";
    public static String ContactLink_WritingCenterEmail        = "//ImageView[@name='writingcenter@wgu.edu']";
    public static String ContactLink_CareerServicesPhone       = "//ImageView[@name='8668959660,,,3196']";
    public static String ContactLink_EnrollmentPhone           = "//ImageView[@name='8662255948']";
    public static String ContactLink_FinancialAidPhone         = "//ImageView[@name='8774357948']";
    public static String ContactLink_ITServiceDeskPhone        = "//ImageView[@name='8774357948']";
    public static String ContactLink_LearningResourcesPhone    = "//ImageView[@name='8774357948']";
    public static String ContactLink_StudentAccountsPhone      = "//ImageView[@name='8774357948']";
    public static String ContactLink_StudentRecordsPhone       = "//ImageView[@name='8774357948']";
    public static String ContactLink_StudentServicesPhone      = "//ImageView[@name='8774357948']";
    public static String ContactLink_StudentSuccessCenterPhone = "//ImageView[@name='8668959660,,,3196']";
    public static String ContactLink_TermBreakPhone            = "//ImageView[@name='8774357948']";
    public static String ContactLink_TransferEvaluationPhone   = "//ImageView[@name='8774357948']";
//    public static String ContactLink_WritingCenterPhone        = "//ImageView[@name='Writing Center']";


    public static String CourseAnnouncementsPage_EmptyMessage = "//ImageView[@id='ivEmptyAnnouncementsPage']";
    public static String CourseAnnouncementsPage_Overview     = "//AppCompatTextView[@value='Overview']";
    public static String CourseAnnouncementsPage_Title        = "//AppCompatTextView[@value='Course Announcements']";

    public static String CourseChatterDetailsPage_Comments    = "//AppCompatTextView[@id='id_chatter_comments']";
    public static String CourseChatterDetailsPage_Image       = "//CircularImageView[@id='id_chatter_image']";
    public static String CourseChatterDetailsPage_Timestamp   = "//AppCompatTextView[@id='id_post_timestamp']";
    public static String CourseChatterDetailsPage_StarImage   = "//ImageView[@id='id_like_star_image']";
    public static String CourseChatterDetailsPage_Username    = "//AppCompatTextView[@id='id_chatter_user_name']";

    public static String CourseChatterPage_MentorNameDegree   = "//AppCompatTextView[@id='id_course_chatter_mentor_name_degree']";
    public static String CourseChatterPage_ChatterPost        = "//AppCompatTextView[@id='id_course_chatter_post']";
    public static String CourseChatterPage_Timestamp          = "//AppCompatTextView[@id='id_course_chatter_timestamp']";
    public static String CourseChatterPage_MentorPic          = "//CircularImageView[@id='id_course_chatter_mentor_pic']";
    public static String CourseChatterPage_StarPic            = "//ImageView[@id='id_course_chatter_star_pic']";
    public static String CourseChatterPage_List               = "//AppCompatTextView[@id='id_course_chatter_post']";
    public static String CourseChatterPage_Post               = "//AppCompatTextView[@id='id_course_chatter_post']";
    public static String CourseChatterPage_Title              = "//AppCompatTextView[@id='id_course_chatter_title']";

    public static String CourseTipsPage_Empty          = "//TextView[@id='id_empty_tips']";
    public static String CourseTipsPage_Tips           = "//AppCompatTextView[@value='Tips']";

    public static String DegreePlan_CourseCode         = "//AppCompatTextView[@id='tvTitle']";
    public static String DegreePlan_CourseForDate      = "//AppCompatTextView[@id='tvEndDate']";
    public static String DegreePlan_CourseUnits        = "//AppCompatTextView[@id='tvCUCircle']";
    public static String DegreePlan_Percentage         = "//AppCompatTextView[@id='percentage_course_terms']";
    public static String DegreePlan_PieChart           = "//ImageView[@id='id_pie_chart']";
    public static String DegreePlan_Status             = "//AppCompatTextView[@id='tvcoursestatus']";
    public static String DegreePlan_TermDate           = "//AppCompatTextView[@id='tvtermdateformated']";
    public static String DegreePlan_TermName           = "//AppCompatTextView[@id='tvterm']";
    public static String DegreePlan_VersionID          = "//AppCompatTextView[@id='id_course_version_id']";

    public static String DialogTitle_ContactWGU        = "//TextView[@value='Contact WGU']";

    public static String FeedbackPage_Desc             = "//AppCompatEditText[@id='id_feedback_description']";
    public static String FeedbackPage_Instructions     = "//AppCompatTextView[@id='id_feedback_instruction']";
    public static String FeedbackPage_NeedHelp         = "//AppCompatTextView[@id='id_feedback_need_help_link']";
    public static String FeedbackPage_SendOption       = "//ActionMenuItemView[@id='id_feedback_send_option']";
    public static String FeedbackPage_Spinner          = "//AppCompatTextView[@id='id_feedback_spinner_subjects']";
    public static String FeedbackPage_Spinner1         = "(//AppCompatTextView[@id='id_feedback_spinner_subjects'])[1]";

    public static String Generic_CardView              = "//CardView[@id='cardView']";
    public static String Generic_Up                    = "//ImageButton[@name='Navigate up']";
    public static String Generic_WebView               = "//WebView[@id='webView']";
    public static String Generic_WebViewModified       = "//WebView[@id='wvText']";

    public static String GMDetailsPage_MentorProfilePic    = "//RoundedImageView[@id='id_ivMentorPic']";
    public static String GMDetailsPage_MentorName          = "//AppCompatTextView[@id='id_tvMentorName']";
    public static String GMDetailsPage_Mentortitle         = "//AppCompatTextView[@id='id_tvMentorTitle']";
    public static String GMDetailsPage_PhoneTitle          = "//AppCompatTextView[@id='textView4']";
    public static String GMDetailsPage_PhoneImage          = "//ImageView[@id='id_mentor_phone_image']";
    public static String GMDetailsPage_PhoneNumber         = "//AppCompatTextView[@id='id_mentor_phone']";
    public static String GMDetailsPage_EmailTitle          = "//AppCompatTextView[@id='textView5']";
    public static String GMDetailsPage_EmailImage          = "//ImageButton[@id='id_ibEmailMentorProfile']";
    public static String GMDetailsPage_EmailAddress        = "//AppCompatTextView[@id='id_tvMentorEmail']";
    public static String GMDetailsPage_AddButton           = "//AppCompatButton[@id='id_ibAddMentorContact']";
    public static String GMDetailsPage_OfficeHours         = "//AppCompatTextView[@id='textView8']";
//    public static String GMDetailsPage_ClockImage          = "//ImageView[@id='id_clock_image']";

    public static String Hamburger                         = "//ImageButton[@name='WGU']";
    public static String Hamburger_AppCenter               = "//AppCompatTextView[@value='App Center']";
    public static String Hamburger_AboutApp                = "//AppCompatTextView[@value='About App']";
    public static String Hamburger_ContactWGU              = "//AppCompatTextView[@value='Contact WGU']";
    public static String Hamburger_Copyright               = "//AppCompatTextView[@id='id_copy_right']";
    public static String Hamburger_DegreePlan              = "//AppCompatTextView[@value='Degree Plan']";
    public static String Hamburger_Mentors                 = "//AppCompatTextView[@value='Mentors']";
    public static String Hamburger_Notifications           = "//AppCompatTextView[@value='Notifications']";
    public static String Hamburger_NotificationSettings    = "//AppCompatTextView[@value='Notification Settings']";
    public static String Hamburger_Profile                 = "//AppCompatTextView[@value='Profile']";
    public static String Hamburger_Resources               = "//AppCompatTextView[@value='Resources']";
    public static String Hamburger_Signout                 = "//AppCompatTextView[@id='id_sign_out']";
    public static String Hamburger_SavedFiles              = "//AppCompatTextView[@value='My Saved Files']";
    public static String Hamburger_WaitForPageLoad         = Hamburger_Copyright;

    public static String Header_CourseChatter              = "//ActionMenuItemView[@id='id_course_chatter']";
    public static String Header_TitleText                  = "//AppCompatTextView[@id='tvHeader']";
    public static String Header_Toolbar                    = "//Toolbar[@id='toolbar']";
    public static String Header_BackButton                 = "//ImageButton[@name='Navigate up']";

    public static String LearningResourcesPage_PacingGuide = "//AppCompatTextView[@value='Pacing Guide']";
    public static String LearningResourcesPage_Cohorts     = "//AppCompatTextView[@value='Cohorts']";
    public static String LearningResourcesPage_Tips        = "//AppCompatTextView[@value='Tips']";

    public static String LoginPage_LoginErrorMsg           = "//AppCompatTextView[@id='tvMessage']";
    public static String LoginPage_KeepSignedIn            = "//AppCompatCheckBox[@id='cbKeepSignedIn']";
    public static String LoginPage_KeepSignedInHelpLink    = "//ImageView[@id='ivKeepSignedInHelp']";
    public static String LoginPage_NeedHelpLink            = "//AppCompatTextView[@id='tvNeedHelp']";
    public static String LoginPage_NotAStudentLink         = "//AppCompatTextView[@id='tvNotAStudent']";
    public static String LoginPage_Password                = "//AppCompatEditText[@id='etPassword']";
    public static String LoginPage_SignInButton            = "//AppCompatButton[@id='bSignIn']";
    public static String LoginPage_SignInText              = "//AppCompatButton[@value='Sign In']";
    public static String LoginPage_Username                = "//AppCompatEditText[@id='etUsername']";
    public static String LoginPage_WGULogo                 = "//ImageView[@id='imageView6']";

    public static String IntroScreen_StatusMessage         = "//TextView[@id='id_login_status_message']";
    public static String IntroScreen_SkipButton            = "//AppCompatTextView[@id='tvSkipAppTour']";

    public static String MentorDetailsPage_AddButton       = "//ImageButton[@id='id_ibAddMentorContact']";
    public static String MentorDetailsPage_DayMon          = "//AppCompatTextView[@value='MON']";
    public static String MentorDetailsPage_DayTue          = "//AppCompatTextView[@value='TUE']";
    public static String MentorDetailsPage_DayWed          = "//AppCompatTextView[@value='WED']";
    public static String MentorDetailsPage_DayThu          = "//AppCompatTextView[@value='THU']";
    public static String MentorDetailsPage_DayFri          = "//AppCompatTextView[@value='FRI']";
    public static String MentorDetailsPage_EmailImage      = "//ImageView[@id='id_ibEmailMentorProfile']";
    public static String MentorDetailsPage_EmailValue      = "//AppCompatTextView[@id='id_tvMentorEmail']";
    public static String MentorDetailsPage_MentorPic       = "//RoundedImageView[@id='id_ivMentorPic']";
    public static String MentorDetailsPage_MentorName      = "//AppCompatTextView[@id='id_tvMentorName']";
    public static String MentorDetailsPage_MentorDesignation = "//AppCompatTextView[@id='id_tvMentorTitle']";
    public static String MentorDetailsPage_PhoneImage      = "//ImageView[@id='id_ibCallMentorProfile']";
    public static String MentorDetailsPage_PhoneValue      = "//AppCompatTextView[@id='id_tvMentorPhoneNumber']";
    public static String MentorDetailsPage_OfficeHours     = "//AppCompatTextView[@id='textView8']";
    public static String MentorDetailsPage_TimeZone        = "//AppCompatTextView[@id='tvTimeZoneMentor']";

    public static String MentorsPage_MentorName            = "//AppCompatTextView[@id='id_tvMentorName']";
    public static String MentorsPage_MentorTitle           = "//AppCompatTextView[@id='id_tvMentorTitle']";
    public static String MentorsPage_MentorPhoto           = "//RoundedImageView[@id='id_ivMentorPic']";
    public static String MentorsPage_MentorEmailButton     = "//ImageButton[@id='id_ibEmailMentor']";
    public static String MentorsPage_MentorPhoneButton     = "//ImageButton[@id='ibCallMentor']";

    public static String NotificationsPage_EmptyNotifications  = "//LinearLayout[@id='id_relative_empty_notifications']";
    public static String NotificationsPage_EmptyAnnouncement   = "//ImageView[@id='id_empty_announcement']";
    public static String NotificationsPage_ItemTitle           = "//AppCompatTextView[@id='tvTitle']";

    public static String NotifSettingsPage_LightBox            = "//AppCompatCheckBox[@id='id_checkbox_light']";
    public static String NotifSettingsPage_LightLabel          = "//AppCompatTextView[@value='Notification Light']";
    public static String NotifSettingsPage_LightText           = "//AppCompatTextView[@id='id_light_summary']";
    public static String NotifSettingsPage_NotificationsBox    = "//AppCompatCheckBox[@id='id_checkbox_notifications']";
    public static String NotifSettingsPage_NotificationsLabel  = "//AppCompatTextView[@value='Notifications']";
    public static String NotifSettingsPage_NotificationsText   = "//AppCompatTextView[@id='id_notifications_summary']";
    public static String NotifSettingsPage_SoundBox            = "//AppCompatCheckBox[@id='id_checkbox_sound']";
    public static String NotifSettingsPage_SoundLabel          = "//AppCompatTextView[@value='Notification sound']";
    public static String NotifSettingsPage_SoundText           = "//AppCompatTextView[@id='id_sound_summary']";
    public static String NotifSettingsPage_VibrateBox          = "//AppCompatCheckBox[@id='id_checkbox_vibrate']";
    public static String NotifSettingsPage_VibrateLabel        = "//AppCompatTextView[@value='Vibrate']";
    public static String NotifSettingsPage_VibrateText         = "//AppCompatTextView[@id='id_vibrate_summary']";

    public static String PacingGuide_Title             = "//AppCompatTextView[@value='Announcements']";

    public static String PageTitle_Announcements       = "//TextView[@value='Announcements']";
    public static String PageTitle_CourseChatter       = "//TextView[@value='Course Chatter']";
    public static String PageTitle_DegreePlan          = "//TextView[@value='Degree Plan']";
    public static String PageTitle_Mentors             = "//TextView[@value='Mentors']";
    public static String PageTitle_Notifications       = "//TextView[@value='Notifications']";
    public static String PageTitle_Resources           = "//TextView[@value='Resources']";

    public static String Presentation_DefaultBullet    = "No details";
    public static String ProfilePage_StudentFullname   = "//AppCompatTextView[@id='tvname']";
    public static String ProfilePage_DegreeTitle       = "//AppCompatTextView[@id='tvgroup']";
    public static String ProfilePage_StudentIdLabel    = "//AppCompatTextView[@id='tvstudentid']";
    public static String ProfilePage_StudentId         = "//AppCompatTextView[@id='tvstudentidval']";
    public static String ProfilePage_EmailLabel        = "//AppCompatTextView[@id='tvemail']";
    public static String ProfilePage_Email             = "//AppCompatTextView[@id='tvemailval']";
    public static String ProfilePage_PhoneLabel        = "//AppCompatTextView[@id='tvprimaryphone']";
    public static String ProfilePage_Phone             = "//AppCompatTextView[@id='tvprimaryphoneval']";
    public static String ProfilePage_AddressLabel      = "//AppCompatTextView[@id='tvaddress']";
    public static String ProfilePage_Address1          = "//AppCompatTextView[@id='tvaddressval']";
    public static String ProfilePage_Address2          = "//AppCompatTextView[@id='tvaddressval2']";

    public static String ResourcesPage_Academics       = "//AppCompatTextView[@value='Academics']";
    public static String ResourcesPage_AccomRequest    = "//AppCompatTextView[@value='Accommodation Request Form']";
    public static String ResourcesPage_Administrative  = "//AppCompatTextView[@value='Administrative']";
    public static String ResourcesPage_ApaRef          = "//AppCompatTextView[@value='APA Quick Reference Guide (Download)']";
    public static String ResourcesPage_CompCapstone    = "//AppCompatTextView[@value='Comprehensive Capstone Archive']";
    public static String ResourcesPage_CampusStore     = "//AppCompatTextView[@value='Campus Store']";
    public static String ResourcesPage_CareerCenter    = "//AppCompatTextView[@value='Career and Professional Development Center']";
    public static String ResourcesPage_ConsumerGuide      = "//AppCompatTextView[@value='Student Consumer Guide']";
    public static String ResourcesPage_ContactWGU      = "//ImageView[@id='id_iv_contact_wgu']";
    public static String ResourcesPage_CoverSheet      = "//AppCompatTextView[@value='Cover Sheet for Assessment Scores (Download)']";
    public static String ResourcesPage_DegreeResources = "//AppCompatTextView[@value='Degree Resources']";
    public static String ResourcesPage_EnrollHistory   = "//AppCompatTextView[@value='Enrollment and Loan Deferment History']";
    public static String ResourcesPage_FERPA           = "//AppCompatTextView[@value='FERPA Authorization/Removal Form']";
    public static String ResourcesPage_Forms           = "//AppCompatTextView[@value='Forms']";
    public static String ResourcesPage_GradeReport     = "//AppCompatTextView[@value='Grade Report (Download)']";
    public static String ResourcesPage_Handbook        = "//ImageView[@id='id_iv_student_hand_book']";
    public static String ResourcesPage_Library         = "//ImageView[@id='id_iv_library']";
    public static String ResourcesPage_ModelCapstone   = "//AppCompatTextView[@value='Model Capstone Archive']";
    public static String ResourcesPage_NightOwlArchives = "//AppCompatTextView[@value='The Night Owl Student Newsletter Archives']";
    public static String ResourcesPage_NightOwlBlog    = "//AppCompatTextView[@value='The Night Owl Blog']";
    public static String ResourcesPage_ReferAFriend    = "//AppCompatTextView[@value='Refer a Friend']";
    public static String ResourcesPage_RequestId       = "//AppCompatTextView[@value='Request a WGU ID Card']";
    public static String ResourcesPage_RequestTrans    = "//AppCompatTextView[@value='Request Official Transcript']";
    public static String ResourcesPage_Social          = "//AppCompatTextView[@value='Social']";
    public static String ResourcesPage_Student         = "//AppCompatTextView[@value='Student']";
    public static String ResourcesPage_StudentPortal   = "//AppCompatTextView[@value='Student Portal Website']";
    public static String ResourcesPage_StudentSuccess  = "//AppCompatTextView[@value='Student Success Appointment Request']";
    public static String ResourcesPage_StudentSuccessCenter  = "//AppCompatTextView[@value='Student Success Center']";
    public static String ResourcesPage_StudentVideo    = "//AppCompatTextView[@value='Student Video System']";
    public static String ResourcesPage_TaskStreamHelp  = "//AppCompatTextView[@value='TaskStream Help']";
    public static String ResourcesPage_TechDeals       = "//AppCompatTextView[@value='Technology and Software Deals']";
    public static String ResourcesPage_TermBreak       = "//AppCompatTextView[@value='Term Break Information Request']";
    public static String ResourcesPage_University      = "//AppCompatTextView[@value='University']";
    public static String ResourcesPage_UnofficialTrans = "//AppCompatTextView[@value='Unofficial Transcript (Download)']";
    public static String ResourcesPage_WGUNews         = "//AppCompatTextView[@value='WGU News']";
    public static String ResourcesPage_WGUVideos       = "//AppCompatTextView[@value='The WGU Video Channel']";
    public static String ResourcesPage_Turnitin        = "//AppCompatTextView[@value='What is Turnitin? (Download)']";

    public static String StudyPlanPage_ADAPolicy           = "//AppCompatTextView[@value='Accessibility Policy']";
    public static String StudyPlanPage_Announcements       = "//AppCompatTextView[@value='Announcements']";
    public static String StudyPlanPage_Assessments         = "//AppCompatTextView[@value='Assessments']";
    public static String StudyPlanPage_Title               = "//AppCompatTextView[@id='tvText']";
    public static String StudyPlanPage_Download            = "//AppCompatTextView[@value='Download Course as PDF']";
    public static String StudyPlanPage_Email               = "//ImageButton[@id='fab']";
    public static String StudyPlanPage_Expand              = "//ImageView[@id='id_expand_collapse']";
    public static String StudyPlanPage_Introduction        = "//AppCompatTextView[@value='Introduction']";
    public static String StudyPlanPage_LearningResourceTips = "//AppCompatTextView[@value='Tips']";
    public static String StudyPlanPage_PreparingForSuccess = "//AppCompatTextView[@value='Preparing for Success']";
    public static String StudyPlanPage_PopupOk             = "//AppCompatButton[@id='button2']";
    public static String StudyPlanPage_Support             = "//AppCompatTextView[@value='Student Support']";
    public static String StudyPlanPage_Tips                = "//AppCompatTextView[@value='Tips']";

    public static String SubjectActivitiesPage_ActivityName  = "//AppCompatTextView[@id='tvActivityTitle']";
    public static String SubjectActivitiesPage_MarkCompleted = "//AppCompatButton[@id='bMarkAsCompleted']";
    public static String SubjectActivitiesPage_MarkedAs      = "//AppCompatTextView[@id='tvMarkedAs']";
    public static String SubjectActivitiesPage_Skip          = "//AppCompatButton[@id='bMarkAsSkipped']";
    public static String SubjectActivitiesPage_TopicName     = "//AppCompatTextView[@id='tvTopicTitle']";

    public static String SubjectDetailsPage_SubjectTitle   = "//AppCompatTextView[@id='tvSubjectTitle']";
    public static String SubjectDetailsPage_TopicTitle     = "//AppCompatTextView[@id='tvTopicTitle']";
    public static String SubjectDetailsPage_ActivityBox    = "//LinearLayout[@id='llCardViewLayout']";
    public static String SubjectDetailsPage_ActivityTitle  = "//AppCompatTextView[@id='tvActivityTitle']";

    public static String TopicPage_Activities              = "//TextView[@id='id_activity_topics']";
    public static String TopicPage_Skip                    = "//AppCompatButton[@id='bMarkAsSkipped']";
    public static String TopicPage_MarkCompleted           = "//AppCompatButton[@id='bMarkAsCompleted']";
    public static String TopicPage_SubjectTitle            = "//AppCompatTextView[@id='tvSubjectTitle']";

    public static String UhOhDialog_Title                  = "//DialogTitle[@id='alertTitle']";
    public static String UhOhDialog_OkButton               = "//AppCompatButton[@id='button1']";

}

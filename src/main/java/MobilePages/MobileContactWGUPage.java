
package MobilePages;

import Utils.General;
import Utils.wguPowerPointFileUtils;

/*
 * Created by timothy.hallbeck on 6/26/2015.
 */

public class MobileContactWGUPage extends MobileDegreePlanPage {

    public MobileContactWGUPage() throws Exception {
        super();
        login();
        getContactWGU().click();
        WaitForPageLoad(MobileContactWGUPage.class);
        wguPowerPointFileUtils.saveScreenshot(this, "MobileContactWGUPage()");
    }
    public MobileContactWGUPage(MobileAppPage page) throws Exception {
        super(page);
    }

    public MobileContactWGUPage verifyBasicInfo() {
        WaitForPageLoad(MobileContactWGUPage.class);

/*        driverContacts
        List<WebElement> elements = getListByXpath(MobileXpath.ContactLink_GenericAddContact);
        for (WebElement element : elements) {
            element.click();
            driverContacts.navigate().back();
        }
*/
        General.assertDebug(getByXpath(MobileXpath.ContactLink_CareerServices) != null, "ContactLink_CareerServices exists");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_Enrollment) != null, "ContactLink_Enrollment exists");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_FinancialAid) != null, "ContactLink_FinancialAid exists");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_ITServiceDesk) != null, "ContactLink_ITServiceDesk exists");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_LearningResources) != null, "ContactLink_LearningResources exists");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_StudentAccounts) != null, "ContactLink_StudentAccounts exists");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_StudentRecords) != null, "ContactLink_StudentRecords exists");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_StudentServices) != null, "ContactLink_StudentServices exists");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_StudentSuccessCenter) != null, "ContactLink_StudentSuccessCenter exists");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_TermBreak) != null, "ContactLink_TermBreak exists");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_TransferEvaluation) != null, "ContactLink_TransferEvaluation exists");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_WritingCenter) != null, "ContactLink_WritingCenter exists");

        return this;
    }

    public MobileContactWGUPage verifyAllButtons() {
        General.assertDebug(getByXpath(MobileXpath.ContactLink_CareerServicesEmail) != null, "ContactLink_CareerServicesEmail is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_EnrollmentEmail) != null, "ContactLink_EnrollmentEmail is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_FinancialAidEmail) != null, "ContactLink_FinancialAidEmail is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_ITServiceDeskEmail) != null, "ContactLink_ITServiceDeskEmail is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_LearningResourcesEmail) != null, "ContactLink_LearningResourcesEmail is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_StudentAccountsEmail) != null, "ContactLink_StudentAccountsEmail is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_StudentRecordsEmail) != null, "ContactLink_StudentRecordsEmail is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_StudentServicesEmail) != null, "ContactLink_StudentServicesEmail is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_StudentSuccessCenterEmail) != null, "ContactLink_StudentSuccessCenterEmail is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_TermBreakEmail) != null, "ContactLink_TermBreakEmail is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_TransferEvaluationEmail) != null, "ContactLink_TransferEvaluationEmail is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_WritingCenterEmail) != null, "ContactLink_WritingCenterEmail is valid");

        General.assertDebug(getByXpath(MobileXpath.ContactLink_CareerServicesPhone) != null, "ContactLink_CareerServicesPhone is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_EnrollmentPhone) != null, "ContactLink_EnrollmentPhone is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_FinancialAidPhone) != null, "ContactLink_FinancialAidPhone is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_ITServiceDeskPhone) != null, "ContactLink_ITServiceDeskPhone is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_LearningResourcesPhone) != null, "ContactLink_LearningResourcesPhone is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_StudentAccountsPhone) != null, "ContactLink_StudentAccountsPhone is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_StudentRecordsPhone) != null, "ContactLink_StudentRecordsPhone is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_StudentServicesPhone) != null, "ContactLink_StudentServicesPhone is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_StudentSuccessCenterPhone) != null, "ContactLink_StudentSuccessCenterPhone is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_TermBreakPhone) != null, "ContactLink_TermBreakPhone is valid");
        General.assertDebug(getByXpath(MobileXpath.ContactLink_TransferEvaluationPhone) != null, "ContactLink_TransferEvaluationPhone is valid");

        return this;
    }

    public MobileContactWGUPage doSomethingRandom(int count) throws Exception {
        General.Debug("MobileContactWGUPage::doSomethingRandom()");

        NavigateToPage(MobileContactWGUPage.class);
        while (count-- > 0) {
            switch (General.getRandomInt(2)) {
                case 0:
                    break;
                case 1:
                    break;
                default:
                    break;
            }
        }

        return this;
    }
}

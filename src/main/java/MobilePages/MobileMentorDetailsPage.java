
package MobilePages;

import Utils.General;
import org.openqa.selenium.WebElement;

/*
 * Created by timothy.hallbeck on 4/9/2015.
 */

public class MobileMentorDetailsPage extends MobileMentorsPage {

    public MobileMentorDetailsPage() throws Exception {
        super();
        Sleep(1000);
        getTopMentorTitle().click();
        WaitForPageLoad(MobileMentorDetailsPage.class);
    }
    public MobileMentorDetailsPage(MobileAppPage page) throws Exception {
        super(page);
    }

    public WebElement getAddToContactsButton() {
        return getByXpath(MobileXpath.MentorDetailsPage_AddButton);
    }

    public MobileMentorDetailsPage addMentorToContacts() {
        WaitForPageLoad(MobileMentorDetailsPage.class);
        getAddToContactsButton().click();
        Sleep(3000);
        General.Debug("Need to check contact app manually or through another test.");

        return this;
    }

    public MobileMentorDetailsPage verifyBasics() {
        WaitForPageLoad(MobileMentorDetailsPage.class);

        getByXpath(MobileXpath.MentorDetailsPage_MentorPic).click();
        Sleep(2000);
//        assert(getByXpath(MobileXpath.MentorDetailsPage_MentorName).getText().contains(Data.getTopMentorDetailName()));
//        assert(getByXpath(MobileXpath.MentorDetailsPage_MentorDesignation).getText().contains(Data.getTopMentorDetailTitle()));

        getByXpath(MobileXpath.MentorDetailsPage_AddButton);
        getByXpath(MobileXpath.MentorDetailsPage_PhoneImage);
        getByXpath(MobileXpath.MentorDetailsPage_PhoneValue);
        getByXpath(MobileXpath.MentorDetailsPage_EmailImage);
        getByXpath(MobileXpath.MentorDetailsPage_EmailValue);
        getByXpath(MobileXpath.MentorDetailsPage_TimeZone);

        assert(getByXpath(MobileXpath.MentorDetailsPage_OfficeHours).getText().contains("Office Hours"));
        getByXpath(MobileXpath.MentorDetailsPage_DayMon);
        getByXpath(MobileXpath.MentorDetailsPage_DayTue);
        getByXpath(MobileXpath.MentorDetailsPage_DayWed);
        getByXpath(MobileXpath.MentorDetailsPage_DayThu);
        getByXpath(MobileXpath.MentorDetailsPage_DayFri);

        return this;
    }

    public MobileMentorDetailsPage doSomethingRandom(int count) throws Exception {
        General.Debug("MobileMentorDetailsPage::doSomethingRandom()");

        while (count-- > 0) {
            NavigateToPage(MobileMentorDetailsPage.class);
            switch (General.getRandomInt(2)) {
                case 0:
                    break;
                case 1:
                    break;
                default:
                    break;
            }
        }

        return this;
    }
}

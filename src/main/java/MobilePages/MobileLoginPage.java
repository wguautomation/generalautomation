
package MobilePages;

import BaseClasses.Page;
import Utils.General;
import Utils.wguPowerPointFileUtils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

/*
 * Created by timothy.hallbeck on 4/2/2015.
 */
public class MobileLoginPage extends MobileAppPage {

    public MobileLoginPage() throws Exception {
        General.Debug("creating MobileLoginPage");
    }
    public MobileLoginPage(MobileAppPage existingPage, long lAttributes) throws Exception {
        super(existingPage, lAttributes);
        Sleep(4000); // Get page error if signin page hasn't started to load before first get
        WaitForPageLoad(MobileLoginPage.class);
        wguPowerPointFileUtils.saveScreenshot(this, "MobileLoginPage");
    }

//    public WebElement getSignInLabel() {
//        return getSignInLabel(this);
//    }
//    public static WebElement getSignInLabel(Page page) {
//        return page.getByXpath(MobileXpath.LoginPage_SignInText);
//    }

    public WebElement getWGULogo() {
        return getWGULogo(this);
    }
    public static WebElement getWGULogo(Page page) {
        return page.getByXpath(MobileXpath.LoginPage_WGULogo);
    }

    public static WebElement getUsername(MobileAppPage page) {
        return page.getByXpath(MobileXpath.LoginPage_Username);
    }
    public void enterUsername(String username) {
        enterUsername(this, username);
    }
    public static void enterUsername(MobileAppPage page, String username) {
        getUsername(page).clear();
        getUsername(page).sendKeys(username);
        wguPowerPointFileUtils.addBulletPoint("enterUsername " + (username.isEmpty() ? "(blank)" : username));
    }

    public static WebElement getPassword(MobileAppPage page) {
        return page.getByXpath(MobileXpath.LoginPage_Password);
    }
    public void enterPassword(String username) {
        enterPassword(this, username);
    }
    public static void enterPassword(MobileAppPage page, String password) {
        getPassword(page).clear();
        getPassword(page).sendKeys(password);
        wguPowerPointFileUtils.addBulletPoint("enterPassword " + (password.isEmpty() ? "(blank)" : password));
    }

    public WebElement getNeedHelp() {
        return getNeedHelp(this);
    }
    public static WebElement getNeedHelp(Page page) {
        return page.getByXpath(MobileXpath.LoginPage_NeedHelpLink);
    }

    public WebElement getKeepMeSignedIn() {
        return getKeepMeSignedIn(this);
    }
    public static WebElement getKeepMeSignedIn(Page page) {
        return page.getByXpath(MobileXpath.LoginPage_KeepSignedIn);
    }

    public WebElement getSignInButton() {
        return getSignInButton(this);
    }
    public static WebElement getSignInButton(Page page) {
        return page.getByXpath(MobileXpath.LoginPage_SignInButton);
    }
    public void clickSignInButton() {
        clickSignInButton(this);
    }
    public static void clickSignInButton(Page page) {
//        getSignInButton(page).click();
        getSignInButton(page).sendKeys(Keys.RETURN);
        wguPowerPointFileUtils.addBulletPoint("clickSignInButton()");
    }

    public WebElement getLoginErrorMessage() {
        return getLoginErrorMessage(this);
    }
    public static WebElement getLoginErrorMessage(Page page) {
        return page.getByXpath(MobileXpath.LoginPage_LoginErrorMsg);
    }

    public void verifyNonActiveLoginMessage() {
        wguPowerPointFileUtils.saveScreenshot(this, "verifyNonActiveLoginMessage");
        String message = getLoginErrorMessage().getText();
        wguPowerPointFileUtils.addBulletPoint("Verify message appears");
        Assert.assertTrue(message.contains("mobile app is currently only available for active students"), "Incorrect error text for a non-active student login");
    }

    public WebElement getNotAStudent() {
        return getNotAStudent(this);
    }
    public static WebElement getNotAStudent(Page page) {
        return page.getByXpath(MobileXpath.LoginPage_NotAStudentLink);
    }

    public MobileLoginPage verifyEnableDisableSignInButton() {
        // Sign in button should be disabled until both username and password have something
        wguPowerPointFileUtils.saveScreenshot(this, "MobileLoginPage");
        enterUsername("");
        enterPassword("");
        assert(!getSignInButton().isEnabled());
        wguPowerPointFileUtils.saveScreenshot(this, "MobileLoginPage verifyEnableDisableSignInButton()");
        wguPowerPointFileUtils.addBulletPoint("username and password blank");
        wguPowerPointFileUtils.addBulletPoint("Verified Sign In button is disabled");

        enterUsername("blah");
        assert(!getSignInButton().isEnabled());
        wguPowerPointFileUtils.saveScreenshot(this, "MobileLoginPage verifyEnableDisableSignInButton()");
        wguPowerPointFileUtils.addBulletPoint("username ok");
        wguPowerPointFileUtils.addBulletPoint("password blank");
        wguPowerPointFileUtils.addBulletPoint("Verified Sign In button is disabled");

        enterUsername("");
        enterPassword("blah");
        assert(!getSignInButton().isEnabled());
        wguPowerPointFileUtils.saveScreenshot(this, "MobileLoginPage verifyEnableDisableSignInButton()");
        wguPowerPointFileUtils.addBulletPoint("username blank");
        wguPowerPointFileUtils.addBulletPoint("password ok");
        wguPowerPointFileUtils.addBulletPoint("Verified Sign In button is disabled");

        enterUsername("blah");
        assert(getSignInButton().isEnabled());
        wguPowerPointFileUtils.saveScreenshot(this, "MobileLoginPage verifyEnableDisableSignInButton()");
        wguPowerPointFileUtils.addBulletPoint("username and password have values");
        wguPowerPointFileUtils.addBulletPoint("Verified Sign In button is enabled");

        return this;
    }

    public MobileLoginPage verifyBasicInfo() {
        wguPowerPointFileUtils.saveScreenshot(this, "MobileLoginPage verifyBasicInfo()");

//        General.assertDebug(getSignInLabel() != null, "verify SignInLabel present");
        General.assertDebug(getWGULogo()        != null, "verify WGULogo present");
        General.assertDebug(getNeedHelp()       != null, "verify NeedHelp present");
        General.assertDebug(getKeepMeSignedIn() != null, "verify KeepMeSignedIn present");
        General.assertDebug(getSignInButton()   != null, "verify SignInButton present");

        General.assertDebug(getByXpath(MobileXpath.LoginPage_SignInText) != null, "verify SignInButton says Sign In");

        enterUsername("");
        enterPassword("");

        return this;
    }

    public MobileLoginPage verifyFailedAttempts() {
        wguPowerPointFileUtils.saveScreenshot(this, "verifyFailedAttempts() bad password");
        enterUsername(username);
        enterPassword("kkkk");
        clickSignInButton();
        assert(getLoginErrorMessage() != null);
        wguPowerPointFileUtils.saveScreenshot(this, "verifyFailedAttempts() w/message");
        wguPowerPointFileUtils.addBulletPoint("getLoginErrorMessage() ok");

        wguPowerPointFileUtils.saveScreenshot(this, "verifyFailedAttempts() bad username");
        enterUsername("kkkk");
        enterPassword(password);
        clickSignInButton();
        assert(getLoginErrorMessage() != null);
        wguPowerPointFileUtils.saveScreenshot(this, "verifyFailedAttempts() w/message");
        wguPowerPointFileUtils.addBulletPoint("getLoginErrorMessage() ok");

        return this;
    }

    public MobileLoginPage ExercisePage(boolean cascade) {
        General.Debug("\nMobileLoginPage::ExercisePage(" + cascade + ")");
        super.ExercisePage(cascade);

        return this;
    }
}


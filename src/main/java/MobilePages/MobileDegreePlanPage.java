
package MobilePages;

import Utils.General;
import Utils.wguPowerPointFileUtils;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

import java.util.List;

/*
 * Created by timothy.hallbeck on 4/6/2015.
 */

public class MobileDegreePlanPage extends MobileAppPage {

    public MobileDegreePlanPage() throws Exception {
        this(0L);
        Sleep(1000);
    }
    public MobileDegreePlanPage(long presets) throws Exception {
        super(null, presets);
        login();
        wguPowerPointFileUtils.saveScreenshot(this, "MobileDegreePlanPage()");
    }
    public MobileDegreePlanPage(MobileAppPage page) throws Exception {
        super(page);
    }

    public List<WebElement> getListOfCourses() {
        General.Debug("getListOfCourses()");

        return getListOfCourses(this);
    }
    public static List<WebElement> getListOfCourses(MobileAppPage page) {
        General.Debug("getListOfCourses()");

        return page.getListByXpath(MobileXpath.DegreePlan_CourseCode);
    }

    public List<WebElement> getListOfCourses(String filter) {
        General.Debug("getListOfCourses()");

        return getListOfCourses(this, filter);
    }
    public static List<WebElement> getListOfCourses(MobileAppPage page, String filter) {
        General.Debug("getListOfCourses(" + filter + ")");
        List<WebElement> courses = getListOfCourses(page);

        int index = courses.size() - 1;
        for (int i=index; i >= 0; i--) {
            if (!courses.get(i).getText().contains(filter))
                courses.remove(i);
            else
                General.Debug("  " + courses.get(i).getText());
        }
        return courses;
    }
    public List<WebElement> getListOfVisibleCourses(String filter) {
        General.Debug("getListOfVisibleCourses(" + filter + ")");
        List<WebElement> courses = getListOfVisibleCourses();

        int index = courses.size() - 1;
        for (int i=index; i >= 0; i--) {
            if (!courses.get(i).getText().contains(filter))
                courses.remove(i);
            else
                General.Debug("  " + courses.get(i).getText());
        }
        return courses;
    }

    static public WebElement ScrollToCourse(MobileAppPage page, String partialText) {
        int maxSwipes = 6;
        page.swipeDown(maxSwipes);

        WebElement element = getListOfCourses(page, partialText).get(0);
        if (element == null)
            throw new RuntimeException("  Course [" + partialText + "] not found");

        Point point = element.getLocation();
        while (point.x < 0 && maxSwipes-- > 0 ) {
            page.swipeUp(1);
            element = getListOfCourses(page, partialText).get(0);
            point = element.getLocation();
        }

        return element;
    }

    public List<WebElement> getListOfVisibleCourses() {
        return getListOfVisibleCourses(this);
    }
    public static List<WebElement> getListOfVisibleCourses(MobileAppPage page) {
        General.Debug("getListOfVisibleCourses()");
        List<WebElement> courses = page.getListByXpath(MobileXpath.DegreePlan_CourseCode);

        int index = courses.size() - 1;
        for (int i=index; i >= 0; i--) {
            if (!(courses.get(i).isDisplayed() && courses.get(i).isEnabled())) {
                General.Debug("  removing " + courses.get(i).getText());
                courses.remove(i);
            }
            else
                General.Debug("  " + courses.get(i).getText());
        }

        return courses;
    }

    public MobileDegreePlanPage clickTopCourse() {
        WaitForPageLoad(MobileDegreePlanPage.class);
        getListOfCourses().get(0).click();
        Sleep(2000);

        return this;
    }

    public MobileDegreePlanPage clickCourse(String courseAbbr) {
        WaitForPageLoad(MobileDegreePlanPage.class);
        getListOfVisibleCourses("of").get(0).click();
        Sleep(3000);

        return this;
    }

    public MobileAppPage checkPassedNotPassed() {
        return this;
    }

    public MobileDegreePlanPage verifyBasics() {
        // Currently this only checks all the info for the 1st item. Need to figure out how to get and iterate
        //   through all the items eventually.
        getByXpath(MobileXpath.PageTitle_DegreePlan);
        getByXpath(MobileXpath.DegreePlan_CourseCode);
        getByXpath(MobileXpath.DegreePlan_CourseForDate);
        getByXpath(MobileXpath.DegreePlan_CourseUnits);
        getByXpath(MobileXpath.DegreePlan_Percentage);
        getByXpath(MobileXpath.DegreePlan_PieChart);
//        getByXpath(MobileXpath.DegreePlan_Status);
        getByXpath(MobileXpath.DegreePlan_TermDate);
//        getByXpath(MobileXpath.DegreePlan_VersionID);

        return this;
    }

    public MobileDegreePlanPage ExercisePage(boolean cascade) {
        General.Debug("\nMobileDegreePlanPage::ExercisePage(" + cascade + ")");
        swipeLeft(MobileXpath.DegreePlan_CourseForDate, 3);
        swipeRight(MobileXpath.DegreePlan_CourseForDate, 3);

        return this;
    }

    public MobileDegreePlanPage doSomethingRandom(int count) throws Exception {
        General.Debug("MobileDegreePlanPage::doSomethingRandom()");
        super.doSomethingRandom(count);

        return this;
    }

}

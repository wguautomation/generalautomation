
package MobilePages;

import Utils.General;
import Utils.wguPowerPointFileUtils;
import org.openqa.selenium.WebElement;

/*
 * Created by timothy.hallbeck on 4/8/2015.
 */

public class MobileNotificationsPage extends MobileAppPage {

    public MobileNotificationsPage() throws Exception {
        super();
        login();
        getNotificationsIcon().click();
        Sleep(4000);
        WaitForPageLoad(MobileNotificationsPage.class);
        wguPowerPointFileUtils.saveScreenshot(this, "MobileNotificationsPage");
    }
    public MobileNotificationsPage(MobileAppPage page) throws Exception {
        super(page);
    }

    public MobileNotificationsPage verifyEmptyNotifications() {
        WaitForPageLoad(MobileNotificationsPage.class);

        WebElement element = getByXpath(MobileXpath.NotificationsPage_EmptyNotifications, true);
        if (element == null) {
            getByXpath(MobileXpath.NotificationsPage_ItemTitle);
            wguPowerPointFileUtils.addBulletPoint("found notifications");
        } else
            wguPowerPointFileUtils.addBulletPoint("verifyEmptyNotificationsText()");

        return this;
    }

    public MobileNotificationsPage verifyNotificationText() {
        WaitForPageLoad(MobileNotificationsPage.class);

        WebElement element = getByXpath(MobileXpath.NotificationsPage_EmptyAnnouncement, true);
        // If empty isn't found, must be at least 1 message
        if (element == null) {
            getByXpath(MobileXpath.NotificationsPage_ItemTitle);
            wguPowerPointFileUtils.addBulletPoint("found notifications");
        } else
            wguPowerPointFileUtils.addBulletPoint("verifyEmptyNotificationsText()");

        return this;
    }

    public MobileNotificationsPage doSomethingRandom(int count) throws Exception {
        General.Debug("MobileNotificationsPage::doSomethingRandom()", true);

        while (count-- > 0) {
            NavigateToPage(MobileNotificationsPage.class);
            switch (General.getRandomInt(2)) {
                case 0:
                    break;
                case 1:
                    break;
                default:
                    break;
            }
        }

        return this;
    }

}


package MobilePages;

import Utils.General;
import org.openqa.selenium.WebElement;

/*
 * Created by timothy.hallbeck on 4/13/2015.
 */
public class MobileFeedbackPage extends MobileAboutAppPage {

    public MobileFeedbackPage() throws Exception {
        super();
        getFeedback(this).click();
        Sleep(1000);
        WaitForPageLoad(MobileFeedbackPage.class);
    }
    public MobileFeedbackPage(MobileAppPage page) throws Exception {
        super(page);
    }

    public MobileFeedbackPage clickSend() {
        getByXpath(MobileXpath.FeedbackPage_SendOption).click();

        return this;
    }

    public MobileFeedbackPage selectComment() {
        getByXpath(MobileXpath.FeedbackPage_Spinner).click();
        Sleep(1000);
        getByXpath(MobileXpath.FeedbackPage_Spinner1).click();
        Sleep(1000);

        return this;
    }

    public MobileFeedbackPage selectFeatureRequest() {
        getByXpath(MobileXpath.FeedbackPage_Spinner).click();
        Sleep(1000);
        getByXpath("(//AppCompatTextView[@id='id_feedback_spinner_subjects')[2]]").click();
        Sleep(1000);

        return this;
    }

    public MobileFeedbackPage selectReportAProblem() {
        getByXpath(MobileXpath.FeedbackPage_Spinner).click();
        Sleep(1000);
        getByXpath("(//AppCompatTextView[@id='id_feedback_spinner_subjects')[3]]").click();
        Sleep(1000);

        return this;
    }

    public static WebElement getDescription(MobileAppPage page) {
        return page.getByXpath(MobileXpath.FeedbackPage_Desc);
    }
    public static MobileAppPage typeDescriptionText(MobileAppPage page, String text) {
        getDescription(page).sendKeys(text);
        return page;
    }

    public MobileFeedbackPage submitFeedback() {
        verifyBasicInfo();
        selectComment();
        typeDescriptionText(this, "Hello there");
        clickSend();
        Sleep(1000);
        WaitForPageLoad(MobileAboutAppPage.class);

        return this;
    }

    public MobileFeedbackPage verifyBasicInfo() {
        assert(getByXpath(MobileXpath.FeedbackPage_Instructions) != null);
        assert(getByXpath(MobileXpath.FeedbackPage_Spinner) != null);
        assert(getByXpath(MobileXpath.FeedbackPage_NeedHelp) != null);

        return this;
    }

    public MobileFeedbackPage doSomethingRandom(int count) throws Exception {
        General.Debug("MobileFeedbackPage::doSomethingRandom()");

        while (count-- > 0) {
            NavigateToPage(MobileFeedbackPage.class);
            switch (General.getRandomInt(2)) {
                case 0:
                    break;
                case 1:
                    break;
                default:
                    break;
            }
        }

        return this;
    }
}



package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfReportsPage extends sfHomePage {
    public sfReportsPage() {
        this(null);
    }

    public sfReportsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfReportsPage load() {
        super.load();
        clickReports();
        return this;
    }

    public sfReportsPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



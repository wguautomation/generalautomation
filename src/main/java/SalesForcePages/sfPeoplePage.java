
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfPeoplePage extends sfHomePage {
    public sfPeoplePage() {
        this(null);
    }

    public sfPeoplePage(WebPage existingPage) {
        super(existingPage);
    }

    public sfPeoplePage load() {
        super.load();
        clickPeople();
        return this;
    }

    public sfPeoplePage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



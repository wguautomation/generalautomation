
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfWGUDegreeProgramsPage extends sfHomePage {
    public sfWGUDegreeProgramsPage() {
        this(null);
    }

    public sfWGUDegreeProgramsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfWGUDegreeProgramsPage load() {
        super.load();
        clickWGUDegreePrograms();
        return this;
    }

    public sfWGUDegreeProgramsPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentProcessesPage extends sfHomePage {
    public sfStudentProcessesPage() {
        this(null);
    }

    public sfStudentProcessesPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentProcessesPage load() {
        super.load();
        clickStudentProcesses();
        return this;
    }

    public sfStudentProcessesPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}

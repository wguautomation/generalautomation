
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentCasesPage extends sfHomePage {
    public sfStudentCasesPage() {
        this(null);
    }

    public sfStudentCasesPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentCasesPage load() {
        super.load();
        clickStudentCases();
        return this;
    }

    public sfStudentCasesPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}

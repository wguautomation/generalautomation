
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfOrientationPage extends sfFilteredReportPage {
    public sfOrientationPage() {
        this(null);
    }

    public sfOrientationPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureAcceptButton(true);
        ConfigureAssignButton(true);
        ConfigureRefreshButton(true);
    }

    public sfOrientationPage load() {
        super.load();
        clickOrientation();
        return this;
    }

    public sfOrientationPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



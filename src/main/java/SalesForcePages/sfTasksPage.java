
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/21/2015.
 */
public class sfTasksPage extends sfHomePage {
    public sfTasksPage() {
        this(null);
    }

    public sfTasksPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfTasksPage load() {
        super.load();
        clickTasks();
        return this;
    }

    public sfTasksPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}


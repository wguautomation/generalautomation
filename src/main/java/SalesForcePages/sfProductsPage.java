
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfProductsPage extends sfHomePage {
    public sfProductsPage() {
        this(null);
    }

    public sfProductsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfProductsPage load() {
        super.load();
        clickProducts();
        return this;
    }

    public sfProductsPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



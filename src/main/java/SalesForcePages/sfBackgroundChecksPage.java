
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfBackgroundChecksPage extends sfFilteredReportPage {
    public sfBackgroundChecksPage() {
        this(null);
    }

    public sfBackgroundChecksPage(WebPage existingPage) {
        super(existingPage);
        ConfigureNewButton(true);
        ConfigureGoButton(true);
    }

    public sfBackgroundChecksPage load() {
        super.load();
        clickBackgroundChecks();
        return this;
    }

    public sfBackgroundChecksPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



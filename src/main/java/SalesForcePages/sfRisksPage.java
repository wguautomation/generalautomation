
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfRisksPage extends sfFilteredReportPage {
    public sfRisksPage() {
        this(null);
    }

    public sfRisksPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureRefreshButton(true);
    }

    public sfRisksPage load() {
        super.load();
        clickRisks();
        return this;
    }

    public sfRisksPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



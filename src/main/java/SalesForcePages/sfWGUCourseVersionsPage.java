
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfWGUCourseVersionsPage extends sfHomePage {
    public sfWGUCourseVersionsPage() {
        this(null);
    }

    public sfWGUCourseVersionsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfWGUCourseVersionsPage load() {
        super.load();
        clickWGUCourseVersions();
        return this;
    }

    public sfWGUCourseVersionsPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


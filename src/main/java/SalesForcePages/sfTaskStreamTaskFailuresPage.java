
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/21/2015.
 */
public class sfTaskStreamTaskFailuresPage extends sfHomePage {
    public sfTaskStreamTaskFailuresPage() {
        this(null);
    }

    public sfTaskStreamTaskFailuresPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfTaskStreamTaskFailuresPage load() {
        super.load();
        clickTaskStreamTaskFailures();
        return this;
    }

    public sfTaskStreamTaskFailuresPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}


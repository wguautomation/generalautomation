
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfPortfoliosPage extends sfFilteredReportPage {
    public sfPortfoliosPage() {
        this(null);
    }

    public sfPortfoliosPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureAssignButton(true);
        ConfigureRefreshButton(true);
    }

    public sfPortfoliosPage load() {
        super.load();
        clickPortfolios();
        return this;
    }

    public sfPortfoliosPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}




package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfKeyValuesPage extends sfFilteredReportPage {
    public sfKeyValuesPage() {
        this(null);
    }

    public sfKeyValuesPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureAssignButton(true);
        ConfigureRefreshButton(true);
    }

    public sfKeyValuesPage load() {
        super.load();
        clickKeyValues();
        return this;
    }

    public sfKeyValuesPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
       }

        return this;
    }

}



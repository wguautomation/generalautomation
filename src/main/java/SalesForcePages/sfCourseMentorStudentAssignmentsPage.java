
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfCourseMentorStudentAssignmentsPage extends sfFilteredReportPage {
    public sfCourseMentorStudentAssignmentsPage() {
        this(null);
    }

    public sfCourseMentorStudentAssignmentsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureNewButton(true);
        ConfigureGoButton(true);
        ConfigureRefreshButton(true);
    }

    public sfCourseMentorStudentAssignmentsPage load() {
        super.load();
        clickCourseMentorStudentAssignments();
        return this;
    }

    public sfCourseMentorStudentAssignmentsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
       }

        return this;
    }

}



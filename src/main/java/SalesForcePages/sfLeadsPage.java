
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfLeadsPage extends sfFilteredReportPage {
    public sfLeadsPage() {
        this(null);
    }

    public sfLeadsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureAssignButton(true);
        ConfigureRefreshButton(true);
    }

    public sfLeadsPage load() {
        super.load();
        clickLeads();
        return this;
    }

    public sfLeadsPage clickLeadLifetime() {
        getByLinkText("Lead Lifetime");
        getByLinkText("Lead Lifetime").click();
        return this;
    }

    public sfLeadsPage clickLeadsBySource() {
        getByLinkText("Leads By Source");
        getByLinkText("Leads By Source").click();
        return this;
    }

    public sfLeadsPage clickBouncedLeads() {
        getByLinkText("Bounced Leads");
        getByLinkText("Bounced Leads").click();
        return this;
    }

    public sfLeadsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        clickLeads();
        clickLeadLifetime();

        clickLeads();
        clickLeadsBySource();

        clickLeads();
        clickBouncedLeads();

        clickLeads();
        return this;
    }

}




package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfProfileOverviewPage extends sfHomePage {
    public sfProfileOverviewPage() {
        this(null);
    }

    public sfProfileOverviewPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfProfileOverviewPage load() {
        super.load();
        clickProfileOverview();
        return this;
    }

    public sfProfileOverviewPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



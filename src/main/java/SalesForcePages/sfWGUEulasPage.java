
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfWGUEulasPage extends sfHomePage {
    public sfWGUEulasPage() {
        this(null);
    }

    public sfWGUEulasPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfWGUEulasPage load() {
        super.load();
        clickWGUEulas();
        return this;
    }

    public sfWGUEulasPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


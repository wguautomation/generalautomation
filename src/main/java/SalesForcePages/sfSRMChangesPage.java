
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfSRMChangesPage extends sfHomePage {
    public sfSRMChangesPage() {
        this(null);
    }

    public sfSRMChangesPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfSRMChangesPage load() {
        super.load();
        clickSRMChanges();
        return this;
    }

    public sfSRMChangesPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



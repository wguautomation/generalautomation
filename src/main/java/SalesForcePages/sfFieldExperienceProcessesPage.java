
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfFieldExperienceProcessesPage extends sfFilteredReportPage {
    public sfFieldExperienceProcessesPage() {
        this(null);
    }

    public sfFieldExperienceProcessesPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureRefreshButton(true);
    }

    public sfFieldExperienceProcessesPage load() {
        super.load();
        clickFieldExperienceProcesses();
        return this;
    }

    public sfFieldExperienceProcessesPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}


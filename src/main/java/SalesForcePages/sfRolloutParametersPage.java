
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfRolloutParametersPage extends sfFilteredReportPage {
    public sfRolloutParametersPage() {
        this(null);
    }

    public sfRolloutParametersPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureAssignButton(true);
        ConfigureRefreshButton(true);
    }

    public sfRolloutParametersPage load() {
        super.load();
        clickRolloutParameters();
        return this;
    }

    public sfRolloutParametersPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



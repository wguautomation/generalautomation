
package SalesForcePages;

import BaseClasses.WebPage;
import org.openqa.selenium.Keys;
import java.util.ArrayList;

/*
 * Created by timothy.hallbeck on 2/20/2015.
 */
public class sfDeveloperConsolePage extends sfHomePage {
    public sfDeveloperConsolePage() {
        this(null);
    }

    public sfDeveloperConsolePage(WebPage existingPage) {
        super(existingPage);
    }

    public sfDeveloperConsolePage load() {
        super.load();
        clickDeveloperConsole();
        return this;
    }

    public sfDeveloperConsolePage clickLogs() {
        getByXpath("//*[@id=\"tab-1153-btnInnerEl\"]").click();
        getByXpath("//*[@id=\"gridcolumn-1082-textEl\"]");
        return this;
    }

    public sfDeveloperConsolePage clickTests() {
        getByXpath("//*[@id=\"tab-1154-btnInnerEl\"]").click();
        getByXpath("//*[@id=\"gridcolumn-1053-textEl\"]");
        return this;
    }

    public sfDeveloperConsolePage clickCheckpoints() {
        getByXpath("//*[@id=\"tab-1155-btnInnerEl\"]").click();
        getByXpath("//*[@id=\"gridcolumn-1103-textEl\"]");
        return this;
    }

    public sfDeveloperConsolePage clickQueryEditor() {
        getByXpath("//*[@id=\"tab-1156-btnInnerEl\"]").click();
        getByXpath("//*[@id=\"queryEditorText-inputEl\"]");
        return this;
    }

    public sfDeveloperConsolePage clickViewState() {
        getByXpath("//*[@id=\"tab-1157-btnInnerEl\"]").click();
        getByXpath("//*[@id=\"treecolumn-1124-textEl\"]");
        return this;
    }

    public sfDeveloperConsolePage clickProgress() {
        getByXpath("//*[@id=\"tab-1158-btnInnerEl\"]").click();
        getByXpath("//*[@id=\"reqId-textEl\"]");
        return this;
    }

    public sfDeveloperConsolePage clickProblems() {
        getByXpath("//*[@id=\"tab-1159-btnInnerEl\"]").click();
        getByXpath("//*[@id=\"gridcolumn-1150-textEl\"]");
        return this;
    }

    public ArrayList<String> executeQuery(String soqlText) {
        String singleString;
        ArrayList<String> stringArray = new ArrayList<String>();

        getByXpath("//*[@id='tab-1172-btnInnerEl']").click();
        getByXpath("//*[@id='queryEditorText-inputEl']").click();
        Sleep(1000);
        getByXpath("//*[@id='queryEditorText-inputEl']").sendKeys(Keys.CONTROL + "a");
        Sleep(1000);
        getByXpath("//*[@id='queryEditorText-inputEl']").sendKeys(soqlText);
        Sleep(1000);
        getByXpath("//*[@id='queryExecuteButton-btnInnerEl']").click();

        long saveTimeout = TIMEOUT_IN_SECS;
        setTimeoutInSecs(1);

        for (int i=2; i<100; i++) {
            singleString = getTextByXpath("//*[@id=\"gridview-1199\"]/table/tbody/tr[" + i + "]/td/div", true);
            if (singleString != null)
                stringArray.add(singleString);
            else
                break;
        }

        setTimeoutInSecs(saveTimeout);
        return stringArray;
    }

    public sfDeveloperConsolePage ExercisePage(boolean cascade) {
        load();

        clickLogs();
        clickTests();
        clickCheckpoints();
        clickQueryEditor();
        clickViewState();
        clickProgress();
        clickProblems();

        if (cascade) {
            return this;
        }

        return this;
    }

}

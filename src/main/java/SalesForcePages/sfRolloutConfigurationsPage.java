
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfRolloutConfigurationsPage extends sfFilteredReportPage {
    public sfRolloutConfigurationsPage() {
        this(null);
    }

    public sfRolloutConfigurationsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureAssignButton(true);
        ConfigureRefreshButton(true);
    }

    public sfRolloutConfigurationsPage load() {
        super.load();
        clickRolloutConfigurations();
        return this;
    }

    public sfRolloutConfigurationsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



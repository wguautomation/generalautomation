
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/23/2015.
 */
public class sfNewItemPage extends sfFilteredReportPage {
    public sfNewItemPage() {
        this(null);
    }

    public sfNewItemPage(WebPage existingPage) {
        super(existingPage);
    }

    public void clickSaveButton() {
        getByXpath("//*[@name=\"save\"]").click();
    }

    public void clickSaveAndNewButton() {
        getByXpath("//*[@name=\"save_new\"]").click();
    }

}



package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfGridWizardPage extends sfHomePage {
    public sfGridWizardPage() {
        this(null);
    }

    public sfGridWizardPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfGridWizardPage load() {
        super.load();
        clickGridWizard();
        return this;
    }

    public sfGridWizardPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}




package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentCompletionCoursesPage extends sfHomePage {
    public sfStudentCompletionCoursesPage() {
        this(null);
    }

    public sfStudentCompletionCoursesPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentCompletionCoursesPage load() {
        super.load();
        clickStudentCompletionCourses();
        return this;
    }

    public sfStudentCompletionCoursesPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}

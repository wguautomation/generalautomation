
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfDuplicateRecordSetsPage extends sfFilteredReportPage {
    public sfDuplicateRecordSetsPage() {
        this(null);
    }

    public sfDuplicateRecordSetsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureNewButton(true);
        ConfigureGoButton(true);
        ConfigureRefreshButton(true);
    }

    public sfDuplicateRecordSetsPage load() {
        super.load();
        clickDuplicateRecordSets();
        return this;
    }

    public sfDuplicateRecordSetsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}


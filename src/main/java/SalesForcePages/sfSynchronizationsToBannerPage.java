
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfSynchronizationsToBannerPage extends sfHomePage {
    public sfSynchronizationsToBannerPage() {
        this(null);
    }

    public sfSynchronizationsToBannerPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfSynchronizationsToBannerPage load() {
        super.load();
        clickSynchronizationsToBanner();
        return this;
    }

    public sfSynchronizationsToBannerPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}

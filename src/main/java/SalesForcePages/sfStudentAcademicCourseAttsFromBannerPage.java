
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentAcademicCourseAttsFromBannerPage extends sfHomePage {
    public sfStudentAcademicCourseAttsFromBannerPage() {
        this(null);
    }

    public sfStudentAcademicCourseAttsFromBannerPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentAcademicCourseAttsFromBannerPage load() {
        super.load();
        clickStudentAcademicCourseAttsFromBanner();
        return this;
    }

    public sfStudentAcademicCourseAttsFromBannerPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}



package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfContactsPage extends sfFilteredReportPage {
    public sfContactsPage() {
        this(null);
    }

    public sfContactsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureNewButton(true);
        ConfigureGoButton(true);
    }

    public sfContactsPage load() {
        super.load();
        clickContacts();
        return this;
    }

    protected sfContactsPage clickHTMLEmailStatusReport() {
        getByLinkText("HTML Email Status Report").click();
        return this;
    }

    protected sfContactsPage clickPartnerAccounts() {
        getByLinkText("Partner Accounts").click();
        return this;
    }

    protected sfContactsPage clickMailingList() {
        getByLinkText("Mailing List").click();
        return this;
    }

    protected sfContactsPage clickContactHistoryReport() {
        getByLinkText("Contact History Report").click();
        return this;
    }

    protected sfContactsPage clickBouncedPersonAccounts() {
        getByLinkText("Bounced PersonServiceOsso Accounts").click();
        return this;
    }

    public sfContactsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        clickHTMLEmailStatusReport();
        clickBrowserBackButton();

        clickPartnerAccounts();
        clickBrowserBackButton();

        clickMailingList();
        clickBrowserBackButton();

        clickContactHistoryReport();
        clickBrowserBackButton();

        clickBouncedPersonAccounts();
        clickBrowserBackButton();
        return this;
    }

}




















package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfTransferEvaluationsPage extends sfHomePage {
    public sfTransferEvaluationsPage() {
        this(null);
    }

    public sfTransferEvaluationsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfTransferEvaluationsPage load() {
        super.load();
        clickTransferEvaluations();
        return this;
    }

    public sfTransferEvaluationsPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


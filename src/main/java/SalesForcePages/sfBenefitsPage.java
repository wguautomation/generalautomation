
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfBenefitsPage extends sfFilteredReportPage {
    public sfBenefitsPage() {
        this(null);
    }

    public sfBenefitsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureNewButton(true);
        ConfigureGoButton(true);
    }

    public sfBenefitsPage load() {
        super.load();
        clickBenefits();
        return this;
    }

    public sfBenefitsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}


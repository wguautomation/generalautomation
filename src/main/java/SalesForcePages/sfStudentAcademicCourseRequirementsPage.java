
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentAcademicCourseRequirementsPage extends sfHomePage {
    public sfStudentAcademicCourseRequirementsPage() {
        this(null);
    }

    public sfStudentAcademicCourseRequirementsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentAcademicCourseRequirementsPage load() {
        super.load();
        clickStudentAcademicCourseRequirements();
        return this;
    }

    public sfStudentAcademicCourseRequirementsPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}

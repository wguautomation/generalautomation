
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfAnswersPage extends sfHomePage {
    public sfAnswersPage() {
        this(null);
    }

    public sfAnswersPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfAnswersPage load() {
        super.load();
        clickAnswers();
        return this;
    }

    public sfAnswersPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


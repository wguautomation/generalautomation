
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfWGUIdeasPage extends sfHomePage {
    public sfWGUIdeasPage() {
        this(null);
    }

    public sfWGUIdeasPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfWGUIdeasPage load() {
        super.load();
        clickWGUIdeas();
        return this;
    }

    public sfWGUIdeasPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



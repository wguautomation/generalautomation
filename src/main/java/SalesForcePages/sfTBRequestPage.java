
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfTBRequestPage extends sfHomePage {
    public sfTBRequestPage() {
        this(null);
    }

    public sfTBRequestPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfTBRequestPage load() {
        super.load();
        clickTBRequest();
        return this;
    }

    public sfTBRequestPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


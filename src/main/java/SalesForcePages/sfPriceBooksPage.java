
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfPriceBooksPage extends sfFilteredReportPage {
    public sfPriceBooksPage() {
        this(null);
    }

    public sfPriceBooksPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureRefreshButton(true);
    }

    public sfPriceBooksPage load() {
        super.load();
        clickPriceBooks();
        return this;
    }

    public sfPriceBooksPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



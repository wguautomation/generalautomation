
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfSiteDotComPage extends sfHomePage {
    public sfSiteDotComPage() {
        this(null);
    }

    public sfSiteDotComPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfSiteDotComPage load() {
        super.load();
        clickSiteDotCom();
        return this;
    }

    public sfSiteDotComPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}




package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfWGUStateLicensurePage extends sfHomePage {
    public sfWGUStateLicensurePage() {
        this(null);
    }

    public sfWGUStateLicensurePage(WebPage existingPage) {
        super(existingPage);
    }

    public sfWGUStateLicensurePage load() {
        super.load();
        clickWGUStateLicensure();
        return this;
    }

    public sfWGUStateLicensurePage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



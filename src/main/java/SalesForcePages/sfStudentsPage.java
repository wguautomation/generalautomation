
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentsPage extends sfHomePage {
    public sfStudentsPage() {
        this(null);
    }

    public sfStudentsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentsPage load() {
        super.load();
        clickStudents();
        return this;
    }

    public sfStudentsPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}

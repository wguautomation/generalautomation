
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfGroupsPage extends sfHomePage {
    public sfGroupsPage() {
        this(null);
    }

    public sfGroupsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfGroupsPage load() {
        super.load();
        clickGroups();
        return this;
    }

    public sfGroupsPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



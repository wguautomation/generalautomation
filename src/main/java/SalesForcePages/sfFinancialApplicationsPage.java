
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfFinancialApplicationsPage extends sfFilteredReportPage {
    public sfFinancialApplicationsPage() {
        this(null);
    }

    public sfFinancialApplicationsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureAssignButton(true);
        ConfigureRefreshButton(true);
    }

    public sfFinancialApplicationsPage load() {
        super.load();
        clickFinancialApplications();
        return this;
    }

    public sfFinancialApplicationsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



package SalesForcePages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/17/2015.
 * For Lane1Qa / Lane1 use only.
 */

public class sfHomePage extends WebPage {
    private static String serverPrefix = "https://srm--qafull.cs45.my.salesforce.com";
    
    public sfHomePage() {
        this(null);
    }

    public sfHomePage(WebPage existingPage) {
        super(existingPage, LoginType.SALESFORCE_QA);
    }

    public void clickGo() {
        getByXpath("//*[@id='filter_element']/div/span/span[1]/input").click();
        Sleep(1000);
    }

    public void waitForHeading() {
        getByXpath("//*[@id='contentWrapper']/div[4]/div/span");
    }

    public sfHomePage clickHomePage() {
        get(serverPrefix + "/home/home.jsp");
        return this;
    }

    public void clickUserMenu() {
        getByXpath("//*[@id='globalHeaderNameMink']/span/span/img[2]");
        getByXpath("//*[@id='globalHeaderNameMink']/span/span/img[2]").click();
    }

    public void clickMySettings() {
        clickUserMenu();
        getByLinkText("My Settings").click();
    }

    public void clickSetup() {
        clickUserMenu();
        getByLinkText("Setup").click();
    }

    public void clickEditContactInfo() {
        clickUserMenu();
        getByLinkText("Edit Contact Info").click();
        Sleep(2000);
        getByXpath("//*[@id='editContactInfoCancelButton']").click();
    }

    public void clickDeveloperConsole() {
        clickUserMenu();
        LoadAndSwitchTabs("Developer Console", BY_LINKTEXT);
        getByXpath("//*[@id='tab-1169-btnInnerEl']");
    }

    public void clickHelpAndTraining() {
        clickUserMenu();
        LoadAndSwitchTabs("Help & Training", BY_LINKTEXT);
    }

    public void clickLogout() {
        clickUserMenu();
        getByLinkText("Logout").click();
    }

    public void clickAccountsPage() {
        get(serverPrefix + "/001/o");
        waitForHeading();
    }

    public void clickActionItemPilotsPage() {
        get(serverPrefix + "/a7P/o");
        waitForHeading();
    }

    public void clickActionItemsPage() {
        get(serverPrefix + "/a7Q/o");
        waitForHeading();
    }

    public void clickAdmissionsPage() {
        get(serverPrefix + "/a0M/o");
        waitForHeading();
    }

    public void clickAdmissionsDecisionInformation() {
        get(serverPrefix + "/a2T/o");
        waitForHeading();
    }

    public void clickAlumniRelations() {
        get(serverPrefix + "/a2K/o");
        waitForHeading();
    }

    public void clickAnswers() {
        get(serverPrefix + "/answers/answersHome.apexp");
    }

    public void clickArticleManagement() {
        get(serverPrefix + "/knowledge/publishing/knowledgePublishingHome.apexp");
    }

    public void clickBackgroundChecks() {
        get(serverPrefix + "/a0R/o");
        waitForHeading();
    }

    public void clickBenefits() {
        get(serverPrefix + "/a05/o");
        waitForHeading();
    }

    public void clickBuyGridBuddy() {
        get(serverPrefix + "/servlet/servlet.Integration?lid=01ra0000001NvIl&ic=1");
        Sleep(4000);
    }

    public void clickCampaigns() {
        get(serverPrefix + "/701/o");
        waitForHeading();
    }

    public void clickCareerServices() {
        get(serverPrefix + "/a22/o");
        waitForHeading();
    }

    public void clickCAREProfiles() {
        get(serverPrefix + "/006/o");
        waitForHeading();
    }

    public void clickCAREPrograms() {
        get(serverPrefix + "/a0j/o");
        waitForHeading();
    }

    public void clickCAREProgramsMiddleEarth() {
        get(serverPrefix + "/a0k/o");
        waitForHeading();
    }

    public void clickCases() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickChatter() {
        get(serverPrefix + "/_ui/core/chatter/ui/ChatterPage");
    }

    public void clickCohorts() {
        get(serverPrefix + "/a0e/o");
        waitForHeading();
    }

    public void clickCommunityAccessControls() {
        get(serverPrefix + "/a6T/o");
        waitForHeading();
    }

    public void clickCommunityManagement() {
        get("https://srm--qafull--c.cs15.visual.force.com/apex/CourseCommunitiesModeration?sfdc.tabName=01ra0000001Nxqf");
    }

    public void clickContacts() {
        get(serverPrefix + "/003/o");
        waitForHeading();
    }

    public void clickContracts() {
        get(serverPrefix + "/800/o");
        waitForHeading();
    }

    public void clickCOS() {
        get(serverPrefix + "/servlet/servlet.Integration?lid=01ra0000001NsFb&ic=1");
    }

    public void clickCourseLists() {
        get(serverPrefix + "/a2P/o");
        waitForHeading();
    }

    public void clickCourseManagement() {
        get(serverPrefix + "/servlet/servlet.Integration?lid=01ra0000001Nt1D&ic=1");
     }

    public void clickCourseMentorStudentAssignments() {
        get(serverPrefix + "/a20/o");
        waitForHeading();
    }

    public void clickDashboards() {
        get(serverPrefix + "/01Za0000000ffHW");
    }

    public void clickDataDotCom() {
        get(serverPrefix + "/DataDotComSearch");
    }

    public void clickDocuments() {
        get(serverPrefix + "/015/o");
        waitForHeading();
    }

    public void clickDuplicateRecordSets() {
        get(serverPrefix + "/0GK/o");
        waitForHeading();
    }

    public void clickEmailAddresses() {
        get(serverPrefix + "/a0i/o");
        waitForHeading();
    }

    public void clickEnrollmentAnswers() {
        get(serverPrefix + "/a0l/o");
        waitForHeading();
    }

    public void clickEnrollmentPrelicensure() {
        get(serverPrefix + "/a1n/o");
        waitForHeading();
    }

    public void clickEnrollmentQuestions() {
        get(serverPrefix + "/a0m/o");
        waitForHeading();
    }

    public void clickEnrollmentResponses() {
        get(serverPrefix + "/a0n/o");
        waitForHeading();
    }

    public void clickFEPortalStudentTasksLists() {
        get(serverPrefix + "/a79/o");
        waitForHeading();
    }

    public void clickFieldExperienceProcesses() {
        get(serverPrefix + "/a0S/o");
        waitForHeading();
    }

    public void clickFieldExperiences() {
        get(serverPrefix + "/a0T/o");
        waitForHeading();
    }

    public void clickFiles() {
        get(serverPrefix + "/_ui/core/chatter/files/FileTabPage");
    }

    public void clickFinancialApplications() {
        get(serverPrefix + "/a0C/o");
        waitForHeading();
    }

    public void clickFinancialAwards() {
        get(serverPrefix + "/a0w/o");
        waitForHeading();
    }

    public void clickFinancialFunds() {
        get(serverPrefix + "/a14/o");
        waitForHeading();
    }

    public void clickGettingStarted() {
        get(serverPrefix + "/servlet/servlet.Integration?lid=01ra0000001NvIm&ic=1");
    }

    public void clickGridBuddyUserGuide() {
        get("https://srm--qafull--gblite.cs15.visual.force.com/apex/GridBuddyUserGuide?sfdc.tabName=01ra0000001NvIn");
     }

    public void clickGrids() {
        get(serverPrefix + "/servlet/servlet.Integration?lid=01ra0000001NvIk&ic=1");
    }

    public void clickGridWizard() {
        get("https://srm--qafull--gblite.cs15.visual.force.com/apex/GridWizard1?sfdc.tabName=01ra0000001NvIo");
    }

    public void clickGroups() {
        get(serverPrefix + "/_ui/core/chatter/groups/GroupListPage");
    }

    public void clickHome() {
        get(serverPrefix + "/home/home.jsp");
    }

    public void clickKeyValues() {
        get(serverPrefix + "/a06/o");
        waitForHeading();
    }

    public void clickKnowledge() {
        get(serverPrefix + "/_ui/knowledge/ui/KnowledgeHome");
    }

    public void clickLeads() {
        get(serverPrefix + "/00Q/o");
        waitForHeading();
    }

    public void clickLearningResourceAccess() {
        get(serverPrefix + "/a1y/o");
        waitForHeading();
    }

    public void clickMarketingSourceCodes() {
        get(serverPrefix + "/a1t/o");
        waitForHeading();
    }

    public void clickMentorChange() {
        get("https://srm--qafull--c.cs15.visual.force.com/apex/TransferStudentMentor?sfdc.tabName=01ra0000001NwSb");
     }

    public void clickMentorCourseAssignments() {
        get(serverPrefix + "/a1M/o");
        waitForHeading();
    }

    public void clickMentorCourseAssignsToBanner() {
        get(serverPrefix + "/a2M/o");
        waitForHeading();
    }

    public void clickMentorCourseGroups() {
        get(serverPrefix + "/a1N/o");
        waitForHeading();
    }

    public void clickMentorCourseGroupsToBanner() {
        get(serverPrefix + "/a2N/o");
        waitForHeading();
    }

    public void clickMentors() {
        get(serverPrefix + "/a07/o");
        waitForHeading();
    }

    public void clickMethodOfPayment() {
        get(serverPrefix + "/a0N/o");
        waitForHeading();
    }

    public void clickNewProspect() {
        get("https://srm--qafull--c.cs15.visual.force.com/apex/NewProspect?sfdc.tabName=01ra0000001NrEM");
    }

    public void clickOAuthServices() {
        get(serverPrefix + "/a77/o");
        waitForHeading();
    }

    public void clickOldPCEApp() {
        get("https://srm--qafull--c.cs15.visual.force.com/apex/OldWGUPCEFrame?sfdc.tabName=01r30000001NIVu");
    }

    public void clickOrientation() {
        get(serverPrefix + "/a7A/o");
        waitForHeading();
    }

    public void clickPeople() {
        get(serverPrefix + "/_ui/core/chatter/people/PeopleListPage");
    }

    public void clickPortfolios() {
        get(serverPrefix + "/a0d/o");
        waitForHeading();
    }

    public void clickPriceBooks() {
        get(serverPrefix + "/01s/o");
        waitForHeading();
    }

    public void clickProducts() {
        get(serverPrefix + "/01t/o");
        waitForHeading();
    }

    public void clickProfile() {
        get(serverPrefix + "/_ui/core/userprofile/UserProfilePage?tab=sfdc.ProfilePlatformFeed");
    }

    public void clickProfileFeed() {
        get(serverPrefix + "/_ui/core/userprofile/UserProfilePage?tab=sfdc.ProfilePlatformFeed");
    }

    public void clickProfileOverview() {
        get(serverPrefix + "/_ui/core/userprofile/UserProfilePage?tab=sfdc.ProfilePlatformOverview");
    }

    public void clickProgramFeedback() {
        get(serverPrefix + "/a0Y/o");
        waitForHeading();
    }

    public void clickProjects() {
        get(serverPrefix + "/a1l/o");
        waitForHeading();
    }

    public void clickPTaskActivation() {
        get(serverPrefix + "/servlet/servlet.Integration?lid=01ra0000001NvXI&ic=1");
    }

    public void clickPTaskDeactivation() {
        get(serverPrefix + "/servlet/servlet.Integration?lid=01ra0000001NvXN&ic=1");
    }

    public void clickQuickText() {
        get(serverPrefix + "/574/o");
        waitForHeading();
    }

    public void clickReadinessAssessments() {
        get(serverPrefix + "/a0p/o");
        waitForHeading();
    }

    public void clickReports() {
        get(serverPrefix + "/00O/o");
        waitForHeading();
    }

    public void clickRequirementRules() {
        get(serverPrefix + "/a15/o");
        waitForHeading();
    }

    public void clickRisks() {
        get(serverPrefix + "/a1m/o");
        waitForHeading();
    }

    public void clickRolloutConfigurations() {
        get(serverPrefix + "/a7S/o");
        waitForHeading();
    }

    public void clickRolloutParameters() {
        get(serverPrefix + "/a7T/o");
        waitForHeading();
    }

    public void clickScripts() {
        get(serverPrefix + "/a0b/o");
        waitForHeading();
    }

    public void clickServiceNow() {
        get(serverPrefix + "/servlet/servlet.Integration?lid=01ra0000001NzF7&ic=1");
    }

    public void clickSiteDotCom() {
        get(serverPrefix + "/udd/Site/siteDashboard.apexp");
    }

    public void clickSMSBusinessHours() {
        get(serverPrefix + "/a76/o");
        waitForHeading();
    }

    public void clickSMSMessageTemplates() {
        get(serverPrefix + "/a75/o");
        waitForHeading();
    }

    public void clickSMSPhoneNumbers() {
        get(serverPrefix + "/a74/o");
        waitForHeading();
    }

    public void clickSolutions() {
        get(serverPrefix + "/501/o");
        waitForHeading();
    }

    public void clickSRMChanges() {
        get(serverPrefix + "/a0x/o");
        waitForHeading();
    }

    public void clickStatusReports() {
        get(serverPrefix + "/a6f/o");
        waitForHeading();
    }

    public void clickStudentAcademicCourseAttempts() {
        get(serverPrefix + "/a6e/o");
        waitForHeading();
    }

    public void clickStudentAcademicCourseAttsFromBanner() {
        get(serverPrefix + "/a04/o");
        waitForHeading();
    }

    public void clickStudentAcademicCourseAttsReqsFromBanner() {
        get(serverPrefix + "/a1r/o");
        waitForHeading();
    }

    public void clickStudentAcademicCourseRequirements() {
        get(serverPrefix + "/a6o/o");
        waitForHeading();
    }

    public void clickStudentAcademicCourses() {
        get(serverPrefix + "/a6d/o");
        waitForHeading();
    }

    public void clickStudentAcademicCoursesFromBanner() {
        get(serverPrefix + "/a03/o");
        waitForHeading();
    }

    public void clickStudentCases() {
        get(serverPrefix + "/a1g/o");
        waitForHeading();
    }

    public void clickStudentCompletionCourses() {
        get(serverPrefix + "/a1S/o");
        waitForHeading();
    }

    public void clickStudentCompletionTasks() {
        get(serverPrefix + "/a09/o");
        waitForHeading();
    }

    public void clickStudentConcerns() {
        get(serverPrefix + "/a0U/o");
        waitForHeading();
    }

    public void clickStudentCourseRegistrations() {
        get(serverPrefix + "/a0h/o");
        waitForHeading();
    }

    public void clickStudentCourses() {
        get(serverPrefix + "/a0A/o");
        waitForHeading();
    }

    public void clickStudentCourseVersions() {
        get(serverPrefix + "/a1T/o");
        waitForHeading();
    }

    public void clickStudentDegreePlans() {
        get(serverPrefix + "/a0B/o");
        waitForHeading();
    }

    public void clickStudentEquivalentCourses() {
        get(serverPrefix + "/a0q/o");
        waitForHeading();
    }

    public void clickStudentProcesses() {
        get(serverPrefix + "/a0V/o");
        waitForHeading();
    }

    public void clickStudents() {
        get("https://srm--qafull--c.cs15.visual.force.com/apex/MyStudents?sfdc.tabName=01ra0000001Nu4U");
    }

//
// Below this line are placeholders only, not mapped yet
//
    public void clickStudentsAttributesToBanner() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickStudentTaskAttempts() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickStudentTerms() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickStudentTransferInstitution() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickStudentTransferredCourses() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickSynchronizationsToBanner() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickSynchronizeToBannerLogs() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickTasks() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickTaskstreamAssessments() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickTaskStreamTaskFailures() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickTaskstreamTasks() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickTBRequest() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickTeacherLicenses() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickTeamMembers() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickTerm() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickTimelineEvents() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickTranscriptsToBanner() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickTransferAttendance() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickTransferEvaluations() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickUnhandledExceptions() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickUserAttributes() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickWGUConfigurationConstants() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickWGUCourseCompletionTask() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickWGUCourseInPrograms() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickWGUCourses() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickWGUCourseVersions() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickWGUDegreePrograms() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickWGUEulas() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickWGUFEToDo() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickWGUIdeas() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickWGUIdeasHelp() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickWGULicenseRequirements() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickWGUStateLicensure() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickWithdrawalsToBanner() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public void clickYears() {
        get(serverPrefix + "/500/o");
        waitForHeading();
    }

    public sfHomePage load() {
        login();
        Sleep(3000);
        clickHomePage();
        return this;
    }

    public sfHomePage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            new sfDeveloperConsolePage(this).ExercisePage(true);

            new sfAccountsPage(this).ExercisePage(true);
            new sfActionItemPilotsPage(this).ExercisePage(true);
            new sfActionItemsPage(this).ExercisePage(true);
            new sfAdmissionsDecisionInformationPage(this).ExercisePage(true);
            new sfAdmissionsPage(this).ExercisePage(true);
            new sfAlumniRelationsPage(this).ExercisePage(true);
            new sfAnswersPage(this).ExercisePage(true);
            new sfArticleManagementPage(this).ExercisePage(true);
            new sfBackgroundChecksPage(this).ExercisePage(true);
            new sfBenefitsPage(this).ExercisePage(true);
            new sfBuyGridBuddyPage(this).ExercisePage(true);
            new sfCampaignsPage(this).ExercisePage(true);
            new sfCareerServicesPage(this).ExercisePage(true);
            new sfCAREProfilesPage(this).ExercisePage(true);
            new sfCAREProgramsMiddleEarthPage(this).ExercisePage(true);
            new sfCAREProgramsPage(this).ExercisePage(true);
            new sfCasesPage(this).ExercisePage(true);
            new sfChatterPage(this).ExercisePage(true);
            new sfCohortsPage(this).ExercisePage(true);
            new sfCommunityAccessControlsPage(this).ExercisePage(true);
            new sfCommunityManagementPage(this).ExercisePage(true);
            new sfContactsPage(this).ExercisePage(true);
            new sfContractsPage(this).ExercisePage(true);
            new sfCOSPage(this).ExercisePage(true);
            new sfCourseListsPage(this).ExercisePage(true);
            new sfCourseManagementPage(this).ExercisePage(true);
            new sfCourseMentorStudentAssignmentsPage(this).ExercisePage(true);
            new sfDashboardsPage(this).ExercisePage(true);
            new sfDataDotComPage(this).ExercisePage(true);
            new sfDocumentsPage(this).ExercisePage(true);
            new sfDuplicateRecordSetsPage(this).ExercisePage(true);
            new sfEnrollmentAnswersPage(this).ExercisePage(true);
            new sfEmailAddressesPage(this).ExercisePage(true);
            new sfEnrollmentPrelicensurePage(this).ExercisePage(true);
            new sfEnrollmentQuestionsPage(this).ExercisePage(true);
            new sfEnrollmentResponsesPage(this).ExercisePage(true);
            new sfFEPortalStudentTasksListsPage(this).ExercisePage(true);
            new sfFieldExperienceProcessesPage(this).ExercisePage(true);
            new sfFieldExperiencesPage(this).ExercisePage(true);
            new sfFilesPage(this).ExercisePage(true);
            new sfFinancialApplicationsPage(this).ExercisePage(true);
            new sfFinancialAwardsPage(this).ExercisePage(true);
            new sfFinancialFundsPage(this).ExercisePage(true);
            new sfGettingStartedPage(this).ExercisePage(true);
            new sfGridBuddyUserGuidePage(this).ExercisePage(true);
            new sfGridsPage(this).ExercisePage(true);
            new sfGridWizardPage(this).ExercisePage(true);
            new sfGroupsPage(this).ExercisePage(true);
            new sfKeyValuesPage(this).ExercisePage(true);
            new sfKnowledgePage(this).ExercisePage(true);
            new sfLeadsPage(this).ExercisePage(true);
            new sfLearningResourceAccessPage(this).ExercisePage(true);
            new sfMarketingSourceCodesPage(this).ExercisePage(true);
            new sfMentorChangePage(this).ExercisePage(true);
//            new sfMentorCourseAssignmentsPage(this).ExercisePage(true);
//            new sfMentorCourseAssignsToBanner(this).ExercisePage(true);
//            new sfMentorCourseGroups(this).ExercisePage(true);
//            new sfMentorCourseGroupsToBannerPage(this).ExercisePage(true);
//            new sfMentorsPage(this).ExercisePage(true);
            new sfMethodOfPaymentPage(this).ExercisePage(true);
            new sfNewProspectPage(this).ExercisePage(true);
            new sfOAuthServicesPage(this).ExercisePage(true);
            new sfOldPCEAppPage(this).ExercisePage(true);
            new sfOrientationPage(this).ExercisePage(true);
            new sfPeoplePage(this).ExercisePage(true);
            new sfPortfoliosPage(this).ExercisePage(true);
            new sfPriceBooksPage(this).ExercisePage(true);
            new sfProductsPage(this).ExercisePage(true);
            new sfProfileFeedPage(this).ExercisePage(true);
            new sfProfileOverviewPage(this).ExercisePage(true);
            new sfProfilePage(this).ExercisePage(true);
            new sfProgramFeedbackPage(this).ExercisePage(true);
            new sfProjectsPage(this).ExercisePage(true);
            new sfPTaskActivationPage(this).ExercisePage(true);
            new sfPTaskDeactivationPage(this).ExercisePage(true);
            new sfQuickTextPage(this).ExercisePage(true);
            new sfReadinessAssessmentsPage(this).ExercisePage(true);
            new sfReportsPage(this).ExercisePage(true);
            new sfRequirementRulesPage(this).ExercisePage(true);
            new sfRisksPage(this).ExercisePage(true);
            new sfRolloutConfigurationsPage(this).ExercisePage(true);
            new sfRolloutParametersPage(this).ExercisePage(true);
            new sfScriptsPage(this).ExercisePage(true);
            new sfServiceNowPage(this).ExercisePage(true);
            new sfSiteDotComPage(this).ExercisePage(true);
            new sfSMSBusinessHoursPage(this).ExercisePage(true);
            new sfSMSMessageTemplatesPage(this).ExercisePage(true);
            new sfSMSPhoneNumbersPage(this).ExercisePage(true);
            new sfSolutionsPage(this).ExercisePage(true);
            new sfSRMChangesPage(this).ExercisePage(true);
            new sfStatusReportsPage(this).ExercisePage(true);
            new sfStudentAcademicCourseAttemptsPage(this).ExercisePage(true);
            new sfStudentAcademicCourseAttsFromBannerPage(this).ExercisePage(true);
            new sfStudentAcademicCourseAttsReqsFromBannerPage(this).ExercisePage(true);
            new sfStudentAcademicCourseRequirementsPage(this).ExercisePage(true);
            new sfStudentAcademicCoursesFromBannerPage(this).ExercisePage(true);
            new sfStudentAcademicCoursesPage(this).ExercisePage(true);
            new sfStudentCasesPage(this).ExercisePage(true);
            new sfStudentCompletionCoursesPage(this).ExercisePage(true);
            new sfStudentCompletionTasksPage(this).ExercisePage(true);
            new sfStudentConcernsPage(this).ExercisePage(true);
            new sfStudentCourseRegistrationsPage(this).ExercisePage(true);
            new sfStudentCoursesPage(this).ExercisePage(true);
            new sfStudentCourseVersionPage(this).ExercisePage(true);
            new sfStudentDegreePlansPage(this).ExercisePage(true);
            new sfStudentEquivalentCoursesPage(this).ExercisePage(true);
            new sfStudentProcessesPage(this).ExercisePage(true);
            new sfStudentsAttributesToBannerPage(this).ExercisePage(true);
//            new sfStudentsPage(this).ExercisePage(true);
            new sfStudentTaskAttemptsPage(this).ExercisePage(true);
            new sfStudentTermsPage(this).ExercisePage(true);
            new sfStudentTransferInstitutionPage(this).ExercisePage(true);
            new sfStudentTransferredCoursesPage(this).ExercisePage(true);
            new sfSynchronizationsToBannerPage(this).ExercisePage(true);
            new sfSynchronizeToBannerLogsPage(this).ExercisePage(true);
            new sfTasksPage(this).ExercisePage(true);
            new sfTaskstreamAssessmentsPage(this).ExercisePage(true);
            new sfTaskStreamTaskFailuresPage(this).ExercisePage(true);
            new sfTaskstreamTasksPage(this).ExercisePage(true);
            new sfTBRequestPage(this).ExercisePage(true);
            new sfTeacherLicensesPage(this).ExercisePage(true);
            new sfTeamMembersPage(this).ExercisePage(true);
            new sfTermPage(this).ExercisePage(true);
            new sfTimelineEventsPage(this).ExercisePage(true);
            new sfTranscrtipsToBannerPage(this).ExercisePage(true);
            new sfTransferAttendancePage(this).ExercisePage(true);
            new sfTransferEvaluationsPage(this).ExercisePage(true);
            new sfUnhandledExceptionsPage(this).ExercisePage(true);
            new sfUserAttributesPage(this).ExercisePage(true);
            new sfWGUConfigurationConstantsPage(this).ExercisePage(true);
            new sfWGUCourseCompletionTaskPage(this).ExercisePage(true);
            new sfWGUCourseInProgramsPage(this).ExercisePage(true);
            new sfWGUCoursesPage(this).ExercisePage(true);
            new sfWGUCourseVersionsPage(this).ExercisePage(true);
            new sfWGUDegreeProgramsPage(this).ExercisePage(true);
            new sfWGUEulasPage(this).ExercisePage(true);
            new sfWGUFEToDoPage(this).ExercisePage(true);
            new sfWGUIdeasHelpPage(this).ExercisePage(true);
            new sfWGUIdeasPage(this).ExercisePage(true);
            new sfWGULicenseRequirementsPage(this).ExercisePage(true);
            new sfWGUStateLicensurePage(this).ExercisePage(true);
            new sfWithdrawalsToBannerPage(this).ExercisePage(true);
            new sfYearsPage(this).ExercisePage(true);

            return this;
        }

        clickAccountsPage();
        clickActionItemPilotsPage();
        clickActionItemsPage();
        clickAdmissionsPage();
        clickAdmissionsDecisionInformation();
        clickAlumniRelations();
        clickAnswers();
        clickArticleManagement();
        clickBackgroundChecks();
        clickBenefits();
        clickBuyGridBuddy();
        clickCampaigns();
        clickCareerServices();
        clickCAREProfiles();
        clickCAREPrograms();
        clickCAREProgramsMiddleEarth();
        clickCases();
        clickChatter();
        clickCohorts();
        clickCommunityAccessControls();
        clickCommunityManagement();
        clickContacts();
        clickContracts();
        clickCOS();
        clickCourseLists();
        clickCourseManagement();
        clickCourseMentorStudentAssignments();
        clickDashboards();
        clickDataDotCom();
        clickDocuments();
        clickDuplicateRecordSets();
        clickEmailAddresses();
        clickEnrollmentAnswers();
        clickEnrollmentPrelicensure();
        clickEnrollmentQuestions();
        clickEnrollmentResponses();
        clickFEPortalStudentTasksLists();
        clickFieldExperienceProcesses();
        clickFieldExperiences();
        clickFiles();
        clickFinancialApplications();
        clickFinancialAwards();
        clickFinancialFunds();
        clickGettingStarted();
        clickGridBuddyUserGuide();
        clickGrids();
        clickGridWizard();
        clickGroups();
        clickHome();
        clickKeyValues();
        clickKnowledge();
        clickLeads();
        clickLearningResourceAccess();
        clickMarketingSourceCodes();
        clickMentorChange();
//        clickMentorCourseAssignments();
//        clickMentorCourseAssignsToBanner();
//       clickMentorCourseGroups();
//        clickMentorCourseGroupsToBanner();
//        clickMentors();
        clickMethodOfPayment();
        clickNewProspect();
        clickOAuthServices();
        clickOldPCEApp();
        clickOrientation();
        clickPeople();
        clickPortfolios();
        clickPriceBooks();
        clickProducts();
        clickProfile();
        clickProfileFeed();
        clickProfileOverview();
        clickProgramFeedback();
        clickProjects();
        clickPTaskActivation();
        clickPTaskDeactivation();
        clickQuickText();
        clickReadinessAssessments();
        clickReports();
        clickRequirementRules();
        clickRisks();
        clickRolloutConfigurations();
        clickRolloutParameters();
        clickScripts();
        clickServiceNow();
        clickSiteDotCom();
        clickSMSBusinessHours();
        clickSMSMessageTemplates();
        clickSMSPhoneNumbers();
        clickSolutions();
        clickSRMChanges();
        clickStatusReports();
        clickStudentAcademicCourseAttempts();
        clickStudentAcademicCourseAttsFromBanner();
        clickStudentAcademicCourseAttsReqsFromBanner();
        clickStudentAcademicCourseRequirements();
        clickStudentAcademicCourses();
        clickStudentAcademicCoursesFromBanner();
        clickStudentCases();
        clickStudentCompletionCourses();
        clickStudentCompletionTasks();
        clickStudentConcerns();
        clickStudentCourseRegistrations();
        clickStudentCourses();
        clickStudentCourseVersions();
        clickStudentDegreePlans();
        clickStudentEquivalentCourses();
        clickStudentProcesses();
//        clickStudents();

/* these havent been mapped yet
        clickStudentsAttributesToBanner();
        clickStudentTaskAttempts();
        clickStudentTerms();
        clickStudentTransferInstitution();
        clickStudentTransferredCourses();
        clickSynchronizationsToBanner();
        clickSynchronizeToBannerLogs();
        clickTasks();
        clickTaskstreamAssessments();
        clickTaskStreamTaskFailures();
        clickTaskstreamTasks();
        clickTBRequest();
        clickTeacherLicenses();
        clickTeamMembers();
        clickTerm();
        clickTimelineEvents();
        clickTranscriptsToBanner();
        clickTransferAttendance();
        clickTransferEvaluations();
        clickUnhandledExceptions();
        clickUserAttributes();
        clickWGUConfigurationConstants();
        clickWGUCourseCompletionTask();
        clickWGUCourseInPrograms();
        clickWGUCourses();
        clickWGUCourseVersions();
        clickWGUDegreePrograms();
        clickWGUEulas();
        clickWGUFEToDo();
        clickWGUIdeas();
        clickWGUIdeasHelp();
        clickWGULicenseRequirements();
        clickWGUStateLicensure();
        clickWithdrawalsToBanner();
        clickYears();
*/
        return this;
   }

}















package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/18/2015.
 */
public class sfAdmissionsDecisionInformationPage extends sfFilteredReportPage {
    public sfAdmissionsDecisionInformationPage() {
        this(null);
    }

    public sfAdmissionsDecisionInformationPage(WebPage existingPage) {
        super(existingPage);
        ConfigureNewButton(true);
        ConfigureGoButton(true);
    }

    public sfAdmissionsDecisionInformationPage load() {
        super.load();
        clickAdmissionsDecisionInformation();
        return this;
    }

    public sfAdmissionsDecisionInformationPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}


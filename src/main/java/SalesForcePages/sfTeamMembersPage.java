
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfTeamMembersPage extends sfHomePage {
    public sfTeamMembersPage() {
        this(null);
    }

    public sfTeamMembersPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfTeamMembersPage load() {
        super.load();
        clickTeamMembers();
        return this;
    }

    public sfTeamMembersPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


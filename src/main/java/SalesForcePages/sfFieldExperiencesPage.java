
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfFieldExperiencesPage extends sfFilteredReportPage {
    public sfFieldExperiencesPage() {
        this(null);
    }

    public sfFieldExperiencesPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureAssignButton(true);
        ConfigureRefreshButton(true);
    }

    public sfFieldExperiencesPage load() {
        super.load();
        clickFieldExperiences();
        return this;
    }

    public sfFieldExperiencesPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}


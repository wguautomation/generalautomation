
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/21/2015.
 */
public class sfStudentAcademicCourseAttemptsPage extends sfHomePage {
    public sfStudentAcademicCourseAttemptsPage() {
        this(null);
    }

    public sfStudentAcademicCourseAttemptsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentAcademicCourseAttemptsPage load() {
        super.load();
        clickStudentAcademicCourseAttempts();
        return this;
    }

    public sfStudentAcademicCourseAttemptsPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfCourseListsPage extends sfFilteredReportPage {
    public sfCourseListsPage() {
        this(null);
    }

    public sfCourseListsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureRefreshButton(true);
    }

    public sfCourseListsPage load() {
        super.load();
        clickCourseLists();
        return this;
    }

    public sfCourseListsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}




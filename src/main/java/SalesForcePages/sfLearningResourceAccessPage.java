
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfLearningResourceAccessPage extends sfFilteredReportPage {
    public sfLearningResourceAccessPage() {
        this(null);
    }

    public sfLearningResourceAccessPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureRefreshButton(true);
    }

    public sfLearningResourceAccessPage load() {
        super.load();
        clickLearningResourceAccess();
        return this;
    }

    public sfLearningResourceAccessPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



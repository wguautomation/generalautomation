
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfUnhandledExceptionsPage extends sfHomePage {
    public sfUnhandledExceptionsPage() {
        this(null);
    }

    public sfUnhandledExceptionsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfUnhandledExceptionsPage load() {
        super.load();
        clickUnhandledExceptions();
        return this;
    }

    public sfUnhandledExceptionsPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


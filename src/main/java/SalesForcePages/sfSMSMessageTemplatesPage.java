
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfSMSMessageTemplatesPage extends sfHomePage {
    public sfSMSMessageTemplatesPage() {
        this(null);
    }

    public sfSMSMessageTemplatesPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfSMSMessageTemplatesPage load() {
        super.load();
        clickSMSMessageTemplates();
        return this;
    }

    public sfSMSMessageTemplatesPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



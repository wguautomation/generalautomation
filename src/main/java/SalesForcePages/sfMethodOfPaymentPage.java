
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfMethodOfPaymentPage extends sfFilteredReportPage {
    public sfMethodOfPaymentPage() {
        this(null);
    }

    public sfMethodOfPaymentPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureAssignButton(true);
        ConfigureRefreshButton(true);
    }

    public sfMethodOfPaymentPage load() {
        super.load();
        clickMethodOfPayment();
        return this;
    }

    public sfMethodOfPaymentPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/18/2015.
 */
public class sfActionItemsPage extends sfFilteredReportPage {
    public sfActionItemsPage() {
        this(null);
    }

    public sfActionItemsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureNewButton(true);
        ConfigureGoButton(true);
    }

    public sfActionItemsPage load() {
        super.load();
        clickActionItemsPage();
        return this;
    }

    public sfActionItemsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}





package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfMentorChangePage extends sfHomePage {
    public sfMentorChangePage() {
        this(null);
    }

    public sfMentorChangePage(WebPage existingPage) {
        super(existingPage);
    }

    public sfMentorChangePage load() {
        super.load();
        clickMentorChange();
        return this;
    }

    public sfMentorChangePage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



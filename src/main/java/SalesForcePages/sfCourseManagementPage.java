
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfCourseManagementPage extends sfHomePage {
    public sfCourseManagementPage() {
        this(null);
    }

    public sfCourseManagementPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfCourseManagementPage load() {
        super.load();
        clickCourseManagement();
        return this;
    }

    public sfCourseManagementPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
       }

        return this;
    }

}



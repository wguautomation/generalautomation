
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfPTaskDeactivationPage extends sfHomePage {
    public sfPTaskDeactivationPage() {
        this(null);
    }

    public sfPTaskDeactivationPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfPTaskDeactivationPage load() {
        super.load();
        clickPTaskDeactivation();
        return this;
    }

    public sfPTaskDeactivationPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



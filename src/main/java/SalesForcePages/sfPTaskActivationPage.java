
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfPTaskActivationPage extends sfHomePage {
    public sfPTaskActivationPage() {
        this(null);
    }

    public sfPTaskActivationPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfPTaskActivationPage load() {
        super.load();
        clickPTaskActivation();
        return this;
    }

    public sfPTaskActivationPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



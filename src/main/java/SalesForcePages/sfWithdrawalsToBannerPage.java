
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfWithdrawalsToBannerPage extends sfHomePage {
    public sfWithdrawalsToBannerPage() {
        this(null);
    }

    public sfWithdrawalsToBannerPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfWithdrawalsToBannerPage load() {
        super.load();
        clickWithdrawalsToBanner();
        return this;
    }

    public sfWithdrawalsToBannerPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfNewProspectPage extends sfHomePage {
    public sfNewProspectPage() {
        this(null);
    }

    public sfNewProspectPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfNewProspectPage load() {
        super.load();
        clickNewProspect();
        return this;
    }

    public sfNewProspectPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


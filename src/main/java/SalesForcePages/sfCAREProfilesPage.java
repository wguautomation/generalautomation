
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/19/2015.
 */
public class sfCAREProfilesPage extends sfFilteredReportPage {
    public sfCAREProfilesPage() {
        this(null);
    }

    public sfCAREProfilesPage(WebPage existingPage) {
        super(existingPage);
        ConfigureNewButton(true);
        ConfigureGoButton(true);
    }

    public sfCAREProfilesPage clickCAREProfilePipeline() {
        getByLinkText("CARE Profile Pipeline").click();
        waitForHeading();
        return this;
    }

    public sfCAREProfilesPage clickStuckCAREProfiles() {
        getByLinkText("Stuck CARE Profiles").click();
        waitForHeading();
        return this;
    }

    public sfCAREProfilesPage clickClosedCAREProfiles() {
        getByLinkText("Closed CARE Profiles").click();
        waitForHeading();
        return this;
    }

    public sfCAREProfilesPage clickCAREProfileFieldHistoryReport() {
        getByLinkText("CARE Profile Field History Report").click();
        waitForHeading();
        return this;
    }

    public sfCAREProfilesPage clickSalesMethodologies() {
        LoadAndSwitchTabs("Sales Methodologies", BY_LINKTEXT);
        return this;
    }

    public sfCAREProfilesPage RunQuarterlySummary(String interval, String include) {
        selectByXpathByText("//*[@id=\"quarter_q\"]", interval);
        selectByXpathByText("//*[@id=\"open\"]", include);
        getByXpath("//*[@id=\"lead_summary\"]/table/tbody/tr[3]/td/input").click();
        return this;
    }

    public sfCAREProfilesPage load() {
        super.load();
        clickCAREProfiles();
        return this;
    }

    public sfCAREProfilesPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        clickCAREProfilePipeline();
        clickBrowserBackButton();
        clickStuckCAREProfiles();
        clickBrowserBackButton();
        clickClosedCAREProfiles();
        clickBrowserBackButton();
        clickCAREProfileFieldHistoryReport();
        clickBrowserBackButton();
        clickSalesMethodologies();
        clickBrowserBackButton();

        RunQuarterlySummary("Current FQ", "All CARE Profiles");
        clickBrowserBackButton();
        return this;
    }

}


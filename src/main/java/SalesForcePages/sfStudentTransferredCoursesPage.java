
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentTransferredCoursesPage extends sfHomePage {
    public sfStudentTransferredCoursesPage() {
        this(null);
    }

    public sfStudentTransferredCoursesPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentTransferredCoursesPage load() {
        super.load();
        clickStudentTransferredCourses();
        return this;
    }

    public sfStudentTransferredCoursesPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
       }

        return this;
    }

}


package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfWGUIdeasHelpPage extends sfHomePage {
    public sfWGUIdeasHelpPage() {
        this(null);
    }

    public sfWGUIdeasHelpPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfWGUIdeasHelpPage load() {
        super.load();
        clickWGUIdeasHelp();
        return this;
    }

    public sfWGUIdeasHelpPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


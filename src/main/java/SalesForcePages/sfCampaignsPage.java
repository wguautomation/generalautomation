
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfCampaignsPage extends sfFilteredReportPage {
    public sfCampaignsPage() {
        this(null);
    }

    public sfCampaignsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureNewButton(true);
        ConfigureGoButton(true);
    }

    public sfCampaignsPage load() {
        super.load();
        clickCampaigns();
        return this;
    }

    public sfCampaignsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}


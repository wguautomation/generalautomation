
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfKnowledgePage extends sfHomePage {
    public sfKnowledgePage() {
        this(null);
    }

    public sfKnowledgePage(WebPage existingPage) {
        super(existingPage);
    }

    public sfKnowledgePage load() {
        super.load();
        clickKnowledge();
        return this;
    }

    public sfKnowledgePage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



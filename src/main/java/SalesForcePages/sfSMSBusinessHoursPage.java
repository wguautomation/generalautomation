
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfSMSBusinessHoursPage extends sfHomePage {
    public sfSMSBusinessHoursPage() {
        this(null);
    }

    public sfSMSBusinessHoursPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfSMSBusinessHoursPage load() {
        super.load();
        clickSMSBusinessHours();
        return this;
    }

    public sfSMSBusinessHoursPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}




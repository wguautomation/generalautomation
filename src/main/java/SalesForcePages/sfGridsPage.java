
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfGridsPage extends sfHomePage {
    public sfGridsPage() {
        this(null);
    }

    public sfGridsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfGridsPage load() {
        super.load();
        clickGrids();
        return this;
    }

    public sfGridsPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



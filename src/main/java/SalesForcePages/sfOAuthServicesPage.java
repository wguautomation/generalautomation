
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfOAuthServicesPage extends sfFilteredReportPage {
    public sfOAuthServicesPage() {
        this(null);
    }

    public sfOAuthServicesPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureAssignButton(true);
        ConfigureRefreshButton(true);
    }

    public sfOAuthServicesPage load() {
        super.load();
        clickOAuthServices();
        return this;
    }

    public sfOAuthServicesPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfMentorCourseAssignmentsPage extends sfFilteredReportPage {
    public sfMentorCourseAssignmentsPage() {
        this(null);
    }

    public sfMentorCourseAssignmentsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureAssignButton(true);
        ConfigureRefreshButton(true);
    }

    public sfMentorCourseAssignmentsPage load() {
        super.load();
        clickMentorCourseAssignments();
        return this;
    }

    public sfMentorCourseAssignmentsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
       }

        return this;
    }

}



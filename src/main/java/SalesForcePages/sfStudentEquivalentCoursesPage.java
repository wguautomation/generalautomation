
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentEquivalentCoursesPage extends sfHomePage {
    public sfStudentEquivalentCoursesPage() {
        this(null);
    }

    public sfStudentEquivalentCoursesPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentEquivalentCoursesPage load() {
        super.load();
        clickStudentEquivalentCourses();
        return this;
    }

    public sfStudentEquivalentCoursesPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}

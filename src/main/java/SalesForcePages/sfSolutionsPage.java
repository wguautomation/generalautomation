
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfSolutionsPage extends sfHomePage {
    public sfSolutionsPage() {
        this(null);
    }

    public sfSolutionsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfSolutionsPage load() {
        super.load();
        clickSolutions();
        return this;
    }

    public sfSolutionsPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



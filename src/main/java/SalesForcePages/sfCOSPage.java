
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfCOSPage extends sfHomePage {
    public sfCOSPage() {
        this(null);
    }

    public sfCOSPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfCOSPage load() {
        super.load();
        clickCOS();
        return this;
    }

    public sfCOSPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


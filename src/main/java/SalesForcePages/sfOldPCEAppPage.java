
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfOldPCEAppPage extends sfFilteredReportPage {
    public sfOldPCEAppPage() {
        this(null);
    }

    public sfOldPCEAppPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfOldPCEAppPage load() {
        super.load();
        clickOldPCEApp();
        return this;
    }

    public sfOldPCEAppPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



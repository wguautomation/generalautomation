
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfMarketingSourceCodesPage extends sfFilteredReportPage {
    public sfMarketingSourceCodesPage() {
        this(null);
    }

    public sfMarketingSourceCodesPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureAssignButton(true);
        ConfigureRefreshButton(true);
    }

    public sfMarketingSourceCodesPage load() {
        super.load();
        clickMarketingSourceCodes();
        return this;
    }

    public sfMarketingSourceCodesPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



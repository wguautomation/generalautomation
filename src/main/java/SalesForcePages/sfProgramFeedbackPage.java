
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfProgramFeedbackPage extends sfFilteredReportPage {
    public sfProgramFeedbackPage() {
        this(null);
    }

    public sfProgramFeedbackPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureAssignButton(true);
        ConfigureRefreshButton(true);
    }

    public sfProgramFeedbackPage load() {
        super.load();
        clickProgramFeedback();
        return this;
    }

    public sfProgramFeedbackPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }
}



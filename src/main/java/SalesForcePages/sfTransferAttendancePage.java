
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfTransferAttendancePage extends sfHomePage {
    public sfTransferAttendancePage() {
        this(null);
    }

    public sfTransferAttendancePage(WebPage existingPage) {
        super(existingPage);
    }

    public sfTransferAttendancePage load() {
        super.load();
        clickTransferAttendance();
        return this;
    }

    public sfTransferAttendancePage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


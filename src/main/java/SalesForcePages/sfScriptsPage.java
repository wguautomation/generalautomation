
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfScriptsPage extends sfHomePage {
    public sfScriptsPage() {
        this(null);
    }

    public sfScriptsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfScriptsPage load() {
        super.load();
        clickScripts();
        return this;
    }

    public sfScriptsPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



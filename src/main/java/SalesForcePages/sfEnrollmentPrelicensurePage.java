
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfEnrollmentPrelicensurePage extends sfFilteredReportPage {
    public sfEnrollmentPrelicensurePage() {
        this(null);
    }

    public sfEnrollmentPrelicensurePage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureRefreshButton(true);
    }

    public sfEnrollmentPrelicensurePage load() {
        super.load();
        clickEnrollmentPrelicensure();
        return this;
    }

    public sfEnrollmentPrelicensurePage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}


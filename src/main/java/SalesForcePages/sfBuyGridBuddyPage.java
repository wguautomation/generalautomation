
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfBuyGridBuddyPage extends sfHomePage {
    public sfBuyGridBuddyPage() {
        this(null);
    }

    public sfBuyGridBuddyPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfBuyGridBuddyPage load() {
        super.load();
        clickBuyGridBuddy();
        return this;
    }

    public sfBuyGridBuddyPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


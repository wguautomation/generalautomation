
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfWGUConfigurationConstantsPage extends sfHomePage {
    public sfWGUConfigurationConstantsPage() {
        this(null);
    }

    public sfWGUConfigurationConstantsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfWGUConfigurationConstantsPage load() {
        super.load();
        clickWGUConfigurationConstants();
        return this;
    }

    public sfWGUConfigurationConstantsPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfFinancialAwardsPage extends sfFilteredReportPage {
    public sfFinancialAwardsPage() {
        this(null);
    }

    public sfFinancialAwardsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureAssignButton(true);
        ConfigureRefreshButton(true);
    }

    public sfFinancialAwardsPage load() {
        super.load();
        clickFinancialAwards();
        return this;
    }

    public sfFinancialAwardsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



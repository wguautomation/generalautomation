
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfEnrollmentResponsesPage extends sfFilteredReportPage {
    public sfEnrollmentResponsesPage() {
        this(null);
    }

    public sfEnrollmentResponsesPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureRefreshButton(true);
    }

    public sfEnrollmentResponsesPage load() {
        super.load();
        clickEnrollmentResponses();
        return this;
    }

    public sfEnrollmentResponsesPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfMentorCourseAssignsToBanner extends sfFilteredReportPage {
    public sfMentorCourseAssignsToBanner() {
        this(null);
    }

    public sfMentorCourseAssignsToBanner(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureAssignButton(true);
        ConfigureRefreshButton(true);
    }

    public sfMentorCourseAssignsToBanner load() {
        super.load();
        clickMentorCourseAssignsToBanner();
        return this;
    }

    public sfMentorCourseAssignsToBanner ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



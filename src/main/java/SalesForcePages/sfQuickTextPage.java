
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfQuickTextPage extends sfFilteredReportPage {
    public sfQuickTextPage() {
        this(null);
    }

    public sfQuickTextPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureRefreshButton(true);
    }

    public sfQuickTextPage load() {
        super.load();
        clickQuickText();
        return this;
    }

    public sfQuickTextPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



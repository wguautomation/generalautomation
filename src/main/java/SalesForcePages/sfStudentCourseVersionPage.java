
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentCourseVersionPage extends sfHomePage {
    public sfStudentCourseVersionPage() {
        this(null);
    }

    public sfStudentCourseVersionPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentCourseVersionPage load() {
        super.load();
        clickStudentCourseVersions();
        return this;
    }

    public sfStudentCourseVersionPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}

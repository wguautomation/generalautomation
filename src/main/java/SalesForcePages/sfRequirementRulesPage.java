
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfRequirementRulesPage extends sfHomePage {
    public sfRequirementRulesPage() {
        this(null);
    }

    public sfRequirementRulesPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfRequirementRulesPage load() {
        super.load();
        clickRequirementRules();
        return this;
    }

    public sfRequirementRulesPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



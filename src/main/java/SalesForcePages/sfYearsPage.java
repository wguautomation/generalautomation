
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfYearsPage extends sfHomePage {
    public sfYearsPage() {
        this(null);
    }

    public sfYearsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfYearsPage load() {
        super.load();
        clickYears();
        return this;
    }

    public sfYearsPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


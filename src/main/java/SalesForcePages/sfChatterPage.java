
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/21/2015.
 */
public class sfChatterPage extends sfHomePage {
    public sfChatterPage() {
        this(null);
    }

    public sfChatterPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfChatterPage load() {
        super.load();
        clickChatter();
        return this;
    }

    public sfChatterPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


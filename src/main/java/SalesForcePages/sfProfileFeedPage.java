
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfProfileFeedPage extends sfHomePage {
    public sfProfileFeedPage() {
        this(null);
    }

    public sfProfileFeedPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfProfileFeedPage load() {
        super.load();
        clickProfileFeed();
        return this;
    }

    public sfProfileFeedPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



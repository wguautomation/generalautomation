
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */

public class sfArticleManagementPage extends sfHomePage {
    public sfArticleManagementPage() {
        this(null);
    }

    public sfArticleManagementPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfArticleManagementPage load() {
        super.load();
        clickArticleManagement();
        return this;
    }

    public sfArticleManagementPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentTaskAttemptsPage extends sfHomePage {
    public sfStudentTaskAttemptsPage() {
        this(null);
    }

    public sfStudentTaskAttemptsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentTaskAttemptsPage load() {
        super.load();
        clickStudentTaskAttempts();
        return this;
    }

    public sfStudentTaskAttemptsPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}

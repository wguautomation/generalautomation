
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfEnrollmentQuestionsPage extends sfFilteredReportPage {
    public sfEnrollmentQuestionsPage() {
        this(null);
    }

    public sfEnrollmentQuestionsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureAcceptButton(true);
        ConfigureAssignButton(true);
        ConfigureRefreshButton(true);
    }

    public sfEnrollmentQuestionsPage load() {
        super.load();
        clickEnrollmentQuestions();
        return this;
    }

    public sfEnrollmentQuestionsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}


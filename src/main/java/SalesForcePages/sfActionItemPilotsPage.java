
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/18/2015.
 */
public class sfActionItemPilotsPage extends sfFilteredReportPage {
    public sfActionItemPilotsPage() {
        this(null);
    }

    public sfActionItemPilotsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureNewButton(true);
        ConfigureGoButton(true);
    }

    public sfActionItemPilotsPage load() {
        super.load();
        clickActionItemPilotsPage();
        return this;
    }

    public sfActionItemPilotsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}




package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/21/2015.
 */
public class sfTaskstreamAssessmentsPage extends sfHomePage {
    public sfTaskstreamAssessmentsPage() {
        this(null);
    }

    public sfTaskstreamAssessmentsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfTaskstreamAssessmentsPage load() {
        super.load();
        clickTaskstreamAssessments();
        return this;
    }

    public sfTaskstreamAssessmentsPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}


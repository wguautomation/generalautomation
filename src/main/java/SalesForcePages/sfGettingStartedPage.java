
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfGettingStartedPage extends sfHomePage {
    public sfGettingStartedPage() {
        this(null);
    }

    public sfGettingStartedPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfGettingStartedPage load() {
        super.load();
        clickGettingStarted();
        return this;
    }

    public sfGettingStartedPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



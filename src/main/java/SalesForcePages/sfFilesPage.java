
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfFilesPage extends sfHomePage {
    public sfFilesPage() {
        this(null);
    }

    public sfFilesPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfFilesPage load() {
        super.load();
        clickFiles();
        return this;
    }

    public sfFilesPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



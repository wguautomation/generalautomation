
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfUserAttributesPage extends sfHomePage {
    public sfUserAttributesPage() {
        this(null);
    }

    public sfUserAttributesPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfUserAttributesPage load() {
        super.load();
        clickUserAttributes();
        return this;
    }

    public sfUserAttributesPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


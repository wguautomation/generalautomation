
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfWGUCoursesPage extends sfHomePage {
    public sfWGUCoursesPage() {
        this(null);
    }

    public sfWGUCoursesPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfWGUCoursesPage load() {
        super.load();
        clickWGUCourses();
        return this;
    }

    public sfWGUCoursesPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


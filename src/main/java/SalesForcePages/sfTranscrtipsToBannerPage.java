
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfTranscrtipsToBannerPage extends sfHomePage {
    public sfTranscrtipsToBannerPage() {
        this(null);
    }

    public sfTranscrtipsToBannerPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfTranscrtipsToBannerPage load() {
        super.load();
        clickTranscriptsToBanner();
        return this;
    }

    public sfTranscrtipsToBannerPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


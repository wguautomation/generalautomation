
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentConcernsPage extends sfHomePage {
    public sfStudentConcernsPage() {
        this(null);
    }

    public sfStudentConcernsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentConcernsPage load() {
        super.load();
        clickStudentConcerns();
        return this;
    }

    public sfStudentConcernsPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
       }

        return this;
    }

}

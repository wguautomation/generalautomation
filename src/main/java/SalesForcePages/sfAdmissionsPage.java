
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/18/2015.
 */
public class sfAdmissionsPage extends sfFilteredReportPage {
    public sfAdmissionsPage() {
        this(null);
    }

    public sfAdmissionsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureNewButton(true);
        ConfigureGoButton(true);
    }

    public sfAdmissionsPage load() {
        super.load();
        clickAdmissionsPage();
        return this;
    }

    public sfAdmissionsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}


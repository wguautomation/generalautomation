
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfAlumniRelationsPage extends sfFilteredReportPage {
    public sfAlumniRelationsPage() {
        this(null);
    }

    public sfAlumniRelationsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureAssignButton(true);
        ConfigureRefreshButton(true);
    }

    public sfAlumniRelationsPage load() {
        super.load();
        clickAlumniRelations();
        return this;
    }

    public sfAlumniRelationsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}


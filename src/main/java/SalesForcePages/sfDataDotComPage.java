
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfDataDotComPage extends sfHomePage {
    public sfDataDotComPage() {
        this(null);
    }

    public sfDataDotComPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfDataDotComPage load() {
        super.load();
        clickDataDotCom();
        return this;
    }

    public sfDataDotComPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



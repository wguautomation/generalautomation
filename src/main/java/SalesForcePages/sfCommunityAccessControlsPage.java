
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/21/2015.
 */
public class sfCommunityAccessControlsPage extends sfFilteredReportPage {
    public sfCommunityAccessControlsPage() {
        this(null);
    }

    public sfCommunityAccessControlsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
    }

    public sfCommunityAccessControlsPage load() {
        super.load();
        clickCommunityAccessControls();
        return this;
    }

    public sfCommunityAccessControlsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}


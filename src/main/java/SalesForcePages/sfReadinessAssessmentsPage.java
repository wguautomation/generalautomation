
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfReadinessAssessmentsPage extends sfFilteredReportPage {
    public sfReadinessAssessmentsPage() {
        this(null);
    }

    public sfReadinessAssessmentsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureRefreshButton(true);
    }

    public sfReadinessAssessmentsPage load() {
        super.load();
        clickReadinessAssessments();
        return this;
    }

    public sfReadinessAssessmentsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



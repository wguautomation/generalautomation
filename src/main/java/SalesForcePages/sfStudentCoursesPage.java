
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentCoursesPage extends sfHomePage {
    public sfStudentCoursesPage() {
        this(null);
    }

    public sfStudentCoursesPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentCoursesPage load() {
        super.load();
        clickStudentCourses();
        return this;
    }

    public sfStudentCoursesPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}

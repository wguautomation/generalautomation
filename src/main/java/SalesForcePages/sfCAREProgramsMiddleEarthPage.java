
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/19/2015.
 */
public class sfCAREProgramsMiddleEarthPage extends sfFilteredReportPage {
    public sfCAREProgramsMiddleEarthPage() {
        this(null);
    }

    public sfCAREProgramsMiddleEarthPage(WebPage existingPage) {
        super(existingPage);
        ConfigureNewButton(true);
        ConfigureGoButton(true);
    }

    public sfCAREProgramsMiddleEarthPage load() {
        super.load();
        clickCAREProgramsMiddleEarth();
        return this;
    }

    public sfCAREProgramsMiddleEarthPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}


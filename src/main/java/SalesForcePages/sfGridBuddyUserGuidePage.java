
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfGridBuddyUserGuidePage extends sfHomePage {
    public sfGridBuddyUserGuidePage() {
        this(null);
    }

    public sfGridBuddyUserGuidePage(WebPage existingPage) {
        super(existingPage);
    }

    public sfGridBuddyUserGuidePage load() {
        super.load();
        clickGridBuddyUserGuide();
        return this;
    }

    public sfGridBuddyUserGuidePage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



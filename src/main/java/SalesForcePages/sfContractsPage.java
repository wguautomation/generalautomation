
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfContractsPage extends sfFilteredReportPage {
    public sfContractsPage() {
        this(null);
    }

    public sfContractsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureRefreshButton(true);
    }

    public sfContractsPage load() {
        super.load();
        clickContracts();
        return this;
    }

    public sfContractsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



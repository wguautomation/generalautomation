
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfWGUCourseCompletionTaskPage extends sfHomePage {
    public sfWGUCourseCompletionTaskPage() {
        this(null);
    }

    public sfWGUCourseCompletionTaskPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfWGUCourseCompletionTaskPage load() {
        super.load();
        clickWGUCourseCompletionTask();
        return this;
    }

    public sfWGUCourseCompletionTaskPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



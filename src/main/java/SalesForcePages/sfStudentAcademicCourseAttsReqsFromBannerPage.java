
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentAcademicCourseAttsReqsFromBannerPage extends sfHomePage {
    public sfStudentAcademicCourseAttsReqsFromBannerPage() {
        this(null);
    }

    public sfStudentAcademicCourseAttsReqsFromBannerPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentAcademicCourseAttsReqsFromBannerPage load() {
        super.load();
        clickStudentAcademicCourseAttsReqsFromBanner();
        return this;
    }

    public sfStudentAcademicCourseAttsReqsFromBannerPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
       }

        return this;
    }

}


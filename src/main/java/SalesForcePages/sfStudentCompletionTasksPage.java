
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentCompletionTasksPage extends sfHomePage {
    public sfStudentCompletionTasksPage() {
        this(null);
    }

    public sfStudentCompletionTasksPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentCompletionTasksPage load() {
        super.load();
        clickStudentCompletionTasks();
        return this;
    }

    public sfStudentCompletionTasksPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}

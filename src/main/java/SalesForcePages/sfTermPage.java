
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfTermPage extends sfHomePage {
    public sfTermPage() {
        this(null);
    }

    public sfTermPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfTermPage load() {
        super.load();
        clickTerm();
        return this;
    }

    public sfTermPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


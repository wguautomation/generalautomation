
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfMentorCourseGroupsToBannerPage extends sfFilteredReportPage {
    public sfMentorCourseGroupsToBannerPage() {
        this(null);
    }

    public sfMentorCourseGroupsToBannerPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureAssignButton(true);
        ConfigureRefreshButton(true);
    }

    public sfMentorCourseGroupsToBannerPage load() {
        super.load();
        clickMentorCourseGroupsToBanner();
        return this;
    }

    public sfMentorCourseGroupsToBannerPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



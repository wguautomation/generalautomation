
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfCommunityManagementPage extends sfHomePage {
    public sfCommunityManagementPage() {
        this(null);
    }

    public sfCommunityManagementPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfCommunityManagementPage load() {
        super.load();
        clickCommunityManagement();
        return this;
    }

    public sfCommunityManagementPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


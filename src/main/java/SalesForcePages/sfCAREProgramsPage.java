
package SalesForcePages;

import BaseClasses.WebPage;

/*
/**
 * Created by timothy.hallbeck on 2/19/2015.
 */
public class sfCAREProgramsPage extends sfFilteredReportPage {
    public sfCAREProgramsPage() {
        this(null);
    }

    public sfCAREProgramsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureNewButton(true);
        ConfigureGoButton(true);
    }

    public sfCAREProgramsPage load() {
        super.load();
        clickCAREPrograms();
        return this;
    }

    public sfCAREProgramsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
       }

        return this;
    }

}


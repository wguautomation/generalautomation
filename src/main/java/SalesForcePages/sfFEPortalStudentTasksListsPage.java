
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfFEPortalStudentTasksListsPage extends sfFilteredReportPage {
    public sfFEPortalStudentTasksListsPage() {
        this(null);
    }

    public sfFEPortalStudentTasksListsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureRefreshButton(true);
    }

    public sfFEPortalStudentTasksListsPage load() {
        super.load();
        clickFEPortalStudentTasksLists();
        return this;
    }

    public sfFEPortalStudentTasksListsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}


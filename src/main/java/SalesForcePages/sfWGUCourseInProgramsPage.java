
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfWGUCourseInProgramsPage extends sfHomePage {
    public sfWGUCourseInProgramsPage() {
        this(null);
    }

    public sfWGUCourseInProgramsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfWGUCourseInProgramsPage load() {
        super.load();
        clickWGUCourseInPrograms();
        return this;
    }

    public sfWGUCourseInProgramsPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


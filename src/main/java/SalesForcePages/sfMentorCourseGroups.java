
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfMentorCourseGroups extends sfFilteredReportPage {
    public sfMentorCourseGroups() {
        this(null);
    }

    public sfMentorCourseGroups(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureAssignButton(true);
        ConfigureRefreshButton(true);
        ConfigureFilterLetters(false);
    }

    public sfMentorCourseGroups load() {
        super.load();
        clickMentorCourseGroups();
        return this;
    }

    public sfMentorCourseGroups ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



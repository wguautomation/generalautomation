
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentTermsPage extends sfHomePage {
    public sfStudentTermsPage() {
        this(null);
    }

    public sfStudentTermsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentTermsPage load() {
        super.load();
        clickStudentTerms();
        return this;
    }

    public sfStudentTermsPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}

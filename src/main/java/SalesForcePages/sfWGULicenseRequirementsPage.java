
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfWGULicenseRequirementsPage extends sfHomePage {
    public sfWGULicenseRequirementsPage() {
        this(null);
    }

    public sfWGULicenseRequirementsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfWGULicenseRequirementsPage load() {
        super.load();
        clickWGULicenseRequirements();
        return this;
    }

    public sfWGULicenseRequirementsPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
       }

        return this;
    }

}



package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfServiceNowPage extends sfHomePage {
    public sfServiceNowPage() {
        this(null);
    }

    public sfServiceNowPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfServiceNowPage load() {
        super.load();
        clickServiceNow();
        return this;
    }

    public sfServiceNowPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



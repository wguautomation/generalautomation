
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfCareerServicesPage extends sfFilteredReportPage {
    public sfCareerServicesPage() {
        this(null);
    }

    public sfCareerServicesPage(WebPage existingPage) {
        super(existingPage);
        ConfigureNewButton(true);
        ConfigureGoButton(true);
    }

    public sfCareerServicesPage load() {
        super.load();
        clickCareerServices();
        return this;
    }

    public sfCareerServicesPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}


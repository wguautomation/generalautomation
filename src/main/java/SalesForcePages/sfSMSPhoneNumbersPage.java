
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfSMSPhoneNumbersPage extends sfHomePage {
    public sfSMSPhoneNumbersPage() {
        this(null);
    }

    public sfSMSPhoneNumbersPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfSMSPhoneNumbersPage load() {
        super.load();
        clickSMSPhoneNumbers();
        return this;
    }

    public sfSMSPhoneNumbersPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



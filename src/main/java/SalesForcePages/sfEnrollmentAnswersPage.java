
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfEnrollmentAnswersPage extends sfFilteredReportPage {
    public sfEnrollmentAnswersPage() {
        this(null);
    }

    public sfEnrollmentAnswersPage(WebPage existingPage) {
        super(existingPage);
        ConfigureNewButton(true);
        ConfigureGoButton(true);
        ConfigureRefreshButton(true);
    }

    public sfEnrollmentAnswersPage load() {
        super.load();
        clickEnrollmentAnswers();
        return this;
    }

    public sfEnrollmentAnswersPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/18/2015.
 */
public class sfAccountsPage extends sfFilteredReportPage {
    public sfAccountsPage() {
        this(null);
    }

    public sfAccountsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureNewButton(true);
        ConfigureGoButton(true);
    }

    public sfAccountsPage load() {
        super.load();
        clickAccountsPage();
        return this;
    }

    public sfAccountsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}


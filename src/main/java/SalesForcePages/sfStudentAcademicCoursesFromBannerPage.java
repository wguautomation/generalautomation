
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentAcademicCoursesFromBannerPage extends sfHomePage {
    public sfStudentAcademicCoursesFromBannerPage() {
        this(null);
    }

    public sfStudentAcademicCoursesFromBannerPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentAcademicCoursesFromBannerPage load() {
        super.load();
        clickStudentAcademicCourseAttsReqsFromBanner();
        return this;
    }

    public sfStudentAcademicCoursesFromBannerPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}

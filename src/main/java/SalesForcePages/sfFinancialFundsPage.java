
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfFinancialFundsPage extends sfFilteredReportPage {
    public sfFinancialFundsPage() {
        this(null);
    }

    public sfFinancialFundsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
        ConfigureNewButton(true);
        ConfigureAssignButton(true);
        ConfigureRefreshButton(true);
    }

    public sfFinancialFundsPage load() {
        super.load();
        clickFinancialFunds();
        return this;
    }

    public sfFinancialFundsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



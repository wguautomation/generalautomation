
package SalesForcePages;

import BaseClasses.WebPage;
import Utils.General;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfDashboardsPage extends sfHomePage {
    public sfDashboardsPage() {
        this(null);
    }

    public sfDashboardsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfDashboardsPage load() {
        super.load();
        clickDashboards();
        return this;
    }

    public sfDashboardsPage ExercisePage(boolean cascade) {
        General.Debug("sfDashboardsPage::ExercisePage(" + cascade + ")");
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


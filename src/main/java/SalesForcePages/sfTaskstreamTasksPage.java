
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/21/2015.
 */
public class sfTaskstreamTasksPage extends sfHomePage {
    public sfTaskstreamTasksPage() {
        this(null);
    }

    public sfTaskstreamTasksPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfTaskstreamTasksPage load() {
        super.load();
        clickTaskstreamTasks();
        return this;
    }

    public sfTaskstreamTasksPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}


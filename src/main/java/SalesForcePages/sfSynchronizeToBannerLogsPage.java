
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfSynchronizeToBannerLogsPage extends sfHomePage {
    public sfSynchronizeToBannerLogsPage() {
        this(null);
    }

    public sfSynchronizeToBannerLogsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfSynchronizeToBannerLogsPage load() {
        super.load();
        clickSynchronizeToBannerLogs();
        return this;
    }

    public sfSynchronizeToBannerLogsPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}

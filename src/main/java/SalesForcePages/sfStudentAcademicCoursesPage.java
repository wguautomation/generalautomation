
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentAcademicCoursesPage extends sfHomePage {
    public sfStudentAcademicCoursesPage() {
        this(null);
    }

    public sfStudentAcademicCoursesPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentAcademicCoursesPage load() {
        super.load();
        clickStudentAcademicCourses();
        return this;
    }

    public sfStudentAcademicCoursesPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}

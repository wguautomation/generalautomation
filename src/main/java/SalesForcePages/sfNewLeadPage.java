
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/23/2015.
 */
public class sfNewLeadPage extends sfNewItemPage {
    public sfNewLeadPage() {
        this(null);
    }

    public sfNewLeadPage(WebPage existingPage) {
        super(existingPage);
    }

}

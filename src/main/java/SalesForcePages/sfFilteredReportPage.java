
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/23/2015.
 */
public abstract class sfFilteredReportPage extends sfHomePage {
    public sfFilteredReportPage() {
        this(null);
    }

    public sfFilteredReportPage(WebPage existingPage) {
        super(existingPage);
    }

    private boolean goButtonExists      = false;
    private boolean newButtonExists     = false;
    private boolean acceptButtonExists  = false;
    private boolean assignButtonExists  = false;
    private boolean refreshButtonExists = false;
    private boolean filterLettersExists = true;

    protected sfFilteredReportPage ConfigureGoButton(boolean exists) {
        goButtonExists = exists;
        return this;
    }

    protected sfFilteredReportPage ConfigureNewButton(boolean exists) {
        newButtonExists = exists;
        return this;
    }

    protected sfFilteredReportPage ConfigureAcceptButton(boolean exists) {
        acceptButtonExists = exists;
        return this;
    }

    protected sfFilteredReportPage ConfigureAssignButton(boolean exists) {
        assignButtonExists = exists;
        return this;
    }

    protected sfFilteredReportPage ConfigureRefreshButton(boolean exists) {
        refreshButtonExists = exists;
        return this;
    }

    protected sfFilteredReportPage ConfigureFilterLetters(boolean exists) {
        filterLettersExists = exists;
        return this;
    }

    public sfFilteredReportPage clickNewButton() {
        getByXpath("//*[@name=\"new\"]");
        getByXpath("//*[@name=\"new\"]").click();
        return this;
    }

    // Item must be selected to be Accepted. No logic for this yet, so this code currently does nothing TODO
    public sfFilteredReportPage clickAcceptButton() {
        getByXpath("//*[@name=\"accept\"]");
//        getByXpath("//*[@name=\"accept\"]").click();
        return this;
    }

    // Item must be selected to be reassigned. No logic for this yet, so this code currently does nothing TODO
    public sfFilteredReportPage clickAssignButton() {
        getByXpath("//*[@name=\"assign\"]");
//        getByXpath("//*[@name=\"assign\"]").click();
        return this;
    }

    public sfFilteredReportPage clickCancelButton() {
        getByXpath("//*[@name=\"cancel\"]");
        getByXpath("//*[@name=\"cancel\"]").click();
        return this;
    }

    // different pages have "btn refreshListButton" or just "refreshListButton"  needs investigation TODO
    public sfFilteredReportPage clickRefreshButton() {
//        getByXpath("//*[@class=\"refreshListButton\"]");
//       getByXpath("//*[@class=\"refreshListButton\"]").click();
        return this;
    }

    protected static String[] FilterItems = {
        "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
        "S", "T", "U", "V", "W", "X", "Y", "Z", "Other", "All",
    };

    public sfFilteredReportPage clickSingleLetterFilter(String letter) {
        getByLinkText(letter).click();
        Sleep(250);
        return this;
    }

    public sfFilteredReportPage ExerciseAllFilters() {
        for (String letter : FilterItems)
            clickSingleLetterFilter(letter);
        return this;
    }

    public sfFilteredReportPage ExerciseCommonControls() {
        if (goButtonExists)
            clickGo();

        if (refreshButtonExists)
            clickRefreshButton();

        if (newButtonExists) {
            clickNewButton();
            clickCancelButton();
        }

        if (assignButtonExists)
            clickAssignButton();

        if (filterLettersExists)
            ExerciseAllFilters();

        return this;
    }

}


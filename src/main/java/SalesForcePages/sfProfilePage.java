
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfProfilePage extends sfHomePage {
    public sfProfilePage() {
        this(null);
    }

    public sfProfilePage(WebPage existingPage) {
        super(existingPage);
    }

    public sfProfilePage load() {
        super.load();
        clickProfile();
        return this;
    }

    public sfProfilePage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



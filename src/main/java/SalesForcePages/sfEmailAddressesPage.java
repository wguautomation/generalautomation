
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfEmailAddressesPage extends sfFilteredReportPage {
    public sfEmailAddressesPage() {
        this(null);
    }

    public sfEmailAddressesPage(WebPage existingPage) {
        super(existingPage);
        ConfigureNewButton(true);
        ConfigureGoButton(true);
        ConfigureRefreshButton(true);
    }

    public sfEmailAddressesPage load() {
        super.load();
        clickEmailAddresses();
        return this;
    }

    public sfEmailAddressesPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}


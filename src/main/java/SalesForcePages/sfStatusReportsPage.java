
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/21/2015.
 */
public class sfStatusReportsPage extends sfHomePage {
    public sfStatusReportsPage() {
        this(null);
    }

    public sfStatusReportsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStatusReportsPage load() {
        super.load();
        clickStatusReports();
        return this;
    }

    public sfStatusReportsPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfWGUFEToDoPage extends sfHomePage {
    public sfWGUFEToDoPage() {
        this(null);
    }

    public sfWGUFEToDoPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfWGUFEToDoPage load() {
        super.load();
        clickWGUFEToDo();
        return this;
    }

    public sfWGUFEToDoPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}


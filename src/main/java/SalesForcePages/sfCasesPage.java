
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/21/2015.
 */
public class sfCasesPage extends sfFilteredReportPage {
    public sfCasesPage() {
        this(null);
    }

    public sfCasesPage(WebPage existingPage) {
        super(existingPage);
        ConfigureGoButton(true);
    }

    public sfCasesPage load() {
        super.load();
        clickCases();
        return this;
    }

    public sfCasesPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}


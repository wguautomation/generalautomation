
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentDegreePlansPage extends sfHomePage {
    public sfStudentDegreePlansPage() {
        this(null);
    }

    public sfStudentDegreePlansPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentDegreePlansPage load() {
        super.load();
        clickStudentDegreePlans();
        return this;
    }

    public sfStudentDegreePlansPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}

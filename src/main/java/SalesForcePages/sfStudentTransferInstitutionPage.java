
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentTransferInstitutionPage extends sfHomePage {
    public sfStudentTransferInstitutionPage() {
        this(null);
    }

    public sfStudentTransferInstitutionPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentTransferInstitutionPage load() {
        super.load();
        clickStudentTransferInstitution();
        return this;
    }

    public sfStudentTransferInstitutionPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}

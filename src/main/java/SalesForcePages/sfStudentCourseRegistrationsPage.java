
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentCourseRegistrationsPage extends sfHomePage {
    public sfStudentCourseRegistrationsPage() {
        this(null);
    }

    public sfStudentCourseRegistrationsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentCourseRegistrationsPage load() {
        super.load();
        clickStudentCourseRegistrations();
        return this;
    }

    public sfStudentCourseRegistrationsPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}

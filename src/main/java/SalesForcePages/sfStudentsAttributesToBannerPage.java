
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfStudentsAttributesToBannerPage extends sfHomePage {
    public sfStudentsAttributesToBannerPage() {
        this(null);
    }

    public sfStudentsAttributesToBannerPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfStudentsAttributesToBannerPage load() {
        super.load();
        clickStudentsAttributesToBanner();
        return this;
    }

    public sfStudentsAttributesToBannerPage ExercisePage(boolean cascade) {
        load();
        clickGo();

        if (cascade) {
            return this;
        }

        return this;
    }

}

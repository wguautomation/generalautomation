
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfDocumentsPage extends sfFilteredReportPage {
    public sfDocumentsPage() {
        this(null);
    }

    public sfDocumentsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureNewButton(true);
        ConfigureGoButton(true);
    }

    public sfDocumentsPage load() {
        super.load();
        clickDocuments();
        return this;
    }

    public sfDocumentsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}



package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/21/2015.
 */
public class sfCohortsPage extends sfFilteredReportPage {
    public sfCohortsPage() {
        this(null);
    }

    public sfCohortsPage(WebPage existingPage) {
        super(existingPage);
        ConfigureNewButton(true);
        ConfigureGoButton(true);
    }

    public sfCohortsPage load() {
        super.load();
        clickCohorts();
        return this;
    }

    public sfCohortsPage ExercisePage(boolean cascade) {
        load();
        ExerciseCommonControls();

        if (cascade) {
            return this;
        }

        return this;
    }

}


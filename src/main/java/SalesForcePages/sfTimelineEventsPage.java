
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfTimelineEventsPage extends sfHomePage {
    public sfTimelineEventsPage() {
        this(null);
    }

    public sfTimelineEventsPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfTimelineEventsPage load() {
        super.load();
        clickTimelineEvents();
        return this;
    }

    public sfTimelineEventsPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



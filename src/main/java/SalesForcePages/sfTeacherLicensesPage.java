
package SalesForcePages;

import BaseClasses.WebPage;

/*
 * Created by timothy.hallbeck on 2/22/2015.
 */
public class sfTeacherLicensesPage extends sfHomePage {
    public sfTeacherLicensesPage() {
        this(null);
    }

    public sfTeacherLicensesPage(WebPage existingPage) {
        super(existingPage);
    }

    public sfTeacherLicensesPage load() {
        super.load();
        clickTeacherLicenses();
        return this;
    }

    public sfTeacherLicensesPage ExercisePage(boolean cascade) {
        load();

        if (cascade) {
            return this;
        }

        return this;
    }

}



package CassandraClasses;

import Utils.General;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;

/*
 * Created by timothy.hallbeck on 5/16/2016.
 */

public class studentInfo extends CassandraBase {

    public static int COLUMN_PIDM                    = 0;
    public static int COLUMN_ABOUTME                 = 1;
    public static int COLUMN_CITY                    = 2;
    public static int COLUMN_COLLEGENAME             = 3;
    public static int COLUMN_COUNTRY                 = 4;
    public static int COLUMN_CURRENTSTUDENTSTATUS    = 5;
    public static int COLUMN_DEGREECODE              = 6;
    public static int COLUMN_DEGREENAME              = 7;
    public static int COLUMN_FIRSTNAME               = 8;
    public static int COLUMN_HASPUBLICABOUTME        = 9;
    public static int COLUMN_HASPUBLICADDRESS        = 10;
    public static int COLUMN_HASPUBLICCOLLEGE        = 11;
    public static int COLUMN_HASPUBLICCOUNTRY        = 12;
    public static int COLUMN_HASPUBLICCURRENTCOURSES = 13;
    public static int COLUMN_HASPUBLICDIRSEARCH      = 14;
    public static int COLUMN_HASPUBLICFIRSTNAME      = 15;
    public static int COLUMN_HASPUBLICGRADYEAR       = 16;
    public static int COLUMN_HASPUBLICMILITARYSTATUS = 17;
    public static int COLUMN_HASPUBLICMOBILEPHONE    = 18;
    public static int COLUMN_HASPUBLICPREFERREDEMAIL = 19;
    public static int COLUMN_HASPUBLICPRIMARYPHONE   = 20;
    public static int COLUMN_HASPUBLICSOCIALLINKS    = 21;
    public static int COLUMN_HASPUBLICSTATUS         = 22;
    public static int COLUMN_HASPUBLICZIPCODE        = 23;
    public static int COLUMN_HOMEAREACODE            = 24;
    public static int COLUMN_HOMEPHONE               = 25;
    public static int COLUMN_HOMEPHONENUMBER         = 26;
    public static int COLUMN_HOMEPHONETYPE           = 27;
    public static int COLUMN_ISACTIVEMILITARY        = 28;
    public static int COLUMN_ISCONFIDENTIAL          = 29;
    public static int COLUMN_ISDECEASED              = 30;
    public static int COLUMN_ISEMPLOYEE              = 31;
    public static int COLUMN_ISENROLLEDINEWB         = 32;
    public static int COLUMN_ISSTUDENT               = 33;
    public static int COLUMN_LASTCARESTAGE           = 34;
    public static int COLUMN_LASTNAME                = 35;
    public static int COLUMN_MIDDLENAME              = 36;
    public static int COLUMN_MOBILEAREACODE          = 37;
    public static int COLUMN_MOBILEPHONE             = 38;
    public static int COLUMN_MOBILEPHONENUMBER       = 39;
    public static int COLUMN_MOBILEPHONETYPE         = 40;
    public static int COLUMN_OFFICIALFIRSTNAME       = 41;
    public static int COLUMN_PERSONTYPE              = 42;
    public static int COLUMN_PLANNEDGRADUATIONDATE   = 43;
    public static int COLUMN_PREFERREDEMAIL          = 44;
    public static int COLUMN_PREFERREDFIRSTNAME      = 45;
    public static int COLUMN_PRIMARYAREACODE         = 46;
    public static int COLUMN_PRIMARYPHONE            = 47;
    public static int COLUMN_PRIMARYPHONENUMBER      = 48;
    public static int COLUMN_PRIMARYPHONETYPE        = 49;
    public static int COLUMN_PROFILETIMEZONE         = 50;
    public static int COLUMN_PROSPECTIVESTUDENTSTARTDATE = 51;
    public static int COLUMN_SOCIALMEDIALINKSJSON    = 52;
    public static int COLUMN_STATE                   = 53;
    public static int COLUMN_STREETLINE1             = 54;
    public static int COLUMN_STREETLINE2             = 55;
    public static int COLUMN_STUDENTID               = 56;
    public static int COLUMN_STUDENTSTATUS           = 57;
    public static int COLUMN_USERNAME                = 58;
    public static int COLUMN_WGUEMAIL                = 59;
    public static int COLUMN_ZIPCODE                 = 60;

    public studentInfo(String... connections) {
        super("rdb", connections);
    }

    public ResultSet getAllStudents() {
        return getAllStudents(false);
    }
    public ResultSet getAllStudents(boolean showOutput) {
        ResultSet results = executeQuery("Select * from rdb.studinfo");
        if (showOutput)
            listAllRows(results);

        return results;
    }

    public ResultSet getRecordCount() {
        ResultSet results = executeQuery("Select count(*) from rdb.studinfo");
        long count = results.one().getLong(0);
        General.Debug("\nThere are " + count + " rows in rdb.studinfo");

        return results;
    }

    // Note - using the iterator drains the set.
    public void listAllRows(ResultSet resultSet) {
        while (!resultSet.isExhausted()) {
            for (Row row : resultSet) {
                String output = "  \nRecord: \n";
                output += "                    pidm: " + getPidm(row)                    + "\n";
                output += "                 aboutme: " + getAboutMe(row)                 + "\n";
                output += "                    city: " + getCity(row)                    + "\n";
                output += "             collegename: " + getCollegeName(row)             + "\n";
                output += "                 country: " + getCountry(row)                 + "\n";
                output += "    currentstudentstatus: " + getCurrentStudentStatus(row)    + "\n";
                output += "              degreecode: " + getDegreeCode(row)              + "\n";
                output += "              degreename: " + getDegreeName(row)              + "\n";
                output += "               firstname: " + getFirstName(row)               + "\n";
                output += "        haspublicaboutme: " + getHasPublicAboutMe(row)        + "\n";
                output += "        haspublicaddress: " + getHasPublicAddress(row)        + "\n";
                output += "        haspubliccollege: " + getHasPublicCollege(row)        + "\n";
                output += "        haspubliccountry: " + getHasPublicCountry(row)        + "\n";
                output += " haspubliccurrentcourses: " + getHasPublicCurrentCourses(row) + "\n";
                output += "      haspublicdirsearch: " + getHasPublicdirSearch(row)      + "\n";
                output += "      haspublicfirstname: " + getHasPublicFirstName(row)      + "\n";
                output += "       haspublicgradyear: " + getHasPublicGradYear(row)       + "\n";
                output += " haspublicmilitarystatus: " + getHasPublicMilitaryStatus(row) + "\n";
                output += "    haspublicmobilephone: " + getHasPublicMobilePhone(row)    + "\n";
                output += " haspublicpreferredemail: " + getHasPublicPreferredEmail(row) + "\n";
                output += "   haspublicprimaryphone: " + getHasPublicPrimaryPhone(row)   + "\n";
                output += "    haspublicsociallinks: " + getHasPublicSocialLinks(row)    + "\n";
                output += "         haspublicstatus: " + getHasPublicStatus(row)         + "\n";
                output += "        haspubliczipcode: " + getHasPublicZipcode(row)        + "\n";
                output += "            homeareacode: " + getHomeAreaCode(row)            + "\n";
                output += "               homephone: " + getHomePhone(row)               + "\n";
                output += "         homephonenumber: " + getHomePhoneNumber(row)         + "\n";
                output += "           homephonetype: " + getHomePhonetype(row)           + "\n";
                output += "        isactivemilitary: " + getIsActiveMilitary(row)        + "\n";
                output += "          isconfidential: " + getIsConfidential(row)          + "\n";
                output += "              isdeceased: " + getIsDeceased(row)              + "\n";
                output += "              isemployee: " + getIsEmployee(row)              + "\n";
                output += "         isenrolledinewb: " + getIsEnrolledNews(row)          + "\n";
                output += "               isstudent: " + getIsStudent(row)               + "\n";
                output += "           lastcarestage: " + getLastCareStage(row)           + "\n";
                output += "                lastname: " + getLastName(row)                + "\n";
                output += "              middlename: " + getMiddleName(row)              + "\n";
                output += "          mobileareacode: " + getMobileAreaCode(row)          + "\n";
                output += "             mobilephone: " + getMobilePhone(row)             + "\n";
                output += "       mobilephonenumber: " + getMobilePhoneNumber(row)       + "\n";
                output += "         mobilephonetype: " + getMobilePhoneType(row)         + "\n";
                output += "       officialfirstname: " + getOfficialFirstName(row)       + "\n";
                output += "              persontype: " + getPersonType(row)              + "\n";
                output += "   plannedgraduationdate: " + getPlannedGraduationDate(row)   + "\n";
                output += "          preferredemail: " + getPreferredEmail(row)          + "\n";
                output += "      preferredfirstname: " + getPreferredFirstName(row)      + "\n";
                output += "         primaryareacode: " + getPrimaryAreaCode(row)         + "\n";
                output += "            primaryphone: " + getPrimaryPhone(row)            + "\n";
                output += "      primaryphonenumber: " + getPrimaryPhoneNumber(row)      + "\n";
                output += "        primaryphonetype: " + getPrimaryPhoneType(row)        + "\n";
                output += "         profiletimezone: " + getProfileTimeZone(row)         + "\n";
                output += "prospectivestudentstartdate: " + getStartDate(row)            + "\n";
                output += "    socialmedialinksjson: " + getSocialMediaLinksJson(row)    + "\n";
                output += "                   state: " + getState(row)                   + "\n";
                output += "             streetline1: " + getStreetLine1(row)             + "\n";
                output += "             streetline2: " + getStreetLine2(row)             + "\n";
                output += "               studentid: " + getStudentId(row)               + "\n";
                output += "           studentstatus: " + getStudentStatus(row)           + "\n";
                output += "                username: " + getUsername(row)                + "\n";
                output += "                wguemail: " + getWguEmail(row)                + "\n";
                output += "                 zipcode: " + getZipCode(row)                 + "\n";
                General.Debug(output);
            }
            resultSet.fetchMoreResults();
        }
    }

    public ResultSet addRecordByPidm(int id) {
        String query = "INSERT INTO rdb.studinfo (pidm, aboutme, city, collegename) VALUES (";
        ResultSet resultSet = executeQuery(query);

        return resultSet;
    }

    public ResultSet getRecordByPidm(int pidm) {
        String query = "select * from rdb.studinfo where pidm = " + pidm;
        ResultSet resultSet = executeQuery(query);

        return resultSet;
    }

    public ResultSet deleteRecordByPidm(int pidm) {
        String query = "delete from rdb.studinfo where pidm = " + pidm;
        ResultSet resultSet = executeQuery(query);

        return resultSet;
    }

    public ResultSet modifyRecordByPidm(int pidm) {
        String query = "update rdb.studinfo set aboutme = 'modified' where pidm = " + pidm;
        ResultSet resultSet = executeQuery(query);

        return resultSet;
    }

    // Accessors
    public String getPidm(Row row) {
        return safeGetDecimal(row, COLUMN_PIDM).toString();
    }
    public String getAboutMe(Row row) {
        return row.getString(COLUMN_ABOUTME);
    }
    public String getCity(Row row) {
        return row.getString(COLUMN_CITY);
    }
    public String getCollegeName(Row row) {
        return row.getString(COLUMN_COLLEGENAME);
    }
    public String getCountry(Row row) {
        return row.getString(COLUMN_COUNTRY);
    }
    public String getCurrentStudentStatus(Row row) {
        return row.getString(COLUMN_CURRENTSTUDENTSTATUS);
    }
    public String getDegreeCode(Row row) {
        return row.getString(COLUMN_DEGREECODE);
    }
    public String getDegreeName(Row row) {
        return row.getString(COLUMN_DEGREENAME);
    }
    public String getFirstName(Row row) {
        return row.getString(COLUMN_FIRSTNAME);
    }
    public String getHasPublicAboutMe(Row row) {
        return safeGetDecimal(row, COLUMN_HASPUBLICABOUTME);
    }
    public String getHasPublicAddress(Row row) {
        return safeGetDecimal(row, COLUMN_HASPUBLICADDRESS);
    }
    public String getHasPublicCollege(Row row) {
        return safeGetDecimal(row, COLUMN_HASPUBLICCOLLEGE);
    }
    public String getHasPublicCountry(Row row) {
        return safeGetDecimal(row, COLUMN_HASPUBLICCOUNTRY);
    }
    public String getHasPublicCurrentCourses(Row row) {
        return safeGetDecimal(row, COLUMN_HASPUBLICCURRENTCOURSES);
    }
    public String getHasPublicdirSearch(Row row) {
        return safeGetDecimal(row, COLUMN_HASPUBLICDIRSEARCH);
    }
    public String getHasPublicFirstName(Row row) {
        return safeGetDecimal(row, COLUMN_HASPUBLICFIRSTNAME);
    }
    public String getHasPublicGradYear(Row row) {
        return safeGetDecimal(row, COLUMN_HASPUBLICGRADYEAR);
    }
    public String getHasPublicMilitaryStatus(Row row) {
        return safeGetDecimal(row, COLUMN_HASPUBLICMILITARYSTATUS);
    }
    public String getHasPublicMobilePhone(Row row) {
        return safeGetDecimal(row, COLUMN_HASPUBLICMOBILEPHONE);
    }
    public String getHasPublicPreferredEmail(Row row) {
        return safeGetDecimal(row, COLUMN_HASPUBLICPREFERREDEMAIL);
    }
    public String getHasPublicPrimaryPhone(Row row) {
        return safeGetDecimal(row, COLUMN_HASPUBLICPRIMARYPHONE);
    }
    public String getHasPublicSocialLinks(Row row) {
        return safeGetDecimal(row, COLUMN_HASPUBLICSOCIALLINKS);
    }
    public String getHasPublicStatus(Row row) {
        return safeGetDecimal(row, COLUMN_HASPUBLICSTATUS);
    }
    public String getHasPublicZipcode(Row row) {
        return safeGetDecimal(row, COLUMN_HASPUBLICZIPCODE);
    }
    public String getHomeAreaCode(Row row) {
        return row.getString(COLUMN_HOMEAREACODE);
    }
    public String getHomePhone(Row row) {
        return row.getString(COLUMN_HOMEPHONE);
    }
    public String getHomePhoneNumber(Row row) {
        return row.getString(COLUMN_HOMEPHONENUMBER);
    }
    public String getHomePhonetype(Row row) {
        return row.getString(COLUMN_HOMEPHONETYPE);
    }
    public String getIsActiveMilitary(Row row) {
        return safeGetDecimal(row, COLUMN_ISACTIVEMILITARY);
    }
    public String getIsConfidential(Row row) {
        return safeGetDecimal(row, COLUMN_ISCONFIDENTIAL);
    }
    public String getIsDeceased(Row row) {
        return safeGetDecimal(row, COLUMN_ISDECEASED);
    }
    public String getIsEmployee(Row row) {
        return safeGetDecimal(row, COLUMN_ISEMPLOYEE);
    }
    public String getIsEnrolledNews(Row row) {
        return safeGetDecimal(row, COLUMN_ISENROLLEDINEWB);
    }
    public String getIsStudent(Row row) {
        return safeGetDecimal(row, COLUMN_ISSTUDENT);
    }
    public String getLastCareStage(Row row) {
        return row.getString(COLUMN_LASTCARESTAGE);
    }
    public String getLastName(Row row) {
        return row.getString(COLUMN_LASTNAME);
    }
    public String getMiddleName(Row row) {
        return row.getString(COLUMN_MIDDLENAME);
    }
    public String getMobileAreaCode(Row row) {
        return row.getString(COLUMN_MOBILEAREACODE);
    }
    public String getMobilePhone(Row row) {
        return row.getString(COLUMN_MOBILEPHONE);
    }
    public String getMobilePhoneNumber(Row row) {
        return row.getString(COLUMN_MOBILEPHONENUMBER);
    }
    public String getMobilePhoneType(Row row) {
        return row.getString(COLUMN_MOBILEPHONETYPE);
    }
    public String getOfficialFirstName(Row row) {
        return row.getString(COLUMN_OFFICIALFIRSTNAME);
    }
    public String getPersonType(Row row) {
        return row.getString(COLUMN_PERSONTYPE);
    }
    public String getPlannedGraduationDate(Row row) {
        return row.getString(COLUMN_PLANNEDGRADUATIONDATE);
    }
    public String getPreferredEmail(Row row) {
        return row.getString(COLUMN_PREFERREDEMAIL);
    }
    public String getPreferredFirstName(Row row) {
        return row.getString(COLUMN_PREFERREDFIRSTNAME);
    }
    public String getPrimaryAreaCode(Row row) {
        return row.getString(COLUMN_PRIMARYAREACODE);
    }
    public String getPrimaryPhone(Row row) {
        return row.getString(COLUMN_PRIMARYPHONE);
    }
    public String getPrimaryPhoneNumber(Row row) {
        return row.getString(COLUMN_PRIMARYPHONENUMBER);
    }
    public String getPrimaryPhoneType(Row row) {
        return row.getString(COLUMN_PRIMARYPHONETYPE);
    }
    public String getProfileTimeZone(Row row) {
        return row.getString(COLUMN_PROFILETIMEZONE);
    }
    public String getStartDate(Row row) {
        return row.getString(COLUMN_PROSPECTIVESTUDENTSTARTDATE);
    }
    public String getSocialMediaLinksJson(Row row) {
        String string = row.getString(COLUMN_SOCIALMEDIALINKSJSON);
        return row.getString(COLUMN_SOCIALMEDIALINKSJSON);
    }
    public String getState(Row row) {
        return row.getString(COLUMN_STATE);
    }
    public String getStreetLine1(Row row) {
        return row.getString(COLUMN_STREETLINE1);
    }
    public String getStreetLine2(Row row) {
        return row.getString(COLUMN_STREETLINE2);
    }
    public String getStudentId(Row row) {
        return row.getString(COLUMN_STUDENTID);
    }
    public String getStudentStatus(Row row) {
        return row.getString(COLUMN_STUDENTSTATUS);
    }
    public String getUsername(Row row) {
        return row.getString(COLUMN_USERNAME);
    }
    public String getWguEmail(Row row) {
        return row.getString(COLUMN_WGUEMAIL);
    }
    public String getZipCode(Row row) {
        return row.getString(COLUMN_ZIPCODE);
    }

    public String toJson(Row row) {
        String json = "{";

        json += "\"pidm\":"                    + unquotedNull(getPidm(row))                    + ",";
        json += "\"aboutme\":"                 + unquotedNull(getAboutMe(row))                 + ",";
        json += "\"city\":"                    + unquotedNull(getCity(row))                    + ",";
        json += "\"collegename\":"             + unquotedNull(getCollegeName(row))             + ",";
        json += "\"country\":"                 + unquotedNull(getCountry(row))                 + ",";
        json += "\"currentstudentstatus\":"    + unquotedNull(getCurrentStudentStatus(row))    + ",";
        json += "\"degreecode\":"              + unquotedNull(getDegreeCode(row))              + ",";
        json += "\"degreename\":"              + unquotedNull(getDegreeName(row))              + ",";
        json += "\"firstname\":"               + unquotedNull(getFirstName(row))               + ",";
        json += "\"haspublicaboutme\":"        + unquotedNull(getHasPublicAboutMe(row))        + ",";
        json += "\"haspublicaddress\":"        + unquotedNull(getHasPublicAddress(row))        + ",";
        json += "\"haspubliccollege\":"        + unquotedNull(getHasPublicCollege(row))        + ",";
        json += "\"haspubliccountry\":"        + unquotedNull(getHasPublicCountry(row))        + ",";
        json += "\"haspubliccurrentcourses\":" + unquotedNull(getHasPublicCurrentCourses(row)) + ",";
        json += "\"haspublicdirsearch\":"      + unquotedNull(getHasPublicdirSearch(row))      + ",";
        json += "\"haspublicfirstname\":"      + unquotedNull(getHasPublicFirstName(row))      + ",";
        json += "\"haspublicgradyear\":"       + unquotedNull(getHasPublicGradYear(row))       + ",";
        json += "\"haspublicmilitarystatus\":" + unquotedNull(getHasPublicMilitaryStatus(row)) + ",";
        json += "\"haspublicmobilephone\":"    + unquotedNull(getHasPublicMobilePhone(row))    + ",";
        json += "\"haspublicpreferredemail\":" + unquotedNull(getHasPublicPreferredEmail(row)) + ",";
        json += "\"haspublicprimaryphone\":"   + unquotedNull(getHasPublicPrimaryPhone(row))   + ",";
        json += "\"haspublicsociallinks\":"    + unquotedNull(getHasPublicSocialLinks(row))    + ",";
        json += "\"haspublicstatus\":"         + unquotedNull(getHasPublicStatus(row))         + ",";
        json += "\"haspubliczipcode\":"        + unquotedNull(getHasPublicZipcode(row))        + ",";
        json += "\"homeareacode\":"            + unquotedNull(getHomeAreaCode(row))            + ",";
        json += "\"homephone\":"               + unquotedNull(getHomePhone(row))               + ",";
        json += "\"homephonenumber\":"         + unquotedNull(getHomePhoneNumber(row))         + ",";
        json += "\"homephonetype\":"           + unquotedNull(getHomePhonetype(row))           + ",";
        json += "\"isactivemilitary\":"        + unquotedNull(getIsActiveMilitary(row))        + ",";
        json += "\"isconfidential\":"          + unquotedNull(getIsConfidential(row))          + ",";
        json += "\"isdeceased\":"              + unquotedNull(getIsDeceased(row))              + ",";
        json += "\"isemployee\":"              + unquotedNull(getIsEmployee(row))              + ",";
        json += "\"isenrolledinewb\":"         + unquotedNull(getIsEnrolledNews(row))          + ",";
        json += "\"isstudent\":"               + unquotedNull(getIsStudent(row))               + ",";
        json += "\"lastcarestage\":"           + unquotedNull(getLastCareStage(row))           + ",";
        json += "\"lastname\":"                + unquotedNull(getLastName(row))                + ",";
        json += "\"middlename\":"              + unquotedNull(getMiddleName(row))              + ",";
        json += "\"mobileareacode\":"          + unquotedNull(getMobileAreaCode(row))          + ",";
        json += "\"mobilephone\":"             + unquotedNull(getMobilePhone(row))             + ",";
        json += "\"mobilephonenumber\":"       + unquotedNull(getMobilePhoneNumber(row))       + ",";
        json += "\"mobilephonetype\":"         + unquotedNull(getMobilePhoneType(row))         + ",";
        json += "\"officialfirstname\":"       + unquotedNull(getOfficialFirstName(row))       + ",";
        json += "\"persontype\":"              + unquotedNull(getPersonType(row))              + ",";
        json += "\"plannedgraduationdate\":"   + unquotedNull(getPlannedGraduationDate(row))   + ",";
        json += "\"preferredemail\":"          + unquotedNull(getPreferredEmail(row))          + ",";
        json += "\"preferredfirstname\":"      + unquotedNull(getPreferredFirstName(row))      + ",";
        json += "\"primaryareacode\":"         + unquotedNull(getPrimaryAreaCode(row))         + ",";
        json += "\"primaryphone\":"            + unquotedNull(getPrimaryPhone(row))            + ",";
        json += "\"primaryphonenumber\":"      + unquotedNull(getPrimaryPhoneNumber(row))      + ",";
        json += "\"primaryphonetype\":"        + unquotedNull(getPrimaryPhoneType(row))        + ",";
        json += "\"profiletimezone\":"         + unquotedNull(getProfileTimeZone(row))         + ",";
        json += "\"prospectivestudentstartdate\":" + unquotedNull(getStartDate(row))           + ",";
        json += "\"socialmedialinksjson\":"    + unquotedNull(getSocialMediaLinksJson(row))    + ",";
        json += "\"state\":"                   + unquotedNull(getState(row))                   + ",";
        json += "\"streetline1\":"             + unquotedNull(getStreetLine1(row))             + ",";
        json += "\"streetline2\":"             + unquotedNull(getStreetLine2(row))             + ",";
        json += "\"studentid\":"               + unquotedNull(getStudentId(row))               + ",";
        json += "\"studentstatus\":"           + unquotedNull(getStudentStatus(row))           + ",";
        json += "\"username\":"                + unquotedNull(getUsername(row))                + ",";
        json += "\"wguemail\":"                + unquotedNull(getWguEmail(row))                + ",";
        json += "\"zipcode\":"                 + unquotedNull(getZipCode(row));
        json += "}";

        return json;
    }

    private String unquotedNull(String string) {
        if (string == null || string.isEmpty())
            return "null";
        return "\"" + string + "\"";
    }

}











package CassandraClasses;

import Utils.General;
import com.datastax.driver.core.*;
import org.testng.TestNGException;
import java.math.BigDecimal;

/*
 * Created by timothy.hallbeck on 5/3/2016.
 */

abstract public class CassandraBase {

    static final private String USERNAME = "cassandra";
    static final private String PASSWORD = "cassandra";
    static final public  String connectionLocalSingleNode          = "127.0.0.1";
    static final public  String connectionRemoteLane2SingleNode    = "10.20.20.139";
    static final public  String connectionRemoteLane2MultipleNode1 = "10.20.20.26";
    static final public  String connectionRemoteLane2MultipleNode2 = "10.20.20.143";
    static final public  String connectionRemoteLane2MultipleNode3 = "10.20.20.148";
    static final public  String connectionRemoteLane2MultipleNode4 = "10.20.20.146";
    static final public  String connectionRemoteLane2MultipleNode5 = "10.20.20.147";

    private Cluster cluster  = null;
    private Session session  = null;

    CassandraBase(String keyspace, String... connectionPool) {
        PlainTextAuthProvider auth_provider = new PlainTextAuthProvider(USERNAME, PASSWORD);
        Cluster.Builder builder = Cluster.builder().withAuthProvider(auth_provider);

        // In case we use prepared sql instead of regular
        QueryOptions options = new QueryOptions();
//        options.setPrepareOnAllHosts(true);
//        builder.withQueryOptions(options);

        // Add all the ip addresses passed in
        for (String connection : connectionPool)
            builder.addContactPoint(connection);

        // Up the response timeout so we can do count(*) and other 1 min+ actions
        SocketOptions socketOptions = new SocketOptions().setReadTimeoutMillis(120000);
        builder.withSocketOptions(socketOptions);

//        PoolingOptions poolingOptions = new PoolingOptions()
//                .setConnectionsPerHost(HostDistance.LOCAL, 4, 4);
//                .setMaxRequestsPerConnection(HostDistance.LOCAL, 1000);
//        builder.withPoolingOptions(poolingOptions);

        cluster = builder.build();
        session = cluster.connect(keyspace);

        Metadata metadata = cluster.getMetadata();
        for (Host host : metadata.getAllHosts()) {
            General.Debug(
                    "\n Datacenter: " + host.getDatacenter() +
                            "\n  Rack: " + host.getRack() +
                            "\n  Host: " + host.getAddress() +
                            "");
        }
    }

    public Cluster getCluster() {
        return cluster;
    }

    public Session getSession() {
        return session;
    }

    public void close() {
        session.closeAsync();
    }

    public ResultSet executeQuery(String query) {
        if (session == null)
            throw new TestNGException("Must call connect() to create an active session");

        ResultSet resultSet = session.execute(query);
        return resultSet;
    }

    public String safeGetDecimal(Row row, int columnIndex) {
        BigDecimal decimal = row.getDecimal(columnIndex);
        if (decimal == null)
            return "";
        return decimal.toString();
    }




}
















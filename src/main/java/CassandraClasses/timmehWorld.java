
package CassandraClasses;

import Utils.General;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;

/*
 * Created by timothy.hallbeck on 5/3/2016.
 */

public class timmehWorld extends CassandraBase {

    private PreparedStatement preparedAdd = null;

    public timmehWorld(String... connections) {
        super("timmehworld", connections);
    }

    public ResultSet getAllEmployees() {
        return getAllEmployees(false);
    }
    public ResultSet getAllEmployees(boolean showOutput) {
        ResultSet results = executeQuery("Select * from timmehworld.employee");
        if (showOutput)
            listAllRows(results);

        return results;
    }

    public ResultSet getRecordCount() {
        ResultSet results = executeQuery("Select count(*) from timmehworld.employee");
        long count = results.one().getLong(0);
        General.Debug("\nThere are " + count + " rows in timmehworld.employee");

        return results;
    }

    // Note - using the iterator drains the set.
    public void listAllRows(ResultSet resultSet) {
        while (!resultSet.isExhausted()) {
            for (Row row : resultSet) {
                String output = "  Record: (";
                output += new Integer(row.getInt("emp_id")).toString() + ", ";
                output += row.getString("emp_first") + ", ";
                output += row.getString("emp_last") + ", ";
                output += row.getString("emp_dept") + ")";
                General.Debug(output);
            }
            resultSet.fetchMoreResults();
        }
    }

    public ResultSet addRecord(int id, String dept, String first, String last) {
        ResultSet resultSet;

        String query = "INSERT INTO timmehworld.employee (emp_id, emp_dept, emp_first, emp_last) VALUES (";
        query += id + ", ";
        query += "'" + dept + "', ";
        query += "'" + first + "', ";
        query += "'" + last + "')";
        resultSet = executeQuery(query);

        return resultSet;
    }

    public void addNRecords(int count) {
        addNRecords(count, false);
    }
    public void addNRecords(int count, boolean showOutput) {
        String iString;
        for (Integer i=1; i<count; i++) {
            iString = i.toString();
            addRecord(i, "dept" + iString, "first" + iString, "last" + iString);
            if (showOutput)
                General.Debug("Added record " + i);
        }
    }

    public void addRecordN(int number) {
        String iString;
        iString = new Integer(number).toString();
        addRecord(number, "dept" + iString, "first" + iString, "last" + iString);
    }

    public ResultSet getRecordById(int id) {
        String query = "select * from timmehworld.employee where emp_id = " + id;
        ResultSet resultSet = executeQuery(query);

        return resultSet;
    }

    public ResultSet deleteRecordById(int id) {
        String query = "delete from timmehworld.employee where emp_id = " + id;
        ResultSet resultSet = executeQuery(query);

        return resultSet;
    }

    public ResultSet modifyRecordById(int id) {
        String query = "update timmehworld.employee set emp_dept = 'deptupdated', emp_first = 'firstupdated', emp_last = 'lastupdated' where emp_id = " + id;
        ResultSet resultSet = executeQuery(query);

        return resultSet;
    }
}


package QtestPages;

/*
 * Created by timothy.hallbeck on 11/30/2015.
 */

public class qtestXpath {

    public static String IndexPage_BrowseTestCases  = "//*[@id='testspecification_topics']/a";

    public static String TextSpecPage_ExpandTree    = "//*[@id='expand_tree']";
    public static String TextSpecPage_CollapseTree  = "//*[@id='collapse_tree']";

}

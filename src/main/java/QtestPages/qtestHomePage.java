
package QtestPages;

/*
 * Created by timothy.hallbeck on 11/24/2015.
 */

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

public class qtestHomePage extends WebPage {

    private int recentProject = 0;

    public qtestHomePage() {
        super(null, LoginType.QTEST, Browser.Firefox);
    }

    public qtestHomePage load() {
        login();

        return this;
    }

    public qtestHomePage clickMobileProject() {
        getByXpath("//*[@id='clientProjectIcon']").click();
        getByXpath("//*[@id='clientList']/li[1]/ul/li[5]/a[2]").click();
        Sleep(2000);

        return this;
    }

    public qtestHomePage GoToProject(int projectId) {
        recentProject = projectId;
        get("https://qtest.wgu.edu/p/" + String.valueOf(recentProject) + "/portal/project#tab=testplan");

        return this;
    }

    public qtestHomePage clickRunButton() {
        getByXpath("//*[@id='testCaseRunConfig_label']").click();
        return this;
    }

    public qtestHomePage clickTestDesign() {
        getByXpath("//*[@widgetid='working-tab_test-design']").click();

        return this;
    }

    public qtestHomePage ExercisePage(boolean cascade) {
        General.Debug("\nqtestHomePage::ExercisePage(" + cascade + ")");
        load();

        return this;
    }
}

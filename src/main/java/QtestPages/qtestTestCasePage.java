
package QtestPages;

import TestlinkPages.testlinkTestCase;
import Utils.General;
import Utils.wguRobot;

import java.util.ArrayList;

/*
 * Created by timothy.hallbeck on 12/2/2015.
 */

public class qtestTestCasePage extends qtestHomePage {

    public qtestTestCasePage() {
        super();
    }

    public qtestTestCasePage load() {
        super.load();
        clickTestDesign();

        return this;
    }

    public qtestTestCasePage addIosCase(testlinkTestCase testcase) throws Exception {
        rightClick(getByXpath("//*[@id='EditableTreeNode_4']/div[1]/span[2]/span"));
        getByXpath("//*[@id='nav_tree_testdesign_New_text']").click();
        getByXpath("//*[@id='nav_tree_testdesign_New_1_text']").click();
        Sleep(2000);

//        setTitle(testcase.getTitle());
//        setDescription(testcase.getSummary());
//        setPrecondition(testcase.getPreconditions());
        setStepsAndResults(null, null);
        setStepsAndResults(testcase.getSteps(), testcase.getResults());
        clickSave();

        return this;
    }

    public qtestTestCasePage addAndroidCase(testlinkTestCase testcase) {
        rightClick(getByXpath("//*[@id='EditableTreeNode_5']/div[1]/span[2]/span"));
        getByXpath("//*[@id='nav_tree_testdesign_New_text']").click();
        getByXpath("//*[@id='nav_tree_testdesign_New_1_text']").click();

        return this;
    }

    public qtestTestCasePage setTitle(String title) {
        typeByXpath("//*[@id='dijit_form_TextBox_0']", title);

        return this;
    }

    public qtestTestCasePage setAssignedTo(String assignedTo) {

        return this;
    }

    public qtestTestCasePage setDescription(String description) {
        switchToDefaultFrame();
        switchToFrame("propDescriptionId_editorNode_ifr");
        typeByXpath("//*[@id='tinymce']", description);
        switchToDefaultFrame();

        return this;
    }

    public qtestTestCasePage setPrecondition(String precondition) {
        switchToDefaultFrame();
        switchToFrame("propPreconditionId_editorNode_ifr");
        typeByXpath("//*[@id='tinymce']", precondition);
        switchToDefaultFrame();

        return this;
    }

    public qtestTestCasePage setStepsAndResults(ArrayList<String> steps, ArrayList<String> results) throws Exception {
//        if (steps.size() <= 0)
//            return this;

        wguRobot robot = new wguRobot();
        robot.mouseMove(600, 800);
        robot.LeftClickMouse();
        robot.SendKeys("hello");

        getByXpath("//*[@id='testStepGrid']/div[3]/div[2]/div/table/tbody/tr/td[3]").click();
        Sleep(250);
        getByXpath("//*[@class='gridxCellWidget']").sendKeys("hello");
        getByXpath("//*[@class='gridxCellWidget']").click();
        getByXpath("//*[@class='gridxCellWidget']").sendKeys("hello");

        getByXpath("//*[@id='testStepGrid']/div[3]/div[2]/div[2]/table/tbody/tr/td[3]").click();
        getByXpath("//*[@id='testStepGrid']/div[3]/div[2]/div[1]/table/tbody/tr/td[3]").click();
        Sleep(1000);
        typeByXpath("//*[@id='testStepGrid']/div[3]/div[2]/div[1]/table/tbody/tr/td[3]", steps.get(0));
        getByXpath("//*[@id='testStepGrid']/div[3]/div[2]/div/table/tbody/tr/td[4]").click();
        typeByXpath("//*[@id='testStepGrid']/div[3]/div[2]/div[1]/table/tbody/tr/td[4]", results.get(0));

        return this;
    }

    public qtestTestCasePage clickSave() {
        getByXpath("//*[@widgetid='testdesignToolbarSave']").click();

        return this;
    }

    //*[@id="assignedToTestCase"]/div[1]/input
    //*[@id="dojox_form__CheckedMultiSelectItem_0"]

//*[@id="dijit_form_TextBox_1"]

    public qtestTestCasePage ExercisePage(boolean cascade) {
        General.Debug("\nqtestTestCasePage::ExercisePage(" + cascade + ")");
        load();

        return this;
    }

}



package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonCampusInfo;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonServiceMappings;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class CampusService extends JsonCampusInfo implements ServiceBase {

    public CampusService() {
    }

    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public CampusService(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }

    public CampusService(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "campus-service";
    }

    public String getDefaultEndpoint() {
        return "/v1/api/students/{userName}/campusInfo";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        ListOfGets.clear();
        ListOfGets.put("/v1/api/students/{userName}/campus", null);
        ListOfGets.put("/v1/api/students/{userName}/campusInfo", null);
        ListOfGets.put("/v1/api/students/{userName}/supportUrl", null);
    }

}
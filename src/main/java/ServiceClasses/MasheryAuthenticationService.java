
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonServiceMappings;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/19/2016.
 */

public class MasheryAuthenticationService extends JsonUnknown implements ServiceBase {

    public MasheryAuthenticationService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public MasheryAuthenticationService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public MasheryAuthenticationService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "masheryauthn";
    }

    public String getDefaultEndpoint() {
        return "/v1/refreshToken/{refreshToken}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{refreshToken}", "notimplemented");
        login.addMacroAndValue("{accessToken}", "notimplemented");

        ListOfGets.clear();
        ListOfGets.put("/v1/refreshToken/{refreshToken}", null);
        ListOfGets.put("/v1/revokeToken/{accessToken}", null);
    }


}

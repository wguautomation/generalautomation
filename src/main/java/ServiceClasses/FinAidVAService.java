
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonFinAidRequirementArray;
import JsonDataClasses.JsonUnknown;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 10/5/2016.
 */

public class FinAidVAService extends JsonUnknown implements ServiceBase {

    public FinAidVAService() {
    }

    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public FinAidVAService(LoginType login) {
        this(login, AuthType.OSSO);
    }

    public FinAidVAService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "finaid-va";
    }

    public String getDefaultEndpoint() {
        return "/v1/varequirementstatus/pidm/{pidm}/termcode/{termCode}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{termCode}", General.getTermCode(login));

        ListOfGets.clear();
        ListOfGets.put("/v1/varequirementstatus/pidm/{pidm}/termcode/{termCode}", null);
        ListOfGets.put("/v1/varequirementstatus/pidm/{pidm}/aidy/{aidy}", null);
    }

}
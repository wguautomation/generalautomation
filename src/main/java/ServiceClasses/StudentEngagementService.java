
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class StudentEngagementService extends JsonUnknown implements ServiceBase {

    public StudentEngagementService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public StudentEngagementService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public StudentEngagementService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "student-engagement";
    }

    public String getDefaultEndpoint() {
        return "/v1/status/student/{pidm}/startedCourses";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        ListOfGets.clear();

        ListOfGets.put("/v1/status/student/{pidm}/startedCourses", null);
        ListOfGets.put("/v1/status/student/{pidm}/course/{courseVersionID}", null);
        ListOfGets.put("/v2/status/student/{pidm}/course/{courseVersionId}/canEarlyStart", null);

        ListOfGets.put("/v2/status/student/{pidm}/startedCourses", null);
        ListOfGets.put("/v2/status/student/{pidm}/course/{courseVersionID}", null);
        ListOfGets.put("/v0/status/student/{pidm}/course/{courseVersionID}", null);
    }

}

package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class CommunityContentService extends JsonUnknown implements ServiceBase {

    public CommunityContentService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public CommunityContentService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public CommunityContentService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "community-content";
    }

    public String getDefaultEndpoint() {
        return "/v1/successCenter/{courseCode}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{courseVersionId}", General.getCourseVersionId(login));
        login.duplicateMacroSourceDest("{pidm}", "{studentPidm}");

        ListOfGets.clear();
        ListOfGets.put("/v1/userForumActivity/username/{username}", null);
        ListOfGets.put("/v1/userForumActivity/username/{username}/course/{courseCode}", null);
        ListOfGets.put("/v1/successCenter/{courseCode}", null);

    }
}
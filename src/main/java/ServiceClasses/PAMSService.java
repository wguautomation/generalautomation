
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class PAMSService extends JsonUnknown implements ServiceBase {

    public PAMSService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public PAMSService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public PAMSService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "pamsservices";
    }

    public String getDefaultEndpoint() {
        return "/v1/courses/{courseCode}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{courseVersionId}", General.getCourseVersionId(login));

        ListOfGets.clear();
        ListOfGets.put("/v1/competencies/course/{courseVersionId}", null);
        ListOfGets.put("/v1/courses/{courseCode}", null);
        ListOfGets.put("/v1/topics/course/{courseVersionId}", null);
    }

}
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonDegreePlan;
import JsonDataClasses.JsonServiceMappings;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 8/30/2016.
 */

public class DegreePlanService extends JsonDegreePlan implements ServiceBase {

    public DegreePlanService() {}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public DegreePlanService(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }
    public DegreePlanService(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "degreeplan";
    }

    public String getDefaultEndpoint() {
        return "/v1/api/degreeplans/current/pidm/{pidm}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        ListOfGets.clear();
        ListOfGets.put("/v0/api/degreeplans/current/pidm/{pidm}", null);
        ListOfGets.put("/v1/api/degreeplans/current/pidm/{pidm}", null);
    }

}
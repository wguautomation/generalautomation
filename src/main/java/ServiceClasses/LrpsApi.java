
package ServiceClasses;

/*
 * Created by timothy.hallbeck on 10/20/2016.
 */

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

public class LrpsApi extends JsonUnknown implements ServiceBase {

    public LrpsApi() {
    }

    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public LrpsApi(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }

    public LrpsApi(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "lrps-api";
    }

    public String getDefaultEndpoint() {
        return "/v1/user/{username}/provisions/{itemId}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{itemId}", "a7Qa000000001RzEAI");

        General.DebugAndOutputWindow("LrpsApi::LoadMacros  need to find out where {itemId} lives / looks like for these 3 calls to work.");

        ListOfGets.clear();
        ListOfGets.put("/v0/provisions/{itemId}", null);
        ListOfGets.put("/v1/user/{username}/provisions/{itemId}", null);
        ListOfGets.put("/v1/user/{username}/provisions/{itemId}/map", null);
    }

}

package ServiceClasses;

/*
 * Created by timothy.hallbeck on 10/31/2016.
 */

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

public class EoCSurvey extends JsonUnknown implements ServiceBase {

    public EoCSurvey() {
    }

    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public EoCSurvey(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }

    public EoCSurvey(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "eocsurvey";
    }

    public String getDefaultEndpoint() {
        return "/v1/survey/eoc";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{coursecode}", General.getCourseCode(login));
        login.addMacroAndValue("{courseversionid}", General.getCourseVersionId(login));

        ListOfGets.clear();
        ListOfGets.put("/v1/survey/eoc", null);
        ListOfGets.put("/v1/survey/student/{pidm}/course/C707/pre/survey", null);
        ListOfGets.put("/v1/survey/student/{pidm}/course/C707/post/survey", null);
        ListOfGets.put("/v1/survey/student/{pidm}/course/{coursecode}/courseversionid/{courseversionid}/survey", null);
    }

}

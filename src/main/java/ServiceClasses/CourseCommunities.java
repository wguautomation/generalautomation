
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 10/19/2016.
 */

public class CourseCommunities extends JsonUnknown implements ServiceBase {

    public CourseCommunities(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public CourseCommunities(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public CourseCommunities(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "coursecommunities";
    }

    public String getDefaultEndpoint() {
        return "/v2/courseChatterFeed/{courseCode}/student/{username}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        ListOfGets.clear();
        ListOfGets.put("/v2/courseChatterFeed/{courseCode}/student/{username}", null);
        ListOfGets.put("/v1/courseChatterFeed/{courseCode}/student/{username}", null);
        ListOfGets.put("/v1/tipsAndAnnouncements/{courseCode}/student/{username}", null);

        ListOfGets.put("/v0/courseChatterFeed/{courseCode}/student/{username}", null);
        ListOfGets.put("/v0/tipsAndAnnouncements/{courseCode}/student/{username}", null);

    }

}
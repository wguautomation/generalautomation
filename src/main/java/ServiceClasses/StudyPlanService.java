package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.*;
import Utils.General;
import Utils.PingSession;
import org.testng.SkipException;
import org.testng.TestNGException;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/1/2016.
 */

public class StudyPlanService extends JsonStudyPlanArray implements ServiceBase {

    public StudyPlanService() {}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public StudyPlanService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public StudyPlanService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "studyplan";
    }

    public String getDefaultEndpoint() {
        return "/api/courses/{courseCode}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{courseVersionId}", General.getCourseVersionId(login));
        login.addMacroAndValue("{studyplanId}", General.getStudyPlanId(login));

        ListOfGets.clear();
        ListOfGets.put("/api/courses/{courseCode}", null);
        ListOfGets.put("/api/students/{pidm}/courses/ora1", null);
        ListOfGets.put("/api/courses/courseVersionId/pidm/{pidm}", null);

        ListOfGets.put("/api/courses/notes/{username}/{courseVersionId}", null);
        ListOfGets.put("/api/courses/{courseCode}/{courseVersionId}", null);
        ListOfGets.put("/api/courses/lastpublished/{courseVersionId}", null);

        ListOfGets.put("/api/courses/getPublishedStudyPlans", null);
        ListOfGets.put("/v0/api/courses/notes", null);
        ListOfGets.put("/bookmark/{studyPlanId}/{username}", null);

        ListOfGets.put("/mobileLinks/linkRatings/{courseVersionId}", null);
        ListOfGets.put("/useractivity/{username}/{studyplanId}", null);
    }

}
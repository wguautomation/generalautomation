
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class SuccessCenterTileService extends JsonUnknown implements ServiceBase {

    public SuccessCenterTileService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public SuccessCenterTileService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public SuccessCenterTileService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "success-center-tile";
    }

    public String getDefaultEndpoint() {
        return "/v1/tiles/pidm/{pidm}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }



}
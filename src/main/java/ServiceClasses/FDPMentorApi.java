
package ServiceClasses;

/*
 * Created by timothy.hallbeck on 10/19/2016.
 */

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;

import java.util.HashMap;
import java.util.Map;

public class FDPMentorApi extends JsonUnknown implements ServiceBase {

    public FDPMentorApi(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public FDPMentorApi(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public FDPMentorApi(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "fdp-mentor-api";
    }

    public String getDefaultEndpoint() {
        return "/v1/api/mentor/mentor-info/{pidm}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }


}

package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonMentorArray;
import JsonDataClasses.JsonServiceMappings;
import Utils.General;
import Utils.PingSession;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 7/28/2016.
 */

public class MentorService extends JsonMentorArray implements ServiceBase {

    public MentorService() {
    }

    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public MentorService(LoginType login) {
        loadArray(load(login, AuthType.OSSO), "");
    }

    public MentorService(LoginType login, AuthType authType) {
        loadArray(load(login, authType), "");
    }

    public String getAppName() {
        return "mentor";
    }

    public String getDefaultEndpoint() {
        return "/v1/mentors/{username}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        if (!ListOfGets.isEmpty())
            return;
        login.addMacroAndValue("{id}", "a7Qa000000001RzEAI");

        ListOfGets.put("/v0/mentors/{username}", null);
        ListOfGets.put("/v1/mentorPhoto/recordid/{id}", null);
        ListOfGets.put("/v1/mentors/{username}", null);
        ListOfGets.put("/v1/mentorPhotoUrl/{pidm}", null);
        ListOfGets.put("/v1/mentorPhotoHiResUrl/{pidm}", null);
    }

    public void LoadExtraMacros(LoginType login, AuthType auth) {
        if (!ListOfGets.isEmpty())
            return;
        login.addMacroAndValue("{bannerCode}", login.getCourseCode());
        login.addMacroAndValue("{banner_code}", login.getCourseCode());

        ListOfGets.put("/v1/courseMentors/{banner_code}", null);
        ListOfGets.put("/v1/courseMentorsByCourse/{bannerCode}", null);
    }


}

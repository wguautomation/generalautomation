
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonServiceMappings;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/19/2016.
 */

public class OSSOTokenValidationService extends JsonUnknown implements ServiceBase {

    public OSSOTokenValidationService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public OSSOTokenValidationService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public OSSOTokenValidationService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "ossotokenvalidation";
    }

    public String getDefaultEndpoint() {
        return "/v1/isTokenValid";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        ListOfGets.clear();
        ListOfGets.put("/v1/isTokenValid", null);
        ListOfGets.put("/v1/ids", null);
        ListOfGets.put("/v1/attributes", null);
    }
}

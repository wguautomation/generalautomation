
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class StudentEmailService extends JsonUnknown implements ServiceBase {

    public StudentEmailService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public StudentEmailService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public StudentEmailService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "student-email";
    }

    public String getDefaultEndpoint() {
        return "/v1/gmail/students/{username}/count";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        ListOfGets.clear();
        ListOfGets.put("/v1/gmail/students/{username}", null);
        ListOfGets.put("/v1/gmail/students/{username}/count", null);
    }

}
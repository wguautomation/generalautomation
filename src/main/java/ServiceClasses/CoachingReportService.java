
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonServiceMappings;
import JsonDataClasses.JsonUnknown;
import Utils.General;
import Utils.PingSession;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/19/2016.
 */

public class CoachingReportService extends JsonUnknown implements ServiceBase {

    public CoachingReportService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public CoachingReportService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public CoachingReportService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "coachingreport";
    }

    public String getDefaultEndpoint() {
        return "/v3/isInV3ByProgram/pidm/{pidm}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{assessmentId}", General.getAssessmentId(login));
        login.addMacroAndValue("{courseVersionId}", General.getCourseVersionId(login));
        login.addMacroAndValue("{studyPlanId}", General.getStudyPlanId(login));
//        login.addMacroAndValue("{studyPlanType}", General.getStudyPlanType(login));

        ListOfGets.clear();
        ListOfGets.put("/pidm/{pidm}/assessmentid/{assessmentId}/studyplanid/{studyPlanId}", null);
        ListOfGets.put("/v3/clearCache", null);
        ListOfGets.put("/v3/pidm/{pidm}/assessmentid/{assessmentId}/studyplanid/{studyPlanId}", null);

        ListOfGets.put("/v3/pidm/{pidm}/assessmentid/{assessmentId}/studyplanid/{studyPlanId}/studyplantype/{studyPlanType}/byDate", null);
        ListOfGets.put("/v3/pidm/{pidm}/assessmentid/{assessmentId}/studyplanid/{studyPlanId}/byDate", null);
        ListOfGets.put("/v3/pidm/{pidm}/assessmentid/{assessmentId}/cv/{courseVersionId}", null);

        ListOfGets.put("/v3/pidm/{pidm}/assessmentid/{assessmentId}/cv/{courseVersionId}/byDate", null);
        ListOfGets.put("/v3/has/pidm/{pidm}/assessmentid/{assessmentId}", null);
        ListOfGets.put("/v3/has/pidm/{pidm}/courseVersionId/{courseVersionId}", null);

        ListOfGets.put("/v3/has/pidm/{pidm}/courseVersionId/{courseVersionId}/assessmentid/{assessmentId}", null);
        ListOfGets.put("/v3/isInV3ByProgram/pidm/{pidm}", null);
        ListOfGets.put("/v3/has/clearCache", null);

        ListOfGets.put("/v3/pidm/{pidm}/assessmentid/{assessmentId}/studyplanid/{studyPlanId}/studyplantype/{studyPlanType}", null);
        ListOfGets.put("/v2/pidm/{pidm}/assessmentid/{assessmentId}/studyplanid/{studyPlanId}", null);
    }

}

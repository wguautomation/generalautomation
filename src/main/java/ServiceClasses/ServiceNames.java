
package ServiceClasses;

import Utils.General;
import android.os.Debug;
import org.testng.TestNGException;
import org.testng.annotations.DataProvider;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.TreeMap;

/*
 * Created by timothy.hallbeck on 9/16/2016.
 */

public enum ServiceNames {
    AllServices(                     "Apply Action to all Services",       null),
    AcademicActivityService(         "Academic Activity Service",          AcademicActivityService.class),
    AssessmentAppWebService(         "Assessment App Web Service",         AssessmentAppWebService.class),
//    AssessmentsService(              "Assessments Service",                AssessmentsService.class),
//    AuthTokenValid(                  "AuthTokenValid",                     AuthTokenValid.class),
    CampusService(                   "Campus Service",                     CampusService.class),
    CoachingReportService(           "Coaching Report Service",            CoachingReportService.class),
    CohortsService(                  "Cohorts Service",                    CohortsService.class),
    CosBService(                     "CosB Service",                       CosBService.class),
    CommunityContentService(         "Community Content Service",          CommunityContentService.class),
    CourseActivitiesService(         "Course Activities Service",          CourseActivitiesService.class),
    CourseCommunities(               "Course Communities",                 CourseCommunities.class),
    CoursePdf(                       "Course Pdf",                         CoursePdf.class),
    CourseReviewService(             "Course Review Service",              CourseReviewService.class),
    DegreePlanService(               "Degree Plan Service",                DegreePlanService.class),
    DevWindowNotificationService(    "Dev Window Notification Service",    DevWindowNotificationService.class),
    DreamMachineService(             "Dream Machine Service",              DreamMachineService.class),
    EoCSurvey(                       "EoC Survey",                         EoCSurvey.class),
//    EurekaService(                   "Eureka Service",                     EurekaService.class),
    EventPublisherService(           "Event Publisher Service",            EventPublisherService.class),
    FDPCalculatorApi(                "FDP Calculator Api",                 FDPCalculatorApi.class),
    FDPDegreePlanService(            "FDP Degree Plan Service",            FDPDegreePlanService.class),
    FdpGraduationApi(                "FDP Graduation Api",                 FdpGraduationApi.class),
    FDPEnrollmentService(            "FDP Enrollment Service",             FDPEnrollmentService.class),
    FdpStudentInfoApi(               "FDP Student Info Api",               FdpStudentInfoApi.class),
    FieldExperienceService(          "Field Experience Service",           FieldExperienceService.class),
    FinaidService(                   "Fin Aid",                            FinaidService.class),
    FinAidVAService(                 "Fin Aid VA",                         FinAidVAService.class),
    LimitedReleaseVersioningService( "Limited Release Versioning Service", LimitedReleaseVersioningService.class),
    LTIIntegrationService(           "LTI Integration Service",            LTIIntegrationService.class),
    LrpsApi(                         "Lrps Api",                           LrpsApi.class),
    LrpsBasicLtiApi(                 "Lrps Basic Lti Api",                 LrpsBasicLtiApi.class),
    LRTabService(                    "LR Tab Service",                     LRTabService.class),
    MasheryAuthenticationService(    "Mashery Authentication Service",     MasheryAuthenticationService.class),
    MentorActionItems(               "Mentor Action Items",                MentorActionItems.class),
    MentorSchedulingService(         "Mentor Scheduling Service",          MentorSchedulingService.class),
    MentorService(                   "Mentor Service",                     MentorService.class),
    MobileFeedbackService(           "Mobile Feedback Service",            MobileFeedbackService.class),
    MobileTokenService(              "Mobile Token Service",               MobileTokenService.class),
    NewsItemService(                 "NewsItemService Service",            NewsItemService.class),
    OSSOTokenValidationService(      "OSSO Token Validation Service",      OSSOTokenValidationService.class),
    PAMSService(                     "PAMS Service",                       PAMSService.class),
    PersonService(                   "Person Service",                     PersonService.class),
    PortalPromptService(             "Portal Prompt Service",              PortalPromptService.class),
    PortalTourService(               "Portal Tour Service",                PortalTourService.class),
    PreAssessmentReportService(      "Pre Assessment Report Service",      PreAssessmentReportService.class),
    ProgramProgressService(          "Program Progress Service",           ProgramProgressService.class),
    PushNotificationService(         "Push Notification Service",          PushNotificationService.class),
    RatingsService(                  "Ratings Service",                    RatingsService.class),
    SCMService(                      "SCM Service",                        SCMService.class),
    ScorezillaService(               "Scorezilla Service",                 ScorezillaService.class),
    SfOnbaseService(                 "SF Onbase Service",                  SfOnbaseService.class),
    StudentActionItemsService(       "Student Action Items Service",       StudentActionItemsService.class),
    StudentAlertsService(            "Student Alerts Service",             StudentAlertsService.class),
    StudentBannerService(            "Student Banner Service",             StudentBannerService.class),
    StudentCommunitiesService(       "Student Communities Service",        StudentCommunitiesService.class),
    StudentCoursesService(           "Student Courses Service",            StudentCoursesService.class),
    StudentDirectoryService(         "Student Directory Service",          StudentDirectoryService.class),
    StudentEmailService(             "Student Email Service",              StudentEmailService.class),
    StudentEngagementService(        "Student Engagement Service",         StudentEngagementService.class),
    StudentNewsService(              "Student News Service",               StudentNewsService.class),
    StudentScheduleService(          "Student Schedule Service",           StudentScheduleService.class),
    StudyPlanService(                "Study Plan Service",                 StudyPlanService.class),
    SuccessCenterTileService(        "Success Center Tile Service",        SuccessCenterTileService.class),
    TaskStreamLTIService(            "Taskstream LTI Service",             TaskStreamLTIService.class),
    VendomaticService(               "Vendomatic Service",                 VendomaticService.class),
    TuitionService(                  "Tuition Service",                    TuitionService.class),
    WguInformationService(           "WGU Information Service",            WguInformationService.class),
    WguSocialBlogSiteService(        "WGU Social Blog Site Service",       WguSocialBlogSiteService.class),
    ;

    private String displayName;
    private Class theClass;

    private static final TreeMap<String, Class> displayNameMap = new TreeMap<>();

    ServiceNames(String displayName, Class theClass) {
        this.displayName = displayName;
        this.theClass    = theClass;
    }

    static public String[] GetDisplayNames() {
        return GetDisplayNames(true);
    }
    static public String[] GetDisplayNames(boolean ignoreAll) {
        // populate with everything
        if (displayNameMap.size() == 0)
            for (ServiceNames entry : ServiceNames.values())
                displayNameMap.put(entry.displayName, entry.theClass);

        // add or remove the All entry
        if (ignoreAll)
            displayNameMap.remove(AllServices.displayName);
        else
            displayNameMap.put(AllServices.displayName, null);

        return displayNameMap.keySet().toArray(new String[displayNameMap.size()]);
    }

    @DataProvider(parallel = false, name = "ServiceDisplayNames")
    public static Object[][] ServiceDisplayNames() {
        return General.ConvertStringArraytoStringArrayArray(GetDisplayNames(true));
    }

    static public ServiceBase Find(String string) {
        GetDisplayNames();

        // null is not an error. 'All' is a null class
        Class lookup = displayNameMap.get(string);
        if (lookup == null)
            return null;

        ServiceBase service;
        try {
            Constructor<?> ctor = lookup.getConstructor();
            service = (ServiceBase) ctor.newInstance();
        } catch (Exception e) {
            return null;
        }

        return service;
    }

}
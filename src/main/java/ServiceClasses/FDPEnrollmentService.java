
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class FDPEnrollmentService extends JsonUnknown implements ServiceBase {

    public FDPEnrollmentService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public FDPEnrollmentService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public FDPEnrollmentService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "fdp-enrollment-api";
    }

    public String getDefaultEndpoint() {
        return "/v1/api/enrollment/{pidm}/enrollment-model";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }


}
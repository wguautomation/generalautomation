
package ServiceClasses;

/*
 * Created by timothy.hallbeck on 10/19/2016.
 */

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

public class MentorActionItems extends JsonUnknown implements ServiceBase {

    public MentorActionItems(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public MentorActionItems(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public MentorActionItems(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "mentor-actionitems";
    }

    public String getDefaultEndpoint() {
        return "/pidm/{pidm}/mentor-items";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        // need to get the id from the default endpoint after creating jsondata class for it
        login.addMacroAndValue("{id}", "a7Qa000000001RzEAI");

        ListOfGets.clear();
        ListOfGets.put("/pidm/{pidm}/mentor-items", null);
        ListOfGets.put("/pidm/{pidm}/mentor-items/{id}", null);
        ListOfGets.put("/v0/actionitem", null);
    }

}
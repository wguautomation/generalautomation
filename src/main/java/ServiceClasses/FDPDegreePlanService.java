
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class FDPDegreePlanService extends JsonUnknown implements ServiceBase {

    public FDPDegreePlanService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public FDPDegreePlanService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public FDPDegreePlanService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "fdp-degreeplan-api";
    }

    public String getDefaultEndpoint() {
        return "/v1/api/degreeplans/current/pidm/{pidm}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{mentorPidm}", General.getMentorPidm(login));

        ListOfGets.clear();
        ListOfGets.put("/v0/api/degreeplans/current/pidm/{pidm}", null);
        ListOfGets.put("/v1/api/degreeplans/current/term/pidm/{pidm}", null);
        ListOfGets.put("/v1/api/students/{pidm}/degreeplans/exists/", null);

        ListOfGets.put("/v1/api/degreeplans/current/pidm/{pidm}", null);
        ListOfGets.put("/v1/api/students/{pidm}/degreeplans/oneplan/", null);
        ListOfGets.put("/v1/api/students/{pidm}/degreeplans", null);

        ListOfGets.put("/v1/api/students/{pidm}/degreeplans-with-fieldexperience", null);
        ListOfGets.put("/v1/api/students/{pidm}/{mentorPidm}/cachebuster", null);
        ListOfGets.put("/v1/api/students/{pidm}/degreeplans/message", null);

        ListOfGets.put("/v1/api/students/{pidm}/degreeplans-legacy", null);
    }
    
}

package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class FieldExperienceService extends JsonUnknown implements ServiceBase {

    public FieldExperienceService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public FieldExperienceService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public FieldExperienceService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "field-experience-api";
    }

    public String getDefaultEndpoint() {
        return "/v1/students/{pidm}/experience";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        ListOfGets.clear();
        ListOfGets.put("/v1/students/{pidm}/experience/raw", null);
        ListOfGets.put("/v1/students/{pidm}/placement-specialist", null);
        ListOfGets.put("/v1/students/{pidm}/placements", null);

        ListOfGets.put("/v1/students/{pidm}/experience", null);
        ListOfGets.put("/v2/students/{pidm}/experience/raw", null);
        ListOfGets.put("/v2/students/{pidm}/placement-specialist", null);

        ListOfGets.put("/v2/students/{pidm}/placements", null);
        ListOfGets.put("/v2/students/{pidm}/requirements", null);
        ListOfGets.put("/v2/students/{pidm}/experience", null);
    }

}

package ServiceClasses;

/*
 * Created by timothy.hallbeck on 10/19/2016.
 */

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

public class FinaidService extends JsonUnknown implements ServiceBase {

    public FinaidService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public FinaidService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public FinaidService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "finaid";
    }

    public String getDefaultEndpoint() {
        return "/v1/finaid/summary/{pidm}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{termCode}", General.getTermCode(login));
        login.addMacroAndValue("{requirement}", "DSPC16");
        login.addMacroAndValue("{aidYear}", "2015");
        login.addMacroAndValue("{dateSinceEpoch}", "1417000000");

        ListOfGets.clear();
        ListOfGets.put("/v2/varequirements/pidm/{pidm}/termcode/{termCode}", null);
        ListOfGets.put("/v2/vastatus/pidm/{pidm}/termcode/{termCode}", null);
        ListOfGets.put("/v2/docs/cumulativeaccountstatement/{pidm}", null);

        ListOfGets.put("/v1/finaid/summary/{pidm}", null);
        ListOfGets.put("/v1/finaid/sap/{pidm}", null);
        ListOfGets.put("/v2/finaid/summary/{pidm}", null);

        ListOfGets.put("/v2/finaid/termcodesanddates/{pidm}", null);
        ListOfGets.put("/v2/finaid/termcodesanddates/{pidm}/finaidreqs", null);
        ListOfGets.put("/v2/finaid/termcodesanddates/{pidm}/va", null);

        ListOfGets.put("/v2/finaid/termcodesanddates/{pidm}/myaccount", null);
        ListOfGets.put("/v2/finaid/links/toolbox", null);
        ListOfGets.put("/v2/finaid/links/va", null);

        ListOfGets.put("/v2/awardsdisbursments/pidm/{pidm}/termcode/{termCode}", null);
        ListOfGets.put("/v2/finaidbalancedue/pidm/{pidm}", null);
        ListOfGets.put("/v2/finaidbalancedue/pidm/{pidm}/termcode/{termCode}", null);

        ListOfGets.put("/v2/myaccount/pidm/{pidm}/termcode/{termCode}", null);
        ListOfGets.put("/v2/cumulativeaccount/pidm/{pidm}", null);
        ListOfGets.put("/v2/finaidstatus/pidm/{pidm}/termcode/{termCode}", null);

        ListOfGets.put("/v2/fafsarequired/pidm/{pidm}/termcode/{termCode}", null);
        ListOfGets.put("/v2/programstatus/pidm/{pidm}/termcode/{termCode}", null);
        ListOfGets.put("/v2/financialaidrequirement/pidm/{pidm}/termcode/{termCode}/req/{requirement}", null);

        ListOfGets.put("/v2/financialaidrequirements/pidm/{pidm}/termcode/{termCode}", null);
        ListOfGets.put("/v2/t1098status/pidm/{pidm}", null);
        ListOfGets.put("/v2/t1098/timer/date/{dateSinceEpoch}", null);

        ListOfGets.put("/forms/financialaidplan/pidm/{pidm}/aidyear/{aidYear}", null);
    }

}
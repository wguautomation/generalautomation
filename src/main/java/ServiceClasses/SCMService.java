
package ServiceClasses;

/*
 * Created by timothy.hallbeck on 10/31/2016.
 */

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;

import java.util.HashMap;
import java.util.Map;

public class SCMService extends JsonUnknown implements ServiceBase {

    public SCMService() {
    }

    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public SCMService(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }

    public SCMService(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "scm";
    }

    public String getDefaultEndpoint() {
        return "/healthcheck/heartbeat";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
//        login.addMacroAndValue("{location}", "1");

        ListOfGets.clear();
        ListOfGets.put("/healthcheck/heartbeat", null);
    }

}

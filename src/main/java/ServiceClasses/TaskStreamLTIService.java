
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonServiceMappings;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/19/2016.
 */

public class TaskStreamLTIService extends JsonUnknown implements ServiceBase {

    public TaskStreamLTIService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public TaskStreamLTIService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public TaskStreamLTIService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "taskstreamlti";
    }

    public String getDefaultEndpoint() {
        return "/v1/api/taskstream/jobs/autoapprovals";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }


}

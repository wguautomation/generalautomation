
package ServiceClasses;

/*
 * Created by timothy.hallbeck on 11/2/2016.
 */

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

public class VendomaticService extends JsonUnknown implements ServiceBase {

    public VendomaticService() {
    }

    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public VendomaticService(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }

    public VendomaticService(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "vendomatic";
    }

    public String getDefaultEndpoint() {
        return "/v1/global/list";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{assessmentId}", General.getAssessmentId(login));
        login.addMacroAndValue("{ssoId}", "22");
        login.addMacroAndValue("{size}", "1");
        login.addMacroAndValue("{id}", "1");

        ListOfGets.clear();
        ListOfGets.put("/v1/global/list", null);
        ListOfGets.put("/v1/provision/debug/pidm/{pidm}/assessmentId/{assessmentId}", null);
        ListOfGets.put("/v1/provision/pidm/{pidm}/assessmentId/{assessmentId}", null);
        ListOfGets.put("/v1/provision/debug/student/{pidm}/sso/{ssoId}", null);
        ListOfGets.put("/v1/provision/student/{pidm}/sso/{ssoId}", null);
        ListOfGets.put("/v1/vendor/generate/passphrase/{size}", null);
        ListOfGets.put("/v1/vendor/id/{id}", null);
        ListOfGets.put("/v1/vendor/list", null);
        ListOfGets.put("/v1/assessment/list", null);
        ListOfGets.put("/v1/assessment/id/{assessmentId}", null);
        ListOfGets.put("/v1/sso/ssotype/list", null);
        ListOfGets.put("/v1/sso/list", null);
        ListOfGets.put("/v1/sso/id/{id}", null);
    }

}

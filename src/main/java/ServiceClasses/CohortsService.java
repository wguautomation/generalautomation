
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class CohortsService extends JsonUnknown implements ServiceBase {

    public CohortsService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public CohortsService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public CohortsService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "cohorts";
    }

    public String getDefaultEndpoint() {
        return "/v0/cohorts/course/{courseVersionId}/{pidm}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{mentorPidm}", General.getMentorPidm(login));
        login.addMacroAndValue("{courseVersionId}", General.getCourseVersionId(login));
        login.duplicateMacroSourceDest("{pidm}", "{studentPidm}");

        ListOfGets.clear();
        ListOfGets.put("/v0/cohorts/course/{courseVersionId}/{pidm}", null);
        ListOfGets.put("/v0/cohorts/course/{mentorPidm}/{studentPidm}/{courseVersionId}", null);
        ListOfGets.put("/v1/pidm/{pidm}/courses/{courseVersionId}", null);

    }

}
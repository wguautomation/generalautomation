
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonServiceMappings;
import Utils.General;
import Utils.MasherySession;
import Utils.OssoSession;
import Utils.PingSession;
import org.testng.TestNGException;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 8/10/2016.
 */

public interface ServiceBase {

    PingSession ping = new PingSession();
    OssoSession osso = new OssoSession();
    MasherySession mashery = new MasherySession();

    Map<String, JsonData> getListOfGets();
    String getDefaultEndpoint();
    String getAppName();

    default void Reset() {
        getListOfGets().clear();
    }

    default void CheckEndpointUrls(LoginType login, AuthType auth) {
        if (getListOfGets() == null) {
            General.DebugAndOutputWindow(getAppName() + "::CheckEndpointUrls() not implemented");
            return;
        }

        JsonServiceMappings mappings = GetMappings(login);
        for (String url : getListOfGets().keySet())
            mappings.verifyGet(url);
    }

    default void ExerciseEndpoints(LoginType login, AuthType auth) {
        General.DebugAndOutputWindow(getAppName() + "::ExerciseEndpoints()");
        LoadExtraMacros(login, auth);

        if (getListOfGets().isEmpty()) {
            QuickLoad(login, auth);
            return;
        }

        String[] fullCommand = new String[1];
        String result = "";
        for (String partialUrl : getListOfGets().keySet()) {
            JsonData data = getListOfGets().get(partialUrl);

            General.SendTextToOutputWindow("\n" + partialUrl);
            switch (auth) {
                case PING:
                    result = ping.GetEndpoint(partialUrl, fullCommand);
                    break;
                case OSSO:
                    result = osso.Get(getAppName(), partialUrl, login, fullCommand);
                    break;
                case MASHERY:
                    result = mashery.Get(partialUrl, login, fullCommand);
                    break;
            }

            General.SendTextToOutputWindow(fullCommand[0]);
            General.DebugAndOutputWindow(result);

            try {
                if (data != null)
                    data.loadObject(result);
            } catch (Exception e) {
                General.DebugAndOutputWindow("endpoint Get failed with " + e.getMessage());
            }

            General.Sleep(100);
        }

    }

    default void QuickLoad(LoginType login, AuthType auth) {
        String[] url = new String[1];
        String result = load(login, auth, url);
        General.Debug(result + "\n");
        General.SendTextToOutputWindow(url[0]);
        General.SendTextToOutputWindow(result);
    }

    default void UnitTest(LoginType login, AuthType auth) {
        General.Debug(getAppName());
        GetMappings(login);
        CheckEndpointUrls(login, auth);
    }

    default void FuncionalTest(LoginType login, AuthType auth) {
        ExerciseEndpoints(login, auth);
    }

    default JsonServiceMappings GetMappings(LoginType login) {
        ping.load(getAppName(), login, PingSession.ServiceKey.MAPPINGS);
        return ping.getMappings();
    }

    default boolean CheckForServiceNotFound(String result) {
        return result.toLowerCase().contains("596 service not found");
    }

    default void LoadExtraMacros(LoginType login, AuthType auth) {
        ;
    }

    default void LoadMacros(LoginType login, AuthType auth) {
        ;
    }

    default String load(LoginType login, AuthType authType) {
        return load(login, authType, new String[1]);
    }

    default String load(LoginType login, AuthType authType, String[] fullCommand) {
        General.Debug(getAppName() + "::load()");
        String command, result;

        LoadMacros(login, authType);
        switch (authType) {
            case PING:
                ping.load(getAppName(), login, PingSession.ServiceKey.NULL);
                result = ping.GetEndpoint(getDefaultEndpoint(), fullCommand);
                break;
            case OSSO:
                result = osso.Get(getAppName(), getDefaultEndpoint(), login, fullCommand);
                break;
            case MASHERY:
                command = getAppName() + getDefaultEndpoint();
                result = mashery.Get(command, login, fullCommand);
                if (CheckForServiceNotFound(result)) {
                    General.DebugAndOutputWindow("Mashery not enabled for " + getAppName());
                    return "";
                }
                break;
            default:
                throw new TestNGException("Unimplemented authType in " + getAppName() + "::load() " + authType);
        }
        return result;
    }

    default String StandardEndpointCheck(String url, AuthType auth, boolean bailIfFail) {
        String result = "";

        General.DebugAndOutputWindow(url);
        try {
            if (auth == AuthType.PING) {
                String[] expandedCommand = new String[1];
                result = ping.GetEndpoint(url, expandedCommand);
                General.DebugAndOutputWindow(expandedCommand[0]);
                General.DebugAndOutputWindow(result + "\n");
            }
        } catch (Exception e) {
            General.SendTextToOutputWindow(e.getMessage());
            if (bailIfFail)
                throw new TestNGException(url + " failed, skipping rest of test");
        }

        return result;
    }


}

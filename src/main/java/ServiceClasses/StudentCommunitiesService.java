
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 10/31/2016.
 */

public class StudentCommunitiesService extends JsonUnknown implements ServiceBase {

    public StudentCommunitiesService() {
    }

    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public StudentCommunitiesService(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }

    public StudentCommunitiesService(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "student-communities";
    }

    public String getDefaultEndpoint() {
        return "/v1/communities/{pidm}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
//        login.addMacroAndValue("{bannerId}", General.getBannerId(login));

        ListOfGets.clear();
        ListOfGets.put("/v1/communities/{pidm}", null);
    }

}

package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 10/19/2016.
 */

public class FdpGraduationApi extends JsonUnknown implements ServiceBase {

    public FdpGraduationApi(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public FdpGraduationApi(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public FdpGraduationApi(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "fdp-graduation-api";
    }

    public String getDefaultEndpoint() {
        return "/v1/api/graduation/graduationdate/{pidm}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }


}
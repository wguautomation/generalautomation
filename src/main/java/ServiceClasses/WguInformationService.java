
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonServiceMappings;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/19/2016.
 */

public class WguInformationService extends JsonUnknown implements ServiceBase {

    public WguInformationService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public WguInformationService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public WguInformationService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "wguinformation";
    }

    public String getDefaultEndpoint() {
        return "/content";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{type}", "xxxx");

        ListOfGets.clear();
        ListOfGets.put("/content", null);
        ListOfGets.put("/content/{type}", null);
        ListOfGets.put("/contactInformation/lastUpdated", null);

        ListOfGets.put("/contactInformation", null);
        ListOfGets.put("/resources", null);
        ListOfGets.put("/resources/lastUpdated", null);

        ListOfGets.put("/faqs", null);
        ListOfGets.put("/faqs/lastUpdated", null);

    }


}


package ServiceClasses;

/*
 * Created by timothy.hallbeck on 10/19/2016.
 */

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

public class CoursePdf extends JsonUnknown implements ServiceBase {

    public CoursePdf(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public CoursePdf(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public CoursePdf(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "coursepdf";
    }

    public String getDefaultEndpoint() {
        return "/user/{username}/pdf/{coursecode}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.duplicateMacroSourceDest("{courseCode}", "{coursecode}");

        ListOfGets.clear();
        ListOfGets.put("/user/{username}/pdf/{coursecode}", null);
    }


}
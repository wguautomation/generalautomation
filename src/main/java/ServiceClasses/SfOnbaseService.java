
package ServiceClasses;

/*
 * Created by timothy.hallbeck on 10/20/2016.
 */

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;

import java.util.HashMap;
import java.util.Map;

public class SfOnbaseService extends JsonUnknown implements ServiceBase {

    public SfOnbaseService() {
    }

    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public SfOnbaseService(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }

    public SfOnbaseService(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "sfonbase";
    }

    public String getDefaultEndpoint() {
        return "/document/list/{pidm}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{docid}", "10234745");

        ListOfGets.clear();
        ListOfGets.put("/document/list/{pidm}", null);
        ListOfGets.put("/document/download/{pidm}/docid/{docid}", null);
    }

}
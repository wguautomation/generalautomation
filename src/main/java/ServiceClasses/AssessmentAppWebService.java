
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonServiceMappings;
import JsonDataClasses.JsonUnknown;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class AssessmentAppWebService extends JsonUnknown implements ServiceBase {

    public AssessmentAppWebService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public AssessmentAppWebService(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }
    public AssessmentAppWebService(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "arp";
    }

    public String getDefaultEndpoint() {
        return "/v1/pending/assessment/list";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        ListOfGets.clear();
        ListOfGets.put("/v2/pending/assessment/list", null);
        ListOfGets.put("/v2/pending/assessment/id/{id}", null);
        ListOfGets.put("/v2/assessment/pending/{id}", null);

        ListOfGets.put("/v2/grader/refresh", null);
        ListOfGets.put("/v2/grader/list", null);
        ListOfGets.put("/v2/vendor/list", null);

        ListOfGets.put("/v2/student/list", null);
        ListOfGets.put("/v2/assessment/code/{code}/list", null);
        ListOfGets.put("/v1/vendor/list", null);

        ListOfGets.put("/v1/grader/refresh", null);
        ListOfGets.put("/v1/grader/list", null);
        ListOfGets.put("/v1/student/list", null);
    }


}

package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class StudentDirectoryService extends JsonUnknown implements ServiceBase {

    public StudentDirectoryService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public StudentDirectoryService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public StudentDirectoryService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "student-directory";
    }

    public String getDefaultEndpoint() {
        return "/v1/studentDirectorySearch/suggestionTag/colleges";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        ListOfGets.clear();
        ListOfGets.put("/v1/studentDirectorySearch/suggestionTag/programCodes", null);
        ListOfGets.put("/v1/studentDirectorySearch/suggestionTag/tagTypes", null);
        ListOfGets.put("/v1/studentDirectorySearch/suggestionTag/colleges", null);

        ListOfGets.put("/v1/studentDirectorySearch/suggestionTag/programs", null);
        ListOfGets.put("/v1/studentDirectorySearch/suggestionTag/programTitles", null);
        ListOfGets.put("/v1/studentDirectorySearch/suggestionTag/courseCodes", null);

        ListOfGets.put("/v1/studentDirectorySearch/suggestionTag/courseTitles", null);
        ListOfGets.put("/v1/studentDirectorySearch/suggestionTag/courses", null);
        ListOfGets.put("/v0/studentConnection/student/{pidm}/connection", null);

        ListOfGets.put("/v1/studentDirectorySearch/{pidm}/lastSearch", null);
        ListOfGets.put("/v1/studentConnection/student/{pidm}/connection", null);
        ListOfGets.put("/v1/studentDirectorySearch/filterTags", null);
    }

}
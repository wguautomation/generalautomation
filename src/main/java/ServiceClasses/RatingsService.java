
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class RatingsService extends JsonUnknown implements ServiceBase {

    public RatingsService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public RatingsService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public RatingsService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "ratings";
    }

    public String getDefaultEndpoint() {
        return "/v1/ratemycompetency/students/{pidm}/courses/{courseVersionId}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{courseVersionId}", General.getCourseVersionId(login));
        login.addMacroAndValue("{competencyId}", General.getCompetencyId(login));

        ListOfGets.clear();
        ListOfGets.put("/v0/ratemycompetency/pidm/{pidm}/courseVersionId/{courseVersionId}/competencyId/{competencyId}", null);
        ListOfGets.put("/v1/ratemycompetency/students/{pidm}/courses/{courseVersionId}/ratings", null);
        ListOfGets.put("/v1/ratemycompetency/students/{pidm}/courses/{courseVersionId}", null);

    }


}
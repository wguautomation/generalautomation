
package ServiceClasses;

/*
 * Created by timothy.hallbeck on 10/20/2016.
 */

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;

import java.util.HashMap;
import java.util.Map;

public class LRTabService extends JsonUnknown implements ServiceBase {

    public LRTabService() {
    }

    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public LRTabService(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }

    public LRTabService(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "lrtab";
    }

    public String getDefaultEndpoint() {
        return "/api/v1/lrs/student/{pidm}/course/{courseCode}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        ListOfGets.clear();
        ListOfGets.put("/api/v0/lrs/student/{pidm}/course/{courseCode}", null);
        ListOfGets.put("/api/v1/lrs/student/{pidm}/course/{courseCode}", null);
    }

}

package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class DevWindowNotificationService extends JsonUnknown implements ServiceBase {

    public DevWindowNotificationService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public DevWindowNotificationService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public DevWindowNotificationService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "deploymentwindow";
    }

    public String getDefaultEndpoint() {
        return "/v1/api/service/{serviceName}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{serviceName}", "courseactivities");

        ListOfGets.clear();
        ListOfGets.put("/v1/api/service/{serviceName}", null);
    }

}
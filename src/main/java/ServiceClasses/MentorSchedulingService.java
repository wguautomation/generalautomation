
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class MentorSchedulingService extends JsonUnknown implements ServiceBase {

    public MentorSchedulingService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public MentorSchedulingService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public MentorSchedulingService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "mentorscheduling";
    }

    public String getDefaultEndpoint() {
        return "/schedule/{pidm}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        ListOfGets.clear();
        ListOfGets.put("/schedule/{pidm}", null);
        ListOfGets.put("/successCenter/{courseCode}/schedule/{pidm}", null);
    }


}

package ServiceClasses;

/*
 * Created by timothy.hallbeck on 10/20/2016.
 */

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

public class LrpsBasicLtiApi extends JsonUnknown implements ServiceBase {

    public LrpsBasicLtiApi() {
    }

    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public LrpsBasicLtiApi(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }

    public LrpsBasicLtiApi(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "lrps-basic-lti-api";
    }

    public String getDefaultEndpoint() {
        return "/v1/api/lti/user/{username}/resource/{resourceId}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{resourceId}", "a7Qa000000001RzEAI");

        General.DebugAndOutputWindow("LrpsBasicLtiApi::LoadMacros  need to find out where {resourceId} lives / looks like for this call to work.");

        ListOfGets.clear();
        ListOfGets.put("/v1/api/lti/user/{username}/resource/{resourceId}", null);
    }

}
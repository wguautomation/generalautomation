
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonAssessmentArray;
import JsonDataClasses.JsonCourseArray;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonServiceMappings;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/19/2016.
 */

public class CosBService extends JsonCourseArray implements ServiceBase {

    public CosBService() {
    }
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public CosBService(LoginType login) {
        loadArray(load(login, AuthType.OSSO), arrayKey);
    }
    public CosBService(LoginType login, AuthType authType) {
        loadArray(load(login, authType), arrayKey);
    }

    public String getAppName() {
        return "cosbservices";
    }

    public String getDefaultEndpoint() {
        return "/v1/courses/info/{courseVersionId}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{courseVersionId}", General.getCourseVersionId(login));

        ListOfGets.clear();
        ListOfGets.put("/v1/courses/{courseVersionId}", null);
        ListOfGets.put("/v1/courses/info/{courseVersionId}", null);
        ListOfGets.put("/v2/courses/{courseVersionId}", null);

    }

}


package ServiceClasses;

/*
 * Created by timothy.hallbeck on 10/31/2016.
 */

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;

import java.util.HashMap;
import java.util.Map;

public class PortalTourService extends JsonUnknown implements ServiceBase {

    public PortalTourService() {
    }

    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public PortalTourService(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }

    public PortalTourService(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "portaltour";
    }

    public String getDefaultEndpoint() {
        return "/v1/currentPortalTours";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{tourName}", "tournamehere");
        login.addMacroAndValue("{version}", "1.0.0");
        login.addMacroAndValue("{tourVersionId}", "1");

        ListOfGets.clear();
        ListOfGets.put("/v1/currentPortalTours", null);
        ListOfGets.put("/v1/portalTour/{tourName}/version/{version}", null);
        ListOfGets.put("/v1/portalTour/{tourName}", null);
        ListOfGets.put("/v0/portalTour/{tourName}", null);
    }

}

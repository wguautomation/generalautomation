
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class LTIIntegrationService extends JsonUnknown implements ServiceBase {

    public LTIIntegrationService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public LTIIntegrationService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public LTIIntegrationService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "ltim";
    }

    public String getDefaultEndpoint() {
        return "/api-docs";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }


}
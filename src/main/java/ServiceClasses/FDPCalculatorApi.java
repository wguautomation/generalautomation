
package ServiceClasses;

/*
 * Created by timothy.hallbeck on 10/19/2016.
 */

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;

import java.util.HashMap;
import java.util.Map;

public class FDPCalculatorApi extends JsonUnknown implements ServiceBase {

    public FDPCalculatorApi(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public FDPCalculatorApi(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public FDPCalculatorApi(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "fdp-calculator-api";
    }

    public String getDefaultEndpoint() {
        return "/v1/api/calculator/{studentPidm}/tuitions";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.duplicateMacroSourceDest("{pidm}", "{studentPidm}");

        ListOfGets.clear();
        ListOfGets.put("/v1/api/calculator/{studentPidm}/tuitions", null);
    }

}
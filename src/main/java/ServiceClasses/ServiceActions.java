
package ServiceClasses;

import org.testng.TestNGException;

/*
 * Created by timothy.hallbeck on 9/15/2016.
 */

public enum ServiceActions {
    EXERCISE_ENDPOINTS,
    RETURN_TOKEN,
    SHOW_ENDPOINTS,
    VERSION_INFO,
    SERVICE_MONITOR,
    ;

    static private String[] DisplayNames = {
            "Return an auth token",
            "Show all Endpoints",
            "Exercise Endpoints",
            "Get Version Info",
            "Create Service Monitor",
    };

    static public String[] GetDisplayNames() {
        return DisplayNames;
    }

    static public ServiceActions Find(String string) {
        switch (string) {
            case "Exercise Endpoints":
                return EXERCISE_ENDPOINTS;

            case "Return an auth token":
                return RETURN_TOKEN;

            case "Show all Endpoints":
                return SHOW_ENDPOINTS;

            case "Get Version Info":
                return VERSION_INFO;

            case "Create Service Monitor":
                return SERVICE_MONITOR;

            default:
                throw new TestNGException("Unknown service action in ServiceActions()::Find  " + string);
        }

    };
}

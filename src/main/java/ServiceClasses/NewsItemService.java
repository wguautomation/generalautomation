
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonNewsItemArray;
import JsonDataClasses.JsonServiceMappings;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/19/2016.
 */

public class NewsItemService extends JsonNewsItemArray implements ServiceBase {

    public NewsItemService() {
    }

    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public NewsItemService(LoginType login) {
        load(login, AuthType.OSSO);
    }

    public NewsItemService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "newsitems";
    }

    public String getDefaultEndpoint() {
        return "/v0/api/feed/{pidm}/{timestamp}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{timestamp}", "1417000000");

        ListOfGets.clear();
        ListOfGets.put("/v0/api/feed/{pidm}/{timestamp}", null);
        ListOfGets.put("/v1/api/feed/{pidm}/{timestamp}", null);
    }

}

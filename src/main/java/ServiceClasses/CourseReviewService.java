
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class CourseReviewService extends JsonUnknown implements ServiceBase {

    public CourseReviewService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public CourseReviewService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public CourseReviewService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "coursereview";
    }

    public String getDefaultEndpoint() {
        return "/unknown";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }


}

package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonIdentity;
import JsonDataClasses.JsonPerson;
import Utils.General;
import org.testng.TestNGException;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 7/28/2016.
 */

public class PersonService extends JsonPerson implements ServiceBase {

    public PersonService() {}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public PersonService(LoginType login) {
        this(login, AuthType.OSSO);
    }
    public PersonService(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "person";
    }

    public String getDefaultEndpoint() {
        return "/v1/person";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadExtraMacros(LoginType login, AuthType auth) {
        JsonIdentity identity = new JsonIdentity();

        login.addMacroAndValue("{bannerId}", General.getBannerId(login));

        ListOfGets.clear();
        ListOfGets.put("/v1/person", this);
        ListOfGets.put("/v1/person/pidm/{pidm}", this);
        ListOfGets.put("/v1/person/pidm/{pidm}/identity", identity);
        ListOfGets.put("/v1/person/pidm/{pidm}/type", null);
        ListOfGets.put("/v1/person/pidm/{pidm}/refresh", null);

        ListOfGets.put("/v1/person/bannerId/{bannerId}", this);
        ListOfGets.put("/v1/person/bannerId/{bannerId}/identity", identity);
        ListOfGets.put("/v1/person/bannerId/{bannerId}/type", null);

        ListOfGets.put("/v1/person/{username}", this);
        ListOfGets.put("/v1/person/{username}/type", null);
        ListOfGets.put("/v1/person/{username}/identity", identity);
        ListOfGets.put("/v1/person/{username}/roles", null);
        ListOfGets.put("/v1/person/{username}/roles/refresh", null);
        ListOfGets.put("/v1/person/username/{username}/refresh", null);
    }

    public void ExerciseEndpoints(LoginType login, AuthType auth) {
        General.Debug("PersonService::ExercisePingEndpoints()");
        String result;

        if (auth != AuthType.PING) {
            ServiceBase.super.ExerciseEndpoints(login, auth);
            return;
        }

        LoadExtraMacros(login, AuthType.PING);

        result = StandardEndpointCheck("/v1/person/pidm/{pidm}", AuthType.PING, true);
        General.Debug("Creating a new JsonPerson");
        JsonPerson person = new JsonPerson(result);
        String bannerId = person.getStudentId();

        result = StandardEndpointCheck("/v1/person/pidm/{pidm}/identity", AuthType.PING, true);
        General.Debug("Creating a new JsonIdentity");
        JsonIdentity identity = new JsonIdentity(result);

        result = StandardEndpointCheck("/v1/person/pidm/{pidm}/type", AuthType.PING, true);
        VerfifyPersonType(result);

        result = StandardEndpointCheck("/v1/person/pidm/{pidm}/refresh", AuthType.PING, false);
        General.DebugAndOutputWindow("Refresh call does not return anything testable");

        result = StandardEndpointCheck("/v1/person/{username}", AuthType.PING, true);
        General.Debug("Creating a new JsonPerson");
        person = new JsonPerson(result);

        result = StandardEndpointCheck("/v1/person/{username}/type", AuthType.PING, true);
        VerfifyPersonType(result);

        result = StandardEndpointCheck("/v1/person/{username}/identity", AuthType.PING, true);
        General.Debug("Creating a new JsonIdentity");
        identity = new JsonIdentity(result);

        result = StandardEndpointCheck("/v1/person/bannerId/{bannerId}", AuthType.PING, true);
        person = new JsonPerson(result);

        result = StandardEndpointCheck("/v1/person/bannerId/{bannerId}/identity", AuthType.PING, true);
        identity = new JsonIdentity(result);

        result = StandardEndpointCheck("/v1/person/bannerId/{bannerId}/type", AuthType.PING, true);
        VerfifyPersonType(result);

        General.SendTextToOutputWindow("Not tested: /v1/person/{username}/roles");
        General.SendTextToOutputWindow("Not tested: /v1/person/{username}/roles/refresh\n");

        result = StandardEndpointCheck("/v1/person/username/{username}/refresh", AuthType.PING, false);
        General.DebugAndOutputWindow("Refresh call does not return anything testable");
    }

    public String VerfifyPersonType(String result) {
        String output;

        General.Debug("PersonService::VerfifyPersonType()");
        if (result.toLowerCase().contains("student"))
            output = "This person is a student";
        else if (result.toLowerCase().contains("mentor"))
            output = "This person is a mentor";
        else if (result.toLowerCase().contains("employee"))
            output = "This person is an employee";
        else
            throw new TestNGException("Unknown person type in PersonService::VerfifyPersonType() " + result);

        General.Debug(output);
        return output;
    }

}
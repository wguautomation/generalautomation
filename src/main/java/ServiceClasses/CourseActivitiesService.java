
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonServiceMappings;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/19/2016.
 */

public class CourseActivitiesService extends JsonUnknown implements ServiceBase {

    public CourseActivitiesService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public CourseActivitiesService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public CourseActivitiesService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "courseactivities";
    }

    public String getDefaultEndpoint() {
        return "/v2/activity/user/{username}/studyplan/{studyplanId}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{studyplanId}", General.getStudyPlanId(login));

        ListOfGets.clear();
        ListOfGets.put("/v2/activity/user/{username}/studyplan/{studyplanId}", null);
    }

}

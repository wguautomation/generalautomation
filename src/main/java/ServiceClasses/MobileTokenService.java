
package ServiceClasses;

/*
 * Created by timothy.hallbeck on 10/20/2016.
 */

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonCampusInfo;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

public class MobileTokenService extends JsonUnknown implements ServiceBase {

    public MobileTokenService() {
    }

    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public MobileTokenService(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }

    public MobileTokenService(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "mobiletoken";
    }

    public String getDefaultEndpoint() {
        return "/mobiletoken/bannerid/{bannerId}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{bannerId}", General.getBannerId(login));

        ListOfGets.clear();
        ListOfGets.put("/mobiletoken/bannerid/{bannerId}", null);
    }

}
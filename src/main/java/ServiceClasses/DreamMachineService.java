
package ServiceClasses;

/*
 * Created by timothy.hallbeck on 11/3/2016.
 */

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

public class DreamMachineService extends JsonUnknown implements ServiceBase {

    public DreamMachineService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public DreamMachineService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public DreamMachineService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "dreammachine";
    }

    public String getDefaultEndpoint() {
        return "/v1/evaluator/all";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{submissionId}", "unksubmissionid");
        login.addMacroAndValue("{studentId}", General.getBannerId(login));
        login.addMacroAndValue("{assessmentCode}", "MBT2");
        login.addMacroAndValue("{taskCode}", "46338460");

        ListOfGets.clear();
        ListOfGets.put("/v1/evaluator/queue/{pidm}", null);
        ListOfGets.put("/v1/evaluator/{pidm}", null);
        ListOfGets.put("/v1/evaluator/all", null);
        ListOfGets.put("/v1/student/{studentId}/submission", null);
        ListOfGets.put("/v1/submission/{submissionId}", null);
        ListOfGets.put("/v1/publication/course/{courseCode}/assessment/{assessmentCode}/task/{taskCode}", null);
        ListOfGets.put("/v1/publication/course/{courseCode}", null);
        ListOfGets.put("/v1/publication/all", null);
        ListOfGets.put("/v1/publication/course/{courseCode}/assessment/{assessmentCode}\n", null);

    }

}
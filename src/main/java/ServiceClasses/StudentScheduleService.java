
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class StudentScheduleService extends JsonUnknown implements ServiceBase {

    public StudentScheduleService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public StudentScheduleService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public StudentScheduleService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "student-schedule";
    }

    public String getDefaultEndpoint() {
        return "/v1/students/{pidm}/schedule/items/count";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        ListOfGets.clear();
        ListOfGets.put("/v1/students/{pidm}/schedule/items/count", null);
        ListOfGets.put("/v1/students/{pidm}/schedule/items", null);
        ListOfGets.put("/v1/students/{pidm}/google/calendar", null);
        ListOfGets.put("/v1/students/{pidm}/calendars/calendar", null);
    }

}
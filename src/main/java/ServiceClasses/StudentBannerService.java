
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 10/31/2016.
 */

public class StudentBannerService extends JsonUnknown implements ServiceBase {

    public StudentBannerService() {
    }

    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public StudentBannerService(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }

    public StudentBannerService(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "student-banner";
    }

    public String getDefaultEndpoint() {
        return "/v2/api/student/{pidm}/tasks";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{id}", "50365");

        ListOfGets.clear();
        ListOfGets.put("/v2/api/student/{pidm}/tasks/ranged", null);
        ListOfGets.put("/v2/api/student/{pidm}/tasks/multRange", null);
        ListOfGets.put("/v2/api/student/{pidm}/tasks", null);
        ListOfGets.put("/v2/api/student/{pidm}/tasks/{id}", null);
    }

}

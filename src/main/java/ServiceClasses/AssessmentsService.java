
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonAssessmentArray;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonDegreePlan;
import JsonDataClasses.JsonServiceMappings;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/19/2016.
 */

public class AssessmentsService extends JsonAssessmentArray implements ServiceBase {

    public AssessmentsService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public AssessmentsService(LoginType login) {
        loadArray(load(login, AuthType.OSSO), arrayKey);
    }
    public AssessmentsService(LoginType login, AuthType authType) {
        loadArray(load(login, authType), arrayKey);
    }

    public String getAppName() {
        return "assessments";
    }

    public String getDefaultEndpoint() {
        return "/v1/pending/assessment/list";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{assessmentId}", General.getAssessmentId(login));
        login.addMacroAndValue("{courseVersionId}", General.getCourseVersionId(login));
        login.addMacroAndValue("{assessmentCode}", General.getAssessmentCode(login));

        ListOfGets.clear();
        ListOfGets.put("/courses/{courseVersionId}", null);
        ListOfGets.put("/assessments/{assessmentId}/competencies", null);
        ListOfGets.put("/students/{pidm}/courses/{courseVersionId}", null);

        ListOfGets.put("/students/{pidm}/courses/{courseVersionId}/assessment/{assessmentCode}/assess", null);
        ListOfGets.put("/students/{pidm}/courses/{courseVersionId}/assessments", null);
        ListOfGets.put("/v0/students/{pidm}/courses/{courseVersionId}\n", null);
    }

}


package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonServiceInfo;
import JsonDataClasses.JsonServiceMappings;
import JsonDataClasses.JsonServiceMetrics;
import Utils.*;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputControl;
import javafx.stage.Stage;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
 * Created by timothy.hallbeck on 9/15/2016.
 */

public class ServiceProcessor {

    private ConcurrentLinkedQueue<String> dataQ = new ConcurrentLinkedQueue<>();

    public ServiceProcessor() {}

    static public String[] GetActions() {
        return ServiceActions.GetDisplayNames();
    }

    static public String[] GetServiceNames() {
        return ServiceNames.GetDisplayNames(false);
    }

    static public String[] GetAuthTypes() {
        return AuthType.GetDisplayNames();
    }

    static public String[] GetLogins() {
        return LoginType.GetDisplayNames();
    }

    static public void Reset() {
        General.ClearTokens();
    }

    static String LimitedAllImplementation   = "(all) only available for /info currently";
    static String LimitedGraphImplementation = "Monitors only spawnable from JavaFX program";

    public String Process(String serviceName, String userName, String authName, String actionName) {
        return Process(serviceName, userName, authName, actionName, null);
    }
    public String Process(String serviceName, String userName, String authName, String actionName, TextInputControl outputWindow) {
        General.Debug("\nServiceProcessor::Process(" + serviceName + ", " + userName + ", " + authName + ", " + actionName + ")");
        General.SetOutputWindow(outputWindow);

        AuthType         auth = AuthType.Find(authName);
        LoginType       login = LoginType.Find(userName);
        ServiceActions  action = ServiceActions.Find(actionName);
        ExecutorService executor;

        if (action == ServiceActions.RETURN_TOKEN) {
            General.ClearStatics();
            executor = Executors.newCachedThreadPool();
            executor.execute(new ReturnToken(auth, login));
            executor.shutdown();
        }

        Reset();
        ServiceBase service = ServiceNames.Find(serviceName);
        if (service != null)
            service.Reset();

        switch (action) {
            case EXERCISE_ENDPOINTS:
                if (service == null)
                    return LimitedAllImplementation;
                executor = Executors.newCachedThreadPool();
                executor.execute(new ExerciseEndpoints(service, auth, login));
                executor.shutdown();
                break;

            case SHOW_ENDPOINTS:
                if (service == null)
                    return LimitedAllImplementation;
                executor = Executors.newCachedThreadPool();
                executor.execute(new ShowEndpoints(service, login));
                executor.shutdown();
                break;

            case VERSION_INFO:
                executor = Executors.newCachedThreadPool();
                executor.execute(new VersionInfo(service, login));
                executor.shutdown();
                break;

            case SERVICE_MONITOR:
                if (service == null)
                    return LimitedAllImplementation;
                if (outputWindow == null)
                    return LimitedGraphImplementation;

                boolean prev = General.setSilentMode(false);

                OssoSession ossoSession = new OssoSession();
                JsonServiceMetrics metrics = new JsonServiceMetrics(ossoSession.Get(service.getAppName(), "/metrics", login));
                String[] array = metrics.getAllKeys();
                General.setSilentMode(prev);
                try {
                    Stage anotherStage = new Stage();
                    wguBarChart window = new wguBarChart(serviceName, userName);
                    window.load(array);
                    window.start(anotherStage);
                } catch (Exception e) {
                    General.Debug(e.getMessage());
                }
                break;

            default:
                return "Unhandled action in ServiceProcessor::Process() " + actionName;
        }
        return "";
    }

    private class ExerciseEndpoints implements Runnable {

        private final AuthType    auth;
        private final LoginType   login;
        private final ServiceBase service;

        public ExerciseEndpoints(ServiceBase service, AuthType auth, LoginType login) {
            this.auth    = auth;
            this.login   = login;
            this.service = service;
        }

        public void run() {
            String output = "";
            boolean prev = General.setSilentMode(false);

            try {
                service.load(login, auth);
                service.ExerciseEndpoints(login, auth);
            } catch (Exception ex) {
                General.Debug(ex.getMessage());
            }
            General.SendTextToOutputWindow(output);
            General.setSilentMode(prev);
        }
    }

    private class ReturnToken implements Runnable {

        private final AuthType  auth;
        private final LoginType login;

        public ReturnToken(AuthType auth, LoginType login) {
            this.auth  = auth;
            this.login = login;
        }

        public void run() {
            String output = "";
            boolean prev = General.setSilentMode(false);

            try {
                switch (auth) {
                    case OSSO:
                        output = new OssoSession().getToken(login);
                        break;
                    case MASHERY:
                        output = new MasherySession().getToken(login);
                        break;
                    case PING:
                        output = new PingSession().getToken(login);
                        break;
                    default:
                        output = "Unknown authType in ServiceProcessor::RETURN_TOKEN " + auth;
                        break;
                }
            } catch (Exception ex) {
                General.Debug(ex.getMessage());
            }
            General.SendTextToOutputWindow(output);
            General.setSilentMode(prev);
        }
    }

    private class ShowEndpoints implements Runnable {

        private final ServiceBase service;
        private final LoginType   login;

        public ShowEndpoints(ServiceBase service, LoginType login) {
            this.service = service;
            this.login   = login;
        }

        public void run() {
            String output;
            boolean prev = General.setSilentMode(false);

            try {
                General.SendTextToOutputWindow(service.getAppName() + " - " + login.getDisplayName());
                OssoSession session = new OssoSession();
                output = new JsonServiceMappings(session.Get(service.getAppName(), "/mappings", login)).toString();
                General.SendTextToOutputWindow(output);
            } catch (Exception ex) {
                General.Debug(ex.getMessage());
            }
//            General.RestoreOutputCursor();
            General.setSilentMode(prev);
        }
    }

    private class VersionInfo implements Runnable {

        private final ServiceBase service;
        private final LoginType   login;

        public VersionInfo(ServiceBase service, LoginType login) {
            this.service = service;
            this.login = login;
        }

        public void run() {
            String output, appName;
            boolean prev = General.setSilentMode(false);

            try {
                String          result;
                OssoSession     session = new OssoSession();
                JsonServiceInfo info    = new JsonServiceInfo();

                if (service == null) {
                    General.SendTextToOutputWindow("Service Name   Lane 2   Lane 1   Prod");

                    String[] names = ServiceNames.GetDisplayNames(true);
                    ServiceBase service;

                    for (int i = 0; i < names.length; i++) {
                        service = ServiceNames.Find(names[i]);

                        appName = service.getAppName();
                        output = appName + "  ";

                        // lane 2 info on the left
                        try {
                            result = session.Get(appName, "/info", LoginType.LANE2_SPOLTUS);
                            if (result.equals("")) {
                                output += "n/a";
                            } else {
                                info.load(result);
                                output += info.getVersion() + "  ";
                            }
                        } catch (Exception | AssertionError e) {
                            output += "exception  ";
                        }

                        // lane 1 info in the middle
                        try {
                            result = session.Get(appName, "/info", LoginType.LANE1_SPOLTUS);
                            if (result.equals("")) {
                                output += "n/a";
                            } else {
                                info.load(result);
                                output += info.getVersion() + "  ";
                            }
                        } catch (Exception | AssertionError e) {
                            output += "exception  ";
                        }

                        // Prod info on the right
                        try {
                            result = session.Get(appName, "/info", LoginType.PROD_SPOLTUS);
                            if (result.equals("")) {
                                output += "n/a";
                            } else {
                                info.load(result);
                                output += info.getVersion() + "  ";
                            }
                        } catch (Exception | AssertionError e) {
                            output += "exception  ";
                        }

                        General.SendTextToOutputWindow(output);
                    }
                } else {
                    // these are on separate lines instead of consolidated for debugging purposes
                    result = session.Get(service.getAppName(), "/info", login);
                    info.load(result);
                    result = info.toString();
                    if (result.isEmpty())
                        result = "No build or version info published as /info for this service";

                    General.SendTextToOutputWindow(result);
                }
            } catch (Exception ex) {
                General.Debug(ex.getMessage());
            }
            General.setSilentMode(prev);
        }
    }

}

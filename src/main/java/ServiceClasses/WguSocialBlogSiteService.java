
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class WguSocialBlogSiteService extends JsonUnknown implements ServiceBase {

    public WguSocialBlogSiteService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public WguSocialBlogSiteService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public WguSocialBlogSiteService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "wsbsi";
    }

    public String getDefaultEndpoint() {
        return "/sites";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{socialSiteId}", "1");
        login.addMacroAndValue("{connectionId}", "1");

        ListOfGets.clear();
        ListOfGets.put("/sites", null);
        ListOfGets.put("/sites/{socialSiteId}", null);
        ListOfGets.put("/sites/sponsored", null);

        ListOfGets.put("/sites/default", null);
        ListOfGets.put("/sites/lastUpdated", null);
        ListOfGets.put("/students/{pidm}/connections", null);

        ListOfGets.put("/connections/{connectionId}/sites", null);
        ListOfGets.put("/students/{pidm}/connections/recommended", null);

    }
}

package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 10/31/2016.
 */

public class StudentAlertsService extends JsonUnknown implements ServiceBase {

    public StudentAlertsService() {
    }

    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public StudentAlertsService(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }

    public StudentAlertsService(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "student-alerts";
    }

    public String getDefaultEndpoint() {
        return "/v1/students/{pidm}/alerts";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
//        login.addMacroAndValue("{location}", "1");

        ListOfGets.clear();
        ListOfGets.put("/v1/students/{pidm}/alerts", null);
        ListOfGets.put("/v1/students/{pidm}/alerts/count", null);
    }

}


package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;
import Utils.General;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class PreAssessmentReportService extends JsonUnknown implements ServiceBase {

    public PreAssessmentReportService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public PreAssessmentReportService(LoginType login) {
        load(login, AuthType.OSSO);
    }
    public PreAssessmentReportService(LoginType login, AuthType authType) {
        load(login, authType);
    }

    public String getAppName() {
        return "pre-assessment-report";
    }

    public String getDefaultEndpoint() {
        return "/v1/students/{pidm}/assessments/{assessmentCode}/byDate";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadExtraMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{assessmentCode}", General.getAssessmentCode(login));
    }

}
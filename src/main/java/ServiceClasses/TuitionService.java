
package ServiceClasses;

/*
 * Created by timothy.hallbeck on 10/31/2016.
 */

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;

import java.util.HashMap;
import java.util.Map;

public class TuitionService extends JsonUnknown implements ServiceBase {

    public TuitionService() {
    }

    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public TuitionService(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }

    public TuitionService(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "tuition";
    }

    public String getDefaultEndpoint() {
        return "/v2/actualtuitioncosts/{pidm}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{termcode}", "201011");

        ListOfGets.clear();
        ListOfGets.put("/summedtuitioncosts/{pidm}/termcode/{termcode}", null);
        ListOfGets.put("/actualtuitioncosts/{pidm}", null);
        ListOfGets.put("/v2/actualtuitioncosts/{pidm}", null);
        ListOfGets.put("/anticipatedtutionmemocostsmemo/{pidm}", null);
        ListOfGets.put("/tuitioncost/pidm/{pidm}/termcode/{termcode}", null);
        ListOfGets.put("/anticipatedtutioncosts/{pidm}", null);
    }

}

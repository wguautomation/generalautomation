
package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.*;
import Utils.General;
import Utils.PingSession;
import org.testng.SkipException;
import org.testng.TestNGException;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/22/2016.
 */

public class AcademicActivityService extends JsonActivityArray implements ServiceBase {

    public AcademicActivityService(){}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public AcademicActivityService(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }
    public AcademicActivityService(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "academic-activity";
    }

    public String getDefaultEndpoint() {
        return "/v1/api/student/{pidm}/activity";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{bannerId}", General.getBannerId(login));
        String result;

        switch (auth) {
            case PING:
                ping.load(getAppName(), login, PingSession.ServiceKey.MAPPINGS);
                result = ping.GetEndpoint(getDefaultEndpoint(), null);
                break;
            case MASHERY:
                result = mashery.Get(getDefaultEndpoint(), login, null);
                break;
            case OSSO:
            default:
                result = osso.Get(getAppName(), getDefaultEndpoint(), login, null);
                break;
        }

        loadArray(result, "");
        login.addMacroAndValue("{activityId}", getAllActivities().getActivity(0).getId());

        ListOfGets.clear();
        ListOfGets.put("/v1/api/student/{pidm}/activity", null);
        ListOfGets.put("/v1/api/student/{pidm}/activity/detail", null);
        ListOfGets.put("/v1/api/student/{pidm}/activity/{activityId}", null);

        ListOfGets.put("/v1/api/student/bannerid/{bannerId}/activity", null);
        ListOfGets.put("/v1/api/student/bannerid/{bannerId}/activity/detail", null);
        ListOfGets.put("/v1/api/student/bannerid/{bannerId}/activity/{activityId}", null);
    }


}
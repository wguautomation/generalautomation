
package ServiceClasses;

/*
 * Created by timothy.hallbeck on 10/31/2016.
 */

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;

import java.util.HashMap;
import java.util.Map;

public class PortalPromptService extends JsonUnknown implements ServiceBase {

    public PortalPromptService() {
    }

    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public PortalPromptService(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }

    public PortalPromptService(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "portal-prompt";
    }

    public String getDefaultEndpoint() {
        return "/portalprompt/{promptTypeDescription}/pidm/{pidm}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{promptTypeDescription}", "mobile");

        ListOfGets.clear();
        ListOfGets.put("/portalprompt/{promptTypeDescription}/pidm/{pidm}", null);
        ListOfGets.put("/v0/portalprompt/reset", null);
        ListOfGets.put("/v0/portalprompt/{promptTypeDescription}/pidm/{pidm}", null);
    }

}

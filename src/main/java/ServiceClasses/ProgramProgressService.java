package ServiceClasses;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonProgramProgress;
import JsonDataClasses.JsonProgramProgressCalculated;
import JsonDataClasses.JsonServiceMappings;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by timothy.hallbeck on 9/1/2016.
 */

public class ProgramProgressService extends JsonProgramProgress implements ServiceBase {

    public ProgramProgressService() {}
    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public ProgramProgressService(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }
    public ProgramProgressService(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "programprogress";
    }

    public String getDefaultEndpoint() {
        return "/v1/api/ProgramProgress/{pidm}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        ListOfGets.clear();
        ListOfGets.put("/v1/api/ProgramProgress/{pidm}", null);
        ListOfGets.put("/v1/api/ProgramProgress/{pidm}/calculated", null);
    }


}
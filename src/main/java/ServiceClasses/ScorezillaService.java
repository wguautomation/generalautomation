
package ServiceClasses;

/*
 * Created by timothy.hallbeck on 11/2/2016.
 */

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;

import java.util.HashMap;
import java.util.Map;

public class ScorezillaService extends JsonUnknown implements ServiceBase {

    public ScorezillaService() {
    }

    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public ScorezillaService(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }

    public ScorezillaService(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "scorezilla";
    }

    public String getDefaultEndpoint() {
        return "/v1/assessment/import/vendor/list";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
//        login.addMacroAndValue("{location}", "1");

        ListOfGets.clear();
        ListOfGets.put("/v1/assessment/import/vendor/list", null);
    }

}

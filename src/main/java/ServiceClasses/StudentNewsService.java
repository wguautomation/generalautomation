
package ServiceClasses;

/*
 * Created by timothy.hallbeck on 10/31/2016.
 */

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonData;
import JsonDataClasses.JsonUnknown;

import java.util.HashMap;
import java.util.Map;

public class StudentNewsService extends JsonUnknown implements ServiceBase {

    public StudentNewsService() {
    }

    // When authtype changeover from osso / mashery to Ping happens, this constructor is the only thing that needs to change
    public StudentNewsService(LoginType login) {
        loadObject(load(login, AuthType.OSSO));
    }

    public StudentNewsService(LoginType login, AuthType authType) {
        loadObject(load(login, authType));
    }

    public String getAppName() {
        return "student-news";
    }

    public String getDefaultEndpoint() {
        return "/v1/announcements/students/{pidm}";
    }

    private Map<String, JsonData> ListOfGets = new HashMap<>();
    public Map<String, JsonData> getListOfGets()
    {
        return ListOfGets;
    }

    public void LoadMacros(LoginType login, AuthType auth) {
        login.addMacroAndValue("{location}", "1");
        login.addMacroAndValue("{newsType}", "1");

        ListOfGets.clear();
        ListOfGets.put("/v1/announcements/students/{pidm}", null);
        ListOfGets.put("/v1/announcements/students/{username}/count", null);
        ListOfGets.put("/v1/announcements/feed/newstype/{newsType}/location/{location}", null);
    }

}
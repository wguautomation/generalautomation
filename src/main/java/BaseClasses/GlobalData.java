
package BaseClasses;

import Utils.General;
import org.testng.SkipException;

/*
 * Created by timothy.hallbeck on 4/28/2015.
 */

public class GlobalData {

    public static String DATA_PATH = "/Users/Default/AppData/Local/testautomation/";

    // Although enums are the modern way to do this, I prefer the simplicity of compile time statics and ORs
    public static long LOGIN_LANE1 = 1<<0;
    public static long LOGIN_LANE2 = 1<<1;
    public static long LOGIN_PROD  = 1<<2;

    public static long STATUS_PROSPECTIVE   = 1<<3;
    public static long STATUS_ACTIVE        = 1<<4;
    public static long STATUS_RETURNINGGRAD = 1<<5;
    public static long STATUS_GRADUATED     = 1<<6;
    public static long STATUS_TB            = 1<<7;

    public static long STATE_TENNESSEE  = 1<<8;
    public static long STATE_UTAH       = 1<<9;
    public static long STATE_TEXAS      = 1<<10;
    public static long STATE_WASHINGTON = 1<<11;
    public static long STATE_INDIANA    = 1<<12;
    public static long STATE_NEVADA     = 1<<13;

    public static long DEVICE_PHONE    = 1<<15;
    public static long DEVICE_TABLET   = 1<<16;
    public static long DEVICE_VIRTUAL  = 1<<17;
    public static long DEVICE_PHYSICAL = 1<<18;

    public static long UNDERGRADUATE = 1<<23;
    public static long GRADUATE      = 1<<24;

    // Internal to Data object
    private long Attributes = 0L;

    // Accessors
    public boolean isPhone() {
        return ((Attributes & DEVICE_PHONE) > 0);
    }
    public boolean isTablet() {
        return ((Attributes & DEVICE_TABLET) > 0);
    }
    public boolean isDeviceVirtual() {
        return ((Attributes & DEVICE_VIRTUAL) > 0);
    }
    public boolean isDevicePhysical() {
        return ((Attributes & DEVICE_PHYSICAL) > 0);
    }

    // Login
    private String Username = "";
    private String Password = "";
    public  String getUsername() { return Username; }
    public  String getPassword() { return Password; }

    // Just pass in "LOGIN_LANE1" or "LOGIN_LANE2" and everything will just magically work
    public GlobalData(long lAttributes) {
        // Introduce defaults

        // If unspecified, assume phone
        if ((lAttributes & (DEVICE_PHONE | DEVICE_TABLET)) == 0)
            lAttributes |= DEVICE_PHONE;

        // Save what we now have
        Attributes = lAttributes;

        // Prospective only gets login info, as it won't able to login
        if ((lAttributes & STATUS_PROSPECTIVE) > 0) {
            if ((lAttributes & LOGIN_LANE1) > 0) {
                Username = LoginType.LANE1_PROSPECTIVE.getUsername();
                Password = LoginType.LANE1_PROSPECTIVE.getPassword();
            } else if ((lAttributes & LOGIN_LANE2) > 0) {
                Username = LoginType.LANE2_PROSPECTIVE.getUsername();
                Password = LoginType.LANE2_PROSPECTIVE.getPassword();
            } else {
                General.Debug("  Error - Prod prospective user account has not been identified");
                General.SkipTest("Prod prospective user account has not been identified");
//                Username = LoginType.PROD_PROSPECTIVE.getUsername(); none identified
//                Password = LoginType.PROD_PROSPECTIVE.getPassword();
            }
            return;
        }

        if ((lAttributes & LOGIN_LANE1) > 0) {
            Username = LoginType.LANE1_NORMAL.getUsername();
            Password = LoginType.LANE1_NORMAL.getPassword();
        } else if ((lAttributes & LOGIN_LANE2) > 0) {
            Username = LoginType.LANE2_NORMAL.getUsername();
            Password = LoginType.LANE2_NORMAL.getPassword();
        } else {
            Username = LoginType.PRODUCTION_NORMAL.getUsername();
            Password = LoginType.PRODUCTION_NORMAL.getPassword();
        }
    }

    GlobalData(LoginType loginType) {
        Username = loginType.getUsername();
        Password = loginType.getPassword();
    }

}

























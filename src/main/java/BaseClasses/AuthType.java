
package BaseClasses;

/*
 * Created by timothy.hallbeck on 8/1/2016.
 */

import org.testng.TestNGException;

public enum AuthType {
    OSSO,
    PING,
    MASHERY;

    static private String[] DisplayNames = {
            "OSSO",
            "Mashery",
            "Ping",
    };

    static public String[] GetDisplayNames() {
        return DisplayNames;
    }

    static public AuthType Find(String string) {
        string = string.toLowerCase();
        switch (string) {
            case "osso":
                return OSSO;
            case "ping":
                return PING;
            case "mashery":
                return MASHERY;
            default:
                throw new TestNGException("Unknown auth name in AuthType::Find  " + string);
        }

    };
}


package BaseClasses;

/*
 * Created by timothy.hallbeck on 8/24/15.
 */

import Utils.General;
import Utils.wguEnvironment;
import Utils.wguPowerPointFileUtils;
import org.openqa.selenium.Keys;
import org.testng.TestNGException;

import java.util.Set;
import java.util.TreeMap;

public enum LoginType {
    BANNER_LANE1(       "",                   wguEnvironment.Lane1Qa,  "thallbeck",        "WhatTheFuck12!",           "Login/Username.PNG",               "Login/Password.PNG",             "Login/Connect.PNG",              "https://l1banner.wgu.edu",                            "n/a",        "n/a"),
    EXTERNAL_SITE(      "",                   wguEnvironment.none,     "",                 "",                         "",                                 "",                               "",                               "http://www.wgu.edu",                                  "n/a",        "n/a"),
    IIQ_LANE1(          "",                   wguEnvironment.Lane1Qa,  "timothy.hallbeck", "HellTheWhat12!",           "//*[@id='loginForm:accountId']",   "//*[@id='loginForm:password']",  "//*[@id='loginForm']/table/tbody/tr[3]/td/span", "https://l1iiq.wgu.edu/iiq/login.jsf", "n/a",        "n/a"),
    IIQ_PROD(           "",                   wguEnvironment.Prod,     "timothy.hallbeck", "HellTheWhat12!",           "//*[@id='loginForm:accountId']",   "//*[@id='loginForm:password']",  "//*[@id='loginForm']/table/tbody/tr[3]/td/span", "https://iiq.wgu.edu/iiq/login.jsf",   "n/a",        "n/a"),
    JIRA_HOMEPAGE(      "",                   wguEnvironment.Prod,     "timothy.hallbeck", "HellTheWhat12!",           "//*[@id='login-form-username']",   "//*[@id='login-form-password']", "//*[@id='login']",               "https://jira-it.wgu.edu/secure/Dashboard.jspa",       "n/a",        "n/a"),
    LANE1_CPOINDEXTER(  "Lane 1 CPoindexter", wguEnvironment.Lane1Qa,  "cpoindexter",      "T3st1tn0w",                "//*[@id='userName']",              "//*[@id='password']",            "//*[@class='btn btn--primary']", "https://l1my.wgu.edu",                                "106679",     "BNC1"),
    LANE1_APOINDEXTER(  "",                   wguEnvironment.Lane1Qa,  "apoindexter",      "T3st1tn0w",                "//*[@id='userName']",              "//*[@id='password']",            "//*[@class='btn btn--primary']", "https://l1my.wgu.edu",                                "106682",     "C711"),
    LANE1_FFURNO(       "",                   wguEnvironment.Lane1Qa,  "ffurno",           "L1student",                "//*[@id='userName']",              "//*[@id='password']",            "//*[@class='btn btn--primary']", "https://l1my.wgu.edu",                                "432356",     "C459"),
    LANE1_MENTOR(       "",                   wguEnvironment.Lane1Qa,  "spoltus",          "T3st1tn0w",                "//*[@id='userName']",              "//*[@id='password']",            "//*[@class='btn btn--primary']", "https://l1my.wgu.edu",                                "106688",     ""),
    LANE1_MPOINDEXTER(  "",                   wguEnvironment.Lane1Qa,  "mpoindexter",      "T3st1tn0w",                "//*[@id='userName']",              "//*[@id='password']",            "//*[@class='btn btn--primary']", "https://l1my.wgu.edu",                                "106676",     ""),
    LANE1_NORMAL(       "",                   wguEnvironment.Lane1Qa,  "cpoindexter",      "T3st1tn0w",                "//*[@id='userName']",              "//*[@id='password']",            "//*[@class='btn btn--primary']", "https://l1my.wgu.edu",                                "106679",     "BNC1"),
    LANE1_PP_DANDE20(   "",                   wguEnvironment.Lane1Qa,  "dande20",          "L1password",               "//*[@id='userName']",              "//*[@id='password']",            "//*[@class='btn btn--primary']", "https://l1my.wgu.edu",                                "153508",     "none"),
    LANE1_PP_ACOX41(    "",                   wguEnvironment.Lane1Qa,  "acox41",           "L1password",               "//*[@id='userName']",              "//*[@id='password']",            "//*[@class='btn btn--primary']", "https://l1my.wgu.edu",                                "",           ""),
    LANE1_PP_CMACDO(    "",                   wguEnvironment.Lane1Qa,  "cmacdo1",          "L1password",               "//*[@id='userName']",              "//*[@id='password']",            "//*[@class='btn btn--primary']", "https://l1my.wgu.edu",                                "",           ""),
    LANE1_PROSPECTIVE(  "",                   wguEnvironment.Lane1Qa,  "yboyer",           "L1password",               "//*[@id='userName']",              "//*[@id='password']",            "//*[@class='btn btn--primary']", "https://l1my.wgu.edu",                                "",           ""),
    LANE1_SPOLTUS(      "Lane 1 SPoltus",     wguEnvironment.Lane1Qa,  "spoltus",          "T3st1tn0w",                "//*[@id='userName']",              "//*[@id='password']",            "//*[@class='btn btn--primary']", "https://l1my.wgu.edu",                                "106688",     ""),
    LANE1_V3(           "",                   wguEnvironment.Lane1Qa,  "rcowser",          "L1student",                "//*[@id='userName']",              "//*[@id='password']",            "//*[@class='btn btn--primary']", "https://l1my.wgu.edu",                                "",           ""),
    LANE2_CASSANDRA(    "",                   wguEnvironment.Lane2Dev, "spoltus",          "T3st1tn0w",                "//*[@id='login-username']",        "//*[@id='login-password']",      "//*[@class='btn btn--primary']", "https://l2my.wgu.edu",                                "106688",     ""),
    LANE2_MENTOR(       "",                   wguEnvironment.Lane2Dev, "spoltus",          "T3st1tn0w",                "//*[@id='login-username']",        "//*[@id='login-password']",      "//*[@class='btn btn--primary']", "https://l2my.wgu.edu",                                "106688",     ""),
    LANE2_PROSPECT(     "",                   wguEnvironment.Lane2Dev, "ejohn23",          "L2password",               "//*[@id='login-username']",        "//*[@id='login-password']",      "//*[@class='btn btn--primary']", "https://l2my.wgu.edu",                                "140335",     ""),
    LANE2_SPOLTUS(      "Lane 2 SPoltus",     wguEnvironment.Lane2Dev, "spoltus",          "T3st1tn0w",                "//*[@id='login-username']",        "//*[@id='login-password']",      "//*[@class='btn btn--primary']", "https://l2my.wgu.edu",                                "106688",     ""),
    LANE2_CPOINDEXTER(  "Lane 2 CPoindexter", wguEnvironment.Lane2Dev, "cpoindexter",      "T3st1tn0w",                "//*[@id='login-username']",        "//*[@id='login-password']",      "//*[@class='btn btn--primary']", "https://l2my.wgu.edu",                                "106679",     "C458"),
    LANE2_CRAFUSE(      "",                   wguEnvironment.Lane2Dev, "crafuse",          "L2student",                "//*[@id='login-username']",        "//*[@id='login-password']",      "//*[@class='btn btn--primary']", "https://l2my.wgu.edu",                                "112371",     "CJC1"),
    LANE2_MPOIN(        "",                   wguEnvironment.Lane2Dev, "mpoindexter",      "T3st1tn0w",                "//*[@id='login-username']",        "//*[@id='login-password']",      "//*[@class='btn btn--primary']", "https://l2my.wgu.edu",                                "106676",     ""),
    LANE2_NORMAL(       "",                   wguEnvironment.Lane2Dev, "cpoindexter",      "T3st1tn0w",                "//*[@id='login-username']",        "//*[@id='login-password']",      "//*[@class='btn btn--primary']", "https://l2my.wgu.edu",                                "106679",     "C458"),
    LANE2_PP_DANDE20(   "",                   wguEnvironment.Lane2Dev, "dande20",          "L2password",               "//*[@id='login-username']",        "//*[@id='login-password']",      "//*[@class='btn btn--primary']", "https://l2my.wgu.edu",                                "",           ""),
    LANE2_PP_CWAYRYN(   "",                   wguEnvironment.Lane2Dev, "Cwayryn",          "L2password",               "//*[@id='login-username']",        "//*[@id='login-password']",      "//*[@class='btn btn--primary']", "https://l2my.wgu.edu",                                "",           ""),
    LANE2_PROSPECTIVE(  "",                   wguEnvironment.Lane2Dev, "ejohn23",          "L2password",               "//*[@id='login-username']",        "//*[@id='login-password']",      "//*[@class='btn btn--primary']", "https://l2my.wgu.edu",                                "",           ""),
    PRODUCTION_JUSTTIM( "",                   wguEnvironment.Prod,     "tpoindexter",      "T3st1tn0w",                "//*[@id='userName']",              "//*[@id='password']",            "//*[@id='loginButton']",         "https://my.wgu.edu",                                  "106674",     ""),
    PROD_CPOINDEXTER(   "",                   wguEnvironment.Prod,     "cpoindexter",      "T3st1tn0w",                "//*[@id='userName']",              "//*[@id='password']",            "//*[@id='loginButton']",         "https://my.wgu.edu",                                  "106679",     "C455"),
    PROD_CPOINDEXTER_PING("Prod CPoindexter", wguEnvironment.Prod,     "cpoindexter",      "T3st1tn0w",                "//*[@id='username']",              "//*[@id='password']",            "/html/body/div/div[2]/div[2]/form/div[6]/a[2]", "https://access.wgu.edu/shadow",        "106679",     "C455"),
    PROD_MENTOR(        "",                   wguEnvironment.Prod,     "spoltus",          "T3st1tn0w",                "//*[@id='userName']",              "//*[@id='password']",            "//*[@id='loginButton']",         "https://my.wgu.edu",                                  "106688",     ""),
    PROD_SPOLTUS(       "Prod SPoltus",       wguEnvironment.Prod,     "spoltus",          "T3st1tn0w",                "//*[@id='userName']",              "//*[@id='password']",            "//*[@id='loginButton']",         "https://my.wgu.edu",                                  "106688",     ""),
    PRODUCTION_LRP   (  "",                   wguEnvironment.Prod,     "cpoindexter",      "T3st1tn0w",                "//*[@id='userName']",              "//*[@id='password']",            "//*[@id='loginButton']",         "https://lrps.wgu.edu/provision/2290",                 "106679",     "C455"),
    PRODUCTION_APOIN (  "",                   wguEnvironment.Prod,     "apoindexter",      "T3st1tn0w",                "//*[@id='userName']",              "//*[@id='password']",            "//*[@id='loginButton']",         "https://my.wgu.edu",                                  "106682",     ""),
    PRODUCTION_LPOIN (  "",                   wguEnvironment.Prod,     "lpoindexter",      "T3st1tn0w",                "//*[@id='userName']",              "//*[@id='password']",            "//*[@id='loginButton']",         "https://my.wgu.edu",                                  "107264",     ""),
    PRODUCTION_NORMAL(  "",                   wguEnvironment.Prod,     "cpoindexter",      "T3st1tn0w",                "//*[@id='userName']",              "//*[@id='password']",            "//*[@id='loginButton']",         "https://my.wgu.edu",                                  "106679",     "C455"),
    PROD_PROSPECTIVE(   "",                   wguEnvironment.Prod,     "qqava44",          "T3st1tn0w",                "//*[@id='userName']",              "//*[@id='password']",            "//*[@id='loginButton']",         "https://my.wgu.edu",                                 "",           ""),
    QTEST            (  "",                   wguEnvironment.none,     "timothy.hallbeck@wgu.edu", "HellTheWhat12!",   "//*[@id='userName']",              "//*[@id='password']", "//*[@id='loginForm']/div/div[1]/a/img",     "https://qtest.wgu.edu",                  "",            ""),
    TESTLINK         (  "",                   wguEnvironment.none,     "timothy.hallbeck@wgu.edu", "Driven12!",        "//*[@id='login']",                 "//*[@id='login_div']/form/p[2]/input",  "//*[@id='login_div']/form/input[5]",  "https://testlink.wgu.edu/index.php",    "",             ""),
    SALESFORCE_QA(      "",                   wguEnvironment.Lane1Qa,  "timothy.hallbeck@wgu.edu.qafull", "Driven12!", "//*[@id='username']",              "//*[@id='password']",             "//*[@id='Login']",         "https://srm--qafull.cs45.my.salesforce.com/home/home.jsp", "",           ""),
    ;
    private final String displayName;
    private final wguEnvironment environment;
    private final String username;
    private final String password;
    private final String usernameXpath;
    private final String passwordXpath;
    private final String loginButtonXpath;
    private final String startingUrl;
    private final String pidm;
    private final String courseCode;

    static TreeMap<String, String>    macroMap       = new TreeMap<>();
    static TreeMap<String, LoginType> displayNameMap = new TreeMap<>();

    private LoginType(String displayName, wguEnvironment environment, String username, String password, String usernameXpath, String passwordXpath, String loginButtonXpath, String startingUrl, String pidm, String courseCode) {
        this.displayName        = displayName;
        this.environment        = environment;
        this.username           = username;
        this.password           = password;
        this.usernameXpath      = usernameXpath;
        this.passwordXpath      = passwordXpath;
        this.loginButtonXpath   = loginButtonXpath;
        this.startingUrl        = startingUrl;
        this.pidm               = pidm;
        this.courseCode         = courseCode;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public String getUsernameXpath() {
        return this.usernameXpath;
    }

    public String getPasswordXpath() {
        return this.passwordXpath;
    }

    public String getLoginButtonXpath() {
        return this.loginButtonXpath;
    }

    public String getStartingUrl() {
        return this.startingUrl;
    }

    public String getPidm() {
        if (this.pidm == "")
            General.Debug("Warning: Undefined pidm for user " + getUsername() + " in loginType class");
        return this.pidm;
    }

    public String getCourseCode() {
        if (this.courseCode.equals(""))
            General.Debug("Warning: Undefined courseCode for user " + getUsername() + " in loginType class");
        return this.courseCode;
    }

    static public LoginType GetSystemDefault() {
        return LoginType.LANE1_CPOINDEXTER;
    }

    public String getUrlPrefix() {
        if (startingUrl.toLowerCase().contains("l1my"))
            return "l1";
        if (startingUrl.toLowerCase().contains("l2my"))
            return "l2";

        return "";
    }

    public String getLanePrefix() {
        if (isLane1())
            return ".qa";
        else if (isLane2())
            return ".dev";

        return "";
    }

    public String getLaneDisplayName() {
        if (isLane1())
            return "Lane 1 / QA";
        else if (isLane2())
            return "Lane 2 / Dev";
        else
            return "Production";
    }

    static public LoginType GetLaneMentor(LoginType login) {
        if (login.isLane1())
            return LoginType.LANE1_MENTOR;
        else if (login.isLane2())
            return LoginType.LANE2_MENTOR;

        return LoginType.PROD_MENTOR;
    }

    public String getMasheryLaneSuffix() {
        if (isLane1())
            return "qa";
        else if (isLane2())
            return "dev";

        return "";
    }

    public String getApimPrefix() {
        if (isLane1())
            return "api.qa";
        else if (isLane2())
            return "api.dev";
        else
            return "apim";
    }

    public wguEnvironment getEnv() {
        return environment;
    }

    static public String[] GetDisplayNames() {
        if (displayNameMap.size() == 0) {
            for (LoginType login : LoginType.values())
                if (!login.displayName.equals(""))
                    displayNameMap.put(login.displayName, login);
        }

        return displayNameMap.keySet().toArray(new String[displayNameMap.size()]);
    }

    static public LoginType Find(String string) {
        if (displayNameMap.size() == 0)
            GetDisplayNames();

        LoginType login = displayNameMap.get(string);
        if (login == null)
            throw new TestNGException("Unknown display name in LoginType::Find() " + string);

        return login;
    }

    public boolean isLane1() {
        return wguEnvironment.isLane1Prefix(startingUrl);
    }

    public boolean isLane2() {
        return wguEnvironment.isLane2Prefix(startingUrl);
    }

    public boolean isProd() {
        return !(isLane1() || isLane2());
    }

    public static void Login(Page page, LoginType type) {
        General.Debug("logging in as (" + type.getUsername() + ", " + type.getPassword() + ")");
        page.getByXpath(type.getUsernameXpath()).clear();
        page.getByXpath(type.getUsernameXpath()).click();
        General.Sleep(1000);
        page.getByXpath(type.getUsernameXpath()).sendKeys(type.getUsername());
        General.Sleep(1000);
        page.getByXpath(type.getUsernameXpath()).sendKeys(Keys.TAB);
        General.Sleep(1000);
        page.getByXpath(type.getPasswordXpath()).clear();
        General.Sleep(1000);
        page.getByXpath(type.getPasswordXpath()).sendKeys(type.getPassword());
        General.Sleep(1000);
        wguPowerPointFileUtils.saveScreenshot(page, "LoginType::Login()");
        page.getByXpath(type.getLoginButtonXpath()).click();
    }

    synchronized
    public TreeMap addMacroAndValue(String macro, String value) {
        macroMap.put(macro, value);
        return macroMap;
    }

    synchronized
    public TreeMap duplicateMacroSourceDest(String srcMacro, String destMacro) {
        macroMap.put(destMacro, macroMap.get(srcMacro));
        return macroMap;
    }

    public String expandMacros(String something) {
        addMacroAndValue("{$LANEPREFIX}", getLanePrefix());
        addMacroAndValue("{$APIMPREFIX}", getApimPrefix());
        addMacroAndValue("{pidm}", getPidm());
        addMacroAndValue("{courseCode}", getCourseCode());
        addMacroAndValue("{username}", getUsername());
        addMacroAndValue("{userName}", getUsername());
        addMacroAndValue("{Username}", getUsername());
        addMacroAndValue("{UserName}", getUsername());
        addMacroAndValue("{password}", getPassword());
        addMacroAndValue("{Password}", getPassword());

        for (String macro : macroMap.navigableKeySet())
            if (something.contains(macro))
                something = something.replace(macro, macroMap.get(macro));

        return something;
    }
}

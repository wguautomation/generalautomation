
package IiqPages;

/*
 * Created by timothy.hallbeck on 1/13/2016.
 */

import org.openqa.selenium.WebElement;

import java.util.List;

public class iiqChangePasswordsPage extends iiqMainPage {

    public iiqChangePasswordsPage() {
        super();
    }

    public iiqChangePasswordsPage load() {
        super.load();
        clickChangeOtherPasswords();

        return this;
    }

    public iiqMainPage clickAdvancedSearch() {
        getByXpath(iiqXpath.SearchPage_AdvancedSearchButton).click();
        Sleep(500);

        return this;
    }

    public iiqMainPage clickSearch() {
        getByXpath(iiqXpath.AdvancedSearchPage_Search).click();
        Sleep(500);

        return this;
    }

    public iiqMainPage clickSubmit() {
        getByXpath(iiqXpath.ChangePasswordsPage_Submit).click();
        Sleep(1500);

        return this;
    }

    public iiqMainPage selectFirstNameInResults() {
        getByXpath(iiqXpath.AdvancedSearchPage_NameFirstRow).click();
        Sleep(1500);

        return this;
    }

    public void selectAllCheckboxes() {
        int checkboxIndex = 2;
        String checkboxPath = iiqXpath.ChangePasswordsPage_AccountCheckbox.replace("REPLACE", Integer.toString(checkboxIndex));
        WebElement checkBox = getByXpath(checkboxPath, true);

        while (checkBox != null) {
            checkBox.click();
            Sleep(500);
            checkboxPath = iiqXpath.ChangePasswordsPage_AccountCheckbox.replace("REPLACE", Integer.toString(++checkboxIndex));
            checkBox = getByXpath(checkboxPath, true);
        }
    }

    public void fillInAllPasswords(String standardPassword) {
        List<WebElement> passwords = getListByXpath(iiqXpath.ChangePasswordsPage_NewPassword);
        for (WebElement element : passwords) {
            element.sendKeys(standardPassword);
            Sleep(500);
        }
        passwords = getListByXpath(iiqXpath.ChangePasswordsPage_ConfirmPassword);
        for (WebElement element : passwords) {
            element.sendKeys(standardPassword);
            Sleep(500);
        }
    }

    public iiqChangePasswordsPage standardizePassword(String user, String password) {
        clickAdvancedSearch();
        typeByXpath(iiqXpath.AdvancedSearchPage_Name, user);
        clickSearch();
        selectFirstNameInResults();
        selectAllCheckboxes();
        fillInAllPasswords(password);
        clickSubmit();
        clickSubmit();
        getByXpath(iiqXpath.ChangePasswordsPage_Confirmed);

        return this;
    }

}

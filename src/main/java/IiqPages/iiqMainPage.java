
package IiqPages;

/*
 * Created by timothy.hallbeck on 12/18/2015.
 */

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;

public class iiqMainPage extends WebPage {
    public iiqMainPage() {
        this(null);
    }

    public iiqMainPage(WebPage existingPage) {
        super(existingPage, LoginType.IIQ_LANE1);
        login();
    }

    public iiqMainPage load() {
        get(iiqXpath.MainPage_Url);

        return this;
    }

    public iiqMainPage clickAccessReviews() {
        getByXpath(iiqXpath.MainPage_AccessReviews).click();
        Sleep(1000);

        return this;
    }

    public iiqMainPage clickPolicyViolations() {
        getByXpath(iiqXpath.MainPage_PolicyViolations).click();
        Sleep(1000);

        return this;
    }

    public iiqMainPage clickChangePassword() {
        getByXpath(iiqXpath.MainPage_ChangePassword).click();
        Sleep(1000);

        return this;
    }

    public iiqMainPage clickEditAuthenticationQuestions() {
        getByXpath(iiqXpath.MainPage_EditAuthenticationQuestions).click();
        Sleep(1000);

        return this;
    }

    public iiqMainPage clickApprovals() {
        getByXpath(iiqXpath.MainPage_Approvals).click();
        Sleep(1000);

        return this;
    }

    public iiqMainPage clickSignOffReports() {
        getByXpath(iiqXpath.MainPage_SignOffReports).click();
        Sleep(1000);

        return this;
    }

    public iiqMainPage clickWorkItems() {
        getByXpath(iiqXpath.MainPage_WorkItems).click();
        Sleep(1000);

        return this;
    }

    public iiqMainPage clickRequestAccess() {
        getByXpath(iiqXpath.MainPage_RequestAccess).click();
        Sleep(1000);

        return this;
    }

    public iiqMainPage clickManageAccounts() {
        getByXpath(iiqXpath.MainPage_ManageAccounts).click();
        Sleep(1000);

        return this;
    }

    public iiqMainPage clickChangeOtherPasswords() {
        getByXpath(iiqXpath.MainPage_ChangeOtherPasswords).click();
        Sleep(1000);

        return this;
    }

    public iiqMainPage clickTrackMyRequests() {
        getByXpath(iiqXpath.MainPage_TrackMyRequests).click();
        Sleep(1000);

        return this;
    }

    public iiqMainPage clickEditIdentity() {
        getByXpath(iiqXpath.MainPage_EditIdentity).click();
        Sleep(1000);

        return this;
    }

    public iiqMainPage clickViewIdentity() {
        getByXpath(iiqXpath.MainPage_ViewIdentity).click();
        Sleep(1000);

        return this;
    }

    public iiqMainPage clickRefreshIdentity() {
        getByXpath(iiqXpath.MainPage_RefreshIdentity).click();
        Sleep(1000);

        return this;
    }

    public iiqMainPage ExercisePage(boolean cascade) {
        General.Debug("\niiqMainPage::ExercisePage(" + cascade + ")");
        load();
        clickAccessReviews();

        load();
        clickPolicyViolations();

        load();
        clickChangePassword();

        load();
        clickEditAuthenticationQuestions();

        load();
        clickApprovals();

        load();
        clickSignOffReports();

        load();
        clickWorkItems();

        load();
        clickRequestAccess();

        load();
        clickManageAccounts();

        load();
        clickChangeOtherPasswords();

        load();
        clickTrackMyRequests();

        load();
        clickEditIdentity();

        load();
        clickViewIdentity();

        load();
        clickRefreshIdentity();


        return this;
    }
}


package IiqPages;

/*
 * Created by timothy.hallbeck on 12/23/2015.
 */

public class iiqXpath {

    static String AdvancedSearchPage_AdvancedSearch = "//*[@id='button-1050']";
    static String AdvancedSearchPage_Name           = "//*[@id='suggest_name-inputEl']";
    static String AdvancedSearchPage_Search         = "//*[@id='button-1069-btnInnerEl']";
    static String AdvancedSearchPage_SearchBox      = "//*[@id='searchfield-1046-inputEl']";
    static String AdvancedSearchPage_NameFirstRow   = "//*[@id='gridview-1042']/table/tbody/tr[2]/td[1]/div";

    static String ChangePasswordsPage_AccountCheckbox = "//*[@id='gridview-1043']/table/tbody/tr[REPLACE]/td/div/table/tbody/tr[1]/td[1]/div";
    static String ChangePasswordsPage_Confirmed       = "//*[@id='dashboardForm:_id101']/ul/li[1]";
    static String ChangePasswordsPage_ConfirmPassword = "//*/td/div/div/table/tbody/tr[3]/td[2]/input";
    static String ChangePasswordsPage_NewPassword     = "//*/td/div/div/table/tbody/tr[2]/td[2]/input";
    static String ChangePasswordsPage_Submit          = "//*[@id='submitBtn']";

    static String SearchPage_AdvancedSearchButton = "//*[@id='button-1050']";
    static String SearchPage_SearchBox            = "//*[@id='searchfield-1046-inputEl']";
    static String SearchPage_NameFirstRow         = "//*[@id='gridview-1042']/table/tbody/tr[2]/td[1]/div";

    static String MainPage_Url              = "https://l1iiq.wgu.edu/iiq/dashboard.jsf";
    static String MainPage_AccessReviews    = "//*[@id='categoryCompliance']/ul/li[1]/a";
    static String MainPage_PolicyViolations = "//*[@id='categoryCompliance']/ul/li[2]/a";
    static String MainPage_ChangePassword              = "//*[@id='categoryCustom']/ul/li[1]/a";
    static String MainPage_EditAuthenticationQuestions = "//*[@id='categoryCustom']/ul/li[2]/a";
    static String MainPage_Approvals        = "//*[@id='categoryTasks']/ul/li[1]/a";
    static String MainPage_SignOffReports   = "//*[@id='categoryTasks']/ul/li[2]/a";
    static String MainPage_WorkItems        = "//*[@id='categoryTasks']/ul/li[3]/a";
    static String MainPage_RequestAccess    = "//*[@id='categoryAccess']/ul/li[1]/a";
    static String MainPage_ManageAccounts   = "//*[@id='categoryAccess']/ul/li[2]/a";
    static String MainPage_ChangeOtherPasswords = "//*[@id='categoryAccess']/ul/li[3]/a";
    static String MainPage_TrackMyRequests  = "//*[@id='categoryAccess']/ul/li[4]/a";
    static String MainPage_EditIdentity     = "//*[@id='categoryManage']/ul/li[1]/a";
    static String MainPage_ViewIdentity     = "//*[@id='categoryManage']/ul/li[2]/a";
    static String MainPage_RefreshIdentity  = "//*[@id='categoryManage']/ul/li[3]/a";

}



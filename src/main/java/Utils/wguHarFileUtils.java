
package Utils;

import org.testng.TestNGException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;

/*
 * Created by timothy.hallbeck on 3/8/2015.
 */

public class wguHarFileUtils {

    protected ArrayList<String> urlList       = new ArrayList<>();
    protected ArrayList<String> blockedList   = new ArrayList<>();
    protected ArrayList<String> dnsList       = new ArrayList<>();
    protected ArrayList<String> connectList   = new ArrayList<>();
    protected ArrayList<String> sendList      = new ArrayList<>();
    protected ArrayList<String> waitList      = new ArrayList<>();
    protected ArrayList<String> receiveList   = new ArrayList<>();
    protected ArrayList<String> sslList       = new ArrayList<>();
    protected ArrayList<String> totalList     = new ArrayList<>();
    protected ArrayList<String> errorList     = new ArrayList<>();
    protected ArrayList<String> headerList    = new ArrayList<>();
    protected ArrayList<String> PageTimesList = new ArrayList<>();

    public wguHarFileUtils() {
        headerList.add("blocked");
        headerList.add("dns");
        headerList.add("connect");
        headerList.add("send");
        headerList.add("wait");
        headerList.add("receive");
        headerList.add("ssl");
        headerList.add("error");
    }

    public wguHarFileUtils ReadPageTimingInfo(String filename) throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(filename));
        String line;

        while ((line = br.readLine()) != null) {
            if (line.contains("\"onLoad\":")) {
                line = line.replace("\"onLoad\": ", "");
                line = String.valueOf((int) Float.parseFloat(line));
                this.PageTimesList.add(line);
            } else if (line.contains("\"title\": ")) {
                line = line.replace("\"title\": ", "");
                line = line.replace("\"", "");
                line = line.replace(",", "");
                line = line.replace(" ", "");
                this.urlList.add(line);
            }
        }
        br.close();

        return this;
    }

    public wguHarFileUtils ReadUrlTimingInfo (String filename) throws Exception{
        return ReadUrlTimingInfo(filename, null);
    }

    public wguHarFileUtils ReadUrlTimingInfo (String filename, String urlCriteria) throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(filename));
        assert(br != null);
        String line, potentialUrl="", potentialStatus="";

        int pageRef=1;
        int total=0;

        while ((line = br.readLine()) != null) {
            if (line.contains("\"url\":")) {
                line = line.replace(" ", "");
                line = line.replace("\"", "");
                line = line.replace(",", "");
                line = line.replace("url:", "");
                potentialUrl = line;
            } else if (line.contains("\"blocked\"")) {
                if (line.contains("-1") || (urlCriteria != null && !potentialUrl.contains(urlCriteria))) {
                    br.readLine();
                    br.readLine();
                    br.readLine();
                    br.readLine();
                    br.readLine();
                    br.readLine();
                    potentialUrl="";
                    potentialStatus="";
                    continue;
                }
                this.urlList.add(potentialUrl);

                line = line.replace(" ", "");
                line = line.replace("\"", "");
                line = line.replace(",", "");
                line = line.replace("blocked:", "");
                line = String.valueOf((int) Float.parseFloat(line));
                this.blockedList.add(line);
                total += Integer.valueOf(line);
            } else if (line.contains("\"dns\"")) {
                this.errorList.add(potentialStatus);
                line = line.replace(" ", "");
                line = line.replace("\"", "");
                line = line.replace(",", "");
                line = line.replace("dns:", "");
                line = line.replace("-1", "0");
                line = String.valueOf((int) Float.parseFloat(line));
                this.dnsList.add(line);
                total += Integer.valueOf(line);
            } else if (line.contains("\"connect\"")) {
                line = line.replace(" ", "");
                line = line.replace("\"", "");
                line = line.replace(",", "");
                line = line.replace("connect:", "");
                line = line.replace("-1", "0");
                line = String.valueOf((int) Float.parseFloat(line));
                this.connectList.add(line);
                total += Integer.valueOf(line);
            } else if (line.contains("\"send\"")) {
                line = line.replace(" ", "");
                line = line.replace("\"", "");
                line = line.replace(",", "");
                line = line.replace("send:", "");
                line = line.replace("-1", "0");
                line = String.valueOf((int) Float.parseFloat(line));
                this.sendList.add(line);
                total += Integer.valueOf(line);
            } else if (line.contains("\"wait\"")) {
                line = line.replace(" ", "");
                line = line.replace("\"", "");
                line = line.replace(",", "");
                line = line.replace("wait:", "");
                line = line.replace("-1", "0");
                line = String.valueOf((int) Float.parseFloat(line));
                this.waitList.add(line);
                total += Integer.valueOf(line);
            } else if (line.contains("\"receive\"")) {
                line = line.replace(" ", "");
                line = line.replace("\"", "");
                line = line.replace(",", "");
                line = line.replace("receive:", "");
                line = line.replace("-1", "0");
                line = String.valueOf((int) Float.parseFloat(line));
                this.receiveList.add(line);
                total += Integer.valueOf(line);
            } else if (line.contains("\"statusText\"")) {
                line = line.replace(": ", "");
                line = line.replace("\"", "");
                line = line.replace(",", "");
                line = line.replace("statusText", "");
                line = line.replace("OK", "");
                line = line.trim();
                potentialStatus = line;
            } else if (line.contains("\"ssl\"")) {
                line = line.replace(" ", "");
                line = line.replace("\"", "");
                line = line.replace(",", "");
                line = line.replace("ssl:", "");
                line = line.replace("-1", "0");
                line = String.valueOf((int) Float.parseFloat(line));
                this.sslList.add(line);
                total += Integer.valueOf(line);

                // This is the last item in our row, so write and reset our row total here
                this.totalList.add(String.valueOf(total));
                total = 0;
            } else if (line.contains("pageref") && !line.contains("page_" + pageRef) && this.urlList.size() > 0) {
                int size = this.urlList.size() - 1;
                pageRef++;

                this.urlList.add(this.urlList.get(size));
                this.urlList.set(size, "Page Change");

                this.blockedList.add(this.blockedList.get(size));
                this.blockedList.set(size, "0");

                this.dnsList.add(this.dnsList.get(size));
                this.dnsList.set(size, "0");

                this.connectList.add(this.connectList.get(size));
                this.connectList.set(size, "0");

                this.sendList.add(this.sendList.get(size));
                this.sendList.set(size, "0");

                this.waitList.add(this.waitList.get(size));
                this.waitList.set(size, "0");

                this.receiveList.add(this.receiveList.get(size));
                this.receiveList.set(size, "0");

                this.sslList.add(this.sslList.get(size));
                this.sslList.set(size, "0");

                this.totalList.add(this.totalList.get(size));
                this.totalList.set(size, "0");

                this.errorList.add(this.errorList.get(size));
                this.errorList.set(size, "0");
            }
        }
        br.close();
        Thread.sleep(2000);

        return this;
    }

    public wguHarFileUtils WriteArraysToConsole() throws Exception {
        // Output things in nice columnar Excel format
        General.Debug("\nUrls");
        for (String singleEntry : urlList)
            General.Debug(singleEntry);

        General.Debug("\nblocked");
        for (String singleEntry : blockedList)
            General.Debug(singleEntry);

        General.Debug("\ndns");
        for (String singleEntry : dnsList)
            General.Debug(singleEntry);

        General.Debug("\nconnect");
        for (String singleEntry : connectList)
            General.Debug(singleEntry);

        General.Debug("\nsend");
        for (String singleEntry : sendList)
            General.Debug(singleEntry);

        General.Debug("\nwait");
        for (String singleEntry : waitList)
            General.Debug(singleEntry);

        General.Debug("\nreceive");
        for (String singleEntry : receiveList)
            General.Debug(singleEntry);

        General.Debug("\nssl");
        for (String singleEntry : sslList)
            General.Debug(singleEntry);

        General.Debug("\ntotal");
        for (String singleEntry : totalList)
            General.Debug(singleEntry);

        General.Debug("\nPage Times");
        for (String singleEntry : PageTimesList)
            General.Debug(singleEntry);

        General.Debug("\nCombo:");
        Iterator urlIterator = urlList.iterator();
        Iterator totalIterator = totalList.iterator();
        Iterator errorIterator = errorList.iterator();

        while (urlIterator.hasNext() && totalIterator.hasNext() && errorIterator.hasNext()) {
            General.Debug(totalIterator.next() + "ms " + urlIterator.next() + " " + errorIterator.next());
        }

        return this;
    }

    public static wguHarFileUtils MergePageTimingData(wguHarFileUtils harData1, wguHarFileUtils harData2) {
        wguHarFileUtils mergedHarData = new wguHarFileUtils();
        int iArraySize = harData1.PageTimesList.size();

        for (int i=0; i<iArraySize; i++) {
            mergedHarData.urlList.add("Before " + harData1.urlList.get(i));
            mergedHarData.urlList.add("After " + harData2.urlList.get(i));
            mergedHarData.urlList.add(" - ");

            mergedHarData.PageTimesList.add(harData1.PageTimesList.get(i));
            mergedHarData.PageTimesList.add(harData2.PageTimesList.get(i));
            mergedHarData.PageTimesList.add("0");
        }

        return mergedHarData;
    }

    public static wguHarFileUtils MergeUrlTimingData(wguHarFileUtils harData1, wguHarFileUtils harData2) {
        wguHarFileUtils mergedHarData = new wguHarFileUtils();
        int iArraySize = harData1.urlList.size();

        if (iArraySize != harData2.urlList.size())
            throw new TestNGException("Data ArrayLists in MergeUrlTimingData are not the same size.");

        for (int i=0; i<iArraySize; i++) {
            mergedHarData.urlList.add(harData1.urlList.get(i));
            mergedHarData.urlList.add(harData2.urlList.get(i));
            mergedHarData.urlList.add("div");

            mergedHarData.blockedList.add(harData1.blockedList.get(i));
            mergedHarData.blockedList.add(harData2.blockedList.get(i));
            mergedHarData.blockedList.add("0");

            mergedHarData.dnsList.add(harData1.dnsList.get(i));
            mergedHarData.dnsList.add(harData2.dnsList.get(i));
            mergedHarData.dnsList.add("0");

            mergedHarData.connectList.add(harData1.connectList.get(i));
            mergedHarData.connectList.add(harData2.connectList.get(i));
            mergedHarData.connectList.add("0");

            mergedHarData.sendList.add(harData1.sendList.get(i));
            mergedHarData.sendList.add(harData2.sendList.get(i));
            mergedHarData.sendList.add("0");

            mergedHarData.waitList.add(harData1.waitList.get(i));
            mergedHarData.waitList.add(harData2.waitList.get(i));
            mergedHarData.waitList.add("0");

            mergedHarData.receiveList.add(harData1.receiveList.get(i));
            mergedHarData.receiveList.add(harData2.receiveList.get(i));
            mergedHarData.receiveList.add("0");

            mergedHarData.sslList.add(harData1.sslList.get(i));
            mergedHarData.sslList.add(harData2.sslList.get(i));
            mergedHarData.sslList.add("0");

            mergedHarData.totalList.add(harData1.totalList.get(i));
            mergedHarData.totalList.add(harData2.totalList.get(i));
            mergedHarData.totalList.add("0");

            mergedHarData.errorList.add(harData1.errorList.get(i));
            mergedHarData.errorList.add(harData2.errorList.get(i));
            mergedHarData.errorList.add("0");
        }

        return mergedHarData;
    }

}

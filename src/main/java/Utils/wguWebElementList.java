
package Utils;

import org.openqa.selenium.WebElement;
import java.util.ArrayList;

/*
 * Created by timothy.hallbeck on 6/29/2015.
 */

//
// Not currently in use, as I dont know how to convert from List to ArrayList, and List is what webdriver uses everywhere.
//
public class wguWebElementList extends ArrayList<WebElement> {
    wguWebElementList() {
        super();
    }

    boolean containsTextItem(String text) {
        if (getWebElement(text) == null)
            return false;
        return true;
    }

    WebElement getWebElement(String text) {
        General.Debug("Verifying existence of " + text);

        for (WebElement element : this) {
            String elementText = element.getText();
            if (elementText.contains(text))
                return element;
        }

        return null;
    }
}


package Utils;

import BaseClasses.GlobalData;

import java.io.*;

/*
 * Created by timothy.hallbeck on 2/3/2016.
 */

public class wguCsvFileUtils {
    wguCsvFileUtils() {}

    public static void appendDataToFile(String data, String file) {
        try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(GlobalData.DATA_PATH + file, true)))) {
            out.println(data);
        } catch (IOException e) {
            General.Debug(e.getMessage());
        }
    }

}


package Utils;

import org.apache.commons.lang3.time.StopWatch;

/*
 * Created by timothy.hallbeck on 2/3/2015.
 */

public class wguTestWrapper extends StopWatch {

    public wguTestWrapper() {
        super();
    }

    public void startTimer() {
        this.reset();
        this.start();
    }

    public long stopAndLogTime(String displayText) {
        return stopAndLogTime(displayText, false);
    }

    public long stopAndLogTime(String displayText, boolean asIs) {
        if (!this.isStopped())
            this.stop();

        long millis = this.getTime();
         if (millis > 0) {
            if (asIs) {
                General.Debug(displayText);
            } else {
                if (millis == 1)
                    General.Debug(displayText + ": " + millis + " ms\n");
                else
                    General.Debug(displayText + ": " + millis + " ms\n");
            }
        }
        return millis;
    }

}

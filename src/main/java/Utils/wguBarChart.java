
package Utils;

/*
 * Created by timothy.hallbeck on 9/29/2016.
 */

import BaseClasses.LoginType;
import JsonDataClasses.JsonServiceMetrics;
import ServiceClasses.ServiceNames;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.awt.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class wguBarChart extends Application {
    private static final int  MAX_DISPLAYED_DATA_POINTS = 60;
    private static final long INTERVAL_IN_MILLISECONDS  = 1000;

    private final String userName;
    private final LoginType login;
    private final String serviceName;
    private final String serviceDisplayName;

    private String recentValue = "";
    private javafx.scene.control.Label recentValueLabel = null;
    private Series series;
    private int xSeriesCounter = 0;
    private ConcurrentLinkedQueue<Number> dataQ = new ConcurrentLinkedQueue<>();
    private ExecutorService executor;
    private NumberAxis xAxis;
    private ComboBox<String> actionCombo = null;
    private Button pingButton;
    private LineChart<Number, Number> sc = null;
    private OssoSession session = new OssoSession();

    public wguBarChart(String serviceDisplayName, String userName) {
        this.serviceDisplayName = serviceDisplayName;
        this.serviceName = ServiceNames.Find(serviceDisplayName).getAppName();
        this.userName = userName;
        this.login = LoginType.Find(userName);
    }

    public void load(String[] comboFiller) {
        InitActionCombo(comboFiller);
    }

    private void init(Stage primaryStage) {
        InitPingButton();

        xAxis = new NumberAxis(0, MAX_DISPLAYED_DATA_POINTS, MAX_DISPLAYED_DATA_POINTS / 10);
        xAxis.setForceZeroInRange(false);
        xAxis.setAutoRanging(false);

        NumberAxis yAxis = new NumberAxis();
        yAxis.setAutoRanging(true);

        sc = new LineChart<Number, Number>(xAxis, yAxis) {
            // Override to remove symbols on each data point
            @Override
            protected void dataItemAdded(Series<Number, Number> series, int itemIndex, Data<Number, Number> item) {
            }
        };
        sc.setAnimated(false);
        sc.setId("liveAreaChart");
        sc.setTitle(serviceDisplayName + " - " + userName);

        series = new LineChart.Series<Number, Number>();
        series.setName(actionCombo.getValue());
        sc.getData().add(series);

        VBox root = new VBox();
        GridPane grid1 = new GridPane();

        int iRow = 0;
        grid1.add(new javafx.scene.control.Label("       "), 0, iRow++);
        grid1.add(new javafx.scene.control.Label("         Select what to watch        "), 0, iRow++);
        grid1.add(actionCombo, 0, iRow++);
        grid1.add(new javafx.scene.control.Label("  "), 0, iRow++);
        recentValueLabel = new javafx.scene.control.Label("");
        grid1.add(recentValueLabel, 0, iRow++);
        grid1.add(new javafx.scene.control.Label("     "), 0, iRow++);
        grid1.add(pingButton, 0, iRow);

        root.getChildren().addAll(sc, grid1);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setTitle(serviceDisplayName);

        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                if (t.getEventType().getName().equals("WINDOW_CLOSE_REQUEST"))
                    try {
                        stop();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        });
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        init(primaryStage);
        primaryStage.show();

        executor = Executors.newCachedThreadPool();
        executor.execute(new AddToQueue());

        prepareTimeline();
    }

    @Override
    public void stop() throws Exception {
        timer.stop();
        executor.shutdown();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void ResetAreaChart() {
        sc.getData().clear();
        series = new LineChart.Series<Number, Number>();
        series.setName(actionCombo.getValue());

        sc.getData().add(series);

        UpdateRecentValue();
    }

    private void UpdateRecentValue() {
        recentValueLabel.setText(" Most recent value: " + recentValue);
    }

    private void InitPingButton() {
        pingButton = new Button(" \"Ping\" ");
        pingButton.setOnAction(event -> {
                    String name = event.getEventType().getName();
                    if (name.equals("ACTION")) {
                        Toolkit.getDefaultToolkit().beep();
                    }
                }
        );
    }

    private void InitActionCombo(String[] array) {
        ObservableList<String> options =
                FXCollections.observableArrayList(array);
        actionCombo = new ComboBox<>(options);

        // mem may not be present
        actionCombo.setValue("mem.free");
        if (!actionCombo.getValue().equals("mem.free"))
            actionCombo.setValue(options.get(0));

        actionCombo.setOnAction(event -> {
                    try {
                        if (event.getEventType().getName().equals("ACTION")) {
                            timer.stop();
                            General.Sleep(INTERVAL_IN_MILLISECONDS);
                            dataQ.clear();
                            ResetAreaChart();
                            timer.start();
                        }
                    } catch (Exception e) {
                        General.Debug(e.getMessage());
                    }
                }
        );
    }

    // Do whatever call out to get data here, its in its own thread
    // Ensure that silent mode is on
    private class AddToQueue implements Runnable {
        public void run() {
            try {
                if (executor.isShutdown())
                    return;

                if (xSeriesCounter % 180 == 179) {
                    xSeriesCounter = 1;
                    General.ClearTokens();
                }

                recentValue = session.Get(serviceName, "/metrics/" + actionCombo.getValue(), login);
                if (recentValue.length() > 4) {
                    recentValue = recentValue.substring(recentValue.indexOf("\":") + 2);
                    recentValue = recentValue.substring(0, recentValue.length() - 1);
                } else
                    recentValue = "";

                dataQ.add(Long.valueOf(recentValue));
                Thread.sleep(INTERVAL_IN_MILLISECONDS);
                executor.execute(this);
            } catch (Exception e) {
                General.Debug(e.getMessage());
            }
        }
    }

    static AnimationTimer timer;
    //-- Timeline gets called in the JavaFX Main thread
    private void prepareTimeline() {
        // Every frame to take any data from queue and add to chart
        timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                addDataToSeries();
                UpdateRecentValue();
            }
        };
        timer.start();
    }

    private void addDataToSeries() {

        if (!dataQ.isEmpty()) {
            Long value = (Long) dataQ.remove();

            series.getData().add(new LineChart.Data(xSeriesCounter++, value));
        }
        // remove points to keep us at no more than MAX_DISPLAYED_DATA_POINTS
        if (series.getData().size() > MAX_DISPLAYED_DATA_POINTS) {
            series.getData().remove(0, series.getData().size() - MAX_DISPLAYED_DATA_POINTS);
        }

        // update
        xAxis.setLowerBound(xSeriesCounter - MAX_DISPLAYED_DATA_POINTS);
        xAxis.setUpperBound(xSeriesCounter - 1);
    }

}
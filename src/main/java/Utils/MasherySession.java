
package Utils;

import BaseClasses.LoginType;
import org.apache.commons.lang3.ArrayUtils;

/*
 * Created by timothy.hallbeck on 6/13/2016.
 */

public class MasherySession extends wguCurlExecuter {

    static protected String devBearer  = "";
    static protected String qaBearer   = "";
    static protected String prodBearer = "";

    static public void ClearTokens() {
        devBearer   = "";
        qaBearer  = "";
        prodBearer = "";
        General.Sleep(100); // cant get new tokens too often
    }

    public String getToken(LoginType login) {
        setUsername(login.getUsername());
        setPassword(login.getPassword());

        if (login.isLane1())
            return getQaBearer();
        else if (login.isLane2())
            return getDevBearer();
        return getProdBearer();
    }

    protected String getDevBearer() {
        if (!devBearer.isEmpty())
            return devBearer;

        String[] fullCommand = ArrayUtils.addAll(new String[]{
                "curl", "-k", "-v", "-X", "POST",
                "\"https://api.dev.wgu.edu/oauthToken/v1/oauthTokenWithCredentials?api_key=mfnun94zmb6nkt6zwtwnc8r8\"",
                "-H", "\"Content-Type: application/json\"",
                "-d", "\"{\\\"username\\\":\\\"" + username + "\\\",\\\"password\\\":\\\"" + password + "\\\"}\"",
        } );

        General.Debug("Getting new dev bearer\n" + General.StringArrayToString(fullCommand, " "));

        String result = executeCurl(fullCommand);
        devBearer = verifyKey(result, "access_token");

        return devBearer;
    }

    protected String getQaBearer() {
        if (!qaBearer.isEmpty())
            return qaBearer;

        String[] fullCommand = ArrayUtils.addAll(new String[]{
                "curl", "-k", "-v", "-X", "POST",
                "\"https://api.qa.wgu.edu/oauthToken/v1/oauthTokenWithCredentials?api_key=pg2sjmj6n5x4kf2y3sxwzph3\"",
                "-H", "\"Content-Type: application/json\"",
                "-d", "{\\\"username\\\":\\\"" + username + "\\\",\\\"password\\\":\\\"" + password + "\\\"}",
        } );

        General.Debug("Getting new qa bearer\n" + General.StringArrayToString(fullCommand, " "));

        String result = executeCurl(fullCommand);
        qaBearer = verifyKey(result, "access_token");

        return qaBearer;
    }

    protected String getProdBearer() {
        if (!prodBearer.isEmpty())
            return prodBearer;

        String[] fullCommand = ArrayUtils.addAll(new String[]{
                "curl", "-k", "-v", "-X", "POST",
                "\"https://apim.wgu.edu/oauthToken/v1/oauthTokenWithCredentials?api_key=h9wudtvftx8hsyt5nhwukwf5\"",
                "-H", "\"Content-Type: application/json\"",
                "-d", "{\\\"username\\\":\\\"" + username + "\\\",\\\"password\\\":\\\"" + password + "\\\"}",
        } );

        General.Debug("Getting new prod bearer\n" + General.StringArrayToString(fullCommand, " "));

        String result = executeCurl(fullCommand);
        prodBearer = verifyKey(result, "access_token");

        return prodBearer;
    }

    public String Get(String command, LoginType login) {
        return Get(command, login, null);
    }
    public String Get(String command, LoginType login, String[] searchTerms) {
        String  bearer="", prefix="", extraKey="";

        setUsername(login.getUsername());
        setPassword(login.getPassword());

        if (command.getBytes()[0] == '/')
            command = command.substring(1);
        command = login.expandMacros(command);

        if (login.getEnv() == wguEnvironment.Lane2Dev) {
            bearer = getDevBearer();
            if(!command.contains("http"))
                prefix = "https://api.dev.wgu.edu/";
            extraKey = "\"api_key: e5uynbx8y282yv7a44k8bbp4\"";
        } else if (login.getEnv() == wguEnvironment.Lane1Qa) {
            bearer = getQaBearer();
            if(!command.contains("http"))
                prefix = "https://api.qa.wgu.edu/";
            extraKey = "\"api_key: fkuurempkb9jw7qyubmcsv4p\"";
        } else if (login.getEnv() == wguEnvironment.Prod){
            bearer = getProdBearer();
            if(!command.contains("http"))
                prefix = "https://apim.wgu.edu/";
            extraKey = "\"api_key: 3fy47c6dj6jrmezz5y2bxd6f\"";
        }

        String[] fullCommand = ArrayUtils.addAll(new String[] { "curl", "-k", "-v" });
        fullCommand = ArrayUtils.addAll(fullCommand, "-H", extraKey);

        fullCommand = ArrayUtils.addAll(
            fullCommand, "-G",
            "-H", "\"Authorization: Bearer " + bearer + "\"",
            "\"" + prefix + command + "\"");

        String output = General.StringArrayToString(fullCommand, " ");
        General.Debug(output);
        if (searchTerms != null)
            searchTerms[0] = output;
        String result = executeCurl(fullCommand);

        return result;
    }

    public String Post(String command, LoginType login, String data) {
        String  bearer="", prefix="", extraKey="";

        setUsername(login.getUsername());
        setPassword(login.getPassword());

        command = login.expandMacros(command);

        // Lane1Qa Ping key   d2d1X21vYmlsZTpzWmtCajZGZ1gxOXZ0dFdkd1llS1Y5UjJHRmFGRXRzczZ5RzZ5cmtlRWlhdGREVnF6WnF4RFJDcENpbXlRRlZG
        if (login.getEnv() == wguEnvironment.Lane2Dev) {
            bearer = getDevBearer();
            if (!command.toLowerCase().contains("http"))
                prefix = "https://api.dev.wgu.edu/";
            if (command.contains("//mashery."))
                extraKey = "\"api_key: e5uynbx8y282yv7a44k8bbp4\"";
        } else if (login.getEnv() == wguEnvironment.Lane1Qa) {
            bearer = getQaBearer();
            if (!command.toLowerCase().contains("http"))
                prefix = "https://api.qa.wgu.edu/";
            if (command.contains("//mashery."))
                extraKey = "\"api_key: fkuurempkb9jw7qyubmcsv4p\"";
        } else if (login.getEnv() == wguEnvironment.Prod){
            bearer = getProdBearer();
            if (!command.toLowerCase().contains("http"))
                prefix = "https://apim.wgu.edu/";
            if (command.contains("//mashery."))
                extraKey = "\"api_key: 3fy47c6dj6jrmezz5y2bxd6f\"";
        }

        String[] fullCommand = ArrayUtils.addAll(new String[] { "curl", "-k", "-v", "-X", "POST" });
        if (!extraKey.isEmpty())
            fullCommand = ArrayUtils.addAll(fullCommand, "-H", extraKey);

        fullCommand = ArrayUtils.addAll(
            fullCommand, "\"" + prefix + command + "\"",
            "-H", "\"Content-Type: application/json\"",
            "-d", data,
            "-H", "\"Authorization: Bearer " + bearer + "\"");

        General.Debug(General.StringArrayToString(fullCommand, " "));
        String result = executeCurl(fullCommand);

        return result;
    }

    public String Put(String command, LoginType login) {
        return Put(command, login, null);
    }
    public String Put(String command, LoginType login, String data) {
        String  bearer="", prefix="", extraKey="";

        setUsername(login.getUsername());
        setPassword(login.getPassword());

        command = login.expandMacros(command);

        // Lane1Qa Ping key   d2d1X21vYmlsZTpzWmtCajZGZ1gxOXZ0dFdkd1llS1Y5UjJHRmFGRXRzczZ5RzZ5cmtlRWlhdGREVnF6WnF4RFJDcENpbXlRRlZG
        if (login.getEnv() == wguEnvironment.Lane2Dev) {
            bearer = getDevBearer();
            if (!command.toLowerCase().contains("http"))
                prefix = "https://api.dev.wgu.edu/";
            if (command.contains("//mashery."))
                extraKey = "\"api_key: e5uynbx8y282yv7a44k8bbp4\"";
        } else if (login.getEnv() == wguEnvironment.Lane1Qa) {
            bearer = getQaBearer();
            if (!command.toLowerCase().contains("http"))
                prefix = "https://api.qa.wgu.edu/";
            if (command.contains("//mashery."))
                extraKey = "\"api_key: fkuurempkb9jw7qyubmcsv4p\"";
        } else if (login.getEnv() == wguEnvironment.Prod){
            bearer = getProdBearer();
            if (!command.toLowerCase().contains("http"))
                prefix = "https://apim.wgu.edu/";
            if (command.contains("//mashery."))
                extraKey = "\"api_key: 3fy47c6dj6jrmezz5y2bxd6f\"";
        }

        String[] fullCommand = ArrayUtils.addAll(new String[] { "curl", "-k", "-v", "-X", "PUT"});
        if (!extraKey.isEmpty())
            fullCommand = ArrayUtils.addAll(fullCommand, "-H", extraKey);

        fullCommand = ArrayUtils.addAll(
                fullCommand,
                "-H", "\"Authorization: Bearer " + bearer + "\"",
                "\"" + prefix + command + "\"");

        if (data != null)
            fullCommand = ArrayUtils.addAll(
                    fullCommand,
                    "-H", "\"Content-Type: application/json\"",
                    "-d", data);

        General.Debug(General.StringArrayToString(fullCommand, " "));
        String result = executeCurl(fullCommand);

        return result;
    }
}

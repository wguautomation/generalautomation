
package Utils;

import org.testng.annotations.DataProvider;

public class wguDataProviders {
    @DataProvider(parallel = false, name = "LRPs")
    public static Object[][] LRPs() {
        return new Object[][]{
                {"47880058", "MindEdge", "//*[@id=\"begin_course_btn\"]"},
                {"2159", "MindEdge", "//*[@id=\"MindEdge_copyright\"]/small"},
                {"48028536", "MindEdge", "//*[@id=\"theme-main-col-copyright\"]"},
                {"1080363", "MindEdge", "//*[@id=\"MindEdge_copyright\"]/small"},
                {"2290", "MindEdge", "//*[@id=\"theme-main-col-copyright\"]"},
                {"1080375", "MindEdge", "//*[@id=\"MindEdge_copyright\"]/small"},
                {"2229", "MindEdge", "//*[@id=\"theme-main-col-copyright\"]"},
                {"48030902", "MindEdge", "//*[@id=\"theme-main-col-copyright\"]"},
                {"40263232", "MindEdge", "//*[@id=\"theme-main-col-copyright\"]"},
                {"48028957", "MindEdge", "//*[@id=\"theme-main-col-copyright\"]"},

//            { "264153", "CourseSmart", "//*[@id=\"theme-main-col-copyright\"]" },

//            { "4630436",  "SkillSoft", "//*[@href=\"#VIEWERTOP\"]" },
//           { "1318992", "SkillSoft", "//*[@href=\"#VIEWERTOP\"]" },
//            { "18423576",  "SkillSoft", "//*[@href=\"#VIEWERTOP\"]" },
//            { "95759",     "SkillSoft", "//*[@href=\"#VIEWERTOP\"]" },
//            { "738",       "SkillSoft", "//*[@href=\"#VIEWERTOP\"]" },
//            { "5531641",   "SkillSoft", "//*[@href=\"#VIEWERTOP\"]" },
//            { "376190",    "SkillSoft", "//*[@href=\"#VIEWERTOP\"]" },
//            { "4630501",   "SkillSoft", "//*[@href=\"#VIEWERTOP\"]" },
//            { "42743492",  "SkillSoft", "//*[@href=\"#VIEWERTOP\"]" },
//            { "102605",    "SkillSoft", "//*[@href=\"#VIEWERTOP\"]" },

                {"20834", "YouTubeProduction", "//*[@id=\"watch8-secondary-actions\"]/span/button/span"},
                {"660873", "YouTubeProduction", "//*[@id=\"watch8-secondary-actions\"]/span/button/span"},
                {"29030405", "YouTubeProduction", "//*[@id=\"watch8-secondary-actions\"]/span/button/span"},

                {"2570", "Soomo", "//*[@id=\"toc-copyright\"]"},
                {"2944720", "Soomo", "//*[@id=\"toc-copyright\"]"},
                {"624841", "Soomo", "/html/body/div[2]/div/input"},
                {"661673", "Soomo", "//*[@id=\"toc-copyright\"]"},
                {"2039518", "Soomo", "//*[@id=\"lower-prev-next-links\"]/div[4]/a/span[1]"},
                {"10321504", "Soomo", "//*[@id=\"toc-copyright\"]"},
                {"10020", "Soomo", "//*[@id=\"toc-copyright\"]"},
                {"661728", "Soomo", "//*[@id=\"toc-copyright\"]"},
                {"2945468", "Soomo", "//*[@id=\"toc-copyright\"]"},
                {"847165", "Soomo", "//*[@id=\"toc-copyright\"]"},

                {"707779", "Thinkwell", "/html/body/table/tbody/tr[2]/td/div/span[2]/img"},
                {"708351", "Thinkwell", "/html/body/table/tbody/tr[2]/td/div/span[2]/img"},
                {"707849", "Thinkwell", "/html/body/table/tbody/tr[2]/td/div/span[2]/img"},
                {"707863", "Thinkwell", "/html/body/table/tbody/tr[2]/td/div/span[2]/img"},
                {"707802", "Thinkwell", "/html/body/table/tbody/tr[2]/td/div/span[2]/img"},
                {"707796", "Thinkwell", "/html/body/table/tbody/tr[2]/td/div/span[2]/img"},
                {"708359", "Thinkwell", "/html/body/table/tbody/tr[2]/td/div/span[2]/img"},
                {"707998", "Thinkwell", "/html/body/table/tbody/tr[2]/td/div/span[2]/img"},
                {"707979", "Thinkwell", "/html/body/table/tbody/tr[2]/td/div/span[2]/img"},
                {"707813", "Thinkwell", "/html/body/table/tbody/tr[2]/td/div/span[2]/img"},

                {"178238", "CollegeAnywhere", "//*[@id=\"bookmark\"]/h5"},
                {"178266", "CollegeAnywhere", "//*[@id=\"bookmark\"]/h5"},
                {"178273", "CollegeAnywhere", "//*[@id=\"bookmark\"]/h5"},
                {"760306", "CollegeAnywhere", "//*[@id=\"bookmark\"]/h5"},
                {"178281", "CollegeAnywhere", "//*[@id=\"bookmark\"]/h5"},
                {"760563", "CollegeAnywhere", "//*[@id=\"bookmark\"]/h5"},
                {"178288", "CollegeAnywhere", "//*[@id=\"bookmark\"]/h5"},
                {"760325", "CollegeAnywhere", "//*[@id=\"bookmark\"]/h5"},
                {"760468", "CollegeAnywhere", "//*[@id=\"bookmark\"]/h5"},
                {"760653", "CollegeAnywhere", "//*[@id=\"bookmark\"]/h5"},

                {"287559", "Cengage Gateway", "//*[@id=\"appFrameContainer\"]"},
                {"4400085", "Cengage Gateway", "//*[@id=\"appFrameContainer\"]"},
//            { "839785", "Cengage Gateway", "//*[@id=\"documentDisplay\"]/h2[1]/span[1]" },
                {"257224", "Cengage Gateway", "//*[@id=\"appFrameContainer\"]"},
                {"257236", "Cengage Gateway", "//*[@id=\"appFrameContainer\"]"},
                {"890989", "Cengage Gateway", "//*[@id=\"appFrameContainer\"]"},
                {"257205", "Cengage Gateway", "//*[@id=\"appFrameContainer\"]"},
                {"302539", "Cengage Gateway", "//*[@id=\"appFrameContainer\"]"},
                {"839812", "Cengage Gateway", "//*[@id=\"appFrameContainer\"]"},
                {"839918", "Cengage Gateway", "//*[@id=\"appFrameContainer\"]"},

//            { "448803",  "ASCD", "//*[@id=\"appFrameContainer\"]" },
//            { "1050245", "ASCD", "//*[@id=\"appFrameContainer\"]" },
//            { "448868",  "ASCD", "//*[@id=\"appFrameContainer\"]" },
//            { "447133",  "ASCD", "//*[@id=\"appFrameContainer\"]" },
//            { "448646",  "ASCD", "//*[@id=\"appFrameContainer\"]" },

                {"8658", "Teachscape", "//*[@id=\"organizationPreviewPage\"]"},
                {"8614", "Teachscape", "//*[@id=\"organizationPreviewPage\"]"},
                {"8613", "Teachscape", "//*[@id=\"organizationPreviewPage\"]"},
                {"8675", "Teachscape", "//*[@id=\"organizationPreviewPage\"]"},
                {"8612", "Teachscape", "//*[@id=\"organizationPreviewPage\"]"},
                {"8660", "Teachscape", "//*[@id=\"organizationPreviewPage\"]"},
                {"8632", "Teachscape", "//*[@id=\"organizationPreviewPage\"]"},
                {"8628", "Teachscape", "//*[@id=\"organizationPreviewPage\"]"},
                {"8678", "Teachscape", "//*[@id=\"organizationPreviewPage\"]"},
                {"650275", "Teachscape", "//*[@id=\"organizationPreviewPage\"]"},

                {"15876083", "CertificationPartners", "//*[@id=\"inf-page-header\"]/h3"},
                {"526826", "CertificationPartners", "//*[@id=\"inf-page-header\"]/h3"},
                {"526814", "CertificationPartners", "//*[@id=\"inf-page-header\"]/h3"},
                {"524937", "CertificationPartners", "//*[@id=\"inf-page-header\"]/h3"},
                {"526802", "CertificationPartners", "//*[@id=\"inf-page-header\"]/h3"},
                {"15876613", "CertificationPartners", "//*[@id=\"inf-page-header\"]/h3"},
                {"524795", "CertificationPartners", "//*[@id=\"inf-page-header\"]/h3"},
                {"15876764", "CertificationPartners", "//*[@id=\"inf-page-header\"]/h3"},
                {"524729", "CertificationPartners", "//*[@id=\"inf-page-header\"]/h3"},
                {"524687", "CertificationPartners", "//*[@id=\"inf-page-header\"]/h3"},

                {"7295130", "WileyStudentBlti", "//*table/tbody/tr/td/table/tbody/tr/td[1]/img"},
                {"22575340", "WileyStudentBlti", "//*table/tbody/tr/td/table/tbody/tr/td[1]/img"},
                {"6302151", "WileyStudentBlti", "//*table/tbody/tr/td/table/tbody/tr/td[1]/img"},
                {"6411836", "WileyStudentBlti", "//*table/tbody/tr/td/table/tbody/tr/td[1]/img"},
                {"28759464", "WileyStudentBlti", "//*table/tbody/tr/td/table/tbody/tr/td[1]/img"},
//            { "7293617",  "WileyStudentBlti", "//*table/tbody/tr/td/table/tbody/tr/td[1]/img" },
//            { "28756993", "WileyStudentBlti", "//*table/tbody/tr/td/table/tbody/tr/td[1]/img" },
//            { "28756712", "WileyStudentBlti", "//*table/tbody/tr/td/table/tbody/tr/td[1]/img" },
//           { "28755582", "WileyStudentBlti", "//*table/tbody/tr/td/table/tbody/tr/td[1]/img" },
//            { "22709159", "WileyStudentBlti", "//*table/tbody/tr/td/table/tbody/tr/td[1]/img" },

                {"1794403", "PearsonMathXL", "//*[@id=\"appFrameContainer\"]"},

//            { "2191631", "PearsonMediaAndWPS", "//*[@id=\"appFrameContainer\"]" },
//            { "2189537", "PearsonMediaAndWPS", "//*[@id=\"appFrameContainer\"]" },
                {"2190075", "PearsonMediaAndWPS", "//*[@id=\"appFrameContainer\"]"},
                {"2189418", "PearsonMediaAndWPS", "//*[@id=\"appFrameContainer\"]"},
                {"2189671", "PearsonMediaAndWPS", "//*[@id=\"appFrameContainer\"]"},
                {"4876184", "PearsonMediaAndWPS", "//*[@id=\"appFrameContainer\"]"},
//            { "4879321", "PearsonMediaAndWPS", "//*[@id=\"appFrameContainer\"]" },
                {"4880062", "PearsonMediaAndWPS", "//*[@id=\"appFrameContainer\"]"},
                {"2172402", "PearsonMediaAndWPS", "//*[@id=\"appFrameContainer\"]"},
                {"4879951", "PearsonMediaAndWPS", "//*[@id=\"appFrameContainer\"]"},
                {"4880401", "PearsonMediaAndWPS", "//*[@id=\"appFrameContainer\"]"},
//            { "4879602", "PearsonMediaAndWPS", "//*[@id=\"appFrameContainer\"]" },
                {"4880148", "PearsonMediaAndWPS", "//*[@id=\"appFrameContainer\"]"},

//            { "610800", "PearsoneText", "//*[@id=\"appFrameContainer\"]" },
                {"626769", "PearsoneText", "//*[@id=\"appFrameContainer\"]"},

                {"787971", "SkillBrief", "//*[@id=\"appFrameContainer\"]"},
                {"861545", "SkillBrief", "//*[@id=\"appFrameContainer\"]"},
                {"861567", "SkillBrief", "//*[@id=\"appFrameContainer\"]"},
                {"861520", "SkillBrief", "//*[@id=\"appFrameContainer\"]"},
//            { "861553", "SkillBrief", "//*[@id=\"appFrameContainer\"]" },

                {"45391930", "EBSCOOpenLinks", "//*[@id=\"appFrameContainer\"]"},
                {"45390488", "EBSCOOpenLinks", "//*[@id=\"appFrameContainer\"]"},
//            { "45388647", "EBSCOOpenLinks", "//*[@id=\"appFrameContainer\"]" },
                {"45390820", "EBSCOOpenLinks", "//*[@id=\"appFrameContainer\"]"},
                {"45391109", "EBSCOOpenLinks", "//*[@id=\"appFrameContainer\"]"},
//            { "45389809", "EBSCOOpenLinks", "//*[@id=\"appFrameContainer\"]" },
                {"45390604", "EBSCOOpenLinks", "//*[@id=\"appFrameContainer\"]"},
                {"8537171", "EBSCOOpenLinks", "//*[@id=\"appFrameContainer\"]"},
                {"19092963", "EBSCOOpenLinks", "//*[@id=\"appFrameContainer\"]"},
                {"45388303", "EBSCOOpenLinks", "//*[@id=\"appFrameContainer\"]"},

                {"32307086", "Docutekupdatedtolibguides", "//*[@id=\"appFrameContainer\"]"},
//            { "3871127",  "Docutekupdatedtolibguides", "//*[@id=\"appFrameContainer\"]" },
                {"3525634", "Docutekupdatedtolibguides", "//*[@id=\"appFrameContainer\"]"},
                {"10001146", "Docutekupdatedtolibguides", "//*[@id=\"appFrameContainer\"]"},
                {"12068260", "Docutekupdatedtolibguides", "//*[@id=\"appFrameContainer\"]"},
                {"11074126", "Docutekupdatedtolibguides", "//*[@id=\"appFrameContainer\"]"},
                {"34270195", "Docutekupdatedtolibguides", "//*[@id=\"appFrameContainer\"]"},
                {"1670205", "Docutekupdatedtolibguides", "//*[@id=\"appFrameContainer\"]"},
                {"14492648", "Docutekupdatedtolibguides", "//*[@id=\"appFrameContainer\"]"},
                {"1672427", "Docutekupdatedtolibguides", "//*[@id=\"appFrameContainer\"]"},

                {"1587790", "FlatworldKnowledge", "//*[@id=\"appFrameContainer\"]"},
                {"3272222", "FlatworldKnowledge", "//*[@id=\"appFrameContainer\"]"},
                {"1589703", "FlatworldKnowledge", "//*[@id=\"appFrameContainer\"]"},
                {"1589745", "FlatworldKnowledge", "//*[@id=\"appFrameContainer\"]"},
                {"3272285", "FlatworldKnowledge", "//*[@id=\"appFrameContainer\"]"},
                {"1587651", "FlatworldKnowledge", "//*[@id=\"appFrameContainer\"]"},
                {"1589924", "FlatworldKnowledge", "//*[@id=\"appFrameContainer\"]"},
                {"1589099", "FlatworldKnowledge", "//*[@id=\"appFrameContainer\"]"},
                {"1589794", "FlatworldKnowledge", "//*[@id=\"appFrameContainer\"]"},
                {"1590073", "FlatworldKnowledge", "//*[@id=\"appFrameContainer\"]"},

                {"1908843", "Panopto", "//*[@id=\"appFrameContainer\"]"},
                {"1908683", "Panopto", "//*[@id=\"appFrameContainer\"]"},
                {"1908903", "Panopto", "//*[@id=\"appFrameContainer\"]"},
                {"1908967", "Panopto", "//*[@id=\"appFrameContainer\"]"},
                {"1909035", "Panopto", "//*[@id=\"appFrameContainer\"]"},
                {"1908795", "Panopto", "//*[@id=\"appFrameContainer\"]"},

        };
    }

    @DataProvider(parallel = false, name = "cosAndOtherPagesProd")
    public static Object[][] cosAndOtherPagesProd() {
        return new Object[][]{
                {"https://cos.wgu.edu/courses/for/C301/3080001", "cosTestC301"},
                {"https://cos.wgu.edu/courses/34047/bookmark?code=C301&cvID=3080001", "cosTestBookmark"},
                {"https://my.wgu.edu/group/wgu-student-v2/cos?href=%3dhttps%3A%2F%2Fcos.wgu.edu%2F%2Fcourses%2F34047%2Fbookmark%3Fcode%3DC301%26cvID%3D3080001&", "cosTestMyBookmark"},
                {"https://cos.wgu.edu//courses/34047/bookmark?code=C301&cvID=3080001&", "cosTestRepeat"},
        };
    } // end of method cosAndOtherPagesProd

    @DataProvider(parallel = false, name = "cosAndOtherPages")
    public static Object[][] cosAndOtherPages() {
        return new Object[][]{
                {"https://l1cos.wgu.edu/courses/for/C301/3080001",},
                {"https://l1cos.wgu.edu/courses/34047/bookmark?code=C301&cvID=3080001",},
                {"https://l1my.wgu.edu/group/wgu-student-v2/cos?href=%3dhttps%3A%2F%2Fl1cos.wgu.edu%2F%2Fcourses%2F34047%2Fbookmark%3Fcode%3DC301%26cvID%3D3080001&",},
                {"https://l1cos.wgu.edu//courses/34047/bookmark?code=C301&cvID=3080001&",},
        };
    } // end of method cosAndOtherPages

    @DataProvider(parallel = false, name = "lane1Pages")
    public static Object[][] lane1Pages() {
        return new Object[][]{
                {"https://l1cos.wgu.edu/courses/for/BNC1/266", "bnc1_266"},
                {"https://l1cos.wgu.edu/courses/for/IWT1/1332", "iwti_1332"},
                {"https://l1cos.wgu.edu/courses/for/MGC1/1513", "mgc1_1513"},
                {"https://l1cos.wgu.edu/courses/for/TPV1/2028", "tpv1_2028"},
                {"https://l1cos.wgu.edu/courses/for/DPV1/20186", "dpv1_20186"},
                {"https://l1cos.wgu.edu/courses/for/GRT1/3000001", "grt1_3000001"},
        };
    } // end of method lane1Pages


    // A $ at the beginning of the 2nd string indicates this is a literal, not a json object
    static String[] AssessmentAssess =        { "assessments/v1/students/{pidm}/courses/1332/assessment/1880/assess",                          null };
    static String[] AssessmentAttempt =       { "assessments/v1/students/{pidm}/courses/1332/preassessments/1880/attempt",                     "$http://" };
    static String[] AssessmentCompetencies =  { "assessments/v1/assessments/1880/competencies",                                                "code" };
    static String[] AssessmentCourses =       { "assessments/v1/courses/1332",                                                                 "courseId" };
    static String[] AssessmentCourseVersion = { "assessments/v1/students/{pidm}/courses/1332",                                                 "studentAssessments" };
    static String[] CoachingReportPlanId =    { "coachingreport/v2/pidm/288914/assessmentid/238/studyplanid/16120?testDateSeconds=1420612913", "competencyResult" };
    static String[] CosBCourseVersionId =     { "cosb/v1/courses/3700001",                                                                     null };
    static String[] CosBCourseVersionDetail = { "cosb/v1/courses/info/1332",                                                                   "courseVersionid" };
    static String[] CourseMentorsSinglePost = { "mentor/v1/courseMentors", "{\\\"bannerCodes\\\":[{\\\"bannerCode\\\":\\\"BDT1\\\"}]}", "bannerCode" };
    static String[] CourseMentorsMultiPost =  { "mentor/v1/courseMentors", "{\\\"bannerCodes\\\":[{\\\"bannerCode\\\":\\\"BDT1\\\"},{\\\"bannerCode\\\":\\\"AYV1\\\"}]}", "bannerCode" };

    @DataProvider(parallel = false, name = "curlGETCommands")
    public static Object[][] curlGETCommands() {
        return new Object[][] {

//                { "actionitems/v2/api/student/106679/tasks",                       "id" }, // not in dev yet
//                { "actionitems/v2/api/student/106679/tasks/paged",                 "id" },
//                { "action-items/v1/pidm/302386/mentor-items", "id" },

                AssessmentAssess,
                AssessmentAttempt,
                AssessmentCompetencies,
                AssessmentCourses,
                AssessmentCourseVersion,

//                CoachingReportPlanId, // needs study plan id with assessment already done to work

                CosBCourseVersionId,
                CosBCourseVersionDetail,

                { "degreeplans/v1/degreeplans/current/pidm/106679",                "id" },

//                { "graduation/v1/graduationdate/pidm/106679",                         "graduationMonth" },

                { "mentor/v1/courseMentors/AYV1",                                  "groupEmail" },
                { "mentor/v1/information/{pidm}",                                  "name" }, // send student, returns mentor - handy
                { "mentor/v1/mentorPhoto/196661",                                   null },
                { "mentor/v1/mentorPhotoUrl/33497",                                 null },
                { "mentor/v1/mentorPhotoHiRes/285285",                              null },
                { "mentor/v1/mentors/{username}",                                 "firstName" },
                { "mentor/v1/mentors/{username}?includeOfficeHours=false",        "pidm" },

//                { "newsitems/v1/feed/106679/1417000000",                           "type" },

                { "person/v1/person",                                              "pidm" },
                { "person/v1/person/bannerId/QA0000007",                           "pidm" },
                { "person/v1/person/bannerId/QA0000007/identity",                  "pidm" },
                { "person/v1/person/bannerId/QA0000007/type",                      "$\"student\"" },
                { "person/v1/person/{username}",                                  "pidm" },
                { "person/v1/person/{username}/identity",                         "username" },
                { "person/v1/person/{username}/type",                             "$\"student\"" },
                { "person/v1/person/pidm/{pidm}",                                  "pidm" },
                { "person/v1/person/pidm/{pidm}/identity",                         "pidm" },
                { "person/v1/person/pidm/{pidm}/type",                             "$\"student\"" },

//                { "studyplan-activities/v2/user/dsmi222/studyplan/24540",          "activityId" }, // not implemented yet

//                { "wgu-pilot/v1/services/pilot/all",                               null }, // not implemented yet

                { "programProgress/v1/api/ProgramProgress/{pidm}",                 "totalProgramCu" },
                { "programProgress/v1/api/ProgramProgress/{pidm}/calculated",      "programProgress" },

                // These are now static tiles, endpoints no longer available
//                { "studentinfo/v1/students/cpoindexter",                           "pidm" },
//                { "studentinfo/v1/students/pidm/106679",                           "pidm" },

                { "studyplan/v1/bookmark/23140/{username}",                       "resourceKind" },
                { "studyplan/v1/useractivity/{username}/23140",                    null },
                { "studyplan/v1/api/courses/courseVersionId/pidm/106679",          "courseVersionId" },
                { "studyplan/v1/api/courses/getPublishedStudyPlans",               "courseVersionId" },
                { "studyplan/v1/api/courses/info/1332",                            "id" },
                { "studyplan/v1/api/courses/lastPublished/1332",                   "id" },
                { "studyplan/v1/mobileLinks/linkRatings/1332",                     "iosTabletRating" },
                { "studyplan/v1/api/courses/notes/{username}/1332",                "notes" },

                { "taskstream/v1/api/taskstream/lti/students/{pidm}/courses/TCP1", null },

                { "wguinformation/v1/contactInformation",                          "departments" },
                { "wguinformation/v1/contactInformation/lastUpdated",              "lastUpdatedLong" },
                { "wguinformation/v1/faqs",                                        "question" },
                { "wguinformation/v1/faqs/lastUpdated",                            "lastUpdatedLong" },
                { "wguinformation/v1/resources",                                   "resourceUrls" },
                { "wguinformation/v1/resources/lastUpdated",                       "lastUpdatedLong" },

                { "wsbsi/v1/sites",                                                "url" },
                { "wsbsi/v1/sites/1",                                              "url" },
                { "wsbsi/v1/sites/default",                                        "id" },
                { "wsbsi/v1/sites/lastUpdated",                                    "lastUpdated" },
                { "wsbsi/v1/sites/sponsored",                                      "url" },
                { "wsbsi/v1/students/{pidm}/connections",                          "pidm" },
//                { "wsbsi/v1/students/106679/connections/100/sites",                "pidm" }, // GET not supported?
                { "wsbsi/v1/students/{pidm}/connections/recommended",              "pidm" },

        };
    } // end of method curlCommands

    // Mashery only
    @DataProvider(parallel = false, name = "curlGETAssessmentCommands")
    public static Object[][] curlGETAssessmentCommands() {
        return new Object[][] {
                AssessmentAssess,
                AssessmentAttempt,
                AssessmentCompetencies,
                AssessmentCourses,
                AssessmentCourseVersion,
        };
    } // end of method curlGETAssessmentCommands

    // Mashery only
    @DataProvider(parallel = false, name = "curlGETCommandsUserSpecific")
    public static Object[][] curlGETCommandsUserSpecific() {
        return new Object[][] {

//                { "actionitems/v2/api/student/106679/tasks",                       "id" }, // not in dev yet
//                { "actionitems/v2/api/student/106679/tasks/paged",                 "id" },
//                { "action-items/v1/pidm/106679/mentor-items", "id" },

                AssessmentAssess,
                AssessmentAttempt,
                AssessmentCourseVersion,

                CoachingReportPlanId, // needs study plan id with assessment already done to work

                { "degreeplans/v1/degreeplans/current/pidm/106679",                "id" },

                { "mentor/v1/information/106679",                                  "name" }, // send student, returns mentor - handy
                { "mentor/v1/mentors/cpoindexter",                                 "firstName" },
                { "mentor/v1/mentors/cpoindexter?includeOfficeHours=true",         "Start" },

                { "newsitems/v1/feed/106679/1417000000",                           "type" },

                { "person/v1/person/bannerId/QA0000007",                           "pidm" },
                { "person/v1/person/bannerId/QA0000007/identity",                  "pidm" },
//                { "person/v1/person/bannerId/QA0000007/type",                      "$\"student\"" }, // type is not considered protected, so any student can see other students type
                { "person/v1/person/cpoindexter",                                  "pidm" },
                { "person/v1/person/cpoindexter/identity",                         "username" },
//                { "person/v1/person/cpoindexter/type",                             "$\"student\"" },
                { "person/v1/person/pidm/106679",                                  "pidm" },
                { "person/v1/person/pidm/106679/identity",                         "pidm" },
//                { "person/v1/person/pidm/106679/type",                             "$\"student\"" },

                { "programProgress/v1/api/ProgramProgress/106679",                 "totalProgramCu" },
                { "programProgress/v1/api/ProgramProgress/106679/calculated",      "programProgress" },

                { "studentinfo/v1/students/cpoindexter",                           "pidm" },
                { "studentinfo/v1/students/pidm/106679",                           "pidm" },

                { "studyplan/v1/bookmark/23140/cpoindexter",                       "resourceKind" },
                { "studyplan/v1/useractivity/cpoindexter/23140",                    null },
                { "studyplan/v1/api/courses/courseVersionId/pidm/106679",          "courseVersionId" },
                { "studyplan/v1/api/courses/notes/cpoindexter/1332",               "notes" },

                { "taskstream/v1/api/taskstream/lti/students/106679/courses/C281", "url" },

                { "wsbsi/v1/students/106679/connections",                          "pidm" },
//                { "wsbsi/v1/students/106679/connections/100/sites",                "pidm" }, // GET not supported?
                { "wsbsi/v1/students/106679/connections/recommended",              "pidm" },

        };
    } // end of method curlGETCommandsUserSpecific

    // OSSO only
    @DataProvider(parallel = false, name = "curlFDPDevGETCommands")
    public static Object[][] curlFDPDevGETCommands() {
        return new Object[][] {
                { "https://fdp-calculator-api.dev.wgu.edu/v1/api/calculator/{pidm}/tuitions",           "201509" },
                { "https://fdp-degreeplan-api.dev.wgu.edu/v1/api/students/{pidm}/degreeplans",          "id" },
                { "https://fdp-degreeplan-api.dev.wgu.edu/v1/api/students/{pidm}/degreeplans/message",  "messageKey" },
                { "https://fdp-degreeplan-api.dev.wgu.edu/v1/api/degreeplans/current/pidm/{pidm}",      "id" },
                { "https://fdp-degreeplan-api.dev.wgu.edu/v1/api/degreeplans/current/term/pidm/{pidm}", "courseVersionId" },
                { "https://fdp-enrollment-api.dev.wgu.edu/v1/api/enrollment/{pidm}/enrollment-model",   "degreePlanId" },
                { "https://fdp-graduation-api.dev.wgu.edu/v1/api/graduation/graduationdate/{pidm}",     "graduationMonth" },
                { "https://fdp-mentor-api.dev.wgu.edu/v1/api/mentor/mentor-info/{pidm}",                "pidm" },
                { "https://fdp-student-info-api.dev.wgu.edu/v1/api/student-info/students/{pidm}",       "pidm" },
        };
    } // end of method curlFDPDevGETCommands

    // OSSO only
    @DataProvider(parallel = false, name = "curlFDPQaGETCommands")
    public static Object[][] curlFDPQaGETCommands() {
        return new Object[][] {
                { "https://fdp-calculator-api.qa.wgu.edu/v1/api/calculator/{pidm}/tuitions",           null },
                { "https://fdp-degreeplan-api.qa.wgu.edu/v1/api/students/{pidm}/degreeplans",          "id" },
                { "https://fdp-degreeplan-api.qa.wgu.edu/v1/api/students/{pidm}/degreeplans/message",  "messageKey" },
                { "https://fdp-degreeplan-api.qa.wgu.edu/v1/api/degreeplans/current/pidm/{pidm}",      "id" },
                { "https://fdp-degreeplan-api.qa.wgu.edu/v1/api/degreeplans/current/term/pidm/{pidm}", "courseVersionId" },
                { "https://fdp-enrollment-api.qa.wgu.edu/v1/api/enrollment/{pidm}/enrollment-model",   "degreePlanId" },
                { "https://fdp-graduation-api.qa.wgu.edu/v1/api/graduation/graduationdate/{pidm}",     "graduationMonth" },
                { "https://fdp-mentor-api.qa.wgu.edu/v1/api/mentor/mentor-info/{pidm}",                "pidm" },
                { "https://fdp-student-info-api.qa.wgu.edu/v1/api/student-info/students/{pidm}",       "pidm" },
                { "https://mashery{$LANEPREFIX}.wgu.edu/mentoritems/v1/pidm/{pidm}/mentor-items",      "id" },
        };
    } // end of method curlFDPQaGETCommands

    // OSSO only
    @DataProvider(parallel = false, name = "execCurlFDPProdGETCommandsCpoindexter")
    public static Object[][] curlFDPProdGETCommandsCpoindexter() {
        return new Object[][] {
                { "https://fdp-calculator-api.wgu.edu/v1/api/calculator/106679/tuitions",           null },
                { "https://fdp-degreeplan-api.wgu.edu/v1/api/students/106679/degreeplans",          "id" },
                { "https://fdp-degreeplan-api.wgu.edu/v1/api/students/106679/degreeplans/message",  "messageKey" },
//                { "https://fdp-degreeplan-api.wgu.edu/v1/api/degreeplans/current/pidm/106679",      "id" },
//                { "https://fdp-degreeplan-api.wgu.edu/v1/api/degreeplans/current/term/pidm/106679", "courseVersionId" },
                { "https://fdp-enrollment-api.wgu.edu/v1/api/enrollment/106679/enrollment-model",   "degreePlanId" },
                { "https://fdp-graduation-api.wgu.edu/v1/api/graduation/graduationdate/106679",     "graduationMonth" },
                { "https://fdp-mentor-api.wgu.edu/v1/api/mentor/mentor-info/106679",                "pidm" },
                { "https://fdp-student-info-api.wgu.edu/v1/api/student-info/students/106679",       "pidm" },
                { "https://mashery{$LANEPREFIX}.wgu.edu/mentoritems/v1/pidm/106679/mentor-items",   "id" },
        };
    } // end of method curlFDPProdGETCommands


    // Mashery only
    @DataProvider(parallel = false, name = "curlStudentPOSTCommands")
    public static Object[][] curlStudentPOSTCommands() {
        return new Object[][] {
                { CourseMentorsSinglePost, },
                { CourseMentorsMultiPost, },
//                { "assessments/v1/students/106679/courses/1332/assessments/1880/approval",                    null }, // POST for later
//                { "assessments/v1/students/106679/courses/1332/assessments/1880/requester/196661/approval",   null }, // POST for later
//                { "assessments/v1/students/106679/courses/1332/assessments/1880/approver/196661/preapproval", null }, // POST for later
        };
    } // end of method curlMentorPOSTCommands

    // Mashery only
    @DataProvider(parallel = false, name = "curlMentorPOSTCommands")
    public static Object[][] curlMentorPOSTCommands() {
        return new Object[][] {
                { "assessments/v1/students/106679/courses/1332/assessments/1880/approval",                    null }, // POST for later
                { "assessments/v1/students/106679/courses/1332/assessments/1880/requester/196661/approval",   null }, // POST for later
                { "assessments/v1/students/106679/courses/1332/assessments/1880/approver/196661/preapproval", null }, // POST for later
        };
    } // end of method curlMentorPOSTCommands

    // Mashery only
    @DataProvider(parallel = false, name = "curlMentorGETCommands")
    public static Object[][] curlMentorGETCommands() {
        return new Object[][] {
            { "person/v1/person/cpoindexter/roles",         "pidm" }, // need mentor login
            { "person/v1/person/cpoindexter/roles/refresh", "pidm" }, // need mentor login
        };
    } // end of method curlMentorGETCommands


    // Osso only
    @DataProvider(parallel = false, name = "curlHomePageCurrentEndpoints")
    public static Object[][] curlHomePageCurrentEndpoints() {
        return new Object[][] {
                { "https://person.qa.wgu.edu/v1/person",         null },
                { "https://l1webapp47.wgu.edu/banner/services/students/program/title/cpoindexter", "programTitle" },
                { "https://l1webapp47.wgu.edu/banner/services/students/graduationdate/cpoindexter", "plannedGraduationDate" },
                { "https://l1webapp47.wgu.edu/banner/services/notifications/cpoindexter", "headers" },
                { "https://l1webapp40.wgu.edu/finance/financialAid", "sap" },
                { "https://l1webapp47.wgu.edu/banner/services/tour/cpoindexter/tween/Home/1448300336861", "overlay" },
                { "https://l1webapp47.wgu.edu/banner/services/identity/studentid/cpoindexter", "userName" },
                { "https://l1webapp47.wgu.edu/banner/services/pilot/username/cpoindexter/group/ContactMentorUser", "ContactMentorUser" },
                { "https://mashery.wgu.edu/qa/mentor/v1/mentors/cpoindexter?includeBio=true&includeOfficeHours=true", "firstName" },
                { "https://person.qa.wgu.edu/v1/person", "pidm" },
                { "https://fdp-degreeplan-api.qa.wgu.edu/v1/api/degreeplans/current/term/pidm/106679", "slimCourseModel" },
                { "https://mashery.wgu.edu/qa/mentor/v1/mentorPhotoUrl/33497", "photoUrl" },
                { "https://l1api.wgu.edu/wgu-wsbsi/v1/students/106679/connections", null },
                { "https://l1webapp47.wgu.edu/banner/services/tour/cpoindexter/tween/Home", "overlay" },
                { "https://mashery.wgu.edu/qa/mentor/v1/information/106679", "pidm" },
                { "https://programprogress.qa.wgu.edu/v1/api/ProgramProgress/106679/calculated", null },
                { "https://student-courses.qa.wgu.edu/v1/api/courses/106679/all", null },
                { "https://student-schedule.qa.wgu.edu/v1/students/106679/schedule/items", null },
                { "https://mashery.wgu.edu/qa/pilot/v1/subscription/?pilotName=MSAI&subscriberType=Student&identity=106679", null },
                { "https://student-news.qa.wgu.edu/v1/announcements/students/106679", null },
                { "https://student-email.qa.wgu.edu/v1/gmail/students/cpoindexter", null },
                { "https://student-communities.qa.wgu.edu/v1/communities/106679", null },
                { "https://programprogress.qa.wgu.edu/v1/api/ProgramProgress/106679/calculated", "programProgress" },
                { "https://student-news.qa.wgu.edu/v1/announcements/students/106679", null },
                { "https://student-email.qa.wgu.edu/v1/gmail/students/cpoindexter", "entries" },
                { "https://student-communities.qa.wgu.edu/v1/communities/106679", "name" },
                { "https://student-courses.qa.wgu.edu/v1/api/courses/106679/all", "completeCourses" },
                { "https://mashery.wgu.edu/qa/pilot/v1/subscription/?pilotName=MSAI&subscriberType=Student&identity=106679", "identity" },
                { "https://student-actionitems.qa.wgu.edu/v2/api/student/106679/tasks", null },
                { "https://mashery.wgu.edu/qa/mentoritems/v1/pidm/106679/mentor-items", "id" },

        };
    } // end of method curlMentorGETCommands


    // Osso only
    @DataProvider(parallel = false, name = "lane1PagesWithDates")
    public static Object[][] lane1PagesWithDates() {
        return new Object[][]{
                {"https://l1cos.wgu.edu/courses/for/BNC1/266", "bnc1_266_2015_04_16_13_10"},
                {"https://l1cos.wgu.edu/courses/for/IWT1/1332", "iwti_1332_2015_04_16_13_12"},
                {"https://l1cos.wgu.edu/courses/for/MGC1/1513", "mgc1_1513_2015_04_16_13_14"},
                {"https://l1cos.wgu.edu/courses/for/TPV1/2028", "tpv1_2028_2015_04_16_13_15"},
                {"https://l1cos.wgu.edu/courses/for/DPV1/20186", "dpv1_20186_2015_04_16_13_17"},
                {"https://l1cos.wgu.edu/courses/for/GRT1/3000001", "grt1_3000001_2015_04_16_13_18"},
        };
    } // end of method lane1PagesWithDates

    // Osso only
    @DataProvider(parallel = false, name = "lane1PagesNoNumber")
    public static Object[][] lane1PagesNoNumber() {
        return new Object[][]{
                {"https://l1cos.wgu.edu/courses/for/BNC1", "bnc1"},
                {"https://l1cos.wgu.edu/courses/for/IWT1", "iwti"},
                {"https://l1cos.wgu.edu/courses/for/MGC1", "mgc1"},
                {"https://l1cos.wgu.edu/courses/for/TPV1", "tpv1"},
                {"https://l1cos.wgu.edu/courses/for/DPV1", "dpv1"},
                {"https://l1cos.wgu.edu/courses/for/GRT1", "grt1"},
        };
    } // end of method lane1PagesNoNumber

    @DataProvider(parallel = false, name = "HarFilesForMerging")
    public static Object[][] HarFilesForMerging() {
        return new Object[][]{
                {"bnc1_03_09", "bnc1_2015_03_11_17_32"},
                {"iwti_03_09", "iwti_2015_03_11_17_33"},
                {"mgc1_03_09", "mgc1_2015_03_11_17_35"},
                {"tpv1_03_09", "tpv1_2015_03_11_17_36"},
                {"dpv1_03_09", "dpv1_2015_03_11_17_38"},
                {"grt1_03_09", "grt1_2015_03_11_17_39"},
        };
    } // end of method HarFilesForMerging

} // end of class wguDataProviders
























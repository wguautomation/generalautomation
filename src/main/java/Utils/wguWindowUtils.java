
package Utils;

/*
 * Created by timothy.hallbeck on 8/25/15.
 */

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;

public class wguWindowUtils {
    wguWindowUtils() {}

    // awt dimension and selenium dimension classes are not compatible, so need this helper method
    public static void setSize(WebDriver driver, int width, int height) {
        driver.manage().window().setSize(new Dimension(width, height));

    }
}


package Utils;

import org.testng.SkipException;
import java.util.ArrayList;

/*
 * Created by timothy.hallbeck on 5/9/2015.
 */

public class Course {

    private String         code="";
    private String         name="";
    private int            units=0;
    private String         url="";
    private String[]       competencies;
    private CourseModule[] modules;

    public String getCourseCode() {
        return code;
    }
    public String getCourseName() {
        return name;
    }
    public int getCourseUnits() {
        return units;
    }
    public String getCourseUrl() {
        return url;
    }
    public String[] getCourseCompetencies() {
        return competencies;
    }
    public CourseModule[] getCourseModules() {
        return modules;
    }

    Course() {}

    Course(String code, String name, int units, String url, String[] competencies) {
        this(code, name, units, url, competencies, null);
    }

    Course(String code, String name, int units, String url, String[] competencies, CourseModule[] modules) {
        this.code  = code;
        this.name  = name;
        this.units = units;
        this.url   = url;
        this.competencies = competencies;
        this.modules = modules;
    }

    public String[] getAllUrls() {
        ArrayList <String> urls = new ArrayList<>();
        urls.add(getCourseUrl());

        for (CourseModule module : modules) {
            urls.add(module.getUrl());
            for (CourseChapter chapter : module.getChapters()) {
                urls.add(chapter.getUrl());
            }
        }

        String[] array = new String[urls.size()];
        array = urls.toArray(array);
        return (array);
    }

    public static Course getCourse(String code) {
        switch(code) {
            case "ASC1":
                return new Course("ASC1", "Marketing Management Concepts", 12, "", new String[] {"", });

            case "AST1":
                return new Course("AST1", "Marketing Management Tasks", 6, "", new String[] {"", });

            case "ATO1":
                return new Course("ATO1", "Concepts in Financial Accounting and Tax", 8,
                        "https://cos.wgu.edu/courses/for/ATO1/145", new String[] {"", });

            case "C100":
                return new Course("C100", "Introduction to Humanities", 3, "", new String[] {"", });

            case "C132":
                return new Course("C132", "Elements of Effective Communication", 3, "", new String[] {"", });

            case "C168":
                return new Course("C168", "Critical Thinking and Logic", 3, "", new String[] {"", });

            case "C200":
                return new Course("C200", "Managing Organizations and Leading People", 3, "", new String[] {"", });

            case "C202":
                return new Course("C202", "Managing Human Capital", 3, "", new String[] {"", });

            case "C204":
                return new Course("C204", "Management Communication", 3, "", new String[] {"", });

            case "C211":
                return new Course("C211", "Global Economics for Managers", 3, "", new String[] {"", });

            case "C212":
                return new Course("C212", "Marketing", 3, "", new String[] {"", });

            case "C255":
                return new Course("C255", "Introduction to Geography", 3, "", new String[] {"", });

            case "C272":
                return new Course("C272", "Foundational Perspectives of Education", 3, "", new String[] {"", });

            case "C278":
                return new Course("C278", "College Algebra", 4,
                        "https://cos.wgu.edu/courses/for/C278/2000001?user=cpoindexter", new String[] {"", });

            case "C301":
                return new Course("C301", "Translational Research for Practice and Populations", 2, "", new String[] {"", });

            case "C305":
                return new Course("C305", "Foundations for Success", 0, "", new String[] {"", });

            case "C306":
                return new Course("C306", "Finite Mathematics", 4, "", new String[] {"", });

            case "C451":
                return new Course("C451", "Integrated Natural Science", 4, "", new String[] {"", });

            case "C452":
                return new Course("C452", "Integrated Natural Science Applications", 4, "", new String[] {"", });

            case "C455":
                return new Course(
                        "C455",
                        "English Composition I",
                        3,
                        "https://cos.wgu.edu/courses/for/C455/3960002",
                        new String[] {"", ""},
                        new CourseModule[] {
                                new CourseModule("Course Introduction", "https://lrps.wgu.edu/provision/40181941", new CourseChapter[] {
                                        new CourseChapter("Tips for using this learning resource", "https://wgu.mindedgeonline.com/content.php?cid=38795"),
                                }),
                                new CourseModule("Module 1: Introduction to the Essay", "https://lrps.wgu.edu/provision/40182001", new CourseChapter[] {
                                        new CourseChapter("Module 1 learning outcomes",        "https://wgu.mindedgeonline.com/content.php?cid=37771"),
                                        new CourseChapter("1.02 Module 1 Pre-Self-Assessment", "https://wgu.mindedgeonline.com/tools/test_intro.php?tid=3062"),
                                        new CourseChapter("Introduction to the essay",         "https://wgu.mindedgeonline.com/content.php?cid=37756"),
                                        new CourseChapter("Overview of different essay forms", "https://wgu.mindedgeonline.com/content.php?cid=37760"),
                                        new CourseChapter("The importance of audience",        "https://wgu.mindedgeonline.com/content.php?cid=37761"),
                                        new CourseChapter("Rhetorical analysis: An example",   "https://wgu.mindedgeonline.com/content.php?cid=37743"),
                                        new CourseChapter("Understanding context",             "https://wgu.mindedgeonline.com/content.php?cid=39623"),
                                        new CourseChapter("Considering purpose",               "https://wgu.mindedgeonline.com/content.php?cid=39622"),
                                        new CourseChapter("Organization and structure",        "https://wgu.mindedgeonline.com/content.php?cid=39624"),
                                        new CourseChapter("What is narrative writing?",        "https://wgu.mindedgeonline.com/content.php?cid=37762"),
                                        new CourseChapter("Voice and point of view",           "https://wgu.mindedgeonline.com/content.php?cid=39625"),
                                        new CourseChapter("Perspective",                       "https://wgu.mindedgeonline.com/content.php?cid=37587"),
                                        new CourseChapter("Style, diction, and tone",          "https://wgu.mindedgeonline.com/content.php?cid=37775"),
                                        new CourseChapter("Diction",                           "https://wgu.mindedgeonline.com/content.php?cid=37773"),
                                        new CourseChapter("Tone",                              "https://wgu.mindedgeonline.com/content.php?cid=39621"),
                                        new CourseChapter("Formal versus informal writing",    "https://wgu.mindedgeonline.com/content.php?cid=37778"),
                                        new CourseChapter("Practice: Analyzing an essay",      "https://wgu.mindedgeonline.com/content.php?cid=38868"),
                                        new CourseChapter("1.18 Module 1 Post-Self-Assessment","https://wgu.mindedgeonline.com/bounce.php?course=570&p=tools%2Ftest_intro.php%3Ftid=3117"),
                                        new CourseChapter("Module Feedback",                   "https://wgu.mindedgeonline.com/content.php?cid=37770"),
                                }),
                                new CourseModule("Module 2: The Writing Process: Developing a Thesis", "https://lrps.wgu.edu/provision/40187421", new CourseChapter[] {
                                        new CourseChapter("Module 2 learning outcomes", "https://wgu.mindedgeonline.com/content.php?cid=37794"),
                                        new CourseChapter("2.02 Module 2 Pre-Self-Assessment", "https://wgu.mindedgeonline.com/bounce.php?course=570&p=tools%2Ftest_intro.php%3Ftid=3118"),
                                        new CourseChapter("2.02 Module 2 Pre-Self-Assessment", "https://wgu.mindedgeonline.com/bounce.php?course=570&p=tools%2Ftest_intro.php%3Ftid=3118"),
                                        new CourseChapter("Choosing a topic", "https://wgu.mindedgeonline.com/content.php?cid=37785"),
                                        new CourseChapter("There is no content on this page.", "https://wgu.mindedgeonline.com/content.php?cid=39619"),
                                        new CourseChapter("Topic selection: Non-research-based", "https://wgu.mindedgeonline.com/content.php?cid=39610"),
                                        new CourseChapter("Assessing your topic", "https://wgu.mindedgeonline.com/content.php?cid=39604"),
                                        new CourseChapter("Developing a thesis: Visual organizers", "https://wgu.mindedgeonline.com/content.php?cid=37787"),
                                        new CourseChapter("Structure of a thesis statement", "https://wgu.mindedgeonline.com/content.php?cid=38872"),
                                        new CourseChapter("Main claim, major points, and evidence", "https://wgu.mindedgeonline.com/content.php?cid=38875"),
                                        new CourseChapter("Finding an arguable thesis", "https://wgu.mindedgeonline.com/content.php?cid=38869"),
                                        new CourseChapter("Making sure your thesis statement is research-based", "https://wgu.mindedgeonline.com/content.php?cid=38871"),
                                        new CourseChapter("Practice writing: Developing a thesis statement", "https://wgu.mindedgeonline.com/content.php?cid=37485"),
                                        new CourseChapter("2.11 Module 2 Post-Self-Assessment", "https://wgu.mindedgeonline.com/bounce.php?course=570&p=tools%2Ftest_intro.php%3Ftid=3120"),
                                        new CourseChapter("Module Feedback", "https://wgu.mindedgeonline.com/content.php?cid=37793"),
                                }),
                                new CourseModule("Module 3: The Writing Process: Organizing, Outlining, and Drafting", "https://lrps.wgu.edu/provision/40188531", new CourseChapter[] {
                                        new CourseChapter("Module 3 learning outcomes",         "https://wgu.mindedgeonline.com/content.php?cid=37818"),
                                        new CourseChapter("3.02 Module 3 Pre-Self-Assessment",  "https://wgu.mindedgeonline.com/bounce.php?course=570&p=tools%2Ftest_intro.php%3Ftid=3121"),
                                        new CourseChapter("Organization",                       "https://wgu.mindedgeonline.com/content.php?cid=37494"),
                                        new CourseChapter("Using a visual organizer",           "https://wgu.mindedgeonline.com/content.php?cid=37805"),
                                        new CourseChapter("Outlining",                          "https://wgu.mindedgeonline.com/content.php?cid=37807"),
                                        new CourseChapter("Drafting an essay",                  "https://wgu.mindedgeonline.com/content.php?cid=37811"),
                                        new CourseChapter("Example: The drafting process",      "https://wgu.mindedgeonline.com/content.php?cid=37812"),
                                        new CourseChapter("Introduction and conclusion",        "https://wgu.mindedgeonline.com/content.php?cid=37415"),
                                        new CourseChapter("Organization: Paragraphs",           "https://wgu.mindedgeonline.com/content.php?cid=37503"),
                                        new CourseChapter("Writing and organizing topic sentences", "https://wgu.mindedgeonline.com/content.php?cid=37401"),
                                        new CourseChapter("Organization within paragraphs",     "https://wgu.mindedgeonline.com/content.php?cid=39133"),
                                        new CourseChapter("Using examples",                     "https://wgu.mindedgeonline.com/content.php?cid=37465"),
                                        new CourseChapter("Using transitions",                  "https://wgu.mindedgeonline.com/content.php?cid=37388"),
                                        new CourseChapter("Practice: Paragraph development",    "https://wgu.mindedgeonline.com/content.php?cid=37345"),
                                        new CourseChapter("3.15 Module 3 Post-Self-Assessment", "https://wgu.mindedgeonline.com/bounce.php?course=570&p=tools%2Ftest_intro.php%3Ftid=3122"),
                                        new CourseChapter("Module Feedback",                    "https://wgu.mindedgeonline.com/content.php?cid=37817"),
                                }),
                                new CourseModule("Module 4: The Narrative Essay",                         "https://lrps.wgu.edu/provision/40241162", new CourseChapter[] {
                                        new CourseChapter("Module 4 learning outcomes",                   "https://wgu.mindedgeonline.com/content.php?cid=37630"),
                                        new CourseChapter("4.02 Module 4 Pre-Self-Assessment",            "https://wgu.mindedgeonline.com/bounce.php?course=570&p=tools%2Ftest_intro.php%3Ftid=3053"),
                                        new CourseChapter("What is a narrative argument?",                "https://wgu.mindedgeonline.com/content.php?cid=37368"),
                                        new CourseChapter("Why narrative works",                          "https://wgu.mindedgeonline.com/content.php?cid=37566"),
                                        new CourseChapter("Video commentary: The importance of narrative writing", "https://wgu.mindedgeonline.com/content.php?cid=37570"),
                                        new CourseChapter("Personal experience and narration",            "https://wgu.mindedgeonline.com/content.php?cid=37375"),
                                        new CourseChapter("Compelling stories",                           "https://wgu.mindedgeonline.com/content.php?cid=37585"),
                                        new CourseChapter("Which story to tell?",                         "https://wgu.mindedgeonline.com/content.php?cid=37584"),
                                        new CourseChapter("Rhetorical, stylistic, and narrative decisions", "https://wgu.mindedgeonline.com/content.php?cid=37384"),
                                        new CourseChapter("Narration: An example",                        "https://wgu.mindedgeonline.com/content.php?cid=37513"),
                                        new CourseChapter("\"Old ladies who didn't love me\": Discussion", "https://wgu.mindedgeonline.com/content.php?cid=37739"),
                                        new CourseChapter("Using description",                            "https://wgu.mindedgeonline.com/content.php?cid=37596"),
                                        new CourseChapter("There is no content on this page.",            "https://wgu.mindedgeonline.com/content.php?cid=39617"),
                                        new CourseChapter("Diction",                                      "https://wgu.mindedgeonline.com/content.php?cid=39613"),
                                        new CourseChapter("Style, diction, and tone in narrative essays", "https://wgu.mindedgeonline.com/content.php?cid=39614"),
                                        new CourseChapter("Appropriate Diction: Narrative Writing",       "https://wgu.mindedgeonline.com/content.php?cid=39615"),
                                        new CourseChapter("Style, diction, and tone in narrative essays", "https://wgu.mindedgeonline.com/content.php?cid=37791"),
                                        new CourseChapter("How to write a narrative essay and topics",    "https://wgu.mindedgeonline.com/content.php?cid=39134"),
                                        new CourseChapter("Sample narrative essay",                       "https://wgu.mindedgeonline.com/content.php?cid=38743"),
                                        new CourseChapter("Practice: Narrative argument",                 "https://wgu.mindedgeonline.com/content.php?cid=38836"),
                                        new CourseChapter("Prewriting: Narrative argument",               "https://wgu.mindedgeonline.com/content.php?cid=39135"),
                                        new CourseChapter("Task 1 checklist",                             "https://wgu.mindedgeonline.com/content.php?cid=39597"),
                                        new CourseChapter("TaskStream",                                   "https://wgu.mindedgeonline.com/content.php?cid=38796"),
                                        new CourseChapter("4.20 Module 4 Post-Self-Assessment",           "https://wgu.mindedgeonline.com/bounce.php?course=570&p=tools%2Ftest_intro.php%3Ftid=3040"),
                                        new CourseChapter("Module Feedback",                              "https://wgu.mindedgeonline.com/content.php?cid=37727"),
                                }),
                                new CourseModule("Module 5: The Writing Process: Working With Sources", "https://lrps.wgu.edu/provision/40242179", new CourseChapter[] {
                                        new CourseChapter("Module 5 learning outcomes", "https://wgu.mindedgeonline.com/content.php?cid=37631"),
                                        new CourseChapter("5.02 Module 5 Pre-Self-Assessment", "https://wgu.mindedgeonline.com/bounce.php?course=570&p=tools%2Ftest_intro.php%3Ftid=3054"),
                                        new CourseChapter("Listening to the conversation", "https://wgu.mindedgeonline.com/content.php?cid=37541"),
                                        new CourseChapter("Thesis statements and evidence", "https://wgu.mindedgeonline.com/content.php?cid=37433"),
                                        new CourseChapter("Video commentary: Reading arguments you disagree with", "https://wgu.mindedgeonline.com/content.php?cid=39137"),
                                        new CourseChapter("What type of evidence is called for?", "https://wgu.mindedgeonline.com/content.php?cid=37374"),
                                        new CourseChapter("Hard data and impartiality", "https://wgu.mindedgeonline.com/content.php?cid=37378"),
                                        new CourseChapter("Primary, secondary, and tertiary sources", "https://wgu.mindedgeonline.com/content.php?cid=37379"),
                                        new CourseChapter("Books, periodicals, and everything else", "https://wgu.mindedgeonline.com/content.php?cid=37515"),
                                        new CourseChapter("Types of sources: Scholarly, non-scholarly, and academic-trade sources", "https://wgu.mindedgeonline.com/content.php?cid=37516"),
                                        new CourseChapter("The library", "https://wgu.mindedgeonline.com/content.php?cid=37517"),
                                        new CourseChapter("Outside of the research library: Using the Internet", "https://wgu.mindedgeonline.com/content.php?cid=38908"),
                                        new CourseChapter("Assessing the credibility and relevance of a source", "https://wgu.mindedgeonline.com/content.php?cid=38909"),
                                        new CourseChapter("Using sources effectively", "https://wgu.mindedgeonline.com/content.php?cid=37520"),
                                        new CourseChapter("Avoiding plagiarism", "https://wgu.mindedgeonline.com/content.php?cid=37521"),
                                        new CourseChapter("Deciding among quotation, paraphrase, and summary", "https://wgu.mindedgeonline.com/content.php?cid=39626"),
                                        new CourseChapter("Incorporating quotations, paraphrases, and summaries into your writing", "https://wgu.mindedgeonline.com/content.php?cid=39609"),
                                        new CourseChapter("Best practices for quoting or paraphrasing", "https://wgu.mindedgeonline.com/content.php?cid=37540"),
                                        new CourseChapter("Paraphrasing accuracy", "https://wgu.mindedgeonline.com/content.php?cid=39627"),
                                        new CourseChapter("Exercise: Paraphrasing", "https://wgu.mindedgeonline.com/content.php?cid=39132"),
                                        new CourseChapter("Citations in APA format", "https://wgu.mindedgeonline.com/content.php?cid=37523"),
                                        new CourseChapter("In-text citations: Patterns for in-text citations", "https://wgu.mindedgeonline.com/content.php?cid=37539"),
                                        new CourseChapter("Common knowledge, specialized knowledge, and correct attribution", "https://wgu.mindedgeonline.com/content.php?cid=39634"),
                                        new CourseChapter("Practice: Working with sources", "https://wgu.mindedgeonline.com/content.php?cid=37602"),
                                        new CourseChapter("5.25 Module 5 Post-Self-Assessment", "https://wgu.mindedgeonline.com/bounce.php?course=570&p=tools%2Ftest_intro.php%3Ftid=3043"),
                                        new CourseChapter("Module Feedback", "https://wgu.mindedgeonline.com/content.php?cid=38785"),
                                }),
                                new CourseModule("Module 6: The Evaluation Essay", "https://lrps.wgu.edu/provision/40242353", new CourseChapter[] {
                                        new CourseChapter("Module 6 learning outcomes", "https://wgu.mindedgeonline.com/content.php?cid=37632"),
                                        new CourseChapter("6.02 Module 6 Pre-Self-Assessment", "https://wgu.mindedgeonline.com/bounce.php?course=570&p=tools%2Ftest_intro.php%3Ftid=3055"),
                                        new CourseChapter("Video commentary: Evaluation arguments", "https://wgu.mindedgeonline.com/content.php?cid=37446"),
                                        new CourseChapter("The structure of evaluation arguments", "https://wgu.mindedgeonline.com/content.php?cid=37381"),
                                        new CourseChapter("Practical, aesthetic, and ethical criteria", "https://wgu.mindedgeonline.com/content.php?cid=37383"),
                                        new CourseChapter("Activity: Criteria for evaluation arguments", "https://wgu.mindedgeonline.com/content.php?cid=37427"),
                                        new CourseChapter("Creating evaluative claims", "https://wgu.mindedgeonline.com/content.php?cid=39136"),
                                        new CourseChapter("The components of successful evaluation arguments", "https://wgu.mindedgeonline.com/content.php?cid=37391"),
                                        new CourseChapter("How to write an evaluation essay and topics", "https://wgu.mindedgeonline.com/content.php?cid=38895"),
                                        new CourseChapter("Sample evaluation essay", "https://wgu.mindedgeonline.com/content.php?cid=38744"),
                                        new CourseChapter("Practice: Evaluation argument", "https://wgu.mindedgeonline.com/content.php?cid=38819"),
                                        new CourseChapter("Prewriting: Evaluation argument", "https://wgu.mindedgeonline.com/content.php?cid=37633"),
                                        new CourseChapter("Task 2 checklist", "https://wgu.mindedgeonline.com/content.php?cid=39596"),
                                        new CourseChapter("TaskStream", "https://wgu.mindedgeonline.com/content.php?cid=38797"),
                                        new CourseChapter("6.15 Module 6 Post-Self-Assessment", "https://wgu.mindedgeonline.com/bounce.php?course=570&p=tools%2Ftest_intro.php%3Ftid=3039"),
                                        new CourseChapter("Module Feedback", "https://wgu.mindedgeonline.com/content.php?cid=37728"),
                                }),
                                new CourseModule("Module 7: The Causal Analysis Essay", "https://lrps.wgu.edu/provision/40242890", new CourseChapter[] {
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38740"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/bounce.php?course=570&p=tools%2Ftest_intro.php%3Ftid=3113"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38727"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38729"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38715"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38733"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38719"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38817"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38816"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=39140"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=39141"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38716"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38783"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38739"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=39598"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38798"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/bounce.php?course=570&p=tools%2Ftest_intro.php%3Ftid=3114"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38741"),
                                }),
                                new CourseModule("Module 8: The Proposal Essay", "https://lrps.wgu.edu/provision/40248977", new CourseChapter[] {
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37655"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/bounce.php?course=570&p=tools%2Ftest_intro.php%3Ftid=3058"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37552"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37558"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37559"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37459"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37554"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37622"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38896"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38745"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38820"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37336"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=39595"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38799"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/bounce.php?course=570&p=tools%2Ftest_intro.php%3Ftid=3046"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37730"),
                                }),
                                new CourseModule("Module 9: The Writing Process: Revising Your Essay", "https://lrps.wgu.edu/provision/40249114", new CourseChapter[] {
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37656"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/bounce.php?course=570&p=tools%2Ftest_intro.php%3Ftid=3060"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37649"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37406"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37417"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38876"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37410"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37404"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37405"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37353"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38839"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37463"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37418"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37420"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38840"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/bounce.php?course=570&p=tools%2Ftest_intro.php%3Ftid=3072"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38784"),
                                }),
                                new CourseModule("Module 10: The Writing Process: Preparing Your Final Paper", "https://lrps.wgu.edu/provision/40249254", new CourseChapter[] {
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37658"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/bounce.php?course=570&p=tools%2Ftest_intro.php%3Ftid=3061"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37356"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37450"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37613"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37625"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37564"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37412"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37545"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37349"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38786"),
                                }),
                                new CourseModule("Appendix A: Grammar Tutorials", "https://wgu.mindedgeonline.com/content.php?cid=38800", new CourseChapter[] {
                                        new CourseChapter("Run-on sentences: Comma splices and fused sentences", "https://wgu.mindedgeonline.com/content.php?cid=38801"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38801"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38802"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38803"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38804"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=38806"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=39630"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=39631"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=39629"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=39603"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=39616"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=39607"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=39608"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=39602"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=39606"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=39620"),
                                }),
                                new CourseModule("Appendix B: APA Format", "https://wgu.mindedgeonline.com/content.php?cid=37604", new CourseChapter[] {
                                        new CourseChapter("Formatting your paper in APA format", "https://wgu.mindedgeonline.com/content.php?cid=37547"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37547"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37614"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37599"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37608"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37600"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37529"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37603"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37607"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37605"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37617"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37615"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37616"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37610"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37546"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37606"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37601"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37621"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=39413"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37609"),
                                }),
                                new CourseModule("Appendix C: Sample Papers", "https://wgu.mindedgeonline.com/content.php?cid=37726", new CourseChapter[] {
                                        new CourseChapter("Sample Narrative Essay", "https://wgu.mindedgeonline.com/content.php?cid=37731"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37731"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37732"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37733"),
                                        new CourseChapter("zzz", "https://wgu.mindedgeonline.com/content.php?cid=37734"),
                                }),
                                new CourseModule("End Of Course Summary", "https://wgu.mindedgeonline.com/content.php?cid=course_summary", new CourseChapter[] {
                                }),
                        }
                );

            case "C457":
                return new Course("C457", "Foundations of College Mathematics", 3,
                        "https://cos.wgu.edu/courses/for/C457/3960004", new String[] {
                        "Applying Basic Numeracy and Calculation Skills",
                        "Applying Basic Algebra Skills",
                } );

            case "C459":
                return new Course("C459", "Introduction to Probability and Statistics", 3, "", new String[] {"", });

            case "C483":
                return new Course("C483", "Principles of Management", 4,
                        "https://cos.wgu.edu/courses/for/C483/4000005?user=cpoindexter", new String[] {"", });

            case "C484":
                return new Course("C484", "Organizational Behavior and Leadership", 3, "", new String[] {"", });

            case "C489":
                return new Course("C489", "Organizational Behavior and Leadership", 3,
                        "https://cos.wgu.edu/courses/for/C489/5300001?user=cpoindexter", new String[] {"", });

            case "CIC1":
                return new Course("CIC1", "College Algebra", 3, "", new String[] {"", });

            case "CMC1":
                return new Course("CMC1", "Mathematics for Elementary Educators I", 3, "", new String[] {"", });

            case "DAC1":
                return new Course("DAC1", "Information Systems Management", 3, "", new String[] {"", });

            case "DOC2":
                return new Course("DOC2", "Instructional Planning and Presentation", 1, "", new String[] {"", });

            case "DJV1":
                return new Course("DJV1", "Software Development Fundamentals", 3, "", new String[] {"", });

            case "EST1":
                return new Course("EST1", "Ethical Situations in Business", 3, "", new String[] {"", });

            case "EZC1":
                return new Course("EZC1", "Finance", 3, "", new String[] {"", });

            case "FCC1":
                return new Course("FCC1", "Introduction to Special Education, Law and Legal Issues", 4, "", new String[] {"", });

            case "FTC1":
                return new Course("FTC1", "Macroeconomics", 3, "", new String[] {"", });

            case "FPC1":
                return new Course("FPC1", "Microeconomics", 3, "", new String[] {"", });

            case "FVC1":
                return new Course("FVC1", "Global Business", 3, "", new String[] {"", });

            case "KET1":
                return new Course("KET1", "Introduction to Programming", 3, "", new String[] {"", });

            case "KFT1":
                return new Course("KFT1", "Object Oriented Design and Development", 4, "", new String[] {"", });

            case "LIT1":
                return new Course("LIT1", "Legal Issues for Business Organizations", 3, "", new String[] {"", });

            case "LWC1":
                return new Course("LWC1", "Fundamentals of Business Law and Ethics", 6, "", new String[] {"", });

            case "MCC4":
                return new Course("MCC4", "Precalculus and Calculus Part II", 1, "", new String[] {"", });

            case "MKC1":
                return new Course("MKC1", "Fundamentals of Marketing and Business Communication", 6, "", new String[] {"", });

            case "QAT1":
                return new Course("QAT1", "Quantitative Analysis for Business", 6, "", new String[] {"", });

            case "QBT1":
                return new Course("QBT1", "Language and Communication: Research", 3, "", new String[] {"", });

            case "QIT1":
                return new Course("QIT1", "Business Marketing Management Capstone Written Project", 4, "", new String[] {"", });

            case "QLC1":
                return new Course("QLC1", "Quantitative Literacy: College Algebra, Measurement and Geometry", 3, "", new String[] {"", });

            case "SFAW":
                return new Course("SFAW", "Field Experience Application Documents", 3, "", new String[] {"", });

            case "TBP1":
                return new Course("TBP1", "English Composition I", 3, "", new String[] {"", });

            case "TCP1":
                return new Course("TCP1", "English Composition II", 3, "", new String[] {"", });

            case "TPV1":
                return new Course("TPV1", "Project Management", 6, "", new String[] {"", });

            case "UFC1":
                return new Course("UFC1", "Managerial Accounting", 3, "", new String[] {"", });

            case "VYC1":
                return new Course("VYC1", "Principles of Accounting", 4, "", new String[] {"", });

            case "VZT1":
                return new Course("VZT1", "Marketing Applications", 3, "", new String[] {"", });

            default:
                General.SkipTest("Unknown code in getCourse() " + code);
                return null;
        }
    }

}













































































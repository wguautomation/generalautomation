
package Utils;

import BaseClasses.GlobalData;
import BaseClasses.LoginType;
import org.apache.xerces.xs.StringList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.TestNGException;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

/*
 * Created by timothy.hallbeck on 9/15/2015.
 */

public abstract class wguCurlExecuter extends wguTestWrapper {
    // The first call that uses any of these fill it in. The expiration is a couple of hours, so good for a single test session
    static protected String username = "";
    static protected String password = "";
    static protected boolean ignoreErrors = false;

    protected String runCurlTest(String command, LoginType login) {
        String result = Get(command, login);
        General.Debug("Command: " + command + "\n  Result: " + result + "\n");

        return result;
    }

    public enum GetPost {GET, POST}

    public wguCurlExecuter() {
    }

    public static void setIgnoreErrors(boolean state) {
        ignoreErrors = state;
    }

    public void setUsername(String name) {
        username = name;
    }

    public void setPassword(String name) {
        password = name;
    }

    public abstract String Get(String getCommand, LoginType login);

    //    public abstract String Put(String command, LoginType login, String data);
    public abstract String Post(String command, LoginType login, String data);

    public String executeCurl(String[] fullCommand) {
        return executeCurl(fullCommand, null);
    }
    public String executeCurl(String[] fullCommand, String[] searchTerms) {

        if (General.getOS() == General.OS.Mac)
            fullCommand[0] = fullCommand[0].replace("curl", "/usr/bin/curl"); // Need to General.FindMacCurl()
        ProcessBuilder builder;
        String success = "";
        File samefile = null;
        List<String> stringList = null;

        if (searchTerms != null)
            searchTerms[0] = General.StringArrayToString(fullCommand, " ");
        try {
            // The java munges the single/double quotes on the Mac, so libcurl doesnt play well with ProcessBuilder.
            //   Have to write out the commands to a shell file, then execute it.
            if (General.getOS() == General.OS.Mac) {

                Date date = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("mmssSSS");
                String fileName = GlobalData.DATA_PATH + "maccurl" + dateFormat.format(date) + ".sh";

                BufferedWriter outputWriter = new BufferedWriter(new FileWriter(fileName));
                outputWriter.write(General.StringArrayToString(fullCommand, " "));
                outputWriter.flush();
                outputWriter.close();

                samefile = new File(fileName);
                assert(samefile.setWritable(true));
                assert(samefile.setExecutable(true));

                builder = new ProcessBuilder(fileName);
            } else { // Windows
                builder = new ProcessBuilder(fullCommand);
            }

            final Process process = builder.start();
            success = new BufferedReader(new InputStreamReader(process.getInputStream())).readLine();
            Stream<String> fulloutput = new BufferedReader(new InputStreamReader(process.getErrorStream())).lines();

            Object[] array = fulloutput.toArray();
            stringList = Arrays.asList(Arrays.copyOf(array, array.length, String[].class));
            processSearchTerms(stringList, searchTerms);

            if (General.getDetailedDebugMode())
                General.Debug(stringList);
        } catch (Exception e) {
            General.Debug(e.getMessage());
        } finally {
            if (samefile != null)
                samefile.delete();
        }

        if (success != null && success.toLowerCase().contains("object moved"))
            success = getHtmlRedirect(stringList);
            // Need to rework error detection vs 200 OK here on calls that return no data
        else if (stringList.size() != 0) {
            for (String string : stringList)
                checkForErrorCodes(string);
        }

        return success;
    }

    // replaces the search term with first matching info it comes across
    //  item[0] is already occupied by the command, so terms start after that.
    private void processSearchTerms(List<String> stringList, String[] searchTerms) {
        if (searchTerms == null)
            return;

        for (int i=1; i<searchTerms.length; i++) {
            for (String string : stringList) {
                if (string.contains(searchTerms[i])) {
                    searchTerms[i] = string;
                    continue;
                }
            }
        }
    }

    private String getHtmlRedirect(List<String> stringList) {
        String result = General.FindSubstringInStringList(stringList, "Location: http");
        if (!result.isEmpty())
            result = result.substring(result.indexOf("https://"));

        return result;
    }

    public void checkForErrorCodes(String checkMe) {
        if (checkMe == null || checkMe.isEmpty())
            return;

        // May add more checking here
        if (checkMe.contains("status\":400") ||
                checkMe.contains("status\":401") ||
                checkMe.contains("status\":403") ||
                checkMe.contains("status\":405") ||
                checkMe.contains("status\":404") ||
                checkMe.contains("status\":500") ||
                checkMe.contains("500 Internal Server Error") ||
                checkMe.contains("596 Service Not Found") ||
                checkMe.contains("Developer Inactive") ||
                checkMe.contains("JsonException") ||
                checkMe.contains("Gateway Timeout")
                )
            General.Debug("Thread id: " + Thread.currentThread().getId() + "    curl returned error:\n  " + checkMe);
    }

    // Checks that a key of this name exists as either a json object or json named array
    public static String verifyKey(String result, String objectOrKey) {
        return verifyKey(result, objectOrKey, null, false);
    }

    // If value is not null, checks that key exists and equals the value
    // To check for a literal / non-json object, set the first char of objectOrKey to a '$'
    public static String verifyKey(String result, String Key, String value, boolean notPresentOk) {
        if (Key == null || Key.isEmpty())
            return null;
        General.Debug("verifyKey(" + Key + ")");

        // If we're checking to make sure a key is -not- present, a null result is ok
        if (result == null || result.isEmpty()) {
            if (notPresentOk)
                return "";
            else
                throw new TestNGException("curl call returned null");
        }

        // Some services return a literal. We indicate this in the dataProvider with a leading '$' char
        if (Key.startsWith("$")) {
            Key = Key.substring(1);
            if (result.substring(0, Key.length()).contentEquals(Key))
                return result;
        }

        // OSSO returns "token.id=" so check for this
        if (result.contains(Key + "="))
            return result.substring(Key.length() + 1);

        JSONArray jsonArray;
        JSONObject jsonObject;

        // Un-array the outermost item. This need to eventually parse out nested json arrays and objects.
        if (result.indexOf("[{") == 0)
            result = result.substring(1, result.length() - 1);

        // Get first entry if the response is an array
        if (result.indexOf("[{") > 0) {
            result = result.substring(result.indexOf("[{"), result.length() - 1);
            jsonArray = new JSONArray(result);
            jsonObject = (JSONObject) jsonArray.get(0);
        } else {
            try {
                jsonObject = new JSONObject(result);
            } catch (Exception e) {
                if (result == null)
                    throw new TestNGException("OSSO token came back null");
                else
                    throw new TestNGException(e.getMessage());
            }
        }

        String response = "";
        try {
            response = String.valueOf(jsonObject.get(Key));
        } catch (JSONException e) {
            if (notPresentOk)
                return "";
            General.Debug("jsonObject.get(" + Key + ") failed");
            throw new TestNGException(e.getMessage());
        }

        if (response == null)
            throw new TestNGException("Json object or key (" + Key + ") not found");
        General.Debug(Key + " = " + response);

        if (value != null && !value.contentEquals(response))
            throw new TestNGException("Json object or key (" + Key + ") found, but did not contain expected value (" + value + ")");

        return response;
    }

    // Meant to space apart multi threaded inits
    protected synchronized void delayThread(long millis) {
        try {
            Thread.sleep(millis);
        } catch (Exception e) {
            ;
        }
        General.Debug("Thread " + Thread.currentThread().getId() + " ready to go");
    }
}


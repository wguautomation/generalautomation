
package Utils;

import BaseClasses.LoginType;
import JsonDataClasses.*;
import ServiceClasses.*;
import StudentPortalPages.myHomePage;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputControl;
import org.testng.SkipException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

/*
 * Created by timothy.hallbeck on 5/26/2016.
 */

public class General {

    // Global data objects
    static private DegreePlanService       degreePlan       = null;
    static private MentorService           mentorArray      = null;
    static private PersonService           person           = null;
    static private ProgramProgressService  programProgress  = null;
    static private StudyPlanService        studyPlan        = null;
    static private String                  courseVersionId  = "";
    private static TextInputControl        outputWindow     = null;

    private static boolean  silentMode        = false;
    private static boolean  detailedDebugMode = false;
    private static int      initialStackDepth;
    private static Random   randomGenerator = new Random(System.currentTimeMillis());

    General() {
        initialStackDepth = Thread.currentThread().getStackTrace().length;
    }

    // return previous state
    static public boolean setSilentMode(boolean mode) {
        boolean prev = silentMode;
        silentMode = mode;
        return prev;
    }

    static public boolean getSilentMode() {
        return silentMode;
    }

    static public void setDetailedDebugMode(boolean mode) {
        silentMode = mode;
    }
    static public boolean getDetailedDebugMode() {
        return silentMode;
    }

    static public int getStackDepth() {
        return Integer.max(Thread.currentThread().getStackTrace().length - initialStackDepth - 30, 1);
    }

    static int cursorPos=0;
    static public void MarkOutputCursor() {
        if (outputWindow != null)
            cursorPos = outputWindow.getLength();
    }

    static public void RestoreOutputCursor() {
        if (outputWindow != null)
            outputWindow.positionCaret(cursorPos);
    }

    static public void SetOutputWindow(TextInputControl output) {
        outputWindow = output;
    }

    static public void SendTextToOutputWindow(String text) {
        if (outputWindow == null)
            return;

        String current = outputWindow.getText();
        outputWindow.setText(current + text + "\n");
        General.RestoreOutputCursor();
        General.MarkOutputCursor();
    }

    static public boolean DebugAndOutputWindow(String info) {
        SendTextToOutputWindow(info);
        return Debug(info, false);
    }

    static public boolean Debug(List<String> list) {
        if (silentMode)
            return false;
        for (String string : list)
            Debug(string, false);
        return (true);
    }
    static public boolean Debug(String info) {
        return Debug(info, false);
    }
    static public boolean Debug(String info, boolean bAddBulletAlso) {
        if (silentMode)
            return false;

        System.out.println(info);
        System.out.flush();

        if (bAddBulletAlso)
            wguPowerPointFileUtils.addBulletPoint(info);

        return true;
    }

    static public void assertDebug(boolean expression, String debugText) {
        assert(expression);
        wguPowerPointFileUtils.addBulletPoint(debugText);
        if (!silentMode)
            System.out.println(debugText);
    }

    static public void SkipTest(String text) {
        General.Sleep(250);
        throw new SkipException(text);
    }

    static public int getRandomInt(int maxNumber) {
        return randomGenerator.nextInt(maxNumber);
    }

    static public boolean Sleep(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return true;
    }

    public enum OS { Mac, Windows }
    static public OS getOS() {
        String uAgent = System.getProperty("os.name");
        if (uAgent.toLowerCase().contains("windows"))
            return OS.Windows;
        else
            return OS.Mac;
    }

    static public String StringArrayToString(String[] array, String separator) {
        String result="";

        for(String string : array)
            result += string + separator;

        return result;
    }

    static public String FindSubstringInStringList(List<String> stringList, String findMe) {
        findMe = findMe.toLowerCase();
        for (String searchMe : stringList)
            if (searchMe.toLowerCase().contains(findMe))
                return searchMe;
        return "";
    }

    static public String[][] ConvertStringArraytoStringArrayArray(String[] array) {
        String[][] arrayArray = new String[array.length][1];
        int i=0;
        for(String string : array)
            arrayArray[i++][0] = string;
        return arrayArray;
    }

    static public String GetDateAndTimeFilePrefix() {
        Date date = new Date() ;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss_") ;

        return dateFormat.format(date);
    }

    static public void ClearTokens() {
        PingSession.ClearTokens();
        MasherySession.ClearTokens();
        OssoSession.ClearTokens();
    }

    static public void ClearStatics() {
        degreePlan = null;
        mentorArray = null;
        person = null;

        programProgress = null;
        studyPlan = null;
        courseVersionId = "";

        ClearTokens();
    }

    static public String getCompetencyId(LoginType login) {
        return "00001";
//        AssessmentsService array = new AssessmentsService(login);
//        JsonAssessment assessment = array.getAssessment(0);

//        if (assessment == null)

//        assessment.getJsonCompetency competency;

//      return competency.getCompetencyId();
    }

    static public String getCourseCode(LoginType login) {
        JsonTerm term = getDegreePlan(login).getCurrentTerm();
        JsonCourse course = term.getAllCourses().getCourse(0);
        String courseCode = course.getCourseCode();

        return courseCode;
    }

    static public String getCourseVersionId(LoginType login) {
        if (courseVersionId.equals("")) {
            JsonTerm term = getDegreePlan(login).getCurrentTerm();
            JsonCourse course = term.getAllCourses().getCourse(0);
            courseVersionId = course.getCourseVersionId();
        }

        return courseVersionId;
    }

    static public String getAssessmentCode(LoginType login) {
        Debug("General::getAssessmentCode() is not implemented......");
        return "22233";
    }

    static public String getAssessmentId(LoginType login) {
        Debug("General::getAssessmentId() is not implemented......");
        return "1";
    }

    static public String getTermCode(LoginType login) {
        Debug("General::getTermCode()");
        return getCurrentTerm(login).getTermCode();
    }

    static public JsonTerm getCurrentTerm(LoginType login) {
        Debug("General::getCurrentTerm()");
        return getDegreePlan(login).getCurrentTerm();
    }

    static public JsonDegreePlan getDegreePlan(LoginType login) {
        Debug("General::getDegreePlan()");
        if (degreePlan == null)
            degreePlan = new DegreePlanService(login);
        return degreePlan;
    }

    static public JsonMentor getMentor(LoginType login) {
        Debug("General::getMentor()");
        return getMentors(login).getMentor(0);
    }

    static public String getMentorPidm(LoginType login) {
        Debug("General::getMentor()");
        return getMentors(login).getMentor(0).getPidm();
    }

    static public MentorService getMentors(LoginType login) {
        Debug("General::getMentors()");
        if (mentorArray == null)
            mentorArray = new MentorService(login);
        return mentorArray;
    }

    static public PersonService getPerson(LoginType login) {
        Debug("General::getPerson()");
        if (person == null)
            person = new PersonService(login);
        return person;
    }

    static public String getBannerId(LoginType login) {
        return getPerson(login).getStudentId();
    }

    static public ProgramProgressService getProgramProgress(LoginType login) {
        Debug("General::getProgramProgress()");
        if (programProgress == null)
            programProgress = new ProgramProgressService(login);
        return programProgress;
    }

    // Not working yet, dont know how to get just the 1 study plan for a user
    static public String getStudyPlanId(LoginType login) {
        Debug("General::getStudyPlanId()");
        return getDegreePlan(login).getCurrentPlan();
    }

    static public String GetProdPingToken() {
        myHomePage myPage = new myHomePage(LoginType.PROD_CPOINDEXTER_PING);
        String pingToken = "";
        try {
            myPage.load();
            pingToken = myPage.getCookie("PA.shadow");
            myPage.quit();
        } catch (Exception e) {
            ;
        }

        return pingToken;
        }

    static public void exerciseAllDataObjects(LoginType login) {
        Debug("General::exerciseAllDataObjects()");
        Debug(getCurrentTerm(login).toString() + "\n");
        Debug(getDegreePlan(login).toString() + "\n");
        Debug(getMentors(login).toString() + "\n");
        Debug(getMentor(login).toString() + "\n");
        Debug(getPerson(login).toString() + "\n");
        Debug(getProgramProgress(login).toString() + "\n");
//        Debug(getStudyPlan().toString() + "\n");
    }
}


package Utils;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.ss.usermodel.charts.*;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

/*
 * Created by timothy.hallbeck on 3/10/2015.
 */

public class wguExcelFileUtils {
    private String       fileName="";
    private XSSFWorkbook currentWorkbook=null;
    private XSSFSheet    currentSheet=null;

    public wguExcelFileUtils()  {
    }

    public enum chartType {
        COLUMN_STACKED, COLUMN_3DCLUSTERED
    }

    public XSSFWorkbook createWorkbook(String filename) throws Exception {
        currentWorkbook = new XSSFWorkbook();
        this.fileName = filename;
        return currentWorkbook;
    }

    public XSSFSheet createSheet(String sheetName) {
        currentSheet = this.currentWorkbook.createSheet(sheetName);
        return currentSheet;
    }

    public wguExcelFileUtils writeWorkbook() throws Exception {
        FileOutputStream out = new FileOutputStream(new File(this.fileName));
        this.currentWorkbook.write(out);
        out.close();

        return this;
    }

    public wguExcelFileUtils addColumnHeaders(int startingRow, ArrayList<String> data, int startingColumn) {
        XSSFRow  workerRow = null;
        XSSFCell workerCell = null;

        workerRow = this.currentSheet.createRow(startingRow);
        for (String dataitem : data) {
            workerCell = workerRow.createCell(startingColumn, XSSFCell.CELL_TYPE_STRING);
            workerCell.setCellValue(dataitem);
            startingColumn++;
        }

        return this;
    }

    public wguExcelFileUtils addColumnData(int startingRow, ArrayList<String> data, int columnIndex) {
        XSSFRow   workerRow = null;
        XSSFCell  workerCell = null;
        CellStyle style = this.currentWorkbook.createCellStyle();

        style.setAlignment(XSSFCellStyle.ALIGN_CENTER);

        for (String dataitem : data) {
            if (columnIndex == 0) {
                workerRow = this.currentSheet.createRow(startingRow);
                workerCell = workerRow.createCell(columnIndex, XSSFCell.CELL_TYPE_STRING);
                workerCell.setCellValue(dataitem);
            } else {
                workerRow = this.currentSheet.getRow(startingRow);
                workerCell = workerRow.createCell(columnIndex, XSSFCell.CELL_TYPE_NUMERIC);
                workerCell.setCellValue(new Integer (dataitem));
                workerCell.setCellStyle(style);
            }

            startingRow++;
        }

        return this;
    }

    public wguExcelFileUtils addRowData(int row, ArrayList<String> data) {
        XSSFRow   workerRow = null;
        XSSFCell  workerCell = null;
        CellStyle style = this.currentWorkbook.createCellStyle();

        style.setAlignment(XSSFCellStyle.ALIGN_LEFT);

        workerRow = this.currentSheet.createRow(row);
        for (int i=0; i<data.size(); i++) {
            workerCell = workerRow.createCell(i, XSSFCell.CELL_TYPE_STRING);
            workerCell.setCellValue(data.get(i));
        }

        return this;
    }

    public wguExcelFileUtils addTimingInfoChart(int rowStart, int rowCount) {
        XSSFDrawing xlsx_drawing = this.currentSheet.createDrawingPatriarch();
        XSSFClientAnchor anchor = xlsx_drawing.createAnchor(50, 50, 0, 0, 0, 0, 12, rowStart - 1);
        XSSFChart my_line_chart = xlsx_drawing.createChart(anchor);

        LineChartData data = my_line_chart.getChartDataFactory().createLineChartData();
        ChartAxis     bottomAxis = my_line_chart.getChartAxisFactory().createCategoryAxis(AxisPosition.BOTTOM);
        ValueAxis     leftAxis = my_line_chart.getChartAxisFactory().createValueAxis(AxisPosition.LEFT);

        ChartDataSource<Number> labels = DataSources.fromNumericCellRange(this.currentSheet, new CellRangeAddress(rowStart, rowStart + rowCount, 0, 0));
        ChartDataSource<Number> values;

        values = DataSources.fromNumericCellRange(this.currentSheet, new CellRangeAddress(rowStart, rowStart + rowCount, 1, 1));
        data.addSeries(labels, values);
        values = DataSources.fromNumericCellRange(this.currentSheet, new CellRangeAddress(rowStart, rowStart + rowCount, 2, 2));
        data.addSeries(labels, values);
        values = DataSources.fromNumericCellRange(this.currentSheet, new CellRangeAddress(rowStart, rowStart + rowCount, 3, 3));
        data.addSeries(labels, values);
        values = DataSources.fromNumericCellRange(this.currentSheet, new CellRangeAddress(rowStart, rowStart + rowCount, 4, 4));
        data.addSeries(labels, values);
        values = DataSources.fromNumericCellRange(this.currentSheet, new CellRangeAddress(rowStart, rowStart + rowCount, 5, 5));
        data.addSeries(labels, values);
        values = DataSources.fromNumericCellRange(this.currentSheet, new CellRangeAddress(rowStart, rowStart + rowCount, 6, 6));
        data.addSeries(labels, values);
        values = DataSources.fromNumericCellRange(this.currentSheet, new CellRangeAddress(rowStart, rowStart + rowCount, 7, 7));
        data.addSeries(labels, values);

        my_line_chart.plot(data, new ChartAxis[] { bottomAxis, leftAxis });

        return this;
    }

    public wguExcelFileUtils addPageInfoChart(int rowStart, int rowCount) {
        XSSFDrawing xlsx_drawing = this.currentSheet.createDrawingPatriarch();
        XSSFClientAnchor anchor = xlsx_drawing.createAnchor(50, 50, 0, 0, 0, 0, 12, rowStart - 1);
        XSSFChart my_line_chart = xlsx_drawing.createChart(anchor);

        LineChartData data = my_line_chart.getChartDataFactory().createLineChartData();
        ChartAxis     bottomAxis = my_line_chart.getChartAxisFactory().createCategoryAxis(AxisPosition.BOTTOM);
        ValueAxis     leftAxis = my_line_chart.getChartAxisFactory().createValueAxis(AxisPosition.LEFT);

        ChartDataSource<Number> labels = DataSources.fromNumericCellRange(this.currentSheet, new CellRangeAddress(rowStart, rowStart + rowCount, 0, 0));
        ChartDataSource<Number> values;

        values = DataSources.fromNumericCellRange(this.currentSheet, new CellRangeAddress(rowStart, rowStart + rowCount, 1, 1));
        data.addSeries(labels, values);

        my_line_chart.plot(data, new ChartAxis[] { bottomAxis, leftAxis });

        return this;
    }

    public void createSpreadsheetFromPageTimes(String excelFileName, wguHarFileUtils harUtils) throws Exception {
        int rowNumber = 16; // zero based

        // Write it to a spreadsheet
        createWorkbook(excelFileName);
        createSheet("ANewSheet1").setColumnWidth(0, 50 * 256);

        addColumnData(rowNumber, harUtils.urlList, 0);
        addColumnData(rowNumber, harUtils.PageTimesList, 1);
        addPageInfoChart(rowNumber, harUtils.PageTimesList.size());

        writeWorkbook();
    }

    public void createSpreadsheetFromHar(String excelFileName, wguHarFileUtils harUtils) throws Exception {
        int rowNumber = 36; // zero based

        // Write it to a spreadsheet
        createWorkbook(excelFileName);
        createSheet("ANewSheet1").setColumnWidth(0, 90 * 256);
        addColumnHeaders(rowNumber - 1, harUtils.headerList, 1);
        addColumnData(rowNumber, harUtils.urlList,     0);
        addColumnData(rowNumber, harUtils.blockedList, 1);
        addColumnData(rowNumber, harUtils.dnsList,     2);
        addColumnData(rowNumber, harUtils.connectList, 3);
        addColumnData(rowNumber, harUtils.sendList,    4);
        addColumnData(rowNumber, harUtils.waitList,    5);
        addColumnData(rowNumber, harUtils.receiveList, 6);
        addColumnData(rowNumber, harUtils.sslList,     7);
        addColumnData(rowNumber, harUtils.totalList,   8);
        addColumnData(rowNumber, harUtils.errorList,   9);
        addTimingInfoChart(rowNumber, harUtils.urlList.size());

        writeWorkbook();
    }

}




















package Utils;


/*
 * Created by timothy.hallbeck on 8/24/2015.
 */

public class CourseChapter {
    private String title="";
    private String url="";

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    CourseChapter() {}

    CourseChapter(String title, String url) {
        this.title = title;
        this.url   = url;
    }
}


package Utils;

/*
 * Created by timothy.hallbeck on 10/12/2015.
 */

import MobilePages.MobileAppPage;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class wguViewLayoutUtils {

    private int width = 0;
    private int height = 0;

    public int getWidth() {
        return width;
    }
    public int getHeight() {
        return height;
    }

    public static void SetToNormalMode(MobileAppPage page) {
        page.driver.switchTo().window("NATIVE_APP");
    }
    public static void SetToWebViewMode(MobileAppPage page) {
        Set<String> handles = page.driver.getWindowHandles();
        String webView = "";

        for (String type : handles)
            if (type.contains("WEBVIEW")) {
                webView = type;
                break;
            }

        Assert.assertTrue(!webView.isEmpty(), "No WebView objects found on this page");
        page.driver.switchTo().window(webView);
    }

    protected wguViewLayoutUtils(MobileAppPage page) throws Exception {
        Thread.sleep(2000);
        WebElement topLevel = page.driver.findElement(By.xpath("//DecorView"));
        Point location = topLevel.getLocation();
        Dimension size = topLevel.getSize();

        assert (location.x == 0);
        assert (location.y == 0);

        width = size.width;
        height = size.height;

        assert (width > 0);
        assert (height > 0);
    }

    public List<WebElement> getAllLayouts(MobileAppPage page) throws Exception {
        return page.driver.findElements(By.xpath("//LinearLayout"));
    }

    public List<WebElement> getAllElements(MobileAppPage page) throws Exception {
        return getAllElements(page, false);
    }

    public List<WebElement> getAllElements(MobileAppPage page, boolean bViewableOnly) throws Exception {
        List<WebElement> allElements = page.driver.findElements(By.xpath(".//*"));
        if (!bViewableOnly)
            return allElements;

        Iterator iterator = allElements.iterator();

        WebElement element;
        Point location;

        while (iterator.hasNext()) {
            element = (WebElement) iterator.next();
            location = element.getLocation();

            if (!element.isDisplayed() || location.x <= 0 || location.y <= 0 ||
                    location.x > getWidth() || location.y > getHeight())
                iterator.remove();
        }
        return allElements;
    }

    public List<WebElement> getAllWebViewElements(MobileAppPage page) throws Exception {
        SetToWebViewMode(page);
        List<WebElement> allElements = getAllElements(page);
        for (WebElement element : allElements) {
            General.Debug(element.getTagName());
            General.Debug("  " + element.getText());
        }
        SetToNormalMode(page);

        return allElements;
    }
}



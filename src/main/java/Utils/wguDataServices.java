
package Utils;

/*
 * Created by timothy.hallbeck on 9/23/2015.
 *   Individual service calls for use by Jenkins jobs. More detailed checking of responses than
 *   the overall service checking tests.
 */

import BaseClasses.LoginType;
import org.testng.annotations.Test;

public class wguDataServices extends OssoSession {

    private String testRunner(LoginType login, GetPost getPost, String[] data) throws Exception {
        String result;

        if (getPost == GetPost.GET)
            result = Get(data[0], login);
        else
            result = Post(data[0], login, data[1]);
        verifyKey(result, data[1]);

        return result;
    }

    @Test
    public void devAssessmentAssess() throws Exception {
        String result = testRunner(LoginType.LANE2_CPOINDEXTER, GetPost.GET, wguDataProviders.AssessmentAssess);
        verifyKey(result, "$https://l2asa.wgu.edu/asa/assessmentRequest/route?assmnt_code=1880&pidm=106679&returnURL&cvid=1332");
    }
    @Test
    public void lane1AssessmentAssess() throws Exception {
        String result = testRunner(LoginType.LANE1_NORMAL, GetPost.GET, wguDataProviders.AssessmentAssess);
        verifyKey(result, "$https://l1asa.wgu.edu/asa/assessmentRequest/route?assmnt_code=1880&pidm=106679&returnURL&cvid=1332");
    }
    @Test
    public void prodAssessments() throws Exception {
        String result = testRunner(LoginType.PRODUCTION_NORMAL, GetPost.GET, wguDataProviders.AssessmentAssess);
        verifyKey(result, "$https://asa.wgu.edu/asa/assessmentRequest/route?assmnt_code=1880&pidm=106679&returnURL&cvid=1332");
    }


    @Test
    public void devAssessmentAttempt() throws Exception {
        String result = testRunner(LoginType.LANE2_CPOINDEXTER, GetPost.GET, wguDataProviders.AssessmentAttempt);
        verifyKey(result, "$http://l2webapp3.wgu.edu/Aap/faces/PreAssessmentTab.jsp?assmnt_code=1880&pidm=106679&userid=cpoindexter&myses=0&cvid=1332");
    }
    @Test
    public void lane1AssessmentAttempt() throws Exception {
        String result = testRunner(LoginType.LANE1_NORMAL, GetPost.GET, wguDataProviders.AssessmentAttempt);
        verifyKey(result, "$http://l1webapp3.wgu.edu/Aap/faces/PreAssessmentTab.jsp?assmnt_code=1880&pidm=106679&userid=cpoindexter&myses=0&cvid=1332");
    }
    @Test
    public void prodAssessmentAttempt() throws Exception {
        String result = testRunner(LoginType.PRODUCTION_NORMAL, GetPost.GET, wguDataProviders.AssessmentAttempt);
        verifyKey(result, "$http://webapp3.wgu.edu/Aap/faces/PreAssessmentTab.jsp?assmnt_code=1880&pidm=106679&userid=cpoindexter&myses=0&cvid=1332");
    }


    @Test
    public void devAssessmentCompetencies() throws Exception {
        String result = testRunner(LoginType.LANE2_CPOINDEXTER, GetPost.GET, wguDataProviders.AssessmentCompetencies);
        verifyKey(result, "title");
        verifyKey(result, "description");
        verifyKey(result, "assessmentCode");
        verifyKey(result, "assessmentId");
        verifyKey(result, "competencyId");
        verifyKey(result, "code");
    }
    @Test
    public void lane1AssessmentCompetencies() throws Exception {
        String result = testRunner(LoginType.LANE1_NORMAL, GetPost.GET, wguDataProviders.AssessmentCompetencies);
    }
    @Test
    public void prodAssessmentCompetencies() throws Exception {
        String result = testRunner(LoginType.PRODUCTION_NORMAL, GetPost.GET, wguDataProviders.AssessmentCompetencies);
    }


    @Test
    public void devAssessmentCourses() throws Exception {
        String result = testRunner(LoginType.LANE2_CPOINDEXTER, GetPost.GET, wguDataProviders.AssessmentCourses);
    }
    @Test
    public void lane1AssessmentCourses() throws Exception {
        String result = testRunner(LoginType.LANE1_NORMAL, GetPost.GET, wguDataProviders.AssessmentCourses);
    }
    @Test
    public void prodAssessmentCourses() throws Exception {
        String result = testRunner(LoginType.PRODUCTION_NORMAL, GetPost.GET, wguDataProviders.AssessmentCourses);
    }


    @Test
    public void devAssessmentCourseVersion() throws Exception {
        String result = testRunner(LoginType.LANE2_CPOINDEXTER, GetPost.GET, wguDataProviders.AssessmentCourseVersion);
    }
    @Test
    public void lane1AssessmentCourseVersion() throws Exception {
        String result = testRunner(LoginType.LANE1_NORMAL, GetPost.GET, wguDataProviders.AssessmentCourseVersion);
    }
    @Test
    public void prodAssessmentCourseVersion() throws Exception {
        String result = testRunner(LoginType.PRODUCTION_NORMAL, GetPost.GET, wguDataProviders.AssessmentCourseVersion);
    }


    @Test
    public void devCoachingReportPlanId() throws Exception {
        String result = testRunner(LoginType.LANE2_CPOINDEXTER, GetPost.GET, wguDataProviders.CoachingReportPlanId);
    }
    @Test
    public void lane1CoachingReportPlanId() throws Exception {
        String result = testRunner(LoginType.LANE1_NORMAL, GetPost.GET, wguDataProviders.CoachingReportPlanId);
    }
    @Test
    public void prodCoachingReportPlanId() throws Exception {
        String result = testRunner(LoginType.PRODUCTION_NORMAL, GetPost.GET, wguDataProviders.CoachingReportPlanId);
    }


    @Test
    public void devCosBCourseVersionId() throws Exception {
        String result = testRunner(LoginType.LANE2_CPOINDEXTER, GetPost.GET, wguDataProviders.CosBCourseVersionId);
    }
    @Test
    public void lane1CosBCourseVersionId() throws Exception {
        String result = testRunner(LoginType.LANE1_NORMAL, GetPost.GET, wguDataProviders.CosBCourseVersionId);
    }
    @Test
    public void prodCosBCourseVersionId() throws Exception {
        String result = testRunner(LoginType.PRODUCTION_NORMAL, GetPost.GET, wguDataProviders.CosBCourseVersionId);
    }


    @Test
    public void devCosBCourseVersionDetail() throws Exception {
        String result = testRunner(LoginType.LANE2_CPOINDEXTER, GetPost.GET, wguDataProviders.CosBCourseVersionDetail);
    }
    @Test
    public void lane1CosBCourseVersionDetail() throws Exception {
        String result = testRunner(LoginType.LANE1_NORMAL, GetPost.GET, wguDataProviders.CosBCourseVersionDetail);
    }
    @Test
    public void prodCosBCourseVersionDetail() throws Exception {
        String result = testRunner(LoginType.PRODUCTION_NORMAL, GetPost.GET, wguDataProviders.CosBCourseVersionDetail);
    }


}

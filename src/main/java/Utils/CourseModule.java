
package Utils;


/*
 * Created by timothy.hallbeck on 8/24/2015.
 */

public class CourseModule {
    private String          title="";
    private String          url="";
    private CourseChapter[] chapters;

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    public CourseChapter[] getChapters() {
        return chapters;
    }

    CourseModule() {}

    CourseModule(String title, String url, CourseChapter[] chapters) {
        this.title    = title;
        this.url      = url;
        this.chapters = chapters;
    }
}

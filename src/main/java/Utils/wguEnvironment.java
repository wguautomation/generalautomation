
package Utils;

/*
 * Created by timothy.hallbeck on 1/11/2016.
 */

public enum wguEnvironment {
    Lane2Dev,
    Lane1Qa,
    Prod,
    none,
    ;

    static public wguEnvironment getEnv(String url) {
        if (isLane1Prefix(url))
            return Lane1Qa;
        else if (isLane2Prefix(url))
            return Lane2Dev;
        else
            return Prod;
    }

    static public boolean isLane1Prefix(String url) {
        return url.toLowerCase().contains("l1my");
    }

    static public boolean isLane2Prefix(String url) {
        return  url.toLowerCase().contains("l2my");
    }

    static public boolean isProdPrefix(String url) {
        return !isLane1Prefix(url) && !isLane2Prefix(url);
    }

}


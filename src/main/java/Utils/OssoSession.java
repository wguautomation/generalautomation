
package Utils;

import BaseClasses.LoginType;
import org.apache.commons.lang3.ArrayUtils;
import org.testng.TestNGException;

/*
 * Created by timothy.hallbeck on 6/13/2016.
 */

public class OssoSession extends wguCurlExecuter {

    static protected String devOSSO    = "";
    static protected String qaOSSO     = "";
    static protected String prodOSSO   = "";

    static public void ClearTokens() {
        devOSSO   = "";
        qaOSSO  = "";
        prodOSSO = "";
        General.Sleep(100); // cant get new tokens too often
    }

    public String getToken(LoginType login) {
        setUsername(login.getUsername());
        setPassword(login.getPassword());

        if (login.isLane1())
            return getQaOSSOToken();
        else if (login.isLane2())
            return getDevOSSOToken();
        return getProdOSSOToken();
    }

    protected synchronized String getDevOSSOToken() {
        if (!devOSSO.isEmpty())
            return devOSSO;

        String[] fullCommand = ArrayUtils.addAll(new String[]{
                "curl", "-k", "-v", "-X", "POST",
                "\"https://l2osso.wgu.edu/opensso/identity/authenticate?username=" + username + "&password=" + password + "\"",
        } );

        General.Debug("Getting new dev token");
        General.Debug(General.StringArrayToString(fullCommand, " "));

        String result = executeCurl(fullCommand);
        devOSSO = verifyKey(result, "token.id");

        // Yes, doing a delay inside a synchronized method. SSO will puke at more than 10 token gets / sec
        delayThread(100);

        return devOSSO;
    }

    protected synchronized String getQaOSSOToken() {
        if (!qaOSSO.isEmpty())
            return qaOSSO;

        String[] fullCommand = ArrayUtils.addAll(new String[]{
                "curl", "-k", "-v", "-X", "POST",
                "\"https://l1osso.wgu.edu/opensso/identity/authenticate?username=" + username + "&password=" + password + "\"",
        } );

        General.Debug("Getting new qa token");
        General.Debug(General.StringArrayToString(fullCommand, " "));

        String result = executeCurl(fullCommand);
        qaOSSO = verifyKey(result, "token.id");

        return qaOSSO;
    }

    protected synchronized String getProdOSSOToken() {
        if (!prodOSSO.isEmpty())
            return prodOSSO;

        String[] fullCommand = ArrayUtils.addAll(new String[]{
                "curl", "-k", "-v", "-X", "POST",
                "\"https://osso.wgu.edu/opensso/identity/authenticate?username=" + username + "&password=" + password + "\"",
        } );

        General.Debug("Getting new prod token");
        General.Debug(General.StringArrayToString(fullCommand, " "));

        String result = executeCurl(fullCommand);
        prodOSSO = verifyKey(result, "token.id");

        return prodOSSO;
    }

    public String Get(String serviceName, String partialCommand, LoginType login) {
        return Get(serviceName, partialCommand, login, null);
    }
    public String Get(String serviceName, String partialCommand, LoginType login, String[] searchTerms) {
        return Get("https://" + serviceName + login.getLanePrefix() + ".wgu.edu" + partialCommand, login, searchTerms);
    }
    public String Get(String command, LoginType login) {
        return Get(command, login, null);
    }
    public String Get(String command, LoginType login, String[] searchTerms) {
        String token="", extraKey="";

        setUsername(login.getUsername());
        setPassword(login.getPassword());

        command = login.expandMacros(command);

        try {
            if (login.getEnv() == wguEnvironment.Lane2Dev) {
                token = getDevOSSOToken();
                if (command.contains("//mashery."))
                    extraKey = "\"api_key: e5uynbx8y282yv7a44k8bbp4\"";
            } else if (login.getEnv() == wguEnvironment.Lane1Qa) {
                token = getQaOSSOToken();
                if (command.contains("//mashery."))
                    extraKey = "\"api_key: fkuurempkb9jw7qyubmcsv4p\"";
            } else if (login.getEnv() == wguEnvironment.Prod) {
                token = getProdOSSOToken();
                if (command.contains("//mashery."))
                    extraKey = "\"api_key: 3fy47c6dj6jrmezz5y2bxd6f\"";
            }
        } catch (Exception e) {
            General.SendTextToOutputWindow("Failed to get osso token");
            throw new TestNGException("Failed to get osso token: " + e.getMessage());
        }

        String[] fullCommand = ArrayUtils.addAll(new String[] { "curl", "-k", "-v" });
        if (!extraKey.isEmpty())
            fullCommand = ArrayUtils.addAll(fullCommand, "-H", extraKey);

        fullCommand = ArrayUtils.addAll(
            fullCommand, "-G",
            "-H", "\"x-wgu-auth: " + token + "\"",
            "\"" + command + "\"");

        General.Debug(General.StringArrayToString(fullCommand, " "));
        String result = executeCurl(fullCommand, searchTerms);

        return result;
    }

    public String[] GetArray(String serviceName, String partialCommand, LoginType login) {
        String[] values = new String[4];
        String result = Get("https://" + serviceName + login.getLanePrefix() + ".wgu.edu" + partialCommand, login);

        return values;
    }

    public String Post(String command, LoginType login) {
        return Post(command, login, null);
    }
    public String Post(String command, LoginType login, String data) {
        String token="", prefix="", extraKey="";

        setUsername(login.getUsername());
        setPassword(login.getPassword());

        command = login.expandMacros(command);

        // Lane1Qa Ping key   d2d1X21vYmlsZTpzWmtCajZGZ1gxOXZ0dFdkd1llS1Y5UjJHRmFGRXRzczZ5RzZ5cmtlRWlhdGREVnF6WnF4RFJDcENpbXlRRlZG
        if (login.getEnv() == wguEnvironment.Lane2Dev) {
            token = getDevOSSOToken();
        } else if (login.getEnv() == wguEnvironment.Lane1Qa) {
            token = getQaOSSOToken();
        } else if (login.getEnv() == wguEnvironment.Prod){
            token = getProdOSSOToken();
        }

        String[] fullCommand = ArrayUtils.addAll(new String[] { "curl", "-k", "-v", "-X", "POST" });
        if (!extraKey.isEmpty())
            fullCommand = ArrayUtils.addAll(fullCommand, "-H", extraKey);

        fullCommand = ArrayUtils.addAll(
            fullCommand, "\"" + prefix + command + "\"",
//            "-H", "\"Content-Type: application/json\"",
//            "-d", data,
            "-H", "\"x-wgu-auth: " + token + "\"");

        General.Debug(General.StringArrayToString(fullCommand, " "));
        String result = executeCurl(fullCommand);

        return result;
    }

    public String Put(String command, LoginType login) {
        return Put(command, login, null);
    }
    public String Put(String command, LoginType login, String data) {
        String token="", prefix="", extraKey="";

        setUsername(login.getUsername());
        setPassword(login.getPassword());

        command = login.expandMacros(command);

        // Lane1Qa Ping key   d2d1X21vYmlsZTpzWmtCajZGZ1gxOXZ0dFdkd1llS1Y5UjJHRmFGRXRzczZ5RzZ5cmtlRWlhdGREVnF6WnF4RFJDcENpbXlRRlZG
        if (login.getEnv() == wguEnvironment.Lane2Dev) {
            token = getDevOSSOToken();
        } else if (login.getEnv() == wguEnvironment.Lane1Qa) {
            token = getQaOSSOToken();
        } else if (login.getEnv() == wguEnvironment.Prod){
            token = getProdOSSOToken();
        }

        String[] fullCommand = ArrayUtils.addAll(new String[] { "curl", "-k", "-v", "-X", "PUT" });
        if (!extraKey.isEmpty())
            fullCommand = ArrayUtils.addAll(fullCommand, "-H", extraKey);

        fullCommand = ArrayUtils.addAll(
                fullCommand, "\"" + prefix + command + "\"",
                "-H", "\"x-wgu-auth: " + token + "\"");

        if (data != null)
            fullCommand = ArrayUtils.addAll(
                fullCommand,
                "-H", "\"Content-Type: application/json\"",
                "-d", data);

        General.Debug(General.StringArrayToString(fullCommand, " "));
        String result = executeCurl(fullCommand);

        return result;
    }
}


package Utils;

import BaseClasses.LoginType;
import JsonDataClasses.*;
import org.apache.commons.lang3.ArrayUtils;
import org.testng.TestNGException;

/*
 * Created by timothy.hallbeck on 6/13/2016.
 */

public class PingSession extends wguCurlExecuter {

    public enum ServiceKey {
        ENV,
        FEATURES,
        HEALTH,
        INFO,
        MAPPINGS,
        METRICS,
        NULL,
    };

    private String               serviceName = "";
    private LoginType            login       = null;
    private boolean              loaded      = false;

    private JsonServiceEnv       env       = null;
    private JsonServiceHealth    health    = null;
    private JsonServiceInfo      info      = null;
    private JsonServiceMetrics   metrics   = null;
    private JsonServiceFeatures  features  = null;
    private JsonServiceMappings  mappings  = null;

    static protected String devPing    = "";
    static protected String qaPing     = "";
    static protected String prodPing   = "";
    private final int TOKEN_DELAY      = 100;

    public PingSession() {}
    public PingSession(String serviceName, LoginType login) {
        load(serviceName, login, null);
    }
    public PingSession(String serviceName, LoginType login, ServiceKey key) {
        load(serviceName, login, key);
    }

    public String load(String serviceName, LoginType login) {
        return load(serviceName, login, null);
    }
    public String load(String serviceName, LoginType login, ServiceKey key) {
        this.serviceName = serviceName;
        this.login = login;
        this.loaded = true;

        if (key == null)
            return "";

        String serverUrl = getServer(login);
        String result = "";

        switch (key) {
            case ENV:
                env = new JsonServiceEnv(Get(serverUrl + "/env", login));
                break;

            case FEATURES:
                features = new JsonServiceFeatures(Get(serverUrl + "/features", login));
                break;

            case HEALTH:
                result = Get(serverUrl + "/health", login);
                checkForErrorCodes(result);

                if (result.equals("") || result.equals("{}"))
                    General.DebugAndOutputWindow("\n/info has not been implemented for this service");
                else
                    health = new JsonServiceHealth(result);
                break;

            case INFO:
                result = Get(serverUrl + "/info", login);
                checkForErrorCodes(result);

                if (result.equals("") || result.equals("{}"))
                    General.DebugAndOutputWindow("\n/info has not been implemented for this service");
                else {
                    info = new JsonServiceInfo(result);
                    General.Debug("Location:      " + login.getLaneDisplayName());
                    result = info.toString();
                }
                break;

            case MAPPINGS:
                if (mappings == null) {
                    mappings = new JsonServiceMappings(Get(serverUrl + "/mappings", login), false);
                    result = mappings.toString();
                }
                break;

            case METRICS:
                metrics = new JsonServiceMetrics(Get(serverUrl + "/metrics", login));
                result = metrics.toString();
                break;

            case NULL:
                break;
        }
        return result;
    }

    public JsonServiceMappings getMappings() {
        if (mappings == null)
            throw new TestNGException("Attempting to use getMappings() when mappings is null");
        return mappings;
    }

    public JsonServiceFeatures getFeatures() {
        if (features == null)
            throw new TestNGException("Attempting to use getFeatures() when features is null");
        return features;
    }

    public JsonServiceHealth getHealth() {
        if (health == null)
            throw new TestNGException("Attempting to use getHealth() when health is null");
        return health;
    }

    public JsonServiceInfo getInfo() {
        if (info == null)
            throw new TestNGException("Attempting to use getInfo() when features is null");
        return info;
    }

    public JsonServiceMetrics getMetrics() {
        if (metrics == null)
            throw new TestNGException("Attempting to use getMetrics() when metrics is null");
        return metrics;
    }

    public JsonServiceEnv getEnv() {
        if (env == null)
            throw new TestNGException("Attempting to use getEnv() when env is null");
        return env;
    }

    public String getServer(LoginType login) {
        if (loaded)
            return "https://" + serviceName + login.getLanePrefix() + ".wgu.edu";
        throw new TestNGException("Attempting to use getServer() before call to load()");
    }

    public LoginType getLogin() {
        if (login == null)
            throw new TestNGException("Using login before it has been set in PingSession::getLogin()");
        if (loaded)
            return login;
        throw new TestNGException("Attempting to use getLogin() before call to load()");
    }

    static public void ClearTokens() {
        qaPing   = "";
        devPing  = "";
        prodPing = "";
//        General.Sleep(100); // cant get new tokens too often
    }

    public String getToken(LoginType login) {
        if (login.isLane1())
            return getQaPing(login);
        else if (login.isLane2())
            return getDevPing(login);
        return getProdPing(login);
    }

    protected String getDevPing(LoginType login) {
        General.Sleep(TOKEN_DELAY); // Problem with multiple thread...asking for tokens too fast
        if (!devPing.isEmpty())
            return devPing;

        String[] fullCommand = ArrayUtils.addAll(new String[]{
                "curl", "-k", "-v", "-X", "POST",
                "-H", "\"Authorization: Basic d2d1X21vYmlsZTpzWmtCajZGZ1gxOXZ0dFdkd1llS1Y5UjJHRmFGRXRzczZ5RzZ5cmtlRWlhdGREVnF6WnF4RFJDcENpbXlRRlZG\"",
                "-d", "\"grant_type=password&username=" + login.getUsername() + "&password=" + login.getPassword() + "\"",
                "\"https://access.dev.wgu.edu/pingfed/as/token.oauth2\"",
        } );

        General.Debug(General.StringArrayToString(fullCommand, " "));

        String result = executeCurl(fullCommand);
        String error = verifyKey(result, "error", null, true) + verifyKey(result, "error_description", null, true);
        if (!error.isEmpty())
            throw new TestNGException(error);
        devPing = verifyKey(result, "access_token");

        return devPing;
    }

    protected String getQaPing(LoginType login) {
        General.Sleep(TOKEN_DELAY); // Problem with multiple thread...asking for tokens too fast
        if (!qaPing.isEmpty())
            return qaPing;

        String[] fullCommand = ArrayUtils.addAll(new String[]{
                "curl", "-k", "-v", "-X", "POST",
                "-H", "\"Authorization: Basic d2d1X21vYmlsZTpzWmtCajZGZ1gxOXZ0dFdkd1llS1Y5UjJHRmFGRXRzczZ5RzZ5cmtlRWlhdGREVnF6WnF4RFJDcENpbXlRRlZG\"",
                "-d", "\"grant_type=password&username=" + login.getUsername() + "&password=" + login.getPassword() + "\"",
                "\"https://access.qa.wgu.edu/pingfed/as/token.oauth2\"",
        } );

        General.Debug(General.StringArrayToString(fullCommand, " "));

        String result = executeCurl(fullCommand);
        String error = verifyKey(result, "error", null, true) + verifyKey(result, "error_description", null, true);
        if (!error.isEmpty())
            throw new TestNGException(error);
        qaPing = verifyKey(result, "access_token");

        return qaPing;
    }

    protected String getProdPing(LoginType login) {
        General.Sleep(TOKEN_DELAY); // Problem with multiple thread...asking for tokens too fast
        if (!prodPing.isEmpty())
            return prodPing;

        prodPing = General.GetProdPingToken();
        return prodPing;
      }

    public String GetEndpoint(String partialCommand) {
        return Get(getServer(login) + partialCommand, login, false, null);
    }
    public String GetEndpoint(String partialCommand, String[] searchTerms) {
        return Get(getServer(login) + partialCommand, login, false, searchTerms);
    }
    public String Get(String command, LoginType login) {
        return Get(command, login, false, null);
    }
    public String Get(String command, LoginType login, boolean bUseJwtFormat, String[] searchTerms) {
        String token="";
        if (!loaded)
            throw new TestNGException("Attempting to use Get() before call to load()");

        setUsername(login.getUsername());
        setPassword(login.getPassword());

        command = login.expandMacros(command);
        General.Debug(command);

        token = getToken(login);
        String[] fullCommand = ArrayUtils.addAll(new String[] { "curl", "-k", "-v" });

        if (!bUseJwtFormat) {
            fullCommand = ArrayUtils.addAll(
                    fullCommand, "-G",
                    "-H", "\"Authorization: Bearer " + token + "\"",
                    "\"" + command + "\"");
        } else {
            fullCommand = ArrayUtils.addAll(
                    fullCommand, "-G",
                    "-d", "\"{\\\"jwtValue\\\":\\\"" + token + "\\\"}\"",
                    "\"" + command + "\"");
        }
        String output = General.StringArrayToString(fullCommand, " ");
        General.Debug(output);
        if (searchTerms != null)
            searchTerms[0] = output;
        String result = executeCurl(fullCommand);

        return result;
    }

    public String Put(String command, LoginType login) {
        return Put(command, login, null);
    }
    public String Put(String command, LoginType login, String data) {
        return ProcessPostDeletePut(command, login, data, "PUT");
    }

    public String Post(String command, LoginType login) {
        return Post(command, login, null);
    }
    public String Post(String command, LoginType login, String data) {
        return Post(command, login, data, false);
    }
    public String Post(String command, LoginType login, String data, boolean bUseJwtFormat) {
        return ProcessPostDeletePut(command, login, data, "POST", bUseJwtFormat);
    }

    public String Delete(String command, LoginType login) {
        return Delete(command, login, null);
    }
    public String Delete(String command, LoginType login, String data) {
        return ProcessPostDeletePut(command, login, data, "DELETE", false);
    }
    private String ProcessPostDeletePut(String command, LoginType login, String data, String type) {
        return ProcessPostDeletePut(command, login, data, type, false);
    }
    private String ProcessPostDeletePut(String command, LoginType login, String data, String type, boolean bUseJwtFormat) {
        String token="";
        if (!loaded)
            throw new TestNGException("Attempting to use ProcessPostDeletePut() before call to load()");

        setUsername(login.getUsername());
        setPassword(login.getPassword());

        command = login.expandMacros(command);
        token = getToken(login);
        String[] fullCommand = ArrayUtils.addAll(new String[] { "curl", "-k", "-v", "-X", type} );

        fullCommand = ArrayUtils.addAll(
                fullCommand,
                "-H", "\"Content-Type: application/json\"");
        if (data != null)
            fullCommand = ArrayUtils.addAll(fullCommand, "-d", data);

        if (!bUseJwtFormat) {
            fullCommand = ArrayUtils.addAll(
                    fullCommand,
                    "-H", "\"Authorization: Bearer " + token + "\"",
                    "\"" + command + "\"");
        } else {
            fullCommand = ArrayUtils.addAll(
                    fullCommand,
                    "-d", "\"{\\\"jwtValue\\\":\\\"" + token + "\\\"}\"",
                    "\"" + command + "\"");
        }

        General.Debug(General.StringArrayToString(fullCommand, " "));
        String result = executeCurl(fullCommand);

        return result;
    }
}


package TestlinkPages;

import BaseClasses.LoginType;
import BaseClasses.WebPage;
import Utils.General;
import Utils.wguExcelFileUtils;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by timothy.hallbeck on 11/24/2015.
 */

public class testlinkHomePage extends WebPage {

    public testlinkHomePage() {
        super(null, LoginType.TESTLINK);
    }

    public testlinkHomePage load() {
        if (!login())
            driver.get(loginType.getStartingUrl());

        return this;
    }

    public testlinkHomePage selectProject(String dropdownText) {
        switchToFrame(0);
        getByXpath(testlinkXpath.IndexPage_TestProject).click();
        getByXpath("//*[@title='" + dropdownText + "']").click();
        switchToDefaultFrame();

        return this;
    }

    public testlinkHomePage OpenAllFirstFolderCases(String dropdownText) {
        load();
        selectProject(dropdownText);
        clickBrowseTestCases();
        clickExpandWholeTree();
        clickFirstFolder();
        clickSecondFolder();
        clickFirstFolder();

        return this;
    }

    public testlinkHomePage OpenAllSecondFolderCases(String dropdownText) {
        load();
        selectProject(dropdownText);
        clickBrowseTestCases();
        clickExpandWholeTree();
        clickFirstFolder();

        return this;
    }

    public void AddAllTestCases(String type, String prefix) throws Exception {
        switchToDefaultFrame();
        switchToFrame("treeframe");
        List<WebElement> list = getListByXpath("//*[@class='x-tree-node-anchor']", false);
        wguExcelFileUtils myExcelUtil = new wguExcelFileUtils();

        myExcelUtil.createWorkbook("C:\\Users\\timothy.hallbeck\\Downloads\\ReadyForQtestImport_" + type + " .xlsx");
        myExcelUtil.createSheet(type).setColumnWidth(0, 40 * 256);
        myExcelUtil.writeWorkbook();

        String title;
        testlinkTestCase testcase;

        int row = 0;
        ArrayList<String> columns = new ArrayList<>();
        // Add header row
        columns.clear();
        columns.add("Test Case Id");
        columns.add("Test Case Name");
        columns.add("Description");
        columns.add("Pre-condition");
        columns.add("Step #");
        columns.add("Step Description");
        columns.add("Expected Result");
        columns.add("Test Case Type");
        columns.add("Test Case Status");
        columns.add("Test Case Assigned To");
        columns.add("Custom Field 1 (if any)");
        columns.add("Custom Field 2 (if any)");
        columns.add("Custom Field 3 (if any)");
        myExcelUtil.addRowData(row++, columns);

        for (WebElement element : list) {
            switchToDefaultFrame();
            switchToFrame("treeframe");
            title = element.getText();
            General.Debug("  title:" + title);
            General.Debug("");

            if (title.contains(prefix) && title.contains(":")) {
                row++;
                element.click();
                Sleep(1000);
                testcase = new testlinkTestCase(title, getSummary(), getPreconditions(), getSteps(), getResults());

                // Add 1st row
                columns.clear();
                columns.add(""); // test case id
                columns.add(title);
                columns.add(testcase.getSummary());
                columns.add(testcase.getPreconditions());
                columns.add("1");
                if (testcase.getSteps().size() > 0) {
                    columns.add(testcase.getSteps().get(0));
                    columns.add(testcase.getResults().get(0));
                }
                myExcelUtil.addRowData(row++, columns);

                // Add all the other rows
                for (int i=1; i<testcase.getSteps().size(); i++) {
                    columns.clear();
                    columns.add("");
                    columns.add("");
                    columns.add("");
                    columns.add("");
                    columns.add(Integer.toString(i + 1));
                    columns.add(testcase.getSteps().get(i));
                    columns.add(testcase.getResults().get(i));
                    myExcelUtil.addRowData(row++, columns);
                }

                myExcelUtil.writeWorkbook();
            }
        }
        switchToDefaultFrame();

    }

    private int findHeaderRow(String headerText) {
        switchToDefaultFrame();
        switchToFrame("workframe");
        WebElement element;
        int i;
        for (i=7; i >= 0; i--) {
            element = getByXpath("//*[@id='stepsControls']/table/tbody/tr[" + i + "]/td", true);
            if (element != null && element.getText().toLowerCase().contains(headerText.toLowerCase()))
                break;
        }

        return i;
    }

    public String getSummary() {
        switchToDefaultFrame();
        switchToFrame("workframe");
        int row = findHeaderRow("summary");

        if (row != -1) {
            WebElement summary = getByXpath("//*[@id='stepsControls']/table/tbody/tr[" + ++row + "]/td", true);
            if (summary != null)
                return summary.getText();
        }
        return "";
    }

    public String getPreconditions() {
        switchToDefaultFrame();
        switchToFrame("workframe");
        int row = findHeaderRow("preconditions");

        if (row != -1) {
            WebElement summary = getByXpath("//*[@id='stepsControls']/table/tbody/tr[" + ++row + "]/td", true);
            if (summary != null)
                return summary.getText();
        }
        return "";
    }

    public ArrayList<String> getSteps() {
        ArrayList<String> steps = new ArrayList<>();
        WebElement step;
        int row = 1;

        do {
            step = getByXpath("//*[@id='step_row_" + row + "']/td[2]", true);
            if (step == null)
                break;
            steps.add(step.getText());
        } while (++row < 100);

        return steps;
    }

    public ArrayList<String> getResults() {
        ArrayList<String> results = new ArrayList<>();
        WebElement result;
        int row = 1;

        do {
            result = getByXpath("//*[@id='step_row_" + row + "']/td[3]", true);
            if (result == null)
                break;
            results.add(result.getText());
        } while (++row < 100);


        return results;
    }

    public testlinkHomePage clickBrowseTestCases() {
        switchToFrame(0);
        get(testlinkXpath.IndexPage_BrowseTestCases);
        switchToDefaultFrame();

        return this;
    }

    public testlinkHomePage clickExpandWholeTree() {
        switchToFrame(0);
        getByXpath(testlinkXpath.TextSpecPage_ExpandWholeTree).click();
        switchToDefaultFrame();
        Sleep(15000);

        return this;
    }

    public testlinkHomePage clickCollapseWholeTree() {
        switchToFrame(0);
        getByXpath(testlinkXpath.TextSpecPage_CollapseWholeTree).click();
        switchToDefaultFrame();
        Sleep(1000);

        return this;
    }

    public testlinkHomePage clickSubFolder(int index) {
        switchToFrame("treeframe");
        getByXpath("//*[@id='ext-gen15']/li/ul/li[" + index + "]/div/img[1]").click();
        switchToDefaultFrame();
        Sleep(1000);

        return this;
    }

    public testlinkHomePage clickFirstFolder() {
        switchToFrame("treeframe");
        getByXpath(testlinkXpath.TextSpecPage_IosArrow).click();
        switchToDefaultFrame();
        Sleep(1000);

        return this;
    }

    public testlinkHomePage clickSecondFolder() {
        switchToFrame("treeframe");
        getByXpath(testlinkXpath.TextSpecPage_AndroidArrow).click();
        switchToDefaultFrame();
        Sleep(1000);

        return this;
    }

    public testlinkHomePage ExercisePage(boolean cascade) {
        General.Debug("\ntestlinkHomePage::ExercisePage(" + cascade + ")");
        load();

        return this;
    }


}


package TestlinkPages;

/*
 * Created by timothy.hallbeck on 11/30/2015.
 */

public class testlinkXpath {

    public static String IndexPage_BrowseTestCases      = "https://testlink.wgu.edu/lib/general/frmWorkArea.php?feature=editTc";
    public static String IndexPage_TestProject          = "/html/body/div[3]/div/form/select";

    public static String TextSpecPage_WGUMobile         = "//*[@id='ext-gen15']/li/div/img[1]";
    public static String TextSpecPage_IosArrow          = "//*[@id='ext-gen15']/li/ul/li[1]/div/img[1]";
    public static String TextSpecPage_AndroidArrow      = "//*[@id='ext-gen15']/li/ul/li[2]/div/img[1]";
    public static String TextSpecPage_ExpandWholeTree   = "//*[@id='expand_tree']";
    public static String TextSpecPage_CollapseWholeTree = "//*[@id='collapse_tree']";

}
//*[@id="ext-gen15"]/li/ul/li[1]/div/img[1]
//*[@id="ext-gen15"]/li/ul/li[2]/div/img[1]
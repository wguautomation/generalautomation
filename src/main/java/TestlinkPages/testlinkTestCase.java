
package TestlinkPages;

import java.util.ArrayList;

/*
 * Created by timothy.hallbeck on 12/1/2015.
 */

public class testlinkTestCase {

    private final String            title;
    private final String            summary;
    private final String            preconditions;
    private final ArrayList<String> steps;
    private final ArrayList<String> results;

    testlinkTestCase(String title, String summary, String preconditions, ArrayList<String> steps, ArrayList<String> results) {
        this.title         = title;
        this.summary       = summary;
        this.preconditions = preconditions;
        this.steps         = steps;
        this.results       = results;
    }

    public String getTitle() {
        return title;
    }

    public String getSummary() {
        return summary;
    }

    public String getPreconditions() {
        return preconditions;
    }

    public ArrayList<String> getSteps() {
        return steps;
    }

    public ArrayList<String> getResults() {
        return results;
    }

}

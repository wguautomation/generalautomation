package RFCs.Deployed;

import BaseClasses.LoginType;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

public class Rfc5837 extends PingSession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        try {
            load("coachingreport", LoginType.LANE2_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5837::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        try {
            load("coachingreport", LoginType.LANE1_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5837::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        try {
            load("coachingreport", LoginType.PRODUCTION_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5837::PingProdTest() failed");
        }
    }

    private void checkEndpointKeys() {
        getMappings().verifyNotTagged("/v3/has/clearCache");
        getMappings().verifyNotTagged("/v3/has/pidm/{pidm}/courseVersionId/{courseVersionId}/assessmentid/{assessmentId}");
        getMappings().verifyNotTagged("/v3/has/pidm/{pidm}/courseVersionId/{courseVersionId}");
        getMappings().verifyNotTagged("/v3/isInV3ByProgram/pidm/{pidm}");
        getMappings().verifyNotTagged("/v3/pidm/{pidm}/assessmentid/{assessmentId}/studyplanid/{studyPlanId}/studyplantype/{studyPlanType}");
        getMappings().verifyNotTagged("/v3/pidm/{pidm}/assessmentid/{assessmentId}/studyplanid/{studyPlanId}");
        getMappings().verifyNotTagged("/v3/pidm/{pidm}/assessmentid/{assessmentId}/studyplanid/{studyPlanId}/studyplantype/{studyPlanType}/byDate");
        getMappings().verifyNotTagged("/v3/pidm/{pidm}/assessmentid/{assessmentId}/studyplanid/{studyPlanId}/byDate");
        getMappings().verifyNotTagged("/v3/pidm/{pidm}/assessmentid/{assessmentId}/cv/{courseVersionId}");
        getMappings().verifyNotTagged("/v3/pidm/{pidm}/assessmentid/{assessmentId}/cv/{courseVersionId}/byDate");
        getMappings().verifyNotTagged("/v3/has/pidm/{pidm}/assessmentid/{assessmentId}");
        getMappings().verifyNotTagged("/v2/pidm/{pidm}/assessmentid/{assessmentId}/studyplanid/{studyPlanId}");
        General.Debug("");
    }

    // Note - these are not testing real values / getting returns, just verifying that the Ping permissions let the call go through
    private void exerciseEndpoints() {
        String result;

        getLogin().addMacroAndValue("{studyPlanId}", "15");
        getLogin().addMacroAndValue("{assessmentId}", "12");
        getLogin().addMacroAndValue("{studyPlanType}", "");
        getLogin().addMacroAndValue("{courseVersionId}", "14");

        result = GetEndpoint("/v3/has/pidm/{pidm}/courseVersionId/{courseVersionId}");
        assert(result.toLowerCase().contains("false") || result.toLowerCase().contains("true"));

        result = GetEndpoint("/v3/pidm/{pidm}/assessmentid/{assessmentId}/studyplanid/{studyPlanId}");

        result = GetEndpoint("/v3/has/pidm/{pidm}/assessmentid/{assessmentId}");

        result = GetEndpoint("/v3/pidm/{pidm}/assessmentid/{assessmentId}/cv/{courseVersionId}");

        result = GetEndpoint("/v3/pidm/{pidm}/assessmentid/{assessmentId}/cv/{courseVersionId}/byDate");

//        result = Get("/v3/clearCache");
//        result = Get("/v3/has/clearCache");
        result = GetEndpoint("/v3/isInV3ByProgram/pidm/{pidm}");
        assert(result.toLowerCase().contains("false") || result.toLowerCase().contains("true"));
    }
}

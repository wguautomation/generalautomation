package RFCs.Deployed;

import BaseClasses.LoginType;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 7/20/2016.
 */

public class Rfc5827 extends PingSession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        try {
            load("programprogress", LoginType.LANE2_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5827::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        try {
            load("programprogress", LoginType.LANE1_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5827::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        try {
            load("programprogress", LoginType.PROD_SPOLTUS, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5827::PingProdTest() failed");
        }
    }

    private void checkEndpointKeys() {
        getMappings().verifyNotTagged("/v1/api/ProgramProgress/{pidm}");
        getMappings().verifyNotTagged("/v1/api/ProgramProgress/{pidm}/calculated");
        General.Debug("");
    }

    private void exerciseEndpoints() {
        String result;

        result = GetEndpoint("/v1/api/ProgramProgress/{pidm}");
        General.Debug(result);

        result = GetEndpoint("/v1/api/ProgramProgress/{pidm}/calculated");
        General.Debug(result);

    }

}

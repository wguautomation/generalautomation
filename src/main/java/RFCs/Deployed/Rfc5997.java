package RFCs.Deployed;

import BaseClasses.LoginType;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

public class Rfc5997 extends PingSession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        try {
            load("student-engagement", LoginType.LANE2_SPOLTUS, ServiceKey.MAPPINGS);
            getLogin().addMacroAndValue("{pidm}", LoginType.LANE2_NORMAL.getPidm());
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5997::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        try {
            load("student-engagement", LoginType.LANE1_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5997::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        try {
            load("student-engagement", LoginType.PRODUCTION_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5997::PingProdTest() failed");
        }
    }

    private void checkEndpointKeys() {
        getMappings().verifyGet("/v1/status/student/{pidm}/startedCourses");
        getMappings().verifyGet("/v1/status/student/{pidm}/course/{courseVersionID}");
        getMappings().verifyGet("/v2/status/student/{pidm}/startedCourses");
        getMappings().verifyGet("/v2/status/student/{pidm}/course/{courseVersionID}");

        getMappings().verifyPut("/v1/status/student/{pidm}/reset/{statusId}");
        getMappings().verifyPut("/v1/status/student/{pidm}/course/{courseVersionID}");
        getMappings().verifyPut("/v2/status/student/{pidm}/reset/{statusId}");
        getMappings().verifyPut("/v2/status/student/{pidm}/course/{courseVersionId}");

        getMappings().verifyPost("/v1/status/student/{pidm}/course/{courseVersionId}");
        getMappings().verifyPost("/v2/status/student/{pidm}/course/{courseVersionId}");
        getMappings().verifyPost("/v2/status/student/{pidm}/course/{courseVersionId}/mentorOverride");

        getMappings().verifyNotTagged("/v0/status/student/{pidm}/course/{courseVersionID}");
        General.Debug("");
    }

    private void exerciseEndpoints() {
        String result;

        String courseVersionId = "7520019";
        getLogin().addMacroAndValue("{courseVersionId}", courseVersionId);
        getLogin().addMacroAndValue("{courseVersionID}", courseVersionId);
        https://student-engagement.qa.wgu.edu/v2/status/student/106679/startedCourses"
        result = GetEndpoint("/v2/status/student/{pidm}/startedCourses");
        General.Debug(result);

        result = GetEndpoint("/v1/status/student/{pidm}/course/{courseVersionID}");
        General.Debug(result);

        result = GetEndpoint("/v2/status/student/{pidm}/course/{courseVersionID}");
        General.Debug(result);

    }
}
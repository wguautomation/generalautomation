package RFCs.Deployed;

import BaseClasses.LoginType;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 7/28/2016.
 */

public class Rfc5844 extends PingSession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        try {
            load("studentinfo", LoginType.LANE2_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints(LoginType.LANE2_NORMAL);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5844::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        try {
            load("studentinfo", LoginType.LANE1_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints(LoginType.LANE1_NORMAL);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5844::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        try {
            load("studentinfo", LoginType.PRODUCTION_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints(LoginType.PRODUCTION_NORMAL);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5844::PingProdTest() failed");
        }
    }

    private void checkEndpointKeys() {
//        mappings.verifyPost("/feedback");
        General.Debug("");
    }

    private void exerciseEndpoints(LoginType login) {
        String result;

/*
Example post data:
{
 "userName" : "MSAI",
 "feedbackType" : "problem",
 "comment" : "Good, this is my comment.",
 "deviceType" : "Android"
}
Example response (media-type: application/json):
{"id":"500e0000005o3znAAA","success":true,"errors":[]}
 */

//        result = Post(basePingService.getServer(login) + "/feedback", login, postdatahere);
//        General.Debug(result);

    }
}

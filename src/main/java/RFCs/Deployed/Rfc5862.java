package RFCs.Deployed;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import ServiceClasses.MentorService;
import Utils.General;
import org.testng.TestNGException;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 7/28/2016.
 */

public class Rfc5862 extends MentorService {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        LoginType login = LoginType.LANE2_NORMAL;

        try {
            load(login, AuthType.PING);
            CheckEndpointUrls(login, AuthType.PING);
            ExerciseEndpoints(login, AuthType.PING);
            ExerciseEndpoints(login, AuthType.MASHERY);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5862::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        LoginType login = LoginType.LANE1_NORMAL;

        try {
            load(login, AuthType.PING);
            CheckEndpointUrls(login, AuthType.PING);
            ExerciseEndpoints(login, AuthType.PING);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5862::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        LoginType login = LoginType.PRODUCTION_NORMAL;

        try {
            load(login, AuthType.PING);
            CheckEndpointUrls(login, AuthType.PING);
            ExerciseEndpoints(login, AuthType.PING);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5862::PingProdTest() failed");
        }
    }


}

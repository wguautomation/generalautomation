package RFCs.Deployed;

import BaseClasses.LoginType;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 7/28/2016.
 */

public class Rfc5859 extends PingSession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        try {
            load("mobiletoken", LoginType.LANE2_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5859::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        try {
            load("mobiletoken", LoginType.LANE1_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5859::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        try {
            load("mobiletoken", LoginType.PRODUCTION_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5859::PingProdTest() failed");
        }
    }

    private void checkEndpointKeys() {
//        mappings.verifyGet("/v0/cohorts/course/{courseVersionId}/{pidm}");

//        mappings.verifyPut("/v1/pidm/{pidm}/courses/{courseVersionId}");

//        mappings.verifyPost("/v0/cohorts/enroll/{pidm}/{offeringId}/{cohortId}");

//        mappings.verifyDelete("/v1/pidm/{pidm}/courses/{courseVersionId}/enrollments/{enrollmentId}");
        General.Debug("");
    }

    private void exerciseEndpoints() {
        String result;

        getLogin().addMacroAndValue("{bannerid}", "QA0000007");

        result = GetEndpoint("/mobiletoken/bannerid/{bannerid}");
        General.Debug(result);

    }
}

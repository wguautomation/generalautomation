package RFCs.Deployed;

import BaseClasses.LoginType;
import JsonDataClasses.JsonFaqArray;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 7/19/2016.
 */

@Test(priority = 0)
public class Rfc5864 extends PingSession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        try {
            load("wguinformation", LoginType.LANE2_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5864::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        try {
            load("wguinformation", LoginType.LANE1_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5864::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        try {
            load("wguinformation", LoginType.PRODUCTION_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5864::PingProdTest() failed");
        }
    }

    private void checkEndpointKeys() {
        getMappings().verifyGet("/content");
        getMappings().verifyGet("/content/{type}");
        getMappings().verifyGet("/contactInformation");
        getMappings().verifyGet("/contactInformation/lastUpdated");
        getMappings().verifyGet("/resources");
        getMappings().verifyGet("/resources/lastUpdated");
        getMappings().verifyGet("/faqs/lastUpdated");
        getMappings().verifyGet("/faqs");

        getMappings().verifyPost("/content");

        getMappings().verifyDelete("/content/{type}");
        General.Debug("");
    }

    private void exerciseEndpoints() {
        String result;

        result = GetEndpoint("/content");
//        JsonFaqArray faqs = new JsonFaqArray(result);
        General.Debug(result);

        result = GetEndpoint("/content/contactInformation");
        General.Debug(result);

        result = GetEndpoint("/content/contact_info");
        General.Debug(result);

        result = GetEndpoint("/content/contact_info_test");
        General.Debug(result);

        result = GetEndpoint("/content/faq_test");
        General.Debug(result);

        result = GetEndpoint("/content/faqs");
        JsonFaqArray faqs = new JsonFaqArray(result);
        General.Debug(faqs.getFaq(0).getQuestion());

        result = GetEndpoint("/content/fetoolbox");
        General.Debug(result);

        result = GetEndpoint("/content/resources");
        General.Debug(result);

        result = GetEndpoint("/content/resources_test");
        General.Debug(result);

        result = GetEndpoint("/content/services");
        General.Debug(result);

        result = GetEndpoint("/content/services_test");
        General.Debug(result);

        result = GetEndpoint("/content/studentsupport");
        General.Debug(result);

        result = GetEndpoint("/content/test");
        General.Debug(result);

        result = GetEndpoint("/contactInformation/lastUpdated");
        General.Debug(result);

        result = GetEndpoint("/resources");
        General.Debug(result);

        result = GetEndpoint("/resources/lastUpdated");
        General.Debug(result);

        result = GetEndpoint("/faqs");
        General.Debug(result);


    }
}

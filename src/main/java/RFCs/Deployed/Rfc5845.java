package RFCs.Deployed;

import BaseClasses.LoginType;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

public class Rfc5845 extends PingSession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        try {
            load("wsbsi", LoginType.LANE2_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints(LoginType.LANE2_NORMAL);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5845::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        try {
            load("wsbsi", LoginType.LANE1_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints(LoginType.LANE1_NORMAL);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5845::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        try {
            load("wsbsi", LoginType.PRODUCTION_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints(LoginType.PRODUCTION_NORMAL);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5845::PingProdTest() failed");
        }
    }

    private void checkEndpointKeys() {
        getMappings().verifyGet("/sites/{socialSiteId}");
        getMappings().verifyGet("/sites/sponsored");
        getMappings().verifyGet("/sites/lastUpdated");
        getMappings().verifyGet("/sites/default");
        getMappings().verifyGet("/sites");
        getMappings().verifyGet("/students/{pidm}/connections");
        getMappings().verifyGet("/connections/{connectionId}/sites");
        getMappings().verifyGet("/students/{pidm}/connections/recommended");

        getMappings().verifyPut("/sites/{socialSiteId}"); // not currently supported

        getMappings().verifyPost("/students/{pidm}/connections/sites/{socialSiteId}");
        getMappings().verifyPost("/students/{pidm}/connections");

        getMappings().verifyDelete("/connections/{connectionId}");
        getMappings().verifyDelete("/students/{pidm}/connections/sites/{socialSiteId}");

        General.Debug("");
    }

    private void exerciseEndpoints(LoginType login) {
        String result;

        getLogin().addMacroAndValue("{socialSiteId}", "39");
        getLogin().addMacroAndValue("{connectionId}", "2027");

        result = GetEndpoint("/students/{pidm}/connections");
        General.Debug(result);

        result = Post(getServer(login) + "/students/{pidm}/connections/sites/{socialSiteId}", login,
                "{\"id\":300,\"pidm\":106679,\"socialSite\":{\"id\":1,\"url\":\"http://www.facebook.com/wgu.edu\",\"description\":\"Western Governors University\",\"platform\":{\"id\":1,\"description\":\"Facebook\",\"code\":\"FB\"},\"avgScore\":1},\"rating\":null,\"socialSiteState\":{\"id\":6,\"state\":\"National\",\"state_abbreviation\":\"  \"}}");
        String connectionId = verifyKey(result, "id");
        General.Debug(result);

        result = Delete(getServer(login) + "/connections/" + connectionId, login);
 //       assert(result.toLowerCase().contains("success"));

        result = Delete(getServer(login) + "/students/{pidm}/connections/sites/99999", login);
        assert(result.toLowerCase().contains("success"));

        result = GetEndpoint("/sites/{socialSiteId}");
        General.Debug(result);

        result = GetEndpoint("/sites/sponsored");
        General.Debug(result);

        result = GetEndpoint("/sites/lastUpdated");
        General.Debug(result);

        result = GetEndpoint("/sites/default");
        General.Debug(result);

        result = GetEndpoint("/sites");
        General.Debug(result);

        result = GetEndpoint("/students/{pidm}/connections");
        General.Debug(result);

        result = GetEndpoint("/connections/{connectionId}/sites");
        General.Debug(result);

        result = GetEndpoint("/students/{pidm}/connections/recommended");
        General.Debug(result);

    }
}

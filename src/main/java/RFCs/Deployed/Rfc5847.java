package RFCs.Deployed;

import BaseClasses.LoginType;
import JsonDataClasses.*;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 7/19/2016.
 */

public class Rfc5847 extends PingSession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        try {
            load("degreeplan", LoginType.LANE2_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5847::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        try {
            load("degreeplan", LoginType.LANE1_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5847::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        try {
            load("degreeplan", LoginType.PRODUCTION_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5847::PingProdTest() failed");
        }
    }

    private void checkEndpointKeys() {
        getMappings().verifyNotTagged("/v0/api/degreeplans/current/pidm/{pidm}");
        getMappings().verifyNotTagged("/v1/api/degreeplans/current/pidm/{pidm}");
        getMappings().verifyNotTagged("/admin/api/queues");
        General.Debug("");
    }

    private void exerciseEndpoints() {
        String result;

        result = GetEndpoint("/v0/api/degreeplans/current/pidm/{pidm}");
        JsonDegreePlan degreePlan = new JsonDegreePlan(result);
        General.Debug(degreePlan.getName());

        result = GetEndpoint("/v1/api/degreeplans/current/pidm/{pidm}");
        degreePlan = new JsonDegreePlan(result);
        General.Debug(degreePlan.getName());
    }
}

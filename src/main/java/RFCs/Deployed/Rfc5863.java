package RFCs.Deployed;

import BaseClasses.LoginType;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

public class Rfc5863 extends PingSession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        try {
            load("newsitems", LoginType.LANE2_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5863::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        try {
            load("newsitems", LoginType.LANE1_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5863::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        try {
            load("newsitems", LoginType.PRODUCTION_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5863::PingProdTest() failed");
        }
    }

    private void checkEndpointKeys() {
        getMappings().verifyNotTagged("/v0/api/feed/{pidm}/{timestamp}");
        getMappings().verifyNotTagged("/v1/api/feed/{pidm}/{timestamp}");
        General.Debug("");
    }

    private void exerciseEndpoints() {
        String result;
        getLogin().addMacroAndValue("{timestamp}", "1417000000");

        result = GetEndpoint("/v0/api/feed/{pidm}/{timestamp}");
        General.Debug(result);

        result = GetEndpoint("/v1/api/feed/{pidm}/{timestamp}");
        General.Debug(result);

    }
}
package RFCs.Deployed;

import BaseClasses.LoginType;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 7/28/2016.
 */

public class Rfc5833 extends PingSession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        try {
            load("coursecommunities", LoginType.LANE2_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5833::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        try {
            load("coursecommunities", LoginType.LANE1_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5833::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        try {
            load("coursecommunities", LoginType.PRODUCTION_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5833::PingProdTest() failed");
        }
    }

    private void checkEndpointKeys() {
        getMappings().verifyGet("/v2/courseChatterFeed/{courseCode}/student/{username}");
        getMappings().verifyGet("/v1/courseChatterFeed/{courseCode}/student/{username}");
        getMappings().verifyGet("/v1/tipsAndAnnouncements/{courseCode}/student/{username}");
        getMappings().verifyGet("/v1/getUserPhoto/{userId}");
        getMappings().verifyGet("/v0/courseChatterFeed/{courseCode}/student/{username}");
        getMappings().verifyGet("/v0/tipsAndAnnouncements/{courseCode}/student/{username}");
        getMappings().verifyGet("/v0/getUserPhoto/{userId}");

        getMappings().verifyPut("/v1/markMessageAsViewedBatch");
        getMappings().verifyPut("/v1/markMessageAsViewed/{id}");
        getMappings().verifyPut("/v0/markMessageAsViewed/{id}");

        General.Debug("");
    }

    // Because this hits Salesforce pretty hard, and we have horrible limits there, we haave
    //   to use the Sleep calls below. If the calls all happen back to back, it will hang. With
    //   no error message, just hang.
    private void exerciseEndpoints() {
        String result;

        result = GetEndpoint("/v2/courseChatterFeed/{courseCode}/student/{username}");
        General.Debug(result);
        General.Sleep(1000);

        result = GetEndpoint("/v1/courseChatterFeed/{courseCode}/student/{username}");
        General.Debug(result);
        General.Sleep(1000);

        result = GetEndpoint("/v1/tipsAndAnnouncements/{courseCode}/student/{username}");
        General.Debug(result);
        General.Sleep(1000);

//        result = GetEndpoint("/v1/getUserPhoto/{userId}");
//        General.Debug(result);

        result = GetEndpoint("/v0/courseChatterFeed/{courseCode}/student/{username}");
        General.Debug(result);
        General.Sleep(1000);

//        result = GetEndpoint("/v0/tipsAndAnnouncements/{courseCode}/student/{username}");
//        General.Debug(result);

    }
}

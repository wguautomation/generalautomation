package RFCs.Deployed;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonIdentity;
import JsonDataClasses.JsonPerson;
import ServiceClasses.PersonService;
import Utils.General;
import org.testng.TestNGException;
import org.testng.annotations.Test;

public class Rfc5841 extends PersonService {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        LoginType login = LoginType.LANE2_NORMAL;

        try {
            load(login, AuthType.PING);
            CheckEndpointUrls(login, AuthType.PING);
            exerciseEndpoints(login);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5841::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        LoginType login = LoginType.LANE1_NORMAL;

        try {
            load(login, AuthType.PING);
            CheckEndpointUrls(login, AuthType.PING);
            exerciseEndpoints(login);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5841::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        LoginType login = LoginType.PRODUCTION_NORMAL;

        try {
            load(login, AuthType.PING);
            CheckEndpointUrls(login, AuthType.PING);
            exerciseEndpoints(login);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5841::PingProdTest() failed");
        }
    }

    public void exerciseEndpoints(LoginType login) {
        String result;

        result = ping.GetEndpoint("/v1/person");
        JsonPerson person = new JsonPerson(result);

        login.addMacroAndValue("{bannerId}", person.getStudentId());
        result = ping.GetEndpoint("/v1/person/pidm/{pidm}");
        person = new JsonPerson(result);
        String bannerId = person.getStudentId();

        result = ping.GetEndpoint("/v1/person/pidm/{pidm}/identity");
        JsonIdentity identity = new JsonIdentity(result);

        result = ping.GetEndpoint("/v1/person/pidm/{pidm}/type");
        assert(!result.isEmpty());

        result = ping.GetEndpoint("/v1/person/pidm/{pidm}/refresh");
        assert(result == null);

//        result = ping.GetEndpoint("/v1/person/bannerId/");
//        result = ping.GetEndpoint("/v1/person/bannerId/");
//        result = ping.GetEndpoint("/v1/person/bannerId/");

        result = ping.GetEndpoint("/v1/person/{username}");
        person = new JsonPerson(result);

        result = ping.GetEndpoint("/v1/person/{username}/type");
        assert(!result.isEmpty());

        result = ping.GetEndpoint("/v1/person/{username}/identity");
        identity = new JsonIdentity(result);

//        result = ping.GetEndpoint("/v1/person/{username}/roles");
//        result = ping.GetEndpoint("/v1/person/{username}/roles/refresh");
        result = ping.GetEndpoint("/v1/person/username/{username}/refresh");
        assert(result == null);
    }
}

package RFCs.Deployed;

import BaseClasses.LoginType;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

public class Rfc5843 extends PingSession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        try {
            load("taskstreamlti", LoginType.LANE2_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
//            load("taskstreamlti", LoginType.LANE2_TASKSTREAM, true);
//            exerciseSecondaryEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5843::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        try {
            load("taskstreamlti", LoginType.LANE1_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
//            load("taskstreamlti", LoginType.LANE1_TASKSTREAM, true);
//            exerciseSecondaryEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5843::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        try {
            load("taskstreamlti", LoginType.PRODUCTION_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
//            load("taskstreamlti", LoginType.PROD_TASKSTREAM, true);
//            exerciseSecondaryEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5843::PingProdTest() failed");
        }
    }

    private void checkEndpointKeys() {

        getMappings().verifyNotTagged("/v0/api/taskstream/lti/students/{pidm}/courses/{courseCode}");
        getMappings().verifyNotTagged("/v0/api/taskstream/jobs/createaccounts/{daysBack}/{maxBatch}");
        getMappings().verifyNotTagged("/v0/api/taskstream/jobs/autoenroll/{daysBack}/{maxBatch}");
        getMappings().verifyNotTagged("/v0/api/taskstream/task-stream-service/autoenroll/{daysBack}/{maxBatch}/processAutoEnrollmentErrors.*");
        getMappings().verifyNotTagged("/v0/api/taskstream/jobs/autoreferrals");

        getMappings().verifyNotTagged("/v1/api/taskstream/lti/students/{pidm}/courses/{courseCode}");
        getMappings().verifyNotTagged("/v1/api/taskstream/lti/students/{pidm}/courses/{courseCode}/score");
        General.Debug("");
    }

    private void exerciseEndpoints() {
        String result;

        result = GetEndpoint("/v0/api/taskstream/lti/students/{pidm}/courses/{courseCode}");
        General.Debug(result);

        result = GetEndpoint("/v1/api/taskstream/lti/students/{pidm}/courses/{courseCode}");
        General.Debug(result);

        result = GetEndpoint("/v1/api/taskstream/lti/students/{pidm}/courses/{courseCode}/score");
        General.Debug(result);

        result = GetEndpoint("/v0/api/taskstream/jobs/createaccounts/7/7");
        General.Debug(result);
    }

    private void exerciseSecondaryEndpoints() {
        String result;

        result = GetEndpoint("/v0/api/taskstream/jobs/createaccounts/1/1");
        General.Debug(result);

        result = GetEndpoint("/v0/api/taskstream/jobs/autoenroll/1/1");
        General.Debug(result);

        result = GetEndpoint("/v0/api/taskstream/task-stream-service/autoenroll/1/1/processAutoEnrollmentErrors.*");
        General.Debug(result);

        result = GetEndpoint("/v0/api/taskstream/jobs/autoreferrals");
        General.Debug(result);
    }
}
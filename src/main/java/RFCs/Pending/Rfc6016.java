package RFCs.Pending;

import BaseClasses.LoginType;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 7/21/2016.
 */

public class Rfc6016 extends PingSession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        try {
            load("cosbservices", LoginType.LANE2_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6016::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        try {
            load("cosbservices", LoginType.LANE1_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6016::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        try {
            load("cosbservices", LoginType.PROD_SPOLTUS, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6016::PingProdTest() failed");
        }
    }

    private void checkEndpointKeys() {
        getMappings().verifyGet("/v1/courses/info/{courseVersionId}");
        getMappings().verifyGet("/v1/courses/{courseVersionId}");
        getMappings().verifyGet("/v2/courses/{courseVersionId}");

        General.Debug("");
    }

    private void exerciseEndpoints() {
        String result;

        getLogin().addMacroAndValue("{courseVersionID}", "1392");
        getLogin().addMacroAndValue("{courseVersionId}", "1392");

        result = GetEndpoint("/v1/courses/info/{courseVersionId}");
        General.Debug(result);
        result = GetEndpoint("/v1/courses/{courseVersionId}");
        General.Debug(result);
        result = GetEndpoint("/v2/courses/{courseVersionId}");
        General.Debug(result);

        getLogin().addMacroAndValue("{courseVersionID}", "3700001");

        result = GetEndpoint("/v1/courses/info/{courseVersionId}");
        General.Debug(result);
        result = GetEndpoint("/v1/courses/{courseVersionId}");
        General.Debug(result);
        result = GetEndpoint("/v2/courses/{courseVersionId}");
        General.Debug(result);
    }
}

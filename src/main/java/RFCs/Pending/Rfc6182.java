package RFCs.Pending;

import BaseClasses.LoginType;
import Utils.General;
import Utils.MasherySession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

public class Rfc6182 extends MasherySession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        try {
            String result = Get("https://api.dev.wgu.edu/assessments/v1/students/106679/courses/1332/assessment/1/assess", LoginType.LANE2_NORMAL);
            General.Debug(result);

            result = Get("https://api.dev.wgu.edu/assessments/v1/V1/healthCheck", LoginType.LANE2_NORMAL);
            General.Debug(result);

            result = Get("https://api.dev.wgu.edu/assessments/v1/assessments/1/competencies", LoginType.LANE2_NORMAL);
            General.Debug(result);

            result = Get("https://api.dev.wgu.edu/assessments/v1/students/106679/courses/1332", LoginType.LANE2_NORMAL);
            General.Debug(result);

            result = Get("https://api.dev.wgu.edu/assessments/v1/students/106679/courses/1332/assessments", LoginType.LANE2_NORMAL);
            General.Debug(result);

            result = Get("https://api.dev.wgu.edu/assessments/v1/courses/1332", LoginType.LANE2_NORMAL);
            General.Debug(result);

            result = Get("https://api.dev.wgu.edu/assessments/v1/v0/students/106679/courses/1332", LoginType.LANE2_NORMAL);
            checkEndpointKeys();

            try {
                exerciseEndpoints();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6182::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        try {
//            load("assessments", LoginType.LANE1_NORMAL, true);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6182::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        try {
//            load("assessments", LoginType.PRODUCTION_NORMAL, true);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6182::PingProdTest() failed");
        }
    }

    private void checkEndpointKeys() {
//        mappings.verifyGet("/v1/person");


        General.Debug("");
    }

    private void exerciseEndpoints() {

    }
}

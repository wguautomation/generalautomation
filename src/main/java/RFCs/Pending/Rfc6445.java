package RFCs.Pending;

import BaseClasses.LoginType;
import JsonDataClasses.JsonCourseNoteArray;
import JsonDataClasses.JsonLinkRatingArray;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 8/2/2016.
 */

public class Rfc6445 extends PingSession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        try {
            load("studyplan", LoginType.LANE2_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6445::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        try {
            load("studyplan", LoginType.LANE1_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6445::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        try {
            load("studyplan", LoginType.PROD_SPOLTUS, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6445::PingProdTest() failed");
        }
    }

    private void checkEndpointKeys() {
        getMappings().verifyGet("/v0/api/courses/notes");
        getMappings().verifyGet("/api/courses/{courseCode}");
        getMappings().verifyGet("/api/courses/lastpublished/{courseVersionId}");
        getMappings().verifyGet("/api/courses/getPublishedStudyPlans");
        getMappings().verifyGet("/api/courses/{courseCode}/{courseVersionId}");
        getMappings().verifyGet("/api/courses/courseVersionId/pidm/{pidm}");
        getMappings().verifyGet("/api/courses/notes/{username}/{courseVersionId}");
        getMappings().verifyGet("/api/students/{pidm}/courses/ora1");
        getMappings().verifyGet("/bookmark/{studyPlanId}/{username}");
        getMappings().verifyGet("/mobileLinks/linkRatings/{courseVersionId}");
        getMappings().verifyGet("/useractivity/{username}/{studyplanId}");

        getMappings().verifyPut("/api/courses/notes/{username}/userNotes/{userNotesId}");

        getMappings().verifyPost("/api/courses/notes/{username}/studyPlan/{studyPlanId}/topics/{topicId}");
        getMappings().verifyPost("/useractivity/{username}/{studyplanId}/{userActivityType}/{isCompleted}/{isSkipped}/{subjectId}/{topicId}/{activityId}/{userActivityId}");

        getMappings().verifyDelete("/api/courses/notes/{username}/userNotes/{userNotesId}/{courseVersionId}");

        getMappings().verifyNotTagged("/admin/api/queues");

        General.Debug("");
    }

    private void exerciseEndpoints() {
        String result;

        result = GetEndpoint("/v0/api/courses/notes");
        JsonCourseNoteArray noteArray = new JsonCourseNoteArray(result);
        General.Debug("Found " + noteArray.size() + " notes");

        getLogin().addMacroAndValue("{courseVersionId}", noteArray.getNote(0).getCourseVersionId());
        getLogin().addMacroAndValue("{studyPlanId}", noteArray.getNote(0).getStudyPlanId());
        getLogin().addMacroAndValue("{studyplanId}", noteArray.getNote(0).getStudyPlanId());

        result = GetEndpoint("/api/courses/getPublishedStudyPlans");
        General.Debug(result);

        result = GetEndpoint("/api/students/{pidm}/courses/ora1");
        General.Debug(result);

        result = GetEndpoint("/api/courses/courseVersionId/pidm/{pidm}");
        General.Debug(result);

        result = GetEndpoint("/api/courses/notes/{username}/{courseVersionId}");
        General.Debug(result);

        result = GetEndpoint("/bookmark/{studyPlanId}/{username}");
        General.Debug(result);

        result = GetEndpoint("/mobileLinks/linkRatings/{courseVersionId}");
        JsonLinkRatingArray linkArray = new JsonLinkRatingArray(result);
        General.Debug("Found " + linkArray.size() + " link rating entries");

        result = GetEndpoint("/useractivity/{username}/{studyplanId}");
        General.Debug(result);

    }
}

package RFCs.Pending;

/*
 * Created by timothy.hallbeck on 8/2/2016.
 */

import BaseClasses.LoginType;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

public class Rfc6511 extends PingSession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        try {
            String result = Get("https://web3.dev.wgu.edu/FldExpLink.asp", LoginType.LANE2_SPOLTUS);
            assert (result.contains("something unique here"));
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6511::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        try {
            String result = Get("https://web3.qa.wgu.edu/FldExpLink.asp", LoginType.LANE1_SPOLTUS);
            assert (result.contains("something unique here"));
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6511::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        try {
            String result = Get("https://web3.wgu.edu/FldExpLink.asp", LoginType.PROD_SPOLTUS);
            assert (result.contains("something unique here"));
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6511::PingProdTest() failed");
        }
    }

}

package RFCs.Pending;

/*
 * Created by timothy.hallbeck on 8/2/2016.
 */

import BaseClasses.LoginType;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

public class Rfc6487 extends PingSession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        try {
            String result = Get("http://web3.dev.wgu.edu/wguidcard/luminisstartup.aspx", LoginType.LANE2_NORMAL);
            assert (result.contains("/wguidcard/luminisstartup.aspx"));
            General.Debug(result);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6487::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        try {
            String result = Get("http://web3.qa.wgu.edu/wguidcard/luminisstartup.aspx", LoginType.LANE1_NORMAL);
            assert (result.contains("/wguidcard/luminisstartup.aspx"));
            General.Debug(result);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6487::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        try {
            String result = Get("https://web3.wgu.edu/ResourceTools/Default.aspx", LoginType.PROD_SPOLTUS);
            assert (result.contains("/wguidcard/luminisstartup.aspx"));
            General.Debug(result);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6487::PingProdTest() failed");
        }
    }

}

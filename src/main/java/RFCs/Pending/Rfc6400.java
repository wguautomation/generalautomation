package RFCs.Pending;

import BaseClasses.LoginType;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 8/1/2016.
 */

public class Rfc6400 extends PingSession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        try {
            String result = Get("https://web5.dev.wgu.edu/ASP3/aap/Pending_referrals.asp", LoginType.LANE2_SPOLTUS);
            assert (result.contains("something unique here"));
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6400::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        try {
            String result = Get("https://access.qa.wgu.edu/ASP3/aap/Pending_referrals.asp", LoginType.LANE1_SPOLTUS);
//            String result = Get("https://web5.dev.wgu.edu/ASP3/aap/Pending_referrals.asp", LoginType.LANE1_SPOLTUS);
            assert (result.contains("something unique here"));
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6400::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        try {
            String result = Get("https://web5.dev.wgu.edu/ASP3/aap/Pending_referrals.asp", LoginType.PROD_SPOLTUS);
            assert (result.contains("something unique here"));
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6400::PingProdTest() failed");
        }
    }

}

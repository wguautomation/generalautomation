package RFCs.Pending;

/*
 * Created by timothy.hallbeck on 8/8/2016.
 */

import BaseClasses.LoginType;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

public class Rfc6474 extends PingSession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        try {
            String result = Get("", LoginType.LANE2_NORMAL);
            assert (result.contains(""));
            General.Debug(result);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6474::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        try {
            String result = Get("", LoginType.LANE1_NORMAL);
            assert (result.contains(""));
            General.Debug(result);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6474::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        try {
            String result = Get("", LoginType.PROD_SPOLTUS);
            assert (result.contains(""));
            General.Debug(result);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6474::PingProdTest() failed");
        }
    }

}

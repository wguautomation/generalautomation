package RFCs.Pending;

import BaseClasses.LoginType;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 7/24/2016.
 */

public class Rfc6329 extends PingSession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {

        LoginType login = LoginType.LANE2_NORMAL;
        String result;

        try {
            result = Post("http://authtokenvalid.dev.wgu.edu/api/v1/ping/token", login, null, true);
            verifyKey(result, "wguUUID");
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5827::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {

        LoginType login = LoginType.LANE1_NORMAL;
        String result;

        try {
            result = Post("https://authtokenvalid.qa.wgu.edu/api/v1/ping/token", login, null, true);
            verifyKey(result, "wguUUID");
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5827::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {

        LoginType login = LoginType.PROD_SPOLTUS;
        String result;

        try {
            result = Post("http://authtokenvalid.wgu.edu/api/v1/ping/token", login, null, true);
            verifyKey(result, "wguUUID");
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5827::PingProdTest() failed");
        }
    }

}
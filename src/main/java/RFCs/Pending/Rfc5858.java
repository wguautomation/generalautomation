package RFCs.Pending;

import BaseClasses.LoginType;
import JsonDataClasses.JsonCohortArray;
import JsonDataClasses.JsonCohortsInfo;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

public class Rfc5858 extends PingSession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {
        try {
            load("cohorts", LoginType.LANE2_CPOINDEXTER, ServiceKey.MAPPINGS);
            getLogin().addMacroAndValue("{studentPidm}", LoginType.LANE2_CPOINDEXTER.getPidm());
            getLogin().addMacroAndValue("{mentorPidm}", LoginType.LANE2_SPOLTUS.getPidm());
            getLogin().addMacroAndValue("{courseVersionId}", "1332");
            checkEndpointKeys();
            exerciseEndpoints(LoginType.LANE2_CPOINDEXTER);

            ClearTokens();
            load("cohorts", LoginType.LANE2_SPOLTUS, null);
            getLogin().addMacroAndValue("{studentPidm}", LoginType.LANE2_CRAFUSE.getPidm());
            getLogin().addMacroAndValue("{mentorPidm}", LoginType.LANE2_SPOLTUS.getPidm());
            getLogin().addMacroAndValue("{courseVersionId}", "3960010");
            exerciseMentorEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5858::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {
        try {
            load("cohorts", LoginType.LANE1_FFURNO, ServiceKey.MAPPINGS);
            getLogin().addMacroAndValue("{studentPidm}", LoginType.LANE1_FFURNO.getPidm());
            getLogin().addMacroAndValue("{mentorPidm}", LoginType.LANE1_SPOLTUS.getPidm());
            getLogin().addMacroAndValue("{courseVersionId}", "3960006");
            checkEndpointKeys();
            exerciseEndpoints(LoginType.LANE1_FFURNO);

            ClearTokens();
            load("cohorts", LoginType.LANE1_MPOINDEXTER, null);
            getLogin().addMacroAndValue("{studentPidm}", LoginType.LANE1_MPOINDEXTER.getPidm());
            getLogin().addMacroAndValue("{mentorPidm}", LoginType.LANE1_SPOLTUS.getPidm());
            getLogin().addMacroAndValue("{courseVersionId}", "3960010");
            exerciseMentorEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5858::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {
        try {
            load("cohorts", LoginType.PRODUCTION_NORMAL, ServiceKey.MAPPINGS);
            checkEndpointKeys();
            exerciseEndpoints(LoginType.PRODUCTION_NORMAL);
            exerciseMentorEndpoints();
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc5858::PingProdTest() failed");
        }
    }

    private void checkEndpointKeys() {
        getMappings().verifyGet("/v0/cohorts/course/{courseVersionId}/{pidm}");
        getMappings().verifyGet("/v0/cohorts/course/{mentorPidm}/{studentPidm}/{courseVersionId}");
        getMappings().verifyGet("/v1/pidm/{pidm}/courses/{courseVersionId}");

        getMappings().verifyPut("/v1/pidm/{pidm}/courses/{courseVersionId}");

        getMappings().verifyPost("/v0/cohorts/enroll/{pidm}/{offeringId}/{cohortId}");
        getMappings().verifyPost("/v0/cohorts/enroll/{mentorPidm}/{studentPidm}/{offeringId}/{cohortId}");
        getMappings().verifyPost("/v0/cohorts/unenroll/{pidm}/{enrollmentId}");
        getMappings().verifyPost("/v0/cohorts/unenroll/{mentorPidm}/{studentPidm}/{enrollmentId}");
        getMappings().verifyPost("/v1/pidm/{pidm}/courses/{courseVersionId}");

        getMappings().verifyDelete("/v1/pidm/{pidm}/courses/{courseVersionId}/enrollments/{enrollmentId}");
        General.Debug("");
    }

    private void exerciseEndpoints(LoginType login) {
        String result;

        result = GetEndpoint("/v0/cohorts/course/{courseVersionId}/{studentPidm}");
        JsonCohortArray cohorts = new JsonCohortArray(result);

        result = GetEndpoint("/v1/pidm/{studentPidm}/courses/{courseVersionId}");
        JsonCohortsInfo cohortsInfo = new JsonCohortsInfo(result);

        String cohortId = "a2ya0000000eIrkAAE";
        String offeringId = "a2wa000000705N3AAI";
        String enrollmentid = "a2u1b0000004FUGAA2";

        getLogin().addMacroAndValue("{cohortId}", cohortId);
        getLogin().addMacroAndValue("{offeringId}", offeringId);
        getLogin().addMacroAndValue("{enrollmentId}", enrollmentid);

        result = Delete(getServer(login) + "/v1/pidm/{studentPidm}/courses/{courseVersionId}/enrollments/{enrollmentId}", login, "");

        result = Post(getServer(login) + "/v1/pidm/{studentPidm}/courses/{courseVersionId}", login, "{\"cohortid\":\"" + cohortId + "\",\"offeringid\":\"" + offeringId + "\",\"enrollmentid\":\"" + enrollmentid + "\"}");
        assert(result.contains("Success!"));
        General.Debug(result);

    }

    private void exerciseMentorEndpoints() {
        String result;

        result = GetEndpoint("/v0/cohorts/course/{mentorPidm}/{studentPidm}/{courseVersionId}");
        General.Debug(result);

        result = GetEndpoint("/v0/cohorts/course/{mentorPidm}/{studentPidm}/{courseVersionId}");
        General.Debug(result);

    }
}

package RFCs.Pending;

import BaseClasses.LoginType;
import Utils.General;
import Utils.PingSession;
import org.testng.TestNGException;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 7/25/2016.
 */

public class Rfc6008 extends PingSession {

    @Test(priority = 1)
    public void PingDevTest() throws Exception {

        LoginType login = LoginType.LANE2_NORMAL;
        String result;

        try {
            result = Get("https://webapp-l249a.wgu.edu:8080/pilot-admin", login);
            assert(result.contains("200 OK"));
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6016::PingDevTest() failed");
        }
    }

    @Test(priority = 2)
    public void PingQaTest() throws Exception {

        LoginType login = LoginType.LANE1_NORMAL;
        String result;

        try {
            result = Get("https://webapp-l109a.wgu.edu:8080/pilot-admin", login);
            assert(result.contains("200 OK"));
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6016::PingQaTest() failed");
        }
    }

    @Test(priority = 3, enabled = false)
    public void PingProdTest() throws Exception {

        LoginType login = LoginType.PROD_SPOLTUS;
        String result;

        try {
            result = Get("https://webapp-unka.wgu.edu:8080/pilot-admin", login);
            assert(result.contains("200 OK"));
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new TestNGException("Rfc6016::PingProdTest() failed");
        }
    }

}

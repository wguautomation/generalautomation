
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 11/3/2015.
 */

public class JsonCoursePamsAssessmentArray extends JsonData implements Iterable<JsonCoursePamsAssessment>, Iterator<JsonCoursePamsAssessment> {

    public JsonCoursePamsAssessmentArray(String jsonResult) {
        super(jsonResult, "pamsAssessments");
    }
    public JsonCoursePamsAssessmentArray(JsonObject object) {
        super(object, "pamsAssessments");
    }
    public JsonCoursePamsAssessmentArray(JsonArray array) {
        super(array);
    }

    public JsonCoursePamsAssessment getAssessment(int index) {
        return new JsonCoursePamsAssessment(jsonArray.get(index).getAsJsonObject());
    }

    public JsonCoursePamsAssessment getAssessmentById(String id) {
        JsonObject object = null;

        for (int index=0; index<jsonArray.size(); index++) {
            object = jsonArray.get(index).getAsJsonObject();
            if (object.get("id").toString().contains(id))
                break;
        }
        return new JsonCoursePamsAssessment(object);
    }

    public JsonCoursePamsAssessmentArray getAllPamsAssessments() {
        return new JsonCoursePamsAssessmentArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonCoursePamsAssessment resource : getAllPamsAssessments())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonCoursePamsAssessment next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonCoursePamsAssessment(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonCoursePamsAssessment> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}

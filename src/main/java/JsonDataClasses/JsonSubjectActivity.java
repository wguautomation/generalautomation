
package JsonDataClasses;

import Utils.wguStringUtils;
import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/10/2015.
 */

public class JsonSubjectActivity extends JsonData {

    public JsonSubjectActivity(String jsonResult) {
        super(jsonResult);
    }
    public JsonSubjectActivity(JsonObject object) {
        jsonObject = object;
    }

    public String getActivityId() {
        return getAsString("id");
    }

    public String getActivityTitle() {
        return getAsString("title");
    }

    public String getInstruction() {
        return getAsString("instruction");
    }

    public String getTopicId() {
        return getAsString("topicId");
    }

    public String getPosition() {
        return getAsString("position");
    }

    public void checkAllKeys() {
        getActivityId();
        getActivityTitle();
        getInstruction();
        getTopicId();
        getPosition();
    }

    public String prettyString(int indention) {
        String prettyString = "";

        prettyString += wguStringUtils.indent(indention) + "          Activity" + "\n";
        prettyString += wguStringUtils.indent(indention) + "         id : " + getActivityId()    + "\n";
        prettyString += wguStringUtils.indent(indention) + "      title : " + getActivityTitle() + "\n";
        prettyString += wguStringUtils.indent(indention) + "instruction : " + wguStringUtils.ellisize(getInstruction(), 90) + "\n";
        prettyString += wguStringUtils.indent(indention) + "    topicId : " + getTopicId()       + "\n";
        prettyString += wguStringUtils.indent(indention) + "   position : " + getPosition()      + "\n";

        return prettyString;
    }
}


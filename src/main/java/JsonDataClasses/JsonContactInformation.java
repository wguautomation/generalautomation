
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/12/2015.
 */

public class JsonContactInformation extends JsonData {

    public JsonContactInformation(String jsonResult) {
        super(jsonResult);
    }
    public JsonContactInformation(JsonObject object) {
        jsonObject = object;
    }

    public String getHelpLinePhoneNumber() {
        return getAsString("helpLinePhoneNumber");
    }

    public String getHelpLinePhoneNumberDialable() {
        return getAsString("helpLinePhoneNumberDialable");
    }

    public String getLastUpdatedLong() {
        return getAsString("lastUpdatedLong");
    }

    public JsonOfficeArray getOffices() {
        return new JsonOfficeArray(jsonObject);
    }

    public String getName() {
        return getAsString("name");
    }

    public String getAddress() {
        return getAsString("address");
    }

    public String getCity() {
        return getAsString("city");
    }

    public String getState() {
        return getAsString("state");
    }

    public String getZip() {
        return getAsString("zip");
    }

    public void checkAllKeys() {
        getHelpLinePhoneNumber();
        getHelpLinePhoneNumberDialable();
        getLastUpdatedLong();
        getOffices();
        getName();
        getAddress();
        getCity();
        getState();
        getZip();
    }

}


package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/13/2015.
 */

public class JsonSiteState extends JsonData {

    public JsonSiteState(String jsonResult) {
        super(jsonResult);
    }
    public JsonSiteState(JsonObject object) {
        jsonObject = object;
    }

    public String getSiteStateId() {
        return getAsString("id");
    }

    public String getState() {
        return getAsString("state");
    }

    public String getStateAbbreviation() {
        return getAsString("stateAbbreviation");
    }

    public void checkAllKeys() {
        getSiteStateId();
        getState();
        getStateAbbreviation();
    }

}

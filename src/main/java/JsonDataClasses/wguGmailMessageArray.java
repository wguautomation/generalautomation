
package JsonDataClasses;

import BaseClasses.LoginType;
import Utils.OssoSession;
import org.testng.TestNGException;

/*
 * Created by timothy.hallbeck on 4/4/2016.
 */

public class wguGmailMessageArray extends JsonGmailMessageArray {

    private LoginType loginType;

    public wguGmailMessageArray(LoginType loginType) {
        super();
        this.loginType = loginType;
        load();
    };

    public wguGmailMessageArray load() {
        String url;
        switch (loginType.getEnv()) {
            case Lane2Dev:
                url = "https://student-email.dev.wgu.edu/v1/gmail/students/" + loginType.getUsername();
                break;
            case Lane1Qa:
                url = "https://student-email.qa.wgu.edu/v1/gmail/students/" + loginType.getUsername();
                break;
            case Prod:
                url = "https://student-email.wgu.edu/v1/gmail/students/" + loginType.getUsername();
                break;
            default:
                throw new TestNGException("Undefined environment in wguGmailMessageArray::load()");
        }

        String result = new OssoSession().Get(url, loginType);
        loadArray(result, "entries");

        return this;
    }

}

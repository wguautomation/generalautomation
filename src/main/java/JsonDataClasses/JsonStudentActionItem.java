
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 1/20/2016.
 */

public class JsonStudentActionItem extends JsonData {

    public JsonStudentActionItem(String jsonResult) {
        super(jsonResult);
    }
    public JsonStudentActionItem(JsonObject object) {
        jsonObject = object;
    }

    public String getId() {
        return getAsString("id");
    }

    public String getTitle() {
        return getAsString("title");
    }

    public String getDateCompleted() {
        return getAsString("dateCompleted");
    }

    public String getPriority() {
        return getAsString("priority");
    }

    public String getDateCreated() {
        return getAsString("dateCreated");
    }

    public String getDescription() {
        return getAsString("description");
    }

    public String getDateDue() {
        return getAsString("dateDue");
    }

    public void checkAllKeys() {
        getId();
        getTitle();
        getDateCompleted();
        getPriority();
        getDateCreated();
        getDescription();
        getDateDue();
    }
}

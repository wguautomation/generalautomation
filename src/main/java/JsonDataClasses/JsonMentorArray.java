
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 11/4/2015.
 */

public class JsonMentorArray extends JsonData implements Iterable<JsonMentor>, Iterator<JsonMentor> {

    public JsonMentorArray() {}
    public JsonMentorArray(String jsonResult) {
        super(jsonResult, "");
    }
    public JsonMentorArray(JsonObject object) {
        super(object, "");
    }
    public JsonMentorArray(JsonArray array) {
        super(array);
    }

    public JsonMentor getMentor(int index) {
        return new JsonMentor(jsonArray.get(index).getAsJsonObject());
    }

    // Returns null if no email addresses match
    public JsonMentor getMentorByEmail(String email) {
        JsonMentor mentor=null;

        for (JsonElement element : jsonArray) {
            mentor = new JsonMentor(element.getAsJsonObject());
            if (mentor.getEmailAddress().contains(email))
                break;
        }

        return mentor;
    }

    public JsonMentorArray getAllMentors() {
        return new JsonMentorArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonMentor resource : getAllMentors())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonMentor next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            if (jsonArray == null || jsonArray.isJsonNull())
                return new JsonMentor(jsonObject);
            else
                return new JsonMentor(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonMentor> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}


package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 10/29/2015.
 */

// jsonArray is an array of (jsonObject) courses
public class JsonCourseArray extends JsonData implements Iterable<JsonCourse>, Iterator<JsonCourse> {

    static protected final String arrayKey = "courses";

    public JsonCourseArray(){}
    public JsonCourseArray(JsonObject object) {
        super(object, arrayKey);
    }
    public JsonCourseArray(JsonArray array) {
        super(array);
    }

    public JsonCourse getCourse(int index) {
        return new JsonCourse(jsonArray.get(index).getAsJsonObject());
    }

    public JsonCourse getCourseByCode(String courseCode) {
        JsonObject object = null;

        for (int index=0; index<jsonArray.size(); index++) {
            object = jsonArray.get(index).getAsJsonObject();
            if (object.get("courseCode").toString().contains(courseCode))
                break;
        }
        return new JsonCourse(object);
    }

    public JsonCourseArray getAllCourses() {
        return new JsonCourseArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonCourse resource : getAllCourses())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonCourse next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonCourse(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonCourse> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}

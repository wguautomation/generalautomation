package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 8/9/2016.
 */

public class JsonSessionArray extends JsonData implements Iterable<JsonSession>, Iterator<JsonSession> {

    public JsonSessionArray() {}
    public JsonSessionArray(JsonObject object) {
        super(object, "sessions");
        setObject(object);
    }
    public JsonSessionArray(JsonArray array) {
        super(array);
    }

    public JsonSessionArray getAllSessions() {
        return new JsonSessionArray(jsonArray);
    }

    public JsonSession getSession(int index) {
        return new JsonSession(jsonArray.get(index).getAsJsonObject());
    }

    public void checkAllKeys() {
        for (JsonSession resource : getAllSessions())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonSession next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonSession(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonSession> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }

}
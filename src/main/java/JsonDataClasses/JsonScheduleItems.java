
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 4/6/2016.
 */

public class JsonScheduleItems extends JsonData {

    public JsonScheduleItems() {}
    public JsonScheduleItems(String jsonResult) {
        super(jsonResult);
        loadArray(jsonResult, "items");
    }
    public JsonScheduleItems(JsonObject object) {
        jsonObject = object;
        loadArray(object, "items");
    }

    public String getSuccess() {
        return getAsString("success");
    }

    public String getHref() {
        return getAsString("href");
    }

    public String getFeedTitle() {
        return getAsString("feedTitle");
    }

    public JsonScheduleItem getItem(int index) {
        return new JsonScheduleItem(jsonArray.get(index).getAsJsonObject());
    }

    public void checkAllKeys() {
        getSuccess();
        getHref();
        getFeedTitle();
    }

}

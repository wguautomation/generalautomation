package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 8/9/2016.
 */

public class JsonSession extends JsonData {

    public JsonSession(String jsonResult) {
        super(jsonResult);
    }
    public JsonSession(JsonObject object) {
        jsonObject = object;
    }

    public String getStartDate() {
        return getAsString("startDate");
    }

    public String getPassEndDate() {
        return getAsString("passEndDate");
    }

    public String getPassDeadLine() {
        return getAsString("passDeadLine");
    }

    public String getIsFull() {
        return getAsString("isFull");
    }

    public String getId() {
        return getAsString("id");
    }

    public String getEndDate() {
        return getAsString("endDate");
    }

    public String getDeadLine() {
        return getAsString("deadLine");
    }

    public String getLiveEventDayAndTime() {
        return getAsString("liveEventDayAndTime");
    }

    public void checkAllKeys() {
        getStartDate();
        getPassEndDate();
        getPassDeadLine();
        getIsFull();
        getId();
        getEndDate();
        getDeadLine();
        getLiveEventDayAndTime();
    }
}

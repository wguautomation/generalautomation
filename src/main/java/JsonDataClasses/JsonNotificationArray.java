
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 4/4/2016.
 */

public class JsonNotificationArray extends JsonData implements Iterable<JsonNotification>, Iterator<JsonNotification> {

    public JsonNotificationArray() {}
    public JsonNotificationArray(String jsonResult) {
        super(jsonResult, "");
    }
    public JsonNotificationArray(JsonObject object) {
        super(object, "");
    }
    public JsonNotificationArray(JsonArray array) {
        super(array);
    }

    public JsonNotification getNotification(int index) {
        return new JsonNotification(jsonArray.get(index).getAsJsonObject());
    }

    public JsonNotificationArray getAllNotifications() {
        return new JsonNotificationArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonNotification resource : getAllNotifications())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonNotification next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            if (jsonArray == null || jsonArray.isJsonNull())
                return new JsonNotification(jsonObject);
            else
                return new JsonNotification(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonNotification> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}

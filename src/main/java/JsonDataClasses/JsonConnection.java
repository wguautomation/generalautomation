
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 4/5/2016.
 */

public class JsonConnection extends JsonData {

    public JsonConnection(String jsonResult) {
        super(jsonResult);
    }
    public JsonConnection(JsonObject object) {
        jsonObject = object;
    }

    public String getId() {
        return getAsString("id");
    }

    public String getPidm() {
        return getAsString("pidm");
    }

    public void checkAllKeys() {
        getId();
        getPidm();
    }

}


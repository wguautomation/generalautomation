
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 11/10/2015.
 */

public class JsonSubjectActivityArray extends JsonData implements Iterable<JsonSubjectActivity>, Iterator<JsonSubjectActivity> {

    public JsonSubjectActivityArray(String jsonResult) {
        super(jsonResult, "activities");
    }
    public JsonSubjectActivityArray(JsonObject object) {
        super(object, "activities");
    }
    public JsonSubjectActivityArray(JsonArray array) {
        super(array);
    }

    public JsonSubjectActivity getSubjectActivity(int index) {
        return new JsonSubjectActivity(jsonArray.get(index).getAsJsonObject());
    }

    public JsonSubjectActivity getSubjectActivityById(String id) {
        JsonObject object = null;

        for (int index=0; index<jsonArray.size(); index++) {
            object = jsonArray.get(index).getAsJsonObject();
            if (object.get("id").toString().contains(id))
                break;
        }
        return new JsonSubjectActivity(object);
    }

    public JsonSubjectActivityArray getAllSubjectActivities() {
        return new JsonSubjectActivityArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonSubjectActivity resource : getAllSubjectActivities())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonSubjectActivity next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonSubjectActivity(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonSubjectActivity> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}



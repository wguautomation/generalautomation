
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 4/6/2016.
 */

public class JsonCommunitiesArray extends JsonData implements Iterable<JsonCommunity>, Iterator<JsonCommunity> {

    JsonCommunitiesArray() {}
    JsonCommunitiesArray(JsonObject object) {
        super(object, "");
        setObject(object);
    }
    JsonCommunitiesArray(JsonArray array) {
        super(array);
    }

    public JsonCommunitiesArray getAllCommunities() {
        return new JsonCommunitiesArray(jsonArray);
    }

    public JsonCommunity getCommunity(int index) {
        return new JsonCommunity(jsonArray.get(index).getAsJsonObject());
    }

    public void checkAllKeys() {
        for (JsonCommunity resource : getAllCommunities())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonCommunity next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonCommunity(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonCommunity> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }

}
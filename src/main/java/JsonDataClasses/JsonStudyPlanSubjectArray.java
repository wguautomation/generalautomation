
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 11/10/2015.
 */

public class JsonStudyPlanSubjectArray extends JsonData implements Iterable<JsonStudyPlanSubject>, Iterator<JsonStudyPlanSubject> {

    public JsonStudyPlanSubjectArray(String jsonResult) {
        super(jsonResult, "subjects");
    }
    public JsonStudyPlanSubjectArray(JsonObject object) {
        super(object, "subjects");
    }
    public JsonStudyPlanSubjectArray(JsonArray array) {
        super(array);
    }

    public JsonStudyPlanSubject getStudyPlanSubject(int index) {
        return new JsonStudyPlanSubject(jsonArray.get(index).getAsJsonObject());
    }

    public JsonStudyPlanSubject getStudyPlanSubjectById(String id) {
        JsonObject object = null;

        for (int index=0; index<jsonArray.size(); index++) {
            object = jsonArray.get(index).getAsJsonObject();
            if (object.get("id").toString().contains(id))
                break;
        }
        return new JsonStudyPlanSubject(object);
    }

    public JsonStudyPlanSubjectArray getAllStudyPlanSubjects() {
        return new JsonStudyPlanSubjectArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonStudyPlanSubject resource : getAllStudyPlanSubjects())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonStudyPlanSubject next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonStudyPlanSubject(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonStudyPlanSubject> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}



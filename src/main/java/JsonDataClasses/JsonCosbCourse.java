
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/3/2015.
 */

public class JsonCosbCourse extends JsonData {

    public JsonCosbCourse(String jsonResult) {
        super(jsonResult);
    }

    public String getTitle() {
        return getAsString("courseTitle");
    }

    public String getIntro() {
        return getAsString("courseIntro");
    }

    public JsonCourseCompetencyArray getCourseCompetencies() {
        return new JsonCourseCompetencyArray(jsonObject);
    }

    public JsonCourseVideo getVideo() {
        return new JsonCourseVideo(getAsObject("courseVideo"));
    }

    public String getURL() {
        return getAsString("courseURL");
    }

    public String getVendor() {
        return getAsString("courseVendor");
    }

    public String getVendorLongDescription() {
        return getAsString("vendorLongDescription");
    }

    public String getVendorTitle() {
        return getAsString("vendorTitle");
    }

    public String getPamsCode() {
        return getAsString("pamsCode");
    }

    public String getPamsTitle() {
        return getAsString("pamsTitle");
    }

    public JsonCoursePamsAssessmentArray getPamsAssessments() {
        return new JsonCoursePamsAssessmentArray(jsonObject);
    }

    public String getPamsCourseVersion() {
        return getAsString("pamsCourseVersion");
    }

    public JsonCourseResourceArray getCoursePreparations() {
        JsonObject object = getAsObject("courseTools");
        return new JsonCourseResourceArray(object, "PreparationsList");
    }

    public JsonCourseResourceArray getCourseResources() {
        JsonObject object = getAsObject("courseTools");
        return new JsonCourseResourceArray(object, "CourseResourcesList");
    }

    public String getVendorShortDescription() {
        return getAsString("vendorShortDescription");
    }

    public void checkAllKeys() {
        getTitle();
        getIntro();
        getCourseCompetencies().checkAllKeys();
        getVideo();
        getURL();
        getVendor();
        getVendorLongDescription();
        getVendorTitle();
        getPamsCode();
        getPamsTitle();
        getPamsAssessments().checkAllKeys();
        getPamsCourseVersion();
        getCoursePreparations().checkAllKeys();
        getCourseResources().checkAllKeys();
        getVendorShortDescription();
    }

}

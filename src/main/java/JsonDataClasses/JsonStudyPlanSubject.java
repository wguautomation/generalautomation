
package JsonDataClasses;

import Utils.wguStringUtils;
import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/10/2015.
 */

public class JsonStudyPlanSubject extends JsonData {

    public JsonStudyPlanSubject(String jsonResult) {
        super(jsonResult);
    }
    public JsonStudyPlanSubject(JsonObject object) {
        jsonObject = object;
    }

    public String getId() {
        return getAsString("id");
    }

    public String getTitle() {
        return getAsString("title");
    }

    public String getShortDescription() {
        return getAsString("shortDescription");
    }

    public String getFullDescription() {
        return getAsString("fullDescription");
    }

    public String getCourseId() {
        return getAsString("courseId");
    }

    public String getPosition() {
        return getAsString("position");
    }

    public JsonSubjectTopicArray getAllTopics() {
        return new JsonSubjectTopicArray(jsonObject);
    }

    public void checkAllKeys() {
        getId();
        getTitle();
        getShortDescription();
        getFullDescription();
        getCourseId();
        getPosition();
        for (JsonSubjectTopic singleSubjectTopic : getAllTopics())
            singleSubjectTopic.checkAllKeys();
    }

    public String prettyString(int indention) {
        String prettyString = "";

        prettyString += wguStringUtils.indent(indention) + "                Subject\n";
        prettyString += wguStringUtils.indent(indention) + "               id : " + getId() + "\n";
        prettyString += wguStringUtils.indent(indention) + "            title : " + getTitle() + "\n";
        prettyString += wguStringUtils.indent(indention) + " shortDescription : " + wguStringUtils.ellisize(wguStringUtils.removeNewlines(getShortDescription()), 90) + "\n";
        prettyString += wguStringUtils.indent(indention) + "  fullDescription : " + wguStringUtils.ellisize(wguStringUtils.removeNewlines(getFullDescription()), 90) + "\n";
        prettyString += wguStringUtils.indent(indention) + "         courseId : " + getCourseId() + "\n";
        prettyString += wguStringUtils.indent(indention) + "         position : " + getPosition() + "\n";

        return prettyString;
    }
}

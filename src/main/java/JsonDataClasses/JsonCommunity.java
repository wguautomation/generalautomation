
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 4/6/2016.
 */

public class JsonCommunity extends JsonData {

    public JsonCommunity(String jsonResult) {
        super(jsonResult);
    }
    public JsonCommunity(JsonObject object) {
        jsonObject = object;
    }

    public String getName() {
        return getAsString("name");
    }

    public String getUrl() {
        return getAsString("url");
    }

    public void checkAllKeys() {
        getName();
        getUrl();
    }
}

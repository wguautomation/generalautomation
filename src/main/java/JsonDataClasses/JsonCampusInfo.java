
package JsonDataClasses;

import Utils.General;

/*
 * Created by timothy.hallbeck on 10/10/2016.
 */

public class JsonCampusInfo extends JsonData {

    public JsonCampusInfo() {}
    public JsonCampusInfo(String jsonResult) {
    }

    public String getCampus() {
        return getAsString("campus");
    }

    public String getSupportUrl() {
        return getAsString("supportUrl");
    }

    public void checkAllKeys() {
        getCampus();
        getSupportUrl();
    }

    @Override
    public String toString() {
        String string;

        string =  "\nCampus:  " + getCampus() + "\n";
        string += "Support URL:  " + getSupportUrl();

        return string;
    }

}

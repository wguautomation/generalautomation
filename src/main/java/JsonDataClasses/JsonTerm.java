
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 10/29/2015.
 */

// jsonArray contains the array of courses
public class JsonTerm extends JsonData {

    JsonTerm(JsonObject object) {
        super(object);
    }

    public int getCourseCount() {
        return getAllCourses().size();
    }

    public String getTermId() {
        return getAsString("id");
    }

    public String getTermCode() {
        return getAsString("termCode");
    }

    public String getStartDate() {
        return getAsString("startDate");
    }

    public String getEndDate() {
        return getAsString("endDate");
    }

    public String getTermNumber() {
        return getAsString("termNumber");
    }

    public String getInPreviousProgram() {
        return getAsString("inPreviousProgram");
    }

    public String getCurrent() {
        return getAsString("current");
    }

    public JsonCourseArray getAllCourses() {
        return new JsonCourseArray(jsonObject);
    }

    public String getTransferTerm() {
        return getAsString("transferTerm");
    }

    public String getCompleted() {
        return getAsString("completed");
    }

    public String getRemaining() {
        return getAsString("remaining");
    }

    public String getApproved() {
        return getAsString("approved");
    }

    public String getLateReferralRequired() {
        return getAsString("lateReferralRequired");
    }

    public String getTotalCus() {
        return getAsString("totalCus");
    }

    public String getPidm() {
        return getAsString("pidm");
    }

    public String getEndDateFormatted() {
        return getAsString("endDateFormatted");
    }

    public String getStartDateFormatted() {
        return getAsString("startDateFormatted");
    }

    public void checkAllKeys() {
        getCourseCount();
//        getTermId();
        getTermCode();
        getStartDate();
        getEndDate();
        getTermNumber();
        getInPreviousProgram();
        getCurrent();
        for (JsonCourse resource : getAllCourses())
            resource.checkAllKeys();
        getTransferTerm();
        getCompleted();
        getRemaining();
        getApproved();
        getLateReferralRequired();
        getTotalCus();
        getPidm();
        getEndDateFormatted();
        getStartDateFormatted();
    }

}

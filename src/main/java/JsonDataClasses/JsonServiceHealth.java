
package JsonDataClasses;

/*
 * Created by timothy.hallbeck on 7/20/2016.
 */

import Utils.General;

public class JsonServiceHealth extends JsonData {

    public JsonServiceHealth() {}
    public JsonServiceHealth(String jsonResult) {
        super(jsonResult);
    }

    public String getDescription() {
        return getAsString("description");
    }

    public String getStatus() {
        return getAsString("status");
    }

    public void checkAllKeys() {
//        getDescription();
//        getStatus();
    }

    @Override
    public String toString() {
        String string = "Status : " + getStatus();
        return string;
    }

}


package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/12/2015.
 */

public class JsonLinkRating extends JsonData {

    public JsonLinkRating(String jsonResult) {
        super(jsonResult);
    }
    public JsonLinkRating(JsonObject object) {
        jsonObject = object;
    }

    public String getIosTabletRating() {
        return getAsString("iosTabletRating");
    }

    public String getLinkUrl() {
        return getAsString("linkUrl");
    }

    public String getCourseVersionId() {
        return getAsString("courseVersionId");
    }

    public String getIosPhoneRating() {
        return getAsString("iosPhoneRating");
    }

    public String getAndroidTabletRating() {
        return getAsString("androidTabletRating");
    }

    public String getStudyPlanId() {
        return getAsString("studyPlanId");
    }

    public String getAndroidPhoneRating() {
        return getAsString("androidPhoneRating");
    }

    public String getProviderName() {
        return getAsString("providerName");
    }

    public String getProviderId() {
        return getAsString("providerId");
    }

    public String getItemId() {
        return getAsString("itemId");
    }

    public void checkAllKeys() {
        getIosTabletRating();
        getLinkUrl();
        getCourseVersionId();
        getIosPhoneRating();
        getAndroidTabletRating();
        getStudyPlanId();
        getAndroidPhoneRating();
        getProviderName();
        getProviderId();
        getItemId();
    }

}

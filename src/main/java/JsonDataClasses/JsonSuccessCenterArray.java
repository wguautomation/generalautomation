
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 4/4/2016.
 */

public class JsonSuccessCenterArray extends JsonData implements Iterable<JsonSuccessCenter>, Iterator<JsonSuccessCenter> {

    public JsonSuccessCenterArray() {}
    public JsonSuccessCenterArray(String jsonResult) {
        super(jsonResult, "");
    }
    public JsonSuccessCenterArray(JsonObject object) {
        super(object, "");
    }
    public JsonSuccessCenterArray(JsonArray array) {
        super(array);
    }

    public JsonSuccessCenter getSuccessCenter(int index) {
        return new JsonSuccessCenter(jsonArray.get(index).getAsJsonObject());
    }

    public JsonSuccessCenterArray getAllSuccessCenters() {
        return new JsonSuccessCenterArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonSuccessCenter resource : getAllSuccessCenters())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonSuccessCenter next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            if (jsonArray == null || jsonArray.isJsonNull())
                return new JsonSuccessCenter(jsonObject);
            else
                return new JsonSuccessCenter(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonSuccessCenter> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}


package JsonDataClasses;

import BaseClasses.LoginType;
import Utils.OssoSession;
import org.testng.TestNGException;

/*
 * Created by timothy.hallbeck on 4/6/2016.
 */

public class wguCommunities extends JsonCommunitiesArray {

    private LoginType loginType;

    public wguCommunities(LoginType loginType) {
        super();
        this.loginType = loginType;
        load();
    };

    public wguCommunities load() {
        String url;
        switch (loginType.getEnv()) {
            case Lane2Dev:
                url = "https://student-communities.dev.wgu.edu/v1/communities/" + loginType.getPidm();
                break;
            case Lane1Qa:
                url = "https://student-communities.qa.wgu.edu/v1/communities/" + loginType.getPidm();
                break;
            case Prod:
                url = "https://student-communities.wgu.edu/v1/communities/" + loginType.getPidm();
                break;
            default:
                throw new TestNGException("Undefined environment in wguCommunities::load()");
        }

        String result = new OssoSession().Get(url, loginType);
        super.loadArray(result, "");

        return this;
    }

}

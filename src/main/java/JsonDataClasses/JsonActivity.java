
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 9/26/2016.
 */

public class JsonActivity extends JsonData {

    public JsonActivity() {}
    public JsonActivity(String jsonResult) {
        super(jsonResult);
    }
    public JsonActivity(JsonObject object) {
        jsonObject = object;
    }

    public String getId() {
        return getAsString("id");
    }

    public String getPidm() {
        return getAsString("pidm");
    }

    public String getActivityDate() {
        return getAsString("activityDate");
    }

    public String getActivityType() {
        return getAsString("activityType");
    }

    public JsonActivityDetailArray getActivityDetails() {
        return new JsonActivityDetailArray(jsonObject);
    }

    public void checkAllKeys() {
        getId();
        getPidm();
        getActivityDate();
        getActivityType();
        getActivityDetails();
    }
}
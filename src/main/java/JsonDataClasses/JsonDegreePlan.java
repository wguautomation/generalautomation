
package JsonDataClasses;

import com.google.gson.JsonArray;
import org.testng.TestNGException;

/*
 * Created by timothy.hallbeck on 10/29/2015.
 */

public class JsonDegreePlan extends JsonData {

    public JsonDegreePlan() {
        super();
    }
    public JsonDegreePlan(String jsonResult) {
        super(jsonResult);
    }

    public String getId() {
        return getAsString("id");
    }

    public String getName() {
        return getAsString("name");
    }

    public String getCurrentPlan() {
        return getAsString("currentPlan");
    }

    public JsonTerm getCurrentTerm() {
        JsonTermArray termArray = getAllTerms();
        for (JsonTerm term : termArray)
            if (term.getCurrent().toLowerCase() == "true")
                return term;
        throw new TestNGException("No current term in " + termArray);
    }

    public JsonTermArray getAllTerms() {
        return new JsonTermArray(jsonObject);
    }

    public JsonTerm getTerm(String code) {
        return getAllTerms().getTermByCode(code);
    }

    public JsonTerm getTerm(int index) {
        return getAllTerms().getTerm(index);
    }

    public String getPidm() {
        return getAsString("pidm");
    }

    public JsonCourseArray getAllCourses() {
        JsonArray array = new JsonArray();

        for (JsonTerm term : getAllTerms())
            array.addAll(term.getAllCourses().getBaseArray());

        return new JsonCourseArray(array);
    }

    public JsonCourse getCourseByCode(String courseCode) {
        JsonCourseArray array = getAllCourses();

        for (JsonCourse course : array)
            if (course.getCourseCode().contains(courseCode))
                return course;

        return null;
    }

    public void checkAllKeys() {
        getId();
        getName();
        getCurrentPlan();
        getAllTerms();
        getPidm();
        for (JsonCourse resource : getAllCourses())
            resource.checkAllKeys();
    }

}

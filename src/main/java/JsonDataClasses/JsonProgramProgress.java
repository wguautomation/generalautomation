
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/9/2015.
 */

public class JsonProgramProgress extends JsonData {

    public JsonProgramProgress() {}
    public JsonProgramProgress(String jsonResult) {
        super(jsonResult);
    }
    public JsonProgramProgress(JsonObject object) {
        jsonObject = object;
    }

    public String getTotalProgramCu() {
        return getAsString("totalProgramCu");
    }

    public String getCurrentTermCode() {
        return getAsString("currentTermCode");
    }

    public String getPlannedGraduationDate() {
        return getAsString("plannedGraduationDate");
    }

    public String getProgramCode() {
        return getAsString("programCode");
    }

    public String getAttemptedCu() {
        return getAsString("attemptedCu");
    }

    public String getRemainingCu() {
        return getAsString("remainingCu");
    }

    public String getProgramVersionId() {
        return getAsString("programVersionId");
    }

    public String getTransferCu() {
        return getAsString("transferCu");
    }

    public String getTotalTerms() {
        return getAsString("totalTerms");
    }

    public String getProgramVersion() {
        return getAsString("programVersion");
    }

    public String getTermsCompleted() {
        return getAsString("termsCompleted");
    }

    public String getPidm() {
        return getAsString("pidm");
    }

    public String getProgramName() {
        return getAsString("programName");
    }

    public String getCompletedCu() {
        return getAsString("completedCu");
    }

    public String getOtp() {
        return getAsString("otp");
    }

    static public void checkAllKeys(String jsonData) {
        new JsonProgramProgress(jsonData).checkAllKeys();
    }
    public void checkAllKeys() {
        getTotalProgramCu();
        getCurrentTermCode();
        getPlannedGraduationDate();
        getProgramCode();
        getAttemptedCu();
        getRemainingCu();
        getProgramVersionId();
        getTransferCu();
        getTotalTerms();
        getProgramVersion();
        getTermsCompleted();
        getPidm();
        getProgramName();
        getCompletedCu();
        getOtp();
    }

}

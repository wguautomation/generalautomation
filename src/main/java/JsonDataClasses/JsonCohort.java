package JsonDataClasses;

/*
 * Created by timothy.hallbeck on 8/9/2016.
 */

import com.google.gson.JsonObject;

public class JsonCohort extends JsonData {

    public JsonCohort(String jsonResult) {
        super(jsonResult);
    }
    public JsonCohort(JsonObject object) {
        jsonObject = object;
    }

    public String getSessionId() {
        return getAsString("sessionId");
    }

    public String getRelatedCourses() {
        return getAsString("relatedCourses");
    }

    public String getPhoneNumberAndCode() {
        return getAsString("phoneNumberAndCode");
    }

    public String getId() {
        return getAsString("id");
    }

    public String getEventUrl() {
        return getAsString("eventUrl");
    }

    public String getEnrollmentId() {
        return getAsString("enrollmentId");
    }

    public String getEnrolledSession() {
        return getAsString("enrolledSession");
    }

    public String getEmail() {
        return getAsString("email");
    }

    public String getDescription() {
        return getAsString("description");
    }

    public String getCohortName() {
        return getAsString("cohortName");
    }

    public JsonSessionArray getSessions() {
        return new JsonSessionArray(jsonObject);
    }

    public void checkAllKeys() {
        getSessionId();
        getRelatedCourses();
        getPhoneNumberAndCode();
        getId();
        getEventUrl();
        getEnrollmentId();
        getEnrolledSession();
        getEmail();
        getDescription();
        getCohortName();
    }
}

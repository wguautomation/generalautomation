
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/9/2015.
 */

public class JsonNewsItem extends JsonData {

    public JsonNewsItem(String jsonResult) {
        super(jsonResult);
    }
    public JsonNewsItem(JsonObject object) {
        jsonObject = object;
    }

    public String getType() {
        return getAsString("type");
    }

    public JsonActivityDate getActivityDate() {
        return new JsonActivityDate(getAsObject("activityDate"));
    }

    public JsonNewsItemData getNewsItemData() {
        return new JsonNewsItemData(getAsObject("data"), getType());
    }

    public String getTitle() {
        return getAsString("title");
    }

    public String getBody() {
        return getAsString("body");
    }

    public void checkAllKeys() {
        getType();
        getActivityDate();
        getNewsItemData();
        getTitle();
        getBody();
    }

}

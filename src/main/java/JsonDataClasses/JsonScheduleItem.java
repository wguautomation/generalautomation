
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 4/6/2016.
 */

public class JsonScheduleItem extends JsonData {

    public JsonScheduleItem() {}
    public JsonScheduleItem(String jsonResult) {
        super(jsonResult);
    }
    public JsonScheduleItem(JsonObject object) {
        jsonObject = object;
    }

    public String getTitle() {
        return getAsString("title");
    }

    public String getDate() {
        return getAsString("date");
    }

    public String getId() {
        return getAsString("id");
    }

    public String getItemType() {
        return getAsString("itemType");
    }

    public String getAction() {
        return getAsString("action");
    }

    public void checkAllKeys() {
        getTitle();
        getDate();
        getId();
        getItemType();
        getAction();
    }

}


package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 9/26/2016.
 */
public class JsonActivityDetail extends JsonData {

    public JsonActivityDetail(String jsonResult) {
        super(jsonResult);
    }
    public JsonActivityDetail(JsonObject object) {
        jsonObject = object;
    }

    public String getId() {
        return getAsString("id");
    }

    public String getPidm() {
        return getAsString("pidm");
    }

    public String getCourseVersionID() {
        return getAsString("courseVersionID");
    }

    public String getStartTerm() {
        return getAsString("startTerm");
    }

    public String getViewState() {
        return getAsString("viewState");
    }

    public String getCanStart() {
        return getAsString("canStart");
    }

    public String getToggleFeature() {
        return getAsString("toggleFeature");
    }

    public String getTitle() {
        return getAsString("title");
    }

    public String getEarlyStartDate() {
        return getAsString("earlyStartDate");
    }

    public String getStartDate() {
        return getAsString("startDate");
    }

    public String getEarlyPreviewViews() {
        return getAsString("earlyPreviewViews");
    }

    public String getEarlyEngagedViews() {
        return getAsString("earlyEngagedViews");
    }

    public String getPreviewViews() {
        return getAsString("previewViews");
    }

    public String getEngagedViews() {
        return getAsString("engagedViews");
    }

    public void checkAllKeys() {
        getId();
        getPidm();
        getCourseVersionID();
        getStartTerm();
        getViewState();
        getCanStart();
        getToggleFeature();
        getTitle();
        getEarlyStartDate();
        getStartDate();
        getEarlyPreviewViews();
        getEarlyEngagedViews();
        getPreviewViews();
        getEngagedViews();
    }
}
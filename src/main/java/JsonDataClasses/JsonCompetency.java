
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 10/28/2015.
 */

public class JsonCompetency extends JsonData {

    public JsonCompetency(String jsonResult) {
        super(jsonResult);
    }
    public JsonCompetency(JsonObject object) {
        jsonObject = object;
    }

    public String getTitle() {
        return getAsString("title");
    }

    public String getDescription() {
        return getAsString("description");
    }

    public String getAssessmentCode() {
        return getAsString("assessmentCode");
    }

    public String getAssessmentId() {
        return getAsString("assessmentId");
    }

    public String getCompetencyId() {
        return getAsString("competencyId");
    }

    public String getCode() {
        return getAsString("code");
    }

    public void checkAllKeys() {
        getTitle();
        getDescription();
        getAssessmentCode();
        getAssessmentId();
        getCompetencyId();
        getCode();
    }
}

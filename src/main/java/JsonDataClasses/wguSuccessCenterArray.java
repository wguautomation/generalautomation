
package JsonDataClasses;

import BaseClasses.LoginType;
import Utils.General;
import Utils.OssoSession;
import org.testng.TestNGException;

/*
 * Created by timothy.hallbeck on 4/4/2016.
 */

public class wguSuccessCenterArray extends JsonSuccessCenterArray {

    protected LoginType loginType;

    public wguSuccessCenterArray(LoginType loginType) {
        super();
        this.loginType = loginType;
        load();
    };

    public wguSuccessCenterArray load() {
        General.Debug("wguSuccessCenterArray::load()");

        String url;
        switch (loginType.getEnv()) {
            case Lane2Dev:
                url = "https://mashery.wgu.edu/dev/successcentertile/v1/tiles/pidm/" + loginType.getPidm();
                break;
            case Lane1Qa:
                url = "https://mashery.wgu.edu/qa/successcentertile/v1/tiles/pidm/" + loginType.getPidm();
                break;
            case Prod:
                url = "https://mashery.wgu.edu/successcentertile/v1/tiles/pidm/" + loginType.getPidm();
                break;
            default:
                throw new TestNGException("Undefined environment in wguSuccessCenterArray::load()");
        }

        String result = new OssoSession().Get(url, loginType);
        loadArray(result, "");

        return this;
    }

}

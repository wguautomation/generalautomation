
package JsonDataClasses;

import BaseClasses.LoginType;
import Utils.OssoSession;
import org.testng.TestNGException;

/*
 * Created by timothy.hallbeck on 4/4/2016.
 */

public class wguNotificationArray extends JsonNotificationArray {

    private LoginType loginType;

    public wguNotificationArray(LoginType loginType) {
        super();
        this.loginType = loginType;
        load();
    };

    public wguNotificationArray load() {
        String url;
        switch (loginType.getEnv()) {
            case Lane2Dev:
                url = "https://mashery.wgu.edu/dev/ne-subscriber/v1/" + loginType.getUsername() + "/notifications";
                break;
            case Lane1Qa:
                url = "https://mashery.wgu.edu/qa/ne-subscriber/v1/" + loginType.getUsername() + "/notifications";
                break;
            case Prod:
                url = "https://mashery.wgu.edu/ne-subscriber/v1/" + loginType.getUsername() + "/notifications";
                break;
            default:
                throw new TestNGException("Undefined environment in wguNotificationArray::load()");
        }

        String result = new OssoSession().Get(url, loginType);
        loadArray(result, "");

        return this;
    }

}


package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 11/9/2015.
 */

public class JsonFaqArray extends JsonData implements Iterable<JsonFaq>, Iterator<JsonFaq> {

    public JsonFaqArray(String jsonResult) {
        super(jsonResult, "faqs");
    }
    public JsonFaqArray(JsonArray array) {
        super(array);
    }

    public JsonFaq getFaq(int index) {
        return new JsonFaq(jsonArray.get(index).getAsJsonObject());
    }

    public JsonFaq getFaqByQuestionText(String text) {
        JsonObject object = null;

        for (int index=0; index<jsonArray.size(); index++) {
            object = jsonArray.get(index).getAsJsonObject();
            if (object.get("question").toString().contains(text))
                break;
        }
        return new JsonFaq(object);
    }

    public JsonFaq getFaqByAnswerText(String text) {
        JsonObject object = null;

        for (int index=0; index<jsonArray.size(); index++) {
            object = jsonArray.get(index).getAsJsonObject();
            if (object.get("answer").toString().contains(text))
                break;
        }
        return new JsonFaq(object);
    }

    public JsonFaqArray getAllwguFaqs() {
        return new JsonFaqArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonFaq resource : getAllwguFaqs())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonFaq next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonFaq(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonFaq> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}


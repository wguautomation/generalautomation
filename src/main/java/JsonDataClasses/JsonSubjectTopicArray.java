
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 11/10/2015.
 */

public class JsonSubjectTopicArray extends JsonData implements Iterable<JsonSubjectTopic>, Iterator<JsonSubjectTopic> {

    public JsonSubjectTopicArray(String jsonResult) {
        super(jsonResult, "topics");
    }
    public JsonSubjectTopicArray(JsonObject object) {
        super(object, "topics");
    }
    public JsonSubjectTopicArray(JsonArray array) {
        super(array);
    }

    public JsonSubjectTopic getSubjectTopic(int index) {
        return new JsonSubjectTopic(jsonArray.get(index).getAsJsonObject());
    }

    public JsonSubjectTopic getSubjectTopicById(String id) {
        JsonObject object = null;

        for (int index=0; index<jsonArray.size(); index++) {
            object = jsonArray.get(index).getAsJsonObject();
            if (object.get("id").toString().contains(id))
                break;
        }
        return new JsonSubjectTopic(object);
    }

    public JsonSubjectTopicArray getAllSubjectTopics() {
        return new JsonSubjectTopicArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonSubjectTopic resource : getAllSubjectTopics())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonSubjectTopic next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonSubjectTopic(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonSubjectTopic> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}


package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 8/9/2016.
 */

public class JsonCohortArray extends JsonData implements Iterable<JsonCohort>, Iterator<JsonCohort> {

    public JsonCohortArray() {}
    public JsonCohortArray(String json) {
        super(json, "");
    }
    public JsonCohortArray(JsonObject object) {
        super(object, "cohorts");
        setObject(object);
        checkAllKeys();
    }
    public JsonCohortArray(JsonArray array) {
        super(array);
    }

    public JsonCohortArray getAllCohorts() {
        return new JsonCohortArray(jsonArray);
    }

    public JsonCohort getCohort(int index) {
        return new JsonCohort(jsonArray.get(index).getAsJsonObject());
    }

    public void checkAllKeys() {
        for (JsonCohort resource : getAllCohorts())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonCohort next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonCohort(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonCohort> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }

}
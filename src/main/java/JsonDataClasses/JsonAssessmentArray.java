
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 11/9/2015.
 */
public class JsonAssessmentArray extends JsonData implements Iterable<JsonAssessment>, Iterator<JsonAssessment> {

    static protected final String arrayKey = "assessment";

    public JsonAssessmentArray() {
    }
    public JsonAssessmentArray(String result) {
        super(result, arrayKey);
    }
    public JsonAssessmentArray(JsonObject object) {
        super(object, arrayKey);
        setObject(object);
    }
    public JsonAssessmentArray(JsonArray array) {
        super(array);
    }

    public JsonAssessment getAssessmentByCourseCode(String courseCode) {
        JsonObject object = null;

        for (int index=0; index<jsonArray.size(); index++) {
            object = jsonArray.get(index).getAsJsonObject();
            if (object.get("courseCode").toString().contains(courseCode))
                break;
        }
        return new JsonAssessment(object);
    }

    public JsonAssessmentArray getAllAssessments() {
        return new JsonAssessmentArray(jsonArray);
    }

    public JsonAssessment getAssessment(int index) {
        return new JsonAssessment(jsonArray.get(index).getAsJsonObject());
    }

    public void checkAllKeys() {
        for (JsonAssessment resource : getAllAssessments())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonAssessment next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonAssessment(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonAssessment> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }

}
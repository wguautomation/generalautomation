
package JsonDataClasses;

/*
 * Created by timothy.hallbeck on 11/3/2015.
 */

public class JsonCosbCourseInfo extends JsonData {

    public JsonCosbCourseInfo(String jsonResult) {
        super(jsonResult);
    }

    public String getCourseVersionid() {
        return getAsString("courseVersionid");
    }

    public String getCourseTitle() {
        return getAsString("courseTitle");
    }

    public String getCourseCode() {
        return getAsString("courseCode");
    }

    public String getCourseType() {
        return getAsString("courseType");
    }

    public String getPdfURL() {
        return getAsString("pdfURL");
    }

    public void checkAllKeys() {
        getCourseVersionid();
        getCourseTitle();
        getCourseCode();
        getCourseType();
        getPdfURL();
    }

}
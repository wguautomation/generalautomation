
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 4/5/2016.
 */

public class JsonConnectionArray extends JsonData implements Iterable<JsonConnection>, Iterator<JsonConnection> {

    public JsonConnectionArray() {}
    public JsonConnectionArray(String jsonResult) {
        super(jsonResult, "");
    }
    public JsonConnectionArray(JsonObject object) {
        super(object, "");
    }
    public JsonConnectionArray(JsonArray array) {
        super(array);
    }

    public void checkAllKeys() {
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonConnection next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonConnection(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonConnection> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}

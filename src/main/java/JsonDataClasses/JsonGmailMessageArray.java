
package JsonDataClasses;

import com.google.gson.JsonArray;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 4/4/2016.
 */

public class JsonGmailMessageArray extends JsonData implements Iterable<JsonGmailMessage>, Iterator<JsonGmailMessage> {

    public JsonGmailMessageArray() {}
    public JsonGmailMessageArray(String jsonResult) {
        super(jsonResult, "entries");
    }
    public JsonGmailMessageArray(JsonArray array) {
        super(array);
    }

    public JsonGmailMessage getEmail(int index) {
        return new JsonGmailMessage(jsonArray.get(index).getAsJsonObject());
    }

    public JsonGmailMessageArray getAllMessages() {
        return new JsonGmailMessageArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonGmailMessage resource : getAllMessages())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonGmailMessage next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonGmailMessage(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonGmailMessage> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}



package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 11/3/2015.
 */

public class JsonCourseCompetencyArray extends JsonData implements Iterable<JsonCourseCompetency>, Iterator<JsonCourseCompetency> {

    public JsonCourseCompetencyArray(String jsonResult) {
        super(jsonResult, "competencies");
    }
    public JsonCourseCompetencyArray(JsonObject object) {
        super(object, "competencies");
    }
    public JsonCourseCompetencyArray(JsonArray array) {
        super(array);
    }

    public JsonCourseCompetency getCompetency(int index) {
        return new JsonCourseCompetency(jsonArray.get(index).getAsJsonObject());
    }

    public JsonCourseCompetency getCompetencyById(String id) {
        JsonObject object = null;

        for (int index=0; index<jsonArray.size(); index++) {
            object = jsonArray.get(index).getAsJsonObject();
            if (object.get("id").toString().contains(id))
                break;
        }
        return new JsonCourseCompetency(object);
    }

    public JsonCourseCompetencyArray getAllCourseCompetencies() {
        return new JsonCourseCompetencyArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonCourseCompetency resource : getAllCourseCompetencies())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonCourseCompetency next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonCourseCompetency(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonCourseCompetency> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}


package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 10/5/2016.
 */

public class JsonFinAidRequirement extends JsonData {

    public JsonFinAidRequirement(String jsonResult) {
        super(jsonResult);
    }
    public JsonFinAidRequirement(JsonObject object) {
        jsonObject = object;
    }

    public String getName() {
        return getAsString("name");
    }

    public String getStatus() {
        return getAsString("status");
    }

    public String getOnbaseDocType() {
        return getAsString("onbaseDocType");
    }

    public void checkAllKeys() {
        getName();
        getStatus();
        getOnbaseDocType();
    }

    @Override
    public String toString() {
        String output = getName() + ", " + getStatus() + ", " + getOnbaseDocType() + "\n";

        return output;
    }

}

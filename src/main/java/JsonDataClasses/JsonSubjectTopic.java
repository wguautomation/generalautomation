
package JsonDataClasses;

import Utils.wguStringUtils;
import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/10/2015.
 */

public class JsonSubjectTopic extends JsonData {

    public JsonSubjectTopic(String jsonResult) {
        super(jsonResult);
    }
    public JsonSubjectTopic(JsonObject object) {
        jsonObject = object;
    }

    public String getId() {
        return getAsString("id");
    }

    public String getTitle() {
        return getAsString("title");
    }

    public String getInstruction() {
        return getAsString("instruction");
    }

    public String getSubjectId() {
        return getAsString("subjectId");
    }

    public String getCourseId() {
        return getAsString("courseId");
    }

    public String getPosition() {
        return getAsString("position");
    }

    public JsonSubjectActivityArray getAllActivities() {
        return new JsonSubjectActivityArray(jsonObject);
    }

    public void checkAllKeys() {
        getId();
        getTitle();
        getInstruction();
        getSubjectId();
        getCourseId();
        getPosition();
        for (JsonSubjectActivity singleActivity : getAllActivities())
            singleActivity.checkAllKeys();
    }

    public String prettyString(int indention) {
        String prettyString = "";

        prettyString += wguStringUtils.indent(indention) + "            Topic" + "\n";
        prettyString += wguStringUtils.indent(indention) + "          id : " + getId() + "\n";
        prettyString += wguStringUtils.indent(indention) + "       title : " + getTitle() + "\n";
        prettyString += wguStringUtils.indent(indention) + " instruction : " + wguStringUtils.ellisize(wguStringUtils.removeNewlines(getInstruction()), 90) + "\n";
        prettyString += wguStringUtils.indent(indention) + "   subjectId : " + getSubjectId() + "\n";
        prettyString += wguStringUtils.indent(indention) + "    courseId : " + getCourseId() + "\n";
        prettyString += wguStringUtils.indent(indention) + "    position : " + getPosition() + "\n";

        return prettyString;
    }
}


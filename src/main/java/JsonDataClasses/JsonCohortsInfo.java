package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 8/9/2016.
 */
public class JsonCohortsInfo extends JsonData {

    public JsonCohortsInfo(String jsonResult) {
        super(jsonResult);
    }
    public JsonCohortsInfo(JsonObject object) {
        jsonObject = object;
    }

    public String getToggleFeature() {
        return getAsString("toggleFeature");
    }

    public String getShowCohort() {
        return getAsString("showCohort");
    }

    public String getCourseVersionId() {
        return getAsString("courseVersionId");
    }

    public String getPidm() {
        return getAsString("pidm");
    }

    public JsonCohortArray getCohorts() {
        return new JsonCohortArray(jsonObject);
    }

    public JsonCohort getCohort(int index) {
        return getCohorts().getCohort(index);
    }

    public void checkAllKeys() {
        getToggleFeature();
        getShowCohort();
        getCourseVersionId();
        getPidm();
        getCohorts();
    }
}


package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/12/2015.
 */

public class JsonResource extends JsonData {

    public JsonResource(String jsonResult) {
        super(jsonResult);
    }
    public JsonResource(JsonObject object) {
        jsonObject = object;
    }

    public String getUrlName() {
        return getAsString("urlName");
    }

    public String getUrl() {
        return getAsString("url");
    }

    public void checkAllKeys() {
        getUrlName();
        getUrl();
    }

}

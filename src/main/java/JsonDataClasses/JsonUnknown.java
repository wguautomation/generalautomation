
package JsonDataClasses;

import Utils.General;
import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 9/19/2016.
 */

public class JsonUnknown extends JsonData {

    public JsonUnknown(){}
    public JsonUnknown(String jsonResult) {
        General.Debug("Class Not implemented");
    }
    public JsonUnknown(JsonObject object) {
        General.Debug("Class Not implemented");
    }

    public void checkAllKeys() {
        General.Debug("JsonUnknown::checkAllKeys() Not implemented");
    }

}


package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/12/2015.
 */

public class JsonResourceHeader extends JsonData {

    public JsonResourceHeader(String jsonResult) {
        super(jsonResult);
    }
    public JsonResourceHeader(JsonObject object) {
        jsonObject = object;
    }

    public String getHeaderName() {
        return getAsString("headerName");
    }

    public JsonResourceArray getAllResources() {
        return new JsonResourceArray(jsonObject);
    }

    public void checkAllKeys() {
        getHeaderName();
        for (JsonResource resource : getAllResources())
            resource.checkAllKeys();
    }
}

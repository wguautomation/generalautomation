
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 11/9/2015.
 */
public class JsonTagArray extends JsonData implements Iterable<JsonTag>, Iterator<JsonTag> {

    public JsonTagArray(String jsonResult) {
        super(jsonResult, "tags");
    }
    public JsonTagArray(JsonObject object) {
        super(object, "tags");
    }
    public JsonTagArray(JsonArray array) {
        super(array);
    }

    public JsonTag getTag(int index) {
        return new JsonTag(jsonArray.get(index).getAsJsonObject());
    }

    public JsonTag getTagByText(String tag) {
        JsonObject object = null;

        for (int index=0; index<jsonArray.size(); index++) {
            object = jsonArray.get(index).getAsJsonObject();
            if (object.get("tag").toString().contains(tag))
                break;
        }
        return new JsonTag(object);
    }

    public JsonTagArray getAllTags() {
        return new JsonTagArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonTag resource : getAllTags())
            resource.checkAllKeys();
      }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonTag next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonTag(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonTag> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}



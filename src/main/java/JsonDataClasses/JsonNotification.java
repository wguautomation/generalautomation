
package JsonDataClasses;

import Utils.General;
import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 4/4/2016.
 */

public class JsonNotification extends JsonData {

    public JsonNotification() {
        super();
    }
    public JsonNotification(String jsonResult) {
        super(jsonResult);
    }
    public JsonNotification(JsonObject object) {
        jsonObject = object;
    }

    public String getActionUrl() {
        return getAsString("action_url");
    }

    public String getCategory() {
        return getAsString("category");
    }

    public String getCreationDate() {
        return getAsString("creationDate");
    }

    public String getDateDismissed() {
        return getAsString("dateDismissed");
    }

    public String getDateSeen() {
        return getAsString("dateSeen");
    }

    public String getDescription() {
        return getAsString("description");
    }

    public String getFireDate() {
        return getAsString("fireDate");
    }

    public String getIconUrl() {
        return getAsString("icon_url");
    }

    public String getId() {
        return getAsString("id");
    }

    public String getIsDismissed() {
        return getAsString("isDismissed");
    }

    public String getIsSeen() {
        return getAsString("isSeen");
    }

    // HACK!!!! json key has changed, is on lane 1 as of 2016/08/31  lane2 unk
/*    public String getIsMobile() {
        if (General.getLogin().isLane1())
            return getAsString("isMobile");
        else
            return getAsString("is_mobile");
    }
*/
    public String getIsPortal() {
        return getAsString("is_portal");
    }

    public String getPriority() {
        return getAsString("priority");
    }

    public String getTitle() {
        return getAsString("title");
    }

    public String getUsername() {
        return getAsString("username");
    }

    public void checkAllKeys() {
        getActionUrl();
        getCategory();
        getCreationDate();
        getDateDismissed();
        getDateSeen();
        getDescription();
        getFireDate();
        getIconUrl();
        getId();
        getIsDismissed();
        getIsSeen();
//        getIsMobile();
        getIsPortal();
        getPriority();
        getTitle();
        getUsername();
    }
}

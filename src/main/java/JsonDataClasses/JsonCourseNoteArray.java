
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 1/25/2016.
 */

public class JsonCourseNoteArray extends JsonData implements Iterable<JsonCourseNote>, Iterator<JsonCourseNote> {

    public JsonCourseNoteArray(String jsonResult) {
        super(jsonResult, "note");
        loadObject(jsonResult);
    }
    public JsonCourseNoteArray(JsonArray array) {
        super(array);
    }

    public JsonCourseNote getNote(int index) {
        return new JsonCourseNote(jsonArray.get(index).getAsJsonObject());
    }

    public JsonCourseNoteArray getAllCourseNotes() {
        return new JsonCourseNoteArray(jsonArray);
    }

    // 2016/09/21 note array is now just an array, no keys itself
    public void checkAllKeys() {
//        getNumberOfNotes();
        for (JsonCourseNote resource : getAllCourseNotes())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonCourseNote next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonCourseNote(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonCourseNote> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}

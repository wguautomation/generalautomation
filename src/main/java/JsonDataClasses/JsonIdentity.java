package JsonDataClasses;

/*
 * Created by timothy.hallbeck on 8/1/2016.
 */

import com.google.gson.JsonObject;

public class JsonIdentity extends JsonData {

    public JsonIdentity() {
        super();
    }
    public JsonIdentity(String jsonResult) {
        super(jsonResult);
    }
    public JsonIdentity(JsonObject object) {
        jsonObject = object;
    }

    public String getUsername() {
        return getAsString("username");
    }

    public String getPidm() {
        return getAsString("pidm");
    }

    public String getBannerId() {
        return getAsString("bannerId");
    }

    public String getPersonType() {
        return getAsString("personType");
    }

    public String getIsEmployee() {
        return getAsString("isEmployee");
    }

    public String getRoles() {
        return getAsString("roles");
    }

    public String getPersonRoleType() {
        return getAsString("personRoleType");
    }

    public void checkAllKeys() {
        getUsername();
        getPidm();
        getBannerId();
        getPersonType();
        getIsEmployee();
        getRoles();
        getPersonRoleType();
    }
}

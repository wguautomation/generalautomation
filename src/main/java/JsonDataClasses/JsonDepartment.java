
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/12/2015.
 */

public class JsonDepartment extends JsonData {

    public JsonDepartment(){}
    public JsonDepartment(String jsonResult) {
        super(jsonResult);
    }
    public JsonDepartment(JsonObject object) {
        jsonObject = object;
    }

    public String getName() {
        return getAsString("name");
    }

    public String getEmail() {
        return getAsString("email");
    }

    public String getPhone() {
        return getAsString("phone");
    }

    public String getPhoneDialable() {
        return getAsString("phoneDialable");
    }

    public String getPosition() {
        return getAsString("position");
    }

    public void checkAllKeys() {
        getName();
        getEmail();
        getPhone();
        getPhoneDialable();
        getPosition();
    }

}

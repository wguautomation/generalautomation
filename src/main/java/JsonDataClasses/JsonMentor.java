
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 10/27/2015.
 */

public class JsonMentor extends JsonData {

    public JsonMentor() {
        super();
    }
    public JsonMentor(String jsonResult) {
        super(jsonResult);
    }
    public JsonMentor(JsonObject object) {
        jsonObject = object;
    }

    public String getFirstName() {
        return getAsString("firstName");
    }

    public String getLastName() {
        return getAsString("lastName");
    }

    public String getEmailAddress() {
        return getAsString("emailAddress");
    }

    public String getPhoneNumber() {
        return getAsString("phoneNumber");
    }

    public String getOfficeHours() {
        return getAsString("officeHours");
    }

    public String getCourseTitle() {
        return getAsString("courseTitle");
    }

    public String getBio() {
        return getAsString("bio");
    }

    public String getType() {
        return getAsString("type");
    }

    public String getCourseCode() {
        return getAsString("courseCode");
    }

    public String getPidm() {
        return getAsString("pidm");
    }

    // no longer present
//    public String getScheduleMeURL() {
//        return getAsString("scheduleMeURL");
//    }

    public void checkAllKeys() {
        getFirstName();
        getLastName();
        getEmailAddress();
        getPhoneNumber();
        getOfficeHours();
        getCourseTitle();
        getBio();
        getType();
        getCourseCode();
        getPidm();
//        getScheduleMeURL();
    }
}

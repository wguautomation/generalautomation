
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 9/26/2016.
 */

public class JsonActivityArray extends JsonData implements Iterable<JsonActivity>, Iterator<JsonActivity> {

    public JsonActivityArray() {}
    public JsonActivityArray(String jsonResult) {
        super(jsonResult, "");
    }
    public JsonActivityArray(JsonArray array) {
        super(array);
    }

    public JsonActivityArray getAllActivities() {
        return new JsonActivityArray(jsonArray);
    }

    public JsonActivity getActivity(int index) {
        return new JsonActivity(jsonArray.get(index).getAsJsonObject());
    }

    public void checkAllKeys() {
        for (JsonActivity resource : getAllActivities())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonActivity next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonActivity(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonActivity> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }

}
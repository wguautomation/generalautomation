
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 10/29/2015.
 */

public class JsonTermArray extends JsonData implements Iterable<JsonTerm>, Iterator<JsonTerm> {

    public JsonTermArray(JsonObject object) {
        super(object, "terms");
        setObject(object);
    }
    public JsonTermArray(JsonArray array) {
        super(array);
    }

    public JsonTermArray getAllTerms() {
        return new JsonTermArray(jsonArray);
    }

    public JsonTerm getTermByCode(String termCode) {
        JsonObject object = null;

        for (int index=0; index<jsonArray.size(); index++) {
            object = jsonArray.get(index).getAsJsonObject();
            if (object.get("termCode").toString().contains(termCode))
                break;
        }
        return new JsonTerm(object);
    }

    public JsonTerm getTerm(int index) {
        return new JsonTerm(jsonArray.get(index).getAsJsonObject());
    }

    public void checkAllKeys() {
        for (JsonTerm resource : getAllTerms())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonTerm next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonTerm(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonTerm> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }

}

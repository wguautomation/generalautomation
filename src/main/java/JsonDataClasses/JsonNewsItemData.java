
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/10/2015.
 */

// The data available depends on the newsitem type. Don't know if just 2 types or more, will subclass later. maybe.
public class JsonNewsItemData extends JsonData {

    String type = "";

    public JsonNewsItemData(JsonObject object, String type) {
        jsonObject = object;
        this.type = type;
    }

    public String getNewsItemDataActivityDate() {
        return getAsString("activityDate");
    }

    public String getAssessmentCode() {
        return getAsString("assessmentCode");
    }

    // type = "TASK_STREAM" only
    public String getNewsItemDataId() {
        return getAsString("id");
    }

    public String getTaskId() {
        return getAsString("taskId");
    }

    public String getStatus() {
        return getAsString("status");
    }

    // type = "ASSESSMENT_ATTEMPT" only
    public String getPidm() {
        return getAsString("pidm");
    }

    public String getCourseId() {
        return getAsString("courseId");
    }

    public String getScheduledTimezone() {
        return getAsString("scheduledTimezone");
    }

    public String getCourseTitle() {
        return getAsString("courseTitle");
    }

    public String getAssessmentId() {
        return getAsString("assessmentId");
    }

    public String getScheduledTime() {
        return getAsString("scheduledTime");
    }

    public String getStatusDate() {
        return getAsString("statusDate");
    }

    public String getCourseCode() {
        return getAsString("courseCode");
    }

    public String getAssessmentStatus() {
        return getAsString("assessmentStatus");
    }

    public String getAssessmentTitle() {
        return getAsString("assessmentTitle");
    }

    public void checkAllKeys() {
        getNewsItemDataActivityDate();
        getAssessmentCode();

        if (type.contentEquals("TASK_STREAM")) {
            getNewsItemDataId();
            getTaskId();
            getStatus();
        } else { // type = "ASSESSMENT_ATTEMPT"
            getPidm();
            getCourseId();
            getScheduledTimezone();
            getCourseTitle();
            getAssessmentId();
            getScheduledTime();
            getStatusDate();
            getCourseCode();
            getAssessmentStatus();
            getAssessmentTitle();
        }
    }

}

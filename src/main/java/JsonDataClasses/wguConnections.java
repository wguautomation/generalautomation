
package JsonDataClasses;

import BaseClasses.LoginType;
import Utils.MasherySession;
import org.testng.TestNGException;

/*
 * Created by timothy.hallbeck on 4/5/2016.
 */

public class wguConnections extends JsonConnectionArray {

    private LoginType loginType;

    public wguConnections(LoginType loginType) {
        super();
        this.loginType = loginType;
        load();
    };

    public wguConnections load() {
        String url;
        switch (loginType.getEnv()) {
            case Lane2Dev:
                url = "https://mashery.wgu.edu/dev/wsbsi/v1/students/" + loginType.getPidm() + "/connections";
                break;
            case Lane1Qa:
                url = "https://mashery.wgu.edu/qa/wsbsi/v1/students/" + loginType.getPidm() + "/connections";
                break;
            case Prod:
                url = "https://mashery.wgu.edu/wsbsi/v1/students/" + loginType.getPidm() + "/connections";
                break;
            default:
                throw new TestNGException("Undefined environment in wguConnections::load()");
        }

        String result = new MasherySession().Get(url, loginType);
        super.loadArray(result, "");

        return this;
    }

}

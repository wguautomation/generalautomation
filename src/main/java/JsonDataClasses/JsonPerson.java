
package JsonDataClasses;

import Utils.General;

/*
 * Created by timothy.hallbeck on 10/27/2015.
 */

public class JsonPerson extends JsonData {

    public JsonPerson() {
        super();
    }
    public JsonPerson(String jsonResult) {
        super(jsonResult);
    }

    public String getPidm() {
        return getAsString("pidm");
    }

    public String getFirstName() {
        return getAsString("firstName");
    }

    public String getMiddleName() {
        return getAsString("middleName");
    }

    public String getLastName() {
        return getAsString("lastName");
    }

    public String getPersonType() {
        return getAsString("personType");
    }

    public String getStreetLine1() {
        return getAsString("streetLine1");
    }

    public String getStreetLine2() {
        return getAsString("streetLine2");
    }

    public String getCity() {
        return getAsString("city");
    }

    public String getState() {
        return getAsString("state");
    }

    public String getZipCode() {
        return getAsString("zipCode");
    }

    public String getPrimaryPhone() {
        return getAsString("primaryPhone");
    }

    public String getMobilePhone() {
        return getAsString("mobilePhone");
    }

    public String getIsEmployee() {
        return getAsString("isEmployee");
    }

    // No data available currently. An anonymous array but nothing there.
    public String getAllRoles() {
        return null;
    }

    public String getPersonRoleType() {
        return getAsString("personRoleType");
    }

    public String getUsername() {
        return getAsString("username");
    }

    public String getStudentId() {
        return getAsString("studentId");
    }

    public String getPreferredEmail() {
        return getAsString("preferredEmail");
    }

    public String getValue(String key) {
        switch (key) {
            case "pidm":
                return getPidm();
            case "studentId":
                return getStudentId();
            default:
                General.Debug("Unknown key request in JsonPerson::getValue(" + key + ")");
        }
        return "";
    }

    public void checkAllKeys() {
        getPidm();
        getFirstName();
        getMiddleName();
        getLastName();
        getPersonType();
        getStreetLine1();
        getStreetLine2();
        getCity();
        getState();
        getZipCode();
        getPrimaryPhone();
        getMobilePhone();
        getIsEmployee();
        getAllRoles();
        getPersonRoleType();
        getUsername();
        getStudentId();
        getPreferredEmail();
    }
}


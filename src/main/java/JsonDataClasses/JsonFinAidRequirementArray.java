
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 10/5/2016.
 */

public class JsonFinAidRequirementArray extends JsonData implements Iterable<JsonFinAidRequirement>, Iterator<JsonFinAidRequirement> {

    public JsonFinAidRequirementArray(String jsonResult) {
        super(jsonResult, "requirements");
    }
    public JsonFinAidRequirementArray(JsonArray array) {
        super(array);
    }

    public JsonFinAidRequirement getRequirement(int index) {
        return new JsonFinAidRequirement(jsonArray.get(index).getAsJsonObject());
    }

    public JsonFinAidRequirementArray getAllFinAidRequirements() {
        return new JsonFinAidRequirementArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonFinAidRequirement resource : getAllFinAidRequirements())
            resource.checkAllKeys();
    }

    @Override
    public String toString() {
        String output = "Requirements\n";
        for (JsonFinAidRequirement req : getAllFinAidRequirements())
            output += req.toString();

        return output;
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonFinAidRequirement next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonFinAidRequirement(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonFinAidRequirement> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}

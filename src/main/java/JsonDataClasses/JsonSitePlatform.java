
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/13/2015.
 */

public class JsonSitePlatform extends JsonData {

    public JsonSitePlatform(String jsonResult) {
        super(jsonResult);
    }
    public JsonSitePlatform(JsonObject object) {
        jsonObject = object;
    }

    public String getSitePlatformId() {
        return getAsString("id");
    }

    public String getDescription() {
        return getAsString("description");
    }

    public String getCode() {
        return getAsString("code");
    }

    public String getAndroidPackage() {
        return getAsString("androidPackage");
    }

    public void checkAllKeys() {
        getSitePlatformId();
        getDescription();
        getCode();
        getAndroidPackage();
    }

}


package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 11/12/2015.
 */

public class JsonLinkRatingArray extends JsonData implements Iterable<JsonLinkRating>, Iterator<JsonLinkRating> {

    public JsonLinkRatingArray(String jsonResult) {
        super(jsonResult, "linkRating");
    }
    public JsonLinkRatingArray(JsonObject object) {
        super(object, "linkRating");
    }
    public JsonLinkRatingArray(JsonArray array) {
        super(array);
    }

    public JsonLinkRating getLinkRating(int index) {
        return new JsonLinkRating(jsonArray.get(index).getAsJsonObject());
    }

    public JsonLinkRatingArray getAllLinkRatings() {
        return new JsonLinkRatingArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonLinkRating resource : getAllLinkRatings())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonLinkRating next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonLinkRating(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonLinkRating> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}

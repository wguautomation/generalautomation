
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 11/3/2015.
 */

public class JsonCourseResourceArray extends JsonData implements Iterable<JsonCourseResource>, Iterator<JsonCourseResource> {

    public JsonCourseResourceArray(String jsonResult, String type) {
        super(jsonResult, type);
    }
    public JsonCourseResourceArray(JsonObject object, String type) {
        super(object, type);
    }
    public JsonCourseResourceArray(JsonObject object) {
        super(object);
    }
    public JsonCourseResourceArray(JsonArray array) {
        super(array);
    }

    public JsonCourseResource getResource(int index) {
        return new JsonCourseResource(jsonArray.get(index).getAsJsonObject());
    }

    public JsonCourseResourceArray getAllCourseResources() {
        return new JsonCourseResourceArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonCourseResource item : getAllCourseResources())
            item.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonCourseResource next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonCourseResource(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonCourseResource> iterator() {
        size   = jsonArray.size();
        cursor = 0;

        return this;
    }
}

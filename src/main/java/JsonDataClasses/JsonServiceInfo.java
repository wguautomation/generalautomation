
package JsonDataClasses;

/*
 * Created by timothy.hallbeck on 9/27/2016.
 */

import Utils.General;

public class JsonServiceInfo extends JsonData {

    public JsonServiceInfo() {}
    public JsonServiceInfo(String jsonResult) {
        load(jsonResult);
    }

    public JsonServiceInfo load(String jsonResult) {
        if (!jsonResult.isEmpty() && jsonResult.contains("build")) {
            loadNestedObject(jsonResult, "build");
            General.Debug(toString());
        }
        return this;
    }

    public String getArtifact() {
        try {
            return getAsString("artifact");
        } catch (Exception | AssertionError e) {
            return "";
        }
    }

    public String getVersion() {
        try {
            return getAsString("version");
        } catch (Exception | AssertionError e) {
            return "";
        }
    }

    public void checkAllKeys() {
        getArtifact();
        getVersion();
    }

    @Override
    public String toString() {
        String string = "";

        try {
            string = "\nArtifact name:  " + getArtifact() + "\n";
            string += "Version:         " + getVersion();
        } catch (Exception e) {
            ;
        }

        return string;
    }

}


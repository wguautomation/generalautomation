
package JsonDataClasses;

import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 11/12/2015.
 */

public class JsonOffice extends JsonData implements Iterable<JsonDepartment>, Iterator<JsonDepartment> {

    // jsonObject has everything. jsonArray we use as a shortcut with just the array items.
    public JsonOffice(JsonObject object) {
        super(object);
        loadArray(object, "departments");
    }

    public JsonDepartment getDepartment(int index) {
        return new JsonDepartment(jsonArray.get(index).getAsJsonObject());
    }

    public String getName() {
        return getAsString("name");
    }

    public String getPosition() {
        return getAsString("position");
    }

    public void checkAllKeys() {
        getName();
        getPosition();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonDepartment next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonDepartment(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonDepartment> iterator() {
        size      = jsonArray.size();
        cursor    = 0;

        return this;
    }
}


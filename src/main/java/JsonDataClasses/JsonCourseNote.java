
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/12/2015.
 */

public class JsonCourseNote extends JsonData {

    public JsonCourseNote(String jsonResult) {
        super(jsonResult);
    }
    public JsonCourseNote(JsonObject object) {
        jsonObject = object;
    }

    public String getUsername() {
        return getAsString("username");
    }

    public String getTopicTitle() {
        return getAsString("topicTitle");
    }

    public String getCourseVersionId() {
        return getAsString("courseVersionId");
    }

    public String getStudyPlanTitle() {
        return getAsString("studyPlanTitle");
    }

    public String getTopicId() {
        return getAsString("topicId");
    }

    public String getLastUpdated() {
        return getAsString("lastUpdated");
    }

    public String getLastUpdatedStr() {
        return getAsString("lastUpdatedStr");
    }

    public String getNoteId() {
        return getAsString("noteId");
    }

    public String getStudyPlanId() {
        return getAsString("studyPlanId");
    }

    public String getNote() {
        return getAsString("note");
    }

    public void checkAllKeys() {
        getUsername();
        getTopicTitle();
        getCourseVersionId();
        getStudyPlanTitle();
        getTopicId();
        getLastUpdated();
        getLastUpdatedStr();
        getNoteId();
        getStudyPlanId();
        getNote();
    }
}

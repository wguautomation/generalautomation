
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/9/2015.
 */

public class JsonTag extends JsonData {

    public JsonTag(String jsonResult) {
        super(jsonResult);
    }
    public JsonTag(JsonObject object) {
        jsonObject = object;
    }

    public String getTagText() {
        return getAsString("tag");
    }

    public void checkAllKeys() {
        getTagText();
    }

}

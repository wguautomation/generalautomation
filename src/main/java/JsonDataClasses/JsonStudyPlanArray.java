
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 11/9/2015.
 */

public class JsonStudyPlanArray extends JsonData implements Iterable<JsonStudyPlan>, Iterator<JsonStudyPlan> {

    public JsonStudyPlanArray() {
        super();
    }
    public JsonStudyPlanArray(String jsonResult) {
        super(jsonResult, "studyPlan");
    }
    public JsonStudyPlanArray(JsonObject object) {
        super(object, "studyPlan");
    }
    public JsonStudyPlanArray(JsonArray array) {
        super(array);
    }

    public JsonStudyPlan getStudyPlan(int index) {
        return new JsonStudyPlan(jsonArray.get(index).getAsJsonObject());
    }

    public JsonStudyPlan getStudyPlanById(String tag) {
        JsonObject object = null;

        for (int index=0; index<jsonArray.size(); index++) {
            object = jsonArray.get(index).getAsJsonObject();
            if (object.get("studyPlanId").toString().contains(tag))
                break;
        }

        return new JsonStudyPlan(object);
    }

    public JsonStudyPlanArray getAllStudyPlans() {
        return new JsonStudyPlanArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonStudyPlan resource : getAllStudyPlans())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonStudyPlan next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonStudyPlan(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonStudyPlan> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}



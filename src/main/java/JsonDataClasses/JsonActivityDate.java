
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/10/2015.
 */

public class JsonActivityDate extends JsonData {

    public JsonActivityDate(String jsonResult) {
        super(jsonResult);
    }
    public JsonActivityDate(JsonObject object) {
        jsonObject = object;
    }

    public JsonObject getOffset() {
        return getAsObject("offset");
    }

    public JsonObject getZone() {
        return getAsObject("zone");
    }

    public String getHour() {
        return getAsString("hour");
    }

    public String getMinute() {
        return getAsString("minute");
    }

    public String getSecond() {
        return getAsString("second");
    }

    public String getNano() {
        return getAsString("nano");
    }

    public String getYear() {
        return getAsString("year");
    }

    public String getMonth() {
        return getAsString("month");
    }

    public String getDayOfMonth() {
        return getAsString("dayOfMonth");
    }

    public String getDayOfWeek() {
        return getAsString("dayOfWeek");
    }

    public String getDayOfYear() {
        return getAsString("dayOfYear");
    }

    public String getMonthValue() {
        return getAsString("monthValue");
    }

    public JsonObject getChronology() {
        return getAsObject("chronology");
    }

    public void checkAllKeys() {
        getOffset();
        getZone();
        getHour();
        getMinute();
        getSecond();
        getNano();
        getYear();
        getMonth();
        getDayOfMonth();
        getDayOfWeek();
        getDayOfYear();
        getMonthValue();
        getChronology();
    }
}
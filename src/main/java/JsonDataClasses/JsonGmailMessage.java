
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 4/4/2016.
 */

public class JsonGmailMessage extends JsonData {

    public JsonGmailMessage(String jsonResult) {
        super(jsonResult);
    }
    public JsonGmailMessage(JsonObject object) {
        jsonObject = object;
    }

    public String getFrom() {
        return getAsString("messageFrom");
    }

    public String getFromAbbr() {
        return getAsString("messageFromAbbreviated");
    }

    public String getSubject() {
        return getAsString("subject");
    }

    public String getSubjectAbbr() {
        return getAsString("subjectAbbreviated");
    }

    public String getBody() {
        return getAsString("shortBody");
    }

    public String getBodyAbbr() {
        return getAsString("shortBodyAbbreviated");
    }

    public String getMessageLink() {
        return getAsString("messageLink");
    }

    public String getPublishedDate() {
        return getAsString("publishedDate");
    }

    public String getPublishedDateDisplay() {
        return getAsString("publishedDateDisplay");
    }

    public String getPublishedDateFull() {
        return getAsString("publishedDateFull");
    }

    public void checkAllKeys() {
        getFrom();
        getFromAbbr();
        getSubject();
        getSubjectAbbr();
        getBody();
        getBodyAbbr();
        getMessageLink();
        getPublishedDate();
        getPublishedDateDisplay();
        getPublishedDateFull();
    }

}

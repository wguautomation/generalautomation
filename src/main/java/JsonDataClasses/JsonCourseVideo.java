
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/10/2015.
 */

public class JsonCourseVideo extends JsonData {

    public JsonCourseVideo(String jsonResult) {
        super(jsonResult);
    }
    public JsonCourseVideo(JsonObject object) {
        jsonObject = object;
    }

    public String getUrl() {
        return getAsString("url");
    }

    public String getType() {
        return getAsString("type");
    }

    public String getVideoId() {
        return getAsString("id");
    }

    public void checkAllKeys() {
        getUrl();
        getType();
        getVideoId();
   }
}

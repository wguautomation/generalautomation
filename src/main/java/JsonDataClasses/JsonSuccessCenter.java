
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 4/4/2016.
 */

public class JsonSuccessCenter extends JsonData {

    public JsonSuccessCenter() {
        super();
    }
    public JsonSuccessCenter(String jsonResult) {
        super(jsonResult);
    }
    public JsonSuccessCenter(JsonObject object) {
        jsonObject = object;
    }

    public String getTitle() {
        return getAsString("title");
    }

    public String getCenterUrl() {
        return getAsString("centerurl");
    }

    public String getMobileCenterUrl() {
        return getAsString("mobilecenterurl");
    }

    public String getIconUrl() {
        return getAsString("iconurl");
    }

    public String getImageUrl() {
        return getAsString("imageurl");
    }

    public String getMobileIconUrl() {
        return getAsString("mobileiconurl");
    }

    public String getFullImageUrl() {
        return getAsString("fullimageurl");
    }

    public String getShortDesc() {
        return getAsString("shortdesc");
    }

    public String getSortOrder() {
        return getAsString("sortorder");
    }

    public String getPidm() {
        return getAsString("pidm");
    }

    public void checkAllKeys() {
        getTitle();
        getCenterUrl();
        getMobileCenterUrl();
        getIconUrl();
        getImageUrl();
        getMobileIconUrl();
        getFullImageUrl();
        getShortDesc();
        getSortOrder();
        getPidm();
    }

}


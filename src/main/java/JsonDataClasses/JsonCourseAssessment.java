
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/9/2015.
 */

public class JsonCourseAssessment extends JsonData {

    public JsonCourseAssessment(String jsonResult) {
        super(jsonResult);
    }
    public JsonCourseAssessment(JsonObject object) {
        jsonObject = object;
    }

    public String getCanRequestApproval() {
        return getAsString("canRequestApproval");
    }

    public JsonAssessmentArray getAllAssessments() {
        return new JsonAssessmentArray(jsonObject);
    }

    public JsonAssessment getAssessment(String courseCode) {
        return new JsonAssessmentArray(jsonObject).getAssessmentByCourseCode(courseCode);
    }

    public JsonAssessment getAssessment(int index) {
        return getAllAssessments().getAssessment(index);
    }

    public void checkAllKeys() {
        getCanRequestApproval();
        for (JsonAssessment assessment : getAllAssessments())
            assessment.checkAllKeys();
   }


}
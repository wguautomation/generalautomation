
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 11/9/2015.
 */

public class JsonNewsItemArray extends JsonData implements Iterable<JsonNewsItem>, Iterator<JsonNewsItem> {

    public JsonNewsItemArray(){}
    public JsonNewsItemArray(String jsonResult) {
        super(jsonResult, "newsItems");
    }
    public JsonNewsItemArray(JsonObject object) {
        super(object, "newsItems");
        setObject(object);
    }
    public JsonNewsItemArray(JsonArray array) {
        super(array);
    }

    // Returns the first one that matches
    public JsonNewsItem getNewsItemByType(String type) {
        JsonObject object = null;

        for (int index = 0; index < jsonArray.size(); index++) {
            object = jsonArray.get(index).getAsJsonObject();
            if (object.get("type").toString().contains(type))
                break;
        }
        return new JsonNewsItem(object);
    }

    public JsonNewsItemArray getAllNewsItems() {
        return new JsonNewsItemArray(jsonArray);
    }

    public JsonNewsItem getNewsItem(int index) {
        return new JsonNewsItem(jsonArray.get(index).getAsJsonObject());
    }

    public void checkAllKeys() {
        for (JsonNewsItem resource : getAllNewsItems())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonNewsItem next() {
        if (this.hasNext()) {
            int current = cursor;
            cursor++;

            return new JsonNewsItem(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonNewsItem> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }

}

package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/9/2015.
 */

public class JsonAssessment extends JsonData {

    public JsonAssessment(String jsonResult) {
        super(jsonResult);
    }
    public JsonAssessment(JsonObject object) {
        jsonObject = object;
    }

    public String getCourseId() {
        return getAsString("courseId");
    }

    public String getCourseCode() {
        return getAsString("courseCode");
    }

    public String getCourseVersion() {
        return getAsString("courseVersion");
    }

    public String getCourseVersionId() {
        return getAsString("courseVersionId");
    }

    public String getNumberOfItems() {
        return getAsString("numberOfItems");
    }

    public String getTimeAllotedInMinutes() {
        return getAsString("timeAllotedInMinutes");
    }

    public String getCutScorePercentage() {
        return getAsString("cutScorePercentage");
    }

    public String getCutScoreRawPoints() {
        return getAsString("cutScoreRawPoints");
    }

    public String getHistory() {
        return getAsString("history");
    }

    public String getPreAssessmentUrl() {
        return getAsString("preAssessmentUrl");
    }

    public String getAssessmentId() {
        return getAsString("assessmentId");
    }

    public String getAssessmentCode() {
        return getAsString("assessmentCode");
    }

    public String getAssessmentTitle() {
        return getAsString("assessmentTitle");
    }

    public String getIsPreAssessment() {
        return getAsString("isPreAssessment");
    }

    public String getHasPreAssessment() {
        return getAsString("hasPreAssessment");
    }

    public String getAssessmentTypeCode() {
        return getAsString("assessmentTypeCode");
    }

    public String getAssessmentType() {
        return getAsString("assessmentType");
    }

    public String getAssessmentSubType() {
        return getAsString("assessmentSubType");
    }

    public String getAssessmentSubTypeCode() {
        return getAsString("assessmentSubTypeCode");
    }

    public String getPreAssessmentId() {
        return getAsString("preAssessmentId");
    }

    public String getAssessmentOrder() {
        return getAsString("assessmentOrder");
    }

    public void checkAllKeys() {
        getCourseId();
        getCourseCode();
        getCourseVersion();
        getCourseVersionId();
        getNumberOfItems();
        getTimeAllotedInMinutes();
        getCutScorePercentage();
        getCutScoreRawPoints();
        getHistory();
        getPreAssessmentUrl();
        getAssessmentId();
        getAssessmentCode();
        getAssessmentTitle();
        getIsPreAssessment();
        getHasPreAssessment();
        getAssessmentTypeCode();
        getAssessmentType();
        getAssessmentSubType();
        getAssessmentSubTypeCode();
        getPreAssessmentId();
        getAssessmentOrder();
    }
}

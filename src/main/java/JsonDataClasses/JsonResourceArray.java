
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 11/12/2015.
 */

public class JsonResourceArray extends JsonData implements Iterable<JsonResource>, Iterator<JsonResource> {

    public JsonResourceArray(String jsonResult) {
        super(jsonResult, "resourceUrls");
    }
    public JsonResourceArray(JsonObject object) {
        super(object, "resourceUrls");
    }
    public JsonResourceArray(JsonArray array) {
        super(array);
    }

    public JsonResource getResource(int index) {
        return new JsonResource(jsonArray.get(index).getAsJsonObject());
    }

    public JsonResourceArray getAllResources() {
        return new JsonResourceArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonResource resource : getAllResources())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonResource next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonResource(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonResource> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}


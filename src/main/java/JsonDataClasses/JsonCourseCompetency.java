
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/3/2015.
 */

public class JsonCourseCompetency extends JsonData {

    public JsonCourseCompetency(String jsonResult) {
        super(jsonResult);
    }
    public JsonCourseCompetency(JsonObject object) {
        jsonObject = object;
    }

    public String getId() {
        return getAsString("id");
    }

    public String getDomainId() {
        return getAsString("domainId");
    }

    public String getSubDomainId() {
        return getAsString("subDomainId");
    }

    public String getParentCode() {
        return getAsString("parentCode");
    }

    public String getDescription() {
        return getAsString("description");
    }

    public String getTitle() {
        return getAsString("title");
    }

    public void checkAllKeys() {
        getId();
        getDomainId();
        getSubDomainId();
        getParentCode();
        getDescription();
        getTitle();
    }

}

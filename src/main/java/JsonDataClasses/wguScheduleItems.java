
package JsonDataClasses;

import BaseClasses.LoginType;
import Utils.OssoSession;
import org.testng.TestNGException;

/*
 * Created by timothy.hallbeck on 4/6/2016.
 */

public class wguScheduleItems extends JsonScheduleItems {

    private LoginType loginType;

    public wguScheduleItems(LoginType loginType) {
        super();
        this.loginType = loginType;
        load();
    };

    public wguScheduleItems load() {
        String url;
        switch (loginType.getEnv()) {
            case Lane2Dev:
                url = "https://student-schedule.dev.wgu.edu/v1/students/" + loginType.getPidm() + "/schedule/items";
                break;
            case Lane1Qa:
                url = "https://student-schedule.qa.wgu.edu/v1/students/" + loginType.getPidm() + "/schedule/items";
                break;
            case Prod:
                url = "https://student-schedule.wgu.edu/v1/students/" + loginType.getPidm() + "/schedule/items";
                break;
            default:
                throw new TestNGException("Undefined environment in wguConnections::load()");
        }

        String result = new OssoSession().Get(url, loginType);
        super.loadObject(result);
        super.loadArray(result, "items");

        return this;
    }

}

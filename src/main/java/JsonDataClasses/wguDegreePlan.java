
package JsonDataClasses;

import BaseClasses.LoginType;
import Utils.OssoSession;
import org.testng.TestNGException;

/*
 * Created by timothy.hallbeck on 3/15/2016.
 */

public class wguDegreePlan extends JsonDegreePlan {

    private LoginType loginType;

    public wguDegreePlan(LoginType loginType) {
        super();
        this.loginType = loginType;
        load();
    }

    public wguDegreePlan load() {
        String url;
        switch (loginType.getEnv()) {
            case Lane2Dev:
                url = "https://fdp-degreeplan-api.dev.wgu.edu/v1/api/degreeplans/current/term/pidm/" + loginType.getPidm();
                break;
            case Lane1Qa:
                url = "https://fdp-degreeplan-api.qa.wgu.edu/v1/api/degreeplans/current/term/pidm/" + loginType.getPidm();
                break;
            case Prod:
                url = "https://fdp-degreeplan-api.wgu.edu/v1/api/degreeplans/current/term/pidm/" + loginType.getPidm();
                break;
            default:
                throw new TestNGException("Undefined environment in wguMentorInfo::load()");
        }

        String result = new OssoSession().Get(url, loginType);
        loadObject(result);

        return this;
    }
}



package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/12/2015.
 */

public class JsonSite extends JsonData {

    public JsonSite(String jsonResult) {
        super(jsonResult);
    }
    public JsonSite(JsonObject object) {
        jsonObject = object;
    }

    public String getSiteId() {
        return getAsString("id");
    }

    public String getUrl() {
        return getAsString("url");
    }

    public String getDescription() {
        return getAsString("description");
    }

    public JsonSitePlatform getPlatform() {
        return new JsonSitePlatform(jsonObject.getAsJsonObject("platform"));
    }

    public String getAvgScore() {
        return getAsString("avgScore");
    }

    public JsonSiteState getSocialSiteState() {
        return new JsonSiteState(jsonObject.getAsJsonObject("socialSiteState"));
    }

    public String getUrlScheme() {
        return getAsString("urlScheme");
    }

    public void checkAllKeys() {
        getSiteId();
        getUrl();
        getDescription();
        getPlatform();
        getAvgScore();
        getSocialSiteState();
        getUrlScheme();
    }

}


package JsonDataClasses;

import Utils.wguStringUtils;
import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/10/2015.
 */

public class JsonStudyPlanCourseInfo extends JsonData {

    public JsonStudyPlanCourseInfo(String jsonResult) {
        super(jsonResult);
    }
    public JsonStudyPlanCourseInfo(JsonObject object) {
        jsonObject = object;
    }

    public String getId() {
        return getAsString("id");
    }

    public String getVersion() {
        return getAsString("version");
    }

    public String getCode() {
        return getAsString("code");
    }

    public String getSubjectLess() {
        return getAsString("subjectLess");
    }

    public String getIntroduction() {
        return getAsString("introduction");
    }

    public String getDescription() {
        return getAsString("description");
    }

    public String getStudyPlanid() {
        return getAsString("studyPlanid");
    }

    public String getLastPublishedDate() {
        return getAsString("lastPublishedDate");
    }

    public String getRunTime() {
        return getAsString("runTime");
    }

    public JsonStudyPlanSubjectArray getAllSubjects() {
        return new JsonStudyPlanSubjectArray(jsonObject);
    }

    public void checkAllKeys() {
        getId();
        getVersion();
        getCode();
        getSubjectLess();
        getIntroduction();
        getDescription();
        getStudyPlanid();
        getLastPublishedDate();
        getRunTime();
        for (JsonStudyPlanSubject resource : getAllSubjects())
            resource.checkAllKeys();
    }

    public String prettyString(int indention) {
        String prettyString = "";

        prettyString += wguStringUtils.indent(indention) + "\n                 Course\n";
        prettyString += wguStringUtils.indent(indention) + "               id : " + getId() + "\n";
        prettyString += wguStringUtils.indent(indention) + "          version : " + getVersion() + "\n";
        prettyString += wguStringUtils.indent(indention) + "             code : " + getCode() + "\n";
        prettyString += wguStringUtils.indent(indention) + "      subjectLess : " + getSubjectLess() + "\n";
        prettyString += wguStringUtils.indent(indention) + "     introduction : " + wguStringUtils.ellisize(wguStringUtils.removeNewlines(getIntroduction()), 90) + "\n";
        prettyString += wguStringUtils.indent(indention) + "      description : " + wguStringUtils.ellisize(wguStringUtils.removeNewlines(getDescription()), 90) + "\n";
        prettyString += wguStringUtils.indent(indention) + "      studyPlanid : " + getStudyPlanid() + "\n";
        prettyString += wguStringUtils.indent(indention) + "lastPublishedDate : " + getLastPublishedDate() + "\n";
        prettyString += wguStringUtils.indent(indention) + "          runTime : " + getRunTime() + "\n";

        return prettyString;
    }
}


package JsonDataClasses;

import Utils.General;
import com.google.gson.JsonElement;
import org.testng.TestNGException;
import java.util.*;

/*
 * Created by timothy.hallbeck on 7/12/2016.
 */

public class JsonServiceMappings extends JsonData {

    public ArrayList<String> Gets      = new ArrayList<>();
    public ArrayList<String> Puts      = new ArrayList<>();
    public ArrayList<String> Posts     = new ArrayList<>();
    public ArrayList<String> Deletes   = new ArrayList<>();
    public ArrayList<String> Commands  = new ArrayList<>();
    public ArrayList<String> NotTagged = new ArrayList<>();

    private ArrayList<String> commandList = new ArrayList<String>(Arrays.asList(
            "/archaius",
            "/autoconfig",
            "/beans",
            "/configprops",
            "/configuration/security",
            "/configuration/ui",
            "/dump",
            "/env",
            "/env/{name:.*}",
            "/env/reset",
            "/error",
            "/features",
            "/health",
            "/info",
            "/mappings",
            "/metrics/{name:.*}",
            "/metrics",
            "/pause",
            "/refresh",
            "/restart",
            "/resume",
            "/trace",
            "/swagger-resources",
            "/v2/api-docs"
    ));

    public JsonServiceMappings() {}
    public JsonServiceMappings(String jsonResult) {
        load(jsonResult, true);
    }
    public JsonServiceMappings(String jsonResult, boolean bIgnoreSystemCommands) {
        load(jsonResult, bIgnoreSystemCommands);
    }
    public void load(String jsonResult, boolean bIgnoreSystemCommands) {
        jsonResult = jsonResult.replace("[]", "()");
        loadObject(jsonResult);
        extractGetsPutsPostsDeletes(bIgnoreSystemCommands);
    }

    protected void extractGetsPutsPostsDeletes(boolean bIgnoreSystemCommands) {
        String  string = "";
        boolean isValid, isGet, isPut, isPost, isDelete;
        Set<Map.Entry<String, JsonElement>> set = jsonObject.entrySet();

        for (Map.Entry entry : set) {
            string = ((String) entry.getKey());

            isValid  = string.contains("{[/");
            isGet    = string.contains(",methods=[GET]");
            isPut    = string.contains(",methods=[PUT]");
            isPost   = string.contains(",methods=[POST]");
            isDelete = string.contains(",methods=[DELETE]");

            if (isValid) {
                string = string.replace("{[/", "/");

                if (string.indexOf(",") > 0)
                    string = string.substring(0, string.indexOf(","));
                if (string.indexOf(" ") > 0)
                    string = string.substring(0, string.indexOf(" "));
                if (string.indexOf("]") > 0)
                    string = string.substring(0, string.indexOf("]"));

                if (commandList.contains(string)) {
                    if (bIgnoreSystemCommands )
                        break;
                    else
                        Commands.add(string);
                }
                else if (isGet)
                    Gets.add(string);
                else if (isPut)
                    Puts.add(string);
                else if (isPost)
                    Posts.add(string);
                else if (isDelete)
                    Deletes.add(string);
                else
                    NotTagged.add(string);
            }
        }

        if (Gets.size() >0) {
            General.Debug("\n[GET]");
            for (String string2 : Gets)
                General.Debug(string2);
        }

        if (Puts.size() >0) {
            General.Debug("\n[PUT]");
            for (String string2 : Puts)
                General.Debug(string2);
        }

        if (Posts.size() >0) {
            General.Debug("\n[POST]");
            for (String string2 : Posts)
                General.Debug(string2);
        }

        if (Deletes.size() >0) {
            General.Debug("\n[DELETE]");
            for (String string2 : Deletes)
                General.Debug(string2);
        }

        if (NotTagged.size() >0) {
            General.Debug("\n[NOT TAGGED]");
            for (String string2 : NotTagged)
                General.Debug(string2);
        }

        if (Commands.size() >0) {
            General.Debug("\n[COMMANDS]");
            for (String string2 : Commands)
                General.Debug(string2);
        }
        General.Debug("");
    }

    public void verifyGet(String key) {
        General.Debug("Checking method GET " + key);
        if (Gets.contains(key))
            return;
        throw new TestNGException("method GET " + key + " not found");
    }

    public void verifyPut(String key) {
        General.Debug("Checking method PUT " + key);
        if (Puts.contains(key))
            return;
        throw new TestNGException("method PUT " + key + " not found");
    }

    public void verifyPost(String key) {
        General.Debug("Checking method POST " + key);
        if (Posts.contains(key))
            return;
        throw new TestNGException("method POST " + key + " not found");
    }

    public void verifyDelete(String key) {
        General.Debug("Checking method DELETE " + key);
        if (Deletes.contains(key))
            return;
        throw new TestNGException("method DELETE " + key + " not found");
    }

    public void verifyNotTagged(String key) {
        General.Debug("Checking untagged method " + key);
        if (NotTagged.contains(key))
            return;
        throw new TestNGException("Untagged method " + key + " not found");
    }

    public void checkAllKeys() {
    }

    @Override
    public String toString() {
        String string = "";

        if (Gets.size() > 0) {
            string += "\n[GET]\n";
            for (String string2 : Gets)
                string += string2 + "\n";
        }

        if (Puts.size() > 0) {
            string += "\n[PUT]\n";
            for (String string2 : Puts)
                string += string2 + "\n";
        }

        if (Posts.size() > 0) {
            string += "\n[POST]\n";
            for (String string2 : Posts)
                string += string2 + "\n";
        }

        if (Deletes.size() > 0) {
            string += "\n[DELETE]\n";
            for (String string2 : Deletes)
                string += string2 + "\n";
        }

        if (NotTagged.size() > 0) {
            string += "\n[NOT TAGGED]\n";
            for (String string2 : NotTagged)
                string += string2 + "\n";
        }

        if (Commands.size() > 0) {
            string += "\n[COMMANDS]\n";
            for (String string2 : Commands)
                string += string2 + "\n";
        }

        return string;

    }
}

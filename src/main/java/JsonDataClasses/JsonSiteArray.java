
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 11/12/2015.
 */

public class JsonSiteArray extends JsonData implements Iterable<JsonSite>, Iterator<JsonSite> {

    public JsonSiteArray(String jsonResult) {
        super(jsonResult, "");
    }
    public JsonSiteArray(JsonObject object) {
        super(object, "");
    }
    public JsonSiteArray(JsonArray array) {
        super(array);
    }

    public JsonSite getSite(int index) {
        return new JsonSite(jsonArray.get(index).getAsJsonObject());
    }

    public JsonSiteArray getAllSites() {
        return new JsonSiteArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonSite resource : getAllSites())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonSite next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonSite(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonSite> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}




package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/9/2015.
 */

public class JsonFaq extends JsonData {

    public JsonFaq(String jsonResult) {
        super(jsonResult);
    }
    public JsonFaq(JsonObject object) {
        jsonObject = object;
    }

    public String getQuestion() {
        return getAsString("question");
    }

    public String getAnswer() {
        return getAsString("answer");
    }

    public JsonTagArray getAllTags() {
        return new JsonTagArray(jsonObject);
    }

    public JsonTag getTag(int index) {
        return new JsonTagArray(jsonObject).getTag(0);
    }

    public JsonTag getTagByText(String tag) {
        return new JsonTagArray(jsonObject).getTagByText(tag);
    }

    public String getOrder() {
        return getAsString("order");
    }

    public void checkAllKeys() {
        getAnswer();
        getQuestion();
        for (JsonTag resource : getAllTags())
            resource.checkAllKeys();
        getOrder();
    }

}

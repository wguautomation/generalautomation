
package JsonDataClasses;

import com.google.gson.JsonArray;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 1/20/2016.
 */

public class JsonStudentActionItemArray extends JsonData implements Iterable<JsonStudentActionItem>, Iterator<JsonStudentActionItem> {

    public JsonStudentActionItemArray(String jsonResult) {
        super(jsonResult, "");
        invertOrder();
    }
    public JsonStudentActionItemArray(JsonArray array) {
        super(array);
    }

    public JsonStudentActionItemArray getAllActionItems() {
        return new JsonStudentActionItemArray(jsonArray);
    }

    public JsonStudentActionItemArray getActiveActionItems() {
        for (int i=jsonArray.size()-1; i>=0; i--) {
            if (!getActionItem(i).getDateCompleted().isEmpty())
                jsonArray.remove(i);
        }
        return new JsonStudentActionItemArray(jsonArray);
    }

    public JsonStudentActionItem getActionItem(int index) {
        return new JsonStudentActionItem(jsonArray.get(index).getAsJsonObject());
    }

    // Json is in reversed order, compared to what is displayed
    private void invertOrder() {
        JsonArray newArray = new JsonArray();

        for (int i=jsonArray.size()-1; i>0; i--)
            newArray.add(jsonArray.get(i));

        jsonArray = newArray;
    }

    public void checkAllKeys() {
        for (JsonStudentActionItem resource : getAllActionItems())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonStudentActionItem next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonStudentActionItem(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonStudentActionItem> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }

}
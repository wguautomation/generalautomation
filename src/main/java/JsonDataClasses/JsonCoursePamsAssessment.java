
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/3/2015.
 */

public class JsonCoursePamsAssessment extends JsonData {

    public JsonCoursePamsAssessment(String jsonResult) {
        super(jsonResult);
    }
    public JsonCoursePamsAssessment(JsonObject object) {
        jsonObject = object;
    }

    public String getId() {
        return getAsString("id");
    }

    public String getBannerCode() {
        return getAsString("bannerCode");
    }

    public String getSubType() {
        return getAsString("subType");
    }

    public String getTitle() {
        return getAsString("title");
    }

    public void checkAllKeys() {
        getId();
        getBannerCode();
        getSubType();
        getTitle();
    }

}

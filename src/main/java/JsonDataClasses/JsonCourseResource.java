
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/3/2015.
 */

// jsonArray of a particular type, CourseResourcesList or PreparationsList
public class JsonCourseResource extends JsonData {

    public JsonCourseResource(String jsonResult) {
        super(jsonResult);
    }
    public JsonCourseResource(JsonObject object) {
        jsonObject = object;
    }

    public String getTitle() {
        return getAsString("title");
    }

    public String getSrc() {
        return getAsString("src");
    }

    public void checkAllKeys() {
        getTitle();
        getSrc();
    }

}

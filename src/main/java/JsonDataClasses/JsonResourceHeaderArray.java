
package JsonDataClasses;

import com.google.gson.JsonArray;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 11/12/2015.
 */

public class JsonResourceHeaderArray extends JsonData implements Iterable<JsonResourceHeader>, Iterator<JsonResourceHeader> {

    // jsonObject has everything. jsonArray we use as a shortcut with just the array items.
    public JsonResourceHeaderArray(String jsonResult) {
        super(jsonResult);
        loadArray(jsonObject, "resourceHeaders");
    }
    public JsonResourceHeaderArray(JsonArray array) {
        super(array);
    }

    public JsonResourceHeader getResourceHeader(int index) {
        return new JsonResourceHeader(jsonArray.get(index).getAsJsonObject());
    }

    public String getHeaderName() {
        return getAsString("headerName");
    }

    public JsonResourceHeaderArray getAllResourceHeaders() {
        return new JsonResourceHeaderArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonResourceHeader resource : getAllResourceHeaders())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonResourceHeader next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonResourceHeader(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonResourceHeader> iterator() {
        size      = jsonArray.size();
        cursor    = 0;

        return this;
    }
}


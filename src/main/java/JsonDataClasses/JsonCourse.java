
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 10/29/2015.
 */

// jsonObject contains a single course
public class JsonCourse extends JsonData {

    public JsonCourse(String jsonResult) {
        super(jsonResult);
    }
    public JsonCourse(JsonObject object) {
        super(object);
    }

    public String getCourseId() {
        return getAsString("id");
    }

    public String getTitle() {
        return getAsString("title");
    }

    public String getCourseCode() {
        return getAsString("courseCode");
    }

    public String getCompetencyUnits() {
        return getAsString("competencyUnits");
    }

    public String getCompleted() {
        return getAsString("completed");
    }

    public String getStartDate() {
        return getAsString("startDate");
    }

    public String getEndDate() {
        return getAsString("endDate");
    }

    public String getActivityDate() {
        return getAsString("activityDate");
    }

    public String getGradeDate() {
        return getAsString("gradeDate");
    }

    public String getNonDegreeRequirement() {
        return getAsString("nonDegreeRequirement");
    }

    public String getCourseStatus() {
        return getAsString("courseStatus");
    }

    public String getEnrolled() {
        return getAsString("enrolled");
    }

    public String getIncomplete() {
        return getAsString("incomplete");
    }

    public String getVersion() {
        return getAsString("version");
    }

    public String getCourseVersionId() {
        return getAsString("courseVersionId");
    }

    public String getCourseType() {
        return getAsString("courseType");
    }

    public String getStudyPlanType() {
        return getAsString("studyPlanType");
    }

    public String getPidm() {
        return getAsString("pidm");
    }

    public String getCourseStatusDate() {
        return getAsString("courseStatusDate");
    }

    public String getGradeDateFormatted() {
        return getAsString("gradeDateFormatted");
    }

    public String getStartDateFormatted() {
        return getAsString("startDateFormatted");
    }

    public String getActivityDateFormatted() {
        return getAsString("activityDateFormatted");
    }

    public String getCourseStatusDateFormatted() {
        return getAsString("courseStatusDateFormatted");
    }

    public String getEndDateFormatted() {
        return getAsString("endDateFormatted");
    }

    public void checkAllKeys() {
        getCourseId();
        getTitle();
        getCourseCode();
        getCompetencyUnits();
        getCompleted();
        getStartDate();
        getEndDate();
        getActivityDate();
        getGradeDate();
        getNonDegreeRequirement();
        getCourseStatus();
        getEnrolled();
        getIncomplete();
        getVersion();
        getCourseVersionId();
        getCourseType();
        getStudyPlanType();
        getPidm();
        getCourseStatusDate();
        getGradeDateFormatted();
        getStartDateFormatted();
        getActivityDateFormatted();
        getCourseStatusDateFormatted();
        getEndDateFormatted();
    }


}

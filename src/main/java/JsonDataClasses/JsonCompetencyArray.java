
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 10/28/2015.
 */

public class JsonCompetencyArray extends JsonData implements Iterable<JsonCompetency>, Iterator<JsonCompetency> {

    public JsonCompetencyArray(String jsonResult) {
        super(jsonResult, "competence");
    }
    JsonCompetencyArray(JsonArray array) {
        super(array);
    }

    public JsonCompetency getCompetency(int index) {
        return new JsonCompetency(jsonArray.get(index).getAsJsonObject());
    }

    public JsonCompetency getCompetencyById(String id) {
        JsonObject object = null;

        for (int index=0; index<jsonArray.size(); index++) {
            object = jsonArray.get(index).getAsJsonObject();
            if (object.get("competencyId").toString().contains(id))
                break;
        }

        return new JsonCompetency(object);
    }

    public JsonCompetencyArray getAllCompetencies() {
        return new JsonCompetencyArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonCompetency resource : getAllCompetencies())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonCompetency next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonCompetency(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonCompetency> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}

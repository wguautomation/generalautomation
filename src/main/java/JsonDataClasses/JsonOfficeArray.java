
package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 11/12/2015.
 */

public class JsonOfficeArray extends JsonData implements Iterable<JsonOffice>, Iterator<JsonOffice> {

    public JsonOfficeArray(String jsonResult) {
        super(jsonResult, "offices");
    }
    public JsonOfficeArray(JsonObject object) {
        super(object, "offices");
    }
    public JsonOfficeArray(JsonArray array) {
        super(array);
    }

    public JsonOffice getOffice(int index) {
        return new JsonOffice(jsonArray.get(index).getAsJsonObject());
    }

    public JsonOfficeArray getAllTags() {
        return new JsonOfficeArray(jsonArray);
    }

    public void checkAllKeys() {
        for (JsonOffice resource : getAllTags())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonOffice next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonOffice(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonOffice> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }
}


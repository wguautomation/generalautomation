
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/9/2015.
 */

public class JsonStudyPlan extends JsonData {

    public JsonStudyPlan() {
        super();
    }
    public JsonStudyPlan(String jsonResult) {
        super(jsonResult);
    }
    public JsonStudyPlan(JsonObject object) {
        jsonObject = object;
    }

    public String getCourseVersionId() {
        return getAsString("courseVersionId");
    }

    public String getStudyPlanId() {
        return getAsString("studyPlanId");
    }

    public String getStudyPlanTitle() {
        return getAsString("studyPlanTitle");
    }

    public String getStudyPlanType() {
        return getAsString("studyPlanType");
    }

    public String getStudyPlanVersion() {
        return getAsString("studyPlanVersion");
    }

    public String getCourseCode() {
        return getAsString("courseCode");
    }

    public String getDescription() {
        return getAsString("description");
    }

    public void checkAllKeys() {
        getCourseVersionId();
        getStudyPlanId();
        getStudyPlanTitle();
        getStudyPlanType();
        getStudyPlanVersion();
        getCourseCode();
        getDescription();
    }
}

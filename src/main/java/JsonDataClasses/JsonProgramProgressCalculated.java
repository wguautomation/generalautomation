
package JsonDataClasses;

import com.google.gson.JsonObject;

/*
 * Created by timothy.hallbeck on 11/9/2015.
 */

//
public class JsonProgramProgressCalculated extends JsonData {

    private JsonProgramProgress jsonProgramProgress = null;

    public JsonProgramProgressCalculated(String jsonResult) {
        super(jsonResult);
        jsonProgramProgress = new JsonProgramProgress(getAsObject("programProgress"));
    }
    public JsonProgramProgressCalculated(JsonObject object) {
        jsonObject = object;
    }

    public String getTotalProgramCu() {
        return jsonProgramProgress.getTotalProgramCu();
    }

    public String getCurrentTermCode() {
        return jsonProgramProgress.getCurrentTermCode();
    }

    public String getPlannedGraduationDate() {
        return jsonProgramProgress.getPlannedGraduationDate();
    }

    public String getProgramCode() {
        return jsonProgramProgress.getProgramCode();
    }

    public String getAttemptedCu() {
        return jsonProgramProgress.getAttemptedCu();
    }

    public String getRemainingCu() {
        return jsonProgramProgress.getRemainingCu();
    }

    public String getProgramVersionId() {
        return jsonProgramProgress.getProgramVersionId();
    }

    public String getTransferCu() {
        return jsonProgramProgress.getTransferCu();
    }

    public String getTotalTerms() {
        return jsonProgramProgress.getTotalTerms();
    }

    public String getProgramVersion() {
        return jsonProgramProgress.getProgramVersion();
    }

    public String getTermsCompleted() {
        return jsonProgramProgress.getTermsCompleted();
    }

    public String getPidm() {
        return jsonProgramProgress.getPidm();
    }

    public String getProgramName() {
        return jsonProgramProgress.getProgramName();
    }

    public String getCompletedCu() {
        return jsonProgramProgress.getCompletedCu();
    }

    public String getOtp() {
        return jsonProgramProgress.getOtp();
    }

    public String getEarnedCu() {
        return getAsString("earnedCu");
    }

    public String getDaysLeftInTerm() {
        return getAsString("daysLeftInTerm");
    }

    public String getProgressCuPct() {
        return getAsString("progressCuPct");
    }

    public String getWeeksLeftInTerm() {
        return getAsString("weeksLeftInTerm");
    }

    public String getCurrentTermToNearestTenth() {
        return getAsString("currentTermToNearestTenth");
    }

    static public void checkAllKeys(String jsonData) {
        new JsonProgramProgressCalculated(jsonData).checkAllKeys();
    }
    public void checkAllKeys() {
//        jsonProgramProgress.checkAllKeys(); // checks itself when it is loaded
        getEarnedCu();
        getDaysLeftInTerm();
        getProgressCuPct();
        getWeeksLeftInTerm();
        getCurrentTermToNearestTenth();
    }

}


package JsonDataClasses;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by timothy.hallbeck on 9/26/2016.
 */

public class JsonActivityDetailArray extends JsonData implements Iterable<JsonActivityDetail>, Iterator<JsonActivityDetail> {

    public JsonActivityDetailArray() {}
    public JsonActivityDetailArray(JsonObject object) {
        super(object, "");
        setObject(object);
    }
    public JsonActivityDetailArray(JsonArray array) {
        super(array);
    }

    public JsonActivityDetailArray getAllActivities() {
        return new JsonActivityDetailArray(jsonArray);
    }

    public JsonActivityDetail getActivity(int index) {
        return new JsonActivityDetail(jsonArray.get(index).getAsJsonObject());
    }

    public void checkAllKeys() {
        for (JsonActivityDetail resource : getAllActivities())
            resource.checkAllKeys();
    }

    private int cursor;
    private int size;

    @Override
    public boolean hasNext() {
        return cursor < size;
    }

    @Override
    public JsonActivityDetail next() {
        if(this.hasNext()) {
            int current = cursor;
            cursor ++;

            return new JsonActivityDetail(jsonArray.get(current).getAsJsonObject());
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<JsonActivityDetail> iterator() {
        cursor = 0;

        if (jsonArray == null)
            size = 0;
        else
            size = jsonArray.size();

        return this;
    }

}
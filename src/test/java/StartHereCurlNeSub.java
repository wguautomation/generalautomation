
import BaseClasses.LoginType;
import Utils.*;
import org.testng.TestNGException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 2/3/2016.
 */

public class StartHereCurlNeSub extends OssoSession {

    //
    // Prod
    //
    static boolean bWroteToFile;

    @Test(invocationCount = 1, timeOut = 13000)
    public void curlappnesubCommand() throws Exception {
        LoginType login = LoginType.PRODUCTION_APOIN;
        bWroteToFile = false;
        wguTestWrapper timer = new wguTestWrapper();
        timer.startTimer();
        String result = Get(
                "https://ne-sub.wgu.edu/students/" + login.getUsername() + "/notifications",
                login);
        if (result.contains("<h1>Developer Inactive</h1>"))
            throw new TestNGException(result);

        long time = timer.stopAndLogTime("  executeCurl() ");
        if (time >= 5000)
            throw new TestNGException("Timeout error");

        verifyKey(result, "username", login.getUsername(), false);

        wguCsvFileUtils.appendDataToFile(String.valueOf(time), "curlappnesubCommandOutput.csv");
        bWroteToFile = true;
        Thread.sleep(500);
    }

    @AfterTest(alwaysRun = true)
    public void afterParty() {
        if (!bWroteToFile)
            wguCsvFileUtils.appendDataToFile(String.valueOf(5000), "curlappnesubCommandOutput.csv");
    }
}

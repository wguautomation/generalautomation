
import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.*;
import ServiceClasses.MentorService;
import Utils.*;
import org.testng.Assert;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 11/2/2015.
 */

public class StartHereJsonTests extends MasherySession {

    @Test
    public void atest() {
        MentorService service = new MentorService(LoginType.LANE1_NORMAL, AuthType.PING);
    }

    @Test
    public void CpoindexterDegreeplanDataTest() {
        String result = Get("degreeplans/v1/degreeplans/current/pidm/106679", LoginType.PROD_CPOINDEXTER);
        JsonDegreePlan degreePlan = new JsonDegreePlan(result);

        degreePlan.getId();
        degreePlan.getName();
        degreePlan.getCurrentPlan();
        degreePlan.getPidm();

        JsonTermArray termArray = degreePlan.getAllTerms();
        String termId = "201211";
        JsonTerm term = termArray.getTermByCode(termId);
        Assert.assertTrue(term.getCourseCount() == 1, "Expected Term " + termId + " to have 1 course, instead has " + term.getCourseCount());

        term.checkAllKeys();

        JsonCourseArray courses = degreePlan.getAllCourses();
//        Assert.assertTrue(courses.size() == 72, "Expected course count to be 72, instead is " + courses.size());

        JsonCourse courseC132 = degreePlan.getCourseByCode("C132");
        courseC132.getCourseId();
        assert(courseC132.getTitle().contains("Elements of Effective Communication"));
        assert(courseC132.getCourseCode().contains("C132"));
        courseC132.getCompetencyUnits();
        courseC132.getCompleted();
        courseC132.getStartDate();
        courseC132.getEndDate();
        courseC132.getActivityDate();
        courseC132.getGradeDate();
        assert(courseC132.getNonDegreeRequirement().contains("false"));
        courseC132.getCourseStatus();
        courseC132.getEnrolled();
        courseC132.getIncomplete();
        assert(courseC132.getVersion().contains("2"));
        assert(courseC132.getCourseVersionId().contains("5940001"));
        courseC132.getCourseType();
        courseC132.getStudyPlanType();
        courseC132.getPidm();
        courseC132.getCourseStatusDate();
        courseC132.getGradeDateFormatted();
        courseC132.getStartDateFormatted();
        courseC132.getActivityDateFormatted();
        courseC132.getCourseStatusDateFormatted();
        courseC132.getEndDateFormatted();

    }

    static String[] termData = {
            "00000",
            "201211",
            "201305",
            "201311",
            "201405",
            "201411",
            "201505",
            "201511",
            "201605",
            "201611",
            "201705",
    };

    @Test
    public void CpoindexterTermDataTest() {
        String result = Get("degreeplans/v1/degreeplans/current/pidm/106679", LoginType.PROD_CPOINDEXTER);
        JsonDegreePlan degreePlan = new JsonDegreePlan(result);
        JsonTermArray terms = degreePlan.getAllTerms();
        String worker;
        JsonTerm term;

        for (int i=0; i<termData.length; i++) {
            term = terms.getTerm(i);
            assert(term != null);

            term.checkAllKeys();

            worker = term.getTermCode();
            Assert.assertTrue(worker.contains(termData[i]),
                    "Expected term " + i + " to have termCode =" + termData[i] + ", instead has termCode = " + worker);
        }
    }

    static String[] competencyData = {
            "2020",
            "3339",
    };

    @Test
    public void FullCosbCourseDataUnitTest() {
        String result = Get("cosb/v1/courses/3700001", LoginType.PRODUCTION_NORMAL);
        JsonCosbCourse cosbCourse = new JsonCosbCourse(result);

        cosbCourse.getTitle();
        cosbCourse.getIntro();

        JsonCourseCompetencyArray competencyArray = cosbCourse.getCourseCompetencies();
        String worker;
        for (int i=0; i<competencyData.length; i++) {
            worker = competencyArray.getCompetency(i).getId();
            Assert.assertTrue(worker.contains(competencyData[i]),
                    "Expected competencyArray " + i + " to have id = " + competencyData[i] + ", instead has id = " + worker);
        }

        JsonCourseVideo video = cosbCourse.getVideo();
        video.checkAllKeys();

        cosbCourse.checkAllKeys();

        JsonCoursePamsAssessmentArray pamsAssessmentArray = cosbCourse.getPamsAssessments();
        for (JsonCoursePamsAssessment pamsAssessment : pamsAssessmentArray) {
            pamsAssessment.checkAllKeys();
        }

        cosbCourse.getPamsCourseVersion();

        JsonCourseResourceArray courseResourceList = cosbCourse.getCourseResources();
        for (JsonCourseResource courseResource : courseResourceList) {
            courseResource.getTitle();
            courseResource.getSrc();
        }

        JsonCourseResourceArray preparationsList = cosbCourse.getCoursePreparations();
        for (JsonCourseResource courseResource : preparationsList) {
            courseResource.getTitle();
            courseResource.getSrc();
        }

        cosbCourse.getVendorShortDescription();
    }

    @Test
    public void BasicCosbCourseInfoDataUnitTest() {
        String result = Get("cosb/v1/courses/info/1332", LoginType.PRODUCTION_NORMAL);
        JsonCosbCourseInfo cosbCourseInfo = new JsonCosbCourseInfo(result);

        Assert.assertTrue(cosbCourseInfo.getCourseVersionid().contains("1332"),
                "Expected courseVersionid = 1332, instead has = " + cosbCourseInfo.getCourseVersionid());
        Assert.assertTrue(cosbCourseInfo.getCourseTitle().contains("IWT1 - Literature, Arts, and the Humanities"),
                "Expected courseTitle = IWT1 - Literature, Arts, and the Humanities, instead has = " + cosbCourseInfo.getCourseTitle());
        Assert.assertTrue(cosbCourseInfo.getCourseCode().contains("IWT1"),
                "Expected courseCode = IWT1, instead has = " + cosbCourseInfo.getCourseCode());
        Assert.assertTrue(cosbCourseInfo.getCourseType().contains("COS_A"),
                "Expected courseType = COS_A, instead has = " + cosbCourseInfo.getCourseType());
        Assert.assertTrue(cosbCourseInfo.getPdfURL().contains(""),
                "Expected pdfURL = \"\", instead has = " + cosbCourseInfo.getPdfURL());
    }

    @Test
    public void CwessmanMentorDataTest() {
        String result = Get("mentor/v1/mentors/cpoindexter", LoginType.PROD_CPOINDEXTER);
        JsonMentorArray mentorArray = new JsonMentorArray(result);
        JsonMentor mentor = mentorArray.getMentorByEmail("cortney.wessman@wgu.edu");

        Assert.assertTrue(mentor.getFirstName().contains("Cortney"),
                "Expected getFirstName = [Cortney], instead getFirstName = " + mentor.getFirstName());
        Assert.assertTrue(mentor.getLastName().contains("Wessman"),
                "Expected getLastName = [Wessman], instead getLastName = " + mentor.getLastName());
        mentor.getEmailAddress();
        Assert.assertTrue(mentor.getPhoneNumber().contains("8012903603x1603"),
                "Expected getPhoneNumber = [8012903603x1603], instead getPhoneNumber = " + mentor.getPhoneNumber());
        mentor.getOfficeHours();
        mentor.getCourseTitle();
        mentor.getBio();
        Assert.assertTrue(mentor.getType().contains("STUDENT_MENTOR"),
                "Expected getType = [STUDENT_MENTOR], instead getType = " + mentor.getType());
        mentor.getCourseCode();
        Assert.assertTrue(mentor.getPidm().contains("33497"),
                "Expected getPidm = [33497], instead getPidm = " + mentor.getPidm());
    }

    static String[] cpoindexterData = {
            "person/v1/person",
            "person/v1/person/bannerId/QA0000007",
            "person/v1/person/cpoindexter",
            "person/v1/person/pidm/106679",
    };

    @Test
    public void CpoindexterPersonInfoDataTest() throws Exception {
        String result;
        JsonPerson person;

        for (String endpoint : cpoindexterData) {
            result = Get(endpoint, LoginType.PROD_CPOINDEXTER);
            person = new JsonPerson(result);

            Assert.assertTrue(person.getPidm().contains("106679"),
                    "Expected getPidm = [106679], instead getPidm = " + person.getPidm());
            Assert.assertTrue(person.getFirstName().contains("Cortney"),
                    "Expected getFirstName = [Cortney], instead getFirstName = " + person.getFirstName());
            person.getMiddleName();
            Assert.assertTrue(person.getLastName().contains("Poindexter"),
                    "Expected getLastName = [Poindexter], instead getLastName = " + person.getLastName());
            Assert.assertTrue(person.getPersonType().contains("Student"),
                    "Expected getPersonType = [Student], instead getPersonType = " + person.getPersonType());
            Assert.assertTrue(person.getPersonType().contains("Student"),
                    "Expected getPersonType = [Student], instead getPersonType = " + person.getPersonType());
            person.getStreetLine1();
            person.getStreetLine2();
            person.getCity();
            person.getState();
            person.getZipCode();
            person.getPrimaryPhone();
            person.getMobilePhone();
            Assert.assertTrue(person.getIsEmployee().contains("false"),
                    "Expected getIsEmployee = [false], instead getIsEmployee = " + person.getIsEmployee());
            person.getAllRoles();
            Assert.assertTrue(person.getPersonRoleType().contains("student"),
                    "Expected getPersonRoleType = [student], instead getPersonRoleType = " + person.getPersonRoleType());
            Assert.assertTrue(person.getUsername().contains("cpoindexter"),
                    "Expected getUsername = [cpoindexter], instead getUsername = " + person.getUsername());
            Assert.assertTrue(person.getStudentId().contains("QA0000007"),
                    "Expected getStudentId = [QA0000007], instead getStudentId = " + person.getStudentId());
            person.getPreferredEmail();
        }
    }

    static String[] competencyTitleData = {
            "Imagination, Values, & Emotions",
            "Connections Across Disciplines",
            "Humanities & Culture",
            "Ethics, Belief Systems, & the Arts",
    };

    @Test
    public void BasicCompetencyInfoUnitTest() throws Exception {
        String result = Get("assessments/v1/assessments/1880/competencies", LoginType.PRODUCTION_NORMAL);
        JsonCompetencyArray array = new JsonCompetencyArray(result);

        for (int i=0; i<array.size(); i++) {
            JsonCompetency competency = array.getCompetency(i);
            String title = competency.getTitle();

            Assert.assertTrue(title.contains(competencyTitleData[i]),
                    "Expected competencyTitle = [" + competencyTitleData[i] + "], instead = " + title);
            competency.checkAllKeys();
        }
    }

    @Test
    public void BasicFaqUnitTest() {
        String result = Get("wguinformation/v1/faqs", LoginType.PRODUCTION_NORMAL);
        JsonFaqArray faqArray = new JsonFaqArray(result);

        for (JsonFaq faq : faqArray) {
            faq.getQuestion();
            faq.getAnswer();

            for (JsonTag tag : faq.getAllTags())
                tag.getTagText();
            faq.getOrder();
        }
    }

    @Test
    public void BasicCourseAssessmentUnitTest() {
        String result = Get("assessments/v1/courses/1332", LoginType.PRODUCTION_NORMAL);
        JsonCourseAssessment courseAssessment = new JsonCourseAssessment(result);
        General.Debug(courseAssessment.toString());

        for (JsonAssessment assessment : courseAssessment.getAllAssessments()) {
            General.Debug(assessment.toString());
            assessment.checkAllKeys();
        }
    }

    @Test
    // Fails to return about 1/2 the time
    public void BasicNewsItemUnitTest() {
        String result = Get("newsitems/v1/feed/106679/1417000000", LoginType.PROD_CPOINDEXTER);
        JsonNewsItemArray newsItemArray = new JsonNewsItemArray(result);

        for (JsonNewsItem newsItem : newsItemArray) {
            General.Debug("  Item: " + newsItem);

            newsItem.checkAllKeys();

            JsonActivityDate date = newsItem.getActivityDate();
            date.checkAllKeys();

            JsonNewsItemData data = newsItem.getNewsItemData();
            switch (newsItem.getType()) {
                case "\"TASK_STREAM\"":
                    data.getNewsItemDataId();
                    data.getAssessmentCode();
                    data.getTaskId();
                    data.getStatus();
                    data.getNewsItemDataId();
                    data.getNewsItemDataActivityDate();

                    break;
                case "\"ASSESSMENT_ATTEMPT\"":
                    data.getNewsItemDataActivityDate();
                    data.getAssessmentCode();
                    data.getPidm();
                    data.getCourseId();
                    data.getScheduledTimezone();
                    data.getCourseTitle();
                    data.getAssessmentId();
                    data.getScheduledTime();
                    data.getStatusDate();
                    data.getCourseCode();
                    data.getAssessmentStatus();
                    data.getAssessmentTitle();
                    break;
            }
        }
    }

    @Test
    public void StudentActionItemsUnitTestLane1() {
        String result = Get("https://student-actionitems.qa.wgu.edu/v2/api/student/106679/tasks", LoginType.LANE1_NORMAL);
        JsonStudentActionItemArray actionItemArray = new JsonStudentActionItemArray(result);

        for (JsonStudentActionItem item : actionItemArray)
            item.checkAllKeys();
    }

    @Test
    public void BasicProgramProgressUnitTest() {
        String result = Get("programProgress/v1/api/ProgramProgress/106679", LoginType.PROD_CPOINDEXTER);
        JsonProgramProgress programProgress = new JsonProgramProgress(result);

        programProgress.checkAllKeys();
    }

    @Test
    public void BasicProgramProgressCalculatedUnitTest() {
        String result = Get("programProgress/v1/api/ProgramProgress/106679/calculated", LoginType.PROD_CPOINDEXTER);
        JsonProgramProgressCalculated programProgressCalculated = new JsonProgramProgressCalculated(result);

        programProgressCalculated.checkAllKeys();
    }

    @Test
    public void FullStudyPlanUnitTest() {
        String result = Get("studyplan/v1/api/courses/getPublishedStudyPlans", LoginType.PRODUCTION_NORMAL);
        JsonStudyPlanArray studyPlanArray = new JsonStudyPlanArray(result);

        for (JsonStudyPlan plan : studyPlanArray) {
            plan.checkAllKeys();
        }
    }

    // JsonStudyPlanCourseInfo is a course, and has a
    //   JsonStudyPlanSubjectArray which has a
    //     JsonSubjectTopicArray which has a
    //       JsonSubjectActivityArray
    @Test
    public void FullStudyPlanCourseInfoUnitTest() {
        for (String url : new String[]{"studyplan/v1/api/courses/info/1332", "studyplan/v1/api/courses/lastPublished/1332"}) {
            String result = Get(url, LoginType.PRODUCTION_NORMAL);
            JsonStudyPlanCourseInfo studyPlanCourseInfo = new JsonStudyPlanCourseInfo(result);

            General.Debug(studyPlanCourseInfo.prettyString(1));
            studyPlanCourseInfo.checkAllKeys();

            JsonStudyPlanSubjectArray studyPlanSubjectArray = studyPlanCourseInfo.getAllSubjects();
            for (JsonStudyPlanSubject subject : studyPlanSubjectArray) {
                General.Debug(subject.prettyString(4));
                subject.checkAllKeys();

                JsonSubjectTopicArray subjectTopicArray = subject.getAllTopics();
                for (JsonSubjectTopic topic : subjectTopicArray) {
                    General.Debug(topic.prettyString(12));
                    topic.checkAllKeys();

                    JsonSubjectActivityArray subjectActivityArray = topic.getAllActivities();
                    for (JsonSubjectActivity activity : subjectActivityArray) {
                        General.Debug(activity.prettyString(17));
                        activity.checkAllKeys();
                   }
                }
            }
        }
    }

    @Test
    public void BasicLinkRatingsUnitTest() {
        String result = Get("studyplan/v1/mobileLinks/linkRatings/1332", LoginType.PRODUCTION_NORMAL);
        JsonLinkRatingArray linkRatingArray = new JsonLinkRatingArray(result);

        for (JsonLinkRating rating : linkRatingArray) {
            rating.checkAllKeys();
        }
    }

    @Test
    public void FullCourseNotesUnitTest() {
        String result = Get("degreeplans/v1/degreeplans/current/pidm/106679", LoginType.PROD_CPOINDEXTER);
        JsonDegreePlan degreePlan = new JsonDegreePlan(result);
        JsonCourseArray courses = degreePlan.getAllCourses();

        for (JsonCourse course : courses) {
            if (course.getCourseVersionId().length() <= 0)
                continue;
            result = Get("studyplan/v1/api/courses/notes/cpoindexter/" + course.getCourseVersionId(), LoginType.PROD_CPOINDEXTER);
            JsonCourseNoteArray courseNotes = new JsonCourseNoteArray(result);
            courseNotes.checkAllKeys();
        }
    }

    @Test
    public void FullContactInformationUnitTest() {
        String result = Get("wguinformation/v1/contactInformation", LoginType.PRODUCTION_NORMAL);
        JsonContactInformation contactInformation = new JsonContactInformation(result);

        contactInformation.checkAllKeys();

        JsonOfficeArray officeArray = contactInformation.getOffices();
        for (JsonOffice office : officeArray) {
            office.checkAllKeys();

            for (JsonDepartment department : office) {
                department.checkAllKeys();
            }
        }
    }

    @Test
    public void FullResourcesUnitTest() {
        String result = Get("wguinformation/v1/resources", LoginType.PRODUCTION_NORMAL);
        JsonResourceHeaderArray resourceHeaderArray = new JsonResourceHeaderArray(result);

        for (JsonResourceHeader resourceHeader : resourceHeaderArray) {
            resourceHeader.getHeaderName();
            for (JsonResource resource : resourceHeader.getAllResources()) {
                resource.getUrl();
                resource.getUrlName();
            }
        }
    }

    @Test
    public void FullWsbsiSitesUnitTest() {
        for (String url : new String[]{"wsbsi/v1/sites", "wsbsi/v1/sites/default", "wsbsi/v1/sites/sponsored"}) {
            String result = Get(url, LoginType.PRODUCTION_NORMAL);
            JsonSiteArray siteArray = new JsonSiteArray(result);

            for (JsonSite site : siteArray) {
                site.getSiteId();
                site.getUrl();
                site.getDescription();

                JsonSitePlatform platform = site.getPlatform();
                platform.checkAllKeys();

                site.getAvgScore();

                JsonSiteState state = site.getSocialSiteState();
                state.checkAllKeys();

                site.getUrlScheme();
            }
        }
    }


}

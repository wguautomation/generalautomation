
import BaseClasses.LoginType;
import Utils.wguCsvFileUtils;
import Utils.OssoSession;
import org.testng.TestNGException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 6/13/2016.
 */

public class StartHereCurlMasheryPrefixReallyOsso extends OssoSession {

    static boolean bWroteToFileMashery=false;

    //
    // Prod
    //

    @Test(invocationCount = 1, timeOut = 10000)
    public void curlMasheryCommand() throws Exception {
        LoginType login = LoginType.PRODUCTION_APOIN;
        bWroteToFileMashery = false;
        startTimer();
        String result = Get("https://mashery.wgu.edu/ne-subscriber/v1/" + login.getUsername() + "/notifications", login);
        if (result.contains("<h1>Developer Inactive</h1>") || result.contains("<h1>Service Unavailable</h1>"))
            throw new TestNGException(result);

        long time = stopAndLogTime("  executeCurl() ");
        if (time >= 7000)
            throw new TestNGException("Timeout error");

        verifyKey(result, "username", login.getUsername(), false);

        wguCsvFileUtils.appendDataToFile(String.valueOf(time), "curlMasheryCommandOutput.csv");
        bWroteToFileMashery = true;
        Thread.sleep(500);
    }

    @AfterTest(alwaysRun = true)
    public void afterParty() {
        if (!bWroteToFileMashery)
            wguCsvFileUtils.appendDataToFile(String.valueOf(15000), "curlMasheryCommandOutput.csv");
    }
}

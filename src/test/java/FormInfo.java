
/*
 * Created by timothy.hallbeck on 4/27/2016.
 */

import java.util.Random;

public class FormInfo {

    private int x=0;
    public Random ran;

    FormInfo() {
        ran = new Random();
    };

    public String getFirst(){
        String first = "Test";

        return first;
    }

    synchronized public int Increment() {
        x++;
        return x;
    }

    synchronized public String getLast(int y){
        String last = "Ooooo" + y;

        return last;
    }

    public String getPreferredName(){
        return "Capt. America";
    }

    public String getEmail(int y){
        String email = getLast(y) + "@darkside.com";
        return email;
    }

    public String getPhone(){

        int tempA = ran.nextInt(899)+100;
        int tempB = ran.nextInt(899)+100;
        int tempC = ran.nextInt(9999);

        String area = String.valueOf(tempA);
        String pre = String.valueOf(tempB);
        String post = String.valueOf(tempC);

        String phone = "(" + area + ")" + pre + "-" + post;

        return phone;
    }

    public String getAddress(){

        return "4545 TEST Dr.";
    }

    public String getCity(){

        return "Reno";
    }

    public String getZipCode(){

        int temp = ran.nextInt(89999) + 10000;
        String zip = String.valueOf(temp);
        return zip;
    }
}


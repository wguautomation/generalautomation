import BaseClasses.LoginType;
import Utils.*;
import org.testng.TestNGException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 2/3/2016.
 */

public class StartHereCurlAppNeSub502 extends OssoSession {

    //
    // Prod
    //
    static boolean bWroteToFilenesub502;

    @Test(invocationCount = 1, timeOut = 13000)
    public void curlappnesub502Command() throws Exception {
        LoginType login = LoginType.PRODUCTION_APOIN;
        bWroteToFilenesub502 = false;
        wguTestWrapper timer = new wguTestWrapper();
        timer.startTimer();
        String result = Get(
                "http://appnesub502.wgu.edu:8502/students/" + login.getUsername() + "/notifications",
                login);
        if (result.contains("<h1>Developer Inactive</h1>"))
            throw new TestNGException(result);

        long time = timer.stopAndLogTime("  executeCurl() ");
        if (time >= 5000)
            throw new TestNGException("Timeout error");

        verifyKey(result, "username", login.getUsername(), false);

        wguCsvFileUtils.appendDataToFile(String.valueOf(time), "curlappnesub502CommandOutput.csv");
        bWroteToFilenesub502 = true;
        Thread.sleep(500);
    }

    @AfterTest(alwaysRun = true)
    public void afterParty() {
        if (!bWroteToFilenesub502)
            wguCsvFileUtils.appendDataToFile(String.valueOf(5000), "curlappnesub502CommandOutput.csv");
    }
}

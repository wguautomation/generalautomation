
import BaseClasses.LoginType;
import CassandraClasses.CassandraBase;
import CassandraClasses.studentInfo;
import CassandraClasses.timmehWorld;
import Utils.General;
import Utils.OssoSession;
import com.datastax.driver.core.ResultSet;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import java.util.TreeMap;

/*
 * Created by timothy.hallbeck on 5/2/2016.
 */

public class StartHereCassandra {

    @Test
    public void simpleTest() throws Exception
    {
        timmehWorld session = new timmehWorld(CassandraBase.connectionRemoteLane2MultipleNode1);
        ResultSet resultSet = session.getAllEmployees();
        session.listAllRows(resultSet);
        session.close();
    }

    @Test
    public void Add10RecordsTest() throws Exception
    {
        timmehWorld session = new timmehWorld(CassandraBase.connectionRemoteLane2MultipleNode1);
        try {
            session.addNRecords(10);
        } catch (Exception e) {
        } finally {
            if (session != null)
                session.close();
        }
    }

    static int uniqueCounter = 0;
    synchronized public int incrementCounter() {
        return ++uniqueCounter;
    }

    static TreeMap<Long, CassandraBase> sortedMap = null;
    static long refreshMillis = 0;

    public synchronized studentInfo getStudentInfoSession() {
        // Need to get a new set of session tokens every hour
        if (sortedMap == null)
            sortedMap = new TreeMap<>();

        if (refreshMillis < System.currentTimeMillis()) {
            sortedMap.clear();
            refreshMillis = System.currentTimeMillis() + 3600000;
        }

        long threadId = Thread.currentThread().getId();
        studentInfo session = (studentInfo) sortedMap.get(threadId);

        if (session == null) {
            session = new studentInfo(
                    CassandraBase.connectionRemoteLane2MultipleNode1,
                    CassandraBase.connectionRemoteLane2MultipleNode2,
                    CassandraBase.connectionRemoteLane2MultipleNode3,
                    CassandraBase.connectionRemoteLane2MultipleNode4,
                    CassandraBase.connectionRemoteLane2MultipleNode5
            );
            sortedMap.put(threadId, session);
        }
        return session;
    }

    public timmehWorld getTimmehWorldSession() {
        if (sortedMap == null)
            sortedMap = new TreeMap<>();

        long threadId = Thread.currentThread().getId();
        timmehWorld session = (timmehWorld) sortedMap.get(threadId);

        if (session == null) {
            session = new timmehWorld(
                    CassandraBase.connectionRemoteLane2MultipleNode1
            );
            sortedMap.put(threadId, session);
        }
        return session;
    }

    @Test(invocationCount = 1000, threadPoolSize = 25)
    public void AddRecordsMultithreadedTest() throws Exception
    {
        int invocationCounter = incrementCounter();
        timmehWorld session = getTimmehWorldSession();
        session.addRecordN(invocationCounter);
    }

    @Test(invocationCount = 1000, threadPoolSize = 1)
    public void GetTimmehRecordsMultithreadedTest() throws Exception
    {
        int invocationCounter = incrementCounter();
        timmehWorld session = getTimmehWorldSession();
        session.getRecordById(invocationCounter);
    }

    @Test(invocationCount = 1000, threadPoolSize = 1)
    public void DeleteTimmehRecordsMultithreadedTest() throws Exception
    {
        int invocationCounter = incrementCounter();
        timmehWorld session = getTimmehWorldSession();
        session.deleteRecordById(invocationCounter);
    }

    @Test(invocationCount = 1000, threadPoolSize = 25)
    public void ModifyTimmehRecordsMultithreadedTest() throws Exception
    {
        int invocationCounter = incrementCounter();
        timmehWorld session = getTimmehWorldSession();
        session.modifyRecordById(invocationCounter);
    }

    @Test
    public void getRecordCountTimmeh() {
        timmehWorld session = new timmehWorld(CassandraBase.connectionLocalSingleNode);
        session.getRecordCount();
        session.close();
    }

    @Test
    public void getAllRecords() throws Exception
    {
        timmehWorld session = new timmehWorld(CassandraBase.connectionRemoteLane2MultipleNode1);
        session.getAllEmployees();
        session.close();
    }

    @Test
    public void getSingleRecordTimmeh() throws Exception
    {
        timmehWorld session = new timmehWorld(CassandraBase.connectionRemoteLane2MultipleNode1);
        for (int i=0; i<1000; i++)
            session.getRecordById(1);
        session.close();
    }

    @Test(invocationCount = 1000, threadPoolSize = 15)
    public void ModifyRecordsMultithreadedTestStudentInfo() throws Exception
    {
        int invocationCounter = incrementCounter();
        studentInfo session = getStudentInfoSession();
        session.modifyRecordByPidm(invocationCounter);
    }

    @Test // 44 secs to count 461385 on 2016_05_16
    public void getRecordCountStudentInfo() {
        studentInfo session = new studentInfo(CassandraBase.connectionRemoteLane2MultipleNode1);
        session.getRecordCount();
        session.close();
    }

    @Test
    public void getSingleRecordStudentInfo() throws Exception
    {
        studentInfo session = new studentInfo(CassandraBase.connectionRemoteLane2MultipleNode1);
        session.listAllRows(session.getRecordByPidm(299323));
        String json = session.toJson(session.getRecordByPidm(299323).one());
        General.Debug(json);
        session.close();
    }

    @Test(invocationCount = 100, threadPoolSize = 10)
    public void GetRecordsMultithreadedTestStudentInfo() throws Exception
    {
        int invocationCounter = incrementCounter();
        studentInfo session = getStudentInfoSession();
        session.getRecordByPidm(invocationCounter);
    }

    // Shared by all threads
    static long startingMillis = 0;
    static int  startingInvocationCounter = 0;
    public synchronized void updateFeedback(int counter, int period) {
        if (Math.floorMod(counter, period) == 0) {
            long recordsPerSecond = (counter - startingInvocationCounter) * 1000 / (System.currentTimeMillis() - startingMillis);
            General.Debug("At " + counter + ", " + recordsPerSecond + " actions per second");
            startingInvocationCounter = counter;
            startingMillis = System.currentTimeMillis();

//            Session.State state = getStudentInfoSession().getSession().getState();
        }
    }

    // Set invocationCount and threadPoolSize to the same number, then use the loop to control
    //   the duration of the test.
    @Test(invocationCount = 18, threadPoolSize = 18)
    public void ModifyRdbRecordsLongRunning() throws Exception
    {
        int periodicCounter = 4000;
        int invocationCounter, pidm;
        studentInfo session = getStudentInfoSession();

        for (int i=0; i<1000000; i++) {
            invocationCounter = incrementCounter();
            pidm = Utils.General.getRandomInt(451000);

            try {
                session.modifyRecordByPidm(pidm);
            } catch (Exception e) {
                General.Debug(e.getMessage());
            }

            updateFeedback(invocationCounter, periodicCounter);
            Thread.yield();
        }
    }

    @Test(invocationCount = 1, threadPoolSize = 1)
    public void ModifyTimmehworldRecordsLongRunning() throws Exception
    {
        int periodicCounter = 500;
        int invocationCounter, pidm;
        timmehWorld session = getTimmehWorldSession();

        for (int i=0; i<10005; i++) {
            invocationCounter = incrementCounter();
            pidm = Utils.General.getRandomInt(451000);

            try {
                session.getRecordById(i);
//                session.modifyRecordById(i);
            } catch (Exception e) {
                General.Debug(e.getMessage());
            }

            updateFeedback(invocationCounter, periodicCounter);
            Thread.yield();
        }
    }

    //
    // Thread count of 8 will hit the current 23 gets / sec using the service call
    //
    @Test(invocationCount = 5, threadPoolSize = 5)
    public void GetUsingServiceCallLongRunning() throws Exception
    {
        int periodicCounter = 20;
        int invocationCounter, pidm;
        OssoSession executer = new OssoSession();
        General.setSilentMode(true);
        String result;

        for (int i=0; i<500000; i++) {
            invocationCounter = incrementCounter();
            pidm = Utils.General.getRandomInt(451000);

            try {
                result = executer.Get("http://rdb-studentprof.dev.wgu.edu:8214/v1/api/students/pidm/" + pidm, LoginType.LANE2_CASSANDRA);
            } catch (Exception e) {
                General.Debug(e.getMessage());
            }

            updateFeedback(invocationCounter, periodicCounter);
            Thread.yield();
        }
    }

    @AfterTest
    public void closeConnections() {
        if (sortedMap == null || sortedMap.size() == 0)
            return;

        for (long key : sortedMap.keySet()) {
            CassandraBase session = sortedMap.get(key);
            session.close();
        }
    }

}

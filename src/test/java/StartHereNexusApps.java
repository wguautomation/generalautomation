import NexusApps.nexusGeneralControls;
import org.sikuli.script.FindFailed;
import org.sikuli.script.ImagePath;
import org.sikuli.script.Match;
import org.sikuli.script.Screen;
import org.testng.annotations.Test;

import java.awt.*;

/*
 * Created by timothy.hallbeck on 11/23/2015.
 */

public class StartHereNexusApps {

    @Test
    public void testBasics() throws AWTException, FindFailed {
        nexusGeneralControls app = new nexusGeneralControls();

        app.closeAllApps();

        Screen screen = new Screen();
        String path = ImagePath.getBundlePath();
        path += "\\src\\main\\resources";
        ImagePath.setBundlePath(path);

        screen.capture(0, 0, 620, 1020);
        Match match = screen.exists("firefoxicon.png");
        if (match != null)
            match.click();
    }

}

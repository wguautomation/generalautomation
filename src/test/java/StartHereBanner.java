
import BannerPages.*;
import org.sikuli.script.FindFailed;
import org.testng.annotations.Test;
import java.awt.*;

/*
 * Created by timothy.hallbeck on 11/23/2015.
 */

public class StartHereBanner {

    @Test
    public void OpenGoatpadPage() throws AWTException, FindFailed {
        GoatpadPage goatpad = new GoatpadPage();
        goatpad.login();

        goatpad.ExercisePage();
        goatpad.clickClose();
    }

    @Test
    public void OpenSgastdnPage() throws AWTException, FindFailed {
        SgastdnPage sgastdn = new SgastdnPage();
        sgastdn.login();

        sgastdn.ExercisePage();
        sgastdn.clickClose();

    }

    @Test
    public void OpenSoaidenPage() throws AWTException, FindFailed {
        SoaidenPage soaiden = new SoaidenPage();
        soaiden.login();

        soaiden.ExercisePage();
        soaiden.clickClose();
    }

    @Test
    public void OpenSpaidenPage() throws AWTException, FindFailed {
        SpaidenPage spaiden = new SpaidenPage();
        spaiden.login();
        spaiden.ExercisePage();

        spaiden.clickClose();
    }

    @Test
    public void OpenSfargfePage() throws AWTException, FindFailed {
        SfargfePage sfargfe = new SfargfePage();
        sfargfe.login();

        sfargfe.ExercisePage();
        sfargfe.clickClose();
    }

    @Test
    public void OpenSfastcaPage() throws AWTException, FindFailed {
        SfastcaPage sfastca = new SfastcaPage();
        sfastca.login();

        sfastca.ExercisePage();
        sfastca.clickClose();
    }

    @Test
    public void OpenSgaadvrPage() throws AWTException, FindFailed {
        SgaadvrPage sgaadvr = new SgaadvrPage();
        sgaadvr.login();

        sgaadvr.ExercisePage();
        sgaadvr.clickClose();
    }

    @Test
    public void OpenSwatestPage() throws AWTException, FindFailed {
        SwatestPage swatest = new SwatestPage();
        swatest.login();

        swatest.ExercisePage();
        swatest.clickClose();
    }

    @Test
    public void OpenSfaregsPage() throws AWTException, FindFailed {
        SfaregsPage swatest = new SfaregsPage();
        swatest.login();

        swatest.ExercisePage();
        swatest.clickClose();
    }

    @Test
    public void OpenTsaarevPage() throws AWTException, FindFailed {
        TsaarevPage tsaarev = new TsaarevPage();
        tsaarev.login();

        tsaarev.ExercisePage();
        tsaarev.clickClose();
    }

    @Test
    public void OpenTsadetlPage() throws AWTException, FindFailed {
        TsadetlPage tsadetl = new TsadetlPage();
        tsadetl.login();

        tsadetl.ExercisePage();
        tsadetl.clickClose();
    }

    @Test
    public void OpenTwaarevPage() throws AWTException, FindFailed {
        TwaarevPage twaarev = new TwaarevPage();
        twaarev.login();

        twaarev.ExercisePage();
        twaarev.clickClose();
    }

}

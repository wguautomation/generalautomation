import BaseClasses.LoginType;
import BaseClasses.Page;
import BaseClasses.WebPage;
import StudentPortalPages.*;
import Utils.*;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.Map;
import java.util.Set;

/*
 * Created by timothy.hallbeck on 1/15/2015.
 */

public class StartHereStudentPortal extends wguTestWrapper {

    //
    // Prod
    //
    @Test(invocationCount = 1)
    public void justBringUpLogin() {
        wguPowerPointFileUtils.savePresentation = true;
        myLoginPage myPage = new myLoginPage();
        myPage.load();
        wguPowerPointFileUtils.savePresentation(myPage, "", "justBringUpLogin.pptx");
        myPage.quit();
    }

    //
    // QA
    //
    @Test
    public void StudentDirectoryHarvester() {
        myHomePage myPage = new myHomePage(LoginType.LANE1_APOINDEXTER);

        General.setSilentMode(true);
        myPage.load();
        myPage.get("https://l1my.wgu.edu/group/wgu-student-v3/network?tab=social#/directory");

 /*       WebElement currentTermDropdown = myPage.getByXpath("/html/body/ng-include/div/div/div/div[1]/div/div[1]/div[1]/div[1]/h4");
        currentTermDropdown.click();

        WebElement Anatomy = myPage.getByXpath("//*[@id='current-courses-filters']/div[1]/div/div/label");
        Anatomy.click();
        currentTermDropdown.click();
*/
        WebElement name;
        WebElement email;
        General.Debug("id, name, email");
        myPage.setTimeoutInSecs(4);

        String output = "";
        for (Long pidm = new Long(49486); pidm<59486; pidm++) {
            myPage.get("https://l1my.wgu.edu/group/wgu-student-v3/profile#/public/" + pidm.toString());
            name = myPage.getByXpath("/html/body/ng-include/div/header/div/div/h1");
            email = myPage.getByXpath("/html/body/ng-include/div/div/div/div/div/div[1]/div[2]/div/a", true);
            if (name != null) {
                output = pidm.toString() + ", " + name.getText();
                if (email != null)
                    output += ", " + email.getAttribute("href");
                General.Debug(output);
            }
        }

        myPage.quit();
    }

    //
    // Prod
    //
    @Test()
    public void OpenMyMathCenterPage() {
        myHomePage myMathCenterPage = new myMathCenterPage(null, LoginType.PROD_CPOINDEXTER, WebPage.Browser.Chrome);
        myMathCenterPage.ExercisePage(false);
    }

    //
    // Prod
    //
    @Test()
    public void ExerciseAllDataObjects() {
        General.exerciseAllDataObjects(LoginType.PROD_CPOINDEXTER);
    }

    //
    // Prod
    //
    @Test
    public void checkMyHomePage() {
        myHomePage myPage = new myHomePage(LoginType.LANE1_MPOINDEXTER);
        myPage.setTimeoutInSecs(myPage.TIMEOUT_IN_SECS);
        myPage.load();

        Page.Sleep((myPage.TIMEOUT_IN_SECS + 1) * 1000);
        myPage.showPageLoadTimes("checkMyHomePage");
        myPage.quit();
    }

    //
    // Prod
    //
    @Test
    public void GetProdPingTokenTest() {
        String pingToken = General.GetProdPingToken();
        General.Debug("Prod Token:\n" + pingToken);
    }

    //
    // Prod
    //
    @Test
    public void ExerciseMyHomePageFullProd() throws Exception {
        myHomePage myPage = null;
        try {
            myPage = new myHomePage(null, LoginType.PRODUCTION_NORMAL, WebPage.Browser.Chrome);
            myPage.ExercisePage(true);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR ExerciseMyHomePageFullProd");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "ExerciseMyHomePageFullProd.pptx");
            if(myPage != null)
                myPage.quit();
        }
    }

    //
    // Prod
    //
    @Test
    public void ExerciseMyHomePageShallowProd() throws Exception {
        myHomePage myPage = null;
        try {
//            wguPowerPointFileUtils.savePresentation = true;
            myPage = new myHomePage(null, LoginType.PRODUCTION_APOIN, WebPage.Browser.Chrome);
            myPage.ExercisePage(false);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR ExerciseMyHomePageShallowProd");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "ExerciseMyHomePageShallowProd.pptx");
            if(myPage != null)
                myPage.quit();
        }
    }

    //
    // Prod Jenkins
    //
    @Test
    public void ExerciseMyHomePageShallowProdJenkins() throws Exception {
        myHomePage myPage = null;
        try {
            myPage = new myHomePage(null, LoginType.PRODUCTION_NORMAL, WebPage.Browser.Firefox);
            myPage.setJenkins(true);
            myPage.ExercisePage(false);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new Exception(e);
        } finally {
            if(myPage != null)
                myPage.quit();
        }
    }

    //
    // QA Jenkins
    //
    @Test
    public void ExerciseMyHomePageShallowQaJenkins() throws Exception {
        myHomePage myPage = null;
        try {
            myPage = new myHomePage(null, LoginType.LANE1_NORMAL, WebPage.Browser.Firefox);
            myPage.setJenkins(true);
            myPage.ExercisePage(false);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new Exception(e);
        } finally {
            if(myPage != null)
                myPage.quit();
        }
    }

    //
    // QA
    //
    @Test
    public void ExerciseMyHomePageQa() throws Exception {
        myHomePage myPage = null;
        try {
            myPage = new myHomePage(null, LoginType.LANE1_NORMAL, WebPage.Browser.Firefox);
            myPage.ExercisePage(true);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new Exception(e);
        } finally {
            if(myPage != null)
                myPage.quit();
        }
    }
    //
    // Prod
    //
    @Test
    public void CheckEnglishC455Prod() throws Exception {
        myHomePage myPage = null;

        try {
            myPage = new myHomePage(null, LoginType.PRODUCTION_NORMAL);
            myPage.load();
            Course myCourse = Course.getCourse("C455");
            String[] listOfUrls = myCourse.getAllUrls();
            myPage.ExerciseUrlList(listOfUrls);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR CheckEnglishC455Prod");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "CheckEnglishC455Prod.pptx");
            if(myPage != null)
                myPage.quit();
        }
    }

    //
    // Lane 1
    //
    @Test
    public void ExerciseMyHomePageShallowLane1() throws Exception {
        myHomePage myPage = null;

        try {
            myPage = new myHomePage(null, LoginType.LANE1_NORMAL, WebPage.Browser.Firefox);
            myPage.showPageLoadTimes(true);
            myPage.ExercisePage(false);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR ExerciseMyHomePageShallowLane1");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "ExerciseMyHomePageShallowLane1.pptx");
            if(myPage != null)
                myPage.quit();
        }
    }

    //
    // Lane 1
    //
    @Test
    public void ExerciseMyHomePageFullLane1() throws Exception {
        myHomePage myPage = null;

        try {
            myPage = new myHomePage(null, LoginType.LANE1_NORMAL, WebPage.Browser.Chrome);
//            myPage.showPageLoadTimes(true);
            myPage.load().ExercisePage(true);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR ExerciseMyHomePageFullLane1");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "ExerciseMyHomePageFullLane1.pptx");
            if(myPage != null)
                myPage.quit();
        }
    }

    //
    // Lane 1
    //
    @Test
    public void AlternateLocationTest() throws Exception {
        myHomePage myPage = null;
        startTimer();

        try {
            myPage = new myHomePage(null, LoginType.LANE1_NORMAL);
            myPage.load();
            myPage.clickNewsPage();
            myPage.get("http://sh.wgu.edu");
            myPage.getByXpath("//*[@id='j_id0:j_id1:j_id16']/div[1]/h1");
            wguPowerPointFileUtils.saveScreenshot(myPage, "sh.wgu.edu");
            myPage.get("http://wellconnect.wgu.edu");
            wguPowerPointFileUtils.saveScreenshot(myPage, "wellconnect.wgu.edu");
            myPage.get("http://sh.wgu.edu");
            wguPowerPointFileUtils.saveScreenshot(myPage, "sh.wgu.edu");
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR AlternateLocationTest");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "AlternateLocationTest.pptx");
            if(myPage != null)
                myPage.quit();
            stopAndLogTime("end of alternate test");
        }
    }

    //
    // Lane 2
    //
    @Test
    public void ExerciseMyHomePageShallowLane2() throws Exception {
        myHomePage myPage = null;

        try {
            myPage = new myHomePage(null, LoginType.LANE2_NORMAL);
            myPage.load().ExercisePage(false);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR ExerciseMyHomePageShallowLane2");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "ExerciseMyHomePageShallowLane2.pptx");
            if(myPage != null)
                myPage.quit();
        }
    }

    //
    // All
    //
    @Test
    public void readHomePageHarFile() throws Exception {
        wguHarFileUtils utils = new wguHarFileUtils();
        utils.ReadUrlTimingInfo("/Users/timothy.hallbeck/Downloads/homepage.har", "/v1/");
        utils.WriteArraysToConsole();
    }

    //
    // Prod
    //
    @Test(invocationCount = 1)
    public void ExerciseSuccessCentersPageShallowProd() throws Exception {
        mySuccessCentersPage myPage = null;

        try {
            myPage = new mySuccessCentersPage(null, LoginType.PRODUCTION_NORMAL);
            myPage.ExercisePage(false);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR ExerciseSuccessCentersPageShallowProd");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "ExerciseSuccessCentersPageShallowProd.pptx");
            if(myPage != null)
                myPage.quit();
        }
    }

    //
    // Prod
    //
    @Test(invocationCount = 1) //
    public void ExerciseMyEmailPage() throws InterruptedException {
        myEmailPage myPage = new myEmailPage();
        myPage.setTimeoutInSecs(30);
        myPage.ExercisePage(false);
        myPage.quit();
    }

    //
    // Prod
    //
    @Test(invocationCount = 1)
    public void ExerciseMyStudentSupportPage() throws InterruptedException {
        myStudentSupportPage myPage = new myStudentSupportPage();
        myPage.setTimeoutInSecs(20);
        myPage.ExercisePage(true);
        myPage.quit();
    }

    //
    // Prod
    //
    @Test(invocationCount = 1)
    public void ExerciseMyCommunitiesPage() throws InterruptedException {
        myCommunitiesPage myPage = new myCommunitiesPage();
        myPage.setTimeoutInSecs(20);
        myPage.ExercisePage(true);
        myPage.quit();
    }

    //
    // Prod
    //
    @Test(invocationCount = 1)
    public void OpenMyLearningResourcesPage() throws InterruptedException {
        myLearningResourcesPage myPage = new myLearningResourcesPage();
        myPage.setTimeoutInSecs(20);
        myPage.ExercisePage(true);
        myPage.quit();
    }

    //
    // Prod
    //
    @Test
    public void ExerciseMyActionItemsPage() {
        myActionItemsPage myPage = new myActionItemsPage();
        myPage.setTimeoutInSecs(20);
        myPage.ExercisePage(false);
        myPage.quit();
    }

    //
    // Prod
    //
    @Test(invocationCount = 1)
    public void OpenMyCoursesPage() throws InterruptedException {
        myCoursesPage myPage = new myCoursesPage();
        myPage.setTimeoutInSecs(60);

        startTimer();
        myPage.load();
        stopAndLogTime("OpenMyCoursesPage::load() single execution");

        Page.Sleep(15 * 1000);
        myPage.showPageLoadTimes("OpenMyCoursesPage");
        myPage.quit();
    }

    //
    // Prod
    //
    @Test(invocationCount = 1)
    public void OpenMyActionItemsPage() throws InterruptedException {
        myActionItemsPage myPage = new myActionItemsPage();
        myPage.setTimeoutInSecs(60);

        startTimer();
        myPage.load();
        stopAndLogTime("OpenMyActionItemsPage::load() single execution");

        Page.Sleep((myPage.TIMEOUT_IN_SECS + 1) * 1000);
        myPage.showPageLoadTimes("OpenMyActionItemsPage");
        myPage.quit();
    }

    //
    // Prod
    //
    @Test(invocationCount = 1)
    public void OpenMySchedulePage() throws InterruptedException {
        mySchedulePage myPage = new mySchedulePage();
        myPage.setTimeoutInSecs(60);

        startTimer();
        myPage.load();
        stopAndLogTime("OpenMySchedulePage::load() single execution");

        Page.Sleep((myPage.TIMEOUT_IN_SECS + 1) * 1000);
        myPage.showPageLoadTimes("OpenMySchedulePage");
        myPage.quit();

    }

    //
    // Prod
    //
    @Test(invocationCount = 1)
    public void OpenMyCommunitiesPage() throws InterruptedException {
        myCommunitiesPage myPage = new myCommunitiesPage();
        measureSinglePage(myPage, "OpenMyCommunitiesPage");
    }

    //
    // Prod
    //
    @Test(invocationCount = 1)
    public void OpenMyEmailPage() throws InterruptedException {
        myEmailPage myPage = new myEmailPage();
        measureSinglePage(myPage, "OpenMyEmailPage");
    }

    private void measureSinglePage(WebPage page, String message) {
        page.setTimeoutInSecs(60);

        startTimer();
        page.load();
        stopAndLogTime(message);

        WebPage.Sleep((page.TIMEOUT_IN_SECS + 1) * 1000);
        page.showPageLoadTimes(message);
        page.closeTab();
    }

    static myCoursesPage myStaticCoursesPage = null;
    @Test(invocationCount = 1, dataProviderClass = wguDataProviders.class, dataProvider = "LRPs")
    public void IterateThroughCourses(String courseID, String providerName, String finishedXpath) throws InterruptedException {
        if (myStaticCoursesPage == null) {
            myStaticCoursesPage = new myCoursesPage();
            General.setSilentMode(true);
            myStaticCoursesPage.setTimeoutInSecs(60);
            myStaticCoursesPage.load();
            myStaticCoursesPage.showPageLoadTimes("IterateThroughCourses");
        }
        String lrpLoadPath = "https://lrps.wgu.edu/provision/" + courseID;

        startTimer();
        myStaticCoursesPage.get(lrpLoadPath);
        myStaticCoursesPage.getByXpath(finishedXpath);
        myStaticCoursesPage.showPageLoadTimes("LRP: (" + courseID + ", " + providerName + ")");
        long elapsedTime = Math.round(this.getTime() / 1000);
        stopAndLogTime(courseID + ", " + providerName + ", " + elapsedTime, true);

        myStaticCoursesPage.clickBrowserBackButton();
    }

    @Test(invocationCount = 1, dataProviderClass = wguDataProviders.class, dataProvider = "LRPs")
    public void robotTest(String courseID, String providerName, String finishedXpath) throws java.awt.AWTException, InterruptedException {
        myHomePage myPage = new myHomePage();

        myPage.load();
        new wguRobot().GenerateUrlMetricsFile(myPage, "https://lrps.wgu.edu/provision/" + courseID, providerName + "_" + courseID);
        myPage.quit();
    }

/*    @Test(invocationCount = 1)
    public void myHomePageWithTimings() throws Exception {
        myHomePage myPage = new myHomePage();

        String fullHarFilePath = "C:\\Users\\timothy.hallbeck\\Downloads\\homepage";
        myPage.load();
        fullHarFilePath = new wguRobot().GenerateUrlMetricsFile(myPage, "https://my.wgu.edu", fullHarFilePath);

        myPage.quit();
        wguHarFileUtils myHarUtils = new wguHarFileUtils();
        myHarUtils.ReadUrlTimingInfo(fullHarFilePath, "wgu.edu/banner/services");
        myHarUtils.WriteArraysToConsole();
//        Page.Sleep(1000 * 60 * 10);

        wguExcelFileUtils myExcelUtil = new wguExcelFileUtils();
        myExcelUtil.createSpreadsheetFromHar(fullHarFilePath + ".xlsx", myHarUtils);
        new wguRobot().ChangeExcelChartType(fullHarFilePath + ".xlsx", wguExcelFileUtils.chartType.COLUMN_3DCLUSTERED);
    }
*/
    //
    // How to use -- run this test to generate the 6 har files
    //
    @Test(invocationCount = 1, dataProviderClass = wguDataProviders.class, dataProvider = "lane1Pages")
    public void lane1TestCreateHars(String url, String fileName) throws Exception {
        myHomePage myPage = new myHomePage(null, LoginType.LANE1_NORMAL);

        fileName = "C:\\Users\\timothy.hallbeck\\Downloads\\" + fileName;
        myPage.load();
        fileName = new wguRobot().GenerateUrlMetricsFile(myPage, url, fileName);
        myPage.quit();
    }

    // Modify the dated filenames in the lane1PagesWithDates method, then run this
    @Test(invocationCount = 1, dataProviderClass = wguDataProviders.class, dataProvider = "lane1PagesWithDates")
    public void lane1TestCreateSpreadsheets(String url, String fileName) throws Exception {
        fileName = "C:\\Users\\timothy.hallbeck\\Downloads\\" + fileName + ".har";

        wguHarFileUtils myHarUtils = new wguHarFileUtils();
        myHarUtils.ReadUrlTimingInfo(fileName);
        myHarUtils.WriteArraysToConsole();

        wguExcelFileUtils myExcelUtil = new wguExcelFileUtils();
        myExcelUtil.createSpreadsheetFromHar(fileName + ".xlsx", myHarUtils);
        new wguRobot().ChangeExcelChartType(fileName + ".xlsx");
    }
    // Examine the results, attach spreadsheets to emails, and enjoy an adult beverage

    @Test(invocationCount = 1, dataProviderClass = wguDataProviders.class, dataProvider = "HarFilesForMerging")
    public void mergeBeforeAndAfterPageLevelTest(String fileName1, String fileName2) throws Exception {
        wguHarFileUtils myHarUtilsFile1 = new wguHarFileUtils();
        wguHarFileUtils myHarUtilsFile2 = new wguHarFileUtils();
        wguHarFileUtils mergedHarData;

        myHarUtilsFile1.ReadPageTimingInfo("C:\\Users\\timothy.hallbeck\\Downloads\\" + fileName1 + ".har");
        myHarUtilsFile2.ReadPageTimingInfo("C:\\Users\\timothy.hallbeck\\Downloads\\" + fileName2 + ".har");
        mergedHarData = wguHarFileUtils.MergePageTimingData(myHarUtilsFile1, myHarUtilsFile2);
        mergedHarData.WriteArraysToConsole();

        String mergedFileName = "C:\\Users\\timothy.hallbeck\\Downloads\\" + fileName1 + "_MergedPage.xlsx";
        wguExcelFileUtils myExcelUtil = new wguExcelFileUtils();
        myExcelUtil.createSpreadsheetFromPageTimes(mergedFileName, mergedHarData);
        new wguRobot().ChangeExcelChartType(mergedFileName);
    }

    @Test(invocationCount = 1, dataProviderClass = wguDataProviders.class, dataProvider = "HarFilesForMerging")
    public void merge2HarFilesTo1Spreadsheet(String fileName1, String fileName2) throws Exception {
        wguHarFileUtils myHarUtilsFile1 = new wguHarFileUtils();
        wguHarFileUtils myHarUtilsFile2 = new wguHarFileUtils();
        wguHarFileUtils mergedHarData;

        myHarUtilsFile1.ReadUrlTimingInfo("C:\\Users\\timothy.hallbeck\\Downloads\\" + fileName1 + ".har");
        myHarUtilsFile2.ReadUrlTimingInfo("C:\\Users\\timothy.hallbeck\\Downloads\\" + fileName2 + ".har");
        mergedHarData = wguHarFileUtils.MergeUrlTimingData(myHarUtilsFile1, myHarUtilsFile2);

        String mergedFileName = "C:\\Users\\timothy.hallbeck\\Downloads\\" + fileName1 + "_Merged.xlsx";
        wguExcelFileUtils myExcelUtil = new wguExcelFileUtils();
        myExcelUtil.createSpreadsheetFromHar(mergedFileName, mergedHarData);
        new wguRobot().ChangeExcelChartType(mergedFileName);
    }
}





















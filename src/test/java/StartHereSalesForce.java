
import SalesForcePages.*;
import Utils.*;
import org.testng.annotations.Test;
import java.util.ArrayList;

/*
 * Created by timothy.hallbeck on 4/26/2016.
 */

public class StartHereSalesForce extends wguTestWrapper {

    //
    // wgu.edu.qafull
    //
    @Test
    public void FullTestSalesForce() {
        sfHomePage myPage = new sfHomePage();
        myPage.setTimeoutInSecs(60);
        myPage.ExercisePage(true);
        myPage.quit();
    }

    //
    // wgu.edu.qafull
    //
    @Test
    public void OpenSalesForce() {
        sfHomePage myPage = new sfHomePage();
        myPage.setTimeoutInSecs(16);
        myPage.ExercisePage(false);
        //       myPage.quit();
    }

    //
    // wgu.edu.qafull
    //
    @Test
    public void OpenSfDevConsole() {
        sfDeveloperConsolePage myPage = new sfDeveloperConsolePage();
        myPage.setTimeoutInSecs(20);
        myPage.load();

        ArrayList<String> listOfFirstNames = myPage.executeQuery("select firstname from Contact limit 10");
        for (String string : listOfFirstNames)
            General.Debug(string);
        myPage.quit();
    }

    //
    // wgu.edu.qafull
    //
    @Test
    public void sfOpenContactPage() {
        sfContactsPage  myPage = new sfContactsPage();
        myPage.setTimeoutInSecs(20);

        myPage.load();
        myPage.getByXpath("//*[@id='bodyCell']/div[3]/div[1]/div/div[2]/table/tbody/tr[2]/th/a").click();
        myPage.getByXpath("//*[@id='003a000001Rlquq_00Na0000009RXcW_body']/table/tbody/tr[2]/th/a").click();
    }

    //
    // wgu.edu.qafull
    //
    @Test
    public void sfOpenCAREProgramsPage() {
        sfCAREProgramsPage myPage = new sfCAREProgramsPage();
        myPage.setTimeoutInSecs(20);
        myPage.ExercisePage(false);
        myPage.quit();
    }

    //
    // wgu.edu.qafull
    //
    @Test
    public void sfOpenLeadsPage() {
        sfLeadsPage myPage = new sfLeadsPage();
        myPage.setTimeoutInSecs(20);
        myPage.load();
        myPage.quit();
    }

    //
    // wgu.edu.qafull
    //
    @Test
    public void sfOpenAlumniRelationsPage() {
        sfAlumniRelationsPage myPage = new sfAlumniRelationsPage();
        myPage.setTimeoutInSecs(20);
        myPage.ExercisePage(false);
        myPage.quit();
    }

}





















import QtestPages.qtestHomePage;
import TestlinkPages.testlinkHomePage;
import Utils.wguPowerPointFileUtils;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 11/24/2015.
 */

public class StartHereTestlinkQtest {

    @Test
    public void doMobileExport() throws Exception {
        testlinkHomePage testlinkHomePage = null;

        try {
            testlinkHomePage = new testlinkHomePage();
            testlinkHomePage.load();

            testlinkHomePage.OpenAllFirstFolderCases("WGU-Mobile");
            testlinkHomePage.AddAllTestCases("ios", "MOBILE-");

            testlinkHomePage.OpenAllSecondFolderCases("WGU-Mobile");
            testlinkHomePage.AddAllTestCases("android", "MOBILE-");
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(testlinkHomePage, "ERROR doMobileExport");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(testlinkHomePage, "", "doMobileExport.pptx");
            testlinkHomePage.quit();
        }
    }

    @Test
         public void doSalesforceExport() throws Exception {
        testlinkHomePage testlinkHomePage = null;

        try {
            testlinkHomePage = new testlinkHomePage();
            testlinkHomePage.load();

            testlinkHomePage.OpenAllFirstFolderCases("SalesForce Org");
            testlinkHomePage.AddAllTestCases("SalesforceTek", "SF-");
            testlinkHomePage.clickSubFolder(1);

            testlinkHomePage.OpenAllSecondFolderCases("SalesForce Org");
            testlinkHomePage.AddAllTestCases("SalesforceMaint", "SF-");
            testlinkHomePage.clickSubFolder(2);

        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(testlinkHomePage, "ERROR doSalesforceExport");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(testlinkHomePage, "", "doSalesforceExport.pptx");
            testlinkHomePage.quit();
        }
    }

    String[] MentorForce = { "MentorForce", "MentF-",
            "MentorForce to Students",
            "MentorForce for Mentors",
            "Student Analytics",
            "MentorForce Mass Email",
            "MentorForce Action items",
            "MF3-CLE Refactor",
            "MCFFCAIR ",
    } ;

    String[] Portal = { "Portal", "Portal-",
            "Portal Login",
            "Portal Theme",
            "Prospective Students",
            "Current Student",
            "Graduate Student",
            "Voltron",
            "demo test suite",
    } ;

    @Test
    public void doExport() throws Exception {
        testlinkHomePage testlinkHomePage = null;

        // Just change this to any of the string[] defs above
        String[] worker = Portal;

        try {
            testlinkHomePage = new testlinkHomePage();
            testlinkHomePage.load();
            testlinkHomePage.selectProject(worker[0]);
            testlinkHomePage.clickBrowseTestCases();
            testlinkHomePage.clickExpandWholeTree();

            int count = worker.length - 2;
            for (int i=1; i<=count; i++)
                testlinkHomePage.clickSubFolder(i);

            for (int i=1; i<=count; i++) {
                testlinkHomePage.clickSubFolder(i);
                testlinkHomePage.AddAllTestCases(worker[i + 1], worker[1]);
                testlinkHomePage.clickSubFolder(i);
            }

        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(testlinkHomePage, "ERROR doExport");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(testlinkHomePage, "", "doExport.pptx");
            testlinkHomePage.quit();
        }
    }

    @Test
    public void qtestHomePageTest() {
        qtestHomePage page = new qtestHomePage();
        page.load();
        page.get("https://qtest.wgu.edu/p/32/portal/project#tab=testexecution&object=3&id=2605");
        page.clickRunButton();

    }
}


import ApplicationPages.appBasicInfoPage;
import Utils.*;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 1/15/2015.
 */

public class StartHereNewApplication extends wguTestWrapper {
    //
    // Lane1Qa
    //
    @Test(invocationCount = 1)
    public void createNewStudentApplication() {
        appBasicInfoPage myPage = new appBasicInfoPage();
        myPage.ExercisePage(true);

        wguPowerPointFileUtils.savePresentation(myPage, "", "createNewStudentApplication.pptx");
        myPage.quit();
    }

}
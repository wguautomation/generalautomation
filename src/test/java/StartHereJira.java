
import JiraPages.jiraHomePage;
import Utils.*;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 5/24/2015.
 */

public class StartHereJira extends wguTestWrapper {

    @Test
    public void justBringUpLogin() {
        jiraHomePage myPage = new jiraHomePage();
        myPage.login();
        myPage.quit();
    }

    @Test()
    public void attachFileToIssueTest() {
        wguJiraUtils.attachFileToIssue("MAUT-24", "C:\\Users\\timothy.hallbeck\\Desktop\\Screenshots\\android_MAUT_24_TestNotificationsPage.pptx");
    }
}

import BaseClasses.GlobalData;
import BaseClasses.Page;
import MobilePages.*;
import Utils.General;
import Utils.wguPowerPointFileUtils;
import Utils.wguTestWrapper;
import org.testng.SkipException;
import org.testng.TestNGException;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 4/1/2015
*/

public class phoneTest extends wguTestWrapper {

    @Test
    public void android_MAUT_16_TestLoginEnableDisable() throws Exception {
        MobileLoginPage myPage = null;

        try {
            myPage = new MobileLoginPage();
            myPage.verifyEnableDisableSignInButton();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_16_TestLoginEnableDisable");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-16", "android_MAUT_16_TestLoginEnableDisable.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_17_TestLoginBasics() throws Exception {
        MobileLoginPage myPage = null;

        try {
            myPage = new MobileLoginPage();
            myPage.verifyBasicInfo();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_17_TestLoginBasics");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
             wguPowerPointFileUtils.savePresentation(myPage, "MAUT-17", "android_MAUT_17_TestLoginBasics.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_18_TestHamburger() throws Exception {
        MobileDegreePlanPage myPage = null;

        try {
            myPage = new MobileDegreePlanPage();
            myPage.VerifyHamburgerContents();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_18_TestHamburger");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-18", "android_MAUT_18_TestHamburger.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_19_TestLoginFailure() throws Exception {
        MobileLoginPage myPage = null;

        try {
            myPage = new MobileLoginPage();
            myPage.verifyFailedAttempts();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_19_TestLoginFailure");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
             wguPowerPointFileUtils.savePresentation(myPage, "MAUT-19", "android_MAUT_19_TestLoginFailure.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_20_TestProfilePage() throws Exception {
        MobileProfilePage myPage = null;

        try {
            myPage = new MobileProfilePage();
            myPage.verifyBasicInfo();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_20_TestProfilePage");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-20", "android_MAUT_20_TestProfilePage.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_21_TestStudentIdToClipboard() throws Exception {
        MobileProfilePage myPage = null;

        try {
            myPage = new MobileProfilePage();
            myPage.copyStudentIdToClipboard();
            myPage.getAboutApp().click();
            MobileAboutAppPage.getFeedback(myPage).click();
            myPage.WaitForPageLoad(MobileFeedbackPage.class);
            myPage.paste(MobileXpath.FeedbackPage_Desc);
            wguPowerPointFileUtils.saveScreenshot(myPage, "Feedback after paste");
            wguPowerPointFileUtils.addBulletPoint("after paste from clipboard");
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_21_TestStudentIdToClipboard");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-21", "android_MAUT_21_TestStudentIdToClipboard.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_22_TestNonStudentLink() throws Exception {
        MobileLoginPage myPage = null;

        try {
            myPage = new MobileLoginPage();
            myPage.getNotAStudent().click();
            Page.Sleep(3000);
            wguPowerPointFileUtils.saveScreenshot(myPage, "After link click");
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_22_TestNonStudentLink");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new TestNGException("ERROR android_MAUT_22_TestNonStudentLink");
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-22", "android_MAUT_22_TestNonStudentLink.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_24_TestNotificationsPage() throws Exception {
        MobileNotificationsPage myPage = null;

        try {
            myPage = new MobileNotificationsPage();
            myPage.verifyNotificationText();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_24_TestNotificationsPage");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-24", "android_MAUT_24_TestNotificationsPage.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUTwreewr_25_TestLoginSignout() throws Exception {
        MobileLoginPage myPage = null;

        try {
            myPage = new MobileLoginPage();
            myPage.login();
            myPage.getSignOut().click();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_25_TestLoginSignout");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-25", "android_MAUT_25_TestLoginSignout.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test // Failing in 1.15.1 and later
    public void android_MAUT_26_TestKeepSignedIn() throws Exception {
        MobileLoginPage myPage = null;

        try {
            myPage = new MobileLoginPage();
            myPage.login(true);
            myPage.getMentorsIcon().click();
            if (myPage != null) myPage.quit();
            // should magically reappear where it left off
            MobileLoginPage myPage2 = new MobileLoginPage();
            wguPowerPointFileUtils.saveScreenshot(myPage2, "after restart");
            // should already be logged in
            MobileMentorsPage.verifyBasics(myPage2);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_26_TestKeepSignedIn");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-26", "android_MAUT_26_TestKeepSignedIn.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_27_Version() throws Exception {
        MobileAboutAppPage myPage = null;

        try {
            myPage = new MobileAboutAppPage();
            wguPowerPointFileUtils.saveScreenshot(myPage, "android_MAUT_27_Version");
            myPage.getByXpath("//AppCompatTextView[@value='" + myPage.VERSION + "']");
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_27_Version");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-27", "android_MAUT_27_Version.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    // MAUT_28 does not exist in Jira
    // MAUT_29 is a duplicate of MAUT-33

    @Test
    public void android_MAUT_30_TestGroupMentorDetailsPage() throws Exception {
        MobileGroupMentorDetailsPage myPage = null;

        try {
            myPage = new MobileGroupMentorDetailsPage();
            myPage.verifyBasicInfo();
        } catch (SkipException e) {
            General.Debug(e.getMessage());
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_30_TestGroupMentorDetailsPage");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-30", "android_MAUT_30_TestGroupMentorDetailsPage.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_31_TestAddMentorToContacts() throws Exception {
        MobileMentorDetailsPage myPage = null;

        try {
            myPage = new MobileMentorDetailsPage();
            myPage.addMentorToContacts();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_31_TestAddMentorToContacts");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-31", "android_MAUT_31_TestAddMentorToContacts.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_32_TestMentorsPage() throws Exception {
        MobileMentorsPage myPage = null;

        try {
            myPage = new MobileMentorsPage();
            MobileMentorsPage.verifyBasics(myPage);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_32_TestMentorsPage");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-32", "android_MAUT_32_TestMentorsPage.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_33_TestMentorDetailsPage() throws Exception {
        MobileMentorDetailsPage myPage = null;

        try {
            myPage = new MobileMentorDetailsPage();
            myPage.verifyBasics();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_33_TestMentorDetailsPage");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-33", "android_MAUT_33_TestMentorDetailsPage.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_34_TestDegreePlanPage() throws Exception {
        MobileDegreePlanPage myPage = null;

        try {
            myPage = new MobileDegreePlanPage();
            myPage.verifyBasics();
            myPage.ExercisePage(false);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_34_TestDegreePlanPage");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-34", "android_MAUT_34_TestDegreePlanPage.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_35_TestBackButton() throws Exception {
        MobileDegreePlanPage myPage = null;

        try {
            myPage = new MobileDegreePlanPage();
            myPage.getMentorsIcon().click();
            myPage.clickBackButton();
            myPage.verifyBasics();

            myPage.getAboutApp().click();
            MobileAboutAppPage.getFeedback(myPage).click();
            myPage.clickBackButton();
            myPage.clickBackButton();
            myPage.verifyBasics();

            myPage.getNotificationSettings().click();
            myPage.clickBackButton();
            myPage.verifyBasics();

            myPage.getSavedFiles().click();
            myPage.clickBackButton();
            myPage.verifyBasics();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_35_TestBackButton");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-35", "android_MAUT_35_TestBackButton.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_36_TestNonActiveStudent() throws Exception {
        MobileLoginPage myPage = null;

        try {
            myPage = new MobileLoginPage(null, GlobalData.STATUS_PROSPECTIVE);
            myPage.login();
            myPage.verifyNonActiveLoginMessage();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_36_TestNonActiveStudent");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-36", "android_MAUT_36_TestNonActiveStudent.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_37_TestCourseDetailsPage() throws Exception {
        MobileSubjectDetailsPage myPage = null;

        try {
            myPage = new MobileSubjectDetailsPage();
            myPage.verifyBasicInfo();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_37_TestCourseDetailsPage");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-37", "android_MAUT_37_TestCourseDetailsPage.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_38_TestStudyPlanPage() throws Exception {
        MobileStudyPlanPage myPage = null;

        try {
            myPage = new MobileStudyPlanPage();
            myPage.verifyBasicInfo();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_38_TestStudyPlanPage");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-38", "android_MAUT_38_TestStudyPlanPage.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_38b_TestSubjectDetailsPage() throws Exception {
        MobileStudyPlanPage myPage = null;

        try {
            myPage = new MobileSubjectDetailsPage();
            myPage.verifyBasicInfo();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_38b_TestSubjectDetailsPage");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-38", "android_MAUT_38b_TestSubjectDetailsPage.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test // 'Introduction' link from course details
    public void android_MAUT_39_TestStudyPlanTablet() throws Exception {
        MobileStudyPlanPage myPage = null;

        try {
            // Need to create and set global for Tablet usage instead of phone
            myPage = new MobileStudyPlanPage(GlobalData.DEVICE_TABLET);
            myPage.verifyBasicInfo();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_39_TestStudyPlanTablet");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-39", "android_MAUT_39_TestStudyPlanTablet.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_40_TestCourseAnnouncements() throws Exception {
        MobileCourseAnnouncementsPage myPage = null;

        try {
            myPage = new MobileCourseAnnouncementsPage();
            myPage.verifyBasicInfo();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_40_TestCourseAnnouncements");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-40", "android_MAUT_40_TestCourseAnnouncements.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_41_TestSkipCourse() throws Exception {
        MobileSubjectDetailsPage myPage = null;

        try {
            // Mark as skipped, then click again to unskip
            myPage = new MobileSubjectDetailsPage();
            MobileTopicPage.clickTopActivity(myPage);
            myPage.swipeUp(1, MobileAppPage.swipeSide.rightSide);
            MobileTopicPage.clickSkipCourse(myPage);
            MobileTopicPage.clickSkipCourse(myPage);

            // Mark as completed / uncompleted
            MobileTopicPage.clickCompleteCourse(myPage);
            MobileTopicPage.clickCompleteCourse(myPage);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_41_TestSkipCourse");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-41", "android_MAUT_41_TestSkipCourse.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_42_TestFeedbackForm() throws Exception {
        MobileFeedbackPage myPage = null;

        try {
            myPage = new MobileFeedbackPage();
            myPage.submitFeedback();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_42_TestFeedbackForm");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-42", "android_MAUT_42_TestFeedbackForm.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    // MAUT_43 to MAUT-69 are iOS

    @Test
    public void android_MAUT_70_TestNotificationSettings() throws Exception {
        MobileNotificationSettingsPage myPage = null;

        try {
            myPage = new MobileNotificationSettingsPage();
            myPage.verifyBasicInfo();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_70_TestNotificationSettings");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-70", "android_MAUT_70_TestNotificationSettings.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_71_TestMentorDetailsForFutureCourse() throws Exception {
        MobileDegreePlanPage myPage = null;

        try {
            myPage = new MobileDegreePlanPage();
            myPage.WaitForPageLoad(MobileDegreePlanPage.class);
            myPage.swipeLeft(MobileXpath.DegreePlan_TermDate, 1);
            myPage.clickTopCourse();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_71_TestMentorDetailsForFutureCourse");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-71", "android_MAUT_71_TestMentorDetailsForFutureCourse.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_72_TestEmptyCourseAnnouncements() throws Exception {
        MobileCourseAnnouncementsPage myPage = null;

        try {
            // Need account with empty course announcements
            myPage = new MobileCourseAnnouncementsPage();
            myPage.verifyEmptyAnnouncement();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_72_TestEmptyCourseAnnouncements");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-72", "android_MAUT_72_TestEmptyCourseAnnouncements.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    // MAUT_73 ia iOS

    @Test
    public void android_MAUT_74_TestEmptyCourseTips() throws Exception {
        MobileCourseTipsPage myPage = null;

        try {
            // Need account with empty course tips
            myPage = new MobileCourseTipsPage();
            myPage.verifyEmptyTips();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_74_TestEmptyCourseTips");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-74", "android_MAUT_74_TestEmptyCourseTips.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    // MAUT_75 ia iOS

    @Test
    public void android_MAUT_76_TestEmptyNotifications() throws Exception {
        MobileNotificationsPage myPage = null;

        try {
            myPage = new MobileNotificationsPage();
            myPage.verifyEmptyNotifications();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_76_TestEmptyNotifications");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-76", "android_MAUT_76_TestEmptyNotifications.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    // MAUT_77, MAUT_78 are iOS

    // No longer exists
/*    @Test
    public void android_MAUT_79_TestLRAlertCheckbox() throws Exception {
        MobileSubjectDetailsPage myPage = null;

        try {
            myPage = new MobileSubjectDetailsPage();
            myPage.getLearningResourceTips().click();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_79_TestLRAlertCheckbox");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-79", "android_MAUT_79_TestLRAlertCheckbox.pptx");
            if (myPage != null) myPage.quit();
        }
    }
*/
    // The Selenium call we need for this is not yet implemented in Selendroid.
    @Test
    public void android_MAUT_80_TestColorOfIncompleteCourse() throws Exception {
        MobileDegreePlanPage myPage = null;

        try {
            myPage = new MobileDegreePlanPage();
            myPage.WaitForPageLoad(MobileDegreePlanPage.class);
            myPage.swipeRight("//TextView[@id='tvcoursecode']", 1);
            myPage.checkPassedNotPassed();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_80_TestColorOfIncompleteCourse");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-80", "android_MAUT_80_TestColorOfIncompleteCourse.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    // MAUT_81 is iOS

    @Test
    public void android_MAUT_82_TestCourseTips() throws Exception {
        MobileCourseTipsPage myPage = null;

        try {
            myPage = new MobileCourseTipsPage();
            myPage.verifyBasicInfo();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_82_TestCourseTips");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-82", "android_MAUT_82_TestCourseTips.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    // To be validated using Saved Files page, but that isn't complete yet
    @Test
    public void android_MAUT_83_TestDocumentDownload() throws Exception {
        MobileStudyPlanPage myPage = null;

        try {
            myPage = new MobileStudyPlanPage();
            myPage.verifyDocumentDownload();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_83_TestDocumentDownload");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-83", "android_MAUT_83_TestDocumentDownload.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test // Broken in build 1.24.0
    public void android_MAUT_84_TestHamburgerOnAllPages() throws Exception {
        MobileDegreePlanPage myPage = null;

        try {
            myPage = new MobileDegreePlanPage();

            myPage.getNotificationsIcon().click();
            myPage.WaitForPageLoad(MobileNotificationsPage.class);
            wguPowerPointFileUtils.saveScreenshot(myPage, "MobileNotificationsPage");

            myPage.getMentorsIcon().click();
            myPage.WaitForPageLoad(MobileMentorsPage.class);
            wguPowerPointFileUtils.saveScreenshot(myPage, "MobileMentorsPage");

            myPage.getProfileLink().click();
            myPage.WaitForPageLoad(MobileProfilePage.class);
            wguPowerPointFileUtils.saveScreenshot(myPage, "MobileProfilePage");

            myPage.getNotificationSettings().click();
            myPage.WaitForPageLoad(MobileNotificationSettingsPage.class);
            wguPowerPointFileUtils.saveScreenshot(myPage, "MobileNotificationSettingsPage");

            myPage.getAppCenter().click();
            Page.Sleep(3000);
            wguPowerPointFileUtils.saveScreenshot(myPage, "getAppCenter");

            myPage.getResources().click();
            Page.Sleep(3000);
            wguPowerPointFileUtils.saveScreenshot(myPage, "getContactWGU");

        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_84_TestHamburgerOnAllPages");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-84", "android_MAUT_84_TestHamburgerOnAllPages.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_85_CourseChatterUI() throws Exception {
        MobileCourseChatterPage myPage = null;

        try {
            myPage = new MobileCourseChatterPage();
            myPage.verifyBasicInfo();
            myPage.doSomethingRandom(3);
//            myPage.clickAndCheckChatterDetails();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_85_CourseChatterUI");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-85", "android_MAUT_85_CourseChatterUI.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_86_SubjectDetailsBookmark() throws Exception {
        MobileStudyPlanPage myPage = null;

        try {
            myPage = new MobileStudyPlanPage();
            myPage.clickSubject(true, 0);
            if (myPage.Data.isPhone())
                myPage.clickBackButton();
            myPage.WaitForPageLoad(MobileStudyPlanPage.class);

            myPage.clickSubject(false, 0);
            if (myPage.Data.isPhone())
                myPage.clickBackButton();
            myPage.WaitForPageLoad(MobileStudyPlanPage.class);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_86_StudyPlanPageBookmark");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-86", "android_MAUT_86_StudyPlanPageBookmarkA.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_86_SubjectDetailsBookmarkB() throws Exception {
        MobileSubjectDetailsPage myPage = null;

        try {
            myPage  = new MobileSubjectDetailsPage();

            myPage.clickTopic(0);
            myPage.WaitForPageLoad(MobileTopicPage.class);
            if (myPage.Data.isPhone())
                myPage.clickBackButton();
            myPage.WaitForPageLoad(MobileSubjectDetailsPage.class);

            myPage.clickTopic(1);
            myPage.WaitForPageLoad(MobileTopicPage.class);
            if (myPage.Data.isPhone())
                myPage.clickBackButton();
            myPage.WaitForPageLoad(MobileSubjectDetailsPage.class);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_86_SubjectDetailsPageBookmark");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-86", "android_MAUT_86_SubjectDetailsPageBookmarkB.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_97_MultipleUsersA() throws Exception {
        MobileProfilePage myPage = null;

        try {
            myPage = new MobileProfilePage(null, 0L, "apoindexter", "T3st1tn0w");
            myPage.verifyBasicInfo();
            assert(myPage.getByXpath(MobileXpath.ProfilePage_StudentFullname).getText().contains("Amber"));
            assert(myPage.getByXpath(MobileXpath.ProfilePage_StudentFullname).getText().contains("Poindexter"));
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_97_MultipleUsersA");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-97", "android_MAUT_97_MultipleUsersA.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_97_MultipleUsersB() throws Exception {
        MobileProfilePage myPage = null;

        try {
            myPage = new MobileProfilePage(null, 0L, "bpoindexter", "T3st1tn0w");
            assert(myPage.getByXpath(MobileXpath.ProfilePage_StudentFullname).getText().contains("Bill"));
            assert(myPage.getByXpath(MobileXpath.ProfilePage_StudentFullname).getText().contains("Poindexter"));
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_97_MultipleUsersB");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-97", "android_MAUT_97_MultipleUsersB.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_98_DepartmentContactInfo() throws Exception {
        MobileContactWGUPage myPage = null;

        try {
            myPage = new MobileContactWGUPage();
            myPage.verifyBasicInfo();
            myPage.verifyAllButtons();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_98_DepartmentContactInfo");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-98", "android_MAUT_98_DepartmentContactInfo.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_130_MobileAppCenterPage() throws Exception {
        MobileAppCenterPage myPage = null;

        try {
            myPage = new MobileAppCenterPage();
            myPage.verifyBasicInfo();
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_130_MobileAppCenterPage");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-130", "android_MAUT_130_MobileAppCenterPage.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_131_DoRandomThings() throws Exception {
        MobileDegreePlanPage myPage = null;

        try {
            myPage = new MobileDegreePlanPage();
            wguPowerPointFileUtils.saveScreenshot(myPage, "android_doSomethingRandom");
            myPage.doSomethingRandom(20);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_doSomethingRandom");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-131", "android_MAUT_131_DoRandomThings.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    // Currently failing due to password required.
    @Test
    public void android_MAUT_132_ExerciseExternalWGUSites() throws Exception {
        MobileLoginPage myPage = null;

        try {
            myPage = new MobileLoginPage();
            myPage.getNotAStudent().click();
            MobileLoginPage.Sleep(2000);
//            wguPowerPointFileUtils.saveScreenshot(myPage, "getNotAStudent().click()");
            myPage.getByXpath("//ImageView[@id='ivClose']").click();
            MobileLoginPage.Sleep(5000);
//            wguPowerPointFileUtils.saveScreenshot(myPage, "('//ImageView[@id='ivClose']').click()");

//            myPage.tap(314, 924, 4000);
//            myPage.tap(119, 758, 1000);
//            myPage.driver.switchTo().activeElement().sendKeys("Hello@go.com"); // Not implemented assertion
            myPage.tap(25, 300, 1000);
            wguPowerPointFileUtils.saveScreenshot(myPage, "menu");
            myPage.tap(200, 580, 1000);
            wguPowerPointFileUtils.saveScreenshot(myPage, "degrees");
            myPage.tap(935, 306, 1000);
           wguPowerPointFileUtils.saveScreenshot(myPage, "close menu");
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_132_ExerciseExternalWGUSites");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-132", "android_MAUT_132_ExerciseExternalWGUSites.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_133_SubjectDetailsActivitiesUI() throws Exception {
        MobileSubjectActivitiesPage myPage = null;

        try {
            myPage = new MobileSubjectActivitiesPage();
            myPage.verifyBasicInfo();
            wguPowerPointFileUtils.saveScreenshot(myPage, "android_MAUT_133_SubjectDetailsActivitiesUI");
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_133_SubjectDetailsActivitiesUI");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-132", "android_MAUT_133_SubjectDetailsActivitiesUI.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_134_ExerciseResourcesPage() throws Exception {
        MobileResourcesPage myPage = null;

        try {
            myPage = new MobileResourcesPage();
            wguPowerPointFileUtils.saveScreenshot(myPage, "android_MAUT_134_ExerciseResourcesPage");
            myPage.verifyBasicInfo();
//            myPage.doSomethingRandom(4);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_134_ExerciseResourcesPage");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-132", "android_MAUT_134_ExerciseResourcesPage.pptx");
            if (myPage != null) myPage.quit();
        }
    }

    @Test
    public void android_MAUT_136_ExerciseLearningResourcesPage() throws Exception {
        MobileLearningResourcesPage myPage = null;

        try {
            myPage = new MobileLearningResourcesPage();
            wguPowerPointFileUtils.saveScreenshot(myPage, "android_MAUT_136_ExerciseLearningResourcesPage");
            myPage.verifyBasicInfo();
            myPage.doSomethingRandom(4);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_136_ExerciseLearningResourcesPage");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "android_MAUT_136_ExerciseLearningResourcesPage.pptx");
            if (myPage != null) myPage.quit();
        }
    }

/*
    @Test
    public void android_MAUT_140_CourseChatterDetails() throws Exception {
        MobileCourseChatterDetailsPage myPage = null;

        try {
            myPage = new MobileCourseChatterDetailsPage();
            myPage.verifyBasicInfo();
            myPage.doSomethingRandom(30);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR android_MAUT_140_CourseChatterDetails");
            wguPowerPointFileUtils.addBulletPoint(e.getMessage());
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "MAUT-85", "android_MAUT_140_CourseChatterDetails.pptx");
            if (myPage != null) myPage.quit();
        }
    }
*/

}

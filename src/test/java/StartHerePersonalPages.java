
import Utils.*;
import BaseClasses.*;
import PersonalPages.ppLandingPage;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 7/20/2015.
 */

public class StartHerePersonalPages extends wguTestWrapper {

    //
    // Lane 1
    //
    @Test(invocationCount = 1, threadPoolSize = 25)
    public void Lane1ExercisePersonalPages() {
        ppLandingPage myPage = new ppLandingPage();

        Page.Sleep(General.getRandomInt(20) * 100);
        myPage.login();
        myPage.ExercisePage(true);

        myPage.quit();
    }

    //
    // Lane 1
    //
    @Test(invocationCount = 1000, threadPoolSize = 20)
    public void Lane1CmacdoErrorPPLogin() {
        ppLandingPage myPage= null;
        try {
            myPage = new ppLandingPage(LoginType.LANE1_PP_CMACDO);

            Page.Sleep(General.getRandomInt(20) * 100);
            myPage.login();
        } catch (Exception e) {
        } finally {
            if (myPage != null) {
                myPage.getByXpath("//*[@id='topMessage']/table/tbody/tr/td[2]/div/span/strong");
                myPage.quit();
            }
        }
    }

    //
    // Lane 2
    //
    @Test(invocationCount = 1, threadPoolSize = 20)
    public void Lane2DandePPLogin() {
        ppLandingPage myPage = new ppLandingPage(LoginType.LANE2_PP_DANDE20);

        myPage.login();
        Page.Sleep(General.getRandomInt(20) * 100);
        myPage.ExercisePage(true);

        myPage.quit();
    }

    //
    // Lane 2
    //
    @Test(invocationCount = 1, threadPoolSize = 20)
    public void Lane2CwayrynPPLogin() {
        ppLandingPage myPage = new ppLandingPage(LoginType.LANE2_PP_CWAYRYN);

        Page.Sleep(General.getRandomInt(20) * 100);
        myPage.login();
        myPage.ExercisePage(true);

        myPage.quit();
    }
}

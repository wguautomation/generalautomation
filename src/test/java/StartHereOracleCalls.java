import OracleClasses.OracleTodosTask;
import Utils.General;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 1/15/2016.
 */

public class StartHereOracleCalls {

    @Test
    public void GetCpoindexterTasks() throws Exception {
        OracleTodosTask tasks = new OracleTodosTask("cpoindexter");
        tasks.getActiveTasks();

        General.Debug(tasks.toString());
        Thread.sleep(500);
    }

}

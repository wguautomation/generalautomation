
import Utils.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;
import java.util.List;
import java.util.concurrent.TimeUnit;

/*
 * Created by timothy.hallbeck on 4/27/2016.
 */

public class StartHereGregStuff  {

    FormInfo info = new FormInfo();

    @Test(invocationCount = 300, threadPoolSize = 1)
    public void testme() throws Exception {
        WebDriver driver;
        List<WebElement> programOptions;
        List<WebElement> campusOptions;
        List<WebElement> stateOptions;
        List<WebElement> leadSourceOptions;
        WebElement program;
        WebElement campus;
        WebElement state;
        WebElement leadSource;

        int x = info.Increment();
        driver = new FirefoxDriver();
        try {
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            driver.manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS);
            driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
            driver.get("https://test.salesforce.com");
            General.Debug("Created a new driver");

            driver.findElement(By.cssSelector("#username")).clear();
            driver.findElement(By.cssSelector("#username")).sendKeys("greg.hiatt@wgu.edu.qafull");
            driver.findElement(By.cssSelector("#password")).sendKeys("Venom88carnage");
            driver.findElement(By.cssSelector("#Login")).click();
            General.Debug("logged in with thread " + Thread.currentThread().getId());

            driver.findElement(By.cssSelector(".allTabsArrow")).click();
            driver.findElement(By.cssSelector(".listRelatedObject.Custom15Block.title")).click();
            General.Debug("After the all Tabs " + Thread.currentThread().getId());

            //**get all dropdown values**

            //Program of Interest
            program = driver.findElement(By.cssSelector("#j_id0\\:j_id27\\:j_id28\\:j_id29\\:j_id30\\:program"));
            programOptions = program.findElements(By.tagName("option"));

            //Campus
            campus = driver.findElement(By.cssSelector("#j_id0\\:j_id27\\:j_id28\\:j_id29\\:j_id42\\:campus"));
            campusOptions = campus.findElements(By.tagName("option"));

            //Lead Source
            leadSource = driver.findElement(By.cssSelector("#j_id0\\:j_id27\\:j_id28\\:j_id29\\:j_id51\\:leadSource"));
            leadSourceOptions = leadSource.findElements(By.tagName("option"));

            //State
            state = driver.findElement(By.cssSelector("#j_id0\\:j_id27\\:j_id28\\:j_id58\\:j_id59\\:state"));
            stateOptions = state.findElements(By.tagName("option"));

            driver.findElement(By.cssSelector("#j_id0\\:j_id27\\:j_id28\\:j_id29\\:firstname")).sendKeys(info.getFirst());
            driver.findElement(By.cssSelector("#j_id0\\:j_id27\\:j_id28\\:j_id29\\:lastname")).sendKeys(info.getLast(x));
            driver.findElement(By.cssSelector("#j_id0\\:j_id27\\:j_id28\\:j_id29\\:email")).sendKeys(info.getEmail(x));
            driver.findElement(By.cssSelector("#j_id0\\:j_id27\\:j_id28\\:j_id29\\:phone")).sendKeys(info.getPhone());
            driver.findElement(By.cssSelector("#j_id0\\:j_id27\\:j_id28\\:j_id58\\:street")).sendKeys(info.getAddress());
            driver.findElement(By.cssSelector("#j_id0\\:j_id27\\:j_id28\\:j_id58\\:city")).sendKeys(info.getCity());
            driver.findElement(By.cssSelector("#j_id0\\:j_id27\\:j_id28\\:j_id58\\:postalcode")).sendKeys(info.getZipCode());
            General.Debug(" After zip code " + Thread.currentThread().getId());

            //Program Selection
            programOptions.get(info.ran.nextInt(programOptions.size() - 1) + 1).click();
            //Campus Selection
            campusOptions.get(info.ran.nextInt(campusOptions.size() - 1) + 1).click();
            //Lead Source Selection
            leadSourceOptions.get(info.ran.nextInt(leadSourceOptions.size() - 1) + 1).click();
            //State Selection
            stateOptions.get(info.ran.nextInt(stateOptions.size() - 1) + 1).click();

            WebElement save = driver.findElement(By.cssSelector("input.btn:nth-child(1)"));
            save.click();
            General.Debug("saved record with thread " + Thread.currentThread().getId());
        } catch (Exception e) {
            ;
        } finally {
            driver.quit();
        }
    }
}

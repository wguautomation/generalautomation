import BaseClasses.LoginType;
import BaseClasses.WebPage;
import ExtSitePages.extHomePage;
import StudentPortalPages.myHomePage;
import Utils.General;
import Utils.wguPowerPointFileUtils;
import Utils.wguTestWrapper;
import org.testng.annotations.Test;

import java.util.TreeMap;

/*
 * Created by timothy.hallbeck on 7/1/2015.
 */

public class StartHereExternalSites extends wguTestWrapper {

    private String[] inclusionFilterArray = new String[] {
            "wgu.mindedgeonline.com",
            ".wgu.edu",
    };

    @Test
    public void LRPCrawlProd() throws Exception {
        String     startingUrl = "https://lrps.wgu.edu/provision/2290";
        TreeMap    masterMap   = new TreeMap();
        myHomePage myPage      = null;

        try {
            myPage = new myHomePage(null, LoginType.PRODUCTION_LRP, WebPage.Browser.Chrome);
            General.setSilentMode(true);
            wguPowerPointFileUtils.savePresentation = true;
            myPage.login();
            myPage.Sleep(3000);
            myPage.DocumentAllHrefs(masterMap, 1, startingUrl, inclusionFilterArray);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR LRPCrawlProd");
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "zzLRPCrawlProd.pptx");
            myPage.quit();
        }
    }

    @Test
    public void StudentPortalCrawlProd() throws Exception {
        String      startingUrl = "https://my.wgu.edu/group/wgu-student-v3";
        TreeMap     masterMap   = new TreeMap();
        myHomePage myPage      = null;

        try {
            myPage = new myHomePage(null, LoginType.PRODUCTION_NORMAL);
            myPage.login();
            myPage.DocumentAllHrefs(masterMap, 2, startingUrl, inclusionFilterArray);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR StudentPortalCrawlProd");
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "zzTwoLevelCrawlProdRepeatsOnly.pptx");
            myPage.quit();
        }
    }

    @Test
    public void StudentPortalCrawlProdCpoindexter() throws Exception {
        String      startingUrl = "https://my.wgu.edu/group/wgu-student-v3";
        TreeMap     masterMap   = new TreeMap();
        myHomePage myPage      = null;

        try {
            myPage = new myHomePage(null, LoginType.PRODUCTION_NORMAL);
            myPage.login();
            myPage.DocumentAllHrefs(masterMap, 3, startingUrl, inclusionFilterArray);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR StudentPortalCrawlProdCpoindexter");
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "zzStudentPortalCrawlProdCpoindexter.pptx");
            myPage.quit();
        }
    }

    private String[] lane1InclusionFilterArray = new String[] {
//            "l1sh.wgu.edu",
            "l1my.wgu.edu",
            "osso.wgu.edu/opensso",
            "wgu.libguides.com",
            "wgu.edu/passthrough",
            "https://webapp",
    };

    @Test
    public void StudentPortalCrawlLane1() throws Exception {
        String      startingUrl = "https://l1my.wgu.edu/group/wgu-student-v3";
        TreeMap     masterMap   = new TreeMap();
        myHomePage myPage      = null;

        try {
            myPage = new myHomePage(null, LoginType.LANE1_NORMAL);
            General.setSilentMode(true);
            myPage.login();
            myPage.DocumentAllHrefs(masterMap, 6, startingUrl, lane1InclusionFilterArray);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR StudentPortalCrawlLane1");
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "StudentPortalCrawlLane1.pptx");
            myPage.quit();
        }
    }

    @Test
    public void extDocumentNevada() throws Exception {
        String      startingUrl = "http://nevada.wgu.edu";
        String[]    filter      = new String[] {"nevada.wgu.edu"};
        TreeMap     masterMap   = new TreeMap();
        extHomePage myPage      = null;

        try {
            myPage = new extHomePage();
            myPage.load();
            myPage.DocumentAllHrefs(masterMap, 4, startingUrl, filter);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR extDocumentNevada");
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "extDocumentNevada.pptx");
            myPage.quit();
        }
    }




    @Test
    public void extDocumentCareerServices() throws Exception {
        String      startingUrl = "http://national.careerservices.wgu.edu/";
        String[]    filter      = new String[] {"national.careerservices.wgu.edu"};
        TreeMap     masterMap   = new TreeMap();
        extHomePage myPage      = null;

        try {
            myPage = new extHomePage();
            wguPowerPointFileUtils.savePresentation = true;
            myPage.load();
            myPage.DocumentAllHrefs(masterMap, 4, startingUrl, filter);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR extDocumentCareerServices");
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "extDocumentCareerServices.pptx");
            myPage.quit();
        }
    }

    @Test
    public void extDocumentIndiana() throws Exception {
        String      startingUrl = "http://indiana.wgu.edu/";
        String[]    filter      = new String[] {"indiana.wgu.edu"};
        TreeMap     masterMap   = new TreeMap();
        extHomePage myPage      = null;

        try {
            myPage = new extHomePage();
            myPage.load();
            myPage.DocumentAllHrefs(masterMap, 4, startingUrl, filter);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR extDocumentIndiana");
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "extDocumentIndiana.pptx");
            myPage.quit();
        }
    }

    @Test
    public void extDocumentTexas() throws Exception {
        String      startingUrl = "http://texas.wgu.edu/";
        String[]    filter      = new String[] {"texas.wgu.edu"};
        TreeMap     masterMap   = new TreeMap();
        extHomePage myPage      = null;

        try {
            myPage = new extHomePage();
            myPage.load();
            myPage.DocumentAllHrefs(masterMap, 4, startingUrl, filter);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR extDocumentTexas");
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "extDocumentTexas.pptx");
            myPage.quit();
        }
    }

    @Test
    public void extDocumentMissouri() throws Exception {
        String      startingUrl = "http://missouri.wgu.edu/";
        String[]    filter      = new String[] {"missouri.wgu.edu"};
        TreeMap     masterMap   = new TreeMap();
        extHomePage myPage      = null;

        try {
            myPage = new extHomePage();
            myPage.load();
            myPage.DocumentAllHrefs(masterMap, 4, startingUrl, filter);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR extDocumentMissouri");
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "extDocumentMissouri.pptx");
            myPage.quit();
        }
    }

    @Test
    public void extDocumentTennessee() throws Exception {
        String      startingUrl = "http://tennessee.wgu.edu/";
        String[]    filter      = new String[] {"tennessee.wgu.edu"};
        TreeMap     masterMap   = new TreeMap();
        extHomePage myPage      = null;

        try {
            myPage = new extHomePage();
            myPage.load();
            myPage.DocumentAllHrefs(masterMap, 4, startingUrl, filter);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR extDocumentTennessee");
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "extDocumentTennessee.pptx");
            myPage.quit();
        }
    }

    @Test
    public void extDocumentWashington() throws Exception {
        String      startingUrl = "http://washington.wgu.edu/";
        String[]    filter      = new String[] {"washington.wgu.edu"};
        TreeMap     masterMap   = new TreeMap();
        extHomePage myPage      = null;

        try {
            myPage = new extHomePage();
            myPage.load();
            myPage.DocumentAllHrefs(masterMap, 4, startingUrl, filter);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR extDocumentWashington");
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "extDocumentWashington.pptx");
            myPage.quit();
        }
    }

    @Test
    public void extDocumentWguEdu() throws Exception {
        String      startingUrl = "http://www.wgu.edu";
        String[]    filter      = new String[] {"www.wgu.edu"};
        TreeMap     masterMap   = new TreeMap();
        extHomePage myPage      = null;

        try {
            myPage = new extHomePage();
            myPage.load();
            masterMap.put(LoginType.EXTERNAL_SITE.getStartingUrl(), new TreeMap());
            myPage.DocumentAllHrefs(masterMap, 3, startingUrl, filter);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR extDocumentWguEdu");
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "extDocumentWguEdu.pptx");
            myPage.quit();
        }
    }

    private String[] ProdInclusionFilterArray = new String[] {
            "www.wgu.edu",
            "wgu.libguides.com",
            "wgu.edu/passthrough",
    };

    @Test
    public void extCrawlMarketingSiteFirefox() throws Exception {
        String      startingUrl = "http://www.wgu.edu";
        TreeMap     masterMap   = new TreeMap();
        extHomePage myPage      = null;

        try {
            myPage = new extHomePage(null, LoginType.PRODUCTION_NORMAL, WebPage.Browser.Firefox);
            myPage.load();
            wguPowerPointFileUtils.saveScreenshots = true;
            wguPowerPointFileUtils.savePresentation = true;
            masterMap.put(LoginType.EXTERNAL_SITE.getStartingUrl(), new TreeMap());
            myPage.DocumentAllHrefs(masterMap, 2, startingUrl, ProdInclusionFilterArray);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR extCrawlMarketingSiteFirefox");
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "extCrawlMarketingSiteFirefox.pptx");
            myPage.quit();
        }
    }

    @Test
    public void extCrawlMarketingSiteChrome() throws Exception {
        String      startingUrl = "http://www.wgu.edu";
        TreeMap     masterMap   = new TreeMap();
        extHomePage myPage      = null;

        try {
            myPage = new extHomePage(null, LoginType.PRODUCTION_NORMAL, WebPage.Browser.Chrome);
            myPage.load();
            wguPowerPointFileUtils.saveScreenshots = true;
            wguPowerPointFileUtils.savePresentation = true;
            masterMap.put(LoginType.EXTERNAL_SITE.getStartingUrl(), new TreeMap());
            myPage.DocumentAllHrefs(masterMap, 3, startingUrl, ProdInclusionFilterArray);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR extCrawlMarketingSiteChrome");
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "extCrawlMarketingSiteChrome.pptx");
            myPage.quit();
        }
    }

    @Test
    public void extCrawlMarketingSiteIE() throws Exception {
        String      startingUrl = "http://www.wgu.edu";
        TreeMap     masterMap   = new TreeMap();
        extHomePage myPage      = null;

        try {
            myPage = new extHomePage(null, LoginType.PRODUCTION_NORMAL, WebPage.Browser.IE);
            myPage.load();
            masterMap.put(LoginType.EXTERNAL_SITE.getStartingUrl(), new TreeMap());
            myPage.DocumentAllHrefs(masterMap, 1, startingUrl, ProdInclusionFilterArray);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR extCrawlMarketingSiteIE");
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "extCrawlMarketingSiteIE.pptx");
            myPage.quit();
        }
    }

    @Test
    public void extHomePageFull() throws Exception {
        extHomePage myPage = null;

        try {
            myPage = new extHomePage();
            myPage.ExercisePage(true);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR extHomePageFull");
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "extHomePageFull.pptx");
            myPage.quit();
        }
    }

    @Test
    public void extDocumentStagingSite() throws Exception {
        String      startingUrl = "http://stg.www.wgu.edu/";
        String[]    filter      = new String[] {"stg.www.wgu.edu"};
        TreeMap     masterMap   = new TreeMap();
        extHomePage myPage      = null;

        try {
            myPage = new extHomePage();
            myPage.load();
            masterMap.put(LoginType.EXTERNAL_SITE.getStartingUrl(), new TreeMap());
            myPage.DocumentAllHrefs(masterMap, 4, startingUrl, filter);
        } catch (Exception e) {
            wguPowerPointFileUtils.saveScreenshot(myPage, "ERROR extDocumentStagingSite");
            throw new Exception(e);
        } finally {
            wguPowerPointFileUtils.savePresentation(myPage, "", "extDocumentStagingSite.pptx");
            myPage.quit();
        }
    }


}

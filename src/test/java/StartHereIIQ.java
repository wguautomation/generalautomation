
import IiqPages.iiqChangePasswordsPage;
import IiqPages.iiqMainPage;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 12/18/2015.
 */

public class StartHereIIQ {
    @Test
    public void iiqLogin() {
        iiqMainPage page = new iiqMainPage();
        page.ExercisePage(false);
        page.driver.close();
    }

    private static String[] accountNames = new String[] {
/*            "dkite",
            "jkhatri",
            "kbro156",
            "khawthor",
            "lbenn16",
            "mdoler",
            "sdaily1",
            "smicha1",
            "ssibbli",
            "svaug16",
            "tsumral",
            "twittma",
            "mpoindexter",
            "kpoindexter",
            "npoindexter",
            "rpoindexter",
            "wpoindexter",
            "zschwar",
            "ssibbli",
            "svaug16",
            "tsumral",
            "twittma",
            "zschwar",
            "spoltus",
            "kburge5",
            "ahedin1",
            "mpolk4",
            "tharmo5",
            "bmalone",
            "aclar72",
            "drobi53",
            "jrosse3",
            "efillio",
            "jstew58",
            "klaugh3",
 */           "ppoindexter",
            "mpoindexter",
            "kpoindexter",
            "npoindexter",
            "cpoindexter",
            "rpoindexter",
            "wpoindexter",
            "kvance",
    };
    @Test
    public void iiqStandardizePasswordsLane1() {
        iiqChangePasswordsPage page = new iiqChangePasswordsPage();

        for (String name : accountNames) {
            page.load();
            page.standardizePassword(name, "T3st1tn0w");
        }

        page.driver.close();
    }

}

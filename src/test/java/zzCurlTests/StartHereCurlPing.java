package zzCurlTests;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonServiceMappings;
import ServiceClasses.MentorService;
import ServiceClasses.PersonService;
import Utils.General;
import Utils.PingSession;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 6/13/2016.
 */

public class StartHereCurlPing extends PingSession {

    @Test(invocationCount = 1000, threadPoolSize = 20)
    public void execCurlPingGetsProd() throws Exception {
        LoginType login = LoginType.PRODUCTION_NORMAL;
        String result="";
        General.setSilentMode(true);

        try {
//            result = Get("https://mentor.wgu.edu/v1/information/" + login.getPidm(), login);
//            String mentorPidm = verifyKey(result, "pidm");
//            result = Get("https://mentor.wgu.edu/v1/mentorPhotoUrl/" + mentorPidm, login);

//            result = Get("https://student-directory.wgu.edu/v1/studentConnection/student/" + login.getPidm() + "/connection/", login);
//            String connectionPidm = verifyKey(result, "pidm");

//        result = Get("https://student-directory.wgu.edu/v1/studentConnection/student/" + login.getPidm() + "/connection/" + connectionPidm, login);

            result = Get("https://student-profile.wgu.edu/v1/students/profile/pidm/" + login.getPidm() + "/public", login);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            General.Debug(result);
        }
    }

    @Test
    public void createPersonServices() throws Exception {
        new PersonService(LoginType.LANE2_NORMAL, AuthType.MASHERY);
        new PersonService(LoginType.LANE2_NORMAL, AuthType.OSSO);
        new PersonService(LoginType.LANE2_NORMAL, AuthType.PING);

        new PersonService(LoginType.LANE1_NORMAL, AuthType.MASHERY);
        new PersonService(LoginType.LANE1_NORMAL, AuthType.OSSO);
        new PersonService(LoginType.LANE1_NORMAL, AuthType.PING);
    }

    @Test
    public void createMentorServices() throws Exception {
        new MentorService(LoginType.LANE2_NORMAL, AuthType.MASHERY);
        new MentorService(LoginType.LANE2_NORMAL, AuthType.OSSO);
        new MentorService(LoginType.LANE2_NORMAL, AuthType.PING);

        new MentorService(LoginType.LANE1_NORMAL, AuthType.MASHERY);
        new MentorService(LoginType.LANE1_NORMAL, AuthType.OSSO);
        new MentorService(LoginType.LANE1_NORMAL, AuthType.PING);
    }

    @Test
    public void execCurlPingGetsQa() throws Exception {

        LoginType login = LoginType.LANE1_NORMAL, login2 = LoginType.LANE1_MPOINDEXTER;;
        String result="", result2="";

        try {
            result = Get("https://newsitems.qa.wgu.edu/mappings", login);
            General.Debug(result);
            JsonServiceMappings mappings2 = new JsonServiceMappings(result);

            // get connections
            result = Get("https://student-directory.qa.wgu.edu/v1/studentConnection/student/" + login.getPidm() + "/connection/", login);
            General.Debug(result);

            result2 = Put("https://student-directory.qa.wgu.edu/v1/studentConnection/student/" + login.getPidm() + "/connection/98797" , login);
            General.Debug(result2);

            result2 = Get("https://mentor.qa.wgu.edu/mappings", login);
            General.Debug(result2);

            // not working
//            result = Get("https://field-experience-api.qa.wgu.edu/v1/students/" + login.getPidm() + "/experience", login);

//            result = Post("https://student-directory.qa.wgu.edu/v1/studentConnection/student/" + login.getPidm() + "/connection/" + login.getPidm(), login);
//            String connectionPidm = verifyKey(result, "pidm");

            // add connection
//            result = Put("https://student-directory.qa.wgu.edu/v1/studentConnection/student/" + login.getPidm() + "/connection/98797", login);

        } catch (Exception e) {
            General.Debug(e.getMessage());
        }
    }

    @Test
    public void QaMappings() throws Exception {

        LoginType login = LoginType.LANE1_NORMAL;
        String result = "";

        try {
            PingSession news = new PingSession("newsitems", login, ServiceKey.MAPPINGS);

            int a=0;
        } catch (Exception e) {
            General.Debug(e.getMessage());
            General.Debug(result);
        }
    }

    @Test(invocationCount = 1, threadPoolSize = 20)
    public void execCurlPingGetsDev() throws Exception {
        String result = "";
        LoginType login = LoginType.LANE2_CPOINDEXTER, login2 = LoginType.LANE2_NORMAL;
        JsonServiceMappings mappings = null;

        try {
// No mappings for assessments
//            result = Get("https://assessments.dev.wgu.edu/mappings", login);
//            mappings = new JsonServiceMappings(result);

            result = Get("https://coursecommunities.dev.wgu.edu/mappings", login);
            mappings = new JsonServiceMappings(result);

            // 38.8/sec
            result = Get("https://mobiletoken.dev.wgu.edu/mappings", login);
            mappings = new JsonServiceMappings(result);

            result = Get("https://newsitems.dev.wgu.edu/mappings", login);
            mappings = new JsonServiceMappings(result);

            result = Get("https://newsitems.dev.wgu.edu/v1/api/feed/106679/1417000000", login);
            General.Debug(result);

            result = Get("https://mentor.dev.wgu.edu/mappings", login);
            mappings = new JsonServiceMappings(result);

            result = Put("https://student-directory.dev.wgu.edu/v1/studentConnection/student/" + login.getPidm() + "/connection/" + login2.getPidm(), login);
            General.Debug(result);

            // 30.8/sec
//            result = Get("https://student-directory.dev.wgu.edu/v1/studentConnection/student/" + login.getPidm() + "/connection/", login);
//            General.Debug(result);

            // 13.2/sec
//            result = Get("https://field-experience-api.dev.wgu.edu/v1/students/" + login.getPidm() + "/experience", login);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            General.Debug(result);
        }
    }

}

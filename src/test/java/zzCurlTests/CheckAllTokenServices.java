
package zzCurlTests;

import ServiceClasses.ServiceProcessor;
import Utils.General;
import Utils.wguTestWrapper;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 9/20/2016.
 */

public class CheckAllTokenServices extends wguTestWrapper {

    @Test
    public void Lane2Ping() {
        String output;
        output = new ServiceProcessor().Process("Person Service", "Lane 2 CPoindexter", "Ping", "Return an auth token");
        General.Debug("\nResult:\n" + output + "\n");
    }

    @Test
    public void Lane1Ping() {
        String output;
        output = new ServiceProcessor().Process("Person Service", "Lane 1 CPoindexter", "Ping", "Return an auth token");
        General.Debug("\nResult:\n" + output + "\n");
    }

    @Test
    public void Lane2Osso() {
        String output;
        output = new ServiceProcessor().Process("Person Service", "Lane 2 CPoindexter", "Osso", "Return an auth token");
        General.Debug("\nResult:\n" + output + "\n");
    }

    @Test
    public void Lane1Osso() {
        String output;
        output = new ServiceProcessor().Process("Person Service", "Lane 1 CPoindexter", "Osso", "Return an auth token");
        General.Debug("\nResult:\n" + output + "\n");
    }

    @Test
    public void ProdOsso() {
        String output;
        output = new ServiceProcessor().Process("Person Service", "Prod CPoindexter", "Osso", "Return an auth token");
        General.Debug("\nResult:\n" + output + "\n");
    }

    @Test
    public void Lane2Mashery() {
        String output;
        output = new ServiceProcessor().Process("Person Service", "Lane 2 CPoindexter", "Mashery", "Return an auth token");
        General.Debug("\nResult:\n" + output + "\n");
    }

    @Test
    public void Lane1Mashery() {
        String output;
        output = new ServiceProcessor().Process("Person Service", "Lane 1 CPoindexter", "Mashery", "Return an auth token");
        General.Debug("\nResult:\n" + output + "\n");
    }

    @Test
    public void ProdMashery() {
        String output;
        output = new ServiceProcessor().Process("Person Service", "Prod CPoindexter", "Mashery", "Return an auth token");
        General.Debug("\nResult:\n" + output + "\n");
    }

}

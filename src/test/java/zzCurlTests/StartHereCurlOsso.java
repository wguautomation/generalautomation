package zzCurlTests;

import BaseClasses.LoginType;
import Utils.*;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 9/15/2015.
 */

@Test
public class StartHereCurlOsso extends OssoSession {

    @Test(invocationCount = 1, threadPoolSize = 20)
    public void execCurlOssoGetRfc5827() throws Exception {
        LoginType login;
        String command, result;
//        General.runSilent();

        // Each of these return headers but no values
        login = LoginType.LANE2_NORMAL;
        command = "https://program.dev.wgu.edu/programProgress/v1/api/ProgramProgress/" + login.getPidm();
        result = Get(command, login);
        General.Debug(result);

        login = LoginType.LANE1_CPOINDEXTER;
        command = "https://program.qa.wgu.edu/programProgress/v1/api/ProgramProgress/" + login.getPidm();
        result = Get(command, login);
        General.Debug(result);

        login = LoginType.PROD_CPOINDEXTER;
        command = "https://program.wgu.edu/programProgress/v1/api/ProgramProgress/" + login.getPidm();
        result = Get(command, login);
        General.Debug(result);
    }

    @Test
    public void execCurlOssoPostRfc5840() throws Exception {
        LoginType login;
        String result;

        login = LoginType.LANE1_NORMAL;
        result = Post("https://l1api.wgu.edu/feedback/v1", login, "need data here");
        General.Debug(result);

        login = LoginType.LANE2_NORMAL;
        result = Post("https://l2api.wgu.edu/feedback/v1", login, "need data here");
        General.Debug(result);
    }

    // Lane2Dev
    @Test(invocationCount = 500, threadPoolSize = 20)
    public void execCurlOSSOGetsDev() throws Exception {
        String result = "";
        General.setSilentMode(true);
        LoginType login = LoginType.LANE2_CPOINDEXTER, login2 = LoginType.LANE2_NORMAL;

        try {
            // 30.5/sec
//            result = Get("https://student-directory.dev.wgu.edu/v1/studentConnection/student/" + login2.getPidm() + "/connection", login2);
//            General.Debug(result);

            // 38.1/sec
//            result = Get("https://mashery.wgu.edu/dev/studentdirectory/v1/studentDirectorySearch/suggestionTag/tagTypes", login2);
//            General.Debug(result);

            // 35.8/sec
//            result = Get("https://mashery.wgu.edu/dev/studentdirectory/v1/studentDirectorySearch/suggestionTag/colleges", login2);
//            General.Debug(result);

            // 26/sec
            result = Get("https://mashery.wgu.edu/dev/studentdirectory/v1/studentDirectorySearch/suggestionTag/courses", login2);
            General.Debug(result);

            // /sec  not working currently
//            result = Put("https://student-directory.dev.wgu.edu/v1/studentConnection/student/" + login.getPidm() + "/connection/" + login2.getPidm(), login);
//            General.Debug(result);

            // 13/sec
//            result = Get("https://field-experience-api.dev.wgu.edu/v1/students/" + login.getPidm() + "/experience", login);
//            General.Debug(result);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            General.Debug(result);
        }
    }
    // Qa
    @Test
    public void execCurlOssoGetsQa() throws Exception {
        LoginType login = LoginType.LANE1_NORMAL, login2 = LoginType.LANE1_MPOINDEXTER;
        String result;

        result = Get("https://student-directory.dev.wgu.edu/v1/studentConnection/student/" + login.getPidm() + "/connection/" + login2.getPidm(), login);
        General.Debug(result);

        result = Get("https://student-directory.dev.wgu.edu/v1/studentConnection/student/" + login.getPidm() + "/connection/", login);
        General.Debug(result);

        result = Get("https://field-experience-api.dev.wgu.edu/v1/students/" + login.getPidm() + "/experience", login);
        General.Debug(result);
    }

    //
    // Lane2Dev FDP OSSO - ok at 120 threads, crashes at 240 ~ 677. Restarts in < 10 minutes.
    //
    @Test(dataProviderClass = wguDataProviders.class, dataProvider = "curlFDPDevGETCommands")
    public void execCurlFDPDevGETCommands(String command, String verifyKey) throws Exception {
        String result = Get(command, LoginType.LANE2_NORMAL);
        verifyKey(result, verifyKey);
        Thread.sleep(500);
    }

    //
    // Qa FDP OSSO
    //
    @Test(dataProviderClass = wguDataProviders.class, dataProvider = "curlFDPQaGETCommands")
    public void execCurlFDPQaGETCommands(String command, String verifyKey) throws Exception {
        String result = Get(command, LoginType.LANE1_NORMAL);
        verifyKey(result, verifyKey);
        Thread.sleep(500);
    }

    //
    // Prod FDP OSSO
    //
    @Test(dataProviderClass = wguDataProviders.class, dataProvider = "execCurlFDPProdGETCommandsCpoindexter")
    public void execCurlFDPProdGETCommandsCpoindexter(String command, String verifyKey) throws Exception {
        String result = Get(command, LoginType.PROD_CPOINDEXTER);
        Thread.sleep(250);
        verifyKey(result, verifyKey);
    }

    //
    // Prod FDP OSSO gets
    //
    @Test
    public void execCurlProdOSSOTokenConfirmation() throws Exception {
        String result = Get("https://ossotokenvalidation.wgu.edu/v1/attributes", LoginType.PROD_CPOINDEXTER);
        Thread.sleep(1000);
        verifyKey(result, "userdetails.token.id");

        result = Get("https://ossotokenvalidation.wgu.edu/v1/isTokenValid", LoginType.PROD_CPOINDEXTER);
        assert(result.contains("true"));
        Thread.sleep(1000);
    }

    //
    // Qa ne-sub stress to failure, mashery
    //
    @Test(threadPoolSize = 50, invocationCount = 200)
    public void execCurlNeSubStressToFailureQA() throws Exception {
        // to give 'slow'OSSO time to service the previous token request
        for (int i=0; i<200; i++) {
            String result = Get("https://mashery.wgu.edu/qa/ne-subscriber/v1/mpoindexter/notifications", LoginType.LANE1_MPOINDEXTER);
            Thread.sleep((int) (Math.random() * 1000));
        }
    }



}

















package zzCurlTests;

import BaseClasses.LoginType;
import JsonDataClasses.JsonProgramProgress;
import JsonDataClasses.JsonProgramProgressCalculated;
import Utils.General;
import Utils.MasherySession;
import Utils.wguDataProviders;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 2/4/2016.
 */

public class StartHereCurlMashery extends MasherySession {

    @Test
    public void execCurlMasheryGetRfc5827() throws Exception {
        General.Debug("Program Progress, all 3 environments:");
        JsonProgramProgress.checkAllKeys(runCurlTest("programProgress/v1/api/ProgramProgress/" + LoginType.LANE2_CPOINDEXTER.getPidm(), LoginType.LANE2_CPOINDEXTER));
        JsonProgramProgress.checkAllKeys(runCurlTest("programProgress/v1/api/ProgramProgress/" + LoginType.LANE1_CPOINDEXTER.getPidm(), LoginType.LANE1_CPOINDEXTER));
        JsonProgramProgress.checkAllKeys(runCurlTest("programProgress/v1/api/ProgramProgress/" + LoginType.PROD_CPOINDEXTER.getPidm(), LoginType.PROD_CPOINDEXTER));

        General.Debug("\nProgram Progress Calculated, all 3 environments:");
        JsonProgramProgressCalculated.checkAllKeys(runCurlTest("programProgress/v1/api/ProgramProgress/" + LoginType.LANE2_CPOINDEXTER.getPidm() + "/calculated", LoginType.LANE2_CPOINDEXTER));
        JsonProgramProgressCalculated.checkAllKeys(runCurlTest("programProgress/v1/api/ProgramProgress/" + LoginType.LANE1_CPOINDEXTER.getPidm() + "/calculated", LoginType.LANE1_CPOINDEXTER));
        JsonProgramProgressCalculated.checkAllKeys(runCurlTest("programProgress/v1/api/ProgramProgress/" + LoginType.PROD_CPOINDEXTER.getPidm() + "/calculated", LoginType.PROD_CPOINDEXTER));
    }

    @Test
    public void execCurlMasheryPostRfc5840() throws Exception {
        LoginType login;
        String result;

        login = LoginType.LANE2_NORMAL;
        result = Post("feedback/v1", login,
                "\" { \\\"userName\\\" : \\\"MSAI\\\", \\\"feedbackType\\\" : \\\"problem\\\", \\\"comment\\\" : \\\"Good, this is my comment.\\\", \\\"deviceType\\\" : \\\"Android\\\" } \" ");
        General.Debug("Return from {https://api.dev.wgu.edu/feedback/v1} is:\n  " + result + "\n");

        login = LoginType.LANE1_NORMAL;
        result = Post("feedback/v1", login,
                "\" { \\\"userName\\\" : \\\"MSAI\\\", \\\"feedbackType\\\" : \\\"problem\\\", \\\"comment\\\" : \\\"Good, this is my comment.\\\", \\\"deviceType\\\" : \\\"Android\\\" } \" ");
        General.Debug("Return from {https://api.qa.wgu.edu/feedback/v1} is:\n  " + result + "\n");
    }

    //
    // Prod Mashery Gets
    //
    @Test(dataProviderClass = wguDataProviders.class, dataProvider = "curlGETCommands")
    public void execCurlGETCommandsProd(String command, String verifyKey) throws Exception {
        String result = Get(command, LoginType.PROD_CPOINDEXTER);
        verifyKey(result, verifyKey);
        Thread.sleep(250);
    }
    @Test(dataProviderClass = wguDataProviders.class, dataProvider = "curlGETAssessmentCommands")
    public void execCurlGETAssessmentCommandsProd(String command, String verifyKey) throws Exception {
        String result = Get(command, LoginType.PROD_CPOINDEXTER);
        verifyKey(result, verifyKey);
        Thread.sleep(250);
    }

    //
    // Prod Mentor Mashery gets
    //
    @Test(dataProviderClass = wguDataProviders.class, dataProvider = "curlMentorGETCommands")
    public void execCurlMentorGETCommandsProd(String command, String verifyKey) throws Exception {
        setUsername("mentor1.qa");
        setPassword("Q@M3n70r");
        String result = Get(command, LoginType.PROD_CPOINDEXTER);
        verifyKey(result, verifyKey);
        Thread.sleep(500);
    }

    //
    // All Mashery Get
    //
    @Test(dataProviderClass = wguDataProviders.class, dataProvider = "curlGETCommands")
    public void execCurlGETCommandsBadBearer(String command, String verifyKey) throws Exception {
        devBearer  = "zzzzzzzzzzzzzzzzzzzzzzzz";
        qaBearer   = "zzzzzzzzzzzzzzzzzzzzzzzz";
        prodBearer = "zzzzzzzzzzzzzzzzzzzzzzzz";

        String result = Get(command, LoginType.LANE2_NORMAL);
        verifyKey(result, "$<h1>Developer Inactive</h1>");

        result = Get(command, LoginType.LANE1_NORMAL);
        verifyKey(result, "$<h1>Developer Inactive</h1>");

        result = Get(command, LoginType.PROD_CPOINDEXTER);
        verifyKey(result, "$<h1>Developer Inactive</h1>");

        Thread.sleep(500);
    }

    //
    // All Mashery gets
    //
    @Test(dataProviderClass = wguDataProviders.class, dataProvider = "curlGETCommandsUserSpecific")
    public void execCurlGETCommandsWrongBearer(String command, String verifyKey) throws Exception {
        setIgnoreErrors(true);

        setUsername("eschec1");
        setPassword("L2mobile");
        String result = Get(command, LoginType.LANE2_NORMAL);
        verifyKey(result, "error",   "Unauthorized", false);
        verifyKey(result, "message", "User not authorized", false);

/*        setUsername("gpoindexter");
        setPassword("T3st1tn0w");
        result = assembleAndExecuteCurl(wguEnvironment.Lane1Qa, GetPost.GET, command);
        verifyKey(result, "error",   "Unauthorized");
        verifyKey(result, "message", "User not authorized");

        setUsername("apoindexter");
        setPassword("T3st1tn0w");
        result = assembleAndExecuteCurl(wguEnvironment.Prod, GetPost.GET, command);
        verifyKey(result, "error",   "Unauthorized");
        verifyKey(result, "message", "User not authorized");
*/
        Thread.sleep(500);
    }

    //
    // Prod Mashery post
    //
    @Test(dataProviderClass = wguDataProviders.class, dataProvider = "curlMentorPOSTCommands")
    public void execCurlMentorPOSTCommandsProd(String command, String verifyKey) throws Exception {
        setUsername("mentor1.qa");
        setPassword("Q@M3n70r");
        String result = Post(command, LoginType.PROD_CPOINDEXTER, null);
        verifyKey(result, verifyKey);
        Thread.sleep(500);
    }

    //
    // Lane1Qa Mashery Gets
    //
    @Test(dataProviderClass = wguDataProviders.class, dataProvider = "curlGETCommands")
    public void execCurlGETCommandsQa(String command, String verifyKey) throws Exception {
        String result = Get(command, LoginType.LANE1_NORMAL);
        verifyKey(result, verifyKey);
        Thread.sleep(500);
    }

    //
    // Lane2Dev Mashery Gets
    //
    @Test(dataProviderClass = wguDataProviders.class, dataProvider = "curlGETCommands", singleThreaded = true)
    public void execCurlGETCommandsDev(String command, String verifyKey) throws Exception {
        String result = Get(command, LoginType.LANE2_NORMAL);
        verifyKey(result, verifyKey);
        Thread.sleep(500);
    }

    @Test(invocationCount = 1, threadPoolSize = 20)
    public void execCurlMasheryGetsDev() throws Exception {
        String result = "";
//        General.runSilent();
        LoginType login = LoginType.LANE2_CPOINDEXTER, login2 = LoginType.LANE2_NORMAL;

        try {
            // 38.8/sec
            result = Put("https://student-directory.dev.wgu.edu/v1/studentConnection/student/" + login.getPidm() + "/connection/" + login2.getPidm(), login);
            General.Debug(result);

            // 30.8/sec
            result = Get("https://student-directory.dev.wgu.edu/v1/studentConnection/student/" + login.getPidm() + "/connection", login);
            General.Debug(result);

            // 13.2/sec
//            result = Get("https://field-experience-api.dev.wgu.edu/v1/students/" + login.getPidm() + "/experience", login);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            General.Debug(result);
        }
    }

}

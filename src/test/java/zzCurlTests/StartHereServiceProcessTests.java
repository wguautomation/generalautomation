
package zzCurlTests;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import JsonDataClasses.JsonServiceDump;
import JsonDataClasses.JsonServiceTrace;
import ServiceClasses.*;
import Utils.General;
import Utils.wguTestWrapper;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 9/16/2016.
 */

public class StartHereServiceProcessTests extends wguTestWrapper {

    @Test
    public void showThreads() {
        String result;

        MentorService m = new MentorService(LoginType.LANE1_NORMAL, AuthType.OSSO);
        result = m.osso.Get(m.getAppName(), "/dump", LoginType.LANE1_NORMAL, null);
        JsonServiceDump threadDump = new JsonServiceDump(result);
        threadDump.getPlaceholder();
    }

    @Test
    public void showRequests() {
        String result;

        MentorService m = new MentorService(LoginType.LANE1_NORMAL, AuthType.OSSO);
        result = m.osso.Get(m.getAppName(), "/trace", LoginType.LANE1_NORMAL, null);
        JsonServiceTrace threadDump = new JsonServiceTrace(result);
        threadDump.getPlaceholder();
    }

    @Test(invocationCount = 1000, threadPoolSize = 20)
    public void hammerServices() {
        try {
//            General.setSilentMode(true);
            PersonService person = new PersonService(LoginType.LANE1_CPOINDEXTER, AuthType.OSSO);
            person.ExerciseEndpoints(LoginType.LANE1_CPOINDEXTER, AuthType.OSSO);

//            MentorService mentor = new MentorService(LoginType.PROD_CPOINDEXTER, AuthType.OSSO);
//            mentor.ExerciseEndpoints(LoginType.PROD_CPOINDEXTER, AuthType.OSSO);

//            for (String string : pidmTestStrings) {
//                person.osso.Get(person.getAppName(), "/v1/person/pidm/" + string, LoginType.PROD_CPOINDEXTER);
//                mentor.osso.Get(mentor.getAppName(), "/v1/mentorPhotoUrl/" + string, LoginType.PROD_CPOINDEXTER);
//            }

            General.Sleep(100);

        } catch (Exception e) {
            General.Debug(e.getMessage());
        }
    }

    String[] pidmTestStrings = {
            "",
            "-1",
            "0",
            "1",
            "123456789012345678",
            "1234567890123456789",
            "sdfsdfasfasdfasdfasdfdasfdasfdasfdasfsdasfasdfasf",
    };

    @Test(invocationCount = 8001, threadPoolSize = 60)
    public void hammerServices2() {
        try {
            General.setSilentMode(true);
            DegreePlanService d = new DegreePlanService(LoginType.LANE1_NORMAL, AuthType.OSSO);
            d.ExerciseEndpoints(LoginType.LANE1_NORMAL, AuthType.OSSO);

            for (String string : pidmTestStrings)
                d.StandardEndpointCheck("/v1/api/degreeplans/current/pidm/" + string, AuthType.OSSO, false);

            General.Sleep(General.getRandomInt(300) + 50);

        } catch (Exception e) {
            General.Debug(e.getMessage());
            General.Sleep(100);
        }
    }

    @Test
    public void newway() {
        ServiceBase service = ServiceNames.Find("Academic Activity Service");
        General.DebugAndOutputWindow(service.getAppName());
    }

    @Test
    public void single() {
        CosBService service = new CosBService();
        service.load(LoginType.LANE1_NORMAL, AuthType.OSSO);
        service.ExerciseEndpoints(LoginType.LANE1_NORMAL, AuthType.OSSO);
    }

    @Test
    public void testEndpointsLane2() {
        FinAidVAService service = new FinAidVAService();

        service.UnitTest(LoginType.LANE2_CPOINDEXTER, AuthType.OSSO);
        service.UnitTest(LoginType.LANE2_CPOINDEXTER, AuthType.PING);

        service.FuncionalTest(LoginType.LANE2_CPOINDEXTER, AuthType.OSSO);
        service.FuncionalTest(LoginType.LANE2_CPOINDEXTER, AuthType.PING);
    }

}

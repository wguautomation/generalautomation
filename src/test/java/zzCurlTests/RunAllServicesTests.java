
package zzCurlTests;

import BaseClasses.AuthType;
import BaseClasses.LoginType;
import ServiceClasses.ServiceNames;
import Utils.General;
import Utils.wguTestWrapper;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 9/21/2016.
 */

public class RunAllServicesTests extends wguTestWrapper {

    //
    // Unit tests
    //
    @Test(dataProviderClass = ServiceNames.class, dataProvider = "ServiceDisplayNames", timeOut = 5000)
    public void UnitTestsLane2OSSO(String displayName) {
        ServiceNames.Find(displayName).UnitTest(LoginType.LANE2_CPOINDEXTER, AuthType.OSSO);
        General.Sleep(100);
    }

    @Test(dataProviderClass = ServiceNames.class, dataProvider = "ServiceDisplayNames", timeOut = 5000)
    public void UnitTestsLane1OSSO(String displayName) {
        ServiceNames.Find(displayName).UnitTest(LoginType.LANE1_CPOINDEXTER, AuthType.OSSO);
        General.Sleep(100);
    }


    //
    // Functional tests
    //
    @Test(dataProviderClass = ServiceNames.class, dataProvider = "ServiceDisplayNames", timeOut = 12000)
    public void FunctionalTestsLane2(String displayName) {
        ServiceNames.Find(displayName).FuncionalTest(LoginType.LANE2_CPOINDEXTER, AuthType.OSSO);
//        ServiceNames.Find(displayName).FuncionalTest(LoginType.LANE2_CPOINDEXTER, AuthType.MASHERY);
//        ServiceNames.Find(displayName).FuncionalTest(LoginType.LANE2_CPOINDEXTER, AuthType.PING);
        General.Sleep(250);
    }

    @Test(dataProviderClass = ServiceNames.class, dataProvider = "ServiceDisplayNames", timeOut = 12000)
    public void FunctionalTestsLane1(String displayName) {
        ServiceNames.Find(displayName).FuncionalTest(LoginType.LANE1_CPOINDEXTER, AuthType.OSSO);
//        ServiceNames.Find(displayName).FuncionalTest(LoginType.LANE1_CPOINDEXTER, AuthType.MASHERY);
//        ServiceNames.Find(displayName).FuncionalTest(LoginType.LANE1_CPOINDEXTER, AuthType.PING);
        General.Sleep(250);
    }

    @Test(dataProviderClass = ServiceNames.class, dataProvider = "ServiceDisplayNames", timeOut = 12000)
    public void FunctionalTestsProd(String displayName) {
        ServiceNames.Find(displayName).FuncionalTest(LoginType.PROD_CPOINDEXTER, AuthType.OSSO);
//        ServiceNames.Find(displayName).FuncionalTest(LoginType.PROD_CPOINDEXTER, AuthType.MASHERY);
//        ServiceNames.Find(displayName).FuncionalTest(LoginType.PROD_CPOINDEXTER, AuthType.PING);
        General.Sleep(250);
    }

}
